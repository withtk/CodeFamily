package com.tkip.phcode3.init

import android.app.Application
import com.tkip.mycode.funs.common.lllogI
import com.tkip.mycode.funs.common.showLog
import com.tkip.mycode.init.COUNTRY
import com.tkip.mycode.init.base.getDrawable
import com.tkip.mycode.init.base.getStr
import com.tkip.mycode.init.part.CodeApp
import com.tkip.phcode3.BuildConfig
import com.tkip.phcode3.R


class Ph3App : Application() {

    init {
        instance = this
    }

    companion object {
        private var instance: Ph3App? = null

        fun getAppContext(): Ph3App {
            return instance as Ph3App
        }

    }

    override fun onCreate() {
        super.onCreate()
        showLog = true   // 로그 스위치
        lllogI("MyAPP PhApp onCreate")

        CodeApp(applicationContext)
        COUNTRY = "ph_"  // todo : ph_ 다른 나라 prefix.
        CodeApp.versionCode = BuildConfig.VERSION_CODE
        CodeApp.versionName = BuildConfig.VERSION_NAME
        CodeApp.clientId = R.string.default_web_client_id.getStr()
        CodeApp.icon = R.drawable.ic_logo_box.getDrawable()

    }

}
