package tkcore.info.phadmin.init

import com.tkip.mycode.R
import com.tkip.mycode.funs.common.lllogD
import com.tkip.mycode.init.base.getStr
import com.tkip.mycode.model.cost.balance.balanceDiv1111
import com.tkip.mycode.model.cost.balance.balanceDiv2
import com.tkip.mycode.model.my_enum.CostType


/**
 * 금액 정리.
 * 순서 중요 : BalanceMap 만들때 기준이 됨.
 */
enum class CostPrice( val no: Int,val costNo: Int, val rate: Int?, val pnt: Int?, val bcrd: Int?, val crd: Int?, val crt: Int?) {

    NOTHING(200, CostType.NOTHING.no,2, null, null, null, null),       // 항목 미정

    /** CONVERT : 본인이 전환 */
    CRD_TO_PAY(500, CostType.CONVERT.no,null, null, null, null, null),    // receive
    PAY_TO_CRD(600, CostType.CONVERT.no,null, null, null, null, null),    // receive
    PNT_TO_BCRD(700, CostType.CONVERT.no,null, null, null, null, null),  // receive
    CRD_TO_PNT(800, CostType.CONVERT.no,null, null, null, null, null),  // receive
    CHANGE(900, CostType.CONVERT.no,null, null, null, null, null),        // give
    MONEY(1000, CostType.CONVERT.no,null, null, null, null, null),        //  결제 했을 때


    /** SEND : 유저간 주고받기 */
    GIVE_CREDIT(2000, CostType.SEND.no,null, null, null, null, null), // 다른유저에게 전달


    /** TAKE : 획득 */
    SIGN_UP( 3000, CostType.TAKE.no,null, 5000, null, null, null),  // 가입축하
    RECOMMEND( 3020, CostType.TAKE.no,null, null, 500, null, null),  // 추천인 보너스
    RECOMMENDED( 3040, CostType.TAKE.no,null, null, 2000, null, null),  // 추천받은 유저 보너스

    BUY_CREDIT(3500, CostType.TAKE.no,null, null, null, null, null), // 크레딧 구매
    TAKE_CREDIT(3600, CostType.TAKE.no,null, null, null, null, null),// 다른유저에게 받을 때

    BETA_FREE_BONUS_CREDIT(3700, CostType.TAKE.no,null, null, 20000, null, null), // 베타테스터 보너스
//    FIRST_ADD_STORE(3701, null, null, 1500, null, null), // 처음 보너스
//    FIRST_ADD_BOARD(3702, null, null, 1200, null, null), // 처음 보너스
//    FIRST_ADD_POST(3703, null, null, 1000, null, null),  // 처음 보너스

    FREE_BONUS_CREDIT(3720, CostType.TAKE.no,null, null, null, null, null), // 무료 보너스 크레딧
    THANKS_BONUS_CREDIT(3721, CostType.TAKE.no,null, null, null, null, null), // 감사 보너스 크레딧
    LUCKY_BONUS_CREDIT(3722, CostType.TAKE.no,null, null, null, null, null),
    FREE_CREDIT(3723, CostType.TAKE.no,null, null, null, null, null), // 무료 크레딧
    THANKS_CREDIT(3724, CostType.TAKE.no,null, null, null, null, null), // 감사 크레딧
    FREE_POINT(3725, CostType.TAKE.no,null, null, null, null, null), // 무료 포인트
    THANKS_POINT(3726, CostType.TAKE.no,null, null, null, null, null), // 감사 포인트

    WATCH_AD(3800, CostType.TAKE.no,null, 50, null, null, null),// 광고시청

    GET_REPLY_ADOPTED(4000, CostType.TAKE.no,30, null, null, null, null),    // 채택 되었을 때 (rewawrd)
    GET_REPLY_ADOPTED_COMPLETED(CostType.TAKE.no,4020, 20, null, null, null, null),  // 채택 완료. : 질문자에게 머니백.
    GET_WARN_COMPLETED(4050, CostType.TAKE.no,20, 5000, null, null, null),  // 올바른 신고 후 :관리자가 처리 완료.


    BID_ALLOT(4300, CostType.TAKE.no,50, null, null, null, null),  // 개별보드 낙찰액 배당.

    /** PAYMENT : 지불  (중요) Cost에 기입시 반드시 마이너스 붙일 것. (표기할때는 양수가 가독력이 좋아서(유저입장) 모두 양수로 기재함.)*/
    ADD_REWARD_POST(5000, CostType.PAYMENT.no,null, 200, null, null, null),    // post에 reward 올렸을 때.
    ADD_ANONYMOUS_POST(5100, CostType.PAYMENT.no,null, 20, null, 500, null),    // post에 익명 업로드.
    ADD_ANONYMOUS_FLEA(5120, CostType.PAYMENT.no,null, 20, null, 1000, null),    // flea 익명 업로드.

    BID_ADD(6400, CostType.PAYMENT.no,null, null, null, null, null),  // 입찰
    BID_ADCREDIT_ADD(6420, CostType.PAYMENT.no,null, null, null, null, null),  // adCredit 추가.
    //    BID_ADD_BACK (6500, 100, null, null,null,null),  // 크레딧백
//    AD_BIDDING_CANCELED (1120, 100, null, null,null,null),  // 입찰 취소
    BID_FAILED(6600, CostType.PAYMENT.no,null, null, null, null, null),  // 유찰
    ERROR_MINUS(6665, CostType.PAYMENT.no,null, null, null, null, null),
    PENALTY(6666, CostType.PAYMENT.no,null, null, null, null, null),

    PREMIUM_BOARD(7140, CostType.PAYMENT.no,null, 2500, null, 2500, null),  // 프리미엄 보드 등록.


    /************************************************************************************************************************************************
     ************************************************************************************************************************************************
     * ACT : 활동 (포인트 및 크레딧 변동 트리거 없음: 차후에 어드민에 의해서 pnt만 올라감.)
     * 중요 : CostType.ACT는 반드시 pnt만 기재할 것. 다른 것들을 쓰려면 CostType 변경 필수.
     * 여기 정렬된 no의 순서는 중요하지 않다.
     *
     * (중요) peer.totalPnt는 오직 act의 pnt로 변동됨.
     ************************************************************************************************************************************************
     ************************************************************************************************************************************************/


    ADD_REPLY(4100, CostType.ACT.no,null, 40, null, null, null),
    ADD_WARN(4120, CostType.ACT.no,null, 200, null, null, null),

    ADD_BOARD(7120, CostType.ACT.no,null, 120, null, null, null),  // 보드 생성.
    ADD_POST(7230, CostType.ACT.no,null, 120, null, null, null),    // 포스트 올리기.
    ADD_FLEA(7240, CostType.ACT.no,null, 120, null, null, null),    // 중고 올리기.
    ADD_ROOM(7360, CostType.ACT.no,null, 120, null, null, null),    // 방 올리기.
    ADD_STORE(7380, CostType.ACT.no,null, 1200, null, null, null),  // 방 올리기.



    LOGIN(10200, CostType.ACT.no,null, null, null, null, null),            // 오픈 어플 : when opened MainActivity
    RE_LOGIN(10220, CostType.ACT.no,null, null, null, null, null),  // 재로그인: 이미 가입자가 어플 새로 로그인시 from SignActivity
    SIGN_OUT(10230, CostType.ACT.no,null, null, null, null, null),
    SIGN_OUT_DUPLICATE(10235, CostType.ACT.no,null, null, null, null, null),
    LEAVE(10250, CostType.ACT.no,null, null, null, null, null),      // 탈퇴

    OPEN_MODEL(10300, CostType.ACT.no,null, null, null, null, null),
    OPEN_BOARD(10400, CostType.ACT.no,null, null, null, null, null),
    OPEN_POST(10500, CostType.ACT.no,null, 1, null, null, null),
    OPEN_FLEA(10600, CostType.ACT.no,null, 1, null, null, null),
    OPEN_ROOM(10700, CostType.ACT.no,null, 1, null, null, null),
    OPEN_STORE(10800, CostType.ACT.no,null, 1, null, null, null),

}

/**
 * String 하나로 만들기.
 */
fun getCostPriceString() {
    lllogD(
            StringBuffer().apply {
                CostPrice.values().forEach {
                    lllogD("TTTestActivity $it costNo ${it.costNo}")
                    if (this.isEmpty()) append("${it.no}") else append("$balanceDiv1111${it.no}")
//                    if (this.isEmpty()) append("${it.costNo}") else append("$balanceDiv2${CostType.ACT.no}")
                      append("$balanceDiv2${it.costNo}")
                    if (it.rate == null) append(balanceDiv2 + "n") else append("$balanceDiv2${it.rate}")
                    if (it.pnt == null) append(balanceDiv2 + "n") else append("$balanceDiv2${it.pnt}")
                    if (it.bcrd == null) append(balanceDiv2 + "n") else append("$balanceDiv2${it.bcrd}")
                    if (it.crd == null) append(balanceDiv2 + "n") else append("$balanceDiv2${it.crd}")
                    if (it.crt == null) append(balanceDiv2 + "n") else append("$balanceDiv2${it.crt}")
                }
            }.toString()
    )
}


