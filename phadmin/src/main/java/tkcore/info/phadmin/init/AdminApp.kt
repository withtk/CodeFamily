package tkcore.info.phadmin.init

import android.app.Application
import android.content.Context
import com.tkip.mycode.funs.common.lllogD
import com.tkip.mycode.funs.common.lllogI
import com.tkip.mycode.funs.common.showLog
import com.tkip.mycode.init.COUNTRY
import com.tkip.mycode.init.base.getDrawable
import com.tkip.mycode.init.base.getStr
import com.tkip.mycode.init.inter.OnExitInterface
import com.tkip.mycode.init.part.CodeApp
import tkcore.info.phadmin.BuildConfig
import tkcore.info.phadmin.R

/**
 * app 생성시
 *     build.gradle 작성.
 *     Firebase.Json 삽입.
 *     manifest 작성
 *     color.xml, style.xml, splash_backgroundground.xml, remote.xml
 *     App : Application() 수정사항 작성.
 *
 *
 */
class AdminApp : Application() {

    init {
        instance = this
    }

    companion object {
        private var instance: AdminApp? = null

        fun getAppContext(): AdminApp {
            return instance as AdminApp
        }

    }

    override fun onCreate() {
        super.onCreate()
        showLog = true   // 로그 스위치
        lllogI("MyAPP AdminApp onCreate")


        CodeApp(applicationContext)
        COUNTRY = "ph_"  // todo : ph_ 다른 나라 prefix.
        CodeApp.versionCode = BuildConfig.VERSION_CODE
        CodeApp.versionName = BuildConfig.VERSION_NAME
        CodeApp.clientId = R.string.default_web_client_id.getStr()
        CodeApp.icon = R.drawable.ic_logo_admin_box.getDrawable()
        CodeApp.isManageApp = true

        /*** admin App은 외부 노출 금지. ***********/
        CodeApp.esId = "elastic"
        CodeApp.esPw = "eOQ4ntCP8EX9ur1GwR7t9Jof"
        /*****************************************/

        CodeApp.interExit = object : OnExitInterface {
            override fun onContext(context: Context) {
                lllogD("AdminApp interTTTTest interTTTTest ")
//                context.startActivity(Intent(context, AdminCodeMainActivity::class.java))

                getCostPriceString()   // 가격 리스트 만들기.

            }
        }


    }


}
