package tkcore.info.phadmin

import com.tkip.mycode.my_test.sample.assignment.BOX
import com.tkip.mycode.my_test.sample.assignment.startLoop
import org.junit.Test
import java.util.*
import kotlin.collections.ArrayList

/**
 * Example local unit test, which will execute on the development machine (host).
 *
 * See [testing documentation](http://d.android.com/tools/testing).
 */
class ExxxxMyTest {
    @Test
    fun addition_isCorrect() {

        println("start ====================")

//        getCostPriceString()

//        val list = llist(START)
//        createBox(list)
//        println("boxInit ====================")

//        boxInit(llist(START), null)
//        openQueue.offer(BOX("654870123", 10, 0, 200, null))
//        openQueue.offer(BOX("123654870", 50, 0, 200, null))
//        openQueue.offer(BOX("870123654", 20, 0, 200, null))
       startLoop()
//        START2.startLoop()
//        priorityQueueTest()
//        breakTest()
//        sortTest()


        println("end ====================")

    }
}

fun priorityQueueTest() {

//    val list = arrayListOf<BOX>().apply {
//        add(BOX("first1", 13, 100, 100, "parent"))
//     add(BOX("first2", 2, 100, 100, "parent"))
//     add(BOX("first3", 233, 100, 100, "parent"))
//     add(BOX("first4", 100, 100, 100, "parent"))
//    }
//
//
//    list.toTypedArray().sortBy {
//        return@sortBy 1
//    }

//    val queue = PriorityQueue<BOX>(list.size, kotlin.Comparator { o1, o2 ->
//        return@Comparator if(o1.f >= o2.f) 1 else -1
//    })

    val queue = PriorityQueue<BOX>()
    queue.offer(BOX("first1", 13, 100, 100, "parent"))
    queue.offer(BOX("first2", 2, 100, 100, "parent"))
    queue.offer(BOX("first3", 233, 100, 100, "parent"))
    queue.offer(BOX("first4", 100, 100, 100, "parent"))


    val sfdds = queue.poll()

    queue.offer(BOX("first5", 7, 100, 100, "parent"))

    queue.forEach {
        println("queue $it")
    }
    println("queue poll $sfdds")

//    val newQ = PriorityQueue<BOX>(queue.size, kotlin.Comparator { o1, o2 ->
//        return@Comparator if(o1.f >= o2.f) 1 else -1
//    })
//
//    val sfdds = newQ.poll()
////    val re = queue.remove()
//
//    newQ.forEach {
//        println("queue $it")
//    }
//    println("queue poll $sfdds")
//    println("queue re $re")


}

fun breakTest() {

    run loop@{
        listOf(1, 2, 3, 4, 5).forEach {
            println(it)
            if (it <= 2) {
                println("true")
            } else {
                println("false")
                return@loop
            }
            println("the rest #####")
        }
    }



    println(" done with explicit label")


//    arrayOf(1, 2, 3, 4, 5, 6, 7, 8, 9).forEach  {
//        if (it == 3) return@forEach
//        println("first $it")
//    }
//    println("first finish")

//    for (i in 1..10) {
//        println("first $i")
//        loop@ for (j in 5..8) {
//            println("second $j")
//            if (i == j) break@loop
//        }
//    }
}

fun sortTest() {

    val list = arrayListOf<BOX>().apply {
        add(BOX("box5", 5, 13, 0, "parent"))
        add(BOX("box7", 13, 22, 0, "parent"))
        add(BOX("box6", 4, 22, 0, "parent"))
        add(BOX("box1", 1, 11, 0, "parent"))
        add(BOX("box4", 0, 22, 0, "parent"))
        add(BOX("box3", 3, 1, 0, "parent"))
    }

    val arr = list.toTypedArray()
//    arr.sortBy { data -> data.g }
//    arr.sortBy { data -> data.f }
    arr.sortWith { d1, d2 ->
        if (d1.g != d2.g) d1.g.compareTo(d2.g) else d1.f.compareTo(d2.f)
    }

    arr.forEach {
        println("end  ${it.box} f${it.f} g${it.g}")
    }


}


//val GOAL = arrayOf(1, 2, 3, 4, 5, 6, 7, 8, 0)
//val START = arrayOf(2, 4, 3, 1, 8, 5, 7, 0, 6)

private const val GOAL = "123456780"
private const val START = "243185706"

//private val START by lazy {    arrayOf(2, 4, 3, 1, 8, 5, 7, 9, 6).asList() as ArrayList<Int>}

private val closeList = arrayListOf<String>()

//private fun llist(arr: Array<Int>): ArrayList<Int> = arrayListOf<Int>().apply { arr.forEach { add(it) } }
private fun llist(noStr: String): ArrayList<Int> =
        arrayListOf<Int>().apply {
            (noStr.indices).forEach {
                add(noStr[it].toString().toInt())
            }
        }

/*********************************************************************************
 * 인접번호 리스트 <1칸, 2칸>
 * 0 1 2
 * 3 4 5
 * 6 7 8
 **********************************************************************************/
private val no0 = Pair(arrayOf(1, 3), arrayOf(Pair(1, 2), Pair(3, 6)))
private val no1 = Pair(arrayOf(0, 2), arrayOf(Pair(4, 7)))
private val no2 = Pair(arrayOf(1, 5), arrayOf(Pair(1, 0), Pair(5, 8)))

private val no3 = Pair(arrayOf(0, 4, 6), arrayOf(Pair(4, 5)))
private val no4 = Pair(arrayOf(1, 3, 5, 7), null)
private val no5 = Pair(arrayOf(2, 4, 8), arrayOf(Pair(4, 3)))

private val no6 = Pair(arrayOf(3, 7), arrayOf(Pair(3, 6), Pair(7, 8)))
private val no7 = Pair(arrayOf(4, 6, 8), arrayOf(Pair(4, 1)))
private val no8 = Pair(arrayOf(5, 7), arrayOf(Pair(5, 2), Pair(7, 6)))

fun boxInit(list: ArrayList<Int>, root: StringBuffer?) {

//    val listToMove = START.getLoc9()?.aro()
    val listToMove = list.indexOf(0).aro()

    listToMove?.first?.forEach { loc ->
        println("boxInit first   $loc")

        val newList = list.new()

//        val loc0 = newList.indexOf(0)
        newList[newList.indexOf(0)] = newList[loc]  // 변경할 번호
        newList[loc] = 0
        createBox(newList, root?.append("$loc") ?: StringBuffer("$loc"))
    }

    listToMove?.second?.forEach { loc ->
        println("boxInit second  $loc")
        val newList = list.new()

//        val loc0 = newList.indexOf(0)
        newList[newList.indexOf(0)] = newList[loc.first]  // 변경할 번호
        newList[loc.first] = 0

        newList[newList.indexOf(0)] = newList[loc.second]  // 변경할 번호
        newList[loc.second] = 0

        createBox(newList, root?.append("$loc") ?: StringBuffer("$loc"))
    }

}

private fun ArrayList<Int>.new(): ArrayList<Int> {
    val newList = arrayListOf<Int>()
    newList.addAll(this)
    return newList
}

private fun createBox(list: ArrayList<Int>, root: StringBuffer) {
    val buffer = StringBuffer().apply { list.forEach { append("$it") } }   // 일렬로
    println(buffer.toString())

    root.append()
    if (buffer.toString() == GOAL) {
        println("success $$$$$$$$$$$$$$$$$$$ $root")
    } else {
        boxInit(list, root)
    }

    println("boxInit ########\n${list[0]}${list[1]}${list[2]}\n${list[3]}${list[4]}${list[5]}\n${list[6]}${list[7]}${list[8]}")
}


//private fun createBox(box: String) {
//    println("boxInit ########\n${box.substring(0, 3)}\n${box.substring(3, 6)}\n${box.substring(6, 9)}")
//}


/** * get 1step 주위위목록 */
private fun Int.aro() =
        when (this) {
            0 -> no0
            1 -> no1
            2 -> no2
            3 -> no3
            4 -> no4
            5 -> no5
            6 -> no6
            7 -> no7
            8 -> no8
            else -> null
        }

/** * 9번 위치 가져오기. */
private fun Array<Int>.getLoc9(): Int? {
    this.forEachIndexed { index, i -> if (i == 9) return index }
    return null
}


/*********************************************************************************
 **********************************************************************************/
