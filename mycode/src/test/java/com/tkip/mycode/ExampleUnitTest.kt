package com.tkip.mycode

import com.tkip.mycode.model.cost.balance.*
import com.tkip.mycode.util.lib.elastic.elasticMapIndexMapping
import com.tkip.mycode.util.tools.date.FORMAT_SUMMARY
import com.tkip.mycode.util.tools.date.LOCALE_PH
import com.tkip.mycode.util.tools.date.getLocalTimeString
import com.tkip.mycode.util.tools.date.rightNowHour
import org.junit.Assert.assertEquals
import org.junit.Test
import java.time.Duration
import java.time.LocalTime
import java.util.*
import java.util.regex.Pattern
import kotlin.math.ceil


/**
 * Example local unit test, which will execute on the development machine (host).
 *
 * See [testing documentation](http://d.android.com/tools/testing).
 */
class ExampleUnitTest {
    @Test
    fun addition_isCorrect() {
        assertEquals(4, 2 + 2)

        println("start ====================")


//        getCostPriceString()     // price 리스트
//        getCostPriceString1()     // price 리스트


//        dataTest()
//        actTypeList()
//        indexOfindexOf()
//        bonusPercent()
//        getIndexfromEs()
//        testIsChanged()
//        chunkedTest()
//        pushTimeTest()
//        test4()

//        test3()

//        noPushTime(20,7,23)
//        noPushTime(20,7,17)
//        noPushTime(20,7,2)
//        noPushTime(20,7,8)
//        noPushTime(7,20,23)
//        noPushTime(7,20,17)
//        noPushTime(7,20,2)
//        noPushTime(7,7,23)
//        noPushTime(7,7,17)
//        noPushTime(7,7,2)
//        test2()
//        test1()

        println("end =============mycode=======")
    }

    data class ABC(
            var key: String = "str",
            var no: Int = 0
    )

    private fun dataTest() {
        val test = "200^2^n^n^n^n--500^2^n^n^n^n--600^2^n^n^n^n--700^2^n^n^n^n--800^2^n^n^n^n--900^n^n^n^n^n--1000^n^n^n^n^n--2000^n^n^n^n^n--3000^5000^5000^n^n^n--3500^n^n^n^n^n--3600^n^n^n^n^n--3700^n^n^n^n^n--3720^n^n^n^n^n--3721^n^n^n^n^n--3723^n^n^n^n^n--3724^n^n^n^n^n--3725^n^n^n^n^n--3726^n^n^n^n^n--3800^n^50^n^n^n--4000^30^n^n^n^n--4020^20^n^n^n^n--4050^20^5000^n^n^n--4100^40^n^n^n^n--4110^n^150^n^n^n--4300^50^n^n^n^n--5000^n^200^n^n^n--5100^n^200^n^500^n--5120^n^200^n^1000^n--6400^n^n^n^n^n--6420^n^n^n^n^n--6600^n^n^n^n^n--7120^n^1000^n^n^n--7140^n^1000^n^n^n--7230^n^200^n^n^n--7240^n^120^n^n^n--7360^n^120^n^n^n--7380^n^120^n^n^n--10200^n^n^n^n^n--10220^n^n^n^n^n--10230^n^n^n^n^n--10235^n^n^n^n^n--10250^n^n^n^n^n"
        test.split(balanceDiv1111)
                .forEach { act ->
                    val arr = act.split(balanceDiv2)    // 한 개의 actType String
                    Balance().apply {
                        arr.forEachIndexed { i, s ->
                            when (i) {
                                0 -> no = checkAddInt(s) ?: 0
                                1 -> rate = checkAddInt(s)
                                2 -> pnt = checkAddInt(s)
                                3 -> bcrd = checkAddInt(s)
                                4 -> crd = checkAddInt(s)
                                5 -> crt = checkAddInt(s)
                            }
                        }
                        BALANCE_MAP[no] = this
                    }
                }

        BALANCE_MAP.forEach {
            println("balance : $it ")
        }

    }

    private fun actTypeList() {
        val tttt = "200^2^null^null^null^null--500^2^null^null^null^null--600^2^null^null^null^null--700^2^null^null^null^null--800^2^null^null^null^null--900^null^null^null^null^null--1000^null^null^null^null^null--2000^null^null^null^null^null--3000^null^5000^5000^null^null--3500^null^null^null^null^null--3600^5^null^null^null^null--3700^null^null^30000^null^null--3720^null^null^null^null^null--3721^null^null^null^null^null--3723^null^null^null^null^null--3724^null^null^null^null^null--3725^null^null^null^null^null--3726^null^null^null^null^null--3800^null^0^50^null^null--4000^30^null^null^null^null--4020^20^null^null^null^null--4050^20^5000^null^null^null--4100^null^40^null^null^null--4110^null^150^null^null^null--4300^50^null^null^null^null--5000^null^200^null^null^null--5100^null^200^null^400^null--5120^null^200^null^1000^null--6400^null^null^null^null^null--6420^null^null^null^null^null--6600^null^null^null^null^null--7120^null^1100^null^0^null--7140^null^1000^null^0^null--7230^null^200^null^2220^null--7240^null^120^null^0^null--7360^null^120^null^0^null--7380^null^120^null^0^null--10200^null^null^null^null^null--10220^null^null^null^null^null--10230^null^null^null^null^null--10235^null^null^null^null^null--10250^null^null^null^null^null--200^2^n^n^n^n--500^2^n^n^n^n--600^2^n^n^n^n--700^2^n^n^n^n--800^2^n^n^n^n--900^n^n^n^n^n--1000^n^n^n^n^n--2000^n^n^n^n^n--3000^n^5000^5000^n^n--3500^n^n^n^n^n--3600^5^n^n^n^n--3700^n^n^30000^n^n--3720^n^n^n^n^n--3721^n^n^n^n^n--3723^n^n^n^n^n--3724^n^n^n^n^n--3725^n^n^n^n^n--3726^n^n^n^n^n--3800^n^0^50^n^n--4000^30^n^n^n^n--4020^20^n^n^n^n--4050^20^5000^n^n^n--4100^n^40^n^n^n--4110^n^150^n^n^n--4300^50^n^n^n^n--5000^n^200^n^n^n--5100^n^200^n^500^n--5120^n^200^n^1000^n--6400^n^n^n^n^n--6420^n^n^n^n^n--6600^n^n^n^n^n--7120^n^1100^n^0^n--7140^n^1000^n^0^n--7230^n^200^n^1700^n--7240^n^120^n^0^n--7360^n^120^n^0^n--7380^n^120^n^0^n--10200^n^n^n^n^n--10220^n^n^n^n^n--10230^n^n^n^n^n--10235^n^n^n^n^n--10250^n^n^n^n^n"
    }

    private fun indexOfindexOf() {
        val test = "200^2^null^null^null^null--500^2^null^null^null^null--600^2^null^null^null^null--700^2^null^null^null^null--800^2^null^null^null^null--900^null^null^null^null^null--1000^null^null^null^null^null--2000^null^null^null^null^null--3000^null^5000^5000^null^null--3500^null^null^null^null^null--3600^5^null^null^null^null--3700^null^null^30000^null^null--3720^null^null^null^null^null--3721^null^null^null^null^null--3723^null^null^null^null^null--3724^null^null^null^null^null--3725^null^null^null^null^null--3726^null^null^null^null^null--3800^null^0^50^null^null--4000^30^null^null^null^null--4020^20^null^null^null^null--4050^20^5000^null^null^null--4100^null^40^null^null^null--4110^null^150^null^null^null--4300^50^null^null^null^null--5000^null^200^null^null^null--5100^null^200^null^500^null--5120^null^200^null^1000^null--6400^null^null^null^null^null--6420^null^null^null^null^null--6600^null^null^null^null^null--7120^null^1100^null^0^null--7140^null^1000^null^0^null--7230^null^200^null^2220^null--7240^null^120^null^0^null--7360^null^120^null^0^null--7380^null^120^null^0^null--10200^null^null^null^null^null--10220^null^null^null^null^null--10230^null^null^null^null^null--10235^null^null^null^null^null--10250^null^null^null^null^null--200^2^n^n^n^n--500^2^n^n^n^n--600^2^n^n^n^n--700^2^n^n^n^n--800^2^n^n^n^n--900^n^n^n^n^n--1000^n^n^n^n^n--2000^n^n^n^n^n--3000^n^5000^5000^n^n--3500^n^n^n^n^n--3600^5^n^n^n^n--3700^n^n^30000^n^n--3720^n^n^n^n^n--3721^n^n^n^n^n--3723^n^n^n^n^n--3724^n^n^n^n^n--3725^n^n^n^n^n--3726^n^n^n^n^n--3800^n^0^50^n^n--4000^30^n^n^n^n--4020^20^n^n^n^n--4050^20^5000^n^n^n--4100^n^40^n^n^n--4110^n^150^n^n^n--4300^50^n^n^n^n--5000^n^200^n^n^n--5100^n^200^n^500^n--5120^n^200^n^1000^n--6400^n^n^n^n^n--6420^n^n^n^n^n--6600^n^n^n^n^n--7120^n^1100^n^0^n--7140^n^1000^n^0^n--7230^n^200^n^1700^n--7240^n^120^n^0^n--7360^n^120^n^0^n--7380^n^120^n^0^n--10200^n^n^n^n^n--10220^n^n^n^n^n--10230^n^n^n^n^n--10235^n^n^n^n^n--10250^n^n^n^n^n"
        test.split("--").forEach { act ->
            val arr = act.split("^")    // 한 개의 actType String
            Balance().apply {
                arr.forEachIndexed { i, s ->
                    when (i) {
                        0 -> no = checkAddInt(s) ?: 0
                        1 -> rate = checkAddInt(s)
                        2 -> pnt = checkAddInt(s)
                        3 -> bcrd = checkAddInt(s)
                        4 -> crd = checkAddInt(s)
                        5 -> crt = checkAddInt(s)
                    }
                }
                BALANCE_MAP[no] = this
            }
        }
        BALANCE_MAP.forEach { (k, v) ->
            println("$k $v")
        }
    }


    private fun String?.isNumeric(): Boolean {
        if (this == null) return false
        try {
            this.toDouble()
        } catch (nfe: NumberFormatException) {
            return false
        }
        return true
    }

    private fun bonusPercent() {

        val a = 35.toDouble()
        val rate = (a / 100)
//        val cal = 1254 * rate
        val cal = 438.112
        val extra = ceil(cal)
        val extra2 = ceil(cal).toInt()
        println("re======== rate:$rate cal:$cal extra:$extra extra2:$extra2")

    }

    private fun getIndexfromEs() {
        elasticMapIndexMapping.forEach { k, v ->
            println("k: $k")
        }
    }

    private fun chunkedTest() {
        val list = arrayListOf<String>()
        list.add("abc")
        list.add("d")
        list.add("3")
        list.add("f")

        val list2 = list.chunked(3)
        list2.forEach { li ->
            li.forEach { str ->
                println(str)
            }
        }

    }

    private fun pushTimeTest() {
        /**  방해 금지 시간이면 true */
        pushTimeSetTime(rightNowHour)
        pushTimeSetTime(0)
        pushTimeSetTime(23)
        pushTimeSetTime(8)
        pushTimeSetTime(18)
    }

    private fun pushTimeSetTime(now: Int) {
        val t1 = 0
        val t2 = 23
        var str = "str"
        val result =
                when {
                    (t1 < t2) -> {
                        str = "0000"
                        (now in t1 until t2)
                    }
                    (t1 > t2) ->
                        when {
                            (now in t1 until 24) -> {
                                str = "1111"
                                true
                            }
                            (now < t2) -> {
                                str = "2222"
                                true
                            }
                            else -> {
                                str = "3333"
                                false
                            }
                        }
                    else -> {
                        str = "same"
                        true
                    } // 같을때
                }
        println(" =======   $result  $now  $str   ")
    }


    private fun testIsChanged() {

        val map = mutableMapOf<String, Any?>().apply {
            checkUpdate(null, "test1", "str")
            checkUpdate(null, 123, "int")
            checkUpdate(null, 123L, "long")
            checkUpdate(null, 77.88f, "float")
            checkUpdate(null, 77.88, "double")
            checkUpdate(null, true, "boolean")
            println(" =======   ")

            checkUpdate("test1", "test1", "str2****")
            checkUpdate(123, 123, "int2****")
            checkUpdate(123L, 123L, "long2****")
            checkUpdate(77.88f, 77.88f, "float2****")
            checkUpdate(77.88, 77.88, "double2****")
            checkUpdate(true, true, "boolean2****")
            println(" =======  ")

            checkUpdate("abc", "test1", "str3----")
            checkUpdate(122343, 123, "int3----")
            checkUpdate(111L, 123L, "long3----")
            checkUpdate(11.88f, 77.88f, "float3----")
            checkUpdate(11.88, 77.88, "double3----")
            checkUpdate(false, true, "boolean3----")
            println(" =======  ")

            checkUpdate("abc", null, "str4")
            checkUpdate(122343, null, "int4")
            checkUpdate(111L, null, "long4")
            checkUpdate(11.88f, null, "float4")
            checkUpdate(11.88, null, "double4")
            checkUpdate(false, null, "boolean4")

        }

        map.forEach { (a, b) ->
            println("== [key]$a [Value]$b ")
        }
        println("size : ${map.size}\n")

    }

    private fun Any?.isChanged(new: Any?): Boolean {
        val result =
                when {
                    this == null -> new != null
                    this is String -> this != (new as? String)
                    this is Int -> this != (new as? Int)
                    this is Long -> this != (new as? Long)
                    this is Float -> this != (new as? Float)
                    this is Double -> this != (new as? Double)
                    this is Boolean -> this != (new as? Boolean ?: false)
                    else -> false
                }
        println("HomeFrag isChanged @@@@@   $this | $new  =  $result")
        return result
    }

    private fun MutableMap<String, Any?>.checkUpdate(a1: Any?, b1: Any?, child: String) {
        if (a1.isChanged(b1)) {
        }
        put(child, b1)
    }

    private fun test3() {

//        val cal = Calendar.getInstance()
////        cal.set(Calendar.HOUR, 24)
//        cal.set(Calendar.MINUTE, 0)
//        cal.set(Calendar.SECOND, 0)
//        cal.set(Calendar.HOUR_OF_DAY, 0)

//        val time12 = cal.timeInMillis
//        println("timetest ================================")
//        println("timetest time12 $time12")
//        println("timetest time12 ${time12.getLocalTimeString(FORMAT_SUMMARY)}")

//        val rightnow1 = rightNow()
//        println("timetest rightnow $rightnow")
//        println("timetest rightnow getLocalTimeString ${rightnow.getLocalTimeString(FORMAT_SUMMARY)}")
//        println("timetest rightnow getKoreanTimeString ${rightnow.getKoreanTimeString(FORMAT_SUMMARY)}")

        val dur = Duration.between(LocalTime.of(10, 0), LocalTime.of(23, 20))
        println("timetest dur $dur")

//        val zoneId = ZoneId.of("Asia/Manila")
//        val date = LocalDateTime.now()
//        val zonedDateTime = date.atZone(zoneId)
//
//        println(date.format(DateTimeFormatter.ISO_DATE_TIME))
//        println(zonedDateTime.format(DateTimeFormatter.ISO_DATE_TIME))
//        println(zonedDateTime.format(DateTimeFormatter.ISO_INSTANT))

        println("timetest ================================")
        val timezone = TimeZone.getTimeZone(LOCALE_PH)
        val cal2 = java.util.Calendar.getInstance()
        cal2.timeZone = timezone
        println("timetest getLocaleLong ${cal2.get(java.util.Calendar.DATE)}  ${cal2.get(java.util.Calendar.HOUR_OF_DAY)} ${cal2.get(java.util.Calendar.MINUTE)} ${cal2.get(java.util.Calendar.SECOND)}")

        val phLong = cal2.timeInMillis
        val korean = phLong.getLocalTimeString(FORMAT_SUMMARY)
        println("timetest korean $korean")

        cal2.set(java.util.Calendar.MINUTE, 0)
        cal2.set(java.util.Calendar.SECOND, 0)
        cal2.set(java.util.Calendar.HOUR_OF_DAY, 0)
        println("timetest getLocaleLong ${cal2.get(java.util.Calendar.DATE)}  ${cal2.get(java.util.Calendar.HOUR_OF_DAY)} ${cal2.get(java.util.Calendar.MINUTE)} ${cal2.get(java.util.Calendar.SECOND)}")

        val ph12Long = cal2.timeInMillis
        val korean12 = ph12Long.getLocalTimeString(FORMAT_SUMMARY)
        println("timetest korean12 $korean12")


//        val nowLong = rightNowPHhour()
        val minus = phLong - ph12Long
        println("timetest phLong $phLong ph12Long $ph12Long")
        println("timetest minus $minus")
        println("timetest minus ${minus / 1000 / 60 / 60}")


    }

    fun getLocaleLong(locale: String, hourOfDay: Int?): Long {
        val timezone = TimeZone.getTimeZone(locale)
        val cal2 = java.util.Calendar.getInstance()
        cal2.timeZone = timezone
        hourOfDay?.let {
            cal2.set(java.util.Calendar.MINUTE, 0)
            cal2.set(java.util.Calendar.SECOND, 0)
            cal2.set(java.util.Calendar.HOUR_OF_DAY, 0)
        }
        println("timetest getLocaleLong ${cal2.get(java.util.Calendar.HOUR_OF_DAY)} ${cal2.get(java.util.Calendar.MINUTE)} ${cal2.get(java.util.Calendar.SECOND)}")
        return cal2.timeInMillis
    }

    private fun noPushTime(time1: Int, time2: Int, nowHour: Int) {
        val result = if (time1 > time2) nowHour > time1 || nowHour < time2
        else nowHour in (time1 + 1) until time2
        val text = if (result) "금지시간" else "푸시"
        println("$time1 ~ $time2 current $nowHour :$text===")
    }

//    private fun test2() {
//        val str = "동해물과    백두산이     닳도록  ! 하나님이 *jS{}fdk!j???dsjf@kjgk#f ...djflkdsj^fjsjfd&jfl  보우하사 우리나라 만세  ← ← "
//        val removeStr = str.removeSpecial().removeLongSpace().trim()
//        println("str removeStr : $removeStr")
//
//        val array = removeStr.split(" ").toTypedArray()
//        array.forEach { println("array : $it") }
//    }


    // Pattern for recognizing a URL, based off RFC 3986
    private val urlPattern = Pattern.compile(
            "(?:^|[\\W])((ht|f)tp(s?):\\/\\/|www\\.)"
                    + "(([\\w\\-]+\\.){1,}?([\\w\\-.~]+\\/?)*"
                    + "[\\p{Alnum}.,%_=?&#\\-+()\\[\\]\\*$~@!:/{};']*)",
            Pattern.CASE_INSENSITIVE or Pattern.MULTILINE or Pattern.DOTALL)

    private fun test1() {

        val testUrl = "foo bar http://example.com baz"
        val matcher = urlPattern.matcher(testUrl)
        while (matcher.find()) {
            val matchStart: Int = matcher.start(1)
            val matchEnd: Int = matcher.end()
            println("matchStart : $matchStart  matchEnd : $matchEnd")
            val result = testUrl.substring(matchStart, matchEnd)
            println("result : $result  ")
            // now you have the offsets of a URL match
        }

//        val regex = "^(https?):\\/\\/([^:\\/\\s]+)(:([^\\/]*))?((\\/[^\\s/\\/]+)*)?\\/?([^#\\s\\?]*)(\\?([^#\\s]*))?(#(\\w*))?$"
//
//        val p = Pattern.compile(regex)
//
//        println(p.matcher("absdf sdfsdf https://goodidea.tistory.com:8888/qr/aaa/ddd.html?abc=def&ddd=fgf#sharp").matches())
//        println(p.matcher("http://dextto.tistory.com").matches())
//        println(p.matcher("http://blog.daum.net/dexter").matches())
//        println(p.matcher("http://www.daum.net:80/index.cfm").matches())
//        println(p.matcher("http://xxx:password@www.daum.net").matches())
//        println(p.matcher("http://localhost/index.php?ab=1&c=2").matches())
//        println(p.matcher("http://localhost:8080").matches())
//        println(p.matcher("http://dextto.tistory.com/admin/entry/post/?id=150&returnURL=/entry/JAVA-Regular-Expression-%EC%A0%95%EA%B7%9C-%ED%91%9C%ED%98%84%EC%8B%9D-%EC%98%88%EC%A0%9C").matches())


//
//        val link = "http://www.naver.com"
//        val link2 = "https://news.naver.com/main/read.nhn?mode=LSD&mid=sec&sid1=102&oid=003&aid=0009854781"
//        println("homefrag link link2 $link2")
//        val fsdsd = link2.matches("^(http|https|ftp)://.*$")
//
//        val p = Pattern.compile("http://(.*?)\"")
//        val m = p.matcher(link2)
//        var url2: String? = null
//        if (m.find()) url2 = m.group(1) // this will give you the URL
//        println("homefrag link url2 $url2")
    }

    fun test() {
        println("##################start###################")
        val aaa = 10
        val bbb = 20
        val ccc = 20

        val ddd = aaa.or(bbb)
        if (ccc == aaa.or(bbb)) println("true")
        else println("false")

        println("ddd $ddd")
    }
}
