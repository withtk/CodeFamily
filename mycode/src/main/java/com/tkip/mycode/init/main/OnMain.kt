package com.tkip.mycode.init.main

import android.content.Context
import android.content.Intent
import com.tkip.mycode.init.MainActivity

class OnMain {
    companion object {

        fun gotoMainActivity(context: Context) {
            val intent = Intent(context, MainActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
            context.startActivity(intent)
        }

    }
}