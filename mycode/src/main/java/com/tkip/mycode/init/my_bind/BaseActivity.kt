package com.tkip.mycode.init.my_bind

import android.content.pm.ActivityInfo
import android.os.Bundle
import android.os.PersistableBundle
import androidx.appcompat.app.AppCompatActivity
import com.tkip.mycode.dialog.progress.MyCircle
import com.tkip.mycode.funs.common.lllogM
import com.tkip.mycode.funs.common.showLog
import com.tkip.mycode.init.part.CodeApp

open class BaseActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?, persistentState: PersistableBundle?) {
        super.onCreate(savedInstanceState, persistentState)
        if (showLog) lllogM(" ${this.localClassName} : onCreate")
        this.requestedOrientation = ActivityInfo.SCREEN_ORIENTATION_PORTRAIT   // orientation portrate 새로방향 고정.
    }


    override fun onResume() {
//         lllogM(" ${this.localClassName} : onResume")
        super.onResume()
        CodeApp.curActivity = this
//        checkNotice()
    }


    override fun onPause() {
//        lllogM(" ${this.localClassName} : onPause")
        super.onPause()
    }


    override fun onDestroy() {
//        lllogM(" ${this.localClassName} : onDestroy")
        super.onDestroy()

        /***  초기화 : 사실상 들어올 일이 없다. 이미 onResume에서 새로운 context로 바꿨기 때문에. */
        if (CodeApp.curActivity?.equals(this) == true) CodeApp.curActivity = null

        MyCircle.cancel()
    }


}