package com.tkip.mycode.init.base

import android.view.View

class Cons {


    companion object {

        const val TAG = "ttttest"

        const val PUT_OBJECT = "PUT_OBJECT"
        const val PUT_PLACE = "PUT_PLACE"
        const val PUT_INT = "PUT_INT"
        const val PUT_STRING = "PUT_STRING"
        const val PUT_STRING2 = "PUT_STRING2"
        const val PUT_DELETE = "PUT_DELETE"
        const val PUT_ARRAY_STRING = "PUT_ARRAY_STRING"
        const val PUT_ARRAY_MYPHOTO = "PUT_ARRAY_MYPHOTO"
        const val PUT_ARRAY_FOTO = "PUT_ARRAY_FOTO"
        const val PUT_TYPE = "PUT_TYPE"
        const val PUT_POST_TYPE_NO = "PUT_POST_TYPE_NO"
        const val PUT_LATLNG = "PUT_LATLNG"
        const val PUT_TITLE = "PUT_TITLE"
        const val PUT_DESCRIPTION = "PUT_DESCRIPTION"
        const val PUT_IS_CANCEL = "PUT_IS_CANCEL"
        const val PUT_IS_CANCEL_TOUCH = "PUT_IS_CANCEL_TOUCH"



        const val ELASTIC_MAIN_URL = "http://34.87.72.110//elasticsearch/"
        const val RET_SUB = "test_p.php"
        const val RET_SEARCH = "_search"


        /********** activity actionType ******* int 필수 : 차후 증감을 함께 아우를 수 있다.*************/
        const val ACTION_TYPE_NEW = 100
        const val ACTION_TYPE_UPDATE = 200
        const val ACTION_TYPE_INSPECTION = 400    // admin이 검사할때
        //        const val ACTION_TYPE_PREVIEW = 500     // 미리보기
        const val ACTION_TYPE_NORMAL = 600


        const val VIEW_TYPE_STATUS_BAR = 100       // storemenu statusNo
        const val VIEW_TYPE_STATUS_TITLE = 150       // storemenu statusNo small
        const val VIEW_TYPE_BTN_DEL = 170       // storemenu btn del
        const val VIEW_TYPE_BTN_MOVE = 180       // storemenu btn move
        const val VIEW_TYPE_BTN_SUBMIT = 191
        const val VIEW_TYPE_BTN_APPROVAL = 192
        const val VIEW_TYPE_CONTENTS = 200     // storemenu contents


        const val VIEW_REPLY_USER = 0
        const val VIEW_REPLY_MY = 1


//        /*********** 사진 업로드 *******************/
//        var LIMIT_UPLOAD_COUNT = 10
//        var WIDTH_THUMB = 400
//        var WIDTH_MID = 1280
//        var WIDTH_ORIGINAL = 10000   // 9000 이상이면 원본사이즈 업로드.
//        todo : 원본사이즈는 차후에. 지금은 사이즈를 최대치 1920으로 수정할 것. (portrate랑 landscape에따라 변경될 사이즈 차이남.)


        /********** DB 업로드 스위치 ********************/
        var SW_DEVICE = true


        fun pair(v: View, transitionName: String) = androidx.core.util.Pair(v, transitionName)


        /********** 유저 등급 및 타입 분류********************/

        /*
        var PEER_PENALTY_100 =PeerType.BLOCK_100.no
         var PEER_PENALTY_200 =PeerType.BLOCK_200.no
         var PEER_PENALTY_300 =PeerType.BLOCK_300.no

         var PEER_BASIC_1000 =PeerType.BASIC_1000.no  // 기본, 익명로그인
         var PEER_BASIC_2000 =PeerType.BASIC_2000.no  // 구글로그인
         var PEER_BASIC_3000 =PeerType.BASIC_3000.no

         var PEER_VIP_5000 =PeerType.VIP_5000.no
         var PEER_VIP_6000 =PeerType.VIP_6000.no
         var PEER_VIP_7000 =PeerType.VIP_7000.no

         var PEER_ADMIN_9100 =PeerType.ADMIN_9100.no
         var PEER_ADMIN_9300 =PeerType.ADMIN_9300.no
         var PEER_ADMIN_SUPERVISOR =PeerType.ADMIN_SUPERVISOR.no


         var POST_TYPE_FREE = PostType.FREE.no
         var POST_TYPE_QNA = PostType.QNA.no
         var POST_TYPE_PROMO = PostType.PROMO.no
         var POST_TYPE_FLEA = PostType.FLEA.no
         var POST_TYPE_POST = PostType.ROOM.no
         var POST_TYPE_STORE = PostType.STORE.no

         var STATUS_DELETED = Status.DELETED.no     // 삭제
         var STATUS_BLOCKED = Status.BLIND.no        // 차단중
         var STATUS_DENIED = Status.DENIED.no         // 승인 거절됨
         var STATUS_WAITING = Status.WAITING.no        // 대기
         var STATUS_SCREENING = Status.SCREENING.no      // 문제 발생하여 검열중

         var STATUS_INSPECTING = Status.INSPECTING.no     // 검사 대기 중
         var STATUS_WRITING = Status.WRITING.no     // 검사 대기 중

         var STATUS_NORMAL_1000 = Status.NORMAL_1000.no        // 정상
         var STATUS_NORMAL_2000 = Status.NORMAL_2000.no        // 정상
 */

    }
}