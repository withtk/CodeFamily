package com.tkip.mycode.init

import com.tkip.mycode.R
import com.tkip.mycode.init.base.getInt

/*******************************************************************
 * 숫자 한계점     **************************************************
 *******************************************************************/
val MIN_BOARD_ID by lazy { R.integer.count_boardid_min.getInt() }

val QUICK_MENU_LIMIT by lazy { R.integer.count_quick_menu_max.getInt() }
var MY_QUICK_COUNT = 0
