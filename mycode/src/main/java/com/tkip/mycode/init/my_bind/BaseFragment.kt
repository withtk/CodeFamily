package com.tkip.mycode.init.my_bind

import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment

abstract class BaseFragment : Fragment() {

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
//        EventBus.getInstance().register(this)
    }

    override fun onDestroyView() {
//        EventBus.getInstance().unregister(this)
        super.onDestroyView()
    }


}