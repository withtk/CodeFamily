package com.tkip.mycode.init.my_bind

import android.app.Activity
import android.content.Intent
import android.content.pm.PackageManager
import android.database.Cursor
import android.net.Uri
import android.os.Bundle
import android.provider.OpenableColumns
import android.view.MenuItem
import android.view.View
import androidx.annotation.LayoutRes
import androidx.core.widget.NestedScrollView
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import com.google.android.gms.maps.model.LatLng
import com.tkip.mycode.ad.ad_show.CustomBannerSlide
import com.tkip.mycode.funs.common.lllogD
import com.tkip.mycode.funs.common.lllogI
import com.tkip.mycode.funs.common.lllogM
import com.tkip.mycode.funs.common.showLog
import com.tkip.mycode.funs.custom.LinearMapAddBox
import com.tkip.mycode.funs.custom.LinearMapBox
import com.tkip.mycode.init.*
import com.tkip.mycode.init.base.Cons
import com.tkip.mycode.init.inter.OnActivityResultInterface
import com.tkip.mycode.init.inter.OnRequestPermissionsResultInterface
import com.tkip.mycode.model.mytag.LinearAddTag
import com.tkip.mycode.model.reply.LinearReplyAddBox
import com.tkip.mycode.model.reply.ReplyAdapterFireS
import com.tkip.mycode.util.lib.map.PUT_MAP_ADDRESS
import com.tkip.mycode.util.lib.map.PUT_MAP_LATLNG
import com.tkip.mycode.util.lib.map.PUT_MAP_POSTAL_CODE
import com.tkip.mycode.util.lib.map.showMap
import com.tkip.mycode.util.lib.photo.MyPhoto
import com.tkip.mycode.util.lib.photo.view.LinearAddPhotoBox
import com.tkip.mycode.util.lib.photo.view.LinearAddPhotoOne
import com.tkip.mycode.util.tools.anim.hideSlideRight

/**
 * Activity : add Photo  등..
 */
abstract class BindingActivityPhoto<T : ViewDataBinding> : BaseActivity() {
    @LayoutRes
    abstract fun getLayoutResId(): Int

    protected lateinit var b: T
        private set

    var banner: CustomBannerSlide? = null

    var replyAdapter: ReplyAdapterFireS? = null
    lateinit var linearReplyAddBox: LinearReplyAddBox
    var linearAddTag: LinearAddTag? = null    // 태그 업로드
    lateinit var linearAddTagBuilt: LinearAddTag  // 빌트인 업로드

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        lllogD("mContextTest BindingActivityPhoto onCreate")

//        this.requestedOrientation = ActivityInfo.SCREEN_ORIENTATION_PORTRAIT   // orientation portrate 새로방향 고정.
        b = DataBindingUtil.setContentView(this, getLayoutResId())
    }

    override fun onResume() {
        super.onResume()
        banner?.startSlide("BindingActivityPhoto")
    }

    override fun onPause() {
        super.onPause()
        banner?.stopSlide()
    }

    override fun onDestroy() {
        super.onDestroy()
        replyAdapter?.stopListening()
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
//          lllogM(" ${this.localClassName} : onOptionsItemSelected")

        if (item.itemId == android.R.id.home) onBackPressed()
        return super.onOptionsItemSelected(item)
    }

    var listenerActivityResult: OnActivityResultInterface? = null
    var viewAddPhotoBox: LinearAddPhotoBox? = null    // 사진 업로드
    var viewAddPhotoOne: LinearAddPhotoOne? = null    // 사진 업로드 1장

    var linearAddMap: LinearMapAddBox? = null   // 맵 업로드1
    var linearMapShow: LinearMapBox? = null    // 맵 보여주기
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
//         lllogM(" ${this.localClassName} : onActivityResult")

        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == Activity.RESULT_OK) {
            lllogI("binding listenerActivityResult requestCode $requestCode data ${data?.data}")

            when (requestCode) {
                REQ_SAF -> {
                    viewAddPhotoBox?.setFotosFromUri(requestCode, data)
                }
                REQ_PICK -> {
                    listenerActivityResult?.onPick(data?.data)
                    viewAddPhotoBox?.setFotosFromUri(requestCode, data)
                    data?.data?.let { uri -> viewAddPhotoOne?.setImageUri(uri) }
                }
                REQ_PICK_MULTI -> {
                }
                REQ_ALBUM -> {
                    listenerActivityResult?.onAlbum(data?.getParcelableArrayListExtra<MyPhoto>(Cons.PUT_ARRAY_MYPHOTO))
                    viewAddPhotoBox?.setFotosFromUri(requestCode, data)
//                    data?.getParcelableArrayListExtra<MyPhoto>(Cons.PUT_ARRAY_MYPHOTO)?.let { albumList ->
//                        viewAddPhotoBox?.selectedFotoList?.resetFromAlbum(albumList)
//                        viewAddPhotoBox?.selectedFotoAdapter?.notifyDataSetChanged()
//                        viewAddPhotoBox?.b?.tvInfo?.showSlideUp(false)
//                    }
                }
                REQ_MAP_LOCATION -> {
                    /** 주소 및 좌표값*/
                    listenerActivityResult?.onMap(data?.getStringExtra(PUT_MAP_ADDRESS), data?.getParcelableExtra<LatLng>(PUT_MAP_LATLNG))
                    val address = data?.getStringExtra(PUT_MAP_ADDRESS)
                    val postalCode = data?.getStringExtra(PUT_MAP_POSTAL_CODE)
                    val latLng = data?.getParcelableExtra<LatLng>(PUT_MAP_LATLNG)

                    /** 위치삽입 맵 1 */
                    linearAddMap?.let { addMap ->
                        addMap.address = address
                        addMap.postalCode = postalCode
                        latLng?.let {
                            linearAddMap?.b?.etAddress?.setText(address)   // 주소
                            linearAddMap?.latLng = it
                            linearAddMap?.map?.showMap(it, address, linearAddMap?.b?.lineMap)
                        }
                    }

                    /** 보여주기 맵 : addStore에서도 쓰임.*/
                    linearMapShow?.let { showMap ->
                        showMap.address = address
                        showMap.postalCode = postalCode
                        latLng?.let {
                            linearMapShow?.b?.address = address   // 주소
                            linearMapShow?.latLng = it
                            linearMapShow?.map?.showMap(it, address, linearMapShow?.b?.lineMap)
                        }
                    }
                }
                REQ_BANNER -> {
                    lllogI("BannerManageActivity REQ_BANNER")
                    /** 내 배너 가져오기 */
                    listenerActivityResult?.onBanner(data?.getParcelableExtra(PUT_BANNER))
                }

                REQ_DATA -> listenerActivityResult?.onData(data)

            }
        }
    }

    var listenerPermissionsResult: OnRequestPermissionsResultInterface? = null
    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
//          lllogM(" ${this.localClassName} : onRequestPermissionsResult")

        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        when (requestCode) {
            REQ_READ_EXSTORAGE ->
                listenerPermissionsResult?.onReadExternalStorage(grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED)
        }
    }


//    var onRestoreState: OnRestoreStateInterface? = null
//    override fun onRestoreInstanceState(save: Bundle) {
//        super.onRestoreInstanceState(save)
//        onRestoreState?.onParcel(save.getParcelable(PUT_RESULT_OBJECT))
//
//        /**         * 퍼미션 있을때에만 앨범에서 사진 가져오기.         */
//        //        //todo: 앨범의 사진이 삭제되면 문제. 그냥 모두 초기화 하는 것이 나아 보인다. 굳이 어렵게 짤 필요가...
//        save.getParcelableArrayList<Foto>(PUT_RESULT_LIST)?.let { fotolist ->
//            onRestoreState?.onFotoList(fotolist)
//            if (OnPermission.hasPermission(this@BindingBaseActivity, READ_EXTERNAL_STORAGE)) {
//                viewAddPhotoBox?.selectedFotoList = fotolist
//                viewAddPhotoBox?.selectedFotoAdapter?.notifyDataSetChanged()
//            }
//        }
//    }

//    override fun onSaveInstanceState(outState: Bundle) {
//        super.onSaveInstanceState(outState)
//        outState.putParcelable(PUT_RESULT_OBJECT, getFinalPost())
//        outState.putParcelableArrayList(PUT_RESULT_LIST, viewAddPhotoBox?.selectedFotoList)
//    }


    /**************************************************************************************
     * postActivity Only
     **************************************************************************************/
//    fun onClickBoardTitle(boardKey: String) {
//        Firee.getBoard(boardKey, success = { it?.let { board -> OnBoard.gotoBoardActivity(this@BindingBaseActivity, false, board, null) } }, fail = {})
//    }


    /**************************************************************************************
     * common Click  : postActivity, FleaActivity, RoomActivity
     **************************************************************************************/
    fun onClickScrollDown(v: View, nScroll: NestedScrollView) {
        nScroll.fullScroll(View.FOCUS_DOWN)
        v.hideSlideRight(false)
    }


    /**     * metaData출력     */
    private fun dumpImageMetaData(uri: Uri) {
        val cursor: Cursor? = contentResolver.query(uri, null, null, null, null, null)
        cursor?.use {
            if (it.moveToFirst()) {
                val displayName = it.getString(it.getColumnIndex(OpenableColumns.DISPLAY_NAME))  // 1
                val sizeIndex = it.getColumnIndex(OpenableColumns.SIZE)  // 2
                val size = if (!it.isNull(sizeIndex)) it.getString(sizeIndex) else "Unknown"
                lllogI("REQ_PICK_MULTI dumpImageMetaData Display Name $displayName")
                lllogI("REQ_PICK_MULTI dumpImageMetaData Size Size $size")
            }
        }
    }
}
