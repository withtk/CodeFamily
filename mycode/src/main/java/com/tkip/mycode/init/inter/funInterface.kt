package com.tkip.mycode.init.inter

import android.content.Context
import android.content.Intent
import android.net.Uri
import android.view.View
import com.google.android.gms.maps.model.LatLng
import com.tkip.mycode.model.bid.Bid
import com.tkip.mycode.util.lib.photo.Foto
import com.tkip.mycode.util.lib.photo.MyPhoto
import com.tkip.mycode.util.tools.etc.OnMyClick

/************************* activityResult ***********************************************/
interface OnExitInterface {
    fun onContext(context: Context)
}



/************************* activityResult ***********************************************/
interface OnActivityResultInterface {
    fun onPick(uri: Uri?)
    fun onAlbum(myPhotoList: ArrayList<MyPhoto>?)
    fun onMap(address: String?, latlng: LatLng?)
    fun onBanner(banner: Bid?)
    fun onData(data:Intent?)
}

fun interActivityResult(onPick: ((uri: Uri?) -> Unit)?, onAlbum: ((myPhotoList: ArrayList<MyPhoto>?) -> Unit)?, onMap: ((address: String?, latlng: LatLng?) -> Unit)?, onBanner: ((banner: Bid?) -> Unit)?, onData: ((data: Intent?) -> Unit)?) =
        object : OnActivityResultInterface {
            override fun onPick(uri: Uri?) {
//                lllogI("interActivityResult onPick uri  ${uri.toString()} ")
                onPick?.invoke(uri)
            }

            override fun onAlbum(myPhotoList: ArrayList<MyPhoto>?) {
//                lllogI("interActivityResult onAlbum myPhotoList size    ")
                onAlbum?.invoke(myPhotoList)
            }

            override fun onMap(address: String?, latlng: LatLng?) {
                onMap?.invoke(address, latlng)
            }

            override fun onBanner(banner: Bid?) {
                onBanner?.invoke(banner)
            }

            override fun onData(data: Intent?) {
                onData?.invoke(data)
            }
        }


/************************** permission **********************************************/
interface OnRequestPermissionsResultInterface {
    fun onReadExternalStorage(granted: Boolean)
}

fun interRequestPermissionsResult(onReadExternalStorage: (granted: Boolean) -> Unit) =
        object : OnRequestPermissionsResultInterface {
            override fun onReadExternalStorage(granted: Boolean) {
                onReadExternalStorage(granted)
            }
        }

/************************ sttBasic ************************************************/
interface OnBasicClickInterface {
    fun onClick1(v: View)
    fun onClick2(v: View)
    fun onClick3(v: View)
}

fun interBasicClick(on1: ((View) -> Unit)?, on2: ((View) -> Unit)?, on3: ((View) -> Unit)?) =
        object : OnBasicClickInterface {
            override fun onClick1(v: View) {
                on1?.invoke(v)
            }

            override fun onClick2(v: View) {
                on2?.invoke(v)
            }

            override fun onClick3(v: View) {
                on3?.invoke(v)
            }
        }

interface OnDialogBasicClickInterface {
    fun onClickDelete(v: View)
    fun onClickCancel(v: View)
    fun onClickOk(v: View)
}

fun interDialogBasicClick( onDel: ((View) -> Unit)?,onCancel: (View) -> Unit, onOk: (View) -> Unit) =
        object : OnDialogBasicClickInterface {
            override fun onClickDelete(v: View) {
                onDel?.invoke(v)
            }
            override fun onClickCancel(v: View) {
                onCancel(v)
            }
            override fun onClickOk(v: View) {
                onOk(v)
            }


        }

//interface OnDialogClickInterface {
//    fun onOkClick(store: Store)
//}
//
//fun interDialogClick(onOkClick: (Store) -> Unit) =
//        object : OnDialogClickInterface {
//            override fun onOkClick(store: Store) {
//                onOkClick(store)
//            }
//        }


interface OnHolderBasicInterface {
    fun onClickCard(v: View)
    fun onClickGo(v: View)
    fun onClickUpdate(v: View)
    fun onClickDel(v: View)
    fun onClickDeepDel(v: View)
    fun onClickStatus(v: View)
}

fun interHolderBasic(onClickCard: ((View) -> Unit)?, onClickGo: ((View) -> Unit)?, onClickUpdate: ((View) -> Unit)?, onClickDel: ((View) -> Unit)?, onDeepDel: ((View) -> Unit)?, onClickStatus: ((View) -> Unit)?) =
        object : OnHolderBasicInterface {
            override fun onClickCard(v: View) {
                onClickCard?.invoke(v)
            }

            override fun onClickGo(v: View) {
                onClickGo?.invoke(v)
            }

            override fun onClickUpdate(v: View) {
                onClickUpdate?.invoke(v)
            }

            override fun onClickDel(v: View) {
                onClickDel?.invoke(v)
            }
            override fun onClickDeepDel(v: View) {
                onDeepDel?.invoke(v)
            }

            override fun onClickStatus(v: View) {
                onClickStatus?.invoke(v)
            }
        }


/************************* RestoreState ***********************************************/
interface OnRestoreStateInterface {
    fun onParcel(any: Any?)
    fun onFotoList(fotoList: ArrayList<Foto>?)
}

fun interRestoreState(onParcel: (any: Any?) -> Unit, onFotoList: (fotoList: ArrayList<Foto>?) -> Unit) =
        object : OnRestoreStateInterface {
            override fun onParcel(any: Any?) {
                onParcel(any)
            }

            override fun onFotoList(fotoList: ArrayList<Foto>?) {
                onFotoList(fotoList)
            }
        }


/************************ sttMenu ************************************************/
interface OnMenuBoxInterface {
    fun onClickThumbUp(v: View)
    fun onClickThumbDown(v: View)
    fun onClickBookmark(v: View)
    fun onClickPush(v: View)
    fun onClickToss(v: View)
    fun onClickMore(v: View)
}

fun interMenuBox(onClickThumbUp: (v: View) -> Unit, onClickThumbDown: (v: View) -> Unit, onClickBookmark: (v: View) -> Unit, onClickPush: (v: View) -> Unit, onClickToss: (v: View) -> Unit, onClickMore: (v: View) -> Unit) =
        object : OnMenuBoxInterface {
            override fun onClickThumbUp(v: View) {
                OnMyClick.setDisableAWhileBTN(v)
                onClickThumbUp(v)
            }

            override fun onClickThumbDown(v: View) {
                OnMyClick.setDisableAWhileBTN(v)
                onClickThumbDown(v)
            }

            override fun onClickBookmark(v: View) {
                OnMyClick.setDisableAWhileBTN(v)
                onClickBookmark(v)
            }

            override fun onClickPush(v: View) {
                OnMyClick.setDisableAWhileBTN(v)
                onClickPush(v)
            }

            override fun onClickToss(v: View) {
                OnMyClick.setDisableAWhileBTN(v)
                onClickToss(v)
            }

            override fun onClickMore(v: View) {
                OnMyClick.setDisableAWhileBTN(v)
                onClickMore(v)
            }

        }

/************************ sttToolbar  ************************************************/
interface OnToolbarInterface {
    fun onClickBack()
    fun onClickBoardTitle()
    fun onClickMore()
}

fun interToolbar(onBack: () -> Unit, onBoardTitle: () -> Unit, onMore: () -> Unit) =
        object : OnToolbarInterface {
            override fun onClickBack() {
                onBack()
            }

            override fun onClickBoardTitle() {
                if (OnMyClick.isDoubleClick()) return
                onBoardTitle()
            }

            override fun onClickMore() {
                if (OnMyClick.isDoubleClick()) return
                onMore()
            }
        }


interface OnToolbarAddInterface {
    fun onClickBack()
    fun onClickBookmark(v: View)
    fun onClickPush(v: View)
    fun onClickAdd(v: View)
    fun onClickMore(v: View)
}

fun interToolbarAdd(onBack: () -> Unit, onBookmark: (View) -> Unit, onPush: (View) -> Unit, onAdd: (View) -> Unit, onMore: (View) -> Unit) =
        object : OnToolbarAddInterface {
            override fun onClickBack() {
                if (OnMyClick.isDoubleClick()) return
                onBack()
            }

            override fun onClickBookmark(v: View) {
                if (OnMyClick.isDoubleClick()) return
                onBookmark(v)
            }

            override fun onClickPush(v: View) {
                if (OnMyClick.isDoubleClick()) return
                onPush(v)
            }

            override fun onClickAdd(v: View) {
                if (OnMyClick.isDoubleClick()) return
                onAdd(v)
            }

            override fun onClickMore(v: View) {
                if (OnMyClick.isDoubleClick()) return
                onMore(v)
            }
        }


interface OnToolbarIconInterface {
    fun onClickFirstIcon(v: View)
    fun onClickTopTitle(v: View)
    fun onClickTextMenu(v: View)
    fun onClickTextMenu2(v: View)
    fun onClickIcon(v: View)
    fun onClickMore(v: View)
}

fun interToolbarIcon(onFirst: ((View) -> Unit)?, onTopTitle: ((View) -> Unit)?, onTextMenu:( (View) -> Unit)?, onTextMenu2: ((View) -> Unit)?, onIcon: ((View) -> Unit)?, onMore:( (View) -> Unit)?) =
        object : OnToolbarIconInterface {
            override fun onClickFirstIcon(v: View) {
                OnMyClick.setDisableAWhileBTN(v)
                onFirst?.invoke(v)
            }

            override fun onClickTopTitle(v: View) {
                OnMyClick.setDisableAWhileBTN(v)
                onTopTitle?.invoke(v)
            }

            override fun onClickTextMenu(v: View) {
                OnMyClick.setDisableAWhileBTN(v)
                onTextMenu?.invoke(v)
            }
            override fun onClickTextMenu2(v: View) {
                OnMyClick.setDisableAWhileBTN(v)
                onTextMenu2?.invoke(v)
            }

            override fun onClickIcon(v: View) {
                OnMyClick.setDisableAWhileBTN(v)
                onIcon?.invoke(v)
            }

            override fun onClickMore(v: View) {
                OnMyClick.setDisableAWhileBTN(v)
                onMore?.invoke(v)
            }
        }

/************************ sttRv ************************************************/
//interface OnHolderInterface {
//    fun onClickCard(v: View)
//}

//fun interHolder(onClickCard: (View) -> Unit) =
//        object : OnHolderInterface {
//            override fun onClickCard(v: View) {
//                OnMyClick.setDisableAWhileBTN(v)
//                onClickCard(v)
//            }
//
//        }

/************************  topNotice ************************************************/
interface OnTopNoticeInterface {
    fun onClickNotice(v: View)
    fun onClickNoMoreShow(v: View)
}

fun interTopNotice(onNotice: (View) -> Unit, onNoMoreShow: (View) -> Unit) =
        object : OnTopNoticeInterface {
            override fun onClickNotice(v: View) {
                OnMyClick.setDisableAWhileBTN(v)
                onNotice(v)
            }

            override fun onClickNoMoreShow(v: View) {
                OnMyClick.setDisableAWhileBTN(v)
                onNoMoreShow(v)
            }
        }

/************************  Title ************************************************/
interface OnAuthorTitleInterface {
    fun onPeerClick()
}

fun interAuthorTitle(onPeerClick: () -> Unit) =
        object : OnAuthorTitleInterface {
            override fun onPeerClick() {
                if (OnMyClick.isDoubleClick()) return
                onPeerClick()
            }
        }


/************************ sttPhoto ************************************************/
interface OnAddPhotoBoxInterface {
    fun onClickOpenAlbum()
}

fun interAddPhotoBox(onClickOpenAlbum: () -> Unit) =
        object : OnAddPhotoBoxInterface {
            override fun onClickOpenAlbum() {
                if (OnMyClick.isDoubleClick()) return
                onClickOpenAlbum()
            }
        }

interface OnAddPhotoOneInterface {
    fun onClickDelete(v: View)
    fun onClickOpenGallery(v: View)
}

fun interAddPhotoOne(onClickDelete: (View) -> Unit, onClickOpenGallery: (View) -> Unit) =
        object : OnAddPhotoOneInterface {
            override fun onClickDelete(v: View) {
                OnMyClick.setDisableAWhileBTN(v)
                onClickDelete(v)
            }

            override fun onClickOpenGallery(v: View) {
                OnMyClick.setDisableAWhileBTN(v)
                onClickOpenGallery(v)
            }
        }

/************************ sttReply ************************************************/
interface OnReplyAddBoxInterface {
    fun onClickOpenAttachMenu()
    fun onClickAddReply()
}

fun interReplyAddBox(onClickOpenAttachMenu: () -> Unit, onClickAddReply: () -> Unit) =
        object : OnReplyAddBoxInterface {
            override fun onClickOpenAttachMenu() {
                if (OnMyClick.isDoubleClick()) return
                onClickOpenAttachMenu()
            }

            override fun onClickAddReply() {
                if (OnMyClick.isDoubleClick()) return
                onClickAddReply()
            }
        }

interface OnPhotoUriInterface {
    fun onClickPhoto()
    fun onClickClose()
}

fun interPhotoUri(onClickPhoto: () -> Unit, onClickClose: () -> Unit): OnPhotoUriInterface {
    return object : OnPhotoUriInterface {
        override fun onClickPhoto() {
            if (OnMyClick.isDoubleClick()) return
            onClickPhoto()
        }

        override fun onClickClose() {
            if (OnMyClick.isDoubleClick()) return
            onClickClose()
        }
    }
}

interface OnReplyEarlierWriteInterface {
    fun onClickLine()
    fun onClickClose()
}

fun interReplyEarlierWrite(onClickLine: () -> Unit, onClickClose: () -> Unit): OnReplyEarlierWriteInterface {
    return object : OnReplyEarlierWriteInterface {
        override fun onClickLine() {
            if (OnMyClick.isDoubleClick()) return
            onClickLine()
        }

        override fun onClickClose() {
            if (OnMyClick.isDoubleClick()) return
            onClickClose()
        }
    }
}

interface OnReplyEarlierInterface {
    fun onClickLine()
}

fun interReplyEarlier(onClickLine: () -> Unit): OnReplyEarlierInterface {
    return object : OnReplyEarlierInterface {
        override fun onClickLine() {
            if (OnMyClick.isDoubleClick()) return
            onClickLine()
        }
    }
}

interface OnTossModelInterface {
    fun onClickGoto(v:View)
    fun onClickRemove(v:View)
    fun onClickClose(v:View)
}

fun interTossModel(onClickGoto: (View) -> Unit, onClickRemove: (View) -> Unit, onClickClose: (View) -> Unit): OnTossModelInterface {
    return object : OnTossModelInterface {
        override fun onClickGoto(v:View) {
//            if (OnMyClick.isDoubleClick()) return
            onClickGoto(v)
        }

        override fun onClickRemove(v:View) {
//            if (OnMyClick.isDoubleClick()) return
            onClickRemove(v)
        }

        override fun onClickClose(v:View) {
//            if (OnMyClick.isDoubleClick()) return
            onClickClose(v)
        }
    }
}


interface OnReplyBottomMenuInterface {

    fun onClickCloseAttachMenu()
    fun onClickOpenGallery()
    fun onClickOpenTossList()
}

fun interReplyBottom(onClickCloseAttachMenu: () -> Unit, onClickOpenGallery: () -> Unit, onClickOpenTossList: () -> Unit) =
        object : OnReplyBottomMenuInterface {
            override fun onClickCloseAttachMenu() {
                if (OnMyClick.isDoubleClick()) return
                onClickCloseAttachMenu()
            }

            override fun onClickOpenGallery() {
                if (OnMyClick.isDoubleClick()) return
                onClickOpenGallery()
            }

            override fun onClickOpenTossList() {
                if (OnMyClick.isDoubleClick()) return
                onClickOpenTossList()
            }
        }

/************************ sttManual  ************************************************/
interface OnManualMoreInterface {
    fun onClickShowMore()
}

fun interManualMore(onClickShowMore: () -> Unit) =
        object : OnManualMoreInterface {
            override fun onClickShowMore() {
                if (OnMyClick.isDoubleClick()) return
                onClickShowMore()
            }
        }

/************************ sttTag  ************************************************/
interface OnAddTagInterface {
    fun onClickOpenTagDialog()
}

fun interAddTag(onClickOpenTagDialog: () -> Unit) =
        object : OnAddTagInterface {
            override fun onClickOpenTagDialog() {
                if (OnMyClick.isDoubleClick()) return
                onClickOpenTagDialog()
            }
        }

/************************ sttBtn  ************************************************/
interface OnBtnPreNextInterface {
    fun onClickPre(p: Int)
    fun onClickNext(p: Int)
}

fun interBtnPreNext(onClickPre: (p: Int) -> Unit, onClickNext: (p: Int) -> Unit) =
        object : OnBtnPreNextInterface {
            override fun onClickPre(p: Int) {
                onClickPre(p)
            }

            override fun onClickNext(p: Int) {
                onClickNext(p)
            }
        }


/************************ sttAd ************************************************/
interface OnBidInterface {
    fun onClickAd(v: View)
}

fun interBid(onClickAd: (v: View) -> Unit) =
        object : OnBidInterface {
            override fun onClickAd(v: View) {
                OnMyClick.setDisableAWhileBTN(v)
                onClickAd(v)
            }
        }


/************************ sttSearch  ************************************************/
interface OnSearchTagviewInterface {
    fun onClickTagview(v: View)
    fun onClickSearch(v: View)
}

fun interSearchTagview(onTagView: (View) -> Unit, onSearch: (View) -> Unit) =
        object : OnSearchTagviewInterface {
            override fun onClickTagview(v: View) {
                OnMyClick.setDisableAWhileBTN(v)
                onTagView(v)
            }

            override fun onClickSearch(v: View) {
                OnMyClick.setDisableAWhileBTN(v)
                onSearch(v)
            }

        }

