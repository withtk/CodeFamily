package com.tkip.mycode.init.main

import android.os.Handler
import android.widget.LinearLayout
import com.firebase.ui.firestore.FirestoreRecyclerOptions
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.firestore.Query
import com.tkip.mycode.R
import com.tkip.mycode.admin.menu.AMenu
import com.tkip.mycode.auth.OnSign
import com.tkip.mycode.databinding.NavHeadBinding
import com.tkip.mycode.databinding.NavMenuBinding
import com.tkip.mycode.dialog.toast.toastNormal
import com.tkip.mycode.funs.common.arrTranToolbar
import com.tkip.mycode.funs.common.onClick
import com.tkip.mycode.funs.common.tranPair
import com.tkip.mycode.funs.custom.menu.ViewMenuBtnSquare
import com.tkip.mycode.init.*
import com.tkip.mycode.init.inter.interBasicClick
import com.tkip.mycode.model.cost.credit.OnCredit
import com.tkip.mycode.model.count.Count
import com.tkip.mycode.model.count.CountAdapterFireS
import com.tkip.mycode.model.count.my_book_push.CountAdapter
import com.tkip.mycode.model.count.quick.OnQuick
import com.tkip.mycode.model.peer.OnPeer
import com.tkip.mycode.model.peer.isAnonymous
import com.tkip.mycode.model.peer.mypeer
import com.tkip.mycode.model.peer.myuid
import com.tkip.mycode.util.lib.retrofit2.ESget
import com.tkip.mycode.util.lib.retrofit2.ESquery
import com.tkip.mycode.util.tools.anim.gone
import com.tkip.mycode.util.tools.etc.OnMyClick

fun NavHeadBinding.setHead(mainActivity: MainActivity) {

    /** 내비 HEAD 세팅 *****************/
    this.peer = mypeer()

    this.inter = interBasicClick(
            on1 = {
                /** Click 구글계정으로 전환*/
                OnMyClick.setDisableAWhileBTN(it)
                OnSign.askSignOut(mainActivity)
            },
            on2 = {
                /** Click 유저사진 수정  */
                OnMyClick.setDisableAWhileBTN(it)
                OnPeer.openUpdatePeerDialog(mainActivity, null)
            },
            on3 = {
                /** Click credit menu*/
                OnMyClick.setDisableAWhileBTN(it)
//                MyAlert.showCreditMenu(mainActivity)
                OnCredit.gotoCreditMenuActivity(mainActivity, arrayOf(this.btnCredit.tranPair(R.string.tran_toolbar)))
            })


    /** 이벤트 세팅 : 베타유저 보너스 *******************************/
    OnCreditEvent.setupEvent(mainActivity, btnEvent)


    /**  내 크레딧 세팅 *************************************************/
    this.inMyCredit.peer = mypeer()
    this.inMyCredit.inter = interBasicClick(
            on1 = {
                // credit click
            }, on2 = null, on3 = null)

}


/**   * 일반 Menu 설정   in drawer  */
fun NavMenuBinding.setMyMenu(mainActivity: MainActivity) {
    val linearParams = LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT).apply { weight = 1f }
    arrayOf(AMenu.MY_BOOKMARK, AMenu.MY_PUSH, AMenu.MY_CONTENTS).forEach { aMenu -> this.lineMenu1.addView(ViewMenuBtnSquare(mainActivity, aMenu, null, onClick = { aMenu.onClickMenu(mainActivity, it.arrTranToolbar()) }), linearParams) }
    if (isAnonymous()) {
        this.lineMenu2.addView(ViewMenuBtnSquare(mainActivity, AMenu.SIGN_OUT_CHANGE, null, onClick = { OnSign.askSignOut(mainActivity) }), linearParams)
    } else {
        this.lineMenu2.addView(ViewMenuBtnSquare(mainActivity, AMenu.SETTINGS, null, onClick = { AMenu.SETTINGS.onClickMenu(mainActivity, it.arrTranToolbar()) }), linearParams)
        this.lineMenu2.addView(ViewMenuBtnSquare(mainActivity, AMenu.ADVERTISEMENT, null, onClick = { AMenu.ADVERTISEMENT.onClickMenu(mainActivity, it.arrTranToolbar()) }), linearParams)
        this.lineMenu2.addView(ViewMenuBtnSquare(mainActivity, AMenu.NOTHING, null, onClick = { }), linearParams)
    }

}

/**   * Admin Menu 설정 in drawer  */
fun NavMenuBinding.setAdminMenu(mainActivity: MainActivity) {
    if (OnPeer.isAdmin(false)) {
        val linearParams = LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT).apply { weight = 1f }
        arrayOf(AMenu.PEER_LIST, AMenu.GRANT_LIST, AMenu.SETTINGS_ADMIN).forEach { aMenu -> this.lineMenuAdmin.addView(ViewMenuBtnSquare(mainActivity, aMenu, null, onClick = { aMenu.onClickMenu(mainActivity, it.arrTranToolbar()) }), linearParams) }
    } else this.lineMenuAdmin.gone()
}


/**********************************************************************************
 * * Quick 메뉴 세팅 in drawer
 **********************************************************************************/
//fun NavMenuBinding.setupQuickMenu(mainActivity: MainActivity) {
//
//      adapter = CountAdapter(quickList, CountAdapter.rvMyQuick)
//    rv.adapter = adapter
//    resetQuickList(null)
//
//    /** 퀵메뉴 관리 버튼 세팅*/
//    btnManage.onClick {
//        if (quickList.isNullOrEmpty()) toastNormal(R.string.info_no_data_quick)
//        else OnQuick.gotoQuickManageActivity(mainActivity, quickList, arrayOf(this.btnManage.tranPair(R.string.tran_toolbar)))
//    }
//    btnRefresh.onClick {
////        btnRefresh.animTurning()
//        resetQuickList(onResult = {
////        btnRefresh.animStopTurning()
//        })
//    }
//}
//
//private fun resetQuickList(onResult:(()->Unit)?) {
//    ESget.getCountList("resetQuickList", EES_QUICK, ESquery.quickList(100, myuid()),
//            success = {list->
//                quickList.clear()
//                if (!list.isNullOrEmpty()) quickList.addAll(list)
//                adapter?.notifyDataSetChanged()
//                onResult?.invoke()
//            }, fail = {onResult?.invoke()})
//}


//fun NavMenuBinding.setQuickMenu(mainActivity: MainActivity): CountAdapterFireS {
//
//    /** 퀵 불러오기 */
//    val query = FirebaseFirestore.getInstance().collection(EES_PEER).document(myuid()).collection(EES_QUICK).orderBy(CH_ORDER, Query.Direction.ASCENDING)
//    val options = FirestoreRecyclerOptions.Builder<Count>().setQuery(query, Count::class.java).build()
////    query.get().addOnSuccessListener { lllogI("CountAdapterFireS setQuickMenu addOnSuccessListener size : ${it.size()}") }
//    val adapter = CountAdapterFireS(mainActivity, options)
//    this.rv.adapter = adapter
////    this.inNoData.tvNoData.text = R.string.info_no_data_quick.getStr()
//    adapter.startListening()   // stopListening : in SuperClass
//
//    /**         * 리프레쉬 : 가끔 안 보일때가 있다.         */
//    Handler().postDelayed({ adapter.notifyDataSetChanged() }, 1000)
//
//    /** 퀵메뉴 관리 버튼 세팅*/
//    this.btnManage.onClick {
//        val quickList = arrayListOf<Count>().apply { adapter.snapshots.forEach { this.add(it) } }
//        if (quickList.isNullOrEmpty()) toastNormal(R.string.info_no_data_quick)
//        else OnQuick.gotoQuickManageActivity(mainActivity, quickList, arrayOf(this.btnManage.tranPair(R.string.tran_toolbar)))
//    }
//
//    return adapter
//}
