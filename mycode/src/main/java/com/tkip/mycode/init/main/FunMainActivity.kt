package com.tkip.mycode.init.main

import android.app.Activity
import android.content.Intent
import android.os.Handler
import android.view.View
import androidx.activity.result.ActivityResult
import androidx.activity.result.contract.ActivityResultContracts
import androidx.databinding.ViewDataBinding
import androidx.drawerlayout.widget.DrawerLayout
import androidx.fragment.app.Fragment
import com.google.firebase.auth.FirebaseAuth
import com.tkip.mycode.R
import com.tkip.mycode.ad.AdType
import com.tkip.mycode.ad.ad_show.CustomBannerSlide
import com.tkip.mycode.admin.resetSystemMan
import com.tkip.mycode.auth.OnAuth
import com.tkip.mycode.auth.OnSign
import com.tkip.mycode.auth.SignActivity
import com.tkip.mycode.databinding.ActivityMainBinding
import com.tkip.mycode.dialog.toast.TToast
import com.tkip.mycode.dialog.toast.toastNormal
import com.tkip.mycode.dialog.toast.toastWarning
import com.tkip.mycode.funs.common.*
import com.tkip.mycode.funs.search.OnSearch
import com.tkip.mycode.init.*
import com.tkip.mycode.init.base.Sred
import com.tkip.mycode.init.my_bind.BindingFragment
import com.tkip.mycode.model.count.Count
import com.tkip.mycode.model.count.my_book_push.CountAdapter
import com.tkip.mycode.model.count.quick.OnQuick
import com.tkip.mycode.model.count.quick.QuickManageActivity
import com.tkip.mycode.model.manage.SystemMan
import com.tkip.mycode.model.peer.OnPeer
import com.tkip.mycode.model.peer.Peer
import com.tkip.mycode.model.peer.mypeer
import com.tkip.mycode.model.peer.myuid
import com.tkip.mycode.nav.board.MainBoardFrag
import com.tkip.mycode.nav.home.HomeFrag
import com.tkip.mycode.nav.store.MainStoreFrag
import com.tkip.mycode.util.lib.retrofit2.ESget
import com.tkip.mycode.util.lib.retrofit2.ESquery
import com.tkip.mycode.util.tools.anim.animStopTurning
import com.tkip.mycode.util.tools.anim.animTurning
import com.tkip.mycode.util.tools.etc.OnMyClick
import com.tkip.mycode.util.tools.fire.Firee
import com.tkip.mycode.util.tools.fire.realOffline
import com.tkip.mycode.util.tools.fire.realOnline


/** * init MainActivity */
/** * Bottom Fragment List : menu_bottom_navigation_shifting.xml과 연동. */
private val arrPairMain by lazy {
    arrayOf(
            Pair(MainStoreFrag.newInstance(), R.id.nav_store),
            Pair(HomeFrag.newInstance(), R.id.nav_home),
            Pair(MainBoardFrag.newInstance(), R.id.nav_board)
//        Pair(LobbyFrag.newInstance(), R.id.nav_land),
//        Pair(MyRoomFrag.newInstance(), R.id.nav_settings)
    )
}
private var preFrag: Fragment? = null

//private var currentItemId = R.id.nav_store
private val defaultNo = 1


//fun MainActivity.resetSystemManage(data: SystemManage?) {
//    Sred.putObjectSystemManage(Sred.SYSTEM_MANAGE, data)
//    systemManage = data
//}


fun MainActivity.initView() {

    realOnline()


    b.activity = this
    b.appMain.activity = this
    setBottomNav(b)
    setDrawerLay(b)
    setupFragmentTransaction(b)


    /***  세션 실시간 체크  */
    FirebaseAuth.getInstance().addAuthStateListener(authListener)
    /***  실시간 리스너  */
    setupRealListener()
    /**    *  실시간 quick 메뉴 셋팅     */
//    quickAdapter = b.inMenu.setQuickMenu(this)
    /**    *  수동 quick 메뉴 셋팅     */
    setupQuickMenu()


    /**    * 로그인 시간   */
    Firee.loginPeer(mypeer(), {}, {})

//    b.appMain.bottomNav.menu.findItem(currentItemId).isChecked = true  // 처음 선택된 메뉴.

}


/**********************************************************************************
 * * Quick 메뉴 세팅 in drawer
 **********************************************************************************/
fun MainActivity.setupQuickMenu() {

    quickAdapter = CountAdapter(this, quickList, CountAdapter.rvMyQuick)
    b.inMenu.rv.adapter = quickAdapter
    resetQuickList(null)

    /** 퀵메뉴 관리 버튼 세팅*/
    b.inMenu.btnManage.onClick {
        if (quickList.isNullOrEmpty()) toastNormal(R.string.info_no_data_quick)
        else OnQuick.gotoQuickManageActivity(this, quickList, arrayOf(b.inMenu.btnManage.tranPair(R.string.tran_toolbar)))
    }
    b.inMenu.btnRefresh.onClick {
        OnMyClick.setDisableAWhileBTN(it)
        b.inMenu.btnRefresh.animTurning()
        resetQuickList(onResult = {
            b.inMenu.btnRefresh.animStopTurning()
        })
    }

}

private fun MainActivity.resetQuickList(onResult: (() -> Unit)?) {
    ESget.getCountList("resetQuickList", EES_QUICK, ESquery.quickUID(100, myuid()),
            success = { list ->
                quickList.clear()
                if (!list.isNullOrEmpty()) quickList.addAll(list)
                quickAdapter?.notifyDataSetChanged()
                onResult?.invoke()
            }, fail = { onResult?.invoke() })
}


/*****************************************************************************************
 * MainActivity   리스너
 *****************************************************************************************/
fun MainActivity.setupRealListener() {
    lllogM("mainActivity listener setupRealListener start ")
    lllogD("mainActivity    FirebaseAuth currentUser2 ${FirebaseAuth.getInstance().currentUser}")

    /**    * 실시간 peer & notice  & system manage    */
//    listenerRegiPeer = null
    if (listenerRegiPeer == null) listenerRegiPeer = Firee.commonRealTimeDoc("listenerRegiPeer", arrayOf(EES_PEER, myuid()),
            success = { doc ->
                doc.toObject(Peer::class.java)?.let { peer ->
                    checkPeerValid(peer)
//                    hhh(3000) {
//                    }
                    b.inHead.peer = mypeer()
                    b.inHead.inMyCredit.peer = mypeer()
                }

//                        ?: OnAuth.signOut(this, false, success = { OnSign.gotoSignActivityAndFinish(this, SignActivity.TYPE_NORMAL, false) }, fail = {})                // 로그아웃 후 다시 SignActivity 실행.
            }, fail = { })


    if (listenerRegiSystem == null) listenerRegiSystem = Firee.commonRealTimeDoc("listenerRegiSystem", arrayOf(EES_SYSTEM, SYSTEM_MAN_KEY), success = { doc -> resetSystemMan(doc.toObject(SystemMan::class.java)) }, fail = { })
}

fun MainActivity.removeAllListener() {
    realOffline()
    FirebaseAuth.getInstance().removeAuthStateListener(authListener)
    OnBasic.removeBasicListener()
}


/** * 서랍 셋팅 */
fun MainActivity.setDrawerLay(b: ActivityMainBinding) {

    /** Drawer HEAD 세팅 */
    b.inHead.setHead(this)

    /** Drawer Menu 세팅 */
    b.inMenu.setMyMenu(this)
    b.inMenu.setAdminMenu(this)

    /**   광고 MENU */
    bannerMenu = CustomBannerSlide(this, AdType.MENU, null)
    b.frameAd.addView(bannerMenu)
    bannerMenu?.stopSlide()   // 메뉴배너는 메뉴 열때 슬라이드 시작.

    /** 통합검색 버튼 */
    b.btnSearch.onClick { v ->
        OnMyClick.setDisableAWhileBTN(v)
        OnSearch.gotoSearchTotalActivity(this, arrayOf(v.tranPair(R.string.tran_toolbar)))
    }


    /** Drawer Toggle Listener */
    b.drawerLayout.addDrawerListener(object : DrawerLayout.DrawerListener {
        override fun onDrawerStateChanged(newState: Int) {
        }

        override fun onDrawerSlide(drawerView: View, slideOffset: Float) {
//            lllogM("drawerListener onDrawerSlided")
        }

        override fun onDrawerClosed(drawerView: View) {
//            lllogM("drawerListener onDrawerClosed")
            bannerMenu?.stopSlide()
        }

        override fun onDrawerOpened(drawerView: View) {
//            lllogM("drawerListener onDrawerOpened")
            bannerMenu?.startSlide("FunMainactivity addDrawerListener")
        }
    })


}


fun MainActivity.setBottomNav(b: ActivityMainBinding) {

    b.appMain.bottomNav.setOnNavigationItemSelectedListener { menuItem ->
        lllogI("mainActivity setOnNavigationItemSelectedListener   ===")
//        if (currentItemId == menuItem.itemId) return@setOnNavigationItemSelectedListener false
//        currentItemId = menuItem.itemId

        /** change Fragment */
//        val f = getNavFrag(menuItem.itemId)
        Handler().post {

            val f: BindingFragment<out ViewDataBinding>
//            val f =
            when (menuItem.itemId) {
                R.id.nav_store -> {
                    f = arrPairMain[0].first
                    toggleHomeBanner(false)
                }
                R.id.nav_board -> {
                    f = arrPairMain[2].first
                    toggleHomeBanner(false)
                }
                /*** R.id.nav_home */
                else -> {
                    f = arrPairMain[1].first
                    toggleHomeBanner(true)
                }
            }

            /*** 보드 메인 초기화 실행 */
//        if (f is MainBoardFrag) f.initView()

            lllogI("mainActivity setOnNavigationItemSelectedListener post ===")
            val fragTran = supportFragmentManager.beginTransaction()
//                fragTran.setCustomAnimations(android.R.anim.fade_in, android.R.anim.fade_out, android.R.anim.fade_in, android.R.anim.fade_out)
            preFrag?.let { fragTran.hide(it) }
            fragTran.show(f)
            preFrag = f
            fragTran.commitAllowingStateLoss()
        }
        return@setOnNavigationItemSelectedListener true
    }
}

/** MAIN광고 슬라이스 재생/스탑 : 문제: 처음에 배너 받아오기전부터 걸려버려서..그게 계속 로그를 남기네. */
private fun toggleHomeBanner(start: Boolean) {
    if (start) arrPairMain[1].first.banner?.startSlide("FunMainactivity toggleHomeBanner")
    else arrPairMain[1].first.banner?.stopSlide()
}

private fun getNavFrag(itemId: Int): Fragment {
    arrPairMain.forEach { pair ->
        if (pair.second == itemId) return pair.first
    }
    return arrPairMain[defaultNo].first   // 없으면 초기셋팅 frag 반환.
}

private fun MainActivity.setupFragmentTransaction(b: ActivityMainBinding) {
    lllogI("mainActivity Init setupFragmentTransaction ===")

//        fragTran = fm.beginTransaction()
    Handler().post {
        val fragTran = supportFragmentManager.beginTransaction()
        for (f in supportFragmentManager.fragments) {
            lllogI("mainActivity Init setupFragmentTransaction remove ${f.javaClass.simpleName} ===")
            fragTran.remove(f)
        }
        //            fragTran.setCustomAnimations(android.R.anim.fade_in, android.R.anim.fade_out, android.R.anim.fade_in, android.R.anim.fade_out)
        arrPairMain.forEach { pair ->
            val f = pair.first
            lllogI("mainActivity Init setupFragmentTransaction add ${f.javaClass.simpleName} ")
            fragTran.add(R.id.frame, f, f.javaClass.simpleName)
            fragTran.hide(f)
        }
//        for (f in arrayFrag) {
//            lllogI("recreateTest setupFragmentTransaction add ${f.javaClass.simpleName} ")
//            fragTran.add(R.id.frame, f, f.javaClass.simpleName)
//            fragTran.hide(f)
//        }
        fragTran.commitAllowingStateLoss()
        lllogI("mainActivity Init 처음 이동할 frag ")
        b.appMain.bottomNav.selectedItemId = arrPairMain[defaultNo].second     // 처음 이동할 frag
    }

}


/*****************************************************************************************
 * Peer 유효성 체크
 *****************************************************************************************/
fun MainActivity.checkPeerValid(peer1: Peer) {
    lllogD("mainActivity removeBasicListenerSSSSSS listener checkPeerValid loginTime:${peer1.loginTime}")

    /** set Sred peer */
    OnPeer.setUpPeer(peer1)  // 매번 onResume

    /** 중복 로그인 체크 */
    peer1.fcmToken?.let { token ->
        /** 중복 감지시 : 중요. 이때는 fcmToken null시키면 안 됨.*/
        if (token != Sred.DEVICE_FCM_TOKEN.sredString()) {
            toastWarning(R.string.ing_login_duplicated)
            removeAllListener()   // 미리 제거 안 하면 peer.fcmToken = null로 변경시 리스너에서 충돌남.
            OnAuth.signOut(this, true, success = { OnSign.gotoSignActivityAndFinish(this, SignActivity.TYPE_DUPLICATE_LOGIN, true) }, fail = {})
        }
    }
}




