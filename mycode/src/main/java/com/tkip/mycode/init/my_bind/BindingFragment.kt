package com.tkip.mycode.init.my_bind

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.FrameLayout
import androidx.annotation.LayoutRes
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.fragment.app.Fragment
import com.tkip.mycode.ad.AdType
import com.tkip.mycode.ad.ad_show.CustomBannerSlide
import com.tkip.mycode.funs.common.lllogD
import com.tkip.mycode.funs.common.lllogM

/**
 */
abstract class BindingFragment<T : ViewDataBinding> : Fragment() {
    @LayoutRes
    abstract fun getLayoutResId(): Int

    lateinit var b: T
        private set

    //    var started = false   // fragment 초기 data 셋팅됐는지 여부.
    var banner: CustomBannerSlide? = null

    lateinit var mContext: Context
    override fun onAttach(context: Context) {
        super.onAttach(context)
        mContext = context
//        lllogD("mContextTest BindingFragment onAttach mContext:$mContext")
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return DataBindingUtil.inflate<T>(inflater, getLayoutResId(), container, false).apply { b = this }.root
    }

    override fun onResume() {
        super.onResume()
//        lllogM(" BindingFragment onResume  banner:${banner?.adType}")
//        banner?.startSlide("BindingFragment")    // 배너 슬라이드 시작
    }

    override fun onPause() {
        super.onPause()
//        lllogM(" BindingFragment onPause  banner:${banner?.adType}")
//        banner?.stopSlide()    // 배너 슬라이드 멈춤
    }


    /**   광고 top */
    fun addAdvertisement(adType: AdType, boardKey: String?, frameLayout: FrameLayout) {
        banner = CustomBannerSlide(mContext, adType, boardKey)
        frameLayout.addView(banner)
    }

}