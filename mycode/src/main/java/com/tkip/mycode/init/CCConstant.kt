package com.tkip.mycode.init

import android.app.Activity
import com.google.firebase.firestore.ListenerRegistration
import com.tkip.mycode.R
import com.tkip.mycode.init.base.Sred
import com.tkip.mycode.init.base.getInt
import com.tkip.mycode.init.base.getStr
import com.tkip.mycode.model.board.Board
import com.tkip.mycode.model.peer.Peer
import com.tkip.mycode.model.toss.Toss
import java.util.*
import kotlin.collections.ArrayList
import kotlin.math.ceil


const val PUT_ACTION = "PUT_ACTION"
const val ACTION_NOTHING = 5
const val ACTION_NEW = 10
const val ACTION_UPDATE = 20
const val ACTION_PREVIEW = 30
const val ACTION_NORMAL = 40
const val ACTION_ADMIN = 50
const val ACTION_DUPLICATE_LOGIN = 60
//const val ACTION_BANNER_MANAGE = 50
//const val ACTION_BANNER_SELECT = 60


const val PUT_UID = "PUT_UID"
const val PUT_PEER = "PUT_PEER"
const val PUT_KEY = "PUT_KEY"
const val PUT_INFO = "PUT_INFO"
const val PUT_BOOLEAN = "PUT_BOOLEAN"
const val PUT_INDEX = "PUT_INDEX"
const val PUT_QUERY = "PUT_QUERY"
const val PUT_INT = "PUT_INT"
const val PUT_URL = "PUT_URL"
const val PUT_DATA_TYPE = "PUT_DATA_TYPE"
const val PUT_DATA_TYPE_NO = "PUT_DATA_TYPE_NO"
const val PUT_COST = "PUT_COST"
const val PUT_COST_TYPE_NO = "PUT_COST_TYPE_NO"
const val PUT_CNT_NO = "PUT_CNT_NO"
const val PUT_OBJECT = "PUT_OBJECT"
const val PUT_ARRAY_COUNT = "PUT_ARRAY_COUNT"
const val PUT_ARRAY_FOTO = "PUT_ARRAY_FOTO"
const val PUT_ARRAY_MYPHOTO = "PUT_ARRAY_MYPHOTO"

const val PUT_BOARD = "PUT_BOARD"
const val PUT_TITLE = "PUT_TITLE"
const val PUT_BOARD_KEY = "PUT_BOARD_KEY"
const val PUT_BOARD_DESC = "PUT_BOARD_DESC"
const val PUT_BOARD_NAME = "PUT_BOARD_NAME"
const val PUT_BOARD_TYPE_NO = "PUT_BOARD_TYPE_NO"
const val PUT_POST = "PUT_POST"
const val PUT_ROOM = "PUT_ROOM"
const val PUT_STORE = "PUT_STORE"
const val PUT_STORE_KEY = "PUT_STORE_KEY"
const val PUT_STORE_MENU = "PUT_STORE_MENU"
const val PUT_BID = "PUT_AD_UNIT"
const val PUT_BANNER_TYPE = "PUT_BANNER_TYPE"
const val PUT_MY_TAG = "PUT_MY_TAG"
const val PUT_TICKET = "PUT_TICKET"
const val PUT_FLEA = "PUT_FLEA"
const val PUT_WARN = "PUT_WARN"
const val PUT_BANNER = "PUT_BANNER"
const val PUT_BANNER_NO = "PUT_BANNER_NO"
const val PUT_COUNT_TYPE_NO = "PUT_COUNT_TYPE_NO"
const val PUT_MANAGE = "PUT_MANAGE"


const val CURRENT_ITEM_ID = "CURRENT_ITEM_ID"


const val PUT_SAVE_BOARD = "PUT_SAVE_BOARD"
const val PUT_SAVE_CURRENT_POS = "PUT_SAVE_CURRENT_POS"
const val PUT_RESULT_OBJECT = "PUT_RESULT_OBJECT"
const val PUT_RESULT_LATLNG = "PUT_RESULT_LATLNG"
const val PUT_RESULT_LIST = "PUT_RESULT_LIST"
const val PUT_RESULT_URI = "PUT_RESULT_URI"

const val PUT_RV_TYPE = "PUT_RV_TYPE"

/******* RV 타입 *****************/
const val RV_VERT = 10
const val RV_HORI = 20
const val RV_HORI_BIG = 24
const val RV_GRID_VERT = 30
const val RV_GRID_HORI = 40
const val RV_MY_CONTENTS = 50
const val RV_MY_CONTENTS_MANAGE = 53   // 유저 데이타목록을 관리자가 볼때.
const val RV_MANAGE = 55
const val RV_SEARCH = 57
const val RV_STORE_HOST = 60
/*********************************/


/**
 * View타입 using : ViewMenuNormal
 */
const val V_TYPE1 = 10
const val V_TYPE2 = 20

/*****************************************************************
 *********************** sred ************************************
 *****************************************************************/
val peerMap by lazy { hashMapOf<String, Peer>() }
val boardMap by lazy { hashMapOf<String, Board>() }


var BOARD_NO1: Board? = Sred.getObjectBoard(Sred.BOARD_NO1)
var recentBoard=Sred.getObjectBoard(Sred.BOARD_RECENT)
var phCodeBoard: Board? = null
var fleaBoard: Board? = null
var roomBoard: Board? = null
const val PH_CODE_BOARD_KEY = "0phcodeBoardKey"
const val RECENT_BOARD_KEY = "0recentBoardKey"
const val FLEA_BOARD_KEY = "0fleaBoardKey"
const val ROOM_BOARD_KEY = "0roomBoardKey"
const val BOARD_KEY_SUGGEST = "158953458705990fEvi"
const val BOARD_KEY_ALLIANCE = "159107987665890fEvi"
const val BOARD_KEY_BILL_CS = "160136511351590fEvi"

var NOTICE_KEY = "notice"
var SYSTEM_MAN_KEY = "system_man"

fun String.sredString() = Sred.getString(this)
fun String.sredInt(default: Int) = Sred.getInt(this, default)
fun String.sredPutInt(i: Int) = Sred.putInt(this, i)
fun String.sredBoolean(default: Boolean) = Sred.getBoolean(this, default)
fun String.sredPutBoolean(bool: Boolean) = Sred.putBoolean(this, bool)


/*****************************************************************
 *********************** toss ************************************
 *****************************************************************/
const val TOSS_COPY = 10   // 복사 완료 표시할 때
const val TOSS_LIST = 20   // 리스트에 보일때
const val TOSS_PASTE = 30  // reply Add box에 붙일때
const val TOSS_VIEW = 40   // in reply에서 보일때


val myTossList = arrayListOf<Toss>()
fun ArrayList<Toss>.has(toss: Toss): Boolean {
    this.forEach {
        if (it.modelKey == toss.modelKey) return true
    }
    return false
}

fun ArrayList<Toss>.checkAdd(toss: Toss) {
    if (!this.has(toss)) this.add(toss)
}

/*****************************************************************
 *********************** reply ************************************
 *****************************************************************/
const val ATTACH_TEXT = 100
const val ATTACH_PHOTO = 200
const val ATTACH_REPLY = 300
const val ATTACH_TOSS = 400
var REPLY_ATTACH_TYPE = ATTACH_TEXT
var REPLY_ATTACH_REPLY_KEY: String? = null
var REPLY_ATTACH_TOSS: Toss? = null


/******************************************************************
 *********************** 자주쓰는 int ******************************
 *****************************************************************/

const val AMT_PLUS = 1
const val AMT_MINUS = -1


/*********** 제한 *******************/
var LIMIT_TAG_COUNT = R.integer.limit_upload_tag.getInt()
var MSG_LIMIT_TAG = String.format(R.string.info_limit_tag.getStr(), LIMIT_TAG_COUNT)
val MAX_STORE_MENU_SIZE = R.integer.store_menu_limit.getInt()

/*********** 사진 업로드 *******************/
var LIMIT_UPLOAD_COUNT = R.integer.limit_upload_photo.getInt()
var WIDTH_THUMB = 400
var WIDTH_MID = 1280
var WIDTH_ORIGINAL = 10000   // 9000 이상이면 원본사이즈 업로드.
//todo : 원본사이즈는 차후에. 지금은 사이즈를 최대치 1920으로 수정할 것. (portrate랑 landscape에따라 변경될 사이즈 차이남.)

/*****************************************************************
 *********************** DBchild *********************************
 *****************************************************************/
var listenerRegiPeer: ListenerRegistration? = null
var listenerRegiSystem: ListenerRegistration? = null

/*********** permission *******************/

const val REQUEST_PICK = 100
const val REQUEST_ALBUM = 200

const val REQ_AUTH_UI = 7776 // auth ui


const val REQ_PICK = 300
const val REQ_READ_EXSTORAGE = 400
const val REQ_FINE_LOC = 500
const val REQ_MAP_LOCATION = 600
const val REQ_ALBUM = 700
const val REQ_DATA = 800

const val REQ_CONFIRM_DIALOG = 800
const val REQ_ADD_BOARD_DIALOG = 900
const val REQ_BANNER = 1000
const val REQ_PICK_MULTI = 1042
const val REQ_SAF = 1050
const val REQ_COUNT_LIST = 1060


/*********** philpay 기준 환율 *******************/
const val RATE_WON = 160f
const val RATE_CREDIT = 45f
const val RATE_POINT = 1200f


fun Int.wonToPhilPay(): Int = ceil(this / RATE_WON).toInt()
fun Int.philPayToWon(): Int = ceil(this * RATE_WON).toInt()
fun Int.philPayToCredit(): Int = ceil(this * RATE_CREDIT).toInt()
fun Int.creditToPhilPay(): Int = ceil(this / RATE_CREDIT).toInt()
fun Int.creditToPoint(): Int = ceil(this / RATE_CREDIT * RATE_POINT).toInt()
fun Int.pointToCredit(): Int = ceil(this / RATE_POINT * RATE_CREDIT).toInt()
fun Int.pointToPhilPay(): Int = ceil(this / RATE_POINT).toInt()

/** * commission 계산하기 : 커미션percentage뺀 결과 float로  : 커미션 없으면 그냥 amount 리턴.*/
fun Float?.percent(digit: Int): Int =
        this?.let { ceil((digit * (100f - it) / 100)).toInt() }  // ceil(올림)
                ?: digit ?: 0

/** * 순수하게 커미션만.  */
fun Float.commision(digit: Int): Int = (digit * this).toInt()

fun Float?.trim(): Int = String.format("%.1f", this).toInt()


const val ADD = 11
const val UPDATE = 22
const val DEL = 33


const val ST_NO_CHILD = "noChild"
const val ST_PEER = "peer"
const val ST_BOARD = "board"
const val ST_POST = "post"
const val ST_FLEA = "flea"
const val ST_ROOM = "room"
const val ST_REPLY = "reply"
const val ST_STORE = "store"
const val ST_STORE_MENU = "store_menu"
const val ST_BANNER = "banner"
const val ST_BID = "bid"
const val ST_TICKET = "ticket"


/***************  최상위 child(Es index)***********************************************/

var COUNTRY = "ct_"
val EES_NOTHING by lazy { COUNTRY + "nothing" }
val EES_NOTICE by lazy { COUNTRY + "notice" }
val EES_SYSTEM by lazy { COUNTRY + "system" }

val EES_HIT by lazy { COUNTRY + "hit" }
val EES_PEER by lazy { COUNTRY + "peer" }
val EES_RECOMMEND by lazy { COUNTRY + "recommend" }
val EES_ONLINE by lazy { COUNTRY + "online" }
val EES_COST by lazy { COUNTRY + "cost" }
val EES_ACT by lazy { COUNTRY + "act" }
val EES_FOTO_INFO by lazy { COUNTRY + "foto_info" }
val EES_QUICK by lazy { COUNTRY + "quick" }
val EES_BANNER by lazy { COUNTRY + "banner" }

val EES_BOARD by lazy { COUNTRY + "board" }
val EES_BOARD_ALLOW by lazy { COUNTRY + "board_allow" }
val EES_ALLOWED by lazy { COUNTRY + "allowed" }
val EES_DENIED by lazy { COUNTRY + "denied" }
val EES_POST by lazy { COUNTRY + "post" }
val EES_FLEA by lazy { COUNTRY + "flea" }
val EES_ROOM by lazy { COUNTRY + "room" }
val EES_STORE by lazy { COUNTRY + "store" }
val EES_STORE_MENU by lazy { COUNTRY + "store_menu" }

val EES_REPLY by lazy { COUNTRY + "reply" }
val EES_WARN by lazy { COUNTRY + "warn" }
val EES_PENALTY by lazy { COUNTRY + "penalty" }
val EES_TAG by lazy { COUNTRY + "tag" }
val EES_BID by lazy { COUNTRY + "bid" }

val EES_DEVICE by lazy { COUNTRY + "device" }
val EES_LOBBY by lazy { COUNTRY + "lobby" }
val EES_CHAT by lazy { COUNTRY + "chat" }
val EES_BILL by lazy { COUNTRY + "bill" }
val EES_TICKET by lazy { COUNTRY + "ticket" }
val EES_GRANT by lazy { COUNTRY + "grant" }

/**************************************************************************/


const val CH_COUNT = "count"
const val CH_UID = "uid"
const val CH_ADMIN_UID = "adminUID"
const val CH_AUTHOR_UID = "authorUID"
const val CH_AUTHOR_NICK = "authorNick"
const val CH_DATA_TYPE_NO = "dataTypeNo"
const val CH_MODEL_KEY = "modelKey"
const val CH_MODEL_TITLE = "modelTitle"
const val CH_CHILD_NO = "childNo"
const val CH_QUICK = "quick"

const val CH_VIEW_COUNT = "viewCount"
const val CH_VIEW_UID_COUNT = "viewUidCount"
const val CH_THUMB_UP_COUNT = "thumbUpCount"
const val CH_THUMB_DOWN_COUNT = "thumbDownCount"
const val CH_BOOKMARK_COUNT = "bookmarkCount"
const val CH_PUSH_COUNT = "pushCount"
const val CH_REPLY_COUNT = "replyCount"
const val CH_ONLINE_COUNT = "onlineCount"

const val CH_VIEW_COUNT_LOWER = "view_count"
const val CH_VIEW_UID_COUNT_LOWER = "view_uid_count"
const val CH_THUMB_UP_COUNT_LOWER = "thumb_up_count"
const val CH_THUMB_DOWN_COUNT_LOWER = "thumb_down_count"
const val CH_BOOKMARK_COUNT_LOWER = "bookmark_count"
const val CH_PUSH_COUNT_LOWER = "push_count"
const val CH_REPLY_COUNT_LOWER = "reply_count"
const val CH_ONLINE_COUNT_LOWER = "online_count"

const val CH_BOARD_COUNT = "board_count"
const val CH_POST_COUNT = "post_count"
const val CH_FLEA_COUNT = "flea_count"
const val CH_ROOM_COUNT = "room_count"
const val CH_STORE_COUNT = "store_count"


//////////////////  count //////////////////////

const val CH_BOARD_BOOK_COUNT = "board_bookmarkcount"
const val CH_BOARD_PUSH_COUNT = "board_pushcount"

const val CH_POST_UID_COUNT = "post_uidcount"
const val CH_POST_UP_COUNT = "post_thumbupcount"
const val CH_POST_DOWN_COUNT = "post_thumbdowncount"
const val CH_POST_BOOK_COUNT = "post_bookmarkcount"
const val CH_POST_PUSH_COUNT = "post_pushcount"

const val CH_FLEA_UID_COUNT = "flea_uidcount"
const val CH_FLEA_BOOK_COUNT = "flea_bookmarkcount"
const val CH_FLEA_PUSH_COUNT = "flea_pushcount"

const val CH_ROOM_UID_COUNT = "room_uidcount"
const val CH_ROOM_BOOK_COUNT = "room_bookmarkcount"
const val CH_ROOM_PUSH_COUNT = "room_pushcount"

const val CH_STORE_UID_COUNT = "store_uidcount"
const val CH_STORE_BOOK_COUNT = "store_bookmarkcount"
const val CH_STORE_PUSH_COUNT = "store_pushcount"


const val CH_TOTAL_POINT = "totalPoint"
const val CH_BOARD_KEY = "boardKey"
const val CH_BOARD_TYPE_NO = "boardTypeNo"
const val CH_BOARD_MASTER_KEY = "boardMasterKey"
const val CH_BOARD_MASTER_UID = "boardMasterUid"


const val CH_NAME = "name"
const val CH_DESCRIPTION = "description"
const val CH_DESCRIPTION_NO = "descriptionNo"
const val CH_BOARD_ID = "boardId"
const val CH_PASSWORD = "password"
const val CH_BLIND = "blind"
const val CH_FOTO_URL = "fotoUrl"
const val CH_FOTO_THUMB = "fotoThumb"
const val CH_PHOTO_URL = "photoUrl"
const val CH_PHOTO_THUMB = "photoThumb"
const val CH_CHOSEN = "chosen"
const val CH_STATUS = "status"
const val CH_STATUS_NO = "statusNo"
const val CH_FACE = "face"
const val CH_FACE_THUMB = "faceThumb"
const val CH_ADD_DATE = "addDate"
const val CH_TOP_CHILD_NAME = "topChildName"
const val CH_ORDER = "order"
const val CH_DEL_DATE = "delDate"
const val CH_INVALID = "invalid"

const val CH_LOGIN_TIME = "loginTime"
const val CH_ONLINE = "online"
const val CH_INDEX = "index"
const val CH_UPDATE_DATE = "updateDate"
const val CH_LAST_POST_DATE = "lastPostDate"


const val CH_CONTRACT_TYPE_NO = "contractTypeNo"
const val CH_ROOM_STATUS_NO = "roomStatusNo"
const val CH_ROOM_TYPE_NO = "roomTypeNo"
const val CH_CONSTRUCTION_DATE = "constructionDate"
const val CH_AREA_TOTAL = "areaTotal"
const val CH_AREA_EX = "areaEx"
const val CH_MIN_TERN = "minTerm"
const val CH_PRICE = "price"
const val CH_MONTHLY_RENTER = "monthlyRenter"
const val CH_INADVANCED = "inAdvanced"
const val CH_DEPOSIT = "deposit"
const val CH_LOCATION = "location"
const val CH_LAT = "lat"
const val CH_LNG = "lng"
const val CH_ADDRESS = "address"
const val CH_CONTACT = "contact"
const val CH_MEDIA_MAP = "mediaMap"
const val CH_COMMENT = "comment"
const val CH_COMMENT_NO = "commentNo"
const val CH_BUILT_IN_LIST = "builtInList"
const val CH_TAG_LIST = "tagList"
const val CH_COMPLETED = "completed"
const val CH_CREDIT_PAID = "creditPaid"
const val CH_AD_POINT = "adPoint"
const val CH_RELOAD_COUNT = "reloadCount"
const val CH_CPAY = "cpay"
const val CH_CREDIT = "crd"
const val CH_BONUS_CREDIT = "bcrd"
const val CH_POINT = "pnt"
const val CH_CARAT = "crt"

const val CH_PLACE_ID = "placeId"
const val CH_POSTAL_CODE = "postalCode"
const val CH_PLACE_TYPE_LIST = "placeTypeList"
const val CH_WEBSITE_URI = "websiteUri"
const val CH_PHONE_NUMBER = "phoneNumber"
const val CH_WEEKDAY_TEXT = "weekdayText"


const val CH_COUNT_ROOM = "countRoom"
const val CH_COUNT_TOILET = "countToilet"
const val CH_COUNT_KITCHEN = "countKitchen"
const val CH_COUNT_LIVING = "countLiving"
const val CH_COUNT_BATH = "countBath"
const val CH_COUNT_FLOOR = "countFloor"
const val CH_COUNT_PARKING = "countParking"


const val CH_FCM_TOKEN = "fcmToken"

const val CH_TYPE = "type"
const val CH_LOGIN_FROM = "loginFrom"
const val CH_DP_NAME = "dpName"
const val CH_TITLE = "title"
const val CH_STUFF_NAME = "stuffName"
const val CH_LOCAL = "local"
const val CH_FOTO = "foto"
const val CH_FOTO_LIST = "fotoList"
const val CH_DATE_LIST = "dateList"
const val CH_REWARD = "reward"
const val CH_ALLOW_SEARCH = "allowSearch"
const val CH_ALLOW_REPLY = "allowReply"
const val CH_ALLOW_POST = "allowPost"
const val CH_ANONYMOUS = "anonymous"
const val CH_NICK = "nick"
const val CH_EMAIL = "email"

const val CH_BANNER_TYPE_NO = "bannerTypeNo"
const val CH_ADTYPE_NO = "adTypeNo"
const val CH_HIT_DATE = "hitDate"
const val CH_AMOUNT = "amount"

const val ATTACH_FAILED = "_failed"
const val ATTACH_ALLOT = "_allot"
const val ATTACH_THUMB = "_thumb"
const val ATTACH_NO = "_no"
const val ATTACH_URL = "_url"
const val CH_THUMB = "thumb"

