package com.tkip.mycode.init.my_bind

import android.os.Bundle
import androidx.annotation.LayoutRes
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import com.tkip.mycode.init.part.CodeApp

/**
 * 기본 Activity
 *       : no Photo 등..
 */
abstract class BindingActivity<T : ViewDataBinding> : BaseActivity() {
    @LayoutRes
    abstract fun getLayoutResId(): Int

    //    protected lateinit var b: T
    lateinit var b: T
        private set

    override fun onResume() {
        super.onResume()
        CodeApp.curActivity = this
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

//        this.requestedOrientation = ActivityInfo.SCREEN_ORIENTATION_PORTRAIT   // orientation portrate 새로방향 고정.
        b = DataBindingUtil.setContentView(this, getLayoutResId())
    }




//    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
//        super.onActivityResult(requestCode, resultCode, data)
//        lllogI("BannerManageActivity BindingActivity onActivityResult")
//        if (resultCode == RESULT_OK) {
//            when (requestCode) {
//                REQ_BANNER -> {
//
//                }
//            }
//        }
//    }


}