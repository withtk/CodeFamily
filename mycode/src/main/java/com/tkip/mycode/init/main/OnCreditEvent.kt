package com.tkip.mycode.init.main

import android.content.Context
import android.widget.Button
import com.tkip.mycode.R
import com.tkip.mycode.dialog.OnDDD
import com.tkip.mycode.funs.common.lllogD
import com.tkip.mycode.funs.common.onClick
import com.tkip.mycode.init.CH_TYPE
import com.tkip.mycode.init.EES_PEER
import com.tkip.mycode.init.base.getStr
import com.tkip.mycode.model.cost.Cost
import com.tkip.mycode.model.my_enum.ActType
import com.tkip.mycode.model.my_enum.PeerType
import com.tkip.mycode.model.my_enum.Status
import com.tkip.mycode.model.peer.mytype
import com.tkip.mycode.model.peer.myuid
import com.tkip.mycode.util.lib.retrofit2.ESget
import com.tkip.mycode.util.lib.retrofit2.ESquery
import com.tkip.mycode.util.tools.anim.gone
import com.tkip.mycode.util.tools.anim.visi
import com.tkip.mycode.util.tools.etc.OnMyClick
import com.tkip.mycode.util.tools.fire.addCost
import com.tkip.mycode.util.tools.fire.batch
import com.tkip.mycode.util.tools.fire.docRef
import com.tkip.mycode.util.tools.fire.myCommit

class OnCreditEvent {

    companion object {

//        fun eventTest(context: Context, btnEvent:Button) {
//            btnEvent.visi()
//            btnEvent.onClick {
//                lllogD("PhApp interTTTTest btnEvent ")
//                CodeApp.interTTTTest?.onTTTTest(context)
//            }
//        }

        fun setupEvent(context: Context, btnEvent: Button) {
            ActType.BETA_FREE_BONUS_CREDIT.balance()?.bcrd?.let {
                /*** 아직 베타유저가 안 된 유저인지 체크. */
                if (mytype() > PeerType.BASIC_1.no && mytype() < PeerType.BASIC_BETA.no) {

                    btnEvent.onClick {
                        OnMyClick.setDisableAWhileBTN(it)

                        /******************************************************************
                         * 요건 충족 체크 : 3개 충족요건의 cost.actTypeNo가 있는지 여부 체크.
                         */
                        checkBetaRequired(
                                onSuccess = {
                                    batch()
                                            .apply {
                                                update(docRef(EES_PEER, myuid()), CH_TYPE, PeerType.BASIC_BETA.no)
                                                addCost(Cost.new(null, ActType.BETA_FREE_BONUS_CREDIT, null))
                                            }
                                            .myCommit("BETA_FREE_BONUS_CREDIT",
                                                    success = {
                                                        OnDDD.info(context, R.string.info_beta_completed)
                                                        btnEvent.gone()
                                                    }, fail = {})
                                },
                                onFail = {no->
                                    val msg = when (no) {
                                        myBoard -> R.string.info_required_board
                                        myPost -> R.string.info_required_post
                                        else -> R.string.info_required_beta_user
                                    }
                                    OnDDD.info(context, msg)
                                })

//                        ESget.sizeCostList(ESquery.betaCheck(myuid()), success = { size ->
//                            lllogD("OnMyEvent sizeCostList $size")
//                            if (size != null && size >= 3) {
//
//                                batch()
//                                        .apply {
//                                            update(docRef(EES_PEER, myuid()), CH_TYPE, PeerType.BASIC_BETA.no)
//                                            addCost(Cost.new(null, ActType.BETA_FREE_BONUS_CREDIT, null))
//                                        }.myCommit("BETA_FREE_BONUS_CREDIT", success = {
//                                            OnDDD.info(context, R.string.info_beta_completed)
//                                            btnEvent.gone()
//                                        }, fail = {})
//
//                            } else OnDDD.info(context, R.string.info_required_beta_user)
//                        }, fail = {})

                    }
                    btnEvent.text = R.string.btn_event_beta.getStr()
                    btnEvent.visi()
                }
                else  btnEvent.gone()
            }
        }


        /**
         * 요건 충족 체크 : 베타 요건 충족 체크 : 1. 스토어 만들기, 2. 보드 만들기 3. 포스트 한개 올리기
         */
        private var myStore = 1
        private var myBoard = 2
        private var myPost = 3
        private fun checkBetaRequired(onSuccess: () -> Unit, onFail: (Int) -> Unit) {
            val query = ESquery.matchUID(10, 0, myuid(), Status.NOTHING)
            ESget.getStoreList(query, success = {
                lllogD("checkBetaRequired getStoreList size   ${it?.size}")
                if (it.isNullOrEmpty()) onFail(myStore)
                else ESget.getBoardList(query, success = {
                    lllogD("checkBetaRequired getBoardList size   ${it?.size}")
                    if (it.isNullOrEmpty()) onFail(myBoard)
                    else ESget.getPostList(query, success = {
                        lllogD("checkBetaRequired getPostList size   ${it?.size}")
                        if (it.isNullOrEmpty()) onFail(myPost)
                        else onSuccess()
                    }, fail = { onFail(myStore) })
                }, fail = { onFail(myStore) })
            }, fail = { onFail(myStore) })


        }


    }
}