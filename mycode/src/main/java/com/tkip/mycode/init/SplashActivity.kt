package com.tkip.mycode.init

import android.content.Intent
import android.os.Bundle
import com.google.firebase.auth.FirebaseAuth
import com.tkip.mycode.auth.OnSign
import com.tkip.mycode.auth.SignActivity
import com.tkip.mycode.dialog.alert.MyAlert
import com.tkip.mycode.funs.common.OnBasic
import com.tkip.mycode.funs.common.OnSystem
import com.tkip.mycode.funs.common.lllogD
import com.tkip.mycode.funs.common.lllogI
import com.tkip.mycode.init.base.Sred
import com.tkip.mycode.init.my_bind.BaseActivity
import com.tkip.mycode.model.board.OnBoard

class SplashActivity : BaseActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        lllogI("mycode SplashActivity start")


        if (Sred.INITIALIZED.sredBoolean(false)) {
            /*** 최초 아님 : 기본 보드 리셋 */
            OnBoard.resetBasicBoard()
            OnBoard.resetFleaRoomBoard()
            init()
        } else {
            /*** 최초실행 : 기본 보드 초기 셋팅 */
            Sred.INITIALIZED.sredPutBoolean(true)
            lllogD("SplashActivity INITIALIZED false")

            /**         * 기본 게시판 셋팅         */
            OnSystem.initBoard(
                    onSuccess = { init() },
                    onFail = { OnSystem.checkElasticAuth(this@SplashActivity) })

        }

    }

    private fun init() {
        /** 업데이트 버전 체크 */
        if (OnBasic.isLatestVersion()) start()
        else MyAlert.showUpdate(this@SplashActivity,
                onOk = { OnBasic.gotoGoogleStore(this@SplashActivity) },
                onForceCancel = { finish() },
                onCancel = { start() })
    }

    private fun start() {
        val currentUser = FirebaseAuth.getInstance().currentUser
        lllogD("SplashActivity currentUser $currentUser")

        if (currentUser == null) OnSign.gotoSignActivityAndFinish(this@SplashActivity, SignActivity.TYPE_NORMAL, true)
//        if (currentUser == null) OnAuthUi.gotoAuthUiActivityAndFinish(this@SplashActivity, AuthUiActivity.SIGN_NEW, true)
        else {
            val intent = Intent(this, MainActivity::class.java)
//            intent.putExtra(PUT_UID, currentUser.uid)
//            intent.putExtra(PUT_INT, 2)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
            startActivity(intent)
            finish()
        }


    }


}
