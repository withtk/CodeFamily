//package com.tkip.mycode.init.activity_result
//
//import android.app.Activity
//import android.content.Context
//import android.content.Intent
//import androidx.activity.result.contract.ActivityResultContract
//import com.tkip.mycode.nav.post.PostActivity
//
//class PostActivityContract : ActivityResultContract<Int, String?>() {
//
//    override fun createIntent(context: Context, input: Int): Intent {
//        return Intent(context, PostActivity::class.java).apply {
//            putExtra(PostActivity.ID, postId)
//        }
//    }
//
//    override fun parseResult(resultCode: Int, intent: Intent?): String? {
//        val data = intent?.getStringExtra(PostActivity.TITLE)
//        return if (resultCode == Activity.RESULT_OK && data != null) data
//        else null
//    }
//}