package com.tkip.mycode.init.base

import android.content.Context
import android.content.SharedPreferences
import androidx.preference.PreferenceManager
import com.cunoraz.tagview.TagView
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import com.tkip.mycode.funs.common.lllogM
import com.tkip.mycode.model.board.Board
import com.tkip.mycode.model.manage.Notice
import com.tkip.mycode.model.manage.SystemMan
import com.tkip.mycode.model.peer.Peer


object Sred {

    const val PEER = "PEER"
    const val PEER_REAL = "PEER_REAL"
    const val INITIALIZED = "INITIALIZED"   // 설치후 최초 앱 기동시 한번만 check
    const val BOARD_NO1 = "BOARD_NO1"
    const val BOARD_RECENT = "BOARD_RECENT"
//    const val BOARD_FLEA = "BOARD_FLEA"
//    const val BOARD_ROOM = "BOARD_ROOM"
//    const val SYSTEM_MANAGE = "SYSTEM_MANAGE"
    const val SYSTEM_NOTICE_NO = "SYSTEM_NOTICE_NO"
    const val MY_CURRENT_TAG = "MY_CURRENT_TAG"
    const val PUSH_TIME_SET = "PUSH_TIME_SET"
    const val PUSH_TIME1 = "PUSH_TIME1"   // 금지 시작시간
    const val PUSH_TIME2 = "PUSH_TIME2"   // 금지 종료시간
    const val PUSH_IS_MUTE = "PUSH_TYPE"     // 금지 타입 : 뮤트 or no노티
    const val ARR_KEYWORD = "ARR_KEYWORD"   // 최근 검색 내역
    const val ARR_STORE_KEYWORD = "ARR_TAG_KEYWORD"   // 최근 검색 태그내역

    const val DEVICE_FCM_TOKEN = "DEVICE_FCM_TOKEN"

    private var pref: SharedPreferences? = null

    fun init(context: Context) {
        //        sharedPref = context.getSharedPreferences(DEFAULT_SETTING, context.MODE_PRIVATE);
        pref = PreferenceManager.getDefaultSharedPreferences(context)
    }


    fun putString(_key: String, _value: String) {
        lllogM("Sred putString  _value : $_value")

        pref?.let {

            val e = it.edit()
            e.putString(_key, _value)
            e.apply()
        }
    }

    fun getString(_key: String): String {
        return pref?.getString(_key, "") ?: ""
    }


    fun putBoolean(_key: String, _value: Boolean) {
        lllogM("Sred putBoolean  _value : $_value")

        pref?.let {
            val e = it.edit()
            e.putBoolean(_key, _value)
            e.apply()
        }
    }

    fun getBoolean(_key: String, _value: Boolean): Boolean {
        return pref?.getBoolean(_key, _value) ?: false
    }

    fun putInt(_key: String, _value: Int) {
        lllogM("Sred putInt  _value : $_value")

        pref?.let {
            val e = it.edit()
            e.putInt(_key, _value)
            e.apply()
        }
    }

    fun getInt(_key: String, _value: Int): Int {
        return pref?.getInt(_key, _value) ?: 0
    }

    fun putLong(_key: String, _value: Long) {
        lllogM("Sred putLong  _value : $_value")

        pref?.let {
            val e = it.edit()
            e.putLong(_key, _value)
            e.apply()
        }
    }

    fun getLong(_key: String, _value: Long): Long? {
        return pref?.getLong(_key, _value)
    }

    fun clear() {
        pref?.let {
            val e = it.edit()
            e.clear()
            e.apply()
        }
    }




    /********************************************
     *********** object PEER **************************
     *********************************************/
    fun putObjectPeer(_key: String, _value: Peer?) {
//        lllogM("Sred putObjectPeer _value $_value")
        pref?.let {
            if (_value == null) {
                val e = it.edit()
                e.putString(_key, null)
                e.apply()
            } else {
                val e = it.edit()
                val json = Gson().toJson(_value)
                e.putString(_key, json)
                e.apply()
            }

        }
    }

    fun getObjectPeer(key: String): Peer {
        pref?.let {
            val json = it.getString(key, null)
            if (json == null) {
                return Peer()
            } else {
                return Gson().fromJson<Peer>(json, Peer::class.java)
            }
        }
        return Peer()
    }

    /********************************************
     *********** object Notice **************************
     *********************************************/
    fun putObjectNotice(_key: String, _value: Notice?) {
//        lllogM("Sred putObjectNotice _value $_value")

        pref?.let {
            if (_value == null) {
                val e = it.edit()
                e.putString(_key, null)
                e.apply()
            } else {
                val e = it.edit()
                val json = Gson().toJson(_value)
                e.putString(_key, json)
                e.apply()
            }
        }
    }

    fun getObjectNotice(key: String): Notice? {
        pref?.let {
            it.getString(key, null)?.let { json ->
                return Gson().fromJson<Notice>(json, Notice::class.java)
            } ?: return null
        }
        return null
    }

    /********************************************
     *********** object Board **************************
     *********************************************/
    fun putObjectBoard(_key: String, _value: Board?) {
//        lllogM("Sred putObjectBoard _value $_value")

        pref?.let {
            if (_value == null) {
                val e = it.edit()
                e.putString(_key, null)
                e.apply()
            } else {
                val e = it.edit()
                val json = Gson().toJson(_value)
                e.putString(_key, json)
                e.apply()
            }

        }
    }

    fun getObjectBoard(key: String): Board? {
        pref?.let {
            lllogM("getObjectBoard key $key")
            val json = it.getString(key, null)
            return if (json == null) null
            else Gson().fromJson<Board>(json, Board::class.java)
        }
        return null
    }


    /********************************************
     *********** object SystemManage **************************
     *********************************************/
    fun putObjectSystemManage(_key: String, _value: SystemMan?) {
        pref?.let {
            if (_value == null) {
                val e = it.edit()
                e.putString(_key, null)
                e.apply()
            } else {
                val e = it.edit()
                val json = Gson().toJson(_value)
                e.putString(_key, json)
                e.apply()
            }

        }
    }

    fun getObjectSystemManage(key: String): SystemMan? {
        pref?.let {
            val json = it.getString(key, null)
            return if (json == null) null
            else Gson().fromJson<SystemMan>(json, SystemMan::class.java)
        }
        return null
    }

    /********************************************
     *********** String set **************************
     *********************************************/

    fun putStringSet(_key: String, set: Set<String>) {
//        lllogM("Sred putStringSet set $set")

        pref?.let {
            val e = it.edit()
            e.putStringSet(_key, set)
            e.apply()
        }
    }

    fun getStringSet(_key: String): Set<String>? {
        return pref?.getStringSet(_key, null)
    }

    fun removeSet(_key: String) {
        pref?.let {
            val e = it.edit()
            e.remove(_key)
            e.apply()
        }
    }

    fun saveArray(_key: String, list: ArrayList<String>) {
//        lllogM("Sred saveArray list.size ${list.size}")

        val json = Gson().toJson(list)
        putString(_key, json)
    }

    fun loadArray(_key: String): ArrayList<String>? {
        val json = getString(_key)
        return fromJson(json)
    }

    private inline fun <reified T> fromJson(json: String): T {
        return Gson().fromJson(json, object : TypeToken<T>() {}.type)
    }

    /* fun putArrayList(_key: String, list: ArrayList<*>) {
         pref?.let {
             val e = it.edit()

             val a = JSONArray()

             for (i in 0 until list.size) {
                 a.put(list[i])
             }

             if (list.isNotEmpty()) {
                 e.putString(_key, a.toString())
             }
             e.apply()


             // 방법1 Member[] array = gson.fromJson(jsonString, Member[].class);
             // List<Member> list = Arrays.asList(array);
             // 방법2 List<Member> list2 = gson.fromJson(jsonString, new TypeToken<List<Member>>(){}.getNo());

         }
     }

     fun getArrayList(_key: String, obj: Any): Peer? {
         pref?.let {


             it.getString(_key, null)?.let { json ->

                 val list = ArrayList<Any>()
                 try {
                     val a = JSONArray(json)
                     for (i in 0 until a.length()) {
                         val url = a.optString(i)
                         list.add(url)
                     }
                 } catch (e: JSONException) {
                     e.printStackTrace()
                 }
             }

             val gson = Gson()
             val json = it.getString(_key, null)
             return gson.fromJson(json, Peer::class.java)
         }
         return null
     }*/

    /************************************
     ************************************/
    fun resetMyTagLoc(tagView: TagView): Set<String> {
        //  전체 리셋하여 add.
        val set = hashSetOf<String>()
        for (tag in tagView.tags) {
            set.add(tag.text)
        }
        putStringSet(MY_CURRENT_TAG, set)
        return set
    }

}