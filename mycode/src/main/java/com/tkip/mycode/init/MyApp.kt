package com.tkip.mycode.init

import android.app.Application
import android.content.Context
import com.tkip.mycode.funs.common.lllogI
import com.tkip.mycode.init.part.CodeApp


class MyApp  : Application() {

    init {
        instance = this
    }

    companion object {
        private var instance: MyApp? = null

        fun getAppContext(): Context {
            lllogI("MyApp onCreate getAppContext")

            return CodeApp.getAppContext()
        }
//
//        fun getAppContext(): MyApp {
//            return instance?:MyApp()
//        }

    }


/*
    companion object {

        private var instance: MyApp? = null

        val init: Context
            get() {
                if (instance == null) {
                    instance = MyApp()
                }
                return instance as MyApp
            }

    }

    init {
        instance = this
    }*/

//    var db: FirebaseFirestore? = null

    override fun onCreate() {
        super.onCreate()
        lllogI("MyApp onCreate start")

//        init()
    }

//    private fun init() {
//        Fabric.with(this, Crashlytics())
//        //        val fabric = Fabric.Builder(this)
//        //                .kits(Crashlytics())
//        //                .debuggable(true)
//        //                .build()
//        //        Fabric.with(fabric)
//
//        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
//            NotiChannelManager.createGroupChannel(applicationContext)     // NotiManager  setup
//        }
//
//
//        /** peer 초기화 */
//        Sred.init(applicationContext)                              // Sred  setup
//        val realPeer = Sred.getObjectPeer(Sred.PEER_REAL)
//        lllogI("MyApp realPeer.uid: " + realPeer.uid)
//        OnPeer.setUpPeer(realPeer)  // 앱 실행때마다
//        OnPeer.peer = realPeer
//
//
//        val settings = FirebaseFirestoreSettings.Builder()         // Firee Settings  setup
//            .setPersistenceEnabled(true)
//            .build()
//        //        db = FirebaseFirestore.getInstance()                       // Firee  setup
//        FirebaseFirestore.getInstance().firestoreSettings = settings   // Firee  setup
//
//        initRemoteConfig()                                         // RemoteConfig  setup
//
//        /**         * 기본 게시판 셋팅         */
//        Board.setDefaultBoard()
//
//        //        /** retrofit2 header 초기화 */
//        //        Cons.headerMap["Authorization"] = Credentials.basic(getString(R.string.elastic_user), getString(R.string.elastic_pw))
//
//        Places.initialize(applicationContext, getString(R.string.my_google_maps_api_key))
//
//        /**refreshLayout*/
//        SmartRefreshLayout.setDefaultRefreshHeaderCreator { context, layout ->
//            layout.setPrimaryColorsId(R.color.colorAccent, R.color.white) // 테마 색을 전체적으로 설정 하거나
//    //                    .SetTimeFormat (new DynamicTimeFormat ( "% s" 업데이트) //클래식 헤드로 지정, 기본값은 베지어 입니다
//            return@setDefaultRefreshHeaderCreator ClassicsHeader(context)
//        }
//        SmartRefreshLayout.setDefaultRefreshFooterCreator { context, _ ->
//            return@setDefaultRefreshFooterCreator ClassicsFooter(context).setDrawableSize(20f)
//        }
//    }
//
//    private fun initRemoteConfig() {
//        lllogI("MyApp initRemoteConfig start")
//
//        val remoteConfig = FirebaseRemoteConfig.getInstance()
//        val configSettings = FirebaseRemoteConfigSettings.Builder()
//                .setDeveloperModeEnabled(BuildConfig.DEBUG)
////                .setMinimumFetchIntervalInSeconds(4200)
//                .build()
//
//        /* long cacheExpiration = 3600; // 1 hour in seconds.
//        // If your app is using developer mode, cacheExpiration is set id 0, so each fetch will
//        // retrieve values from the service.
//        if (remoteConfig.getInfo().getConfigSettings().isDeveloperModeEnabled()) {
//            cacheExpiration = 0;
//        }*/
//
//        remoteConfig.setConfigSettingsAsync(configSettings)
//        remoteConfig.setDefaults(R.xml.remote_config_defaults)
//
//        remoteConfig.fetch(if (BuildConfig.DEBUG) 0 else TimeUnit.MINUTES.toSeconds(10))
//                .addOnCompleteListener { task ->
//
//                    if (task.isSuccessful) {
//                        remoteConfig.activate()
//                    } else {
//                        TToast.showWarning(R.string.db_error_remote_config_get_data)
//                    }
//
//                    // --------------------remote data 정리-------------------------
////                    val lastestVersion = remoteConfig.getString("latest_version")
//
//                    LIMIT_UPLOAD_COUNT = remoteConfig.getLong("limit_upload_count").toInt()
//                    WIDTH_THUMB = remoteConfig.getLong("width_thumb").toInt()
//                    WIDTH_MID = remoteConfig.getLong("width_mid").toInt()
//
//
//                }
//                .addOnFailureListener { e -> lllogW(e) }


//    }


}
