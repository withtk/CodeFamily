package com.tkip.mycode.init.my_bind

import android.Manifest
import android.app.Activity
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.os.Bundle
import android.util.DisplayMetrics
import android.view.LayoutInflater
import android.view.WindowManager
import androidx.annotation.LayoutRes
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.fragment.app.DialogFragment
import com.tkip.mycode.funs.common.OnPermission
import com.tkip.mycode.init.REQ_PICK
import com.tkip.mycode.init.REQ_READ_EXSTORAGE
import com.tkip.mycode.util.lib.photo.view.LinearAddPhotoOne
import com.tkip.mycode.util.tools.etc.OnMyClick


/**
 * 기본 다이얼로그
 */
abstract class BindingDialog<T : ViewDataBinding> : DialogFragment() {
    @LayoutRes
    abstract fun getLayoutResId(): Int

    protected lateinit var b: T
        private set

    protected abstract fun getSize(): Pair<Float?, Float?>

    lateinit var mContext: Context
    override fun onAttach(context: Context) {
        super.onAttach(context)
        mContext = context
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        b = DataBindingUtil.inflate(LayoutInflater.from(activity), getLayoutResId(), null, false)
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        when (requestCode) {
            REQ_READ_EXSTORAGE ->
                // 다시 들어가서 체크하고 method 실행
                OnPermission.onOpenGallery(activity as Activity, grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED, false)
        }
    }

    var viewAddPhotoOne: LinearAddPhotoOne? = null    // 사진 업로드 1장
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == Activity.RESULT_OK)
            when (requestCode) {
                REQ_PICK -> data?.data?.let { uri -> viewAddPhotoOne?.setImageUri(uri) }
            }
    }

    /***********************************************************************************
     ****************************** onClick ********************************************
     ***********************************************************************************/
    fun onClickOpenGallery() {
//        OnMyClick.setDisableAWhileBTN(v)
        if (OnMyClick.isDoubleClick()) return

//        OnPermission.onOpenGallery(true, activity as AppCompatActivity)
        val readPermission = ContextCompat.checkSelfPermission(mContext, Manifest.permission.READ_EXTERNAL_STORAGE)
        if (readPermission == PackageManager.PERMISSION_GRANTED) startActivityForResult(OnPermission.intentPickSingle(), REQ_PICK)
        else ActivityCompat.requestPermissions(activity as AppCompatActivity, arrayOf(Manifest.permission.READ_EXTERNAL_STORAGE), REQ_READ_EXSTORAGE)
    }


    override fun onResume() {

        // Get screen width and height in pixels
        val displayMetrics = DisplayMetrics()
        activity?.windowManager?.defaultDisplay?.getMetrics(displayMetrics)
        val displayWidth = displayMetrics.widthPixels
        val displayHeight = displayMetrics.heightPixels

        // Initialize a new window manager layout parameters
        val layoutParams = WindowManager.LayoutParams()
        // Copy the alert dialog window attributes id new layout parameter instance
        layoutParams.copyFrom(dialog?.window?.attributes)


        layoutParams.width = WindowManager.LayoutParams.WRAP_CONTENT
        layoutParams.height = WindowManager.LayoutParams.WRAP_CONTENT

        getSize().first?.let {
            layoutParams.width = (displayWidth * it).toInt()
        }

        getSize().second?.let {
            layoutParams.height = (displayWidth * it).toInt()
        }


        /*
          if (getSize().first) {   // true = hight WRAP_CONTENT
              layoutParams.width = (displayWidth * getSize().first).toInt()
              layoutParams.height = WindowManager.LayoutParams.WRAP_CONTENT
          } else {
              layoutParams.width = (displayWidth * getSize().first).toInt()
              layoutParams.height = (displayHeight * getSize().second).toInt()
              // Apply the newly created layout parameters id the alert dialog window
          }*/

        dialog?.window?.attributes = layoutParams

        super.onResume()
    }
}