package com.tkip.mycode.init.part

import android.app.Activity
import android.content.Context
import android.graphics.drawable.Drawable
import android.os.Build
import com.google.android.libraries.places.api.Places
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.firestore.FirebaseFirestoreSettings
import com.scwang.smartrefresh.layout.SmartRefreshLayout
import com.scwang.smartrefresh.layout.footer.ClassicsFooter
import com.scwang.smartrefresh.layout.header.ClassicsHeader
import com.tkip.mycode.R
import com.tkip.mycode.funs.common.initRemoteConfig
import com.tkip.mycode.funs.common.lllogI
import com.tkip.mycode.init.base.Sred
import com.tkip.mycode.init.inter.OnExitInterface
import com.tkip.mycode.model.peer.OnPeer
import com.tkip.mycode.util.system.notification.NotiChannelManager

class CodeApp(private val myContext: Context) {


    init {
        instance = myContext
        lllogI("MyAPP OnApp onCreate init")

        initView()
    }

    companion object {
        private lateinit var instance: Context

        fun getAppContext(): Context {
            return instance
        }

        var versionCode: Int = 0
        var versionName: String = "1.0"
        var clientId: String = ""
        var icon: Drawable? = null
        var isManageApp = false

        var curActivity: Activity? = null   // 현재 Context
        var curModelKey: String? = null  // for Push  현재 보고있는 페이지의 알림은 안 울림.

        var esId:String? = null
        var esPw:String? = null


        var interExit : OnExitInterface? = null


    }


    private fun initView() {
        lllogI("MyAPP OnApp onCreate")

        /**   Notification 채널 초기화  */
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) NotiChannelManager.createGroupChannel(myContext)

        /** peer 초기화 */
        Sred.init(myContext)                              // Sred  setup
        val realPeer = Sred.getObjectPeer(Sred.PEER_REAL)
        lllogI("MyApp realPeer.uid: " + realPeer.uid)
        OnPeer.setUpPeer(realPeer)  // 앱 실행때마다
        OnPeer.peer = realPeer

        /*** FireStore 초기화 */
        val settings = FirebaseFirestoreSettings.Builder()         // Firee Settings  setup
                .setPersistenceEnabled(true)
                .build()
        //        db = FirebaseFirestore.getInstance()                       // Firee  setup
        FirebaseFirestore.getInstance().firestoreSettings = settings   // Firee  setup

        /*** RemoteConfig 초기화 */
        initRemoteConfig()                                         // RemoteConfig  setup

        /**         * 기본 게시판 셋팅         */
//        resetSystemManage()
//        OnSystem.initBoard(null)

        /**         * 구글맵 Places 초기화         */
        Places.initialize(myContext, myContext.getString(R.string.my_google_maps_api_key))

        /**  refreshLayout 기본 뷰 설정  */
        SmartRefreshLayout.setDefaultRefreshHeaderCreator { context, layout ->
            layout.setPrimaryColorsId(R.color.colorAccent, R.color.colorLight) // 테마 색을 전체적으로 설정 하거나
            //                    .SetTimeFormat (new DynamicTimeFormat ( "% s" 업데이트) //클래식 헤드로 지정, 기본값은 베지어 입니다
            return@setDefaultRefreshHeaderCreator ClassicsHeader(context)
        }
        SmartRefreshLayout.setDefaultRefreshFooterCreator { context, _ ->
            return@setDefaultRefreshFooterCreator ClassicsFooter(context).setDrawableSize(20f)
        }


    }


}