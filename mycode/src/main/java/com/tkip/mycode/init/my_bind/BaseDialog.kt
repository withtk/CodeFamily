package com.tkip.mycode.init.my_bind

import android.content.Context
import android.util.DisplayMetrics
import android.view.WindowManager
import androidx.fragment.app.DialogFragment

abstract class BaseDialog : DialogFragment() {

    protected abstract fun getSize(): Pair< Float?, Float?>

    lateinit var mContext: Context
    override fun onAttach(context: Context) {
        super.onAttach(context)
        mContext = context
    }
    override fun onResume() {

        // Get screen width and height in pixels
        val displayMetrics = DisplayMetrics()
        activity?.windowManager?.defaultDisplay?.getMetrics(displayMetrics)
        val displayWidth = displayMetrics.widthPixels
        val displayHeight = displayMetrics.heightPixels

        // Initialize a new window manager layout parameters
        val layoutParams = WindowManager.LayoutParams()
        // Copy the alert dialog window attributes id new layout parameter instance
        layoutParams.copyFrom(dialog?.window?.attributes)


        layoutParams.width = WindowManager.LayoutParams.WRAP_CONTENT
        layoutParams.height = WindowManager.LayoutParams.WRAP_CONTENT

        getSize().first?.let {
            layoutParams.width = (displayWidth * it).toInt()
        }

        getSize().second?.let {
            layoutParams.height = (displayWidth * it).toInt()
        }


      /*
        if (getSize().first) {   // true = hight WRAP_CONTENT
            layoutParams.width = (displayWidth * getSize().first).toInt()
            layoutParams.height = WindowManager.LayoutParams.WRAP_CONTENT
        } else {
            layoutParams.width = (displayWidth * getSize().first).toInt()
            layoutParams.height = (displayHeight * getSize().second).toInt()
            // Apply the newly created layout parameters id the alert dialog window
        }*/

        dialog?.window?.attributes = layoutParams

        super.onResume()
    }
}