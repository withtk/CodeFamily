package com.tkip.mycode.init.base

import android.content.res.ColorStateList
import android.graphics.Bitmap
import android.net.Uri
import android.text.TextUtils
import android.view.View
import android.widget.*
import androidx.cardview.widget.CardView
import androidx.core.content.ContextCompat
import androidx.databinding.BindingAdapter
import com.cunoraz.tagview.TagView
import com.google.android.gms.maps.model.LatLng
import com.tkip.mycode.R
import com.tkip.mycode.ad.BannerType
import com.tkip.mycode.ad.getAdtype
import com.tkip.mycode.ad.getBannerType
import com.tkip.mycode.admin.menu.AMenu
import com.tkip.mycode.admin.versionGteQ
import com.tkip.mycode.funs.common.*
import com.tkip.mycode.init.ACTION_UPDATE
import com.tkip.mycode.model.bid.Bid
import com.tkip.mycode.model.board.BoardType
import com.tkip.mycode.model.board.getBoardType
import com.tkip.mycode.model.grant.Grant
import com.tkip.mycode.model.grant.OnGrant
import com.tkip.mycode.model.media.Media
import com.tkip.mycode.model.my_enum.*
import com.tkip.mycode.model.mytag.*
import com.tkip.mycode.model.peer.*
import com.tkip.mycode.model.reply.Reply
import com.tkip.mycode.model.store.Store
import com.tkip.mycode.model.ticket.Ticket
import com.tkip.mycode.model.ticket.billEvent
import com.tkip.mycode.nav.lobby.chat.Chat
import com.tkip.mycode.nav.store.add.MyMedia
import com.tkip.mycode.nav.store.add.getMediaHint
import com.tkip.mycode.nav.store.add.getMediaName
import com.tkip.mycode.nav.store.add.getMediaResId
import com.tkip.mycode.util.lib.map.getDistanceFromCurrentLoc
import com.tkip.mycode.util.lib.photo.Foto
import com.tkip.mycode.util.lib.photo.MyPhoto
import com.tkip.mycode.util.lib.photo.glide.*
import com.tkip.mycode.util.tools.anim.AnimUtil
import com.tkip.mycode.util.tools.anim.gone
import com.tkip.mycode.util.tools.anim.showOverUp
import com.tkip.mycode.util.tools.anim.visi
import com.tkip.mycode.util.tools.date.*
import java.text.DecimalFormat
import java.util.concurrent.TimeUnit


object MyBinding {


    @JvmStatic
    @BindingAdapter("android:setMyTags1")
    fun TagView.setAllTags1(list: ArrayList<String>?) {
        list?.let { this.setAll(TAG_1, it) }

    }

    @JvmStatic
    @BindingAdapter("android:setMyTags2")
    fun TagView.setAllTags2(list: ArrayList<String>?) {
        list?.let { this.setAll(TAG_2, it) }

    }

    @JvmStatic
    @BindingAdapter("android:setMyTags3")
    fun TagView.setAllTags3(list: ArrayList<String>?) {
        list?.let { this.setAll(TAG_3, it) }
    }

    /**************************************************************************************
     ****  bdtextview
     *************************************************************************************/

    @JvmStatic
    @BindingAdapter("android:text_divide_line")
    fun RadioButton.textDivideLine(str: String?) {
        str?.let { text = it.replace(" ", "\n") }
    }

    /**     * act 가격 표시     */
    @JvmStatic
    @BindingAdapter("android:act_price")
    fun TextView.actPrice(actTypeNo: Int?) {
        actTypeNo?.actType()?.let { actType ->
            this.textComma(actType.getCrdAmount())
            actType.payType().getIcon()?.let { setCompoundDrawablesWithIntrinsicBounds(it, null, null, null) }
        }
    }

    @JvmStatic
    @BindingAdapter("android:text_area_ex", "android:text_area_total")
    fun TextView.textAreaExTotal(areaEx: Int?, areaTotal: Int?) {
        areaEx?.let {
            areaTotal?.let {
                val result = "$areaEx / $areaTotal"
                text = result
            }
        }
    }

    @JvmStatic
    @BindingAdapter("android:text_thousand")
    fun TextView.textThousand(price: Int?) {
        price?.let {
            text = price.getStringComma()
        }
    }

    /**     * 부동산 거래종류 set: text, color     */
    @JvmStatic
    @BindingAdapter("android:text_contract_type_title_color")
    fun TextView.textContractTypeTitleColor(no: Int?) {
        no?.let {
            it.getContractType().let { type ->
                text = type.title
                setBackgroundResource(type.colorId)
            }
        }
    }

    /**     * 부동산 거래종류  set: text */
    @JvmStatic
    @BindingAdapter("android:text_contract_type_title")
    fun TextView.textContractTypeTitle(no: Int?) {
        no?.let { it.getContractType().let { type -> text = type.title } }
    }

    /**     * 부동산 거래종류의 가격의 명칭 : 매매가, 임대료.     */
    @JvmStatic
    @BindingAdapter("android:text_contract_type_price")
    fun TextView.textContractTypePrice(no: Int?) {
        no?.let { it.getContractType().apply { text = priceTitle } }
    }

    /**     * 부동산 종류  set: text, color*/
    @JvmStatic
    @BindingAdapter("android:text_room_type_title_color")
    fun TextView.textRoomTypeTitleColor(no: Int?) {
        no?.let {
            it.getRoomType().let { type ->
                text = type.title
                setBackgroundColor(type.colorId.getClr())
            }
        }
    }

    /**     * 부동산 종류  set: text   */
    @JvmStatic
    @BindingAdapter("android:text_room_type_title")
    fun TextView.textRoomTypeTitle(no: Int?) {
        no?.let { it.getRoomType().let { type -> text = type.title } }
    }

    @JvmStatic
    @BindingAdapter("android:text_room_status_type_title")
    fun TextView.textRoomStatusTypeTitle(no: Int?) {
        no?.let {
            it.getRoomStatus().apply {
                //                val result = "${R.string.name_type_room_status.getStr()} : $title"
                text = title
            }
        }
    }


    @JvmStatic
    @BindingAdapter("text_construction_date")
    fun textConstructionDate(tv: TextView, yearMonth: Int?) {
        yearMonth?.let {
            val dateStr = it.toString()
            val year = dateStr.substring(0, 4)
            val month = dateStr.substring(4, dateStr.length)
            val result = String.format(R.string.construction_date.getStr(), year, month)
            tv.text = result
        }
    }

    @JvmStatic
    @BindingAdapter("text_room_count")
    fun TextView.textRoomCount(count: Int?) {
        count?.let {
            text = it.toString()
            visi()
        } ?: let { text = "0" }
    }

    @JvmStatic
    @BindingAdapter("android:text_nick_rereply")
    fun TextView.textNickRereply(nick: String?) {
        nick?.let {
            val str = it + R.string.rereply_nick.getStr()
            text = str
        }
    }

    @JvmStatic
    @BindingAdapter("android:text_view_count")
    fun TextView.text_view_count(count: Int?) {
        text = count.toString()
    }


    @JvmStatic
    @BindingAdapter("android:text_email_myroom")
    fun TextView.text_email_myroom(peer: Peer?) {
        peer?.let {
            if (OnPeer.peer.type.lt(PeerType.BASIC_2.no)) {   // 익명로그인 일때
                text = context.getString(R.string.info_current_login_anonymous)
                background = ContextCompat.getDrawable(context, R.drawable.shape_corner_10_alpha)
                setPadding(30, 5, 30, 5)
                isClickable = true
                isFocusable = true
            } else {
                text = it.email
            }
        }
    }


    @JvmStatic
    @BindingAdapter("android:alpha_rank")
    fun View.alphaRank(rank: Boolean?) {
        rank?.let { alpha = if (rank) 1f else 0.5f }
    }


    @JvmStatic
    @BindingAdapter("android:alpha_bid")
    fun View.alphaBid(statusNo: Int?) {
        statusNo?.let {
            alpha = if (it < Status.BID_BIDDING.no) 0.5f else 1f
        }
    }

    /**
     * true = alpha 0.5
     * false = alpha 1
     */
    @JvmStatic
    @BindingAdapter("android:alpha_5")
    fun View.alpha5(bool: Boolean?) {
        alpha = if (bool == true) 0.5f else 1f
        lllogD("alpha5Grant alpha5 alpha $alpha")
    }

    /**
     * NORMAL = alpha 1
     */
    @JvmStatic
    @BindingAdapter("android:alpha_5_not_normal")
    fun View.alpha5NotNormal(statusNo: Int?) {
        statusNo?.let {
            if (it==Status.NORMAL.no) 1f else 0.5f
        }
    }

    @JvmStatic
    @BindingAdapter("android:alpha_5_grant")
    fun View.alpha5Grant(statusNo: Int?) {
        lllogD("alpha5Grant statusNo $statusNo")
        statusNo?.let {
            alpha5(it.gte(Status.GRANT_HANDLED.no))
        }
    }


    /**************************************************************************************
     ****  bdDateformat
     *************************************************************************************/
    @JvmStatic
    @BindingAdapter("android:text_date_basic")
    fun TextView.text_date_basic(longDate: Long?) {
        longDate?.let {
            this.text = it.getLocalTimeString(FORMAT_BASIC)
        }
    }

    @JvmStatic
    @BindingAdapter("android:text_date_summary")
    fun TextView.text_date_summary(longDate: Long?) {
        longDate?.let {
            this.text = it.getLocalTimeString(FORMAT_SUMMARY)
        }
    }

    @JvmStatic
    @BindingAdapter("android:text_date_year")
    fun TextView.text_date_year(longDate: Long?) {
        longDate?.let {
            this.text = it.getLocalTimeString(FORMAT_YEAR)
        }
    }

    @JvmStatic
    @BindingAdapter("android:text_date8_month_date")
    fun TextView.textDate8MonthDate(hitDate: Int?) {
        hitDate?.let {
            val str = it.toString()
            val monthDate = "${R.string.title_hit_date.getStr()} ${str.substring(4, 6)}${R.string.str_month.getStr()} ${str.substring(6, 8)}${R.string.str_date.getStr()}"
            this.text = monthDate
        }
    }

    @JvmStatic
    @BindingAdapter("android:text_date_summary")
    fun TextView.text_date_summary_long(dateList: ArrayList<Long>?) {
        dateList?.let {
            if (dateList.isNotEmpty())
                this.text = dateList[dateList.size - 1].getLocalTimeString(FORMAT_SUMMARY)
        }
    }

    /**
     * 경과시간 글자로 표시.
     */
    @JvmStatic
    @BindingAdapter("android:text_date_elapsed_letter")
    fun TextView.textDateElapsedLetter(longDate: Long?) {
        longDate?.let {
            this.text = it.getElapsedTimeString()
        }
    }

    @JvmStatic
    @BindingAdapter("android:text_date_elapsed_short")
    fun TextView.textDateElapsedShort(longDate: Long?) {
        longDate?.let {
            val pair = it.getElapsedTimeStringShort()
            this.text = pair.first
            pair.second?.let { colorID -> setTextColor(colorID.getClr()) }
        }
    }

    @JvmStatic
    @BindingAdapter("android:text_date_time")
    fun TextView.text_date_time(longDate: Long?) {
        longDate?.let { setLocaleDateString(it, FORMAT_TIME) } ?: let { gone() }
    }

    @JvmStatic
    @BindingAdapter("android:text_chat_date_time")
    fun TextView.textChatDateTime(longDate: Long?) {
        longDate?.let {
            val elapsedSeconds = (System.currentTimeMillis() - it) / 1000
            text = if (elapsedSeconds < TimeUnit.HOURS.toSeconds(1)) {
                it.getElapsedTimeString()
            } else {
                it.getLocalTimeString(FORMAT_TIME)
            }
        }
    }

    @JvmStatic
    @BindingAdapter("android:text_date_year_month")
    fun TextView.text_date_year_month(longDate: Long?) {
        longDate?.let { setLocaleDateString(it, FORMAT_YEAR_MONTH_DATE) }
                ?: let { gone() }
    }

    @JvmStatic
    @BindingAdapter("android:text_date_month")
    fun TextView.text_date_month(longDate: Long?) {
        longDate?.let { setLocaleDateString(it, FORMAT_MONTH) } ?: let { gone() }
    }

    @JvmStatic
    @BindingAdapter("android:text_date_month_time")
    fun TextView.getTextDateMonthTime(longDate: Long?) {
        longDate?.let { setLocaleDateString(it, FORMAT_DATE_TIME) }
                ?: let { gone() }
    }

    @JvmStatic
    @BindingAdapter("android:text_date_month", "android:pre_reply")
    fun TextView.text_date_month_pre_reply(longDate: Long?, preReply: Reply?) {
        val currentDate = longDate?.getLocalTimeString(FORMAT_MONTH)
        val preDate = preReply?.addDate?.getLocalTimeString(FORMAT_MONTH)

        if (currentDate == preDate) {
            gone()
        } else {
            visi()
            text = currentDate
        }
    }

    @JvmStatic
    @BindingAdapter("android:text_date_month", "android:pre_chat")
    fun TextView.textDateMonthPreChat(longDate: Long?, preChat: Chat?) {
        val currentDate = longDate?.getLocalTimeString(FORMAT_MONTH)
        val preDate = preChat?.addDate?.getLocalTimeString(FORMAT_MONTH)

        if (currentDate == preDate) {
            gone()
        } else {
            visi()
            text = currentDate
        }
    }


    /**************************************************************************************
     ****  bdimageview
     *************************************************************************************/


    @JvmStatic
    @BindingAdapter("android:src_drawable")
    fun ImageView.srcDrawable(resId: Int?) {
        resId?.let { setImageDrawable(it.getDrawable()) }
    }

    @JvmStatic
    @BindingAdapter("android:src_pay_type")
    fun ImageView.srcPayType(payTypeNo: Int?) {
        payTypeNo?.let {
            setImageDrawable(it.payType().getIcon())
        }
    }

    @JvmStatic
    @BindingAdapter("android:src_thumb_myphoto")
    fun ImageView.src_thumb_myphoto(myPhoto: MyPhoto?) {
        myPhoto?.let {
            if (versionGteQ) {
                it.bitmap?.let { bitmap -> setImageBitmap(bitmap) }
            } else {
                it.thumbUri?.let { thumbUri -> setImageURI(thumbUri) }
                it.orientation?.let { orientation -> rotation = java.lang.Float.parseFloat(orientation) }
//            AnimUtil.showSlideUp(this,true)
            }
        }
    }


    @JvmStatic
    @BindingAdapter("android:src_room_thumb")
    fun ImageView.srcRoomThumb(foto: Foto?) {
//        setImageDrawable(R.drawable.ic_image01.getDrawable())
        foto?.let {
            setThumbImageGlide(it)
        } ?: setImageDrawable(R.drawable.ic_no_image.getDrawable())
    }

    @JvmStatic
    @BindingAdapter("android:src_foto_thumb")
    fun ImageView.srcFotoThumb(foto: Foto?) {
        foto?.let {
            when {
                /*** bitmap 체크 먼저 : 이것이 곧 versionQ를 처리하는 방법.*/
                (it.thumbBitmap != null) -> {
                    this.setImageBitmap(it.thumbBitmap)
                    visi()
                }
                (it.thumbUri != null) || (it.thumbUrl != null) -> setThumbImageGlide(it)
                else -> gone()
            }
        } ?: gone()
    }


    @JvmStatic
    @BindingAdapter("android:src_foto_origin")
    fun ImageView.src_foto_origin(foto: Foto?) {
        setOriginImageGlide(foto)
    }


    @JvmStatic
    @BindingAdapter("android:src_url")
    fun srcUrl(iv: ImageView, url: String?) {
        iv.setUrlGlide(url)
    }

    /**
     * 해당 주소에 이미지 없으면 gone
     */
    @JvmStatic
    @BindingAdapter("android:src_url_gone")
    fun srcUrlGone(iv: ImageView, url: String?) {
        iv.setUrlGlideGone(url)
    }


    @JvmStatic
    @BindingAdapter("android:src_url_refresh")
    fun srcUrlRefresh(iv: ImageView, url: String?) {
        iv.setUrlGlide(url)
        hhh(1000) { iv.setUrlGlide(url) }
    }

    @JvmStatic
    @BindingAdapter("android:src_ad_url")
    fun srcAdUrl(iv: ImageView, url: String?) {
        url?.let {
            iv.setUrlGlide(url)
        } ?: let { iv.setImageDrawable(R.drawable.img_blank_ad.getDrawable()) }
    }

    @JvmStatic
    @BindingAdapter("android:src_status")
    fun ImageView.src_status(status: Status?) {
        status?.let {
            setImageResource(status.iconId)
        }
    }


    @JvmStatic
    @BindingAdapter("android:src_face")
    fun srcFace(iv: ImageView, peer: Peer?) {
        iv.setFaceGlide(peer?.faceThumb)
    }

    @JvmStatic
    @BindingAdapter("android:src_face_upload")
    fun srcFaceUpload(iv: ImageView, peer: Peer?) {
        iv.setFaceGlide(peer?.faceThumb)
    }

    @JvmStatic
    @BindingAdapter("android:src_bitmap")
    fun src_bitmap(iv: ImageView, bitmap: Bitmap?) {
        bitmap?.let { iv.setImageBitmap(it) }
    }

    @JvmStatic
    @BindingAdapter("android:src_uri")
    fun srcUri(iv: ImageView, uri: Uri?) {
        uri?.let { iv.setImageURI(it) }
    }


    /**************************************************************************************
     ****  bdlinearlayout   채팅에서 유저사진 보여줄지 말지 결정.
     *************************************************************************************/
    @JvmStatic
    @BindingAdapter("android:is_show_peer")
    fun LinearLayout.is_show_peer(show: Boolean?) {
        if (show == true) visi() else gone()
    }


    @JvmStatic
    @BindingAdapter("android:text_anonymous")
    fun TextView.textAnonymous(string: String?) {
        text = R.string.nick_anonymous.getStr()
        string?.let { text = it }
    }

    /**************************************************************************************
     ****  bdMap
     *************************************************************************************/
/*@JvmStatic
@BindingAdapter("android:text_rate_map")
fun TextView.text_rate_map(store: Store?) {

    store?.let { s ->
        if (s.rating == null) {
            gone()
        } else {
            visi()
            val str = "${s.rating} (${s.userRatingsTotal}" + context.getString(R.string.info_each) + ")"
            this.text = str
        }
    }
}*/



    @JvmStatic
    @BindingAdapter("android:text_type_map")
    fun TextView.testTypeMap(store: Store?) {
        store?.let {
            val buffer = StringBuffer()
//            val list = it.types as ArrayList

//            s.placeTypeList.let {
//                for (type in it) {
//                    buffer.append(type.getTypeStr())
//                    buffer.append(" | ")
//                }
//            }
            this.text = buffer.toString()
            lllogD("text_type_map buffer: $buffer")
        }
    }

    @JvmStatic
    @BindingAdapter("android:text_distance_map")
    fun TextView.textDistanceMap(store: Store?) {
        store?.let { s ->
            val str = LatLng(s.lat!!, s.lng!!).getDistanceFromCurrentLoc()
            this.text = str
        }
    }

    @JvmStatic
    @BindingAdapter("card_color_bid")
    fun CardView.cardColorBid(statusNo: Int?) {
        statusNo?.let {
            val status = it.getStatus()
            if (status == Status.BID_SUCCESS || status == Status.BID_FAILED) this.setCardBackgroundColor(status.colorId.getClr())
            else this.setCardBackgroundColor(R.color.colorPrimary2.getClr())
        }
    }

    /**
     * 스토어 info 전체 세팅.
     */
//    @JvmStatic
//    @BindingAdapter("android:store_info")
//    fun LinearLayout.setStoreInfo(store: Store?) {
//        store?.let { s ->
//            s.comment?.let {
//                val card = ViewExpandableCard(this.context, R.string.add_store_comment.getStr(), it)
//                this.addView(card)
//            }
//            s.contact?.let {
//                val card = ViewExpandableCard(this.context, R.string.add_store_detail.getStr(), it)
//                this.addView(card)
//                /*** 미디어 기입 */
//                s.mediaMap.setupMediaButton(this.context, card.b.lineAttach)
//            }
//
//            s.weekdayText?.let { this.addView(ViewExpandableCard(this.context, R.string.store_working_time.getStr(), it)) }
//
//            s.address?.let {
//                val card = ViewExpandableCard(this.context, R.string.name_address_map.getStr(), it)
//                this.addView(card)
//
//                /*** 지도 add & set */
//                if (s.address != null || s.lat != null) {
//                    ViewUtil.addLinearMapShow(this.context, card.b.lineAttach, s.address, OnMap.createLatlng(s.lat, s.lng))
//                }
//                /*** 검색 태그 */
//                s.tagList?.let { list -> card.b.lineAttach.addView(ViewTagView(this.context).b.tagView.setAll(TAG_3, list)) }
//            }


//        }
//    }


    /**************************************************************************************
     ****  bdStore
     *************************************************************************************/

    @JvmStatic
    @BindingAdapter("android:text_postal_code")
    fun TextView.text_postal_code(postalCode: String?) {
        postalCode?.let {
            val str = context.getString(R.string.add_store_postal_code) + it
            text = str
        }

    }


    @JvmStatic
    @BindingAdapter("android:text_price")
    fun TextView.textPrice(price: Int?) {
        price?.let {
            val df = DecimalFormat("#,##0")
            val str = df.format(it)
            text = str
        }

    }


    /**************************************************************************************
     ****  bdMedia
     *************************************************************************************/

    /**
     * int == 0 이면 그냥 공란으로
     */
    @JvmStatic
    @BindingAdapter("android:text_digit")
    fun EditText.textDigit(digit: Int?) {
        if (digit == null || digit < 1) text = null
        else setText(digit.toString())
    }

    /**
     * ET 활성화 분기하기.
     */
    @JvmStatic
    @BindingAdapter("android:disable_update")
    fun EditText.disableUpdate(actionType: Int?) {
        actionType?.let {
            lllogI("EditText disableUpdate actionType $it")
            if (isAdmin()) {
                isEnabled = true
                return
            }
            isEnabled = it != ACTION_UPDATE
        }
    }


    @JvmStatic
    @BindingAdapter("android:text_media")
    fun TextView.textMedia(media: Media?) {
        media?.let { text = media.title } ?: let { text = R.string.info_select_media.getStr() }
    }

    @JvmStatic
    @BindingAdapter("android:src_media")
    fun ImageView.srcMediaIcon(media: Media?) {
        media?.let {
            setImageDrawable(media.getIcon())
            visi()
        } ?: gone()
    }

    @JvmStatic
    @BindingAdapter("android:media_text_icon")
    fun TextView.mediaTextAndIcon(media: Media?) {
        media?.let {
            text = it.title
            setCompoundDrawablesWithIntrinsicBounds(it.getIcon(), null, null, null)
        } ?: let { text = R.string.info_select_media.getStr() }
    }


    @JvmStatic
    @BindingAdapter("android:src_my_media")
    fun ImageView.srcMyMediaIcon(myMedia: MyMedia?) {
        if (myMedia == null) {
            gone()
        } else {
            setImageDrawable(ContextCompat.getDrawable(context, myMedia.getMediaResId()))
        }
    }

    @JvmStatic
    @BindingAdapter("android:text_media")
    fun TextView.textMedia(media: MyMedia?) {
        text = if (media == null) this.context.getString(R.string.info_select_media) else media.getMediaName()
    }


    @JvmStatic
    @BindingAdapter("android:text_media_name")
    fun TextView.textMediaName(myMedia: MyMedia?) {
        if (myMedia == null) {
            text = this.context.getString(R.string.info_select_media)
        } else {
            val drawable = ContextCompat.getDrawable(context, myMedia.getMediaResId())
            setCompoundDrawablesWithIntrinsicBounds(drawable, null, null, null)
            text = myMedia.getMediaName()
        }
    }


    @JvmStatic
    @BindingAdapter("android:text_media")
    fun Button.textMedia(mediaId: String?) {
        if (!TextUtils.isEmpty(mediaId)) {
            text = mediaId
        }
    }

    @JvmStatic
    @BindingAdapter("android:hint_media")
    fun EditText.hintMedia(media: MyMedia?) {
        media?.let {
            hint = media.getMediaHint()
        }
    }


    @JvmStatic
    @BindingAdapter("android:pay_type_name")
    fun TextView.payTypeName(payType: PayType?) {
        payType?.let {
            setCompoundDrawablesWithIntrinsicBounds(null, null, it.getIcon(), null)
            text = it.title
            visi()
        } ?: gone()
    }


    /**************************************************************************************
     ****  bdButton
     *************************************************************************************/
    @JvmStatic
    @BindingAdapter("android:amenu_icon")
    fun Button.amenu_icon(aMenu: AMenu?) {
        aMenu?.let {
            setCompoundDrawablesWithIntrinsicBounds(null, aMenu.getIcon(), null, null)
        }
    }

    @JvmStatic
    @BindingAdapter("android:text_btn_date_elapsed")
    fun Button.textBtnDateElapsed(longDate: Long?) {
        longDate?.let {
            if (it > 0) {   // 0이상일때만
                text = it.getElapsedTimeString()
                var isElapsed = true
                setOnClickListener {
                    text = if (isElapsed) longDate.getLocalTimeString(FORMAT_SUMMARY) else longDate.getElapsedTimeString()
                    isElapsed = !isElapsed
                    AnimUtil.showSlideDown(this, true)
                }
                visi()
            } else gone()
        }
    }


    /**************************************************************************************
     ****  bdVisibility
     *************************************************************************************/


    /**     show  for admin, author */
    @JvmStatic
    @BindingAdapter("android:show_author_uid")
    fun View.visibilityShowAuthor(authorUid: String?) {
        when {
            authorUid == null -> gone()
            authorUid.isAuthorAndAdmin(null, null) -> visi()
            else -> gone()
        }
    }

    /**     show  for admin   */
    @JvmStatic
    @BindingAdapter("android:show_admin")
    fun View.visibilityShowAdmin(peerType: PeerType) {
        if (OnPeer.isAdmin(peerType, false)) visi() else gone()
    }

    /**     show  for admin   */
    @JvmStatic
    @BindingAdapter("android:show_admin")
    fun View.visibilityShowAdminBool(bool: Boolean?) {
        bool?.let {
            if (it) if (isAdmin()) visi() else gone()
            else if (isAdmin()) gone() else visi()
        } ?: gone()
    }

    /**     show  for admin   */
    @JvmStatic
    @BindingAdapter("android:show_supervisor")
    fun View.visibilityShowSupervisor(bool: Boolean?) {
        if (isSupervisor()) visi() else gone()
    }

    /** 익명 계정일때 (true : visi, false : gone)
     *  익명 아닐때 (true : gone, false : visi)
     **/
    @JvmStatic
    @BindingAdapter("android:visi_anonymous")
    fun View.visiAnonymous(bool: Boolean?) {
        bool?.let {
            if (isAnonymous()) if (it) visi() else gone()
            else if (it) gone() else visi()
        } ?: gone()
    }

    /**
     *  보드마스터 일 때 (true : gone, false : visi)
     */
    @JvmStatic
    @BindingAdapter("android:visi_master")
    fun View.visiMaster(boardMasterUid: String?) {
        boardMasterUid?.let {
            if (it.isMaster() || isAdmin()) {
                visi()
            } else gone()
        } ?: gone()
    }

    /**
     *  보드마스터 일 때 (true : gone, false : visi)
     */
    @JvmStatic
    @BindingAdapter("android:visi_author")
    fun View.visiAuthor(authorUid: String?) {
        authorUid?.let {
            if (it.isAuthor(null, null) || isAdmin()) {
                visi()
            } else gone()
        } ?: gone()
    }

    /**
     *  고객센터 보드의 show/hide
     **/
    @JvmStatic
    @BindingAdapter("android:gone_cs_center")
    fun View.goneCsCenter(boardTypeNo: Int?) {
        boardTypeNo?.let {
            if (it.isCsBoard()) gone() else visi()
        }
    }

    /**
     * GteQ 맞으면 bool에 따라 visi or gone
     * GteQ 아니면 bool의 반대로 visi or gone
     */
    @JvmStatic
    @BindingAdapter("android:visi_version_gte_q")
    fun View.visiVersionGteQ(bool: Boolean?) {
        bool?.let {
            if (versionGteQ) if (it) visi() else gone()
            else if (it) gone() else visi()
        } ?: gone()
    }

    /**
     * 패널티 여부 : for admin
     */
    @JvmStatic
    @BindingAdapter("android:visi_penalty")
    fun TextView.visiPenalty(penaltyLastDate: Long?) {
        penaltyLastDate?.let {
            if (it > rightNow()) visi() else gone()
        } ?: gone()
    }

//    /**
//     * 탈퇴 여부 : 한번 탈퇴하면 deletedDate는 계속 남는다.
//     */
//    @JvmStatic
//    @BindingAdapter("android:alpha_deleted_peer")
//    fun TextView.alphaDeletedPeer(deletedDate: Long?) {
//        deletedDate?.let {
//            if(it > rightNow()) visi() else gone()
//        } ?: gone()
//    }


//    /** peerType gte AdminSuper && true */
//    @JvmStatic
//    @BindingAdapter("android:visi_admin_super")
//    fun View.visibilityAdminSuper(show: Boolean?) {
//        show?.let { if (isAdminSupervisor(false) ) if(it) visi() else gone() }
//    }


    /**
     *  <for storemenu info statusNo ONLY>
     *  infoStatus match   +   contents LinearLayout
     */
    @JvmStatic
    @BindingAdapter("view_type", "action_type", "statusNo")
    fun View.visibilityStatusInfoMatchStoremenu(viewType: Int?, actionType: Int?, statusNo: Int?) {
        viewType?.let {
            actionType?.let {
                statusNo?.let {

                    when (viewType) {
                        Cons.VIEW_TYPE_STATUS_BAR -> {
                            (this as TextView).text = statusNo.getStatus().msg
                            visibility = if (OnPeer.isAdmin(false)) {
                                View.GONE
                            } else {
                                when (actionType) {
                                    Cons.ACTION_TYPE_NORMAL -> {
                                        when (statusNo.getStatus()) {
                                            Status.BLIND_ADMIN, Status.SCREENING -> View.VISIBLE
                                            else -> View.GONE
                                        }
                                    }
                                    else -> View.GONE
                                }
                            }
                        }
                        Cons.VIEW_TYPE_STATUS_TITLE -> {
                            (this as TextView).text = statusNo.getStatus().title
                            visibility = if (OnPeer.isAdmin(false)) {
                                when (actionType) {
                                    Cons.ACTION_TYPE_NORMAL -> View.GONE
                                    else -> View.VISIBLE
                                }
                            } else {
                                when (actionType) {
                                    Cons.ACTION_TYPE_NORMAL -> View.GONE
                                    else -> {
                                        when (statusNo.getStatus()) {
                                            Status.DELETED_USER, Status.BLIND_ADMIN, Status.DENIED, Status.WAIT_MASTER, Status.SCREENING, Status.INSPECTING, Status.WRITING -> View.VISIBLE
                                            else -> View.GONE
                                        }
                                    }
                                }
                            }
                        }
                        Cons.VIEW_TYPE_BTN_MOVE -> {
                            visibility = when (actionType) {
                                Cons.ACTION_TYPE_NEW, Cons.ACTION_TYPE_UPDATE -> View.GONE
                                else -> View.VISIBLE
                            }
                        }
                        Cons.VIEW_TYPE_CONTENTS -> {
                            visibility = if (OnPeer.isAdmin(false)) {
                                View.VISIBLE
                            } else {
                                when (actionType) {
                                    Cons.ACTION_TYPE_NORMAL -> {
                                        when (statusNo.getStatus()) {
                                            Status.NORMAL -> View.VISIBLE
                                            else -> View.GONE
                                        }
                                    }
                                    else -> View.VISIBLE
                                }
                            }
                        }
                        else -> gone()
                    }
                }
            }
        }
    }

    /**
     *  storemenu delete 버튼.
     */
    @JvmStatic
    @BindingAdapter("action_type", "store")
    fun View.visibilityStoreMenuBtnDel(actionType: Int?, store: Store?) {
        actionType?.let {
            store?.let {
                when (actionType) {
                    Cons.ACTION_TYPE_NEW, Cons.ACTION_TYPE_UPDATE, Cons.ACTION_TYPE_INSPECTION -> visi()
                    else -> {
                        if (OnPeer.isAdmin(false) || OnPeer.isMyPeer(store.hostUID)) View.VISIBLE
                        else View.GONE
                    }
                }
            }
        }
    }


    /**
     *  1이하이면 gone.
     *  이상이면 comma로 표시.
     */
    @JvmStatic
    @BindingAdapter("android:text_number_gone")
    fun TextView.textNumberGone(number: Int?) {
        number?.let {
            textComma(number)
            visi()
        } ?: gone()
    }

    /***  "00일간"  for bid 날짜 수.     */
    @JvmStatic
    @BindingAdapter("android:text_bid_days")
    fun TextView.textBidDays(days: Int?) {
        if (days == null || days < 2) gone()
        else {
            text = days.getStrTerm()
            visi()
        }
    }


    /**
     *  1이하이면 gone.
     */
    @JvmStatic
    @BindingAdapter("android:visi_number")
    fun TextView.visiNumber(number: Int?) {
        number?.let {
            if (it < 1) gone() else visi()
        } ?: gone()
    }

    /**
     *  1이하이면 gone.
     */
    @JvmStatic
    @BindingAdapter("android:text_ticket_percent")
    fun TextView.textTicketPercent(bonusPercent: Int?) {
        bonusPercent?.let {
            val str = "+$it%"
            text = str
            visi()
        } ?: gone()
    }


    /**
     */
    @JvmStatic
    @BindingAdapter("android:text_bill_spec")
    fun LinearLayout.textBillSpec(ticket: Ticket?) {
        ticket?.let {

            addView(TextView(context).apply {
                setTextColor(R.color.colorAccent.getClr())
                textSize = 18f
                text = ticket.orderId
            })

            addView(TextView(context).apply {
                setTextColor(R.color.text_dark.getClr())
                text = ticket.purchaseToken
            })

            addView(TextView(context).apply {
                val str = "signature : " + ticket.signature
                text = str
            })


        }
    }


//    /**
//     * (actionType : 수정)  &&  (peerType  for admin) = VISIBLE
//     */
//    @JvmStatic
//    @BindingAdapter("android:visibility_action_type", "android:visibility_peer_more_than")
//    fun View.visibility_type_and_peer_more_than(actionType: Int?, peerType: PeerType?) {
//        actionType?.let {
//            peerType?.let {
//                visibility = if (actionType < Cons.ACTION_TYPE_NORMAL && (OnPeer.peer.actionType.greaterOrEqual(peerType, false))) {
//                    View.VISIBLE
//                } else {
//                    View.GONE
//                }
//            }
//        }
//    }


    /**
     * true : show
     * false : gone
     */
    @JvmStatic
    @BindingAdapter("android:visi_show_gone")
    fun View.visibilityShowGone(bool: Boolean?) {
        if (bool == true) visi() else gone()
    }

    /**
     *  null -> gone
     *  값 있으면 visi
     */
    @JvmStatic
    @BindingAdapter("android:visi_gone")
    fun View.visiGone(data: Any?) {
        if (data == null) gone() else visi()
    }


    @JvmStatic
    @BindingAdapter("android:visibility_show_mypeer")
    fun View.visibilitySHowMyPeer(uid: String?) {
        visibility = if (OnPeer.isMyPeer(uid)) View.VISIBLE else View.GONE
    }

    @JvmStatic
    @BindingAdapter("android:visibility_gone_mypeer")
    fun View.visibilityGoneMyPeer(uid: String?) {
        visibility = if (OnPeer.isMyPeer(uid)) View.GONE else View.VISIBLE
    }

    @JvmStatic
    @BindingAdapter("android:visibility_show_un_same")
    fun View.visibilityShowUnSame(uid: String?) {
        visibility = if (OnPeer.isMyPeer(uid)) {
            View.GONE
        } else {
            View.VISIBLE
        }
    }

    @JvmStatic
    @BindingAdapter("android:visibility_master", "android:visibility_string_data")
    fun View.visibilityShowUserAndNotNull(uid: String?, data: String?) {
        visibility = if (OnPeer.isMyPeer(uid)) {
            View.GONE
        } else {
            if (data == null || data.isEmpty()) {
                View.GONE
            } else {
                View.VISIBLE
            }
        }
    }


    @JvmStatic
    @BindingAdapter("android:visibility_alert_status")
    fun TextView.visiAlertStatus(statusNo: Int?) {
        statusNo?.let {
            text = statusNo.getStatus().msg
            if (Status.NORMAL.gte(statusNo)) gone() else visi()
        } ?: gone()
    }


    @JvmStatic
    @BindingAdapter("android:text_cost_type")
    fun TextView.textCostType(actType: ActType?) {
        actType?.let {
            text = it.getCostNo().costType().title
            setTextColor(it.getCostNo().costType().clr())
        }
    }


    @JvmStatic
    @BindingAdapter("android:text_status")
    fun TextView.textStatus(statusNo: Int?) {
        statusNo?.let { text = statusNo.getStatus().title }
    }

    /**     * 배너 상태 표시     */
    @JvmStatic
    @BindingAdapter("android:text_status_banner")
    fun TextView.textStatusBanner(statusNo: Int?) {
        statusNo?.let {
            //            if (it < Status.BANNER_DENIED.no) gone()
//            else {
            text = statusNo.getStatus().title
            backgroundTintList = ColorStateList.valueOf(it.getStatus().clr())
            visi()
//            }
        } ?: gone()
    }


    @JvmStatic
    @BindingAdapter("android:text_tag_type")
    fun TextView.textTagType(tagTypeNo: Int?) {
        tagTypeNo?.let {
            text = tagTypeNo.getTagType().title
        }
    }


    @JvmStatic
    @BindingAdapter("android:text_bill_event")
    fun TextView.textBillEvent(eventNo: Int?) {
        eventNo?.let {
            text = it.billEvent().title.getStr()
            visi()
        } ?: gone()
    }


    @JvmStatic
    @BindingAdapter("android:text_remote")
    fun TextView.textRemote(strId: Int?) {
        strId?.let {
            text = it.remoteStr()
        }
    }


//    /**
//     * storeMenuStatus에 따라
//     * action_type , statusNo , peerType
//     */
//    @JvmStatic
//    @BindingAdapter("action_type", "storemenu")
//    fun View.setStoreMenuStatus(action: Int?, storeMenu: StoreMenu?) {
////                                    Status.INVALID, Status.DELETED, Status.BLIND, Status.DENIED, Status.WAITING, Status.SCREENING, Status.INSPECTING, Status.WRITING, Status.NORMAL_1000 -> View.GONE
//        action?.let {
//            storeMenu?.let {
//                val result = if (OnPeer.isAdmin(false)) {
//                    true
//                } else {
//                    when (action) {
//                        Cons.ACTION_TYPE_NEW, Cons.ACTION_TYPE_UPDATE -> storeMenu.statusNo == Status.INSPECTING
//                        Cons.ACTION_TYPE_INSPECTION -> true
//                        Cons.ACTION_TYPE_PREVIEW -> storeMenu.statusNo.greaterThanOrEqualTo(Status.WAITING)
//                        Cons.ACTION_TYPE_NORMAL -> storeMenu.statusNo.greaterThanOrEqualTo(Status.NORMAL_1000)
//                        else -> false
//                    }
//                }
//
//                if (result) {
//                    /** width match로 변경 */
//                    OnVisible.setWidthMatchParent(storeMenu.statusNo.lessThan(Status.WAITING), this)
//                }
//                visibility = result.visi()
//            }
//        }
//    }

//    /**
//     * storeMenuStatus에 따라
//     * action_type , statusNo , peerType
//     */
//    @JvmStatic
//    @BindingAdapter("view_type", "action_type", "storemenu")
//    fun View.setStoreMenuStatus(viewType: String?, action: Int?, storeMenu: StoreMenu?) {
////                                    Status.INVALID, Status.DELETED, Status.BLIND, Status.DENIED, Status.WAITING, Status.SCREENING, Status.INSPECTING, Status.WRITING, Status.NORMAL_1000 -> View.GONE
//        action?.let {
//            storeMenu?.let {
//                val result = if (OnPeer.isAdmin(false)) {
//                    true
//                } else {
//                    when (action) {
//                        Cons.ACTION_TYPE_NEW, Cons.ACTION_TYPE_UPDATE -> storeMenu.statusNo == Status.INSPECTING
//                        Cons.ACTION_TYPE_INSPECTION -> true
//                        Cons.ACTION_TYPE_PREVIEW -> storeMenu.statusNo.greaterThanOrEqualTo(Status.WAITING)
//                        Cons.ACTION_TYPE_NORMAL -> storeMenu.statusNo.greaterThanOrEqualTo(Status.NORMAL_1000)
//                        else -> false
//                    }
//                }
//
//                if (result) {
//                    /** width match로 변경 */
//                    OnVisible.setWidthMatchParent(storeMenu.statusNo.lessThan(Status.WAITING), this)
//                }
//                visibility = result.visi()
//            }
//        }
//    }


    @JvmStatic
    @BindingAdapter("android:visibility_status")
    fun View.visibilityStatus(statusNo: Int?) {
        statusNo?.let {
            if (it.gte(Status.NORMAL.no)) visi() else gone()
        }
    }

    /**
     * action type이 수정상태이면 VISIBLE
     */
//    @JvmStatic
//    @BindingAdapter("android:visibility_show_modify")
//    fun View.visibility_show_modify(type: Int?) {
//        type?.let {
//            visibility = when (type) {
//                Cons.ACTION_TYPE_NORMAL -> View.GONE
//                else -> View.VISIBLE
//            }
//        }
//    }

//    /**
//     * show : 어드민 or HostUID
//     */
//    @JvmStatic
//    @BindingAdapter("action_type", "store")
//    fun View.setModifyVisibility(actionType: Int?, store: Store?) {
//        actionType?.let {
//            store?.let {
//                visibility = if (OnPeer.isAdmin(false) || OnPeer.isSamePeer(store.hostUID)) {
//                    View.VISIBLE
//                } else {
//                    View.GONE
//                }
//            }
//        }
//    }

    /**
     * type이 수정상태이면 GONE
     */
//    @JvmStatic
//    @BindingAdapter("android:visibility_hide_modify")
//    fun View.visibility_hide_modify(type: Int?) {
//        type?.let {
//            visibility = when (type) {
//                Cons.ACTION_TYPE_NORMAL -> View.VISIBLE
//                else -> View.GONE
//            }
//        }
//    }


    @JvmStatic
    @BindingAdapter("android:text_my_version_name")
    fun TextView.textMyVersionName(versionName: String?) {
        versionName?.let { text = String.format(R.string.version_current.getStr(), it) }
    }

    @JvmStatic
    @BindingAdapter("android:text_latest_version_name")
    fun TextView.textLatestVersionName(versionName: String?) {
        versionName?.let { text = String.format(R.string.version_latest.getStr(), it) }
    }


    /**     * 3개짜리 iv in Holder     */
    @JvmStatic
    @BindingAdapter("android:text_foto_list3")
    fun TextView.textFotoList3(fotoList: ArrayList<Foto>?) {
        fotoList?.let {
            when {
                fotoList.isEmpty() || fotoList.size == 1 || fotoList.size == 2 || fotoList.size == 3 -> gone()
                fotoList.size > 3 -> {
                    val result = "+${fotoList.size - 3}"
                    text = result
                    visi()
                }
            }
        } ?: let { gone() }
    }


    /**     * 2개짜리 iv in Holder     */
    @JvmStatic
    @BindingAdapter("android:text_foto_list2")
    fun TextView.textFotoList2(fotoList: ArrayList<Foto>?) {
        fotoList?.let {
            when {
                fotoList.isEmpty() || fotoList.size == 1 || fotoList.size == 2 -> gone()
                fotoList.size > 2 -> {
                    val result = "+${fotoList.size - 2}"
                    text = result
                    visi()
                }
            }
        } ?: let { gone() }
    }

    /**     * 1개짜리 iv in Holder     */
    @JvmStatic
    @BindingAdapter("android:text_foto_list1")
    fun TextView.textFotoList1(fotoList: ArrayList<Foto>?) {
        fotoList?.let {
            when {
                fotoList.isEmpty() || fotoList.size == 1 -> gone()
                fotoList.size > 1 -> {
                    val result = "+${fotoList.size - 1}"
                    text = result
                    visi()
                }
            }
        } ?: let { gone() }
    }


    @JvmStatic
    @BindingAdapter("android:text_reply_contents")
    fun TextView.textContents(reply: Reply?) {
        reply?.let {
            if (it.statusNo.gte(Status.NORMAL.no) && it.comment != null) {
                visi()
                text = it.comment
            } else gone()
        }
    }

    @JvmStatic
    @BindingAdapter("android:text_chat_comment")
    fun TextView.textChatComment(chat: Chat?) {
        chat?.let {
            if (it.statusNo.gte(Status.NORMAL.no) && it.comment != null) {
                visi()
                text = it.comment
            } else gone()
        }
    }

    @JvmStatic
    @BindingAdapter("android:text_gone")
    fun TextView.textGoneBinding(comment: String?) {
        this.textGone(comment)
    }

    @JvmStatic
    @BindingAdapter("android:text_user_gone")
    fun TextView.textUserGone(comment: String?) {
        comment?.let {
            this.textGone(String.format(R.string.prefix_user.getStr(), it))
        } ?: gone()
    }

    @JvmStatic
    @BindingAdapter("android:text_admin_gone")
    fun TextView.textAdminGone(reason: String?) {
        reason?.let {
            this.textGone(String.format(R.string.prefix_admin.getStr(), it))
        } ?: gone()
    }

    @JvmStatic
    @BindingAdapter("android:text_hide")
    fun TextView.textHideBinding(comment: String?) {
        this.textHide(comment)
    }

    @JvmStatic
    @BindingAdapter("android:text_data_type_clr")
    fun TextView.textDataTypeClr(dataTypeNo: Int?) {
        dataTypeNo?.let {
            val dataType = it.dataType()
            text = dataType.title
            setTextColor(dataType.clr())
        }
    }


    /**
     * 숫자가 null이면 blank로.
     */
    @JvmStatic
    @BindingAdapter("android:text_digit_blank")
    fun TextView.textDigitBlank(digit: Int?) {
        digit?.let {
            text = if (it == 0) "" else digit.toString()
        }
    }

    @JvmStatic
    @BindingAdapter("android:text_banner_type")
    fun TextView.textBannerType(bannerTypeNo: Int?) {
        bannerTypeNo?.let {
            val bannerType = it.getBannerType()
            if (bannerType == BannerType.NOTHING) gone() else {
                text = bannerType.title
                visi()
            }
        }
    }

    @JvmStatic
    @BindingAdapter("android:text_board_type")
    fun TextView.textBoardType(boardTypeNo: Int?) {
        boardTypeNo?.let {
            when (it.getBoardType()) {
                BoardType.CS_CENTER -> {
                    text = R.string.title_prefix_csboard.getStr()
                    visi()
                }
                else -> gone()
            }
        }
    }

    @JvmStatic
    @BindingAdapter("android:text_adtype")
    fun TextView.textAdTypeNo(adtypeNo: Int?) {
        adtypeNo?.let {
            text = it.getAdtype().title
            visi()
        } ?: gone()
    }

    @JvmStatic
    @BindingAdapter("android:text_adtype_no")
    fun TextView.textAdtypeNo(adtypeNo: Int?) {
        adtypeNo?.let { text = it.getAdtype().title }
    }


//    @JvmStatic
//    @BindingAdapter("android:text_cost_payment")
//    fun TextView.textCostPayment(amount: Int?) {
//        amount?.let {
//            text = if (it < 0) (-it).toString()
//            else it.toString()
//        }
//    }

    @JvmStatic
    @BindingAdapter("android:text_act_title")
    fun TextView.textActTitle(actTypeNo: Int?) {
        actTypeNo?.let {
            val actType = it.actType()
            text = actType.title
//            setTextColor(actType.colorId.getClr())
        }
    }

    @JvmStatic
    @BindingAdapter("android:text_comma")
    fun TextView.textComma(digit: Int?) {
        digit?.let { text = it.getStringComma() }
    }

    @JvmStatic
    @BindingAdapter("android:text_comma")
    fun TextView.textComma(digit: Long?) {
        digit?.let { text = it.getStringComma() }
    }

    @JvmStatic
    @BindingAdapter("android:text_comma_gone")
    fun TextView.textCommaGone(digit: Int?) {
        digit?.let {
            text = digit.getStringComma()
            visi()
        } ?: gone()
    }

    /**
     * 0 or null -> GONE
     */
    @JvmStatic
    @BindingAdapter("android:text_comma_zero_gone")
    fun TextView.textCommaZeroGone(digit: Int?) {
        if (digit == null || digit == 0) gone()
        else {
            text = digit.getStringComma()
            visi()
        }
    }

    @JvmStatic
    @BindingAdapter("android:text_ad_credit")
    fun TextView.textAdCredit(any: Any?) {
        any?.let {
            text = any.anyAdCredit().getStringComma()
        }
    }

    @JvmStatic
    @BindingAdapter("android:text_total_ad_credit")
    fun TextView.textTotalAdCredit(any: Any?) {
        any?.let {
            text = any.anyTotalAdCredit().getStringComma()
        }
    }


    @JvmStatic
    @BindingAdapter("android:text_number")
    fun TextView.textNumber(digit: Int?) {
        digit?.let { text = it.toString() }
    }

    @JvmStatic
    @BindingAdapter("android:text_number")
    fun TextView.textNumberLong(digit: Long?) {
        digit?.let { text = it.toString() }
    }

    /**      __표시로 개행하여 setText     */
    @JvmStatic
    @BindingAdapter("android:text_align")
    fun TextView.textAlign(str: String?) {
        if (str.isNullOrEmpty()) gone() else {
            text = str.setAlign()
            visi()
        }
    }

    @JvmStatic
    @BindingAdapter("android:text_credit")
    fun TextView.textCredit(digit: Int?) {
        digit?.let {
            text = it.getStringComma()
            showOverUp(true)
        }
    }


    @JvmStatic
    @BindingAdapter("android:text_act_commission")
    fun TextView.textActCommission(commission: Int?) {
        if (commission == null || commission < 1) gone()
        else {
            "$commission%".apply { text = this }
            visi()
        }
    }


    @JvmStatic
    @BindingAdapter("android:text_multiply_a", "android:text_multiply_b")
    fun TextView.textAmountTerm(amount: Int?, term: Int?) {
        amount?.let {
            text = getStringAmountMultiplyTerm(amount, term!!)
            visi()
        } ?: gone()
    }

    @JvmStatic
    @BindingAdapter("android:multiply_a", "android:multiply_b")
    fun TextView.textMultiply(amount: Int?, term: Int?) {
        amount?.let {
            text = amount.multiply(term!!).toString()
            visi()
        } ?: gone()
    }


    @JvmStatic
    @BindingAdapter("android:text_mybid_status")
    fun TextView.textMybidStatus(statusNo: Int?) {
        statusNo?.let {
            text = it.getStatus().title
        }
    }


    @JvmStatic
    @BindingAdapter("android:text_bid_status")
    fun TextView.textbidStatus(statusNo: Int?) {
        statusNo?.let {
            it.getStatus().let { status ->
                if (status != Status.BID_BIDDING) text = status.title  // 입찰중일때는 position 번호가 적혀야 함. }
            }
        }
    }


//    @JvmStatic
//    @BindingAdapter("android:text_count", "android:cntch")
//    fun TextView.textCount(anything: Any?, cntchNo: Int?) {
//        anything?.let {
//            cntchNo?.let {no->
//                when(no){
//                    Cntch.UP.no->
//                    Cntch.DOWN.no->
//                    Cntch.BOOK.no->
//                    Cntch.VIEW_UID.no->
//                    Cntch.PUSH.no->
//                    Cntch.ONLINE.no->
//                }
//
//                when (it) {
//                    is Post ->
//                }
//
//                text = if (it < 0) (-it).toString()
//                else it.toString()
//            }
//        }
//    }


//    @JvmStatic
//    @BindingAdapter("android:text_status")
//    fun TextView.text_status(status: Status?) {
//
//        status?.let {
//            if (status.no < Status.NORMAL_1000.no) {
//                visi()
//                text = status.name
//            } else {
//                gone()
//            }
//
////                Status.DELETED->{}
////                Status.BLIND->{}
////                Status.DENIED->{}
////                Status.WAITING->{}
////                Status.SCREENING->{}
////                Status.NORMAL_1000->{}
////                Status.NORMAL_2000->{}
//
//        }
//
//    }


    @JvmStatic
    @BindingAdapter("android:text_alert_status")
    fun TextView.textAlertStatus(statusNo: Int?) {
        statusNo?.let {
            text = statusNo.getStatus().msg
            if (Status.NORMAL.lt(statusNo)) visi() else gone()
            backgroundTintList = ColorStateList.valueOf(it.getStatus().clr())
        }
    }

    @JvmStatic
    @BindingAdapter("android:text_status_title")
    fun TextView.textStatusTitle(statusNo: Int?) {
        statusNo?.let {
            val status = statusNo.getStatus()
            text = status.title
            setTextColor(status.colorId.getClr())
            visi()
        }
    }

    @JvmStatic
    @BindingAdapter("android:text_status_msg")
    fun TextView.textStatusMsg(statusNo: Int?) {
        statusNo?.let {
            val status = statusNo.getStatus()
            text = status.msg
            setTextColor(status.colorId.getClr())
            visi()
        }
    }

    @JvmStatic
    @BindingAdapter("android:text_peer_type")
    fun TextView.textPeerType(type: Int?) {
        lllogI("mybinding textPeerType type $type")
        type?.let {
            val peerType = type.getPeerType()
            text = peerType.title
            setTextColor(peerType.colorId.getClr())
        }
    }


    @JvmStatic
    @BindingAdapter("android:text_login_from")
    fun TextView.textLoginFrom(loginFromNo: Int?) {
        loginFromNo?.let {
            val loginFrom = it.getLoginFrom()
            text = loginFrom.title
            setBackgroundColor(loginFrom.clr())
            visi()
//            setTextColor(loginFrom.clr())
        } ?: gone()
    }


    /**     * LInk Preview 링크 프리뷰     */
//    @JvmStatic
//    @BindingAdapter("android:link_preview")
//    fun LinearLayout.linkPreview(sentence: String?) {
//        OnFunction.setUrlPreview(context, sentence, this, onSuccess = {})
//    }


    /*************** Grant ************************************************************************/

    /**
     * grant switch 허가여부.
     */
    @JvmStatic
    @BindingAdapter("android:checked_grant_handled")
    fun CheckBox.checkedGrantHandled(grant: Grant?) {
        grant?.let {
            isChecked = (it.statusNo.gte(Status.GRANT_HANDLED.no))
            val dataType = it.dataTypeNo.dataType()
            text = dataType.title
            setTextColor(dataType.clr())
            visi()
        } ?: gone()
    }

    /**
     * 승인 요청 버튼 : (grant의 상태가 아닌) 모델의 상태에 따라 가능여부 판별.
     */
    @JvmStatic
    @BindingAdapter("android:visi_avail_grant")
    fun View.visiAllowRequestGrant(any: Any?) {
        any?.let {
            if (OnGrant.availRequestGrant(null, it)) visi() else gone()
        } ?: gone()
    }

    /**
     * 승인 거절 사유 : reason 없으면 grantType.title
     */
    @JvmStatic
    @BindingAdapter("android:text_grant_reason", "android:text_banner_status")
    fun TextView.textGrantReason(g: Grant?, b: Bid?) {
        lllogD("OnMyBinding textGrantReason11111 text ${this.text}  visibility ${this.visibility} ")
        g?.let { grant ->
            b?.let { banner ->
                if (banner.ltNormalStatus()) {
                    text = if (grant.reason == null) banner.statusNo.getStatus().msg else grant.reason
                    visi()
                }
//                else hide()
                lllogD("OnMyBinding textGrantReason22222 text ${this.text}  visibility ${this.visibility} ")
            }
        }
    }

    /**
     * 승인 거절 사유 : reason 없으면 grantType.title
     */
//    @JvmStatic
//    @BindingAdapter("android:text_grant_reason_gone")
//    fun TextView.textGrantReasonGone(grant: Grant?) {
//        grant?.let {
//            if (it.reason == null)
//                if (it.grantTypeNo.grantType() == GrantType.NOTHING) gone()
//                else text = it.grantTypeNo.grantType().title
//            else text = it.reason
//            visi()
//        } ?: gone()
//    }

    /**
     * 승인 거절 사유 : reason 없으면 grantType.title
     */
//    @JvmStatic
//    @BindingAdapter("android:text_grant_type")
//    fun TextView.textGrantType(grantTypeNo: Int?) {
//        grantTypeNo?.let {
//            if (it.grantType() == GrantType.NOTHING) gone()
//            else text = it.grantType().title
//        } ?: gone()
//    }


    /**     * 요청날짜 \n 처리날짜     */
    @JvmStatic
    @BindingAdapter("android:text_grant_date")
    fun TextView.textGrantDate(grant: Grant?) {
        grant?.let {
            val addDate = it.addDate.getElapsedTimeStringShort().first
            val handleDate = it.handleDate?.getElapsedTimeStringShort()?.first ?: "--"
            val sumDate = "$addDate\n\n$handleDate"
            text = sumDate
        }
    }

}