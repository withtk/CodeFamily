package com.tkip.mycode.init.base

import android.util.Log
import android.view.View
import android.widget.AdapterView
import android.widget.EditText
import android.widget.Spinner
import android.widget.TextView
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProviders
import com.google.android.material.textfield.TextInputLayout
import com.tkip.mycode.R
import com.tkip.mycode.dialog.toast.TToast
import com.tkip.mycode.funs.common.getNullStr
import com.tkip.mycode.funs.common.getStr
import com.tkip.mycode.funs.common.lllogI
import com.tkip.mycode.init.part.CodeApp
import com.tkip.mycode.model.my_enum.Status
import com.tkip.mycode.model.my_enum.StoreType
import com.tkip.mycode.model.peer.OnPeer
import com.tkip.mycode.util.lib.BaseViewModelFactory
import com.tkip.mycode.util.lib.photo.Foto
import okhttp3.RequestBody
import java.text.DecimalFormat
import java.util.*


data class Quad<T1, T2, T3, T4>(val t1: T1, val t2: T2, val t3: T3, val t4: T4)
//infix fun <T1, T2,T3, T4> T1.then(t2: T2): Quad<T1, T2,T3, T4> {    return Quad(this, t2)}

/**
 * common
 */
fun createKey() = System.currentTimeMillis().toString() + myUID6()

fun createNanoKey() = System.nanoTime().toString() + myUID6()
fun createGuestNick() = "guest" + System.currentTimeMillis().toString().substring(2, 8)

/** * 자신의 UID에만 유일값 : 사실상 유일값은 아님.*/
fun getKeyFromUID(modelKey: String) = "${modelKey}_${OnPeer.myUid()}"

fun createDataKey(preStr: String) = (preStr + System.currentTimeMillis().toString() + myUID6()).toLowerCase(Locale.ENGLISH)

private fun myUID6() = if (OnPeer.peer.uid.length < 6) OnPeer.peer.uid else OnPeer.peer.uid.substring(0, 6)

inline fun <reified T : ViewModel> Fragment.getViewModel(noinline creator: (() -> T)? = null): T {
    return if (creator == null) ViewModelProviders.of(this).get(T::class.java)
    else ViewModelProviders.of(this, BaseViewModelFactory(creator)).get(T::class.java)
}

inline fun <reified T : ViewModel> FragmentActivity.getViewModel(noinline creator: (() -> T)? = null): T {
    return if (creator == null) ViewModelProviders.of(this).get(T::class.java)
    else ViewModelProviders.of(this, BaseViewModelFactory(creator)).get(T::class.java)
}


inline fun <T> tryNull(block: () -> T) = try {
    block()
} catch (e: Throwable) {
    null
}


/** * 널체크 & 토스트 */
fun Any?.isAnyNull(msgId: Int?): Boolean {
    return if (this == null) {
        msgId?.let { TToast.showAlert(it) }
        true
    } else false
}

fun String?.isNullBlank(msgId: Int?): Boolean {
    return if (this.isNullOrBlank()) {
        msgId?.let { TToast.showAlert(it) }
        true
    } else false
}

/** * 널체크 & no숫자 이하 체크 & 토스트 */
fun Int?.isNull0(no: Int, msgId: Int?): Boolean {
    return if (this == null || this.lt(no)) {
        msgId?.let { TToast.showAlert(it) }
        true
    } else false
}


fun Int.getInt() = CodeApp.getAppContext().resources.getInteger(this)

fun Int.getStr() = CodeApp.getAppContext().getString(this)
fun Int.getClr() = ContextCompat.getColor(CodeApp.getAppContext(), this)
fun Int.getArr() = CodeApp.getAppContext().resources.getStringArray(this)
fun Int.getDrawable() = ContextCompat.getDrawable(CodeApp.getAppContext(), this)

fun Int.format02Plus() = String.format("%02d", this + 1)
fun Int.format02() = String.format("%02d", this)
//fun Int.formatString(strId:Int?) = String.format(strId?.getStr()?:"", this.getStr())
fun Int.getStringComma(): String = DecimalFormat("#,##0").format(this)
fun Long.getStringComma(): String = DecimalFormat("#,##0").format(this)
fun String.getStringComma(): String = DecimalFormat("#,##0").format(this)

/** * 숫자인지 체크 */
  fun String.isNumeric( ): Boolean {
    if (this == null)  return false
    try {
        this.toDouble()
    } catch (nfe: NumberFormatException) {
        return false
    }
    return true
}


//fun Int.getStringComma123(): String = NumberFormat.getInstance().format(this)
//fun Int.getStringKOREA(): String =  NumberFormat.getCurrencyInstance(Locale.KOREA).format(this)

/***  "Int일간"  */
fun Int.getStrTerm() = String.format(R.string.cal_differ.getStr(), this)

/** * "90 x 3일간" */
fun getStringAmountMultiplyTerm(amount: Int, term: Int): String = "${amount.getStringComma()} X ${term.getStrTerm()}"

fun Int.multiply(term: Int): Int = this * term



fun Int.appendMonth() = String.format(R.string.cal_month.getStr(), this)
/** 10월 */


fun getCommaArr(vararg arr: Int): String {
    val str = StringBuffer()
    arr.forEachIndexed { i, d ->
        if (i == 0) str.append(d)
        else str.append(",").append(d)
    }
    return str.toString()
}

/** * 이상 미만 */
fun Int.gt(no: Int) = this > no

fun Int.gte(no: Int) = this >= no
fun Int.lt(no: Int) = this < no
fun Long.gt(no: Int) = this > no
fun Long.gte(no: Int) = this >= no
fun Long.lt(no: Int) = this < no
fun Long.gt(no: Long) = this > no
fun Long.gte(no: Long) = this >= no
fun Long.lt(no: Long) = this < no

/** * 첫글자 0 제거 */
fun String.remove0String(): String = this.toInt().toString()

fun String.same(str: String) = this == str
fun String.discard0(): String {
    return this.replace("\\s".toRegex(), "") // 공백 제거.
}

fun String.requestBody(): RequestBody {
    return RequestBody.create(okhttp3.MediaType.parse("application/json; charset=utf-8"), this)
}

fun String.removeComma() = replace(R.string.comma.getStr(), "")
fun String.reverse() = StringBuffer(this).reverse().toString()
fun String.oneInt(i: Int) = get(i) - '0'
//Character.getNumericValue(input.charAt(i))


/**     * tabLayout에 너무 길면 안 되기때문.     */
fun String.cutName() = if (this.length > 8) this.substring(0, 10) else this

fun String.cutName(max: Int) = if (this.length > max - 2) this.substring(0, max) else this

/**  특수문자 제거 하기   */
fun String.removeSpecial() = replace("[^\uAC00-\uD7A3xfe0-9a-zA-Z\\s]".toRegex(), " ")

/**  Alphabet 제거 하기   */
fun String.removeAlphabet() = replace("[a-zA-Z]".toRegex(), " ")

/**  연속 스페이스 제거   */
fun String.removeLongSpace() = replace("\\s{2,}".toRegex(), " ")

fun String.removeSpace() = replace(" ".toRegex(), "")
/**  나눠서 array로 반환 */
fun String.getArraySplitSpace() = split(" ").toTypedArray()

//fun View.tranPair(nameRes: Int) = androidx.core.util.Pair(this, nameRes.getStr())

/**  __로 개행하기 */
fun String.setAlign() = replace("__", "\n")


/************************************** * stPosition *************************************/
fun Int.getTypePosition(array: Array<Int>): Int {
    for ((i, type) in array.withIndex()) {
        if (this == type) return i
    }
    return 0
}

fun Status.getPosition(list: ArrayList<Status>): Int {
    for ((i, status) in list.withIndex()) {
        if (this == status) return i
    }
    return 0
}

fun StoreType.getPosition(): Int {
    val array = StoreType.values().toList()
    for ((i, type) in array.withIndex()) {
        if (this == type) return i
    }
    return 0
}


/**
 * board,post
 * pre와 변경된 new  체크
 */
fun String?.isChanged(new: String?) = this ?: "" != new ?: ""
fun Int?.isChanged(new: Int?) = this ?: 0 != new ?: 0
fun Boolean?.isChanged(new: Boolean?) = this ?: false != new ?: false

fun ArrayList<Foto>?.isChanged(fotoList: ArrayList<Foto>?): Boolean {
    this?.forEach { f1 ->
        fotoList?.forEach { f2 ->
            if (f1.comment != f2.comment) return true
        }
    }
    return false
}

fun Any?.isChanged(new: Any?): Boolean {
    val result = when {
        this == null -> new != null
        this is String -> this != (new as? String)
        this is Int -> this != (new as? Int)
        this is Long -> this != (new as? Long)
        this is Float -> this != (new as? Float)
        this is Double -> this != (new as? Double)
        this is Boolean -> this != (new as? Boolean ?: false)
        else -> false
    }
    lllogI("HomeFrag isChanged : $this | $new = $result")
    return result
}


fun MutableMap<String, Any?>.checkUpdate(a1: Any?, b1: Any?, child: String) {
    lllogI("checkUpdate isChanged start ----")
    if (a1.isChanged(b1)) put(child, b1)
}

//fun MutableMap<String, Any>.checkUpdate(a1:Int ,b1:Int , child:String) {
//    if (a1.isChanged(b1)) put(child, b1 )
//}
//
//fun MutableMap<String, Any>.checkUpdate(a1:Boolean ,b1:Boolean , child:String) {
//    if (a1.isChanged(b1)) put(child, b1 )
//}


/************************************** * View *************************************/


/** TextInputEditText 에서   * 1. Null 체크 * 2. set Error */
fun EditText.isNullBlankAndSetError(layout: TextInputLayout?, stringID: Int): Boolean {
    return if (getNullStr() == null) {
        if (layout == null) error = stringID.getStr()
        else layout.error = stringID.getStr()
        true
    } else {
        error = null
        layout?.let { it.error = null }
        false
    }
}

/** number et의 미니멈 체크 * 1. isEnabled 체크 * 2. null 체크 */
fun EditText.removeZero() {
    getNullStr()?.let { setText(it.toInt().toString()) }
}

/** number et의 미니멈 체크 * 1. isEnabled 체크 * 2. null 체크 */
fun EditText.isLtAmount(minAmount: Int, layout: TextInputLayout?, stringID: Int): Boolean {
    if (isEnabled && getNullStr() != null) {   // enable이 null이면 update Post임 && text==null이면 error표시할 필요없음.
        val myDigit = getStr().toInt()
        if (myDigit.gte(1) && myDigit.lt(minAmount)) {
            if (layout != null) layout.error = String.format(stringID.getStr(), minAmount)
            else this.error = String.format(stringID.getStr(), minAmount)
            return true
        }
    }
    getNullStr()?.let { if (it.toLong() == 0L) text = null } // 0일때는 null로
    layout?.let { lay ->
        lay.error?.let { lay.error = null }
        if (!isEnabled) lay.helperText = R.string.hint_already_reward_add_post.getStr()
    }
    return false
}

fun EditText.isGtAmount(amount: Int, layout: TextInputLayout?, stringID: Int): Boolean {
    if (isEnabled && getNullStr() != null) {   // enable이 null이면 update Post임 && text==null이면 error표시할 필요없음.
        val myDigit = getStr().toInt()
        if (myDigit.gte(1) && myDigit.gt(amount)) {
            if (layout != null) layout.error = String.format(stringID.getStr(), amount)
            else this.error = String.format(stringID.getStr(), amount)
            return true
        }
    }
    getNullStr()?.let { if (it.toLong() == 0L) text = null } // 0일때는 null로
    error = null
    layout?.let { lay ->
        lay.error?.let { lay.error = null }
        if (!isEnabled) lay.helperText = R.string.hint_already_reward_add_post.getStr()
    }
    return false
}

fun String.isLt(min: Int) = this.length < min


/************************************ ArrayList<T> *********************************/

/** * arrayList null check 본인 반환.
 * error 자기를 넘겨야지 왜 새걸 넘겨. */
//fun <T> ArrayList<T>?.me(): ArrayList<T> = this ?: arrayListOf()

/** * arrayList 복제. 클론.*/
//fun <T> ArrayList<T>?.myclone(newList: ArrayList<T>) {
//    this?.let { oldList ->
//        newList.clear()
//        newList.addAll(oldList)
//    }
//}

/** * clear and addAll, 만약 널이면 새로 만들어서 리턴. */
fun <T> ArrayList<T>?.clearAddAll(finalList: ArrayList<T>?): ArrayList<T>? {
    if (finalList != null && finalList.isNotEmpty()) {
        this?.let {
            clear()
            addAll(finalList)
            return it
        } ?: return arrayListOf<T>().apply { addAll(finalList) }
    }
    return this
}

/** * clear and addAll, 만약 널이면 새로 만들어서 리턴. */
fun <T> ArrayList<T>?.myAddAll(finalList: ArrayList<T>?): ArrayList<T>? {
    if (!finalList.isNullOrEmpty()) {
        this?.let {
            addAll(finalList)
            return it
        } ?: return arrayListOf<T>().apply { addAll(finalList) }
    }
    return this
}

fun <T> ArrayList<T>?.notNullhasItem(): Boolean = this != null && this.isNotEmpty()

/** * 1. 널체크, 2. 존재여부 체크  3. Get */
fun <T> ArrayList<T>?.getI(index: Int): T? = if (this == null || this.size <= index) null else this[index]


/** * 리스트 -> String */
fun ArrayList<String>.getStr() = StringBuffer().apply { this@getStr.forEach { this@apply.append(it) } }.toString().removeSpace()

/** * 리스트내의 String 인덱스 받아오기 */
fun ArrayList<String>.getI(str:String) :Int?{
    this.forEachIndexed { i, s ->
        if(s==str) return i
    }
    return null
}



/************************************** * Spinner *************************************/
fun Spinner.setListener(onItemSelected: (Int) -> Unit, onNothingSelected: () -> Unit) {
    onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
        override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
            Log.d(Cons.TAG, "spin onItemSelected")
            onItemSelected(position)
        }

        override fun onNothingSelected(parent: AdapterView<*>?) {
            Log.d(Cons.TAG, "spin onNothingSelected")
            onNothingSelected()
        }
    }

}


/**
 * Model 캐스팅 하기.
 * 제너릭으로 만들다 맘.
 */
/*
fun  DocumentSnapshot.getObject(model: MyModel )    =
        when (model) {
            MyModel.PEER ->     toObject(Peer::class.java)!!
            MyModel.DEVICE_INFO -> toObject(DeviceInfo::class.java)!!
            MyModel.SERVER_PEER -> toObject(ServerPeer::class.java)!!

            MyModel.BOOKMARK -> toObject(BookmarkBefore::class.java)!!

            MyModel.STORE -> toObject(Store::class.java)!!
            MyModel.STORE_MENU -> toObject(StoreMenu::class.java)!!
            MyModel.RATING -> toObject(RatingBar::class.java)!!
            MyModel.STORE_MIN -> toObject(StoreMin::class.java)!!

            MyModel.POST -> toObject(Post::class.java)!!
            MyModel.COUNTER -> toObject(CounterUID::class.java)!!
            MyModel.REPLY -> toObject(Reply::class.java)!!

            MyModel.UID_DATE -> toObject(UidDate::class.java)
            else->{Any()}
        }*/

/*
inline fun <reified T> doSomething() : T? {
    return when (T::class.java) {
        Boolean::class.java -> somethingThatReturnsBoolean() as T
        Int::class.java -> somethingThatReturnsInt() as? T
        else -> throw Exception("Unhandled return actionType")
    }
}*/






