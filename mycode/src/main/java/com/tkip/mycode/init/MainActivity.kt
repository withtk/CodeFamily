package com.tkip.mycode.init

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.view.MenuItem
import androidx.activity.result.ActivityResultLauncher
import androidx.annotation.LayoutRes
import androidx.core.view.GravityCompat
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.ListenerRegistration
import com.tkip.mycode.R
import com.tkip.mycode.ad.ad_show.CustomBannerSlide
import com.tkip.mycode.auth.OnSign
import com.tkip.mycode.auth.SignActivity
import com.tkip.mycode.databinding.ActivityMainBinding
import com.tkip.mycode.funs.common.*
import com.tkip.mycode.init.main.initView
import com.tkip.mycode.init.main.removeAllListener
import com.tkip.mycode.init.my_bind.BindingActivity
import com.tkip.mycode.model.count.Count
import com.tkip.mycode.model.count.CountAdapterFireS
import com.tkip.mycode.model.count.my_book_push.CountAdapter
import com.tkip.mycode.model.peer.OnPeer
import com.tkip.mycode.model.peer.Peer
import com.tkip.mycode.util.tools.etc.BackPressCloseHandler
import com.tkip.mycode.util.tools.fire.realOnline

/**
 */
class MainActivity : BindingActivity<ActivityMainBinding>() {
    @LayoutRes
    override fun getLayoutResId() = R.layout.activity_main

//    lateinit var startForResult: ActivityResultLauncher<Intent>
//    startForResult = registerForActivityResult(ActivityResultContracts.StartActivityForResult()) { result: ActivityResult ->
//        if (result.resultCode == Activity.RESULT_OK) {
//            lllogD("startForResult  registerForActivityResult ")
//            result.data?.getParcelableArrayListExtra<Count>(PUT_ARRAY_COUNT)?.let {
//                lllogD("startForResult  registerForActivityResult data $it")
//                quickList = it
//                quickAdapter?.notifyDataSetChanged()
//            }
//        }
//    }

//    var quickAdapterFireS: CountAdapterFireS? = null
    var quickAdapter: CountAdapter? = null
    var quickList = arrayListOf<Count>()   // quick List
    val authListener by lazy {
        FirebaseAuth.AuthStateListener { firebaseAuth ->
            lllogD("MainActivity AuthStateListener firebaseAuth: $firebaseAuth")
            if (firebaseAuth.currentUser == null) {
                lllogD("MainActivity AuthStateListener null firebaseAuth: $firebaseAuth")
                OnSign.gotoSignActivityAndFinish(this@MainActivity, SignActivity.TYPE_NORMAL, true)
                OnPeer.peer = Peer()
            }
        }
    }

    var bannerMenu: CustomBannerSlide? = null

    override fun onResume() {
        super.onResume()
    }

    override fun onStop() {
        super.onStop()
        bannerMenu?.stopSlide()
    }

    override fun onDestroy() {
        removeAllListener()
        super.onDestroy()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)


        lllogI("mainActivity Start ===")
        lllogD("mainActivity Start ===")
        lllogW("mainActivity Start ===")
        lllogE("mainActivity Start ===")


        /** 업데이트 버전 체크 */
//        if (OnBasic.isLatestVersion()) start()
//        else MyAlert.showUpdate(this@SplashActivity,
//                onOk = { OnBasic.gotoGoogleStore(this@SplashActivity) },
//                onForceCancel = { finish() },
//                onCancel = { start() })


        /*** 기본 보드 셋팅 : 준비되면 활성화 */
//        Handler().postDelayed({ OnSystem.initBoard(  onSuccess = { }, onFail = { }) }, 1000)

//        MyObserver(lifecycle)

        initView()

    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == android.R.id.home) toggleDrawer()
        return super.onOptionsItemSelected(item)
    }

//    override fun onRestoreInstanceState(save: Bundle) {
//        super.onRestoreInstanceState(save)
////        b.appMain.bottomNav.selectedItemId = save.getInt(CURRENT_ITEM_ID)
//    }
//
//    override fun onSaveInstanceState(outState: Bundle) {
//        super.onSaveInstanceState(outState)
////        outState.putInt(CURRENT_ITEM_ID, currentItemId)
//    }


    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        for (fragment in supportFragmentManager.fragments) {
            fragment.onRequestPermissionsResult(requestCode, permissions, grantResults)
        }
    }

    override fun onBackPressed() {
        if (b.drawerLayout.isDrawerOpen(GravityCompat.START)) b.drawerLayout.closeDrawers()
        else BackPressCloseHandler.onBackPressed(this)
    }

    /** drawerLayout 토글 Onclick */
    fun toggleDrawer() {
        if (b.drawerLayout.isDrawerOpen(GravityCompat.START)) b.drawerLayout.closeDrawers()  // CLOSE
        else b.drawerLayout.openDrawer(GravityCompat.START)  // OPEN
    }
}
