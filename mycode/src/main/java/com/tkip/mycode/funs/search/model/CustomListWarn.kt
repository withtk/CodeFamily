package com.tkip.mycode.funs.search.model

import android.content.Context
import android.view.LayoutInflater
import android.widget.LinearLayout
import androidx.databinding.DataBindingUtil
import com.tkip.mycode.R
import com.tkip.mycode.databinding.CustomModelListBinding
import com.tkip.mycode.init.EES_PENALTY
import com.tkip.mycode.init.EES_WARN
import com.tkip.mycode.init.base.getInt
import com.tkip.mycode.model.warn.Warn
import com.tkip.mycode.model.warn.WarnAdapter
import com.tkip.mycode.util.lib.retrofit2.ESget
import com.tkip.mycode.util.lib.retrofit2.ESquery
import com.tkip.mycode.util.tools.recycler.OnRv

/**
 * 1. warn
 * 2. penalty
 */
class CustomListWarn : LinearLayout {
    private lateinit var b: CustomModelListBinding

    private var isWarn = true
    private var warnAdapter: WarnAdapter? = null
    private val warnItems by lazy { arrayListOf<Warn>() }

    private val esSize by lazy { R.integer.es_size_warn.getInt() }

    private var esLastIndex = 0

    constructor(context: Context) : super(context)
    constructor(context: Context, isWarn: Boolean, onItem: ((Int, Warn) -> Unit)?) : super(context) {
        b = DataBindingUtil.inflate(LayoutInflater.from(context), R.layout.custom_model_list, this, true)
        this.isWarn = isWarn

        /** set Adapter */
        val rvType = if (isWarn) WarnAdapter.rvWarn else WarnAdapter.rvPenalty
        warnAdapter = WarnAdapter(warnItems, rvType, onItem)
        b.inRefresh.rv.adapter = warnAdapter

        b.inRefresh.refresh.setOnRefreshListener { getEsDataList(0) }
        b.inRefresh.refresh.setOnLoadMoreListener { getEsDataList(esLastIndex) }
//        b.refresh.setEnableRefresh(false)   // 상단 refresh는 불능.
//        b.refresh.setEnableLoadMore(false)  // 하단 loadMore 불능.

    }

    /** * 가져오기  */
    fun getEsDataList(from: Int) {

        val index: String
        val query: String

        if (isWarn) {
            index = EES_WARN
            query = ESquery.warnList(esSize, from)
        } else {
            index = EES_PENALTY
            query = ESquery.penaltyList(esSize, from)
        }

        ESget.getWarnList(index, query,
                success = {
                    esLastIndex = OnRv.setRvRefresh(it, from, warnItems, b.inRefresh, true)
                    warnAdapter?.notifyDataSetChanged()
                }, fail = {})

    }


}