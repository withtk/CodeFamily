package com.tkip.mycode.funs.search.model

import android.content.Context
import android.view.LayoutInflater
import android.widget.LinearLayout
import androidx.core.widget.NestedScrollView
import androidx.databinding.DataBindingUtil
import com.tkip.mycode.R
import com.tkip.mycode.databinding.CustomModelListBinding
import com.tkip.mycode.funs.common.lllogI
import com.tkip.mycode.init.CH_COMMENT
import com.tkip.mycode.init.CH_TAG_LIST
import com.tkip.mycode.init.CH_TITLE
import com.tkip.mycode.init.RV_VERT
import com.tkip.mycode.init.base.getInt
import com.tkip.mycode.model.board.Board
import com.tkip.mycode.model.flea.Flea
import com.tkip.mycode.model.my_enum.DataType
import com.tkip.mycode.model.my_enum.Status
import com.tkip.mycode.model.post.Post
import com.tkip.mycode.model.room.Room
import com.tkip.mycode.model.store.Store
import com.tkip.mycode.nav.board.tab_first.BoardAdapter
import com.tkip.mycode.nav.flea.FleaAdapter
import com.tkip.mycode.nav.post.PostAdapter
import com.tkip.mycode.nav.room.RoomAdapter
import com.tkip.mycode.nav.store.StoreAdapter
import com.tkip.mycode.util.lib.retrofit2.ESget
import com.tkip.mycode.util.lib.retrofit2.ESquery
import com.tkip.mycode.util.tools.recycler.OnRv

/**
 * for board, post, flea, room, store, mytag
 * 검색결과
 */
class CustomSearchModel : LinearLayout {
    private lateinit var b: CustomModelListBinding
    private var manage: Boolean = false
    private lateinit var dataType: DataType
    var rvType :Int? = null
    //    private var keyword = ""
    private var searchWordList = arrayListOf<String>()

//    private lateinit var aaaalist: ArrayList<*>
//    private lateinit var aaaaada: RecyclerView.Adapter<RecyclerView.ViewHolder>

    private val boardItems by lazy { arrayListOf<Board>() }
    private val postItems by lazy { arrayListOf<Post>() }
    private val fleaItems by lazy { arrayListOf<Flea>() }
    private val roomItems by lazy { arrayListOf<Room>() }
    private val storeItems by lazy { arrayListOf<Store>() }
    private var boardAdapter: BoardAdapter? = null
    private var postAdapter: PostAdapter? = null
    private var fleaAdapter: FleaAdapter? = null
    private var roomAdapter: RoomAdapter? = null
    private var storeAdapter: StoreAdapter? = null

    private val esSize by lazy { R.integer.es_size_search.getInt() }

    var esLastIndex = 0

    constructor(context: Context) : super(context)
    constructor(context: Context, manage: Boolean, dataType: DataType, rvType:Int?, onScroll: ((Boolean) -> Unit)?, onItem: ((Any) -> Unit)?) : super(context) {
        b = DataBindingUtil.inflate(LayoutInflater.from(context), R.layout.custom_model_list, this, true)
        this.manage = manage
        this.dataType = dataType
        this.rvType = rvType
        lllogI("CustomSearchList dataType:$dataType rvType:$rvType")

        /** set Adapter */
        setupRV(onItem)

        b.inRefresh.nScroll.setOnScrollChangeListener { _: NestedScrollView?, _: Int, scrollY: Int, _: Int, oldScrollY: Int ->
             onScroll?.invoke((scrollY < oldScrollY))
        }

//        b.inRefresh.rv.myScroll(onScrolled = { _, _, _ -> },
//                onScrollStateChanged = { _, state -> if (state == AbsListView.OnScrollListener.SCROLL_STATE_TOUCH_SCROLL) onScroll?.invoke() })


        b.inRefresh.refresh.setOnRefreshListener { getEsDataList(0, searchWordList) }
        b.inRefresh.refresh.setOnLoadMoreListener { getEsDataList(esLastIndex, searchWordList) }
//        b.refresh.setEnableRefresh(false)   //상단 refresh는 불능
//        b.refresh.setEnableLoadMore(false)  // 하단 loadMore 불능.

//        getEsDataList(0, searchWordList)   // 시작하자마자 일단 검색.
    }


    /** * adapter 세팅 */
    private fun setupRV(onItem: ((Any) -> Unit)?) {

        when (dataType) {
            DataType.BOARD -> {
                if (boardAdapter == null) boardAdapter = BoardAdapter(boardItems, rvType?: RV_VERT, onItem)
                b.inRefresh.rv.adapter = boardAdapter
            }
            DataType.POST -> {
                if (postAdapter == null) postAdapter = PostAdapter(postItems, rvType?: RV_VERT, onItem)
//                    b.inRefresh.rv.layoutManager = LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false)
                b.inRefresh.rv.adapter = postAdapter
            }
            DataType.FLEA -> {
                if (fleaAdapter == null) fleaAdapter = FleaAdapter(fleaItems,rvType?: RV_VERT, onItem)
                b.inRefresh.rv.adapter = fleaAdapter
            }
            DataType.ROOM -> {
                if (roomAdapter == null) roomAdapter = RoomAdapter(roomItems, rvType?:RV_VERT, onItem)
                b.inRefresh.rv.adapter = roomAdapter
            }
            DataType.STORE -> {
                if (storeAdapter == null) storeAdapter = StoreAdapter(storeItems, rvType?:RV_VERT, onItem)
                b.inRefresh.rv.adapter = storeAdapter
            }
            else -> {
            }
        }

    }

    private fun getQuery(from: Int, childArr: Array<String>, wordList: ArrayList<String>, totalPoint: Boolean, addDate: Boolean): String {
//        return ESquery.matchKeyword(esSize, esLastIndex, childArr, keyword, Status.BLIND)
        return ESquery.searchMultiMatch(esSize, from, childArr, wordList, Status.NORMAL, totalPoint, addDate)
    }

    /** * 가져오기  */
    fun getEsDataList(from: Int, wordList: ArrayList<String>?) {
        if (wordList == null) searchWordList.clear()
        else searchWordList = wordList
//        this.keyword = searchText
//        b.avLoading.visi()
        when (dataType) {
            DataType.BOARD ->
//                val query = ESquery.boardMatchKeywordTotalPoint(esSize, esLastIndex, arrayOf(CH_TITLE, CH_COMMENT, CH_TAG_LIST), searchWordList, Status.BLIND)
                ESget.getBoardList(ESquery.searchMultiMatch4Board(esSize, from, true, arrayOf(CH_TITLE, CH_COMMENT, CH_TAG_LIST), searchWordList, Status.NORMAL),
                        success = {
                            esLastIndex = OnRv.setRvRefresh(it, from, boardItems, b.inRefresh, true)
                            boardAdapter?.notifyDataSetChanged()
                        }, fail = { })
            DataType.POST -> ESget.getPostList(ESquery.searchMultiMatch4Board(esSize, from, true, arrayOf(CH_TITLE, CH_COMMENT, CH_TAG_LIST), searchWordList, Status.NORMAL),
//                    getQuery(from, arrayOf(CH_TITLE, CH_COMMENT, CH_TAG_LIST), searchWordList, totalPoint = false, addDate = false),
                    success = {
                        esLastIndex = OnRv.setRvRefresh(it, from, postItems, b.inRefresh, true)
                        postAdapter?.notifyDataSetChanged()
                    }, fail = { })

            DataType.FLEA -> ESget.getFleaList(ESquery.searchAdCredit(esSize,from, arrayOf(CH_TITLE, CH_COMMENT, CH_TAG_LIST), searchWordList, Status.NORMAL, addDate = true),
                    success = {
                        esLastIndex = OnRv.setRvRefresh(it, from, fleaItems, b.inRefresh, true)
                        fleaAdapter?.notifyDataSetChanged()
                    }, fail = { })

            DataType.ROOM -> {
                ESget.getRoomList(ESquery.searchAdCredit(esSize,from, arrayOf(CH_TITLE, CH_COMMENT, CH_TAG_LIST), searchWordList, Status.NORMAL, addDate = true)  ,
                        success = {
                            esLastIndex = OnRv.setRvRefresh(it, from, roomItems, b.inRefresh, true)
                            roomAdapter?.notifyDataSetChanged()
                        }, fail = { })
            }
            DataType.STORE -> ESget.getStoreList(ESquery.searchAdCredit(esSize,from, arrayOf(CH_TITLE, CH_TAG_LIST), searchWordList, Status.NORMAL, addDate = true) ,
                    success = {
                        esLastIndex = OnRv.setRvRefresh(it, from, storeItems, b.inRefresh, true)
                        storeAdapter?.notifyDataSetChanged()
                    }, fail = { })

            else -> {
            }
        }
    }


}