package com.tkip.mycode.funs.custom.text

import android.content.Context
import android.view.LayoutInflater
import android.widget.LinearLayout
import androidx.databinding.DataBindingUtil
import com.tkip.mycode.R
import com.tkip.mycode.databinding.BtnCornerBinding
import com.tkip.mycode.funs.common.onClick

/**
 * for tag
 */
class BtnCorner : LinearLayout {
    lateinit var b: BtnCornerBinding
    lateinit var name:String

    constructor(context: Context) : super(context)
    constructor(context: Context, name: String, onClick:((name:String)->Unit)?) : super(context) {
        b = DataBindingUtil.inflate(LayoutInflater.from(context), R.layout.btn_corner, this, true)
        this.name = name
        b.name = name

        b.btn.onClick { onClick?.invoke(name) }
    }


}