package com.tkip.mycode.funs.search.view

import android.content.Context
import android.view.LayoutInflater
import android.widget.LinearLayout
import androidx.databinding.DataBindingUtil
import com.tkip.mycode.R
import com.tkip.mycode.databinding.CustomSearchTagEdittextBinding
import com.tkip.mycode.funs.common.*
import com.tkip.mycode.funs.search.model.CustomSearchTag
import com.tkip.mycode.init.base.getArraySplitSpace
import com.tkip.mycode.init.base.removeLongSpace
import com.tkip.mycode.init.base.removeSpecial
import com.tkip.mycode.init.inter.interSearchTagview
import com.tkip.mycode.model.mytag.*
import com.tkip.mycode.util.tools.anim.AnimUtil
import com.tkip.mycode.util.tools.anim.gone
import com.tkip.mycode.util.tools.anim.visi

/**
 */
class CustomSearchTagEdittext : LinearLayout {
    lateinit var b: CustomSearchTagEdittextBinding
    private val wordList = arrayListOf<String>()
    private lateinit var customSearchTag: CustomSearchTag

    lateinit var tagAdapter: MyTagAdapter
    private val tagItems = arrayListOf<MyTag>()

    constructor(context: Context) : super(context)
    constructor(context: Context, tagType:TagTYPE,onClear: () -> Unit, onSearch: ((wordList: ArrayList<String>?) -> Unit) ) : super(context) {
        b = DataBindingUtil.inflate(LayoutInflater.from(context), R.layout.custom_search_tag_edittext, this, true)

        /*** 클릭 리스너 */
        b.inter = interSearchTagview(
                onTagView = {  },
                onSearch = { resetWordList(onSearch) })

        b.tagView.setOnTagClickListener { _, p ->
            b.tagView.remove(p)
            resetWordList(onSearch)
        }
//        b.tagView.setOnTagDeleteListener { _, _, _ -> }
//        b.tagView.setOnTagLongClickListener { _, _ -> }


        /** * 태그 adapter  */
        tagAdapter = MyTagAdapter(tagItems, MyTagAdapter.rvNormal,
                /**   * MyTagAdapter에서  아이템 클릭.    */
                onItemClick = { adap, p, myTag ->
                    b.etSearch.setText("")
                    b.tagView.checkAdd(TAG_1, myTag.name)    // tagView에 추가
                    tagItems.clear()
                    afterSearch()
                    resetWordList(onSearch)
                })
        b.rvTag.adapter = tagAdapter


        /** * 실시간 태그 검색 */
        b.etSearch.textListener(after = { s ->
            OnVisible.toggleSimple(!s.isNullOrBlank(), b.btnClearEt)   // x버튼 토글
            b.btnSearch.isEnabled = !s.isNullOrBlank()      // search버튼 가능/불가

            b.etSearch.keyword()?.let { keyword ->
                OnTag.getEsTagList(10, 0,tagType, keyword,
                        success = {
                            lllogI("ViewSearchTagView list.size :${it?.size}")
                            it.addCheckTagView(tagItems, b.tagView)
                            afterSearch()
                        }, fail = { afterSearch() })
            } ?: let {
                // keyword 널이면 list 지워.
                tagItems.clear()
                afterSearch()
            }
        }, before = { _, _, _, _ -> }, on = { s, _, _, c -> })


        b.btnClearEt.onClick {
            b.etSearch.setText("")
        }
    }

    private fun resetWordList(onSearch: ((wordList: ArrayList<String>?) -> Unit) )  {
//        val list = arrayListOf<String>()
        wordList.clear()
        b.tagView.tags.forEach { wordList.add(it.text) }   // tagView 넣기.
        b.etSearch.keyword()?.let {
            it.removeSpecial().removeLongSpace().trim().getArraySplitSpace().forEach { str -> wordList.add(str) } // keyword 다듬어서 넣기.
        }
        lllogI("getWordList list ${wordList.size}")
        onSearch (wordList)
    }



    private fun afterSearch() {
        AnimUtil.hideFadeOut(b.avLoading, true)
        if (tagItems.isEmpty()) b.rvTag.gone()
        else b.rvTag.visi()
        tagAdapter.notifyDataSetChanged()
    }


}