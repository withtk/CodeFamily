package com.tkip.mycode.funs.custom.menu

import android.content.Context
import android.view.LayoutInflater
import android.widget.LinearLayout
import androidx.databinding.DataBindingUtil
import com.tkip.mycode.R
import com.tkip.mycode.databinding.ViewMenuLineCheckBinding
import com.tkip.mycode.funs.common.lllogI
import com.tkip.mycode.funs.common.onClick


/**
 * Menu line - SW or CheckBox
 * onClick 리스너를 여는 쪽에 따라 선택됨. 둘 다 null이면 안 됨.
 */
class ViewMenuLineCheck : LinearLayout {
    private lateinit var b: ViewMenuLineCheckBinding
    private var modeSW = true

    constructor(context: Context) : super(context)
    constructor(context: Context, title: String, desc: String, checked: Boolean, onClickSW: ((checked: Boolean) -> Unit)?, onClickCB: ((checked: Boolean) -> Unit)?) : super(context) {

        b = DataBindingUtil.inflate(LayoutInflater.from(context), R.layout.view_menu_line_check, this, true)
        b.title = title
        b.desc = desc
        modeSW = (onClickCB == null)   // 모드선택 SW or CB
        if (modeSW) b.swCheck = checked else b.cbCheck = checked

        lllogI("ViewMenuLineCheck   modeSW $modeSW  checked $checked")

        b.cons.setOnClickListener {
            lllogI("ViewMenuLineCheck cons setOnClickListener ")
            if (modeSW) b.sw.isChecked = !b.sw.isChecked
            else b.cb.isChecked = !b.cb.isChecked
            sendClick(onClickSW, onClickCB)
        }
        b.sw.onClick { sendClick(onClickSW, onClickCB) }
        b.cb.onClick { sendClick(onClickSW, onClickCB) }

    }

    private fun sendClick(onClickSW: ((checked: Boolean) -> Unit)?, onClickCB: ((checked: Boolean) -> Unit)?) {
        if (modeSW) onClickSW?.invoke(b.sw.isChecked)
        else onClickCB?.invoke(b.cb.isChecked)
    }


//
//    constructor(context: Context, attrs: AttributeSet?) : super(context, attrs) {
//        init()
//    }
//
//    constructor(context: Context, attrs: AttributeSet?, defStyleAttr: Int) : super(context, attrs, defStyleAttr) {
//        init()
//    }
//


}