package com.tkip.mycode.funs.custom.toolbar

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.widget.LinearLayout
import androidx.databinding.DataBindingUtil
import com.tkip.mycode.R
import com.tkip.mycode.databinding.ViewToolBarIconBinding
import com.tkip.mycode.init.inter.interToolbarIcon

/**
 * used in  .. tag acti
 */
class ViewToolbarIcon : LinearLayout {
    lateinit var b: ViewToolBarIconBinding

    constructor(context: Context) : super(context)


    /**     타이틀 + back     */
    constructor(context: Context, title: String?, onBack: ((View) -> Unit)?) : super(context) {
        init(context, null, title, null, null, null, onBack, null, null, null)
    }

    /**     * 버튼 1개     */
    constructor(context: Context, onBack: ((View) -> Unit)?, title: String?, menuText: String?, onTextMenu: ((View) -> Unit)?, onMore: ((View) -> Unit)?) : super(context) {
        init(context, null, title, menuText, null, onMore, onBack, null, onTextMenu, null)
    }

    /**     * 버튼 2개  활성화    */
    constructor(context: Context, onBack: ((View) -> Unit)?, title: String?, menuText: String?,menuText2: String?, onTextMenu: ((View) -> Unit)?,onTextMenu2: ((View) -> Unit)?, onMore: ((View) -> Unit)?) : super(context) {
        init(context, null, title, menuText, menuText2, onMore, onBack, null, onTextMenu, onTextMenu2)
    }

    /**     * 버튼 1개     */
    constructor(context: Context, onBack: ((View) -> Unit)?, topTitle: String?, onTopTitle: ((View) -> Unit)?, title: String?, menuText: String?, onTextMenu: ((View) -> Unit)?, onMore: ((View) -> Unit)?) : super(context) {
        init(context, topTitle, title, menuText, null, onMore, onBack, onTopTitle, onTextMenu, null)
    }

//    /**     * 버튼 2개     */
//    constructor(context: Context, onBack: ((View) -> Unit)?, topTitle: String?, onTopTitle: ((View) -> Unit)?, title: String?, menuText: String?, menuText2: String?, onTextMenu: ((View) -> Unit)?, onTextMenu2: ((View) -> Unit)?, onMore: ((View) -> Unit)?) : super(context) {
//        init(context, topTitle, title, menuText, menuText2, onMore, onBack, onTopTitle, onTextMenu, onTextMenu2)
//    }

    private fun init(context: Context, topTitle: String?, title: String?, menuText: String?, menuText2: String?, onMore: ((View) -> Unit)?, onBack: ((View) -> Unit)?, onTopTitle: ((View) -> Unit)?, onTextMenu: ((View) -> Unit)?, onTextMenu2: ((View) -> Unit)?) {
        b = DataBindingUtil.inflate(LayoutInflater.from(context), R.layout.view_tool_bar_icon, this, true)
        b.backIcon = true  // backIcon or menuIcon
        b.topTitle = topTitle
        b.title = title
        b.icon = null
        b.menuText = menuText
        b.menuText2 = menuText2
        b.hasMore = onMore != null

        b.inter = interToolbarIcon(
                onFirst = { onBack?.invoke(it) },
                onTopTitle = { onTopTitle?.invoke(it) },
                onTextMenu = { onTextMenu?.invoke(it) },
                onTextMenu2 = { onTextMenu2?.invoke(it) },
                onIcon = { },
                onMore = { onMore?.invoke(it) }
        )
    }

    fun setTopTitle(title: String) {
        b.topTitle = title
    }


}