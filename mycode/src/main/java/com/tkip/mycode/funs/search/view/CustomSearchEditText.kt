package com.tkip.mycode.funs.search.view

import android.content.Context
import android.view.LayoutInflater
import android.widget.LinearLayout
import androidx.databinding.DataBindingUtil
import com.tkip.mycode.R
import com.tkip.mycode.databinding.CustomSearchEdittextBinding
import com.tkip.mycode.funs.common.*
import com.tkip.mycode.init.base.getArraySplitSpace
import com.tkip.mycode.init.base.removeLongSpace
import com.tkip.mycode.init.base.removeSpecial
import com.tkip.mycode.util.tools.anim.hideOverDown
import com.tkip.mycode.util.tools.anim.showOverUp

/**
 * search EditText
 * 검색창 : using tagList
 */
class CustomSearchEditText : LinearLayout {
    lateinit var b: CustomSearchEdittextBinding
    private val wordList = arrayListOf<String>()

    constructor(context: Context) : super(context)
    constructor(context: Context, onClear: () -> Unit, onSearch: ((wordList: ArrayList<String>?) -> Unit)?) : super(context) {
        b = DataBindingUtil.inflate(LayoutInflater.from(context), R.layout.custom_search_edittext, this, true)


        b.tagView.setOnTagClickListener { _, p ->
            b.tagView.remove(p)
            onSearch?.invoke(resetWordList())
            showhideTvInfo()
        }
//        b.tagView.setOnTagDeleteListener { _, _, _ -> }
//        b.tagView.setOnTagLongClickListener { _, _ -> }

        b.btnSearch.onClick { onSearch?.invoke(resetWordList()) }

        b.btnClearEt.onClick {
            b.etSearch.setText("")
            onClear()
        }

        /** * 실시간 태그 검색 */
        b.etSearch.textListener(after = { s ->
            OnVisible.toggleSimple(!s.isNullOrBlank(), b.btnClearEt)   // x버튼 토글
            b.btnSearch.isEnabled = !s.isNullOrBlank()      // search버튼 가능/불가
            onSearch?.invoke(resetWordList())
        }, before = { _, _, _, _ -> }, on = { s, _, _, c -> })
    }


    private fun resetWordList(): ArrayList<String> {
//        val list = arrayListOf<String>()
        wordList.clear()
        b.tagView.tags.forEach { wordList.add(it.text) }   // tagView 넣기.
        b.etSearch.keyword()?.let {
            it.removeSpecial().removeLongSpace().trim().getArraySplitSpace().forEach { str -> wordList.add(str) } // keyword 다듬어서 넣기.
        }
        lllogI("getWordList list ${wordList.size}")
        return wordList
    }

    private fun afterSearch() {
        b.etSearch.setText("")
    }


    /**     * info 보이기 감추기     */
    private fun showhideTvInfo() {
        if (b.tagView.tags.isEmpty()) b.tvInfo.showOverUp(false)
        else b.tvInfo.hideOverDown(false)
    }


}