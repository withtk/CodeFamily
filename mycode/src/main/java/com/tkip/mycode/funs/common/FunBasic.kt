package com.tkip.mycode.funs.common

import android.os.Handler
import android.util.Log
import com.tkip.mycode.R
import com.tkip.mycode.dialog.toast.TToast
import com.tkip.mycode.dialog.toast.toastNormal
import com.tkip.mycode.init.base.Cons
import com.tkip.mycode.init.base.getStr

/** lllogI는 myCode모듈에서만 사용가능 */
/*************************************************************************************/

var showLog = false   // PhApp 에서 수정할 것.


/** * method 시작 알림 */
fun lllogM(str: String?) {
    if (showLog) Log.i(Cons.TAG, "++++@@@@++++ $str ")
}


fun lllogI(str: String?) {
    if (showLog) Log.i(Cons.TAG, str ?: "none")
//    Crashlytics.log(str)
//    Crashlytics.log(Log.DEBUG, "MyCode", str)
}

fun lllogI(strId:Int) {
    if (showLog) Log.i(Cons.TAG, strId.getStr())
}

fun lllogItoast(str: String?) {
    if (showLog) lllogI(str)
    toastNormal(str)
}

fun lllogW(str: String?) {
    if (showLog) Log.w(Cons.TAG, str ?: "none")
}

fun lllogW(strId: Int) {
    if (showLog) Log.w(Cons.TAG, strId.getStr())
}

fun lllogW(e: Exception?) {
    if (showLog) Log.w(Cons.TAG, e)
}

fun lllogW(str: String?, e: Throwable?) {
    if (showLog) Log.w(Cons.TAG, str, e)
}

fun lllogD(str: String?) {
    if (showLog) Log.d(Cons.TAG, str ?: "none")
}

fun lllogD(strId: Int) {
    if (showLog) Log.w(Cons.TAG, strId.getStr())
}

fun lllogD(str: String?, e: Throwable?) {
    if (showLog) Log.d(Cons.TAG, str, e)
}

fun lllogE(str: String?) {
    if (showLog) Log.e(Cons.TAG, str ?: "none")
}

fun lllogE(strId: Int) {
    if (showLog) Log.w(Cons.TAG, strId.getStr())
}

fun lllogE(str: String?, t: Throwable?) {
    if (showLog) Log.e(Cons.TAG, str + t?.message, t)
}


/**
 * Toast 날리고나서 true도 날려줄 때.
 */
fun trueToast(msg: String): Boolean {
    TToast.showAlert(msg)
    return true
}

fun trueToast(strId: Int): Boolean {
    TToast.showAlert(strId)
    return true
}

/**
 * when문 else로 빠졌을 때.
 */
fun errorWhen() {
    TToast.showNormal(R.string.error_when_else)
}


/**
 * 간편 Handler
 */
fun hhh(interval:Int,on:()->Unit) {
    Handler().postDelayed(on,interval.toLong())
}