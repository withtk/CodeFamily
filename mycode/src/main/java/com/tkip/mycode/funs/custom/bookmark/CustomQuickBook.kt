package com.tkip.mycode.funs.custom.bookmark

import android.content.Context
import android.view.LayoutInflater
import android.widget.LinearLayout
import androidx.databinding.DataBindingUtil
import com.tkip.mycode.R
import com.tkip.mycode.databinding.CustomQuickBookBinding
import com.tkip.mycode.funs.common.onClick
import com.tkip.mycode.model.count.my_book_push.CountAdapter

/**
 * 즐겨찾기 in 서랍
 */
class CustomQuickBook : LinearLayout {
    lateinit var b: CustomQuickBookBinding
    private lateinit var adapterBookBoard: CountAdapter


    constructor(context: Context) : super(context)
    constructor(context: Context, anonymous: Boolean, uid: String, addDate: Long, onRefresh:( () -> Unit)?, onSet: (() -> Unit)?) : super(context) {
        b = DataBindingUtil.inflate(LayoutInflater.from(context), R.layout.custom_quick_book, this, true)

        b.btnRefresh.onClick { onRefresh?.invoke() }
        b.btnSetting.onClick { onSet?.invoke() }




    }


}