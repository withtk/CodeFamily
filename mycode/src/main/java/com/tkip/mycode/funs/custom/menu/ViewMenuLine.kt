package com.tkip.mycode.funs.custom.menu

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.widget.LinearLayout
import androidx.core.util.Pair
import androidx.databinding.DataBindingUtil
import com.tkip.mycode.R
import com.tkip.mycode.admin.menu.AMenu
import com.tkip.mycode.databinding.ViewMenuLineBinding
import com.tkip.mycode.funs.common.arrTranToolbar
import com.tkip.mycode.funs.common.lllogI
import com.tkip.mycode.init.V_TYPE1
import com.tkip.mycode.init.base.getClr


/**
 * 한줄짜리 메뉴
 */
class ViewMenuLine : LinearLayout {
    private lateinit var b: ViewMenuLineBinding

    constructor(context: Context) : super(context)
    constructor(context: Context, type: Int?, aMenu: AMenu,  onClick: ((arrPair: Array<androidx.core.util.Pair<View, String>>?) -> Unit)?) : super(context) {
        init(context, aMenu, aMenu.title, aMenu.desc, onClick, type)
    }
    constructor(context: Context, type: Int?,  title: String, desc: String?, onClick: ((arrPair: Array<androidx.core.util.Pair<View, String>>?) -> Unit)?) : super(context) {
        init(context, null,title, desc, onClick, type)
    }

    private fun init(context: Context, aMenu: AMenu?, title: String, desc: String?, onClick: ((arrPair: Array<Pair<View, String>>?) -> Unit)?, type: Int?) {
        b = DataBindingUtil.inflate(LayoutInflater.from(context), R.layout.view_menu_line, this, true)
        b.title = title
        b.desc = desc

        b.cons.setOnClickListener {
            lllogI("ViewMenuLine cons setOnClickListener ")

            onClick?.let {
                it(b.tvTitle.arrTranToolbar())
                return@setOnClickListener
            }

            aMenu?.onClickMenu(context,b.tvTitle.arrTranToolbar())
        }


        type?.let {
            when (it) {
                V_TYPE1 -> b.tvTitle.setTextColor(R.color.colorAccent2.getClr())
            }
        }
    }


}