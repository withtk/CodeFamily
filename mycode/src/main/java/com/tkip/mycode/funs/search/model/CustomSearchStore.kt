package com.tkip.mycode.funs.search.model

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.widget.LinearLayout
import androidx.databinding.DataBindingUtil
import com.cunoraz.tagview.TagView
import com.tkip.mycode.R
import com.tkip.mycode.databinding.CustomSearchStoreBinding
import com.tkip.mycode.funs.common.*
import com.tkip.mycode.funs.custom.text.ViewTvInfoBig
import com.tkip.mycode.init.RV_VERT
import com.tkip.mycode.init.base.Sred
import com.tkip.mycode.init.base.getInt
import com.tkip.mycode.init.base.getStr
import com.tkip.mycode.init.inter.interSearchTagview
import com.tkip.mycode.model.my_enum.Status
import com.tkip.mycode.model.mytag.*
import com.tkip.mycode.model.store.Store
import com.tkip.mycode.nav.store.StoreAdapter
import com.tkip.mycode.util.lib.retrofit2.ESget
import com.tkip.mycode.util.lib.retrofit2.ESquery
import com.tkip.mycode.util.tools.anim.*
import com.tkip.mycode.util.tools.recycler.OnRv

/**
 * tagView를 이용한 검색.
 * 일단 store만.
 *
 */
class CustomSearchStore : LinearLayout {
    lateinit var b: CustomSearchStoreBinding

    lateinit var tagAdapter: MyTagAdapter
    private var rvType = RV_VERT
    private val tagItems = arrayListOf<MyTag>()
    private val storeItems = arrayListOf<Store>()
    private lateinit var storeAdapter: StoreAdapter

//    private lateinit var customSearchTag: CustomSearchTag
//    private lateinit var customSearchEditText: CustomSearchEditText
//    private lateinit var customSearchTagEditText: CustomSearchTagEdittext

    private val esSize by lazy { R.integer.es_size_post.getInt() }

    private var esLastIndex = 0

    constructor(context: Context) : super(context)

    constructor(context: Context, myDataOnly: Boolean, tagType: TagTYPE, rvType: Int, onTagView: ((View) -> Unit)?, onSearch: ((TagView, String?) -> Unit)?, onLoadMore: ((TagView, String?) -> Unit)?) : super(context) {
        b = DataBindingUtil.inflate(LayoutInflater.from(context), R.layout.custom_search_store, this, true)
        b.myDataOnly = myDataOnly
        this.rvType = rvType

        /*** 검색 안내 */
        b.lineInfo.addView(ViewTvInfoBig(context, R.string.info_search_store_ready.getStr(), R.string.info_search_store_way.getStr()))

        /*** 클릭 리스너 */
        b.inter = interSearchTagview(
                onTagView = { },
                onSearch = { getEsDataList(0) })

        b.tagView.setOnTagClickListener { _, p ->
            b.tagView.remove(p)
//            onSearch?.invoke(b.tagView, b.etSearch.keyword())
            getEsDataList(0)
            toggleTopTvInfo()
        }
//        b.tagView.setOnTagDeleteListener { _, _, _ -> }
//        b.tagView.setOnTagLongClickListener { _, _ -> }


//        b.refresh.setEnableRefresh(false)   //상단 refresh는 불능
//        b.refresh.setEnableLoadMore(false)  // 하단 loadMore 불능.
        b.refresh.setOnRefreshListener {
            getEsDataList(0)
        }
        b.refresh.setOnLoadMoreListener {
            getEsDataList(esLastIndex)
            onLoadMore?.invoke(b.tagView, b.etSearch.keyword())
        }

        /** * 태그 adapter  */
        tagAdapter = MyTagAdapter(tagItems, MyTagAdapter.rvNormal,
                /**   * MyTagAdapter에서  아이템 클릭.    */
                onItemClick = { _, _, myTag ->
                    b.etSearch.setText("")
                    addTagAndSearch(myTag.name)
                })
        b.rvTag.adapter = tagAdapter


        /*** 처음 포커싱 됐을때 지난 검색결과 보이기 */
        b.etSearch.setOnFocusChangeListener { _, _ ->
            b.lineInfo.hideFadeOut(false)    // 포커스 되면 안내 제거.
//            lllogD("etSearch setOnFocusChangeListener  tagItems.isEmpty():${tagItems.isEmpty()} etSearch:${b.etSearch.keyword() }")
//            if (hasFocus && tagItems.isEmpty() && b.etSearch.keyword() == null) showLastKeyword()
        }


        /*** 클릭시 지난 검색결과 보이기 */
        b.etSearch.setOnClickListener {
            if (b.etSearch.keyword() == null) showLastKeyword()
//            if (b.etSearch.keyword() == null) if (isLastKeyword) hideTagRv() else showLastKeyword()
        }


        /** * 실시간 태그 검색 */
        b.etSearch.textListener(after = { s ->
            OnVisible.toggleSimple(!s.isNullOrBlank(), b.btnClearEt)   // x버튼 토글
            b.btnSearch.isEnabled = !s.isNullOrBlank()      // search버튼 가능/불가
            b.tvNoResult.gone() // 노데이타 hide

            b.etSearch.keyword()?.let { keyword ->
                OnTag.getEsTagList(10, 0, tagType, keyword,
                        success = {
                            lllogI("ViewSearchTagView list.size :${it?.size}")
                            it.addCheckTagView(tagItems, b.tagView)
                            afterSearch()
                        }, fail = { afterSearch() })
            } ?: let {
                // keyword 널이면 list 지워.
                tagItems.clear()
                afterSearch()
            }
        }, before = { _, _, _, _ -> }, on = { s, _, _, c -> })


        /** set adapter */
        setAdapter()

        b.btnClearEt.onClick {
            b.etSearch.setText("")
        }


    }

    fun addTagAndSearch(name: String) {
        OnTag.saveMyKeywordList(Sred.ARR_STORE_KEYWORD,name)
        b.tagView.checkAdd(TAG_2, name)    // tagView에 추가
        tagItems.clear()
        afterSearch()
        toggleTopTvInfo()
        //                    onSearch?.invoke(b.tagView, null)    // keyword는 무조건 null.
        getEsDataList(0)
    }


    private fun setAdapter() {
        storeAdapter = StoreAdapter(storeItems, rvType, null)
        b.rv.adapter = storeAdapter
    }

    /**     * 지난 검색어 표시     */
    private fun showLastKeyword() {
        tagItems.clear()
        OnTag.loadMyKeywordList(Sred.ARR_KEYWORD)?.forEach { tagItems.add(MyTag(it)) }
        afterSearch()
        b.btnClearEt.visi()
    }

    private fun afterSearch() {
        b.avLoading.hideFadeOut(true)
        if (tagItems.isEmpty()) b.rvTag.gone()
        else b.rvTag.visi()
        tagAdapter.notifyDataSetChanged()
    }


    /**     * info 결과없음 보이기 감추기     */
    private fun toggleTopTvInfo() {
        if (b.tagView.tags.isEmpty()) b.tvInfo.showOverUp(false)
        else b.tvInfo.hideOverDown(false)
    }


    /** * 가져오기  */
    private fun getEsDataList(from: Int) {

        b.avLoading.visi()

        val query = ESquery.storeSearch(esSize, from, ESquery.getTermTaglist(b.tagView, b.etSearch.keyword()), Status.NORMAL)
        ESget.getStoreList(query,
                success = {
                    b.lineInfo.hide()    // 매뉴얼은 무조건 가려줘.
                    lllogI("CustomSearchTagView list.size : ${it?.size}")
                    esLastIndex = OnRv.setSearchResult(it, from, storeItems, b.tvNoResult, R.string.info_no_search_result_store.getStr(), b.avLoading, b.refresh)
                    storeAdapter.notifyDataSetChanged()
                    lllogI("CustomSearchTagView childCount ${b.rv.childCount}")

                }, fail = { })

    }


}