package com.tkip.mycode.funs.custom.toolbar

import android.content.Context
import android.view.LayoutInflater
import android.widget.LinearLayout
import androidx.databinding.DataBindingUtil
import com.tkip.mycode.R
import com.tkip.mycode.databinding.ViewToolBarBinding
import com.tkip.mycode.init.inter.interToolbar

/**
 * add 없는 toolbar
 * used in postActivity..
 */
class ViewToolbar : LinearLayout {
    lateinit var b: ViewToolBarBinding

    constructor(context: Context) : super(context)
    constructor(context: Context, boardTitle: String?, title: String?, addDate: Long, onBack: () -> Unit, onBoardTitle: () -> Unit, onMore: () -> Unit) : super(context) {
        b = DataBindingUtil.inflate(LayoutInflater.from(context), R.layout.view_tool_bar, this, true)

        b.inter = interToolbar(
                onBack = { onBack() },
                onBoardTitle = { onBoardTitle() },
                onMore = { onMore() })

        b.boardTitle = boardTitle
        b.title = title
        b.addDate = addDate

    }


}