package com.tkip.mycode.funs.common

import com.tkip.mycode.init.NOTICE_KEY
import com.tkip.mycode.init.base.Sred
import com.tkip.mycode.model.manage.Notice


/*************************************************************************************/
/********************  ***********************************************************/
/*************************************************************************************/


/**
 * Sred에 넣기
 */
fun Notice.put() {
    Sred.putObjectNotice(NOTICE_KEY, this)
}

fun getMyNotice() = Sred.getObjectNotice(NOTICE_KEY)


/**
 * 새로운 Notice 만들어 가져오기
 */
fun Notice.get(title: String, contents: String, urgent: Boolean) = Notice(this).apply {
    this.no += 1
    this.title = title
    this.comment = comment
    this.urgent = urgent
    this.addDate = System.currentTimeMillis()
}


//fun Notice.checkNoticeNo(context: Context) {
//    getMyNotice()?.let {
//        if (it.no.lt(this.no)) {
//            put()
//            MyAlert.showNotice(context, this)
//        }
//    } ?: put()
//}
