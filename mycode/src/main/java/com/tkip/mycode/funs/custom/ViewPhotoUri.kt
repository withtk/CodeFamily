package com.tkip.mycode.funs.custom

import android.content.Context
import android.net.Uri
import android.view.LayoutInflater
import android.widget.LinearLayout
import androidx.databinding.DataBindingUtil
import com.tkip.mycode.R
import com.tkip.mycode.databinding.ViewPhotoUriBinding
import com.tkip.mycode.init.inter.interPhotoUri
import com.tkip.mycode.util.lib.photo.util.PhotoUtil
import com.tkip.mycode.util.tools.anim.hideSlideDown

/**
 * reply에 사진 추가할때 사용.
 */
class ViewPhotoUri : LinearLayout {
    private lateinit var b: ViewPhotoUriBinding

    constructor(context: Context) : super(context)
    constructor(context: Context, uri: Uri, onPhoto: () -> Unit, onClose: () -> Unit) : super(context) {
        b = DataBindingUtil.inflate(LayoutInflater.from(context), R.layout.view_photo_uri, this, true)

        b.inter = interPhotoUri(
                onClickPhoto = { onPhoto() },
                onClickClose = { onClickClose(onClose) }
        )

        /** set URI */
        b.ivPhoto.setImageURI(uri)

        /**
         * upload할 포토에 추가
         */
        PhotoUtil.resetAllUriList()
        PhotoUtil.photoUris.add(uri)


    }

    private fun onClickClose(onClose: () -> Unit) {
        b.lineContents.hideSlideDown(true)
        PhotoUtil.resetAllUriList()
        onClose()
    }


}