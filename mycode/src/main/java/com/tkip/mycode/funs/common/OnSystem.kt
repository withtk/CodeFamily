package com.tkip.mycode.funs.common

import android.app.Activity
import android.app.AlarmManager
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.os.Handler
import com.tkip.mycode.R
import com.tkip.mycode.admin.versionGteQ
import com.tkip.mycode.dialog.alert.MyAlert
import com.tkip.mycode.init.MainActivity
import com.tkip.mycode.init.base.Sred
import com.tkip.mycode.init.base.getStr
import com.tkip.mycode.init.BOARD_NO1
import com.tkip.mycode.init.part.CodeApp
import com.tkip.mycode.model.board.OnBoard
import kotlin.system.exitProcess


class OnSystem {

    companion object {


        /*******************************************************************************************
         * 초기 보드 셋팅
         * recentBoard, bestBoard, FLeaBoard, ROomBoard
         *******************************************************************************************/
        fun initBoard(onSuccess: (() -> Unit)?, onFail: () -> Unit) {

            /*** 베스트 보드 받아오기 & refresh */
            OnBoard.getBestBoardList(1,
                    onSuccess = {
                        if (it.isNullOrEmpty()) retryInitBoard(onSuccess, onFail)
                        else {
                            BOARD_NO1 = it[0]
                            Sred.putObjectBoard(Sred.BOARD_NO1, BOARD_NO1)
                            lllogD("initBoard#  initBoard success board1:$BOARD_NO1 ")

                            OnBoard.resetFleaRoomBoard()
                            onSuccess?.invoke()
                        }
                    }, onFail = {
                retryInitBoard(onSuccess, onFail)
            })
        }


        private var ii = 0   // 시도 횟수
        private fun retryInitBoard(onSuccess: (() -> Unit)?, onFail: () -> Unit) {
            if (ii < 2) {
                lllogI("initBoard# initBoard retryInitBoard ii:$ii")
                ii++
//                myRemoteConfigFetch({}, {})   // 리모트 다시 받아오기.
//                lllogI("initBoard# initBoard start es: ${R.string.remote_elasticsearch_id.remoteStr()} / ${R.string.remote_elasticsearch_pw.remoteStr()}")

                Handler().postDelayed({
//                Retro.resetEsApi()                      // Retro 리셋
                  initBoard(onSuccess, onFail)   // 필수데이타 다시 받아오기
                }, 1000)
            } else {
                /*** 3회 시도 초과시 */
                onFail()   // 최종적으로 날리는 onFail()
//                context?.let { con -> checkElasticAuth(con) }   // show 다이얼로그
            }
        }


        /***
         * < elastic auth 체크 >
         * show Dialog  : fail이면 finish()
         * */
        fun checkElasticAuth(context: Context) {
//            lllogI("initBoard# checkElasticAuth es: ${R.string.remote_elasticsearch_id.remoteStr()} / ${R.string.remote_elasticsearch_pw.remoteStr()}")

            if (versionGteQ)
                MyAlert.showConfirm(context, null, R.string.error_remote_reset_q.getStr(), null, false, false,
                        ok = {
                            //                            myRemoteConfigFetch()    // 리모트 다시 받아오기.
                            (context as Activity).finish()
                        }, cancel = null)
            else MyAlert.showConfirm(context, null, R.string.error_remote_reset.getStr(),
                    R.string.btn_restart.getStr(), false, false,
                    ok = {

                        /*** application 재실행 using AlarmManager*/
                        val intent = Intent(context, MainActivity::class.java)
                        intent.putExtra("crash", true)
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP
                                or Intent.FLAG_ACTIVITY_CLEAR_TASK
                                or Intent.FLAG_ACTIVITY_NEW_TASK)
                        val pendingIntent = PendingIntent.getActivity(CodeApp.getAppContext(), 0, intent, PendingIntent.FLAG_ONE_SHOT)
                        val mgr = CodeApp.getAppContext().getSystemService(Context.ALARM_SERVICE) as AlarmManager
                        mgr[AlarmManager.RTC, System.currentTimeMillis() + 100] = pendingIntent

//                        myRemoteConfigFetch()    // 리모트 다시 받아오기.
                        (context as Activity).finish()
                        exitProcess(2)


                        /*** activity  재실행 */
//                        val mStartActivity = Intent(context, MainActivity::class.java)
//                        val mPendingIntentId = 123456
//                        val mPendingIntent = PendingIntent.getActivity(context, mPendingIntentId, mStartActivity, PendingIntent.FLAG_CANCEL_CURRENT)
//                        val mgr = context.getSystemService(Context.ALARM_SERVICE) as AlarmManager
//                        mgr[AlarmManager.RTC, System.currentTimeMillis() + 100] = mPendingIntent
//                        (context as Activity).finish()
//                        exitProcess(0)
                    }, cancel = null)

        }


    }

}