package com.tkip.mycode.funs.search

import android.content.Context
import android.content.Intent
import android.view.Gravity
import android.view.View
import android.widget.EditText
import android.widget.LinearLayout
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.google.android.libraries.places.api.model.AutocompletePrediction
import com.google.android.libraries.places.api.net.PlacesClient
import com.mancj.slideup.SlideUp
import com.mancj.slideup.SlideUpBuilder
import com.tkip.mycode.R
import com.tkip.mycode.funs.common.lllogI
import com.tkip.mycode.funs.common.startActi
import com.tkip.mycode.util.lib.map.OnMap
import com.tkip.mycode.util.tools.anim.AnimUtil


class OnSearch {

    companion object {


        fun gotoSearchTotalActivity(context: Context, arrPair: Array<androidx.core.util.Pair<View, String>>?) {
            val intent = Intent(context, SearchTotalActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
            startActi(context, intent, null)
        }


        /**
         * slideUp
         */
        fun initSlideUp(root:View, view: View, onVisiChanged: ((visibility: Int) -> Unit)?, onSlide: ((percent: Float) -> Unit)?) =
                SlideUpBuilder(view)
                        .withStartState(SlideUp.State.HIDDEN)
                        .withStartGravity(Gravity.TOP)
                        .withLoggingEnabled(true)
                        .withSlideFromOtherView(root)
                        //.withGesturesEnabled()
                        //.withHideSoftInputWhenDisplayed()
                        //.withInterpolator()
                        //.withAutoSlideDuration()
                        //.withTouchableAreaPx()
                        //.withTouchableAreaDp()
                        //.withListeners()
                        //.withSavedState()
                        .withListeners(object : SlideUp.Listener.Events {
                            override fun onVisibilityChanged(visibility: Int) {
                                lllogI("SlideUp onVisibilityChanged visibility : $visibility")
                                onVisiChanged?.invoke(visibility)
                            }

                            override fun onSlide(percent: Float) {
                                lllogI("SlideUp onSlide percent : $percent")
                                onSlide?.invoke(percent)
                            }
                        })
                        .build()


        fun controlVisibility(items: ArrayList<*>, rv: RecyclerView, tvNoData: TextView) {
            if (items.isEmpty()) {
                AnimUtil.hideSlideUp(rv, true)
                AnimUtil.showSlideDown(tvNoData, true)
            } else {
                AnimUtil.showSlideDown(rv, true)
                AnimUtil.hideSlideUp(tvNoData, true)
            }
        }


        fun resetSearch(linear: LinearLayout, rv: RecyclerView, tv: TextView) {
            val et = linear.getChildAt(0).findViewById<EditText>(R.id.et_search)
            et.setText("")

            AnimUtil.hideSlideUp(rv, true)
            AnimUtil.hideSlideUp(tv, true)
        }


        /**********************************************************
         ********** Place *************************************
         **********************************************************/

        /**
         * prediction 실시간 검색
         */
        fun getPrediction_inList(placesClient: PlacesClient, items: ArrayList<AutocompletePrediction>, str: String, result: (ArrayList<AutocompletePrediction>) -> Unit) {

            OnMap.getPrediction(placesClient, str,
                    success = { response ->
                        items.clear()
                        for (prediction in response.autocompletePredictions) {
                            items.add(prediction)
                        }

                        result(items)
                    }, fail = {})

        }


    }

}