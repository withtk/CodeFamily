package com.tkip.mycode.funs.custom.expandable

import android.content.Context
import android.view.LayoutInflater
import android.widget.LinearLayout
import androidx.databinding.DataBindingUtil
import com.tkip.mycode.R
import com.tkip.mycode.databinding.ViewExpandableCardBinding
import com.tkip.mycode.init.inter.interBasicClick
import com.tkip.mycode.util.tools.anim.AnimUtil
import com.tkip.mycode.util.tools.anim.visi

/**
 * for store info
 */
class ViewExpandableCard : LinearLayout {
    lateinit var b: ViewExpandableCardBinding

    constructor(context: Context) : super(context)
    constructor(context: Context, title: String, contents: String?, isOpen: Boolean, onClick: (ViewExpandableCardBinding) -> Unit) : super(context) {
        b = DataBindingUtil.inflate(LayoutInflater.from(context), R.layout.view_expandable_card, this, true)
        b.inTitle.title = title
        b.contents = contents

        b.inTitle.inter = interBasicClick({
            onClick(b)
            AnimUtil.rotateArrow(b.inTitle.ivArrow)
            AnimUtil.toggleUpDown(b.lineContents, true)
        }, null, null)

//        if (isOpen) b.inTitle.lineTitle.performClick()   // 초기값에 따라 오픈여부 결정.
        if (isOpen) b.lineContents.visi()
    }
}