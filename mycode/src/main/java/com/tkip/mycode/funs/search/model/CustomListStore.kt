package com.tkip.mycode.funs.search.model

import android.content.Context
import android.view.LayoutInflater
import android.widget.LinearLayout
import androidx.databinding.DataBindingUtil
import com.tkip.mycode.R
import com.tkip.mycode.databinding.CustomModelListBinding
import com.tkip.mycode.model.store.Store
import com.tkip.mycode.nav.store.StoreAdapter
import com.tkip.mycode.util.lib.retrofit2.ESget
import com.tkip.mycode.util.lib.retrofit2.ESquery
import com.tkip.mycode.util.tools.recycler.OnRv

/**
 */
class CustomListStore : LinearLayout {
    private lateinit var b: CustomModelListBinding
//    private var uid: String = myuid()
//    private var costType: CostType = CostType.ACT

    private var storeAdapter: StoreAdapter? = null
    private val items by lazy { arrayListOf<Store>() }

    private val esSize = 15
    private var esLastIndex = 0

    constructor(context: Context) : super(context)
    constructor(context: Context, rvType:Int,  onItem: ((Store) -> Unit)?) : super(context) {
        b = DataBindingUtil.inflate(LayoutInflater.from(context), R.layout.custom_model_list, this, true)
//        uid?.let { this.uid = it }
//        this.costType = costType

        /** set Adapter */
        storeAdapter = StoreAdapter(items,rvType, onItem)
        b.inRefresh.rv.adapter = storeAdapter

        b.inRefresh.refresh.setOnRefreshListener { getEsDataList(0) }
        b.inRefresh.refresh.setOnLoadMoreListener { getEsDataList(esLastIndex) }
//        b.refresh.setEnableRefresh(false)   // 상단 refresh는 불능.
//        b.refresh.setEnableLoadMore(false)  // 하단 loadMore 불능.


    }

    /** * 가져오기  */
    fun getEsDataList(from: Int ) {
            val query = ESquery.matchAll(esSize,from)
            ESget.getStoreList(query,
                    success = {
                        esLastIndex = OnRv.setRvRefresh(it, from, items, b.inRefresh, true)
                        storeAdapter?.notifyDataSetChanged()
                    }, fail = { })
    }


}