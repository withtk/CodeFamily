package com.tkip.mycode.funs.search.view

import android.content.Context
import android.view.LayoutInflater
import android.widget.LinearLayout
import androidx.databinding.DataBindingUtil
import com.tkip.mycode.R
import com.tkip.mycode.databinding.CustomStoreSearchDetailBinding


/**
 * search Slide
 */
class CustomStoreSearchDetail : LinearLayout {
    private lateinit var b: CustomStoreSearchDetailBinding

    constructor(context: Context) : super(context)
    constructor(context: Context,  onBtn: (() -> Unit)? , onTag: ((visibility: Int) -> Unit)?) : super(context) {
        b = DataBindingUtil.inflate(LayoutInflater.from(context), R.layout.custom_store_search_detail, this, true)

//        b.lineCover.addView(ViewFlowBtn())

    }

}


