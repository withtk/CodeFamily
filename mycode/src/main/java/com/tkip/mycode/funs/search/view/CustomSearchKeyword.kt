package com.tkip.mycode.funs.search.view

import android.content.Context
import android.view.LayoutInflater
import android.widget.LinearLayout
import androidx.databinding.DataBindingUtil
import com.tkip.mycode.R
import com.tkip.mycode.databinding.CustomSearchKeywordBinding
import com.tkip.mycode.funs.common.OnVisible
import com.tkip.mycode.funs.common.keyword
import com.tkip.mycode.funs.common.onClick
import com.tkip.mycode.funs.common.textListener

/**
 * keyword만으로 하는 검색.
 * store, flea, room, post
 */
class CustomSearchKeyword : LinearLayout {
    lateinit var b: CustomSearchKeywordBinding

    val esSize = 10
    var esLastIndex = 0


    constructor(context: Context) : super(context)
    constructor(context: Context, onSearch: (String) -> Unit, onLoadMore: (String) -> Unit) : super(context) {
        b = DataBindingUtil.inflate(LayoutInflater.from(context), R.layout.custom_search_keyword, this, true)

        /*** Model 선택하는 라디오 추가 */
//        val list = arrayListOf<String>()
//        val modelArr = arrayOf(DataType.STORE, DataType.FLEA, DataType.ROOM).apply {
//            forEach { list.add(it.title) }
//        }
//        val rgBasic = ViewRGbasic(context, list,
//                onSelect = { lllogI("ViewRGbasic onSelect ${modelArr[it]}") })
//        b.frameRg.addView(rgBasic, 0)

        /*** 상단 refresh는 차단 */
        b.refresh.setEnableRefresh(false)
        b.refresh.setOnLoadMoreListener { b.etSearch.keyword()?.let { onLoadMore(it) } }

        /** * 실시간 검색 */
        b.etSearch.textListener(after = { s ->
            OnVisible.toggleSimple(!s.isNullOrBlank(), b.btnClearEt)   // x버튼 토글

            b.etSearch.keyword()?.let { onSearch(it) }

//            b.etSearch.keyword()?.let { keyword ->
//            } ?: let {
//                AnimUtil.hideFadeOut(b.avLoading, true)
//            }

        }, before = { _, _, _, _ -> }, on = { s, _, _, c -> })


        b.btnClearEt.onClick { b.etSearch.setText("") }
    }











}