package com.tkip.mycode.funs.custom.toolbar

import android.content.Context
import android.view.LayoutInflater
import android.widget.LinearLayout
import androidx.databinding.DataBindingUtil
import com.tkip.mycode.R
import com.tkip.mycode.databinding.ViewToolBarEmptyBinding
import com.tkip.mycode.init.inter.interToolbar

/**
 * 단순 toolbar
 */
class ViewToolbarEmpty : LinearLayout {
    lateinit var b: ViewToolBarEmptyBinding

    constructor(context: Context) : super(context)
    constructor(context: Context,     title: String?,  onBack: () -> Unit,   onMore: (() -> Unit)?) : super(context) {
        b = DataBindingUtil.inflate(LayoutInflater.from(context), R.layout.view_tool_bar_empty, this, true)

        b.inter = interToolbar(
                onBack = { onBack() },
                onBoardTitle = {   },
                onMore = { onMore?.invoke() }
        )

        b.title = title
        b.onMore = onMore!=null

    }


}