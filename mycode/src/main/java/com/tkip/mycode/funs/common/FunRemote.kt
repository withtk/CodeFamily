package com.tkip.mycode.funs.common

import com.google.android.gms.tasks.OnCompleteListener
import com.google.firebase.iid.FirebaseInstanceId
import com.google.firebase.remoteconfig.FirebaseRemoteConfig
import com.google.firebase.remoteconfig.FirebaseRemoteConfigSettings
import com.tkip.mycode.R
import com.tkip.mycode.init.base.Sred
import com.tkip.mycode.init.base.getStr


/**
 * Remote 쉽게 받아오기
 */
val remoteConfig: FirebaseRemoteConfig by lazy { FirebaseRemoteConfig.getInstance() }

fun Int.remoteStr(): String = remoteConfig.getString(this.getStr())

fun Int.remoteLong(): Long = remoteConfig.getLong(this.getStr())
fun Int.remoteInt(): Int = this.remoteLong().toInt()
fun Int.remoteBool() = remoteConfig.getBoolean(this.getStr())


//val remoteConfig : FirebaseRemoteConfig? = null
//fun remoteConfig(): FirebaseRemoteConfig? {
//    if(remoteConfig == null)
//    return remoteConfig
//}
fun initRemoteConfig() {
    lllogM("initBoard#  MyApp initRemoteConfig start")

//    lllogD("initBoard#  FunRemote initRemoteConfig id 1111 ${R.string.remote_elasticsearch_id.remoteStr()}")

    val remoteConfig = remoteConfig
    val configSettings = FirebaseRemoteConfigSettings.Builder()
//            .setDeveloperModeEnabled(BuildConfig.DEBUG)
//                .setMinimumFetchIntervalInSeconds(4200)
            .build()

    /* long cacheExpiration = 3600; // 1 hour in seconds.
    // If your app is using developer mode, cacheExpiration is set id 0, so each fetch will
    // retrieve values from the service.
    if (remoteConfig.getInfo().getConfigSettings().isDeveloperModeEnabled()) {
        cacheExpiration = 0;
    }*/

    remoteConfig.setConfigSettingsAsync(configSettings)
    remoteConfig.setDefaults(R.xml.remote_config_defaults)

    myRemoteConfigFetch({}, {})

}


/**
 * remote 다시 가져오기
 */
fun myRemoteConfigFetch(onSuccess: () -> Unit, onFail: () -> Unit) {
//    remoteConfig.fetch(if (BuildConfig.DEBUG) 0 else TimeUnit.MINUTES.toSeconds(10))
    remoteConfig.fetch(0)
            .addOnSuccessListener {
                remoteConfig.activate()
                        .addOnSuccessListener {
                            onSuccess()
//                        lllogI("initBoard#  FunRemote myRemoteConfigFetch onSuccess id 2222 ${R.string.remote_elasticsearch_id.remoteStr()}")
                        }
                        .addOnFailureListener {
                            failMessage(onFail)
                            lllogD("addOnFailureListener failMessage ${it.message}")
                        }
            }
//            .addOnCompleteListener { task ->
//
//                if (task.isSuccessful) {
//                    remoteConfig.activate()
//                            .addOnSuccessListener {
//                                onSuccess()
//                                lllogI("initBoard#  FunRemote myRemoteConfigFetch onSuccess id 2222 ${R.string.remote_elasticsearch_id.remoteStr()}")
//                            }
//                            .addOnFailureListener {
//                                failMessage(onFail)
//                                lllogD("addOnFailureListener failMessage")
//                            }
//
//                } else {
//                    failMessage(onFail)
//                    lllogD("isNotSuccessful failMessage")
//                }
//
//                // --------------------remote data 정리-------------------------  //
//
//            }
            .addOnFailureListener { e ->
                lllogD("addOnFailureListener failMessage")
                failMessage(onFail)
                lllogW(e)
            }
}

private fun failMessage(onFail: () -> Unit) {
    onFail()
//    TToast.showWarning(R.string.db_error_remote_config_get_data)
}


/*************************************************************************************************
 * fCM 토큰 받아오기
 *************************************************************************************************/
fun getFcmToken(onSuccess: (token:String) -> Unit) {

    FirebaseInstanceId.getInstance().instanceId
            .addOnCompleteListener(OnCompleteListener { task ->
                if (!task.isSuccessful) {
                    lllogW("initFcmToken failed task.exception : ${task.exception}")
                    return@OnCompleteListener
                }

                task.result?.token?.let { token ->
                    Sred.putString(Sred.DEVICE_FCM_TOKEN, token)     // token 저장.
                    lllogD("initFcmToken token $token")

                    onSuccess(token)
                }

            })
}

