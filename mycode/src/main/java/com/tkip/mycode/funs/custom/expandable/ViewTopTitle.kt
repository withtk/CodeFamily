package com.tkip.mycode.funs.custom.expandable

import android.content.Context
import android.view.LayoutInflater
import android.widget.LinearLayout
import androidx.databinding.DataBindingUtil
import com.tkip.mycode.R
import com.tkip.mycode.databinding.ViewTopTitleBinding

/**
 * 리스트의 상단 제목
 */
class ViewTopTitle : LinearLayout {
    lateinit var b: ViewTopTitleBinding

    constructor(context: Context) : super(context)
    constructor(context: Context, title: String, showArrow: Boolean) : super(context) {
        b = DataBindingUtil.inflate(LayoutInflater.from(context), R.layout.view_top_title, this, true)

        b.title = title
        b.showArrow = showArrow

    }

}