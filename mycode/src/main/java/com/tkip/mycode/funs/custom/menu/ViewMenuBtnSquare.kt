package com.tkip.mycode.funs.custom.menu

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.widget.LinearLayout
import androidx.databinding.DataBindingUtil
import com.tkip.mycode.R
import com.tkip.mycode.admin.menu.AMenu
import com.tkip.mycode.databinding.ViewMenuBtnSquareBinding
import com.tkip.mycode.init.V_TYPE1
import com.tkip.mycode.init.V_TYPE2
import com.tkip.mycode.init.base.getDrawable
import com.tkip.mycode.util.tools.etc.OnMyClick


/**
 * 네모 버튼 using drawerLayout
 */
class ViewMenuBtnSquare : LinearLayout {
    private lateinit var b: ViewMenuBtnSquareBinding

    constructor(context: Context) : super(context)
    constructor(context: Context, amenu: AMenu, type: Int?, onClick: (view: View) -> Unit) : super(context) {

        b = DataBindingUtil.inflate(LayoutInflater.from(context), R.layout.view_menu_btn_square, this, true)
        b.btnMenu.setOnClickListener {
            if (OnMyClick.isDoubleClick()) return@setOnClickListener
            onClick(it)
        }

        b.amenu = amenu

        type?.let {
            when (it) {
                V_TYPE1 -> b.btnMenu.background = R.drawable.selector_menu_btn_square_accent.getDrawable()  // 외곽선 그리기
                V_TYPE2 -> {
                }
            }

        }
    }


//
//    constructor(context: Context, attrs: AttributeSet?) : super(context, attrs) {
//        init()
//    }
//
//    constructor(context: Context, attrs: AttributeSet?, defStyleAttr: Int) : super(context, attrs, defStyleAttr) {
//        init()
//    }
//


}