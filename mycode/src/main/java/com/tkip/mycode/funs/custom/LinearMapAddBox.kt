package com.tkip.mycode.funs.custom

import android.content.Context
import android.view.LayoutInflater
import android.widget.LinearLayout
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.LatLng
import com.tkip.mycode.R
import com.tkip.mycode.databinding.LinearMapAddBoxBinding
import com.tkip.mycode.funs.common.lllogI
import com.tkip.mycode.funs.common.onClick
import com.tkip.mycode.util.lib.map.MAP_ACT_ADD
import com.tkip.mycode.util.lib.map.OnMap
import com.tkip.mycode.util.lib.map.showMap
import com.tkip.mycode.util.tools.anim.gone
import com.tkip.mycode.util.tools.etc.OnMyClick

/**
 * <작은 업로드 버튼 + 맵>
 *  context는 무조건 appCompatActivity의 context
 *  맵에서 위치와 주소를 찍을때 사용하는 작은 맵.
 *  메인맵에서 찍은 위치,주소 -> activity.onActivityResult() -> BaseActivity -> 여기 작은 맵에서 쇼.
 */
class LinearMapAddBox : LinearLayout, OnMapReadyCallback {
    lateinit var b: LinearMapAddBoxBinding
    lateinit var activity: AppCompatActivity

    var map: GoogleMap? = null
    var address: String? = null
    var postalCode: String? = null
    var latLng: LatLng? = null


    constructor(context: Context) : super(context)
    constructor(context: Context, mode:Int?,address1: String?, latLng1: LatLng?) : super(context) {
        lllogI("========LinearMapAdd start  ")

        this.address = address1
        this.latLng = latLng1

        b = DataBindingUtil.inflate(LayoutInflater.from(context), R.layout.linear_map_add_box, this, true)
        b.address = address
        b.mode = mode

        activity = (context as AppCompatActivity)
        val mapFragment = activity.supportFragmentManager.findFragmentById(R.id.map) as SupportMapFragment
        mapFragment.getMapAsync(this)

        b.btnOpenMap.onClick {
            if (OnMyClick.isDoubleClick()) return@onClick
            OnMap.gotoMainMap(activity, MAP_ACT_ADD, latLng)
        }

    }

    override fun onMapReady(googleMap: GoogleMap?) {
        map = googleMap
        lllogI("========LinearMapAdd onMapReady  latLng : $latLng")
        map?.run {
            latLng?.let {
                lllogI(" LinearMapAdd map?.run  latLng : $latLng")
                map?.showMap(it, address, b.lineMap)
            }

            //            room.lat?.let { lat ->
//                OnMap.moveMap(this, LatLng(lat, room.lng!!), OnMap.zoomStopMap, false)
//                map?.addMarker(OnMap.getMarkerOpt(LatLng(lat, room.lng!!), room.title, room.address))
//            }

            setOnMapClickListener {
                if (OnMyClick.isDoubleClick()) return@setOnMapClickListener
                OnMap.gotoMainMap(activity, MAP_ACT_ADD, latLng)

            }

        }
    }

    fun deleteLocation() {
        b.etAddress.setText("")
        b.lineMap.gone()
    }


}