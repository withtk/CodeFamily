package com.tkip.mycode.funs.search.model

import android.content.Context
import android.view.LayoutInflater
import android.widget.LinearLayout
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import com.tkip.mycode.R
import com.tkip.mycode.databinding.CustomModelListBinding
import com.tkip.mycode.init.*
import com.tkip.mycode.init.base.getInt
import com.tkip.mycode.model.board.Board
import com.tkip.mycode.model.flea.Flea
import com.tkip.mycode.model.my_enum.DataType
import com.tkip.mycode.model.my_enum.Status
import com.tkip.mycode.model.peer.isAdmin
import com.tkip.mycode.model.post.Post
import com.tkip.mycode.model.reply.Reply
import com.tkip.mycode.model.reply.ReplyAdapter
import com.tkip.mycode.model.room.Room
import com.tkip.mycode.model.store.Store
import com.tkip.mycode.nav.board.tab_first.BoardAdapter
import com.tkip.mycode.nav.flea.FleaAdapter
import com.tkip.mycode.nav.post.PostAdapter
import com.tkip.mycode.nav.room.RoomAdapter
import com.tkip.mycode.nav.store.StoreAdapter
import com.tkip.mycode.util.lib.retrofit2.ESget
import com.tkip.mycode.util.lib.retrofit2.ESquery
import com.tkip.mycode.util.tools.recycler.OnRv

/**
 * for 모든 model1
 */
class CustomModelList : LinearLayout {
    private lateinit var b: CustomModelListBinding
    private var uid: String? = null  // 널이면 모든 유저의 데이타
    private lateinit var dataType: DataType
    private var rvType: Int = RV_VERT
    private var span: Int? = null
//    private var hori = false

//    private lateinit var aaaalist: ArrayList<*>
//    private lateinit var aaaaada: RecyclerView.Adapter<RecyclerView.ViewHolder>

    private val boardItems by lazy { arrayListOf<Board>() }
    private val postItems by lazy { arrayListOf<Post>() }
    private val fleaItems by lazy { arrayListOf<Flea>() }
    private val roomItems by lazy { arrayListOf<Room>() }
    private val storeItems by lazy { arrayListOf<Store>() }
    private val replyItems by lazy { arrayListOf<Reply>() }
    private var boardAdapter: BoardAdapter? = null
    private var postAdapter: PostAdapter? = null
    private var fleaAdapter: FleaAdapter? = null
    private var roomAdapter: RoomAdapter? = null
    private var storeAdapter: StoreAdapter? = null
    private var replyAdapter: ReplyAdapter? = null

    private val esSize by lazy { R.integer.es_size.getInt() }
    var esLastIndex = 0

    constructor(context: Context) : super(context)

    /*** 일반 */
    constructor(context: Context, uid: String?, dataType: DataType, span: Int?,rvType: Int, onItem: ((Any) -> Unit)?) : super(context) {
        init(context, uid, dataType, rvType, span, onItem)
    }

    private fun init(context: Context, uid: String?, dataType: DataType, rvType: Int, span: Int?, onItem: ((Any) -> Unit)?) {
        b = DataBindingUtil.inflate(LayoutInflater.from(context), R.layout.custom_model_list, this, true)
        this.uid = uid
        this.dataType = dataType
        this.rvType = rvType
        this.span = span
//        this.hori = hori

        /**  Adapter 세팅*/
        setupRV(onItem)
        /*** rv Grid Layout 셋팅 */
        setupRvLayout()

        b.inRefresh.refresh.setOnRefreshListener { getEsDataList(0) }
        b.inRefresh.refresh.setOnLoadMoreListener { getEsDataList(esLastIndex) }
//                b.inRefresh.refresh.setEnableRefresh(false)   //상단 refresh는 불능
        //        b.refresh.setEnableLoadMore(false)  // 하단 loadMore 불능.

        /** 시작하자마자 받아오기. */
//        getEsDataList(0)
    }


    /** * adapter 세팅 */
    private fun setupRV(onItem: ((Any) -> Unit)?) {

        b.inRefresh.inData.tvNoData.text = dataType.noDataMsg

        when (dataType) {
            DataType.BOARD -> {
                if (boardAdapter == null) boardAdapter = BoardAdapter(boardItems, rvType, onItem)
                b.inRefresh.rv.adapter = boardAdapter
            }
            DataType.POST -> {
                if (postAdapter == null) postAdapter = PostAdapter(postItems, rvType, onItem)
                b.inRefresh.rv.adapter = postAdapter
            }
            DataType.FLEA -> {
                if (fleaAdapter == null) fleaAdapter = FleaAdapter(fleaItems, rvType, onItem)
                b.inRefresh.rv.adapter = fleaAdapter
            }
            DataType.ROOM -> {
                if (roomAdapter == null) roomAdapter = RoomAdapter(roomItems, rvType, onItem)
                b.inRefresh.rv.adapter = roomAdapter
            }
            DataType.STORE -> {
                if (storeAdapter == null) storeAdapter = StoreAdapter(storeItems, rvType, onItem)
                b.inRefresh.rv.adapter = storeAdapter
            }
            DataType.REPLY -> {
                if (replyAdapter == null) replyAdapter = ReplyAdapter(replyItems, rvType, onItem)
                b.inRefresh.rv.adapter = replyAdapter
            }
            else -> {
            }
        }


    }


    /*** RvLayout 셋팅 */
    private fun setupRvLayout() {
        when (rvType) {
            RV_GRID_HORI -> b.inRefresh.rv.layoutManager = GridLayoutManager(context, span!!, GridLayoutManager.HORIZONTAL, false)
            RV_GRID_VERT -> b.inRefresh.rv.layoutManager = GridLayoutManager(context, span!!, GridLayoutManager.VERTICAL, false)
            RV_HORI -> b.inRefresh.rv.layoutManager = LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false)
        }
    }


    /** * 쿼리 만들기 */
    private fun myQuery(from: Int): String {
        val query = when (rvType) {
//            RV_VERT ->
//                when (dataType) {
//                    DataType.POST ->
//                }
            RV_MY_CONTENTS -> uid?.let { ESquery.myContentsList(esSize, from, it, Status.BLIND_ADMIN) }
            RV_MY_CONTENTS_MANAGE -> if (isAdmin()) uid?.let { ESquery.myContentsList(esSize, from, it, Status.NOTHING) } else null    // 해당 유저의 모든 데이타 (삭제포함)
            RV_MANAGE -> if (isAdmin()) ESquery.matchAll(esSize, from) else null    // 전체 유저의 데이타
            else -> null
        }
        return query ?: ""
    }


    /** * 가져오기  */
    fun getEsDataList(from: Int) {

        val query = myQuery(from)

        when (dataType) {
            DataType.BOARD -> ESget.getBoardList(query,
                    success = {
                        esLastIndex = OnRv.setRvRefresh(it, from, boardItems, b.inRefresh, true)
                        boardAdapter?.notifyDataSetChanged()
                    },
                    fail = {})
            DataType.POST -> ESget.getPostList(query,
                    success = {
                        esLastIndex = OnRv.setRvRefresh(it, from, postItems, b.inRefresh, true)
                        postAdapter?.notifyDataSetChanged()
                    },
                    fail = {})

            DataType.FLEA -> ESget.getFleaList(query,
                    success = {
                        esLastIndex = OnRv.setRvRefresh(it, from, fleaItems, b.inRefresh, true)
                        fleaAdapter?.notifyDataSetChanged()
                    },
                    fail = {})

            DataType.ROOM -> ESget.getRoomList(query,
                    success = {
                        esLastIndex = OnRv.setRvRefresh(it, from, roomItems, b.inRefresh, true)
                        roomAdapter?.notifyDataSetChanged()
                    },
                    fail = {})
            DataType.STORE -> ESget.getStoreList(query,
                    success = {
                        esLastIndex = OnRv.setRvRefresh(it, from, storeItems, b.inRefresh, true)
                        storeAdapter?.notifyDataSetChanged()
                    },
                    fail = {})

            DataType.REPLY  -> ESget.getReplyList(query,
                    success = {
                        esLastIndex = OnRv.setRvRefresh(it, from, replyItems, b.inRefresh, true)
                        replyAdapter?.notifyDataSetChanged()
                    },
                    fail = {})

            else -> {
            }
        }


    }


}