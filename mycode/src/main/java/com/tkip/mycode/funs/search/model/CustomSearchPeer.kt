package com.tkip.mycode.funs.search.model

import android.content.Context
import android.view.LayoutInflater
import android.widget.LinearLayout
import androidx.databinding.DataBindingUtil
import com.tkip.mycode.R
import com.tkip.mycode.databinding.CustomModelListBinding
import com.tkip.mycode.init.CH_LOGIN_TIME
import com.tkip.mycode.init.base.getInt
import com.tkip.mycode.model.peer.Peer
import com.tkip.mycode.model.peer.PeerAdapter
import com.tkip.mycode.util.lib.retrofit2.ESget
import com.tkip.mycode.util.lib.retrofit2.ESquery
import com.tkip.mycode.util.tools.recycler.OnRv

/**
 */
class CustomSearchPeer : LinearLayout {
    private lateinit var b: CustomModelListBinding
    private var keyword = ""
    private val peerItems by lazy { arrayListOf<Peer>() }
    private var peerAdapter: PeerAdapter? = null

    private var field: String = CH_LOGIN_TIME
    private var sort = false

    private val esSize by lazy { R.integer.es_size_search.getInt() }
    private var esLastIndex = 0

    constructor(context: Context) : super(context)
    constructor(context: Context, rvType: Int) : super(context) {
        b = DataBindingUtil.inflate(LayoutInflater.from(context), R.layout.custom_model_list, this, true)

        /** set Adapter */
        peerAdapter = PeerAdapter(peerItems, rvType, null)
        b.inRefresh.rv.adapter = peerAdapter

        b.inRefresh.refresh.setOnRefreshListener { getEsDataList(0, field, sort, keyword) }
        b.inRefresh.refresh.setOnLoadMoreListener { getEsDataList(esLastIndex, field, sort, keyword) }
//        b.refresh.setEnableRefresh(false)   //상단 refresh는 불능.
//        b.refresh.setEnableLoadMore(false)  //하단 loadMore 불능.

    }

    /** * 가져오기  */
    fun getEsDataList(from: Int, field: String, sort: Boolean, keyword1: String?) {
        this.keyword = keyword1 ?: ""
        this.field = field
        this.sort = sort
//        this.tagTYPE = tagTYPE

        val query = ESquery.peerAllArrange(esSize, from, field, sort)
        ESget.getPeerList(query,
                success = {
                    esLastIndex = OnRv.setRvRefresh(it, from, peerItems, b.inRefresh, true)
                    peerAdapter?.notifyDataSetChanged()
                }, fail = { })

    }


}