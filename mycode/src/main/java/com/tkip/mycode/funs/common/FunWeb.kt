package com.tkip.mycode.funs.common

import android.content.Context
import android.content.Intent
import android.net.Uri


/*************************************************************************************/
/********************  ***********************************************************/
/*************************************************************************************/

fun Context.moveToWeb(url: String) {
    val uri = Uri.parse(url)
    val intent = Intent(Intent.ACTION_VIEW, uri)
    startActivity(intent)
}

