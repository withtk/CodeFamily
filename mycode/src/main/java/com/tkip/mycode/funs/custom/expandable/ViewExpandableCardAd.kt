package com.tkip.mycode.funs.custom.expandable

import android.content.Context
import android.view.LayoutInflater
import android.widget.LinearLayout
import androidx.databinding.DataBindingUtil
import com.tkip.mycode.R
import com.tkip.mycode.databinding.ViewExpandableCardAdBinding
import com.tkip.mycode.util.tools.anim.*

/**
 * for addAdActivity
 */
class ViewExpandableCardAd : LinearLayout {
    lateinit var b: ViewExpandableCardAdBinding

    constructor(context: Context) : super(context)
    constructor(context: Context,  title: String) : super(context) {
        b = DataBindingUtil.inflate(LayoutInflater.from(context), R.layout.view_expandable_card_ad, this, true)
        b.title = title

        b.lineTitle.setOnClickListener {
            toggle()
//            AnimUtil.rotateArrow(b.ivArrow)
//            AnimUtil.toggleUpDown(b.lineContents, true)
//            onClick()
        }
//        view?.let { b.lineContents.addView(it) }

//        if (isOpen) b.lineTitle.performClick()   // 초기값에 따라 오픈여부 결정.

    }

    fun toggle() {
//        if(b.lineContents.isVisi()) return
        b.ivArrow.animRotateArrow()

//        OnVisible.toggleSimple(b.lineContents.visibility!= View.VISIBLE,b.lineContents)
        AnimUtil.toggleUpDown(b.lineContents, true)
    }

    fun open() {
        b.ivArrow.animRotate180()
        b.lineContents.visi()
        b.lineContents.requestLayout()
    }

    fun close() {
        b.ivArrow.animRotate0()
        b.lineContents.gone()
    }


}