package com.tkip.mycode.funs.search.model

import android.content.Context
import android.view.LayoutInflater
import android.widget.LinearLayout
import androidx.databinding.DataBindingUtil
import com.tkip.mycode.R
import com.tkip.mycode.databinding.CustomModelListBinding
import com.tkip.mycode.init.RV_VERT
import com.tkip.mycode.init.base.getInt
import com.tkip.mycode.model.grant.Grant
import com.tkip.mycode.model.grant.GrantAdapter
import com.tkip.mycode.util.lib.retrofit2.ESget
import com.tkip.mycode.util.lib.retrofit2.ESquery
import com.tkip.mycode.util.tools.recycler.OnRv

/**
 * 1. grant
 */
class CustomListGrant : LinearLayout {
    private lateinit var b: CustomModelListBinding

    private var adapter: GrantAdapter? = null
    private val items by lazy { arrayListOf<Grant>() }

    private val esSize by lazy { R.integer.es_size_grant.getInt() }
    private var esLastIndex = 0

    constructor(context: Context) : super(context)
    constructor(context: Context,  onItem: ((Grant) -> Unit)?) : super(context) {
        b = DataBindingUtil.inflate(LayoutInflater.from(context), R.layout.custom_model_list, this, true)

        /** set Adapter */
        adapter = GrantAdapter( items, RV_VERT, onItem)
        b.inRefresh.rv.adapter = adapter

        b.inRefresh.refresh.setOnRefreshListener { getEsDataList(0) }
        b.inRefresh.refresh.setOnLoadMoreListener { getEsDataList(esLastIndex) }
//        b.refresh.setEnableRefresh(false)   // 상단 refresh는 불능.
//        b.refresh.setEnableLoadMore(false)  // 하단 loadMore 불능.

    }

    /** * 가져오기  */
    fun getEsDataList(from: Int) {

        ESget.getGrantList( ESquery.grantListLately(esSize,from),
                success = {
                    esLastIndex = OnRv.setRvRefresh(it, from, items, b.inRefresh, true)
                    adapter?.notifyDataSetChanged()
                }, fail = {})

    }


}