package com.tkip.mycode.funs.custom.toolbar

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.widget.LinearLayout
import androidx.databinding.DataBindingUtil
import com.tkip.mycode.R
import com.tkip.mycode.databinding.ViewToolBar4boardBinding
import com.tkip.mycode.funs.common.OnValid
import com.tkip.mycode.funs.common.OnVisible
import com.tkip.mycode.init.inter.interToolbarAdd
import com.tkip.mycode.model.board.Board
import com.tkip.mycode.model.peer.isAdmin
import com.tkip.mycode.model.peer.isCsBoard
import com.tkip.mycode.model.peer.isMaster
import com.tkip.mycode.util.tools.etc.OnMyClick

/**
 * using in boardActivity
 */
class ViewToolbar4Board : LinearLayout {
    lateinit var b: ViewToolBar4boardBinding

    constructor(context: Context) : super(context)
    constructor(context: Context, board: Board, boardName: String?, addName: String,
                bookmarkCount: Int?, pushCount: Int?, onBack: () -> Unit,
                onBookmark: ((View) -> Unit)?, onPush: ((View) -> Unit)?, onAdd: (View) -> Unit, onMore: ((View) -> Unit)?) : super(context) {
        b = DataBindingUtil.inflate(LayoutInflater.from(context), R.layout.view_tool_bar_4board, this, true)

        b.inter = interToolbarAdd(
                onBack = { onBack() },
                onBookmark = {
                    OnMyClick.disableAWile(b.cbBookmark, b.cbPush)
                    onBookmark?.invoke(it)
                },
                onPush = {
                    OnMyClick.disableAWile(b.cbBookmark, b.cbPush)
                    onPush?.invoke(it)
                },
                onAdd = { onAdd(it) },
                onMore = { onMore?.invoke(it) }
        )

        b.board = board
        b.enableAdd = true

        b.boardName = boardName
        b.addName = addName
        b.bookmarkCount = bookmarkCount
        b.pushCount = pushCount

        /** 고객센터 보드 */
        if (board.isCsBoard()) {
            b.csCenter = true
            b.onPush = if (board.authorUID.isMaster() || isAdmin()) (onPush != null) else null    // 마스터or어드민 = show PushCB
            b.onMore = (onMore != null)
        } else {
            b.onPush = (onPush != null)
            b.onMore = (onMore != null)
        }


    }


}