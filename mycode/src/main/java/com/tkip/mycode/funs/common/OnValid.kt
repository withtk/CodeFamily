package com.tkip.mycode.funs.common

import android.os.Handler
import android.text.Editable
import android.widget.EditText
import com.google.android.material.textfield.TextInputLayout
import com.tkip.mycode.R
import com.tkip.mycode.dialog.toast.TToast
import com.tkip.mycode.util.tools.date.rightNow
import com.tkip.mycode.util.tools.etc.MyKeyboard


class OnValid {

    companion object {

        var LIMIT: Long = 1000L
        var before: Long? = null
        val handler by lazy { Handler() }
        fun readySearch( onGo: () -> Unit) {
            val now = rightNow()
            if (before == null) {
                lllogD("readySearch before $before ")
                before = now
                return
            } else {
//                lllogD("readySearch before $before ")
                val cal = now - (before ?: 0L)
                before = now
                val run = Runnable(onGo)
                handler.postDelayed(run, LIMIT)

                /**
                 * true : 1초 이내의 타이핑
                 */
                if (cal < LIMIT) {
                    lllogD("readySearch true : no search")
                    handler.removeCallbacks(run)
                    //todo: 마지막에는 remove하지 말아야..
                } else {
//                    lllogD("readySearch false : goto search")
                }
            }

        }

        /**
         * onBackPressed 키보드 올림 여부 체크.
         */
        fun checkKeyboardHide(vararg arr: EditText): Boolean {
            return if (hasFocusEt(*arr)) {
                MyKeyboard.hide(arr[0])   // 키보드 숨기기
                removeFocusEt(*arr)
                true
            } else false
        }


        /**
         * et 포커싱 여부 한번에 체크하기
         */
        fun hasFocusEt(vararg arr: EditText): Boolean {
            arr.forEach { et -> if (et.hasFocus()) return true }
            return false
        }

        /**
         * et 포커싱 한번에 제거하기
         */
        fun removeFocusEt(vararg arr: EditText): Boolean {
            arr.forEach { et -> et.clearFocus() }
            return false
        }


        /**
         * et null 체크
         */
        fun hasNullET(vararg editTexts: EditText): Boolean {
            for (et in editTexts) {
                if (et.getStr().isEmpty()) {
                    TToast.showAlert(R.string.error_edittext_null)
                    return true
                }
            }
            return false
        }

        fun hasNullET(isToast: Boolean, vararg editTexts: EditText): Boolean {
            for (et in editTexts) {
                if (et.getStr().isEmpty()) {
                    if (isToast) TToast.showAlert(R.string.error_edittext_null)
                    return true
                }
            }
            return false
        }

        /**
         *  ERROR
         *  없으면 false.
         *  있으면 true 메세지
         */
        fun hasError(vararg editTexts: EditText): Boolean {
            for (et in editTexts) {
                if (!et.error.isNullOrBlank()) {
                    TToast.showAlert(R.string.error_edittext_null)
                    return true
                }
            }
            return false
        }

        fun hasError(vararg layouts: TextInputLayout): Boolean {
            for (lay in layouts) {
                if (!lay.error.isNullOrBlank()) {
                    TToast.showAlert(R.string.error_edittext_null)
                    return true
                }
            }
            return false
        }


        /**
         * 1. Null 체크
         * 2. set Error
         */
//        fun isNullBlankAndSetError(et: EditText, stringID: Int): Boolean {
//            return if (et.getNullStr() == null) {
//                et.error = stringID.getStr()
//                true
//            } else false
//        }

    }

}