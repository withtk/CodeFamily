package com.tkip.mycode.funs.custom.flow_layout

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import androidx.databinding.DataBindingUtil
import com.tkip.mycode.R
import com.tkip.mycode.databinding.ViewBottomTagBinding
import com.tkip.mycode.funs.common.lllogD
import com.tkip.mycode.funs.custom.text.BtnCorner
import com.tkip.mycode.model.mytag.OnTag
import com.tkip.mycode.util.lib.FlowLayout
import com.zhy.view.flowlayout.TagAdapter


/**
 * string tag
 * 검색했던 즐겨찾는 태그리스트 in flow Layout
 */
class ViewBottomTag : FlowLayout {
    private lateinit var b: ViewBottomTagBinding
    private lateinit var strList: List<String>

    constructor(context: Context) : super(context)
    constructor(context: Context, list: ArrayList<String>?, onOpen: () -> Unit, onTagClick: ((name: String) -> Unit)?) : super(context) {
        b = DataBindingUtil.inflate(LayoutInflater.from(context), R.layout.view_bottom_tag, this, true)
        lllogD("  ViewBottomTag size ${list?.size}")

        strList =
                if (list.isNullOrEmpty()) OnTag.setBasicBookTags()!!
                else list

        val adapter = object : TagAdapter<String?>(strList) {
            override fun getView(parent: com.zhy.view.flowlayout.FlowLayout?, position: Int, t: String?): View {
                return BtnCorner(context, t!!, onTagClick)
            }
        }
        b.flow.adapter = adapter
        b.flow.setOnSelectListener {
            lllogD("ViewBottomTag setOnSelectListener set ${it.toString()} ")
        }
        b.flow.setOnTagClickListener { view, position, parent ->
            lllogD("ViewBottomTag setOnSelectListener view $view position $position parent $parent ")
            return@setOnTagClickListener true
        }

//        adapter.setSelected()
//        b.flow.selectedList


//        addAll(onTagClick)

//        b.btnOpen.onClick {  onOpen()}
        b.tvOpen.setOnClickListener { onOpen() }
    }

    private fun addAll(onTagClick: ((name: String) -> Unit)?) {


//        strList.forEach {
//            lllogD("  ViewBottomTag BtnCorner  $it")

//            val btn = BtnCorner(context, it, onTagClick)
//            b.flow.addView(btn)


//        }
//            lllogD("  ViewBottomTag childCount  ${b.flow.childCount}")

    }


//
//    private lateinit var tagList: ArrayList<MyTag>
//    constructor(context: Context, arr: Array<MyTag>) : super(context) {
//        init(context, arr)
//    }
//
//    private fun init(context: Context, arr: Array<MyTag>) {
//        b = DataBindingUtil.inflate(LayoutInflater.from(context), R.layout.view_my_flow, this, true)
//        tagList = arr.toList() as ArrayList<MyTag>
//        addAll()
//    }
//
//    private fun addAll() {
//        tagList.forEach { b.flow.addView(BtnCorner(context, it.name)) }
//    }
//
//    fun removeAll() {
//        tagList.clear()
//        b.flow.removeAllViews()
//    }
//
//    fun addTag(list: ArrayList<MyTag>) {
//        this.tagList = list
//        addAll()
//    }
//
//    fun addTag(tag:MyTag) {
//        b.flow.addView(BtnCorner(context, tag.name))
//    }

}


//    private var lineHeight = 0
//
//    constructor(context: Context) : super(context)
//    constructor(context: Context, attrs: AttributeSet?) : super(context, attrs)
//
//    override fun onMeasure(widthMeasureSpec: Int, heightMeasureSpec: Int) {
////        super.onMeasure(widthMeasureSpec, heightMeasureSpec)
//        assert(MeasureSpec.getMode(widthMeasureSpec) != MeasureSpec.UNSPECIFIED)
//
//        val width = MeasureSpec.getSize(widthMeasureSpec) - paddingLeft - paddingRight
//        var height = MeasureSpec.getSize(heightMeasureSpec) - paddingTop - paddingBottom
//        val count = childCount
//        var line_height = 0
//
//        var xpos = paddingLeft
//        var ypos = paddingTop
//
//        val childHeightMeasureSpec: Int
//        childHeightMeasureSpec = if (MeasureSpec.getMode(heightMeasureSpec) == MeasureSpec.AT_MOST) {
//            MeasureSpec.makeMeasureSpec(height, MeasureSpec.AT_MOST)
//        } else {
//            MeasureSpec.makeMeasureSpec(0, MeasureSpec.UNSPECIFIED)
//        }
//
//
//        for (i in 0 until count) {
//            val child = getChildAt(i)
//            if (child.visibility != View.GONE) {
//                val lp = child.layoutParams as FlowLayout.LayoutParams
//                child.measure(MeasureSpec.makeMeasureSpec(width, MeasureSpec.AT_MOST), childHeightMeasureSpec)
//                val childw = child.measuredWidth
//                line_height = Math.max(line_height, child.measuredHeight + lp.vertical_spacing)
//                if (xpos + childw > width) {
//                    xpos = paddingLeft
//                    ypos += line_height
//                }
//                xpos += childw + lp.horizontal_spacing
//            }
//        }
//        this.lineHeight = line_height
//
//        if (MeasureSpec.getMode(heightMeasureSpec) == MeasureSpec.UNSPECIFIED) {
//            height = ypos + line_height
//        } else if (MeasureSpec.getMode(heightMeasureSpec) == MeasureSpec.AT_MOST) {
//            if (ypos + line_height < height) {
//                height = ypos + line_height
//            }
//        }
//        setMeasuredDimension(width, height)
//    }
//
//    override fun generateDefaultLayoutParams(): LayoutParams {
//        return LayoutParams(1, 1) // default of 1px spacing
//    }
//
//    override fun generateLayoutParams(attrs: AttributeSet?): LayoutParams {
//        return LayoutParams(1, 1) // default of 1px spacing
//    }
//
//    override fun checkLayoutParams(p: LayoutParams?): Boolean {
//        return p is FlowLayout.LayoutParams
//    }
//
//    override fun onLayout(changed: Boolean, l: Int, t: Int, r: Int, b: Int) {
//        val count = childCount
//        val width = r - l
//        var xpos = paddingLeft
//        var ypos = paddingTop
//
//        for (i in 0 until count) {
//            val child = getChildAt(i)
//            if (child.visibility != View.GONE) {
//                val childw = child.measuredWidth
//                val childh = child.measuredHeight
//                val lp = child.layoutParams as FlowLayout.LayoutParams
//                if (xpos + childw > width) {
//                    xpos = paddingLeft
//                    ypos += lineHeight
//                }
//                child.layout(xpos, ypos, xpos + childw, ypos + childh)
//                xpos += childw + lp.horizontal_spacing
//            }
//        }
//    }

