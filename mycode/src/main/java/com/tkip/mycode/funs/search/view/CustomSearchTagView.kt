package com.tkip.mycode.funs.search.view

import android.content.Context
import android.view.LayoutInflater
import android.widget.LinearLayout
import androidx.databinding.DataBindingUtil
import com.tkip.mycode.R
import com.tkip.mycode.databinding.CustomSearchTagviewBinding
import com.tkip.mycode.funs.common.*
import com.tkip.mycode.init.base.*
import com.tkip.mycode.model.mytag.*
import com.tkip.mycode.util.tools.anim.*

/**
 * MyTag 검색.
 */
class CustomSearchTagView : LinearLayout {
    lateinit var b: CustomSearchTagviewBinding
    private lateinit var tagAdapter: MyTagAdapter
    private val items = arrayListOf<MyTag>()
    private val wordList = arrayListOf<String>()
//    private val storeItems = arrayListOf<Store>()
//    private lateinit var storeAdapter: StoreAdapter

    private val esSize by lazy { R.integer.es_size_search.getInt() }
    private var esLastIndex = 0

    constructor(context: Context) : super(context)
    constructor(context: Context, tagType: TagTYPE, onSearch: ((wordList: ArrayList<String>?) -> Unit)?) : super(context) {
        b = DataBindingUtil.inflate(LayoutInflater.from(context), R.layout.custom_search_tagview, this, true)

//        /*** 클릭 리스너 */
//        b.inter = interSearchTagview(
//                onTagView = { },
//                onSearch = { onSearch?.invoke(getWordList()) })

        b.tagView.setOnTagClickListener { _, p ->
            b.tagView.remove(p)
            onSearch?.invoke(resetWordList())
            showhideTvInfo()
        }
//        b.tagView.setOnTagDeleteListener { _, _, _ -> }
//        b.tagView.setOnTagLongClickListener { _, _ -> }

        b.btnSearch.onClick {
            onSearch?.invoke(resetWordList())
            OnTag.saveMyKeywordList(Sred.ARR_KEYWORD,b.etSearch.keyword())   // 검색내역 저장.
        }

        /** * 태그 adapter  */
        tagAdapter = MyTagAdapter(items, MyTagAdapter.rvNormal,
                /**   * MyTagAdapter에서  아이템 클릭.    */
                onItemClick = { _, _, myTag ->
                    b.etSearch.setText("")
                    b.tagView.checkAdd(TAG_1, myTag.name)    // tagView에 추가
                    items.clear()
                    afterSearch()
                    showhideTvInfo()
                    onSearch?.invoke(resetWordList())    // 태그를 클릭한 순간에는 무조건 tagView의 태그를 이용한 검색만 됨. (따라서 keyword는 무조건 null.)
                    OnTag.saveMyKeywordList(Sred.ARR_KEYWORD,myTag.name)   // 검색내역 저장.
                })
        b.rvTag.adapter = tagAdapter

        b.etSearch.setOnFocusChangeListener { _, hasFocus ->
            /*** 처음 포커싱 됐을때 지난 검색결과 보이기 */
            if (hasFocus && items.isEmpty() && b.etSearch.keyword() != null) {
                OnTag.loadMyKeywordList(Sred.ARR_KEYWORD)?.forEach { items.add(MyTag(it)) }
                tagAdapter.notifyDataSetChanged()
            }
        }


        /** * 실시간 태그 검색 */
        b.etSearch.textListener(after = { s ->
            OnVisible.toggleSimple(!s.isNullOrBlank(), b.btnClearEt)   // x버튼 토글
            b.btnSearch.isEnabled = !s.isNullOrBlank()      // search버튼 가능/불가

            b.etSearch.keyword()?.let { keyword ->
                OnTag.getEsTagList(10, 0, tagType, keyword,
                        success = {
                            lllogI("ViewSearchTagView list.size :${it?.size}")
                            it.addCheckTagView(items, b.tagView)
                            afterSearch()
                        }, fail = { afterSearch() })
            } ?: let {
                // keyword 널이면 list 지워.
                items.clear()
                afterSearch()
            }
        }, before = { _, _, _, _ -> }, on = { s, _, _, c -> })


        b.btnClearEt.onClick {
            b.etSearch.setText("")
            onSearch?.invoke(resetWordList())
        }
    }


    private fun resetWordList(): ArrayList<String> {
//        val list = arrayListOf<String>()
        wordList.clear()
        b.tagView.tags.forEach { wordList.add(it.text) }   // tagView 넣기.
        b.etSearch.keyword()?.let {
            it.removeSpecial().removeLongSpace().trim().getArraySplitSpace().forEach { str -> wordList.add(str) } // keyword 다듬어서 넣기.
        }
        lllogI("getWordList list ${wordList.size}")
        return wordList
    }

    private fun afterSearch() {
        AnimUtil.hideFadeOut(b.avLoading, true)
        if (items.isEmpty()) b.rvTag.gone()
        else b.rvTag.visi()
        tagAdapter.notifyDataSetChanged()
    }


    /**     * info 보이기 감추기     */
    private fun showhideTvInfo() {
        if (b.tagView.tags.isEmpty()) b.tvInfo.showOverUp(false)
        else b.tvInfo.hideOverDown(false)
    }


    /** * 가져오기  */
//    private fun getEsDataList(from: Int) {
//
//        b.avLoading.visi()
//
//        val query = ESquery.storeSearch(esSize, from, ESquery.getTermTaglist(b.tagView, b.etSearch.keyword()), Status.NORMAL)
//        ESget.getStoreList(query,
//                success = {
//                    lllogI("CustomSearchTagView list.size : ${it?.size}")
//                    esLastIndex = OnRv.setSearchResult(it, from, storeItems, b.tvNoResult, R.string.info_no_search_result_bid.getStr(), b.avLoading, b.refresh)
//                    storeAdapter.notifyDataSetChanged()
//                    lllogI("CustomSearchTagView childCount ${b.rv.childCount}")
//
//                }, fail = { })
//
//    }


}