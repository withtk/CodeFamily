package com.tkip.mycode.funs.custom.menu

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.widget.LinearLayout
import androidx.databinding.DataBindingUtil
import com.tkip.mycode.R
import com.tkip.mycode.admin.menu.AMenu
import com.tkip.mycode.databinding.ViewMenuNormalBinding
import com.tkip.mycode.funs.common.arrTranToolbar
import com.tkip.mycode.util.tools.etc.OnMyClick


class ViewMenuNormal : LinearLayout {
    private lateinit var b: ViewMenuNormalBinding

    constructor(context: Context) : super(context)
    constructor(context: Context, type: Int?, aMenu: AMenu, onClick: ((aMenu: AMenu, arrPair: Array<androidx.core.util.Pair<View, String>>?) -> Unit)?) : super(context) {
        b = DataBindingUtil.inflate(LayoutInflater.from(context), R.layout.view_menu_normal, this, true)
        b.aMenu = aMenu

        b.line.setOnClickListener {
            OnMyClick.setDisableAWhileBTN(it)
            if (onClick == null) aMenu.onClickMenu(context, it.arrTranToolbar())   // 바로이동.
            else onClick.invoke(aMenu, null)   // onCLick을 분기해야 할 때
        }

//        type?.let {
//            when (it) {
//                V_TYPE1->{
//                    /** 덜 중요한 버튼 */
//                    b.tvBtn.setTextColor(R.color.text_gray.getClr())
//                }
//                V_TYPE2->{}
//            }
//        }

    }


//
//    constructor(context: Context, attrs: AttributeSet?) : super(context, attrs) {
//        init()
//    }
//
//    constructor(context: Context, attrs: AttributeSet?, defStyleAttr: Int) : super(context, attrs, defStyleAttr) {
//        init()
//    }
//


}