package com.tkip.mycode.funs.custom

import android.content.Context
import android.view.LayoutInflater
import android.widget.LinearLayout
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.LatLng
import com.tkip.mycode.R
import com.tkip.mycode.databinding.LinearMapBoxBinding
import com.tkip.mycode.funs.common.lllogI
import com.tkip.mycode.util.lib.map.MAP_ACT_NORMAL
import com.tkip.mycode.util.lib.map.OnMap
import com.tkip.mycode.util.lib.map.showMap
import com.tkip.mycode.util.tools.etc.OnMyClick

/**
 *  context는 무조건 appCompatActivity의 context
 *
 *  위치,주소 -> 여기 작은 맵에서 쇼.
 */
class LinearMapBox : LinearLayout, OnMapReadyCallback {
    lateinit var b: LinearMapBoxBinding
    lateinit var activity: AppCompatActivity

    var map: GoogleMap? = null
    var address: String? = null
    var postalCode: String? = null
    var latLng: LatLng? = null

    constructor(context: Context) : super(context)
    constructor(context: Context, address1: String?, latLng1: LatLng?) : super(context) {
        lllogI("========LinearMap constructor      $latLng1")

        this.address = address1
        this.latLng = latLng1

        b = DataBindingUtil.inflate(LayoutInflater.from(context), R.layout.linear_map_box, this, true)
        b.address = address
//        b.lat = latLng?.latitude

        activity = (context as AppCompatActivity)
        val mapFragment = activity.supportFragmentManager.findFragmentById(R.id.map) as SupportMapFragment
        mapFragment.getMapAsync(this)

    }

    override fun onMapReady(googleMap: GoogleMap?) {
        map = googleMap
        lllogI("========LinearMap onMapReady      $latLng")
        map?.run {
            latLng?.let {

                map?.showMap(it, address, b.lineMap)

//                Handler().postDelayed({
//                    showMap(LatLng(finalRoom.lat!!, finalRoom.lng!!), finalRoom.address)
//                }, 3000)
            }

            //            room.lat?.let { lat ->
//                OnMap.moveMap(this, LatLng(lat, room.lng!!), OnMap.zoomStopMap, false)
//                map?.addMarker(OnMap.getMarkerOpt(LatLng(lat, room.lng!!), room.title, room.address))
//            }

            setOnMapClickListener {
                if (OnMyClick.isDoubleClick()) return@setOnMapClickListener
                OnMap.gotoMainMap(activity, MAP_ACT_NORMAL, latLng)
            }

        }
    }

}