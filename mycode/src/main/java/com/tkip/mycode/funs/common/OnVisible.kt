package com.tkip.mycode.funs.common

import android.os.Handler
import android.view.View
import android.widget.Button
import android.widget.LinearLayout
import com.tkip.mycode.init.base.gte
import com.tkip.mycode.model.my_enum.PeerType
import com.tkip.mycode.model.my_enum.Status
import com.tkip.mycode.model.peer.OnPeer
import com.tkip.mycode.util.tools.anim.AnimUtil
import com.tkip.mycode.util.tools.anim.gone
import com.tkip.mycode.util.tools.anim.visi


class OnVisible {

    companion object {

        fun show(animId: Int?, vararg views: View) {
            views.forEach { v ->
                animId?.let { AnimUtil.show(v, it, false) } ?: let { v.visibility = View.VISIBLE }
            }
        }

        fun hide(animId: Int?, vararg views: View) {
            views.forEach { v ->
                animId?.let { AnimUtil.hide(v, it, false) } ?: let { v.visibility = View.INVISIBLE }
            }
        }

        fun gone(vararg views: View) {
            views.forEach { it.visibility = View.GONE }
        }

        /**
         *  for Button : visibility & enable
         */
        fun showEnable(animId: Int?, vararg views: View){
            show(animId, *views)
            views.forEach { v->(v as Button).isEnabled = true}
        }

        /**
         */
        fun enable(bool:Boolean, vararg views: Button){
            views.forEach { v->   v.isEnabled = bool }
        }


        /**
         * 단순 show hide : DB 시작할때 끝날때
         */
        fun toggleSimple(isOn: Boolean, vararg views:View) {
            views.forEach { if(isOn) it.visi() else it.gone() }
        }

        fun toggleDB(isStart: Boolean, toShow: Array<View>?, toHide: Array<View>?) {
            toShow?.let { for (v in it) v.visibility = if (isStart) View.VISIBLE else View.INVISIBLE }
            toHide?.let { for (v in it) v.visibility = if (isStart) View.INVISIBLE else View.VISIBLE }
        }

        fun toggleDB(isStart: Boolean, toShow: View?, toHide: View?) {
            toShow?.let { it.visibility = if (isStart) View.VISIBLE else View.INVISIBLE }
            toHide?.let { it.visibility = if (isStart) View.INVISIBLE else View.VISIBLE }
        }

        /**
         * isEmpty = return true
         */
        fun toggle(isEmpty: Boolean, toShow: Array<View>?, toHide: Array<View>?): Boolean {
            return if (isEmpty) {
                toShow?.let { for (v in it) v.visibility = View.GONE }
                toHide?.let { for (v in it) v.visibility = View.VISIBLE }
                true
            } else {
                toShow?.let { for (v in it) v.visibility = View.VISIBLE }
                toHide?.let { for (v in it) v.visibility = View.GONE }
                false
            }
        }


        /**
         *  rv 전용
         */
        fun toggleIsEmpty(isEmpty: Boolean, toShow: Array<View>?, toHide: Array<View>?): Boolean {
            return if (isEmpty) {
                toShow?.let { for (v in it) AnimUtil.showOverUp(v, false) }
                toHide?.let { for (v in it) AnimUtil.hideNormal(v) }
                true
            } else {
                toShow?.let { for (v in it) AnimUtil.hideNormal(v) }
                toHide?.let { for (v in it) AnimUtil.showNormal(v) }
                false
            }
        }

        /**
         *  Nodata 전용
         *  avLoading은 언제나 hide
         */
        fun toggleNoData(isEmpty: Boolean, toShow: Array<View>?, avLoading: View): Boolean {
            return if (isEmpty) {
                toShow?.let { for (v in it) AnimUtil.showSlideUp(v, false) }
                AnimUtil.hideNormal(avLoading)
                true
            } else {
                toShow?.let { for (v in it) AnimUtil.hideNormal(v) }
                AnimUtil.hideNormal(avLoading)
                false
            }
        }

        /**
         *  Fade in/out
         */
        fun fadeToggle(isStart: Boolean, toShow: Array<View>?, toHide: Array<View>?): Boolean {
            return if (isStart) {
                toShow?.let { for (v in it) AnimUtil.showFadeIn(v, false) }
                toHide?.let { for (v in it) AnimUtil.hideNormal(v) }
                true
            } else {
                toShow?.let { for (v in it) AnimUtil.hideFadeOut(v, false) }
                toHide?.let { for (v in it) AnimUtil.showFadeIn(v, false) }
                false
            }
        }

        /**
         * 0.5초후에 animation
         */
        fun showHandler(vararg views: View) {
            for (v in views) {
                Handler().postDelayed({
                    AnimUtil.showSlideUp(v, false)
                }, 500)
            }
        }

        /**
         * swiper 안쪽 view의 height 설정.
         */
        fun setSwipeHeight(onView: View, underView: View) {
            Handler().post {
                val params = underView.layoutParams
                params.width = LinearLayout.LayoutParams.WRAP_CONTENT
                params.height = onView.height
                underView.layoutParams = params
            }
        }

        fun setWidthMatchParent(isMatch: Boolean, view: View) {
            val params = view.layoutParams
            params.width = if (isMatch) LinearLayout.LayoutParams.MATCH_PARENT else LinearLayout.LayoutParams.WRAP_CONTENT
            params.height = LinearLayout.LayoutParams.WRAP_CONTENT
            view.layoutParams = params
        }


        /******************************************************************************
         ****** visibility for xml ***************************************************
         *****************************************************************************/

        @JvmStatic
        fun showSupervisor() =
                if (OnPeer.peer.type.gte(PeerType.ADMIN_SUPERVISOR.no)) View.VISIBLE
                else View.GONE


        @JvmStatic
        fun showAdmin() =
                if (OnPeer.isAdmin(false)) View.VISIBLE
                else View.GONE

        @JvmStatic
        fun showSameUID(uid: String?): Int {
            return if (uid == null) {
                View.GONE
            } else {
                if (OnPeer.myUid() == uid) View.VISIBLE
                else View.GONE
            }
        }


        @JvmStatic
        fun showSameStatus(status: Status, status2: Status) =
                if (status == status2) View.VISIBLE
                else View.GONE


        /**         * storeMenu viewHolder         */
        @JvmStatic
        fun setVisi(view: View): Int {
            if (view.visibility == View.VISIBLE) return View.GONE
            else return View.VISIBLE
        }


    }


}