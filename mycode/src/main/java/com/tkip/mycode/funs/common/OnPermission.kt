package com.tkip.mycode.funs.common

import android.Manifest
import android.Manifest.permission.ACCESS_FINE_LOCATION
import android.Manifest.permission.READ_EXTERNAL_STORAGE
import android.app.Activity
import android.content.ActivityNotFoundException
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.net.Uri
import android.provider.MediaStore
import android.provider.Settings
import android.util.Log
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import com.tkip.mycode.R
import com.tkip.mycode.dialog.alert.MyAlert
import com.tkip.mycode.init.REQ_ALBUM
import com.tkip.mycode.init.REQ_PICK
import com.tkip.mycode.init.REQ_PICK_MULTI
import com.tkip.mycode.init.REQ_READ_EXSTORAGE
import com.tkip.mycode.init.base.Cons
import com.tkip.mycode.init.base.getStr
import com.tkip.mycode.init.part.CodeApp
import com.tkip.mycode.util.lib.photo.album.AlbumActivity

class OnPermission {

    companion object {

        /********** permission ********************/

//        const val REQ_PICK = 1234
//        const val REQ_READ_EXSTORAGE = 7500
//        const val REQ_FINE_LOC = 7705

        fun hasPermission(context: Context, permission: String) =
                ContextCompat.checkSelfPermission(context, permission) == PackageManager.PERMISSION_GRANTED


        /**
         * 단말기의 갤러리. AlbumActivity와 다름!!
         */
        fun onOpenGallery(activity: Activity, isGranted: Boolean, multi: Boolean) {
            if (isGranted) {
                /*** 다시 체크하면서 intent날림. permission check -> gotoAlbum or requestPermissions*/
                val readPermission = ContextCompat.checkSelfPermission(activity, READ_EXTERNAL_STORAGE)
                if (readPermission == PackageManager.PERMISSION_GRANTED) {
                    if (multi) activity.startActivityForResult(Intent.createChooser(intentPickMulti(), "Select Picture"), REQ_PICK_MULTI)
                    else activity.startActivityForResult(intentPickSingle(), REQ_PICK)
                } else ActivityCompat.requestPermissions(activity, arrayOf(READ_EXTERNAL_STORAGE), REQ_READ_EXSTORAGE)
            } else informAbout_READ_PERMISSION(activity)
        }

        fun intentPickSingle() =
                Intent().apply {
                    action = Intent.ACTION_PICK
                    type = MediaStore.Images.Media.CONTENT_TYPE
                }

        fun intentPickMulti() =
                Intent().apply {
                    action = Intent.ACTION_PICK
                    type = MediaStore.Images.Media.CONTENT_TYPE
                    putExtra(Intent.EXTRA_ALLOW_MULTIPLE, true)   // muliple selection
                }


        fun onOpenAlbum(isGranted: Boolean, activity: Activity) {

//            if (versionGteQ)
//                OnSAF.openSAF(activity)
//                1개씩 올려보자.
//                activity.startActivityForResult(intentPickSingle(), REQ_PICK)
//            else
                if (isGranted) {
                    /***  다시 체크하면서 intent날림.  permission check -> gotoAlbum or requestPermissions*/
                    val readPermission = ContextCompat.checkSelfPermission(activity, READ_EXTERNAL_STORAGE)
                    if (readPermission == PackageManager.PERMISSION_GRANTED) activity.startActivityForResult(Intent(activity, AlbumActivity::class.java), REQ_ALBUM)
                    else ActivityCompat.requestPermissions(activity, arrayOf(READ_EXTERNAL_STORAGE), REQ_READ_EXSTORAGE)
                } else informAbout_READ_PERMISSION(activity)
        }

        /******************************************************************************
         * 퍼미션 거절할때 안내멘트
         ******************************************************************************/
        fun informAbout_READ_PERMISSION(activity: Activity) {
            // 거부한 적이 있는 경우.
            if (ActivityCompat.shouldShowRequestPermissionRationale(activity, READ_EXTERNAL_STORAGE)) {
                Log.d(Cons.TAG, "informAbout_READ_PERMISSION 거부한 적 있음.")
            } else {
                Log.d(Cons.TAG, "informAbout_READ_PERMISSION 거부 안 했다 false  ")
                showDenyAlert(activity)
            }
        }

        fun informAbout_FINE_LOC_PERMISSION(activity: Activity) {
            // 거부한 적이 있는 경우.
            if (ActivityCompat.shouldShowRequestPermissionRationale(activity, ACCESS_FINE_LOCATION)) {
                Log.d(Cons.TAG, "informAbout_FINE_LOC_PERMISSION 거부한 적 있음.")
            } else {
                Log.d(Cons.TAG, "informAbout_FINE_LOC_PERMISSION 거부 안 했다 false  ")
                showDenyAlert(activity)
            }
        }

        private fun showDenyAlert(activity: Activity) {
            MyAlert.showConfirm(activity, R.string.info_permission_denied.getStr(),
                    ok = {
                        try {
                            val intent = Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS)
                                    .setData(Uri.parse("package:" + (activity).packageName))
                            (activity).startActivity(intent)
                        } catch (e: ActivityNotFoundException) {
                            val intent = Intent(Settings.ACTION_MANAGE_APPLICATIONS_SETTINGS)
                            (activity).startActivity(intent)
                        }
                    }, cancel = {})
        }


        /**
         * check mainLoc Permission
         */
        fun checkLocation(): Boolean {
            val fineLocPermission = ContextCompat.checkSelfPermission(CodeApp.getAppContext(), Manifest.permission.ACCESS_FINE_LOCATION)
            val coarseLocPermission = ContextCompat.checkSelfPermission(CodeApp.getAppContext(), Manifest.permission.ACCESS_COARSE_LOCATION)

            return fineLocPermission == PackageManager.PERMISSION_GRANTED && coarseLocPermission == PackageManager.PERMISSION_GRANTED

        }


    }

}