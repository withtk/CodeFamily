package com.tkip.mycode.funs.custom.number

import android.content.Context
import android.view.LayoutInflater
import android.widget.LinearLayout
import androidx.databinding.DataBindingUtil
import com.tkip.mycode.R
import com.tkip.mycode.databinding.ViewNumberBtnBinding
import com.tkip.mycode.databinding.ViewTvInfoBinding
import com.tkip.mycode.funs.common.OnVisible
import com.tkip.mycode.funs.common.onClick
import com.tkip.mycode.init.base.getStr
import com.tkip.mycode.util.tools.anim.showSlideUp

/**
 * 금액 기입시 활용.
 */
class ViewNumberBtn : LinearLayout {
    lateinit var b: ViewNumberBtnBinding

    constructor(context: Context) : super(context)
    constructor(
            context: Context,
            info: String?,
            onChunMaan: ((d: Int) -> Unit)?,
            onBaekMaan: ((d: Int) -> Unit)?,
            onShipMaan: ((d: Int) -> Unit)?,
            onMaan: ((d: Int) -> Unit)?,
            onChun: ((d: Int) -> Unit)?,
            onBaek: ((d: Int) -> Unit)?,
            onShip: ((d: Int) -> Unit)?,
            onPlus1: ((d: Int) -> Unit)?,
            onMinus1: ((d: Int) -> Unit)?,
            onInit: ((d: Int?) -> Unit)?

    ) : super(context) {

        b = DataBindingUtil.inflate(LayoutInflater.from(context), R.layout.view_number_btn, this, true)

        b.chunMaan = onChunMaan != null
        b.baekMaan = onBaekMaan != null
        b.shipMaan = onShipMaan != null
        b.maan = onMaan != null
        b.chun = onChun != null
        b.baek = onBaek != null
        b.ship = onShip != null
        b.plus1 = onPlus1 != null
        b.minus1 = onMinus1 != null
        b.init = onInit != null

        b.btnChunMaan.onClick { onChunMaan?.invoke(10000000) }
        b.btnBaekMaan.onClick { onBaekMaan?.invoke(1000000) }
        b.btnShipMaan.onClick { onShipMaan?.invoke(100000) }
        b.btnMaan.onClick { onMaan?.invoke(10000) }
        b.btnChun.onClick { onChun?.invoke(1000) }
        b.btnBaek.onClick { onBaek?.invoke(100) }
        b.btnShip.onClick { onShip?.invoke(10) }
        b.btnPlus1.onClick { onPlus1?.invoke(1) }
        b.btnMinus1.onClick { onMinus1?.invoke(-1) }
        b.btnInit.onClick { onInit?.invoke(null) }

        setInfo(info ?: R.string.info_chagne_pnt_select_number.getStr())
    }

    fun setInfo(str: String?) {
        if (str != b.tvInfo.text.toString()) b.tvInfo.showSlideUp(true)   // 글자 바꼈을 때만 애니메이션
        b.tvInfo.text = str
    }

    fun resetEnable(bool: Boolean) {
        OnVisible.enable(bool, b.btnBaekMaan, b.btnShipMaan, b.btnMaan, b.btnChun, b.btnBaek, b.btnShip, b.btnPlus1, b.btnMinus1, b.btnInit)
    }




}