package com.tkip.mycode.funs.common

import android.text.Editable
import android.text.TextWatcher
import android.view.View
import android.widget.*
import androidx.core.widget.NestedScrollView
import androidx.recyclerview.widget.RecyclerView
import androidx.viewpager.widget.ViewPager
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.google.android.material.textfield.TextInputLayout
import com.tkip.mycode.R
import com.tkip.mycode.dialog.toast.TToast
import com.tkip.mycode.init.base.*
import com.tkip.mycode.util.tools.anim.*
import com.tkip.mycode.util.tools.date.getDiffDay
import com.tkip.mycode.util.tools.date.getLocalTimeString
import java.text.SimpleDateFormat
import java.util.*


/*************************************************************************************/
/********************  ***********************************************************/
/*************************************************************************************/

//fun View.onClick(on: (View) -> Unit) {
//    setOnClickListener { v -> on(v) }
//}

fun Button.onClick(on: (View) -> Unit) {
    setOnClickListener { v -> on(v) }
}

fun ImageButton.onClick(on: (View) -> Unit) {
    setOnClickListener { v -> on(v) }
}


/** * 애니메이션 tran */
fun View.tranPair(nameRes: Int) = androidx.core.util.Pair(this, nameRes.getStr())


/** * Picker에 다시 셋팅될 값. */
fun RadioButton.pickerValue() = if (text == R.string.ellipsis.getStr()) null else text.toString()


/** TextInputEditText 에서  * 1. minLength 체크 * 2. set Error */
fun EditText.isLtLength(min: Int, layout: TextInputLayout?, stringID: Int): Boolean {
    if (isEnabled && getNullStr() != null) {
        if (getStr().isLt(min)) {
            if (layout != null) layout.error = String.format(stringID.getStr(), min)
            else error = String.format(stringID.getStr(), min)
            return true
        }
    }
    layout?.let { lay -> lay.error?.let { lay.error = null } }
    return false
}

/** * Blank, null 체크 */
fun EditText.isNullBlank(msg: Int?): Boolean {
    if (this.text.isNullOrBlank()) {
        msg?.let { TToast.showAlert(msg) }
        return true
    }
    return false
}

/** * 그냥 getText.toString.trim() * 비어있으면 return "" */
fun EditText.getStr(): String = this.text.toString().trim()

/** * 비어있으면 return Null * Null을 얻기위한 메서드 */
fun EditText.getNullStr(): String? = if (this.text.isNullOrBlank()) null else this.text.toString().trim()

/** *  검색어 가져오기. lowerCase로 */
fun EditText.keyword() = this.getNullStr()?.toLowerCase(Locale.ENGLISH)

fun Editable?.getNullLower(): String? = if (this == null || this.isBlank()) null else this.toString().trim().toLowerCase(Locale.ENGLISH)

/** * Blank, null 체크 */
fun TextView.isNullBlank(msg: Int?): Boolean {
    if (this.text.isNullOrBlank()) {
        msg?.let { TToast.showAlert(msg) }
        return true
    }
    return false
}

/** * 비어있으면 return Null * Null을 얻기위한 메서드 */
fun TextView.getNullStr(): String? = if (this.text.isNullOrBlank()) null else this.text.toString().trim()
fun TextView.getStr(): String = this.text.toString().trim()
/**  가격을 숫자로 받아올 때 */
fun TextView.getInt(): Int = if(this.getStr().removeComma().isNumeric())  this.getStr().removeComma().toInt() else 0

fun TextView.setLocaleDateString(longDate: Long, format: SimpleDateFormat) {
    visibility = View.VISIBLE
    text = longDate.getLocalTimeString(format)
}


/** * text + visibility */
fun TextView.textGone(str: String?) {
    str?.let {
        text = it
        this.showSlideUp(true)
    } ?: this.gone()
}

fun TextView.textHide(str: String?) {
    str?.let {
        text = it
        this.showSlideUp(true)
    } ?: this.hide()
}


/** * 1월 1일 */
//fun TextView.monthDate(m: Int, d: Int) {
//    val str = String.format(R.string.cal_month_date.getStr(), m + 1, d)
//    this.text = str
//}

/** * 1일간  return : Int */
fun TextView.getIntDiffDay(start: Long, end: Long): Int {
    val diff = start.getDiffDay(end)
    this.text = diff.getStrTerm()
    return diff
}

/** * 1일간  */
fun TextView.setTerm(term: Int) {
    if (term > 0) {
        this.text = term.getStrTerm()
        this.visi()
    } else gone()
}

fun TextView.diffDay(diff: Int) {
    this.text = diff.getStrTerm()
}

/** * comma 제거. return : Int */
fun TextView.getRemoveComma(): Int = text.toString().removeComma().toInt()


/** * 중간공백까지 모두 제거해서 get String */
fun EditText.getStrDiscard(): String = this.text.toString().trim().discard0()

fun EditText.textListener(after: (Editable?) -> Unit, before: (CharSequence?, Int, Int, Int) -> Unit, on: (CharSequence?, Int, Int, Int) -> Unit) {
    addTextChangedListener(object : TextWatcher {
        override fun afterTextChanged(s: Editable?) {
            after(s)
        }

        override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
            before(s, start, count, after)
        }

        override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
            on(s, start, before, count)
        }
    })
}


fun CheckBox.onCheckedChange(change: (buttonView: CompoundButton?, isChecked: Boolean) -> Unit) {
    setOnCheckedChangeListener { buttonView, isChecked -> change(buttonView, isChecked) }
}


fun ViewPager.onPageChange(onState: (Int) -> Unit, onScrolled: (position: Int, positionOffset: Float, positionOffsetPixels: Int) -> Unit, onSelected: (Int) -> Unit) {
    addOnPageChangeListener(object : ViewPager.OnPageChangeListener {
        override fun onPageScrollStateChanged(state: Int) {
//            lllogD("onPageChange onPageScrollStateChanged state $state")
            onState(state)
        }

        override fun onPageScrolled(position: Int, positionOffset: Float, positionOffsetPixels: Int) {
//            lllogD("onPageChange onPageScrolled  position $position positionOffset $positionOffset positionOffsetPixels $positionOffsetPixels")
            onScrolled(position, positionOffset, positionOffsetPixels)
        }

        override fun onPageSelected(position: Int) {
            lllogD("onPageChange onPageSelected  position $position")
            onSelected(position)
        }
    })
}

fun Spinner.onItemSelected(onNothin: (parent: AdapterView<*>?) -> Unit, onSelected: (parent: AdapterView<*>?, view: View?, position: Int, id: Long) -> Unit) {
    onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
        override fun onNothingSelected(parent: AdapterView<*>?) {
//            Log.d(Cons.TAG, "spin onNothingSelected")
            onNothin(parent)
        }

        override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
//            Log.d(Cons.TAG, "spinnerStatus :  ")
            onSelected(parent, view, position, id)
        }
    }
}

/***********************************************************************************************
 ***********************************************************************************************/
fun NestedScrollView.onScroll(onScrollChange: (v: NestedScrollView?, scrollX: Int, scrollY: Int, oldX: Int, oldY: Int) -> Unit) {
    setOnScrollChangeListener(NestedScrollView.OnScrollChangeListener { v, scrollX, scrollY, oldScrollX, oldScrollY -> onScrollChange(v, scrollX, scrollY, oldScrollX, oldScrollY) })
}
/*
fun NestedScrollView.setFabFromScroll(rv: RecyclerView, fab: FloatingActionButton) {
    var i = 0
    this.onScroll { v, x, y, oldX, oldY ->
        i++
//        v?.let {
//            val measureH = it.measuredHeight
//            val rvH =  rv.measuredHeight
//            if (measureH > 500) fab.showSlideLeft(false)
//            if (measureH > rvH) fab.hideSlideRight(false)
//        }

        if (y < oldY) fab.showSlideLeft(false)
//            if (y > oldY) lllogI("nestedScrollView $i DOWN")
//            if (y < oldY) lllogI("nestedScrollView $i UP")
//        if (y == 0) lllogI("nestedScrollView $i TOP SCROLL")
        lllogI("nestedScrollView $i   measuredHeight  : ${v?.measuredHeight}  ")
        lllogI("nestedScrollView $i   rvH  : ${rv.measuredHeight}  ")
    }
}*/



/************************************************
 * listener
 *************************************************/

fun RecyclerView.myScroll(onScrolled: (RecyclerView, dx: Int, dy: Int) -> Unit, onScrollStateChanged: (RecyclerView, newState: Int) -> Unit) {
    addOnScrollListener(object : RecyclerView.OnScrollListener() {
        override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
            super.onScrolled(recyclerView, dx, dy)
            onScrolled(recyclerView, dx, dy)
        }

        override fun onScrollStateChanged(recyclerView: RecyclerView, newState: Int) {
            super.onScrollStateChanged(recyclerView, newState)
            onScrollStateChanged(recyclerView, newState)
        }
    })
}


/** * 스크롤에 따라 버튼 생성/제거 */
fun RecyclerView.scrollUp(view: View) {
    addOnScrollListener(object : RecyclerView.OnScrollListener() {
        override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
            super.onScrolled(recyclerView, dx, dy)
            AnimUtil.showFadeIn(dy == 0, view, true)
        }

        override fun onScrollStateChanged(recyclerView: RecyclerView, newState: Int) {
            super.onScrollStateChanged(recyclerView, newState)
        }
    })
}


fun BottomSheetBehavior<LinearLayout>.myToggle() {
    this.state =
            if (this.state == BottomSheetBehavior.STATE_EXPANDED) BottomSheetBehavior.STATE_COLLAPSED
            else BottomSheetBehavior.STATE_EXPANDED
}




