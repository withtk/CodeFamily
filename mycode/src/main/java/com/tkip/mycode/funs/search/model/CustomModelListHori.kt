package com.tkip.mycode.funs.search.model

import android.annotation.SuppressLint
import android.content.Context
import android.view.LayoutInflater
import android.widget.LinearLayout
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import com.tkip.mycode.R
import com.tkip.mycode.databinding.CustomModelListHoriBinding
import com.tkip.mycode.init.RV_HORI
import com.tkip.mycode.model.board.Board
import com.tkip.mycode.model.flea.Flea
import com.tkip.mycode.model.my_enum.DataType
import com.tkip.mycode.model.post.Post
import com.tkip.mycode.model.room.Room
import com.tkip.mycode.model.store.Store
import com.tkip.mycode.nav.board.tab_first.BoardAdapter
import com.tkip.mycode.nav.flea.FleaAdapter
import com.tkip.mycode.nav.post.PostAdapter
import com.tkip.mycode.nav.room.RoomAdapter
import com.tkip.mycode.nav.store.StoreAdapter
import com.tkip.mycode.util.lib.SnapHelper
import com.tkip.mycode.util.lib.retrofit2.ESget
import com.tkip.mycode.util.tools.anim.animRotateArrow
import com.tkip.mycode.util.tools.anim.toggleUpDown

/**
 * for 모든 모델 horizontal
 */
class CustomModelListHori : LinearLayout {
    private lateinit var b: CustomModelListHoriBinding

    private lateinit var dataType: DataType
    private var rvType: Int = RV_HORI
    private var ori: Int  = GridLayoutManager.HORIZONTAL
    private var span: Int? = null
    private lateinit var query: String


    private val boardItems by lazy { arrayListOf<Board>() }
    private val postItems by lazy { arrayListOf<Post>() }
    private val fleaItems by lazy { arrayListOf<Flea>() }
    private val roomItems by lazy { arrayListOf<Room>() }
    private val storeItems by lazy { arrayListOf<Store>() }
    private var storeAdapter: StoreAdapter? = null
    private var postAdapter: PostAdapter? = null
    private var fleaAdapter: FleaAdapter? = null
    private var roomAdapter: RoomAdapter? = null
    private var boardAdapter: BoardAdapter? = null


    constructor(context: Context) : super(context)

    constructor(context: Context, title: String?, showArrow: Boolean, dataType: DataType, rvType: Int, vert: Boolean, span: Int?, query: String, onItem: ((Any) -> Unit)?) : super(context) {
        b = DataBindingUtil.inflate(LayoutInflater.from(context), R.layout.custom_model_list_hori, this, true)
        this.dataType = dataType
        this.rvType = rvType
        this.ori = if(vert) GridLayoutManager.VERTICAL else GridLayoutManager.HORIZONTAL
        this.span = span
        this.query = query

        /**  상단바 세팅*/
        b.inTopTitle.title = title
        b.inTopTitle.showArrow = showArrow
        if (showArrow) {
            b.inTopTitle.lineTitle.setOnClickListener {
                b.inTopTitle.ivArrow.animRotateArrow()
                b.rv.toggleUpDown(true)
            }
        }


        /**  Adapter 세팅*/
        setupRV(onItem)

        /**  바로 받아오기. */
        getEsDataList(0)
    }


    /** * adapter 세팅 */
    @SuppressLint("WrongConstant")
    private fun setupRV(onItem: ((Any) -> Unit)?) {

        when (dataType) {
            DataType.BOARD -> {
                if (boardAdapter == null) boardAdapter = BoardAdapter(boardItems, rvType, onItem)
                b.rv.adapter = boardAdapter
            }
            DataType.POST -> {
                if (postAdapter == null) postAdapter = PostAdapter(postItems, rvType, onItem)
                b.rv.adapter = postAdapter
            }
            DataType.FLEA -> {
                if (fleaAdapter == null) fleaAdapter = FleaAdapter(fleaItems, rvType, onItem)
                b.rv.adapter = fleaAdapter
            }
            DataType.ROOM -> {
            if (roomAdapter == null) roomAdapter = RoomAdapter(roomItems, rvType, onItem)
            b.rv.adapter = roomAdapter
        }
            DataType.STORE -> {
                if (storeAdapter == null) storeAdapter = StoreAdapter(storeItems, rvType, onItem)
                b.rv.adapter = storeAdapter
            }
            else -> {
            }
        }

        /**  GridLayout 세팅*/
        span?.let {
            b.rv.layoutManager = GridLayoutManager(context, it, ori, false)
        }

//        b.rv.layoutManager = LinearLayoutManager(context, ori, false)


        /**  SnapHelper 세팅*/
        SnapHelper().attachToRecyclerView(b.rv)

    }


    /** * 가져오기  */
    private fun getEsDataList(from: Int) {

        when (dataType) {
            DataType.BOARD -> ESget.getBoardList(query,
                    success = {
                        it?.let { list ->
                            boardItems.addAll(list)
                            boardAdapter?.notifyDataSetChanged()
                        }
                    },
                    fail = {})
            DataType.POST -> ESget.getPostList(query,
                    success = {
                        it?.let { list ->
                            postItems.addAll(list)
                            postAdapter?.notifyDataSetChanged()
                        }
                    },
                    fail = {})

            DataType.FLEA -> ESget.getFleaList(query,
                    success = {
                        it?.let { list ->
                            fleaItems.addAll(list)
                            fleaAdapter?.notifyDataSetChanged()
                        }
                    },
                    fail = {})

            DataType.ROOM -> ESget.getRoomList(query,
                    success = {
                        it?.let { list ->
                            roomItems.addAll(list)
                            roomAdapter?.notifyDataSetChanged()
                        }
                    },
                    fail = {})
            DataType.STORE -> ESget.getStoreList(query,
                    success = {
                        it?.let { list ->
                            storeItems.addAll(list)
                            storeAdapter?.notifyDataSetChanged()
                        }
                    },
                    fail = {})

            else -> {
            }
        }


    }


}