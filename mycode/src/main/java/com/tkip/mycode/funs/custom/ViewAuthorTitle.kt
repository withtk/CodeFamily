package com.tkip.mycode.funs.custom

import android.content.Context
import android.view.LayoutInflater
import android.widget.LinearLayout
import androidx.databinding.DataBindingUtil
import com.tkip.mycode.R
import com.tkip.mycode.databinding.ViewAuthorTitleBinding
import com.tkip.mycode.dialog.alert.MyAlert
import com.tkip.mycode.init.base.getStr
import com.tkip.mycode.init.inter.interAuthorTitle
import com.tkip.mycode.model.peer.Peer
import com.tkip.mycode.util.lib.photo.glide.setFaceGlide
import com.tkip.mycode.util.lib.retrofit2.ESget

/**
 * white line 상단의 글쓴이 박스.
 */
class ViewAuthorTitle : LinearLayout {
    lateinit var b: ViewAuthorTitleBinding
    var peer: Peer? = null

    constructor(context: Context) : super(context)
    constructor(context: Context, anonymous: Boolean, uid: String, addDate: Long, onPeer: (Peer) -> Unit) : super(context) {
        b = DataBindingUtil.inflate(LayoutInflater.from(context), R.layout.view_author_title, this, true)
        b.addDate = addDate

        ESget.getPeer(uid,
                success = {
                    this.peer = it
                    this.peer?.let { peer1 ->

                        /**   익명체크  */
                        if (anonymous) {

                            b.ivFace.setImageDrawable(context.getDrawable(R.drawable.ic_person_gray))
                            b.tvNick.text = R.string.nick_anonymous.getStr()

                        } else {

//                            Glide.with(context)
//                                    .load(peer1.faceThumb)
//                                    .thumbnail(0.1f)
//                                    .into(b.ivFace)
                            b.ivFace.setFaceGlide(peer?.faceThumb)
                            b.tvNick.text = peer1.nick

                            b.inter = interAuthorTitle(
                                    onPeerClick = {
                                        MyAlert.showPeerInfo(context, peer1.uid, onLobby = {})
                                        onPeer(peer1)
                                    })
                        }
                    }


                }, fail = {})


    }

}