package com.tkip.mycode.funs.custom.menu

import android.content.Context
import android.util.AttributeSet
import android.view.LayoutInflater
import android.widget.LinearLayout
import androidx.databinding.DataBindingUtil
import com.tkip.mycode.R


class ViewMenuEditText : LinearLayout {
    public lateinit var b: com.tkip.mycode.databinding.ViewMenuEdittextBinding

    constructor(context: Context?  ,minus:(ViewMenuEditText)->Unit ) : super(context) {
        b = DataBindingUtil.inflate(LayoutInflater.from(context), R.layout.view_menu_edittext, this, true)

     /*   b.iv.setOnClickListener{
            onClickIv(this)
        }*/
        b.btnMinus.setOnClickListener{
            minus(this)
        }

    }


    constructor(context: Context, attrs: AttributeSet?) : super(context, attrs) {
        init( )
    }

    constructor(context: Context, attrs: AttributeSet?, defStyleAttr: Int) : super(context, attrs, defStyleAttr) {
        init( )
    }


    private fun init( ) {
        /* // inflate(getContext(), R.layout.beacon_setup_view, this);
         val inflater = context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
         inflater.inflate(R.layout.view_info, this)

         tvTitle = findViewById(R.id.tv_title)
         tvDescription = findViewById(R.id.tv_description)
         ivIcon = findViewById(R.id.iv_icon)
         btn = findViewById<Button>(R.id.btn)*/
    }


}