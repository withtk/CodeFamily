package com.tkip.mycode.funs.common

import android.content.ClipData
import android.content.ClipboardManager
import android.content.Context
import android.content.Context.CLIPBOARD_SERVICE
import android.widget.LinearLayout
import com.tkip.mycode.R
import com.tkip.mycode.dialog.toast.toastNormal
import com.tkip.mycode.init.part.CodeApp
import io.github.ponnamkarthik.richlinkpreview.RichLinkViewTwitter
import io.github.ponnamkarthik.richlinkpreview.ViewListener
import java.util.regex.Pattern


class OnFunction {

    companion object {

        /**
         * 클립보드에  복사 기능
         * @param context
         * @param link
         */
        fun setClipBoardLink(str: String) {
            val clipboardManager = CodeApp.getAppContext().getSystemService(CLIPBOARD_SERVICE) as ClipboardManager
            clipboardManager.setPrimaryClip(ClipData.newPlainText("label", str))

            // 너무 길면 좀 자르자.
            val limit = 100
            val copied = if (str.length > limit) str.substring(0, limit) + "..." else str

            toastNormal(String.format(CodeApp.getAppContext().getString(R.string.completed_clipboard_copy), copied))
        }


        /*********************************************************************************
         ****************************** URL 만들기 ****************************************/
        /**
         * setence(문장)안의 url을 찾아서 프리뷰 만들기
         */
        fun setUrlPreview(context: Context, sentence: String?, linear: LinearLayout, onSuccess: (url: String) -> Unit) {
            sentence?.let {
                getUrlFromSentence(it)?.let { url ->
                    lllogI("setUrlPreview onSuccess url $url")
                    onSuccess(url)

                    val rich = RichLinkViewTwitter(context)
                            .apply {
//                                setOnClickListener{
//                                    lllogD("setUrlPreview setOnClickListener ")
//                                    OnWeb.gotoWebViewActivity(context,url)
//                                }

                                setLink(url, object : ViewListener {
                                    override fun onSuccess(status: Boolean) {
                                        lllogI("setUrlPreview onSuccess status $status")
                                    }

                                    override fun onError(e: Exception?) {
                                        lllogI("setUrlPreview onError e $e")
                                    }
                                })
                            }

                    linear.addView(rich)
                }
            }

        }

        // Pattern for recognizing a URL, based off RFC 3986
        private val urlPattern = Pattern.compile(
                "(?:^|[\\W])((ht|f)tp(s?):\\/\\/|www\\.)"
                        + "(([\\w\\-]+\\.){1,}?([\\w\\-.~]+\\/?)*"
                        + "[\\p{Alnum}.,%_=?&#\\-+()\\[\\]\\*$~@!:/{};']*)",
                Pattern.CASE_INSENSITIVE or Pattern.MULTILINE or Pattern.DOTALL)

        private fun getUrlFromSentence(sentence: String): String? {

            val matcher = urlPattern.matcher(sentence)
            while (matcher.find()) {
                val matchStart: Int = matcher.start(1)
                val matchEnd: Int = matcher.end()
                println("matchStart : $matchStart  matchEnd : $matchEnd")
                val result = sentence.substring(matchStart, matchEnd)
                println("result : $result  ")
                return result
                // now you have the offsets of a URL match
            }
            return null
        }

        /*********************************************************************************/


    }


}