package com.tkip.mycode.funs.custom

import android.content.Context
import android.text.Editable
import android.text.TextWatcher
import android.util.AttributeSet
import android.view.LayoutInflater
import android.view.inputmethod.EditorInfo
import android.widget.LinearLayout
import android.widget.TextView
import androidx.databinding.DataBindingUtil
import com.tkip.mycode.R
import com.tkip.mycode.databinding.ViewSearchBarBinding
import com.tkip.mycode.util.tools.anim.AnimUtil

/**
 * 검색 on Map
 */
class ViewSearchBar : LinearLayout {
    private lateinit var b: ViewSearchBarBinding

    constructor(context: Context?, btnMenu: () -> Unit, search: (String) -> Unit, cancel: () -> Unit) : super(context) {

        b = DataBindingUtil.inflate(LayoutInflater.from(context), R.layout.view_search_bar, this, true)

        b.btnMenu.setOnClickListener {
            btnMenu()

        }

        b.etSearch.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                if (s.toString().length > 1) {
                    search(s.toString())
                }
            }

        })

        b.etSearch.setOnEditorActionListener(TextView.OnEditorActionListener { _, actionId, _ ->
            if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                search(b.etSearch.text.toString().trim().replace("\\s".toRegex(), ""))
                return@OnEditorActionListener true
            }
            false
        })


        b.btnClose.setOnClickListener {
            cancel()
            AnimUtil.hideSlideUp(this,true)
        }

    }

    constructor(context: Context) : super(context) {
        init()
    }

    constructor(context: Context, attrs: AttributeSet?) : super(context, attrs) {
        init()
    }

    constructor(context: Context, attrs: AttributeSet?, defStyleAttr: Int) : super(context, attrs, defStyleAttr) {
        init()
    }

    private fun init() {

    }


}