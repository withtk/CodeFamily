package com.tkip.mycode.funs.common

class OnBoolean(var mChecked: Boolean = false, var changed:(Boolean)->Unit) {

    interface MyBoolChanger {
        fun onVariableChanged(myBool: Boolean)
    }

    lateinit var mListener: MyBoolChanger


    fun setOnVariableChanged(myBoolChanger: MyBoolChanger) {
        this.mListener = myBoolChanger
    }

    fun log(checked: Boolean) {

        if (mChecked != checked) {
            mChecked = checked
//            mListener.onVariableChanged(mChecked)
            changed(mChecked)
        }

    }

}