package com.tkip.mycode.funs.search

import android.os.Bundle
import android.os.Handler
import androidx.annotation.LayoutRes
import androidx.fragment.app.Fragment
import com.tkip.mycode.R
import com.tkip.mycode.databinding.ActivitySearchTotalBinding
import com.tkip.mycode.funs.common.*
import com.tkip.mycode.funs.search.view.SearchModelFrag
import com.tkip.mycode.init.base.*
import com.tkip.mycode.init.inter.interToolbarIcon
import com.tkip.mycode.init.my_bind.BindingActivity
import com.tkip.mycode.model.my_enum.DataType
import com.tkip.mycode.model.mytag.*
import com.tkip.mycode.util.tools.anim.*
import com.tkip.mycode.util.tools.view_pager.OnPager

/**
 * 전체 검색 : board, post, flea, room, store
 */
class SearchTotalActivity : BindingActivity<ActivitySearchTotalBinding>() {
    @LayoutRes
    override fun getLayoutResId() = R.layout.activity_search_total

    private val fragList = arrayListOf<Pair<Fragment, String>>()
    private lateinit var tagAdapter: MyTagAdapter
    private val tagItems = arrayListOf<MyTag>()
    private val wordList = arrayListOf<String>()
//    private var isLastKeyword = false  // 지난 검색어 rv에 표시중인지 여부.

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        initView()
    }

    private fun initView() {
        b.activity = this

        /** 툴바 */
        b.inToolbar.title = R.string.title_search_total.getStr()
        b.inToolbar.backIcon = true
//        b.inToolbar.menuText = R.string.add_ad_unit_manage.getStr()
        b.inToolbar.inter = interToolbarIcon(onFirst = { onBackPressed() }, onTopTitle = {}, onTextMenu = {},onTextMenu2 = null, onIcon = {}, onMore = {})


//        b.frame.addView(CustomSearchTotalPager(this@SearchTotalActivity,supportFragmentManager,manage,uid,countType, onItemClick = {}))

        /** 검색창 */
//        b.etSearch.onTextChange(after = {}, before = { _, _, _, _ -> }, on = { s, _, _, c ->
//            val keyword = s.toString()
//            lllogI("SearchTotalActivity keyword $keyword")
//            if (keyword.length > 1) {
//                Handler().postDelayed({
//                    gotoSearch(keyword)
//                }, 1000)
//            }
//        })

//        b.frameTag.addView(CustomSearchTagView(this@SearchTotalActivity, null,
//                onSearch = { wordList ->
//                    wordList?.let {
//                        gotoSearch(it)
////                        Handler().postDelayed({ lllogI("SearchTotalActivity frameTag onSearch handler") }, 1000)
//                    }
//                }))

        /** 검색창 tagView 세팅 */
        setEtTagView()

        /*** tab & viewPager 세팅 */
//        val modelList = arrayOf(DataType.POST, DataType.BOARD, DataType.FLEA, DataType.ROOM, DataType.STORE)
        arrayOf(DataType.BOARD, DataType.POST, DataType.FLEA, DataType.ROOM, DataType.STORE)
                .forEach { fragList.add(Pair(SearchModelFrag.newInstance(it.no, onScroll = { hideTagRv() }, onItemClick = { }), it.title)) }
        OnPager.commonSetPager(supportFragmentManager, fragList, b.viewPager, b.tabLayout)
        b.viewPager.onPageChange(onState = {}, onScrolled = { _, _, _ -> }, onSelected = { p ->
            (fragList[p].first).let { frag -> if (frag is SearchModelFrag) frag.initSearch() }
        })

        /*** 0번 Frag는 바로 초기 검색 시작. */
        Handler().postDelayed({ (fragList[0].first).let { frag -> if (frag is SearchModelFrag) frag.initSearch() } }, 100)

    }

    /********************************************************************************************
     * 검색창 tagView   **************************************************************************
     *******************************************************************************************/
    private fun setEtTagView() {
        b.tagView.setOnTagClickListener { _, p ->
            b.tagView.remove(p)
            gotoSearch()
            showhideTvInfo()
        }
        //        b.tagView.setOnTagDeleteListener { _, _, _ -> }
        //        b.tagView.setOnTagLongClickListener { _, _ -> }

        b.btnSearch.onClick {
            OnTag.saveMyKeywordList(Sred.ARR_KEYWORD, b.etSearch.keyword())   // 검색내역 저장.
            tagItems.clear()   // tagList 제거
            afterSearch()
            gotoSearch()
        }

        /** * 태그 adapter  */
        tagAdapter = MyTagAdapter(tagItems, MyTagAdapter.rvNormal,
                /**   * MyTagAdapter에서  아이템 클릭.    */
                onItemClick = { _, _, myTag ->
                    b.etSearch.setText("")
                    b.tagView.checkAdd(TAG_1, myTag.name)    // tagView에 추가
                    tagItems.clear()
                    afterSearch()
                    showhideTvInfo()
                    gotoSearch()   // 태그를 클릭한 순간에는 무조건 tagView의 태그를 이용한 검색만 됨. (따라서 keyword는 무조건 null.)
                    OnTag.saveMyKeywordList(Sred.ARR_KEYWORD, myTag.name)   // 검색내역 저장.
                })
        b.rvTag.adapter = tagAdapter


        /*** 처음 포커싱 됐을때 지난 검색결과 보이기 */
//        b.etSearch.setOnFocusChangeListener { _, hasFocus ->
//            if (hasFocus && tagItems.isEmpty() && b.etSearch.keyword() == null) showLastKeyword()
//        }

        /*** 클릭시 지난 검색결과 보이기 */
        b.etSearch.setOnClickListener {
            if (b.etSearch.keyword() == null) showLastKeyword()
//            if (b.etSearch.keyword() == null) if (isLastKeyword) hideTagRv() else showLastKeyword()
        }

        /** * 실시간 태그 검색 */
        b.etSearch.textListener(after = { s ->

            OnVisible.toggleSimple(!s.isNullOrBlank(), b.btnClearEt)   // x버튼 토글
            b.btnSearch.isEnabled = !s.isNullOrBlank()      // search버튼 가능/불가

//            OnValid.readySearch  {
                lllogD("################ OnValid readySearch start ${s.toString()}   ")
                b.etSearch.keyword()?.let { keyword ->
                    OnTag.getEsTagList(10, 0, TagTYPE.ALL, keyword,
                            success = {
                                lllogI("ViewSearchTagView list.size :${it?.size}")
                                it.addCheckTagView(tagItems, b.tagView)
                                afterSearch()
                            }, fail = { afterSearch() })
                } ?: let {
                    // keyword 널이면 list 지워.
                    tagItems.clear()
                    afterSearch()
                }
//            }
        }, before = { _, _, _, _ -> }, on = { _, _, _, _ -> })


        b.btnClearEt.onClick {
            b.etSearch.setText("")
            hideTagRv()
//            gotoSearch()
        }
    }

    /**     * 지난 검색어 표시     */
    private fun showLastKeyword() {
        tagItems.clear()
        OnTag.loadMyKeywordList(Sred.ARR_KEYWORD)?.forEach { tagItems.add(MyTag(it)) }
        afterSearch()
        b.btnClearEt.visi()
//        isLastKeyword = true
    }

    private fun hideTagRv() {
        b.rvTag.hide()
//        isLastKeyword = false
    }

    private fun resetWordList(): ArrayList<String> {
//        val list = arrayListOf<String>()
        wordList.clear()
        b.tagView.tags.forEach { wordList.add(it.text) }   // tagView 넣기.
        b.etSearch.keyword()?.let {
            it.removeSpecial().removeLongSpace().trim().getArraySplitSpace().forEach { str -> wordList.add(str) } // keyword 다듬어서 넣기.
        }
        lllogI("getWordList list ${wordList.size}")
        return wordList
    }

    private fun afterSearch() {
        AnimUtil.hideFadeOut(b.avLoading, true)
        if (tagItems.isEmpty()) b.rvTag.gone()
        else b.rvTag.visi()
        tagAdapter.notifyDataSetChanged()
    }


    /**     * info 보이기 감추기     */
    private fun showhideTvInfo() {
        if (b.tagView.tags.isEmpty()) b.tvInfo.showOverUp(false)
        else b.tvInfo.hideOverDown(false)
    }

    /**********************************************************************************************
     ****** 검색하기 *******************************************************************************
     **********************************************************************************************/

    private fun gotoSearch() {
        resetWordList()
        fragList.forEach {
            val frag = it.first as SearchModelFrag
            frag.customModelList?.getEsDataList(0, wordList)
        }
    }


}