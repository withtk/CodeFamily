package com.tkip.mycode.funs.custom.text

import android.content.Context
import android.view.LayoutInflater
import android.widget.LinearLayout
import androidx.databinding.DataBindingUtil
import com.tkip.mycode.R
import com.tkip.mycode.databinding.ViewTvInfoBigBinding

/**
 * 두개짜리 안내판.
 */
class ViewTvInfoBig : LinearLayout {
    lateinit var b: ViewTvInfoBigBinding

    constructor(context: Context) : super(context)
    constructor(context: Context,   msg: String,   msg2: String) : super(context) {
        b = DataBindingUtil.inflate(LayoutInflater.from(context), R.layout.view_tv_info_big, this, true)
        b.msg = msg
        b.msg2 = msg2

    }


}