package com.tkip.mycode.funs.custom.manual

import android.content.Context
import android.view.LayoutInflater
import android.widget.LinearLayout
import androidx.databinding.DataBindingUtil
import com.tkip.mycode.R
import com.tkip.mycode.databinding.LinearManualMoreBinding
import com.tkip.mycode.funs.common.onCheckedChange
import com.tkip.mycode.init.inter.interManualMore
import com.tkip.mycode.util.tools.anim.gone

/**
 * 1. 매뉴얼 더보기 표시
 * 2. + 동의 표시
 */
class LinearManualMore : LinearLayout {
      lateinit var b: LinearManualMoreBinding

    constructor(context: Context) : super(context)
    constructor(context: Context, manual: String?, onAgree: ((Boolean) -> Unit)?) : super(context) {
        b = DataBindingUtil.inflate(LayoutInflater.from(context), R.layout.linear_manual_more, this, true)

        b.manual = manual
        b.inter = interManualMore {
            b.tvManual1.maxLines = 100
            b.btnMoreInfo.gone()
        }

        b.hasAgree = onAgree != null
        b.cbAgree.onCheckedChange { _, isOn -> onAgree?.invoke(isOn) }
    }


}