package com.tkip.mycode.funs.search.dialog

import android.app.Dialog
import android.content.Context
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import androidx.appcompat.app.AlertDialog
import com.tkip.mycode.R
import com.tkip.mycode.databinding.DialogSearchModelBinding
import com.tkip.mycode.dialog.OnDDD
import com.tkip.mycode.funs.search.view.CustomSearchAll4Bid
import com.tkip.mycode.init.PUT_BOOLEAN
import com.tkip.mycode.init.PUT_DATA_TYPE_NO
import com.tkip.mycode.init.PUT_INFO
import com.tkip.mycode.init.my_bind.BindingDialog
import com.tkip.mycode.model.my_enum.DataType
import com.tkip.mycode.util.tools.radio.CustomRGbasic


/**
 * model 선택해서 가져오는 dialog
 */
class SearchModelDialog : BindingDialog<DialogSearchModelBinding>() {
    override fun getLayoutResId(): Int = R.layout.dialog_search_model
    override fun getSize(): Pair<Float?, Float?> = Pair(0.95f, null)


private var onOk: ((Any) -> Unit)? = null
    //    private var info: String? = null
    private val info by lazy { arguments?.getString(PUT_INFO) }
    private val myDataOnly by lazy { arguments?.getBoolean(PUT_BOOLEAN) ?: true }
//    private val dataType by lazy {        (arguments?.getInt(PUT_DATA_TYPE_NO) ?: DataType.POST.no).getDataType()    }

    companion object {
        @JvmStatic
        fun newInstance(info: String?, myDataOnly: Boolean, dataTypeNo: Int, onOk: ((Any) -> Unit)?) = SearchModelDialog().apply {
            this.onOk = onOk
            arguments = Bundle().apply {
                putString(PUT_INFO, info)
                putBoolean(PUT_BOOLEAN, myDataOnly)
                putInt(PUT_DATA_TYPE_NO, dataTypeNo)
            }
        }
    }

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        if(onOk == null) dismiss()

        initView()

        val builder = AlertDialog.Builder(activity as Context).apply {
            setView(b.root)
            setCancelable(true)
        }

        return builder.create().apply {
            setCanceledOnTouchOutside(true)
            window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        }

    }

    private fun initView() {


        val rgModelList = arrayOf(DataType.STORE, DataType.FLEA, DataType.ROOM, DataType.BOARD, DataType.POST)  // model List
        val list = arrayListOf<String>().apply { rgModelList.forEach { add(it.title) } }    // string List
        CustomRGbasic(mContext, list, true,
                onSelected = { listIndex ->

                    /*** 검색창 붙이기 (내 데이타) */
                    CustomSearchAll4Bid(mContext, info, myDataOnly, rgModelList[listIndex],
                            onSelected = { any ->
                                onOk?.invoke(any) ?: OnDDD.alertNullListner()
                                dialog?.dismiss()
                            })
                            .apply {
                                b.frame2.removeAllViews()
                                b.frame2.addView(this)
                            }

                }).apply { b.frame.addView(this) }


    }


}