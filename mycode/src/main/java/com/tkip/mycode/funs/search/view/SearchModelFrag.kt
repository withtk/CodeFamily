package com.tkip.mycode.funs.search.view

import android.os.Bundle
import com.tkip.mycode.R
import com.tkip.mycode.databinding.ViewFrameBinding
import com.tkip.mycode.funs.common.lllogI
import com.tkip.mycode.funs.common.lllogM
import com.tkip.mycode.funs.search.model.CustomSearchModel
import com.tkip.mycode.init.PUT_DATA_TYPE_NO
import com.tkip.mycode.init.my_bind.BindingFragment
import com.tkip.mycode.model.my_enum.DataType
import com.tkip.mycode.model.my_enum.dataType


class SearchModelFrag : BindingFragment<ViewFrameBinding>() {
    override fun getLayoutResId(): Int = R.layout.view_frame

    var customModelList: CustomSearchModel? = null
    private val dataType by lazy {
        arguments?.getInt(PUT_DATA_TYPE_NO)?.dataType() ?: DataType.POST
    }
    private lateinit var onItemClick: (Any) -> Unit
    private lateinit var onScroll: () -> Unit

    companion object {
        @JvmStatic
        fun newInstance(dataTypeNo: Int, onScroll: () -> Unit, onItemClick: (Any) -> Unit) = SearchModelFrag().apply {
            arguments = Bundle().apply {
                putInt(PUT_DATA_TYPE_NO, dataTypeNo)
            }
            this.onScroll = onScroll
            this.onItemClick = onItemClick
        }
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        initView()
    }

    private fun initView() {
        customModelList = CustomSearchModel(mContext, false, dataType, null,
                onScroll = { if(it) lllogI("sdfsd SearchModelFrag scrollUp") else lllogI("sdfsd SearchModelFrag scrollDown") },
                onItem = null)
        b.frame.addView(customModelList)
    }


    /**
     * 초기 검색 결과 가져오기 : 비어있을때만
     */
    fun initSearch() {
        lllogM("SearchModelFrag initSearch dataType $dataType ")
        customModelList?.let {
            if (it.esLastIndex == 0) {
                it.getEsDataList(0, null)
            }
        }
    }


}
