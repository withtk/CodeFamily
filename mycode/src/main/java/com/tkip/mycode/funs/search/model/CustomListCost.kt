package com.tkip.mycode.funs.search.model

import android.content.Context
import android.view.LayoutInflater
import android.widget.LinearLayout
import androidx.databinding.DataBindingUtil
import com.tkip.mycode.R
import com.tkip.mycode.databinding.CustomModelListBinding
import com.tkip.mycode.init.base.getInt
import com.tkip.mycode.model.cost.Cost
import com.tkip.mycode.model.cost.CostAdapter
import com.tkip.mycode.model.my_enum.CostType
import com.tkip.mycode.model.peer.myuid
import com.tkip.mycode.util.lib.retrofit2.ESget
import com.tkip.mycode.util.lib.retrofit2.ESquery
import com.tkip.mycode.util.tools.recycler.OnRv

/**
 * 단순 cost refresh List
 */
class CustomListCost : LinearLayout {
    private lateinit var b: CustomModelListBinding
    private var uid: String = myuid()
    private var costType1: CostType = CostType.PAYMENT   // 기준이 되는 costType 꼭 필요.
    private var costType2: CostType? = null

    private var costAdapter: CostAdapter? = null
    private val costItems by lazy { arrayListOf<Cost>() }

    private val esSize by lazy { R.integer.es_size_cost.getInt() }
    private var esLastIndex = 0

    constructor(context: Context) : super(context)
    constructor(context: Context, uid: String?, costType1: CostType, costType2: CostType?, onItem: ((Cost) -> Unit)?) : super(context) {
        b = DataBindingUtil.inflate(LayoutInflater.from(context), R.layout.custom_model_list, this, true)
        uid?.let { this.uid = it }
        this.costType1 = costType1
        this.costType2 = costType2

        /** set Adapter */
        costAdapter = CostAdapter(costItems, onItem)
        b.inRefresh.rv.adapter = costAdapter

        b.inRefresh.refresh.setOnRefreshListener { getEsDataList(0, null) }
        b.inRefresh.refresh.setOnLoadMoreListener { getEsDataList(esLastIndex, null) }
//        b.refresh.setEnableRefresh(false)   // 상단 refresh는 불능.
//        b.refresh.setEnableLoadMore(false)  // 하단 loadMore 불능.


    }

    /** * 가져오기  */
    fun getEsDataList(from: Int, uid2: String?) {
        uid2?.let { this.uid = it }

        when (costType1) {
            CostType.ACT ->
                ESget.getActList(ESquery.costAll(esSize, from, this.uid),
                        success = {
                            esLastIndex = OnRv.setRvRefresh(it, from, costItems, b.inRefresh, true)
                            costAdapter?.notifyDataSetChanged()
                        }, fail = {})

            CostType.PAYMENT, CostType.TAKE, CostType.CONVERT, CostType.SEND -> {

                val query = if (costType2 == null) ESquery.costTypeList(esSize, from, this.uid, costType1)
                else ESquery.costAllNoAct(esSize, from, this.uid, costType1, costType2!!)
                ESget.getCostList(query,
                        success = {
                            esLastIndex = OnRv.setRvRefresh(it, from, costItems, b.inRefresh, true)
                            costAdapter?.notifyDataSetChanged()
                        }, fail = { })
            }
        }


    }


}