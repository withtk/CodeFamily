package com.tkip.mycode.funs.common

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.net.Uri
import android.widget.LinearLayout
import androidx.core.app.ActivityCompat
import com.tkip.mycode.R
import com.tkip.mycode.funs.custom.menu.ViewMenuLine
import com.tkip.mycode.init.base.gte
import com.tkip.mycode.init.listenerRegiPeer
import com.tkip.mycode.init.listenerRegiSystem
import com.tkip.mycode.init.part.CodeApp
import com.tkip.mycode.util.tools.fire.realOffline

class OnBasic {

    companion object {

        /**
         * 최신버전 체크 및 강제업데이트
         */
        fun isLatestVersion(): Boolean {
            //            val myVer = BuildConfig.VERSION_CODE
            val myVer = CodeApp.versionCode
            val latestVer = R.string.remote_latest_version.remoteInt()
            lllogI("  myVer $myVer  :   latestVer $latestVer")

            return myVer.gte(latestVer)
        }


        /**
         * web으로 이동.
         */
        fun gotoGoogleStore(activity: Activity) {
            val intent = Intent(Intent.ACTION_VIEW)
            val appUrl = R.string.remote_app_url.remoteStr()
            intent.data = Uri.parse(appUrl)
//        intent.data = Uri.parse(FirebaseRemoteConfig.getInstance().getString("app_url"))
            activity.startActivity(intent)
            activity.finish()
        }


        fun setLibraryList(context: Context, line: LinearLayout) {
            line.addView(ViewMenuLine(context, null, "library1", "library2 desc", onClick = {}))
            line.addView(ViewMenuLine(context, null, "library1", "library2 desc", onClick = {}))
            line.addView(ViewMenuLine(context, null, "library1", "library2 desc", onClick = {}))
            line.addView(ViewMenuLine(context, null, "library1", "library2 desc", onClick = {}))
            line.addView(ViewMenuLine(context, null, "library1", "library2 desc", onClick = {}))
        }


        fun finishAllActivity() {
            lllogM("MainActivity removeBasicListenerSSSSSS finishAllActivity curActivity=> ${CodeApp.curActivity?.javaClass?.simpleName}"  )
            removeBasicListener()
//            activity.finishAffinity()
            CodeApp.curActivity?.finishAffinity()   // 현재 activity를 이용하여 모두 종료.
        }

        fun removeBasicListener() {
            lllogM("MainActivity removeBasicListenerSSSSSS before removeAllListener start ####\n listenerRegiPeer:$listenerRegiPeer\n  listenerRegiSystem:$listenerRegiSystem")
            listenerRegiPeer?.remove()
            listenerRegiSystem?.remove()
            listenerRegiPeer = null
            listenerRegiSystem = null
            lllogM("MainActivity removeBasicListenerSSSSSS end ####\n listenerRegiPeer:$listenerRegiPeer\n  listenerRegiSystem:$listenerRegiSystem")
        }
    }

}