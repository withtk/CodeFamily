package com.tkip.mycode.funs.common

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.view.View
import com.tkip.mycode.R
import com.tkip.mycode.util.lib.TransitionHelper


/*************************************************************************************/
/********************  한개짜리 transition 일때 ***************************************/
/*************************************************************************************/
fun View.arrTranToolbar() = arrayOf(this.tranPair(R.string.tran_toolbar))

fun View.arrTranLinear() = arrayOf(this.tranPair(R.string.tran_linear))






/********************************************************************************************
 **************** Activity Open *************************************************************
 ********************************************************************************************/
//todo :  나중에 여기로 통과시킬 것.
fun startActi(context: Context, intent: Intent, arrPair: Array<androidx.core.util.Pair<View, String>>?) {
    if (arrPair == null) context.startActivity(intent)
    else context.startActivity(intent, TransitionHelper.createTran(context, false, arrPair).toBundle())
}

fun startActi(activity: Activity, intent: Intent, arrPair: Array<androidx.core.util.Pair<View, String>>?) {
    if (arrPair == null) activity.startActivity(intent)
    else activity.startActivity(intent, TransitionHelper.createTran(activity, false, arrPair).toBundle())
}
