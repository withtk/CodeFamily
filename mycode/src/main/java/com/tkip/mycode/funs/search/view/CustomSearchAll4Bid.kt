package com.tkip.mycode.funs.search.view

import android.content.Context
import android.view.LayoutInflater
import android.widget.LinearLayout
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.LinearLayoutManager
import com.tkip.mycode.R
import com.tkip.mycode.databinding.CustomSearchAll4bidBinding
import com.tkip.mycode.funs.common.*
import com.tkip.mycode.init.CH_COMMENT
import com.tkip.mycode.init.CH_TITLE
import com.tkip.mycode.init.RV_HORI
import com.tkip.mycode.init.RV_VERT
import com.tkip.mycode.init.base.getInt
import com.tkip.mycode.init.base.getStr
import com.tkip.mycode.model.board.Board
import com.tkip.mycode.model.flea.Flea
import com.tkip.mycode.model.my_enum.DataType
import com.tkip.mycode.model.my_enum.Status
import com.tkip.mycode.model.my_enum.gteStatus
import com.tkip.mycode.model.peer.OnPeer
import com.tkip.mycode.model.peer.isAdmin
import com.tkip.mycode.model.peer.myuid
import com.tkip.mycode.model.post.Post
import com.tkip.mycode.model.room.Room
import com.tkip.mycode.model.store.Store
import com.tkip.mycode.nav.board.tab_first.BoardAdapter
import com.tkip.mycode.nav.flea.FleaAdapter
import com.tkip.mycode.nav.post.PostAdapter
import com.tkip.mycode.nav.room.RoomAdapter
import com.tkip.mycode.nav.store.StoreAdapter
import com.tkip.mycode.util.lib.SnapHelper
import com.tkip.mycode.util.lib.retrofit2.ESget
import com.tkip.mycode.util.lib.retrofit2.ESquery
import com.tkip.mycode.util.tools.anim.visi
import com.tkip.mycode.util.tools.etc.MyKeyboard
import com.tkip.mycode.util.tools.recycler.OnRv

/**
 * 모든 데이타를 받는 RV : store, flea, room, board, post
 */
class CustomSearchAll4Bid : LinearLayout {
    private lateinit var b: CustomSearchAll4bidBinding
    private var myDataOnly = false
    private lateinit var onSelected: (Any) -> Unit
    private lateinit var dataType: DataType
//    var b: ViewRefreshRvBinding? = null

    private val storeItems by lazy { arrayListOf<Store>() }
    private val boardItems by lazy { arrayListOf<Board>() }
    private val postItems by lazy { arrayListOf<Post>() }
    private val fleaItems by lazy { arrayListOf<Flea>() }
    private val roomItems by lazy { arrayListOf<Room>() }
    private var storeAdapter: StoreAdapter? = null
    private var postAdapter: PostAdapter? = null
    private var fleaAdapter: FleaAdapter? = null
    private var roomAdapter: RoomAdapter? = null
    private var boardAdapter: BoardAdapter? = null

    private val esSize by lazy { R.integer.es_size_search.getInt() }
    private var esLastIndex = 0


    constructor(context: Context) : super(context)
    constructor(context: Context, info: String?, myDataOnly: Boolean, dataType: DataType, onSelected: (Any) -> Unit) : super(context) {
        b = DataBindingUtil.inflate(LayoutInflater.from(context), R.layout.custom_search_all4bid, this, true)
        b.info = info
        this.myDataOnly = if (isAdmin()) false else myDataOnly    // 어드민은 선택가능.
        this.onSelected = onSelected
        this.dataType = dataType

        initView()


        /** * 실시간 태그 검색 */
        b.etSearch.textListener(after = { s ->

            OnVisible.toggleSimple(!s.isNullOrBlank(), b.btnClearEt)   // x버튼 토글
            b.etSearch.keyword()?.let { keyword ->
                if (keyword.length > 1) getNewList()    // 두 글자부터 순간검색
            } ?: let {
                // keyword 널이면 list 지워.
                clearRV(dataType)
            }

        }, before = { _, _, _, _ -> }, on = { s, _, _, c -> })

        b.btnClearEt.onClick { b.etSearch.setText("") }


        SnapHelper().attachToRecyclerView(b.rv)   // 스냅헬퍼


//        b.refresh.setEnableRefresh(false)   //상단 refresh는 불능
//        b.refresh.setEnableLoadMore(false)  // 하단 loadMore 불능.
//        b.refresh.setOnLoadMoreListener {
//            getEsDataList(esLastIndex)
//            onLoadMore?.invoke(b.tagView, b.etSearch.keyword())
//        }

    }

    private fun initView() {

        b.sw.isChecked = myDataOnly
        b.sw.setOnCheckedChangeListener { _, isOn ->
            this.myDataOnly = isOn
            getNewList()
        }

        /** * adapter 세팅 */
        setupRV()

        /** * 실시간 검색 */
        getNewList()
    }

    /** * adapter 세팅 */
    private fun setupRV() {
        when (dataType) {
            DataType.STORE -> {
                if (storeAdapter == null) storeAdapter = StoreAdapter(storeItems, RV_HORI, onItemClick = { afterGetData(it) })
                b.rv.adapter = storeAdapter
            }
            DataType.BOARD -> {
                if (boardAdapter == null) boardAdapter = BoardAdapter(boardItems, RV_HORI, onItemClick = { afterGetData(it) })
                b.rv.adapter = boardAdapter
            }
            DataType.ROOM -> {
                if (roomAdapter == null) roomAdapter = RoomAdapter(roomItems, RV_HORI, onItemClick = { afterGetData(it) })
                b.rv.adapter = roomAdapter
            }
            DataType.FLEA -> {
                if (fleaAdapter == null) fleaAdapter = FleaAdapter(fleaItems, RV_HORI, onItemClick = { afterGetData(it) })
                b.rv.adapter = fleaAdapter
            }
            DataType.POST -> {
                if (postAdapter == null) postAdapter = PostAdapter(postItems, RV_VERT, onItemClick = { afterGetData(it) })
                b.rv.layoutManager = LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false)
                b.rv.adapter = postAdapter
            }
            else -> {
            }
        }

    }

    /** * 가져오기  */
    private fun getNewList() {
        b.avLoading.visi()
        when (dataType) {
            DataType.STORE -> ESget.getStoreList(getQuery(arrayOf(CH_TITLE)),
                    success = {
                        OnRv.setSearchResult(it, esLastIndex, storeItems, b.tvNoResult, R.string.info_no_search_result_store.getStr(), b.avLoading, null)
                        storeAdapter?.notifyDataSetChanged()
                    }, fail = { })

            /**
             * Board
             *    case1 : 광고할 개체 검색.
             *         -
             *    case2 : 광고위치 검색.
             *         - no비공개
             *
             * 안내 : 비공개되면 환불됨.
             */
            DataType.BOARD -> {
                val query = if (myDataOnly) ESquery.boardMatchUID(esSize, esLastIndex, OnPeer.myUid(), arrayOf(CH_TITLE, CH_COMMENT), keyword(), Status.NORMAL)
                else ESquery.boardMatchKeywordTotalPoint(esSize, esLastIndex, arrayOf(CH_TITLE, CH_COMMENT), keyword(), Status.NORMAL)
                ESget.getBoardList(query,
                        success = {
                            OnRv.setSearchResult(it, esLastIndex, boardItems, b.tvNoResult, R.string.info_no_search_result_board.getStr(), b.avLoading, null)
                            boardAdapter?.notifyDataSetChanged()
                        }, fail = { })
            }

            DataType.FLEA -> ESget.getFleaList(getQuery(arrayOf(CH_TITLE, CH_COMMENT)),
                    success = {
                        OnRv.setSearchResult(it, esLastIndex, fleaItems, b.tvNoResult, R.string.info_no_search_result_flea.getStr(), b.avLoading, null)
                        fleaAdapter?.notifyDataSetChanged()
                    }, fail = { })

            DataType.ROOM -> {
                ESget.getRoomList(getQuery(arrayOf(CH_TITLE, CH_COMMENT)),
                        success = {
                            OnRv.setSearchResult(it, esLastIndex, roomItems, b.tvNoResult, R.string.info_no_search_result_room.getStr(), b.avLoading, null)
                            roomAdapter?.notifyDataSetChanged()
                        }, fail = { })
            }

            DataType.POST -> ESget.getPostList(getQuery(arrayOf(CH_TITLE, CH_COMMENT)), // todo : 나중에 comment로 수정할 것.
                    success = {
                        OnRv.setSearchResult(it, esLastIndex, postItems, b.tvNoResult, R.string.info_no_search_result_post.getStr(), b.avLoading, null)
                        postAdapter?.notifyDataSetChanged()
                    }, fail = { })
            else -> {
            }
        }
    }

    private fun afterGetData(any: Any) {
        lllogI("onItemClick afterGetData $any")
        MyKeyboard.hide(b.etSearch)
        if (any.gteStatus(Status.NORMAL, true)) onSelected(any)
    }


    /**
     * hasSw : 내 데이타만 검색.
     * child : keyword가 검색될 부분.
     */
    private fun getQuery(childArr: Array<String>): String {
        return if (myDataOnly) ESquery.matchUIDwithKeyword(esSize, esLastIndex, myuid(), childArr, keyword(), Status.NORMAL)
        else ESquery.matchKeyword(esSize, esLastIndex, childArr, keyword(), Status.NORMAL)
    }

    private fun keyword() = b.etSearch.keyword() ?: ""

    private fun clearRV(dataType: DataType) {
        when (dataType) {

            DataType.STORE -> {
                storeItems.clear()
                storeAdapter?.notifyDataSetChanged()
            }

            DataType.BOARD -> {
                boardItems.clear()
                boardAdapter?.notifyDataSetChanged()
            }

            DataType.ROOM -> {
                roomItems.clear()
                roomAdapter?.notifyDataSetChanged()
            }

            DataType.FLEA -> {
                fleaItems.clear()
                fleaAdapter?.notifyDataSetChanged()
            }

            DataType.POST -> {
                postItems.clear()
                postAdapter?.notifyDataSetChanged()
            }

            else -> {
            }
        }


    }

}