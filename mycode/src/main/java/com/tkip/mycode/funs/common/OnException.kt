package com.tkip.mycode.funs.common

import com.tkip.mycode.R
import com.tkip.mycode.dialog.toast.toastWarning

class OnException {

    companion object {

        fun loginChange(memo: String, e: Exception) {
            lllogD("loginChange $memo===============================", e)
        }

        fun dbfailed(memo: String, e: Exception) {
            lllogD("dbfailed $memo===============================", e)
//            val msg = String.format(R.string.db_failure2.getStr(),memo)
//            toastWarning(msg)
            toastWarning(R.string.db_failure)
        }

        fun dbNoData(memo: String) {
            lllogD("dbNoData $memo===============================")
            toastWarning(R.string.db_no_data)
        }

        fun dbSyncFailed(memo: String, e: Exception) {
            lllogD("dbSyncFailed $memo===============================", e)
//            toastWarning(R.string.db_sync_no_data)
        }

        fun logicError(memo: String, e: Exception?) {
            lllogD("logicError $memo===============================", e)
            toastWarning(R.string.error_android)
        }

        fun forceException(memo: String) {
            toastWarning(R.string.db_force_exception)
            lllogE("forceException $memo===============================")
            throw  Exception("forceException : $memo===============================")
//            Crashlytics.logException(e)
        }

        /**
         * async photo 변환 오류
         */
        fun forceExceptionAsyncResize(memo: String) {
            toastWarning(R.string.error_photo_async_upload)
            lllogE("forceException $memo===============================")
            throw  Exception("forceException : $memo===============================")
        }


        /**
         * map
         */
        fun failMapReady(memo: String) {
            lllogD("failMapReady $memo===============================")
            toastWarning(R.string.error_map_not_ready)
        }

        fun failMapData(memo: String) {
            lllogD("failMapData $memo===============================")
            toastWarning(R.string.error_map_no_data)
        }

        fun failMapFetchPhoto(memo: String) {
            lllogD("failMapFetchPhoto $memo===============================")
            toastWarning(R.string.error_map_no_photo)
        }

        fun failMapLikelyHood(memo: String) {
            lllogD("failMapLikelyHood $memo===============================")
            toastWarning(R.string.error_map_no_likelyhood)
        }

        fun failMapStyle(memo: String) {
            lllogD("failMapStyle $memo===============================")
            toastWarning(R.string.error_map_no_style)
        }


        /**
         * foto upload
         */
        fun uploadFail(memo: String) {
            lllogD("uploadFail $memo===============================")
            toastWarning(R.string.error_storage_upload)
        }




    }

}