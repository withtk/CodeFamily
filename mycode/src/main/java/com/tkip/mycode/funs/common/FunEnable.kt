package com.tkip.mycode.funs.common

import android.view.View




fun enableAllView(vararg views: View) {
    views.forEach {
        it.isEnabled = true
    }
}