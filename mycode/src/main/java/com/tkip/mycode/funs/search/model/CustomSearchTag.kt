package com.tkip.mycode.funs.search.model

import android.content.Context
import android.view.LayoutInflater
import android.widget.LinearLayout
import androidx.databinding.DataBindingUtil
import com.tkip.mycode.R
import com.tkip.mycode.databinding.CustomModelListBinding
import com.tkip.mycode.init.base.getInt
import com.tkip.mycode.model.my_enum.Status
import com.tkip.mycode.model.mytag.MyTag
import com.tkip.mycode.model.mytag.MyTagAdapter
import com.tkip.mycode.model.mytag.TagTYPE
import com.tkip.mycode.util.lib.retrofit2.ESget
import com.tkip.mycode.util.lib.retrofit2.ESquery
import com.tkip.mycode.util.tools.recycler.OnRv

/**
 * custom_model_list.xml 공유중.
 */
class CustomSearchTag : LinearLayout {
    private lateinit var b: CustomModelListBinding
    private var keyword = ""
    private var manage = false
    var tagTYPE = TagTYPE.ALL
    private val myTagItems by lazy { arrayListOf<MyTag>() }
    private var myTagAdapter: MyTagAdapter? = null

    private val esSize by lazy { R.integer.es_size_tag.getInt() }
    private var esLastIndex = 0

    constructor(context: Context) : super(context)
    constructor(context: Context, manage: Boolean, tagTYPE: TagTYPE) : super(context) {
        b = DataBindingUtil.inflate(LayoutInflater.from(context), R.layout.custom_model_list, this, true)
        this.tagTYPE = tagTYPE
        this.manage = manage

        /** set Adapter */
        myTagAdapter = MyTagAdapter(myTagItems, MyTagAdapter.rvManage, null)
        b.inRefresh.rv.adapter = myTagAdapter

        b.inRefresh.refresh.setOnRefreshListener { getEsDataList(0, keyword) }
        b.inRefresh.refresh.setOnLoadMoreListener { getEsDataList(esLastIndex, keyword) }
//        b.refresh.setEnableRefresh(false)   //상단 refresh는 불능
//        b.refresh.setEnableLoadMore(false)  // 하단 loadMore 불능.

//        getEsDataList(0, searchWordList)   // 시작하자마자 일단 검색.
    }

    /** * 가져오기  */
    fun getEsDataList(from: Int, keyword1: String?) {
        this.keyword = keyword1 ?: ""
//        this.tagTYPE = tagTYPE

        val queryTypes = if (manage) tagTYPE.no.toString() else tagTYPE.queryTypes
        if (tagTYPE == TagTYPE.ALL) ESget.getMyTagList(ESquery.tagSearchAllforAdmin(esSize, from, keyword, Status.DENIED), success = { afterGet(it, from) }, fail = { })
        else ESget.getMyTagList(ESquery.mytagListManage(esSize, from, queryTypes, keyword, Status.NORMAL), success = { afterGet(it, from) }, fail = { })
    }

    private fun afterGet(it: ArrayList<MyTag>?, from: Int) {
        esLastIndex = OnRv.setRvRefresh(it, from, myTagItems, b.inRefresh, true)
        myTagAdapter?.notifyDataSetChanged()
    }

}