package com.tkip.mycode.funs.custom.text

import android.content.Context
import android.view.LayoutInflater
import android.widget.LinearLayout
import androidx.databinding.DataBindingUtil
import com.tkip.mycode.R
import com.tkip.mycode.databinding.ViewTvStatusBinding

/**
 * status Normal아닐때 보여줌.
 */
class ViewTvStatus : LinearLayout {
    lateinit var b: ViewTvStatusBinding

    constructor(context: Context) : super(context)
    constructor(context: Context,   msg: String) : super(context) {
        b = DataBindingUtil.inflate(LayoutInflater.from(context), R.layout.view_tv_status, this, true)
        b.msg = msg

    }


}