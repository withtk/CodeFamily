package com.tkip.mycode.funs.custom.menu

import android.content.Context
import android.view.LayoutInflater
import android.widget.LinearLayout
import androidx.databinding.DataBindingUtil
import com.tkip.mycode.R
import com.tkip.mycode.databinding.ViewMenuBoxBinding


/**
 * Menu 그룹
 */
class ViewMenuBox : LinearLayout {
      lateinit var b: ViewMenuBoxBinding

    constructor(context: Context) : super(context)
    constructor(context: Context, title: String? ) : super(context) {
        b = DataBindingUtil.inflate(LayoutInflater.from(context), R.layout.view_menu_box, this, true)
        b.title = title
    }


}