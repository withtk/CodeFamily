package com.tkip.mycode.ad.bid


import android.content.Context
import android.view.LayoutInflater
import android.widget.LinearLayout
import androidx.databinding.DataBindingUtil
import com.tkip.mycode.R
import com.tkip.mycode.databinding.SubBidBillsBinding
import com.tkip.mycode.funs.common.OnVisible
import com.tkip.mycode.funs.common.lllogI
import com.tkip.mycode.init.base.getStringAmountMultiplyTerm
import com.tkip.mycode.init.base.multiply
import com.tkip.mycode.model.bid.Bid
import com.tkip.mycode.model.cost.credit.OnCredit
import com.tkip.mycode.model.my_enum.PayType
import com.tkip.mycode.model.peer.isGte10000Bonus
import com.tkip.mycode.model.peer.mypeer
import com.tkip.mycode.model.ticket.OnTicket
import com.tkip.mycode.util.tools.anim.gone
import com.tkip.mycode.util.tools.anim.visi
import com.tkip.mycode.util.tools.etc.OnMyClick


/**
 * Bid 기입 : paidBonusCredit, paidCredit, amountBack
 */
class SubBidBills : LinearLayout {
    lateinit var b: SubBidBillsBinding
    lateinit var bid: Bid
    var sum: Int = 0 //결제할 크레딧
    var payWithBonus = false
    var selectPayType : PayType? = null

    constructor(context: Context) : super(context)
    constructor(context: Context, bid: Bid) : super(context) {
        b = DataBindingUtil.inflate(LayoutInflater.from(context), R.layout.sub_bid_bills, this, true)

        b.btnBonus.setOnClickListener { selectPayType(PayType.BONUS_CREDIT) }
        b.btnCredit.setOnClickListener { selectPayType(PayType.CREDIT) }
        b.btnGotoCredit.setOnClickListener {
            OnMyClick.setDisableAWhileBTN(it)
            OnTicket.gotoPurchaseActivity(context)
        }

        setReceipt(bid)
    }


    fun setReceipt(bid: Bid) {
        lllogI("CustomBidBills setReceit $bid")
        b.line.visi()
        this.bid = bid
        b.peer = mypeer()

//        b.line.visi()

        sum = (bid.amount ?: 0).multiply(bid.term ?: 0)   // 전체 결제금액

        b.tvDetail.text = getStringAmountMultiplyTerm(bid.amount ?: 0, bid.term ?: 0)   // 결제 상세내역
        b.total = sum
        lllogI("CustomBidBills setReceipt sum $sum")


        /** 보너스 */
        if (PayType.BONUS_CREDIT.isEnough(sum)) showSelectBtn(true)
        OnVisible.toggleSimple(!isGte10000Bonus(), b.tvBonusInfo)    // 10000이하 메세지 표시
        OnVisible.toggleSimple(isGte10000Bonus() && PayType.BONUS_CREDIT.isNotEnough(sum), b.tvBonusLack)  // 부족메세지 표시
        OnVisible.toggleSimple(PayType.BONUS_CREDIT.isEnough(sum), b.tvBonusPay)  // 결제금액 표시


        /** 크레딧 */
        OnVisible.toggleSimple(PayType.CREDIT.isNotEnough(sum), b.tvCreditLack)  // 부족메세지 표시
//        OnVisible.toggleSimple(PayType.CREDIT.isEnough(sum) && PayType.BONUS_CREDIT.isEnough(sum), b.lineBtn)  // lineBtn 표시
        OnVisible.toggleSimple(PayType.CREDIT.isEnough(sum), b.tvCreditPay)  // 결제금액 표시

        b.btnGotoCredit.gone()

        /*** 결제 가능여부 셋팅 */
        OnCredit.checkAvailPayment(sum,
                onNotAvail = {
                    /**  모두 결제 불가능    */
                    showSelectBtn(false)
                    b.btnGotoCredit.visi()
//                    b.tvSelect.text = R.string.title_goto_purchase_credit.getStr()
                },
                onCreditOnly = { selectPayType(PayType.CREDIT) },
                onBonusOnly = { selectPayType(PayType.BONUS_CREDIT) },
                onAvail = {
                    /**  모두 결제가능    */
                    showSelectBtn(true)
//                    b.tvSelect.text = R.string.title_select_pay.getStr()
                })

    }


    private fun showSelectBtn(show: Boolean) {
        OnVisible.toggleSimple(show, b.btnBonus, b.btnCredit, b.tvSelect)
    }

    private fun selectPayType(payType: PayType) {
//        payWithBonus = isBonus
        selectPayType = payType

        OnVisible.toggleSimple((payType == PayType.BONUS_CREDIT), b.tvBonusPay)
        OnVisible.toggleSimple(payType != PayType.BONUS_CREDIT, b.tvCreditPay)

//        when (payType) {
//            PayType.BONUS_CREDIT -> {
//                b.tvBonusPay.visi()
//            }
//            PayType.CREDIT -> b.tvCreditPay.visi()
//            else -> {}
//        }

        bid.payTypeNo = payType.no
        showSelectBtn(false)
    }


    /**
     * 결제가능 : false
     * 불가능 : true
     */
//    private fun PayType.notEnoughAmount(sum:Int): Boolean {
//        val credit = if(this == PayType.CREDIT) mycredit() else mybonusCredit()
//        return if (credit.gte(sum)) false
//        else {
//            toastAlert(R.string.error_not_enough_credit)
//            true
//        }
//    }

}