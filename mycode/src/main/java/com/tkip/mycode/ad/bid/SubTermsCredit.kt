package com.tkip.mycode.ad.bid

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.widget.LinearLayout
import androidx.databinding.DataBindingUtil
import com.tkip.mycode.R
import com.tkip.mycode.ad.OnBid
import com.tkip.mycode.ad.getAdtype
import com.tkip.mycode.databinding.SubTermsCreditBinding
import com.tkip.mycode.dialog.alert.MyAlert
import com.tkip.mycode.funs.common.lllogI
import com.tkip.mycode.funs.common.onClick
import com.tkip.mycode.init.base.getStr
import com.tkip.mycode.model.bid.Bid
import com.tkip.mycode.model.my_enum.PickerFactory
import com.tkip.mycode.util.tools.anim.gone
import com.tkip.mycode.util.tools.anim.showOverUp
import com.tkip.mycode.util.tools.etc.OnMyClick
import java.util.*


class SubTermsCredit : LinearLayout {
    private lateinit var b: SubTermsCreditBinding
    private lateinit var bid: Bid
    private   var selectedAmount = "0"

    constructor(context: Context) : super(context)
    constructor(context: Context, bid: Bid, onOpenRange: () -> Unit, onOpenPicker: () -> Unit, onSelected: (selList: ArrayList<Int>?) -> Unit, onCompleted: (amount: Int) -> Unit) : super(context) {
        b = DataBindingUtil.inflate(LayoutInflater.from(context), R.layout.sub_terms_credit, this, true)
        b.bid = bid
        this.bid = bid

        /**         * 현황 오픈         */
        b.btnStatus.onClick {
            OnBid.gotoBidStateActivity(context, bid, BidAdapter.rvBidState)
//            MyAlert.showCalendarAdUnitStatus(context, adUnit.adTypeNo!!.getAdtype(), adUnit.boardKey, adUnit.boardName)
        }

        /**         * 달력 기간 선택 오픈         */
        b.btnCal.onClick {
            MyAlert.showCalendarRange(context,
                    onResult = { selList ->
                        lllogI("SubTermsCredit onResult selList ${selList?.size}")
//                        b.tvTerm.setTerm(iSet?.size ?: 0)
                        onSelected(selList)
                        b.tvInfo.text = R.string.title_ad_term_manual_amount.getStr()
                        b.tvInfo.showOverUp(false)
                    })
            onOpenRange()
        }


        /**         * amount 설정하기         */
        b.btnCredit.onClick { gotoNumberPicker(it, onOpenPicker, onCompleted) }
//        b.tvCredit.setOnClickListener { gotoNumberPicker(it, onOpenPicker, onCompleted) }
//        b.tvAmount.setOnClickListener { gotoNumberPicker(it, onOpenPicker, onCompleted) }

    }

    private fun gotoNumberPicker(v: View, onOpenPicker: () -> Unit, onCompleted: (Int) -> Unit) {
        OnMyClick.setDisableAWhileBTN(v)
        onOpenPicker()
        PickerFactory.BIDDING.omitNumber = bid.adTypeNo!!.getAdtype().minCredit   // 최소 단위 수정.
        lllogI("gotoNumberPicker adUnit $bid")
        lllogI("gotoNumberPicker PickerFactory ${PickerFactory.BIDDING.omitNumber}")
        MyAlert.showNumberPicker(context, PickerFactory.BIDDING, selectedAmount,
                onOk = { amount, commaStr ->
                    if (amount < 1) return@showNumberPicker
                    lllogI("tvAmount showNumberPicker $commaStr")
                    selectedAmount = commaStr
                    onCompleted(amount)
                    b.tvInfo.gone()
                })
    }

    /*************************************************************************************************************
     ***************************************** 기간 선택 ********************************************************/

//    /**     * 기간 선택 로직.     */
//    private fun executeTerm(clickDate: Long, m: Int, d: Int, adUnit: AdUnit) {
//        startDate?.let { start ->
//            if (clickDate < start) setStart(clickDate, m, d) // 시작일보다 전일 선택(즉, 다시 시작일)
//            else setEnd(m, d, adUnit, start, clickDate)   // 종료일 or 동일한 시작일 클릭.
//        } ?: setStart(clickDate, m, d)          // 시작일 클릭.
//    }

//    /**     * 안내멘트 바꾸기.     */
//    private fun changeTvInfo(strId: Int) {
//        b.tvInfo.text = strId.getStr()
//        b.tvInfo.showSlideUp(true)
//    }

//    private fun setStart(clickDate: Long, m: Int, d: Int) {
//        b.tvStartDate.monthDate(m, d)
//        startDate = clickDate
//        changeTvInfo(R.string.title_ad_term_manual_end)
//        AnimUtil.showNormal(b.tvStart, b.tvStartDate)
//    }
//
//    private fun setEnd(m: Int, d: Int, adUnit: AdUnit, start: Long, clickDate: Long) {
//        b.tvEndDate.monthDate(m, d)
//        adUnit.term = start.getDiffDay(clickDate)
//        changeTvInfo(R.string.title_ad_term_manual_amount)
//        AnimUtil.showNormal(b.tvEnd, b.tvEndDate, b.lineCredit)
//    }


}