package com.tkip.mycode.ad.bid

import android.content.Context
import android.view.LayoutInflater
import android.widget.LinearLayout
import androidx.databinding.DataBindingUtil
import com.tkip.mycode.R
import com.tkip.mycode.ad.AdType
import com.tkip.mycode.ad.HIT_DATE_DEFAULT_BID
import com.tkip.mycode.ad.OnBid
import com.tkip.mycode.databinding.CustomBidStateBinding
import com.tkip.mycode.dialog.OnDDD
import com.tkip.mycode.dialog.alert.MyAlert
import com.tkip.mycode.funs.common.lllogI
import com.tkip.mycode.funs.common.onClick
import com.tkip.mycode.init.base.getInt
import com.tkip.mycode.init.base.getStr
import com.tkip.mycode.model.bid.Bid
import com.tkip.mycode.model.my_enum.DataType
import com.tkip.mycode.model.my_enum.STATUS_LIST_BID
import com.tkip.mycode.model.my_enum.Status
import com.tkip.mycode.model.my_enum.createStringlist
import com.tkip.mycode.model.peer.myuid
import com.tkip.mycode.util.lib.SnapHelper
import com.tkip.mycode.util.lib.calendar.CustomCalendarNormal
import com.tkip.mycode.util.lib.retrofit2.ESget
import com.tkip.mycode.util.lib.retrofit2.ESquery
import com.tkip.mycode.util.tools.date.getIntDate8TodayPH
import com.tkip.mycode.util.tools.recycler.OnRv

/**
 * 광고 현황
 */
class CustomBidState : LinearLayout {
    private lateinit var b: CustomBidStateBinding
    private lateinit var bidAdapter: BidAdapter
    private var rvType: Int = BidAdapter.rvBidState
    private lateinit var adType: AdType
    //    private lateinit var customCal: CustomCalendarNormal
    private var boardKey: String? = null
    private var boardName: String? = null
    private var status: Status = Status.BID_FAILED
    private val items = arrayListOf<Bid>()

    var date8 = getIntDate8TodayPH()  // 유저가 선택한 date : 초기값은 오늘.
    private val esSize by lazy { R.integer.es_size_bid.getInt() }

    var esLastIndex = 0

    constructor(context: Context) : super(context)
    constructor(context: Context, info: String?, rvType: Int, adType: AdType, boardKey: String?, boardName: String?) : super(context) {
        lllogI("CustomBidState rvType $rvType, adType $adType, boardKey $boardKey, boardName $boardName")
        b = DataBindingUtil.inflate(LayoutInflater.from(context), R.layout.custom_bid_state, this, true)
        this.rvType = rvType
        this.adType = adType
        this.boardKey = boardKey
        this.boardName = boardName
        b.info = info
        b.rvType = this.rvType
        b.adType = this.adType
        b.boardName = this.boardName


        /** 상단 달력 붙이기 */
        if (rvType == BidAdapter.rvBidState || rvType == BidAdapter.rvAllBid) attachCalendar(context)

        b.btnAdtype.onClick {
            /*** adType 선택 View */
            MyAlert.showSelectAdType(context, R.string.title_ad_type_select.getStr(), false, DataType.ALL) { adType1, board1 ->
                lllogI("HomeFrag showSelectAdType adType $adType1   board $board1")
                this.adType = adType1
                this.boardKey = board1?.key
                this.boardName = board1?.title
                b.adType = this.adType
                b.boardName = this.boardName

                esLastIndex = 0
                getEsDataList(0)   // 처음 검색
            }
        }

        b.btnDate.onClick {
            date8 = HIT_DATE_DEFAULT_BID
            esLastIndex = 0
            getEsDataList(0)   // 처음 검색
        }

        b.btnStatus.onClick {
            MyAlert.showSelectString(context, null, STATUS_LIST_BID.createStringlist()) { p ->
                this.status = STATUS_LIST_BID[p]
                esLastIndex = 0
                getEsDataList(0)   // 처음 검색
            }
        }

        b.btnAuction.onClick {
            OnDDD.confirm(context, R.string.ask_handle_auction, ok = { OnBid.handleAuction(items, adType.no) }, cancel = {})
        }

        b.btnRecover.onClick {
            OnDDD.confirm(context, R.string.ask_handle_auction_recover, ok = { OnBid.recoverAuction(items) }, cancel = {})
        }

        b.inRefresh.refresh.setOnRefreshListener {
            esLastIndex = 0
            getEsDataList(0)    // 처음 검색
        }
        b.inRefresh.refresh.setOnLoadMoreListener {
            getEsDataList(esLastIndex)    // 이어서 검색
        }
        b.inRefresh.lastMsg = R.string.info_last_msg_bidstate.getStr()
        b.inRefresh.inData.tvNoData.text = R.string.info_no_search_result_bid.getStr()
//        b.refresh.setEnableLoadMore(false)  // 하단 loadMore 불능.
//        b.refresh.setEnableRefresh(false)  // 상단 refresh 불능.

        setupRV()
        getEsDataList(0)   // 처음 검색
    }

    /** 달력 세팅 */
    private fun attachCalendar(context: Context) {
        CustomCalendarNormal(context, true,
                onDate = {
                    date8 = it
                    getEsDataList(0)
                }).apply { b.frame.addView(this) }
    }

    /***********************************************************************************************************
     ***************************************** 입찰 현황 ********************************************************/

    /** * adapter 세팅 */
    private fun setupRV() {
        bidAdapter = BidAdapter(items, rvType, null)
        b.inRefresh.rv.adapter = bidAdapter
        SnapHelper().attachToRecyclerView(b.inRefresh.rv)
//        val mLayoutManager = LinearLayoutManager(context)
//        mLayoutManager.reverseLayout = true
//        mLayoutManager.stackFromEnd = true
//        b.inRefresh.rv .setLayoutManager(mLayoutManager)
    }


    /** * 가져오기  */
    private fun getEsDataList(from: Int) {

        val query =
                when (rvType) {
                    /** 단순 현황 검색*/
                    BidAdapter.rvBidState -> {
                        when (adType) {
                            AdType.TOP_BOARD -> ESquery.bidTopBoard(esSize, from, adType.no, date8, boardKey!!, Status.BID_FAILED)   // failed 부터 검색.
                            else -> ESquery.bidStatus(esSize, from, adType.no, date8, Status.BID_FAILED)
                        }
                    }

                    /** 내 입찰 검색 */
                    BidAdapter.rvMyBid -> ESquery.bidManage(esSize, from, myuid(), Status.AD_DENIED)  // AD_DENIED 부터

                    /** 전체 입찰 검색 for admin */
                    BidAdapter.rvAllBid -> ESquery.bidManageAdmin(esSize, from, adType.no, date8, status)  // status : 선택에 따라.
                    else -> ""
                }

        ESget.getBidList("CustomBidState",query,
                success = {
                    esLastIndex = OnRv.setRvRefresh(it, from, items, b.inRefresh, false)
//                    esLastIndex = OnRv.setSearchResult(it, from, items, b.tvNoResult, R.string.info_no_search_result_bid.getStr(), b.avLoading, b.refresh)
                    bidAdapter.notifyDataSetChanged()

//                    lllogI("CustombidStatus childCount ${b.rv.childCount}")
//                    b.refresh.finishRefresh(0)  // swf 닫기
//                    b.refresh.finishLoadMore(0) // swf 닫기

//                    b.refresh.setEnableLoadMore(it?.isNotEmpty() ?:true)   // 더이상 data 없으면 loadMore 불능.

                },
                fail = {})
    }


}