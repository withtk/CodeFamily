package com.tkip.mycode.ad

import com.tkip.mycode.R
import com.tkip.mycode.dialog.toast.toastAlert
import com.tkip.mycode.funs.common.lllogI
import com.tkip.mycode.init.base.getStringAmountMultiplyTerm
import com.tkip.mycode.init.base.isAnyNull
import com.tkip.mycode.init.base.isNullBlank
import com.tkip.mycode.model.bid.Bid
import com.tkip.mycode.model.my_enum.*


/*****************************************************************************************************
 *************************  배너 *******************************************************************
 *********************************************************************************************************/

fun BannerType.getRdID() =
        when (this) {
            BannerType.MAIN -> R.id.rb_main
            BannerType.MENU -> R.id.rb_menu
            BannerType.TOP -> R.id.rb_top
            else -> R.id.rb_top
        }


/**
 * Banner 유효성 체크
 * 통과 못하면 -> true
 */
fun Bid.bannerInValid(): Boolean {
    val checkTitle = if (this.bannerTypeNo != BannerType.MAIN.no) this.title.isNullBlank(R.string.error_add_ad_unit_title) else false  // main은 글자없는 배너 가능하게.
    return (checkTitle || this.bannerTypeNo.isAnyNull(R.string.error_add_banner_bannertype))
}

fun Bid.setAdBannerType(adType: AdType) {
    adTypeNo = adType.no
    bannerTypeNo = adType.bannerType.no
}

///**
// * 입찰 유효성 체크
// * 통과 못하면 -> true
// */
//fun Bid.bidInValid(): Boolean {
//    return (
//            this.title.isAnyNull(R.string.error_add_ad_unit_title)
//                    || this.adTypeNo.isAnyNull(R.string.error_add_banner_bannertype)
////                    || bannerTypeNo!!.getBannerType().invalidPhoto()   // 포토 필요할 때, 포토 없으면.
//            )
//}


fun Bid.setBanner(banner: Bid) {
    this.title = banner.title
    this.comment = banner.comment
    this.comment2 = banner.comment2
    this.photoThumb = banner.photoThumb
    this.photoUrl = banner.photoUrl
}


/**
 * 선택한 model을 bid에 적용.
 */
val maxComment: Int by lazy { 30 }
fun Bid.setModel(any: Any) {
    this.dataTypeNo = any.getMyDataType().no
    this.modelKey = any.anyKey()
    this.modelTitle = any.anyTitle()
    this.modelThumb = any.anyThumb(0)
    this.comment = any.anyComment()?.let { if (it.length > maxComment) it.substring(0, maxComment) else it }
    lllogI("#####addAdActivity setModel Bid $this")
}


/*****************************************************************************************************
 *************************  입찰 *******************************************************************
 *********************************************************************************************************/


/**
 * Bidding 유효성 체크
 * 통과 못하면 -> true
 */
fun Bid.biddingInValid(): Boolean {

    return (
            this.adTypeNo.isAnyNull(R.string.error_add_ad_type)
                    || (if (this.adTypeNo!!.getAdtype() == AdType.TOP_BOARD) boardKey.isAnyNull(R.string.error_ad_top_board_key) else false)
                    || this.dataTypeNo.isAnyNull(R.string.error_ad_data_type)
                    || (this.term ?: 0 < 1).apply { if (this) toastAlert(R.string.error_ad_term) }
                    || (this.amount == 0).apply { if (this) toastAlert(R.string.error_ad_amount) }
                    || this.payTypeNo.isAnyNull(R.string.error_ad_paytype)
                    || (this.amount ?: 0 < this.adTypeNo!!.getAdtype().minCredit).apply { if (this) toastAlert(R.string.error_ad_amount_min) }
            )
}


/**
 * "90 x 3일간"
 */
fun Bid.amountAndTerm() = getStringAmountMultiplyTerm(this.amount ?: 0, this.term ?: 0)


/***************************************************************************************************
 *  승인 필요한 광고타입 = true
 *  */
fun Bid.isBannerToBeNeededGrant(toast: Boolean): Boolean {
    if (this.bannerTypeNo == BannerType.MAIN.no) {
        if (toast) toastAlert(R.string.alert_banner_need_granted)
        return true
    }
    return false
}

/**
 *  승인 필요한 광고타입 여부 + 현재 미승인 = true
 **************************************************************************************************/
fun Bid.needGranted(toast: Boolean): Boolean {
    if (this.isBannerToBeNeededGrant(false) && this.statusNo != Status.BANNER_GRANTED.no) {
        if (toast) toastAlert(R.string.alert_banner_need_granted)
        return true
    }
    return false
}

/** * 승인 체크 */
fun Bid.isNotGranted(toast: Boolean): Boolean {
    if (this.statusNo != Status.BANNER_GRANTED.no) {
        if (toast) toastAlert(R.string.status_banner_denied)
        return true
    }
    return false
}

