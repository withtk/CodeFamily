package com.tkip.mycode.ad.ad_show

import android.view.View
import android.view.ViewGroup
import androidx.viewpager.widget.PagerAdapter
import androidx.viewpager.widget.ViewPager
import com.tkip.mycode.ad.BannerType
import com.tkip.mycode.model.bid.Bid
import com.tkip.mycode.funs.common.lllogI


/**
 */
class BannerPagerAdapter(val list: ArrayList<Bid>, val adSign:Boolean ) : PagerAdapter() {

    override fun instantiateItem(container: ViewGroup, p: Int): Any {

        val bid = list[p]
        lllogI("")
        val view = when (bid.bannerTypeNo) {
            BannerType.TOP.no -> ViewBannerTop(container.context, true , bid,false,null)
            BannerType.MAIN.no -> ViewBannerMAIN(container.context, false , bid,false,null)
            BannerType.MENU.no -> ViewBannerMENU(container.context, true , bid,false,null)
            else -> ViewBannerTop(container.context, true,bid,false,null)
        }

        container.addView(view)
        return view
    }

//    override fun instantiateItem(container: ViewGroup, p: Int): Any {
//        lllogI("TestPager instantiateItem")
//        b = ViewTvInfoBinding.inflate(LayoutInflater.from(container.context), container, false)
//        b.tvInfoModel.text = "test $p"
//        lllogI("TestPager instantiateItem b $b")
//        container.addView(b.root)
//        return b.root
//    }

    override fun isViewFromObject(view: View, obj: Any): Boolean {
        return view == obj
    }

    override fun destroyItem(container: ViewGroup, position: Int, `object`: Any) {
        (container as ViewPager).removeView(`object` as View)
//        super.destroyItem(container, position, `object`)
    }

    override fun getCount(): Int {
//        lllogI("BannerPagerAdapter getCount ${list.size}")
        return list.size
    }

}
