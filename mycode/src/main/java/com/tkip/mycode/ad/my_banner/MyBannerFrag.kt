package com.tkip.mycode.ad.my_banner

import android.os.Bundle
import com.tkip.mycode.R
import com.tkip.mycode.databinding.FragmentMyBannerBinding
import com.tkip.mycode.funs.common.lllogI
import com.tkip.mycode.init.PUT_BANNER_NO
import com.tkip.mycode.init.PUT_MANAGE
import com.tkip.mycode.init.PUT_UID
import com.tkip.mycode.init.my_bind.BindingFragment
import com.tkip.mycode.model.bid.Bid


class MyBannerFrag : BindingFragment<FragmentMyBannerBinding>() {
    override fun getLayoutResId(): Int = R.layout.fragment_my_banner
    private val uid by lazy { arguments?.getString(PUT_UID) }
    private val manage by lazy { arguments?.getBoolean(PUT_MANAGE, false) ?: false }
    private val typeNo by lazy { arguments?.getInt(PUT_BANNER_NO) }

    lateinit var customMyBannerList: CustomMyBannerList
    private lateinit var onSelected: (Bid) -> Unit

    companion object {
        @JvmStatic
        fun newInstance(uid: String?,  manage: Boolean, typeNo: Int, onItemClick: (Bid) -> Unit) = MyBannerFrag().apply {
            arguments = Bundle().apply {
                putString(PUT_UID, uid)
                putInt(PUT_BANNER_NO, typeNo)
                putBoolean(PUT_MANAGE, manage)
            }
            this.onSelected = onItemClick
        }
    }


    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        b.fragment = this
        lllogI("CustomMyBannerList    MyBannerFrag  start")


        typeNo?.let {
            customMyBannerList = CustomMyBannerList(mContext, uid, manage, it, onSelected = { banner ->
                lllogI("CustomMyBannerList    MyBannerFrag result banner $banner")
                onSelected(banner)
            })
            b.frame.addView(customMyBannerList)
        }

//        typeNo?.let {
//            b.frame.addView(CustomMyBannerList(mContext, uid,manage, it, onSelected = { banner ->
//                lllogI("CustomMyBannerList    MyBannerFrag result banner $banner")
//                onSelected(banner)
//            }))
//        }
    }


}

