package com.tkip.mycode.ad

import com.tkip.mycode.funs.common.lllogD
import com.tkip.mycode.model.bid.Bid
import com.tkip.mycode.model.my_enum.Status
import com.tkip.mycode.util.lib.retrofit2.ESget
import com.tkip.mycode.util.lib.retrofit2.ESquery


class OnAd {

    companion object {

        /******************************************************************************
         * 모든 광고는 여기서 시작된다.
         *******************************************************************************/
//        fun setAllAdBanner(context: Context) : CustomBannerSlide {
//
//            when
//
//            /**   광고 top */
//            val adType = if (board.key == RECENT_BOARD_KEY) AdType.TOP_TOTAL_BOARD else AdType.TOP_BOARD    // recentBoard일 때 분기.
//            val boardKeyForAd = if (board.key == RECENT_BOARD_KEY) null else board.key   // recentBoard가 아니면 개별보드의 키.
//
//
//            /**   광고 top */
//            banner = CustomBannerSlide(mContext, AdType.TOP_FLEA, null)
//            b.frameAd.addView(banner)
//        }


        /******************************************************************************
         ******************************   ****************************************
         *****************************************************************************/

        /**     * 부족한 광고 Count     */
        fun getLackCount(adType: AdType, list: ArrayList<Bid>?) = adType.stack - (list?.size ?: 0)

        /**   기본 광고 불러오기  : TOP_BOARD일때는 TOP_TOTAL_BOARD */
        fun getBasicBanner(adType: AdType, onSuccess: (basicList: ArrayList<Bid>?) -> Unit) {
            val query = if (adType == AdType.TOP_BOARD) ESquery.bidStatus(10, 0,  AdType.TOP_TOTAL_BOARD.no, HIT_DATE_DEFAULT_BID, Status.BID_SUCCESS)
            else ESquery.bidStatus(10, 0, adType.no, HIT_DATE_DEFAULT_BID, Status.BID_SUCCESS)
            ESget.getBidList("$adType getBasicBanner", query,
                    success = {
                        lllogD("OnAd ($adType) getBasicBanner query $query")
                        lllogD("OnAd getBasicBanner size:${it?.size}")
                        onSuccess(it)
                    }, fail = {})
        }

        /**   TOTAL TOP 광고 불러오기  */
        fun getTopTotalAd(onSuccess: (totalList: ArrayList<Bid>?) -> Unit) {
            val query = ESquery.bidStatus(10, 0, AdType.TOP_TOTAL_BOARD.no, HIT_DATE, Status.BID_SUCCESS)
            ESget.getBidList("  getTopTotalAd", query,
                    success = {
                        lllogD("OnAd   getTopTotalAd query $query")
                        lllogD("OnAd getTopTotalAd ${it?.size}")
                        onSuccess(it)
                    }, fail = {})
        }

        /**   개별 Board 광고 불러오기  */
        fun getBoardAd(boardKey: String, onSuccess: (boardAdList: ArrayList<Bid>?) -> Unit) {
            val query = ESquery.bidTopBoard(10, 0, AdType.TOP_BOARD.no, HIT_DATE, boardKey, Status.BID_SUCCESS)
            ESget.getBidList("  getEsData", query,
                    success = {
                        lllogD("OnAd   getBoardAd query $query")
                        lllogD("OnAd getBoardAd ${it?.size}")
                        onSuccess(it)
                    }, fail = {})
        }

        /**   adType에 맞는 광고 불러오기  */
        fun getAds(adType: AdType, onSuccess: (adList: ArrayList<Bid>?) -> Unit) {
            val query = ESquery.bidStatus(10, 0, adType.no, HIT_DATE, Status.BID_SUCCESS)
            ESget.getBidList("$adType getEsData", query,
                    success = {
                        lllogD("OnAd ($adType) getAds query $query")
                        lllogD("OnAd getAds ${it?.size}")
                        onSuccess(it)
                    }, fail = {})
        }

    }

}