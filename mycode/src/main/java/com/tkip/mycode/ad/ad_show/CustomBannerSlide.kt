package com.tkip.mycode.ad.ad_show

import android.annotation.SuppressLint
import android.content.Context
import android.os.Handler
import android.view.LayoutInflater
import android.view.MotionEvent
import android.widget.LinearLayout
import androidx.databinding.DataBindingUtil
import androidx.viewpager.widget.ViewPager
import com.tkip.mycode.R
import com.tkip.mycode.ad.AdType
import com.tkip.mycode.ad.BannerType
import com.tkip.mycode.ad.HIT_DATE_DEFAULT_BID
import com.tkip.mycode.ad.OnAd
import com.tkip.mycode.databinding.CustomBannerSlideBinding
import com.tkip.mycode.funs.common.lllogD
import com.tkip.mycode.funs.common.lllogI
import com.tkip.mycode.funs.common.lllogM
import com.tkip.mycode.model.bid.Bid
import com.tkip.mycode.model.my_enum.Status
import com.tkip.mycode.util.lib.retrofit2.ESget
import com.tkip.mycode.util.lib.retrofit2.ESquery
import com.tkip.mycode.util.tools.anim.gone
import com.tkip.mycode.util.tools.date.getIntDate8TodayPH
import com.tkip.mycode.util.tools.view_pager.ViewPagerHori
import com.tkip.mycode.util.tools.view_pager.ViewPagerVert

/**
 * 배너 페이저 vertical
 * onReady : 셋팅 끝났을 때. target Activity에서 add 할 것.
 */
class CustomBannerSlide : LinearLayout {
    private lateinit var b: CustomBannerSlideBinding
    private lateinit var pager: ViewPager
    lateinit var adType: AdType
    private var boardKey: String? = null
    private var manage = false  // true : 기본광고  (상단 광고표시 여부 결정됨.)
    private val status = Status.BID_SUCCESS

    //    private val hitDate by lazy { getIntDate8TodayPH() }
    //    private var basicList: ArrayList<Bid>? = null
    private var finalList: ArrayList<Bid> = arrayListOf()
//    private val size = 10 // 혹시 몰라서 그냥 BID_SUCCESS는 모두 받아와.

    constructor(context: Context) : super(context)
    constructor(context: Context, adType: AdType, boardKey: String?) : super(context) {
        b = DataBindingUtil.inflate(LayoutInflater.from(context), R.layout.custom_banner_slide, this, true)
        this.adType = adType
        this.boardKey = boardKey
        lllogI("CustomBannerSlide adType $adType")


        /*** 시작하자마자 indicator 무조건 gone으로 : 나중에 분기하기보다 이게 낫다. */
        b.pageIndicator.gone()

//        setTimer()

        getEsData(context, boardKey)

    }

    /**  <입찰된 오늘의 광고 불러오기>
     * 1. TOP_BOARD 광고 => TOTAL_TOP 가져오기 + 기본광고
     * 2. 그 외 광고  +  기본광고
     * */
    private fun getEsData(context: Context, boardKey: String?) {

        when (adType) {
            AdType.TOP_BOARD -> {
                OnAd.getBoardAd(boardKey!!,
                        onSuccess = {
                            lllogD("CustomBannerSlide getEsData TOP_BOARD ${it?.size}")
                            if (it.isNullOrEmpty()) OnAd.getTopTotalAd(onSuccess = { totalList -> setFinalList(context, totalList) })
                            else setFinalList(context, it)
                        })
            }
            else -> OnAd.getAds(adType, onSuccess = { setFinalList(context, it) })
        }

    }

    private fun setFinalList(context: Context, list: ArrayList<Bid>?) {
        /*** 유효한 기본광고 받아오기 */
        OnAd.getBasicBanner(adType) { basicList ->
            /*** 광고 리스트 삽입하기 */
            list?.let { finalList.addAll(it) }
            basicList?.let { finalList.addAll(it) }
            setAdapter(context)
        }
    }


    @SuppressLint("ClickableViewAccessibility")
    private fun setAdapter(context: Context) {
        lllogI("CustomBannerSlide getBidList setAdapter  (adType:$adType)   finalList${finalList?.size}")

        if (finalList.isEmpty()) return
        setupRunnable(finalList.size)
//        startSlide()

        /**    viewPager & 인디케이터 셋팅       */
        when (adType.bannerType) {
            BannerType.TOP, BannerType.MENU -> pager = ViewPagerVert(context)   // 세로 슬라이드
            BannerType.MAIN -> {
                pager = ViewPagerHori(context)   // 가로 슬라이드
                b.pageIndicator.setViewPager(pager)   // 하단 동그라미 붙이기.
            }
            BannerType.NOTHING, BannerType.FLEA, BannerType.ROOM -> pager = ViewPagerVert(context)  // 뭐였지??
        }

        /**         * 터치중일때는 슬라이드 정지         */
        pager.setOnTouchListener { v, event ->
            when (event.action) {
                MotionEvent.ACTION_DOWN -> lllogI("Pressed : ${event.x}, ${event.y}")
                MotionEvent.ACTION_MOVE -> stopSlide()
                MotionEvent.ACTION_UP -> startSlide("CustomBannerSlide")
            }
            v.parent.requestDisallowInterceptTouchEvent(true)
            return@setOnTouchListener false
        }


        pager.adapter = BannerPagerAdapter(finalList, manage)
        pager.offscreenPageLimit = finalList.size // tab이 리셋되지 않도록.

        b.line.addView(pager, 0)

    }

    private fun setIndicator() {
        b.pageIndicator.setViewPager(pager)
        b.pageIndicator.gone()

        b.pageIndicator.setViewPager(pager)
//        b.pageIndicator.visi()
    }


    /********************************************************************************************
     *************  auto slide  *****************************************************************/

    private val timeHandler: Handler = Handler()
    private val slideTime = 5000L
    private var run: Runnable? = null
    //    private var totalSize = 0
//    private var cur = 0


    /**
     * runnable 셋팅만 : postDelayed 실행하지 않으면 슬라이드 아직 시작 안 함.
     */
    private fun setupRunnable(totalSize: Int) {
        lllogM("   setTimer  start ")
        if (finalList.isEmpty()) return
        var currentNo = 0
        run = object : Runnable {
            override fun run() {
                lllogI("   setTimer $adType run Start => $currentNo totalSize $totalSize")

                if (totalSize > 1) {
                    val next = currentNo % totalSize
                    pager.currentItem = next   // slide 하기.
                    currentNo = next + 1
//                    lllogD(" CustomBannerSlide setTimer  gte1 Start =>   ${this.hashCode()} -> currentNo $currentNo totalSize $totalSize")
                    timeHandler.postDelayed(this, slideTime)   // run 한번 실행 후 무한 반복 시작.
                }
            }
        }
    }

    fun startSlide(method: String) {
        lllogM("startSlide setTimer  ($method) =>$adType size:${finalList.size}")
        if (finalList.size > 1) {
            run?.let {
                timeHandler.removeCallbacks(it)
//            timeHandler.post(it)
                timeHandler.postDelayed(it, 1000)
            }
        }
    }

    fun stopSlide() {
        lllogM("$adType stopSlide -   setTimer ")
        run?.let { timeHandler.removeCallbacks(it) }
    }


}


