package com.tkip.mycode.ad.bid

import android.os.Bundle
import android.view.View
import androidx.annotation.LayoutRes
import androidx.lifecycle.Observer
import com.haibin.calendarview.Calendar
import com.haibin.calendarview.CalendarView
import com.tkip.mycode.R
import com.tkip.mycode.databinding.ActivityAdMainBinding
import com.tkip.mycode.funs.common.lllogD
import com.tkip.mycode.init.base.getInt
import com.tkip.mycode.init.base.getViewModel
import com.tkip.mycode.init.my_bind.BindingActivity
import com.tkip.mycode.model.cost.CostAdapter
import com.tkip.mycode.model.peer.OnPeer
import com.tkip.mycode.model.viewmodel.CostViewModel
import com.tkip.mycode.util.lib.calendar.monthDate
import com.tkip.mycode.util.tools.anim.AnimUtil

/**
 * 미사용중.
 * 전체광고를 보는
 */
class AdMainActivity : BindingActivity<ActivityAdMainBinding>() {

    @LayoutRes
    override fun getLayoutResId() = R.layout.activity_ad_main

    private lateinit var adapter: CostAdapter
    private val vmCost by lazy { getViewModel<CostViewModel>() }

    private val esSize by lazy { R.integer.es_size_bid.getInt() }
    private var esLastIndex = 0

    private var mYear: Int? = null


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        b.activity = this


        var init = true
        vmCost.liveCostList.observe(this@AdMainActivity, Observer {

            if (init) {
//                b.rv.layoutManager = LinearLayoutManager(mContext)
//                b.rv.addItemDecoration(GroupItemDecoration<String, Cost>())
//                adapter = CCCostAdapter(mContext, it)
//                b.rv.adapter = adapter
//                b.rv.notifyDataSetChanged()

//                lllogD("onActivityCreated init")

                adapter = CostAdapter(it, null)
                b.rv.adapter = adapter
                b.rv.setHasFixedSize(true)
                init = false

            } else {
//                lllogD("onActivityCreated size : ${it.size}")
                adapter.notifyDataSetChanged()

//                b.refresh.finishRefresh(0)  // swf 닫기
//                b.refresh.finishLoadMore(0) // swf 닫기
//
//                b.refresh.setEnableLoadMore(esLastIndex != it.size)   // 같으면(더이상 data 없으면) : loadMore 불능.
//                esLastIndex = it.size
//                AnimUtil.hideNormal(b.inData.tvRefresh)
//                AnimUtil.hideNormal(b.inData.tvNoData)
            }
        })
        setUpCalendar()
        getFirstESdata()

    }

    private fun getFirstESdata() {
        esLastIndex = 0  // 필수.
        vmCost.getEsCostList(esSize, esLastIndex, OnPeer.myUid())
//        b.inData.tvRefresh.gone()
    }


    private fun setUpCalendar() {

        mYear = b.cal.curYear

        b.tvMonthDay.text = b.cal.monthDate()
        b.tvYear.text = b.cal.curYear.toString()
        b.tvCurrentDay.text = b.cal.curDay.toString()
        AnimUtil.hideNormal(b.tvYear)


        b.tvMonthDay.setOnClickListener(View.OnClickListener {
            if (!b.calLayout.isExpand) {
                b.calLayout.expand()
                return@OnClickListener
            }

            b.cal.showYearSelectLayout(b.cal.curYear)
            AnimUtil.hideOverRight(b.tvYear)
            b.tvMonthDay.text = b.cal.curYear.toString()
        })


        b.frameCurrent.setOnClickListener { b.cal.scrollToCurrent() }

        b.cal.setOnCalendarRangeSelectListener(object : CalendarView.OnCalendarRangeSelectListener {
            override fun onCalendarSelectOutOfRange(calendar: Calendar?) {
                lllogD("calendarView onCalendarSelectOutOfRange")

            }

            override fun onCalendarRangeSelect(calendar: Calendar?, isEnd: Boolean) {
                lllogD("calendarView onCalendarRangeSelect")

            }

            override fun onSelectOutOfRange(calendar: Calendar?, isOutOfMinRange: Boolean) {
                lllogD("calendarView onCalendarRangeSelect")

            }

        })

        b.cal.setOnViewChangeListener { lllogD("setOnViewChangeListener init") }
        b.cal.setOnCalendarSelectListener(object : CalendarView.OnCalendarSelectListener {
            override fun onCalendarSelect(calendar: Calendar, isClick: Boolean) {
                lllogD("calendarView onCalendarSelect")
                AnimUtil.showNormal(b.tvYear)
                val str = calendar.month.toString() + "월" + calendar.day + "일"
                b.tvMonthDay.text = str
                b.tvYear.text = calendar.year.toString()
                mYear = calendar.year
            }

            override fun onCalendarOutOfRange(calendar: Calendar?) {
                lllogD("calendarView onCalendarOutOfRange")
            }
        })


//        b.cal.setOnCalendarInterceptListener(object : CalendarView.OnCalendarInterceptListener {
//            override fun onCalendarInterceptClick(calendar: Calendar?, isClick: Boolean) {
//                lllogD("calendarView onCalendarInterceptClick")
//            }
//
//            override fun onCalendarIntercept(calendar: Calendar?): Boolean {
//                lllogD("calendarView onCalendarIntercept")
//                return true
//            }
//        })

    }


}