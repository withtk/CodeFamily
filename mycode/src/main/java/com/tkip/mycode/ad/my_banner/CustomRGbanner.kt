package com.tkip.mycode.ad.my_banner

import android.content.Context
import android.view.LayoutInflater
import android.widget.LinearLayout
import androidx.databinding.DataBindingUtil
import com.tkip.mycode.R
import com.tkip.mycode.ad.BannerType
import com.tkip.mycode.ad.getBannerType
import com.tkip.mycode.ad.getRdID
import com.tkip.mycode.databinding.CustomRgBannerBinding

/**
 * RG  배너 종류
 */
class CustomRGbanner : LinearLayout {
      lateinit var b: CustomRgBannerBinding

    constructor(context: Context) : super(context)
    constructor(context: Context, bannerTypeNo: Int?, onRadio: ((bannerType:BannerType) -> Unit)?) : super(context) {
        b = DataBindingUtil.inflate(LayoutInflater.from(context), R.layout.custom_rg_banner, this, true)

        /** set 라디오 그룹 */
        b.rg.setOnCheckedChangeListener { _, checkedId ->
            when (checkedId) {
                R.id.rb_main -> onRadio?.invoke(BannerType.MAIN)
                R.id.rb_menu -> onRadio?.invoke(BannerType.MENU)
                R.id.rb_top -> onRadio?.invoke(BannerType.TOP)
            }
        }

        /**
         * 초기선택
         */
        bannerTypeNo?.let {
            b.rg.check(it.getBannerType().getRdID())
        }

    }


}