package com.tkip.mycode.ad.bid

import android.os.Bundle
import androidx.annotation.LayoutRes
import com.tkip.mycode.R
import com.tkip.mycode.ad.AdType
import com.tkip.mycode.ad.getAdtype
import com.tkip.mycode.databinding.ActivityBidStateBinding
import com.tkip.mycode.funs.common.lllogI
import com.tkip.mycode.init.PUT_BID
import com.tkip.mycode.init.PUT_RV_TYPE
import com.tkip.mycode.init.base.getStr
import com.tkip.mycode.init.inter.interToolbarIcon
import com.tkip.mycode.init.my_bind.BindingActivity
import com.tkip.mycode.model.bid.Bid


/**
 * 1. 입찰현황
 * 1. 내 입찰내역
 * 1. 전체 입찰내역
 *
 * adType 필수
 * adType.TOP_BOARD : boardKey, boardName 필수
 */
class BidStateActivity : BindingActivity<ActivityBidStateBinding>() {
    @LayoutRes
    override fun getLayoutResId() = R.layout.activity_bid_state

    private val rvType by lazy { intent.getIntExtra(PUT_RV_TYPE, BidAdapter.rvBidState) }
    private val bid by lazy { intent.getParcelableExtra<Bid>(PUT_BID) }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        initView()
    }

    private fun initView() {

        val title =
                when (rvType) {
                    BidAdapter.rvBidState -> R.string.title_ad_check.getStr()
                    BidAdapter.rvMyBid -> R.string.menu_mybid_status.getStr()
                    BidAdapter.rvAllBid -> R.string.menu_all_bid_status.getStr()
                    else -> R.string.title_ad_check.getStr()
                }

        val info =
                when (rvType) {
                    BidAdapter.rvBidState -> R.string.title_ad_term_calendar.getStr()
                    BidAdapter.rvMyBid -> null
                    BidAdapter.rvAllBid -> R.string.title_ad_term_calendar.getStr()
                    else -> R.string.title_ad_check.getStr()
                }


        /** Toolbar */
        b.inToolbar.title = title
        b.inToolbar.backIcon = true
        b.inToolbar.inter = interToolbarIcon(onFirst = { onBackPressed() }, onTopTitle = {}, onTextMenu = { }, onTextMenu2 = null, onIcon = {}, onMore = {})


        lllogI("BidStateActivity rvType $rvType")
        lllogI("BidStateActivity bid $bid")
        CustomBidState(this@BidStateActivity,info, rvType, bid?.adTypeNo?.getAdtype()?: AdType.TOP_TOTAL_BOARD, bid?.boardKey, bid?.boardName)
                .apply { b.frame.addView(this) }

    }

}
