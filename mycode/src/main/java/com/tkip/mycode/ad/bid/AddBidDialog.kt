package com.tkip.mycode.ad.bid

import android.app.Dialog
import android.content.Context
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import com.tkip.mycode.R
import com.tkip.mycode.ad.*
import com.tkip.mycode.databinding.DialogAddBidBinding
import com.tkip.mycode.dialog.OnDDD
import com.tkip.mycode.dialog.alert.MyAlert
import com.tkip.mycode.dialog.progress.MyCircle
import com.tkip.mycode.dialog.toast.toastAlert
import com.tkip.mycode.funs.common.getNullStr
import com.tkip.mycode.funs.common.lllogD
import com.tkip.mycode.funs.common.lllogI
import com.tkip.mycode.funs.search.view.CustomSearchAll4Bid
import com.tkip.mycode.init.PUT_BID
import com.tkip.mycode.init.base.getStr
import com.tkip.mycode.init.base.isNullBlankAndSetError
import com.tkip.mycode.init.inter.interDialogBasicClick
import com.tkip.mycode.init.my_bind.BindingDialog
import com.tkip.mycode.model.bid.Bid
import com.tkip.mycode.model.my_enum.DataType
import com.tkip.mycode.model.my_enum.STATUS_LIST_BID
import com.tkip.mycode.model.my_enum.Status
import com.tkip.mycode.model.my_enum.anyTitle
import com.tkip.mycode.model.peer.isAdmin
import com.tkip.mycode.util.lib.calendar.AD_BASIC_DATE
import com.tkip.mycode.util.lib.photo.util.PhotoUtil
import com.tkip.mycode.util.lib.photo.view.LinearAddPhotoOne
import com.tkip.mycode.util.lib.retrofit2.ESget
import com.tkip.mycode.util.lib.retrofit2.ESquery
import com.tkip.mycode.util.tools.date.getIntDate8TodayPH
import com.tkip.mycode.util.tools.etc.OnMyClick
import com.tkip.mycode.util.tools.radio.CustomRGbasic
import com.tkip.mycode.util.tools.spinner.LinearSpinnerStatus
import com.tkip.mycode.util.tools.spinner.LinearSpinnerString
import com.tkip.mycode.util.tools.spinner.TypeList


/**
 *
 */
class AddBidDialog : BindingDialog<DialogAddBidBinding>() {
    override fun getLayoutResId(): Int = R.layout.dialog_add_bid
    override fun getSize(): Pair<Float?, Float?> = Pair(0.95f, null)


    private val bid by lazy { arguments?.getParcelable(PUT_BID) ?: Bid().apply { } }
    private var onOk: ((Bid) -> Unit)? = null
    private lateinit var spinStatus: LinearSpinnerStatus
    private lateinit var spinAdType: LinearSpinnerString
    private lateinit var spinBannerType: LinearSpinnerString

    companion object {
        @JvmStatic
        fun newInstance(bid: Bid?, onAdded: (Bid) -> Unit) = AddBidDialog().apply {
            this.onOk = onAdded
            arguments = Bundle().apply { putParcelable(PUT_BID, bid) }
        }
    }

    override fun onResume() {
        super.onResume()
        if (onOk == null) dismiss()
    }

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {

        initView()

        val builder = AlertDialog.Builder(activity as Context).apply {
            setView(b.root)
            setCancelable(true)
        }

        return builder.create().apply {
            setCanceledOnTouchOutside(true)
            window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        }

    }

    private fun initView() {
        b.dialog = this@AddBidDialog
        b.inter = interDialogBasicClick(onDel = null,
                onCancel = { onClickCancel(it) },
                onOk = { onClickOk(it) })
        b.bid = bid
        lllogI("AddBidDialog initView bid $bid")


        b.tvHitDate.setOnLongClickListener {
            bid.hitDate = AD_BASIC_DATE
            b.tvHitDate.text = AD_BASIC_DATE.toString()
            return@setOnLongClickListener true
        }

        /**         * 배너별 분기         */
        when (bid.bannerTypeNo.getBannerType()) {
            BannerType.MAIN -> {
            }  // MAIN은 타이틀 없어도 됨.
            else -> b.etTitle.setOnFocusChangeListener { _, isOn -> if (!isOn) b.etTitle.isNullBlankAndSetError(b.layTitle, R.string.error_add_ad_unit_title) }
        }

        //        b.etComment.setOnFocusChangeListener { _, isOn -> if (!isOn) b.etComment.isNullBlankAndSetError(b.layComment, R.string.error_add_store_menu_price) }

        if (isAdmin()) {
            viewAddPhotoOne = LinearAddPhotoOne(activity as AppCompatActivity, bid.photoThumb
                    ?: bid.modelThumb, null,
                    null,
                    onGallery = { onClickOpenGallery() })
                    .apply { this@AddBidDialog.b.frameAddPhotoOne.addView(this) }

            /**   스피너 : status     */
            spinStatus = LinearSpinnerStatus(mContext, true, STATUS_LIST_BID, bid.statusNo, onClick = { bid.statusNo = it.no }).apply { this@AddBidDialog.b.lineSpin.addView(this) }
        }

        /**  스피너 : adType   */
        spinAdType = LinearSpinnerString(mContext, true, null, TypeList.AD_TYPE, bid.adTypeNo, onSelected = { bid.setAdBannerType(it.getAdtype()) })
        b.lineSpin.addView(spinAdType)

//        /**  스피너 : bannerType   */
//        spinBannerType = LinearSpinnerString(mContext, true,null, TypeList.BANNER_TYPE, bid.bannerTypeNo, onClick = { bid.bannerTypeNo = it }).apply { this@AddBidDialog.b.lineSpin.addView(this) }

    }

    fun onClickDate(v: View) {
        OnMyClick.setDisableAWhileBTN(v)
        MyAlert.showSelectCalendar(mContext, false,
                onSelected = { date8 ->
                    bid.hitDate = date8
                    b.bid = bid
                })
    }


    fun onClickModel(v: View) {
        OnMyClick.setDisableAWhileBTN(v)
        /***  모델 선택뷰 붙이기  */
        if (b.frame.childCount > 0) {
            b.frame.removeAllViews()
            b.frame2.removeAllViews()
        } else addSelectModel()
//        MyAlert.showSelectModel(mContext, null, false, bid.dataTypeNo?.getDataType()
//                ?: DataType.POST,
//                onSelected = { bid.setModel(it) })
    }

    private fun addSelectModel() {

        val rgModelList = arrayOf(DataType.STORE, DataType.FLEA, DataType.ROOM, DataType.BOARD, DataType.POST)  // model List
        val list = arrayListOf<String>().apply { rgModelList.forEach { add(it.title) } }    // string List
        CustomRGbasic(mContext, list, true,
                onSelected = { listIndex ->


                    /*** 검색창 붙이기 (내 데이타) */
                    CustomSearchAll4Bid(mContext, null, false, rgModelList[listIndex],
                            onSelected = { any ->
                                bid.setModel(any)
                                b.btnModel.text = any.anyTitle()
                                b.frame.removeAllViews()
                                b.frame2.removeAllViews()
                            })
                            .apply {
                                b.frame2.removeAllViews()
                                b.frame2.addView(this)
                            }
                })
                .apply {
                    b.frame.addView(this)
                }


    }


    fun onClickCancel(v: View) {
        OnMyClick.setDisableAWhileBTN(v)
        this.dialog?.cancel()
    }

    fun onClickOk(v: View) {
        OnMyClick.setDisableAWhileBTN(v)
        onOk?.let {

            if (spinAdType.selected == 0) {
                toastAlert(R.string.error_add_ad_type)
                return
            }

            bid.title = b.etTitle.getNullStr()
            bid.comment = b.etComment.getNullStr()
            bid.comment2 = b.etComment2.getNullStr()
            bid.modelTitle = b.etModelTitle.getNullStr()
            bid.amount = b.etAmount.getNullStr()?.toInt()


            /** 업데이트 시작 */
            MyAlert.showConfirm(mContext, R.string.info_wanna_upload_add_bidding.getStr(), ok = {

                /** 이전 이미지 삭제하기.
                 * 1. 그냥 이전 이미지만 삭제.
                 * 2. 새로운 이미지 올림. */
                viewAddPhotoOne?.let {

                    if (it.hasToBeDeletedPhoto && bid.photoThumb != null) deleteImageAndUpdate()
                    else addPhotoAndUpdate()

                } ?: OnDDD.alertNullListner()

            }, cancel = {})
        } ?: OnDDD.alertNullListner()

    }

    private fun deleteImageAndUpdate() {
        lllogI("delAndUpdate start  ")

        /*** 사진이 현재 입찰중인지 체크 */
        ESget.getBidList("delAndUpdate", ESquery.bidPhotoUsed(bid.photoThumb!!, getIntDate8TodayPH(), Status.BID_BIDDING),
                success = { list ->
                    lllogI("delAndUpdate getBidList list ${list?.size}")
                    if (list.isNullOrEmpty() || list.size <= 1) {
                        PhotoUtil.deletePhoto(bid.photoUrl, bid.photoThumb)   // storage에서 지우는 것.
                    }
                    bid.photoUrl = null
                    bid.photoThumb = null
                    addPhotoAndUpdate()
                }, fail = { })
    }

    private fun addPhotoAndUpdate() {
        lllogD("AddBidDialog photoAndUpdate  start bid$bid")
        OnBid.addPhotoAndBid(mContext, bid,
                success = {
                    onOk?.invoke(bid)
                    this.dialog?.cancel()
                    MyCircle.cancel()
                },
                fail = { })
    }


}