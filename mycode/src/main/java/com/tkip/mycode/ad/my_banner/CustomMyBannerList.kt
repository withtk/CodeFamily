package com.tkip.mycode.ad.my_banner

import android.content.Context
import android.view.LayoutInflater
import android.widget.LinearLayout
import androidx.databinding.DataBindingUtil
import com.tkip.mycode.R
import com.tkip.mycode.ad.BannerType
import com.tkip.mycode.ad.getBannerType
import com.tkip.mycode.databinding.CustomMyBannerListBinding
import com.tkip.mycode.funs.common.lllogI
import com.tkip.mycode.init.base.getInt
import com.tkip.mycode.init.base.getStr
import com.tkip.mycode.model.bid.Bid
import com.tkip.mycode.model.my_enum.Status
import com.tkip.mycode.model.peer.isAdmin
import com.tkip.mycode.util.lib.retrofit2.ESget
import com.tkip.mycode.util.lib.retrofit2.ESquery
import com.tkip.mycode.util.tools.anim.gone
import com.tkip.mycode.util.tools.recycler.OnRv

/**
 * 내 배너 종류: main, menu, top
 */
class CustomMyBannerList : LinearLayout {
    private lateinit var b: CustomMyBannerListBinding
    private var uid: String? = null  // 널이면 모든 유저의 데이타  for admin
    private lateinit var bannerType: BannerType
      val items = arrayListOf<Bid>()
    lateinit var adapter: BannerAdapter

    private val esSize by lazy { R.integer.es_size_my_banner.getInt() }
    private var esLastIndex = 0

    constructor(context: Context) : super(context)
    constructor(context: Context, uid: String?, manage: Boolean, bannerTypeNo: Int, onSelected: (Bid) -> Unit) : super(context) {
        b = DataBindingUtil.inflate(LayoutInflater.from(context), R.layout.custom_my_banner_list, this, true)
        this.uid = uid
        this.bannerType = bannerTypeNo.getBannerType()
//        b.title = bannerType.title

        /** set Adapter */
        setAdapter(manage, onSelected)

//        b.viewRefresh.b.refresh.setOnRefreshListener()
        b.inRefresh.refresh.setOnRefreshListener { getEsDataList(0) }
        b.inRefresh.refresh.setOnLoadMoreListener { getEsDataList(esLastIndex) }
        b.inRefresh.inData.tvNoData.text = R.string.info_no_my_banner.getStr()
//        b.refresh.setEnableRefresh(false)   //상단 refresh는 불능
//        b.refresh.setEnableLoadMore(false)  // 하단 loadMore 불능.

        getEsDataList(0)

    }

    fun removeTvNoData() {
        b.inRefresh.inData.tvNoData?.gone()
        b.inRefresh.inData.tvRefresh?.gone()
    }

    private fun setAdapter(manage: Boolean, onSelected: (Bid) -> Unit) {
//        val spanCount = if (bannerType == BannerType.TOP) 1 else 2
        adapter = BannerAdapter(items,(uid==null), manage, onItemClick = {
            lllogI("CustomMyBannerList selected banner $it")
            onSelected(it)
        })
        b.inRefresh.rv.adapter = adapter
//        b.rv.layoutManager = GridLayoutManager(context, spanCount)
//        SnapHelper().attachToRecyclerView(b.rv)   // 스냅헬퍼
    }


    /** * 가져오기  */
    private fun getEsDataList(from: Int) {

        val query =
                if (uid == null && isAdmin()) ESquery.bannerList(esSize, from, bannerType.no)    // admin
                else ESquery.myBannerList(esSize, from, uid!!, bannerType.no,Status.BLIND_ADMIN)   // user

        ESget.getBannerList(query,
                success = {
                    lllogI("CustomMyBannerList getEsDataList size ${it?.size}")
//                    esLastIndex = OnRv.setRvList2(it,from, items, b.inData, R.string.info_no_my_banner.getStr(), b.refresh,b.tvLast)
                    esLastIndex = OnRv.setRvRefresh(it, from, items, b.inRefresh, true)
                    adapter.notifyDataSetChanged()
                },
                fail = {})

    }
}