package com.tkip.mycode.ad.bid

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.RecyclerView
import com.tkip.mycode.R
import com.tkip.mycode.ad.AdType
import com.tkip.mycode.ad.getAdtype
import com.tkip.mycode.databinding.HolderBidStateBinding
import com.tkip.mycode.databinding.HolderMyBidBinding
import com.tkip.mycode.init.base.getStr
import com.tkip.mycode.model.bid.Bid
import com.tkip.mycode.model.peer.isAdmin
import com.tkip.mycode.util.tools.etc.OnMyClick
import java.util.*


/**
 * 1. 입찰 현황 - VERT
 * 1. 나의 입찰내역 - HORI.MAIN, HORI.MENU, VERT.TOP
 */
class BidAdapter(val items: ArrayList<Bid>, val rvType: Int, val onItemClick: ((Bid) -> Unit)?) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    private lateinit var activity: AppCompatActivity

    companion object {
        const val rvAllBid = 10    // 전체 입찰 for admin
        const val rvBidState = 30   // 입찰 현황
        const val rvMyBid = 50     // 나의 입찰
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        activity = parent.context as AppCompatActivity
        return when (rvType) {
            rvMyBid -> MyBidStateViewHolder(HolderMyBidBinding.inflate(LayoutInflater.from(parent.context), parent, false))
            else -> BidStateViewHolder(HolderBidStateBinding.inflate(LayoutInflater.from(parent.context), parent, false))
        }
    }

    override fun onBindViewHolder(h: RecyclerView.ViewHolder, p: Int) {
        when (rvType) {
            rvMyBid -> (h as MyBidStateViewHolder).bind(items[p])
            else -> (h as BidStateViewHolder).bind(items[p])
        }
    }

    override fun getItemId(position: Int): Long {
        return items[position].hashCode().toLong()
    }

    override fun getItemCount(): Int {
        return items.size
    }


    /*** 입찰현황, 전체 입찰현황 for admin  */
    inner class BidStateViewHolder(val b: HolderBidStateBinding) : RecyclerView.ViewHolder(b.root) {

        private lateinit var bid: Bid

        fun bind(bid: Bid) {
            this.bid = bid
            b.holder = this
            b.bid = bid

            /** 경매종료전 : item 번호 표시
             *  경매종료후 : status 표시*/
            val rank = adapterPosition + 1
            b.tvRank.text = rank.toString()
            b.success = (rank <= bid.adTypeNo!!.getAdtype().stack)  // 현재시간 낙찰 유력일때

            /*
              */
            /** 낙찰여부 표시 *//*
            bid.adTypeNo?.getAdtype()?.let { adType ->
                when (adType) {
                    AdType.AD_CREDIT -> {
                        b.success = true
                        b.statusStr = R.string.complete_success_adcredit.getStr()
                    }
                    else -> {
*//*
                        if (bid.statusNo == Status.AD_FAILED.no) {
                            b.statusStr = R.string.complete_fail_bid.getStr()
                        } else {`1
                            b.statusStr = (adapterPosition + 1).toString()
                            val successBid = adapterPosition < adType.stack
                            b.success = successBid
                        }*//*

                        val successBid = adapterPosition < adType.stack
                        b.success = successBid
                        b.statusStr =
                                if (successBid) (adapterPosition + 1).toString()
                                else R.string.complete_fail_bid.getStr()
                    }
                }
            }*/

            b.executePendingBindings()

        }


        fun onClickCard(v: View) {
            OnMyClick.setDisableAWhileBTN(v)
            if (isAdmin()) {
                AddBidDialog.newInstance(bid,
                        onAdded = { bid ->
                            items[adapterPosition] = bid
                            notifyItemChanged(adapterPosition)
                        }).show(activity.supportFragmentManager, "dialog")
            }
        }

//        fun onClickGo(v: View) {
//            OnMyClick.setDisableAWhileBTN(v)
//        }
//
//        fun onClickDel(v: View) {
//            OnMyClick.setDisableAWhileBTN(v)
//        }
//
//        fun onClickUpdate(v: View) {
//            OnMyClick.setDisableAWhileBTN(v)
//        }
    }

    /**     * 나의 입찰   */
    inner class MyBidStateViewHolder(val b: HolderMyBidBinding) : RecyclerView.ViewHolder(b.root) {

        private lateinit var bid: Bid

        fun bind(bid: Bid) {
            this.bid = bid
            b.holder = this
            b.bid = bid

            /** 낙찰여부 표시 */
            bid.adTypeNo?.getAdtype()?.let { adType ->
                when (adType) {
                    AdType.AD_CREDIT -> {
//                        b.success = true
                        b.statusStr = R.string.complete_success_adcredit.getStr()
                    }
                    else -> {
/*
                        if (bid.statusNo == Status.AD_FAILED.no) {
                            b.statusStr = R.string.complete_fail_bid.getStr()
                        } else {
                            b.statusStr = (adapterPosition + 1).toString()
                            val successBid = adapterPosition < adType.stack
                            b.success = successBid
                        }*/

                        val successBid = adapterPosition < adType.stack
//                        b.success = successBid
                        b.statusStr =
                                if (successBid) (adapterPosition + 1).toString()
                                else R.string.complete_fail_bid.getStr()
                    }
                }
            }

            b.executePendingBindings()

        }


        fun onClickCard(v: View) {
            OnMyClick.setDisableAWhileBTN(v)
            if (isAdmin()) {
                AddBidDialog.newInstance(bid,
                        onAdded = { bid ->
                            items[adapterPosition] = bid
                            notifyItemChanged(adapterPosition)
                        }).show(activity.supportFragmentManager, "dialog")
            }
        }

    }


}