package com.tkip.mycode.ad

import android.content.Context
import android.content.Intent
import com.tkip.mycode.R
import com.tkip.mycode.ad.bid.AddBidActivity
import com.tkip.mycode.ad.bid.BidStateActivity
import com.tkip.mycode.ad.bid.SubBidBills
import com.tkip.mycode.dialog.progress.MyCircle
import com.tkip.mycode.dialog.toast.TOAST_ALERT
import com.tkip.mycode.dialog.toast.checkAndToast
import com.tkip.mycode.dialog.toast.toastAlert
import com.tkip.mycode.dialog.toast.toastNormal
import com.tkip.mycode.funs.common.lllogI
import com.tkip.mycode.init.*
import com.tkip.mycode.model.bid.Bid
import com.tkip.mycode.model.cost.Cost
import com.tkip.mycode.model.my_enum.Status
import com.tkip.mycode.model.peer.checkAllAvailUpload
import com.tkip.mycode.util.lib.calendar.isNotBidTime
import com.tkip.mycode.util.lib.calendar.isTimeOver
import com.tkip.mycode.util.lib.photo.util.PhotoUtil
import com.tkip.mycode.util.lib.retrofit2.ESget
import com.tkip.mycode.util.lib.retrofit2.ESquery
import com.tkip.mycode.util.tools.date.rightNowPHhour
import com.tkip.mycode.util.tools.fire.Firee
import com.tkip.mycode.util.tools.fire.batch
import com.tkip.mycode.util.tools.fire.docRef
import com.tkip.mycode.util.tools.fire.myCommit


class OnBid {
    companion object {

        /****************************************************************************************
         ******************************   ************************************************/
        fun gotoAddBidActivity(context: Context) {

            /*** 업로드 가능 여부 체크 */
            if (checkAllAvailUpload(context)) return

            /*** 입찰시간 체크 */
            if (isNotBidTime(context)) return

            lllogI("isNotBidTime rightNowPHhour $rightNowPHhour")

            val intent = Intent(context, AddBidActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
//            intent.putExtra(PUT_BANNER_NO, bannerType.no)
//            intent.putExtra(PUT_MANAGE, manage)
            context.startActivity(intent)
        }

        fun gotoBidStateActivity(context: Context, bid: Bid?, rvType: Int) {
            val intent = Intent(context, BidStateActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
            intent.putExtra(PUT_RV_TYPE, rvType)
            intent.putExtra(PUT_BID, bid)
//            intent.putExtra(PUT_MANAGE, manage)
            context.startActivity(intent)
        }


        /******************************************************************************
         ****************************** upload ****************************************
         *****************************************************************************/

        fun addPhotoAndBid(context: Context, bid: Bid, success: () -> Unit, fail: () -> Unit) {
            PhotoUtil.uploadUriSingle(context, bid,
                    successUploading = { result ->

                        result?.let { foto ->
                            bid.photoThumb = foto.thumbUrl
                            bid.photoUrl = foto.url
                        }

//                        bid.adTypeNo = AdType.NOTHING.no   // adTypeNo 넣기.
                        Firee.addBid(bid,
                                success = {
                                    success()
                                    toastNormal(R.string.complete_update)
                                    MyCircle.cancel()
                                }, fail = { fail() })
                    }, fail = { })
        }

        /*****************************************************************************************
         * 기재한 amount로
         * 마지막 등수에 들어갈 수 있으면 : true
         *******************************************************************************************/
        fun moreThanLowestBid(date8List: ArrayList<Int>, bid: Bid, onResult: (Boolean) -> Unit) {
            val adType = bid.adTypeNo!!.getAdtype()

            val count = arrayListOf<Boolean>()
            date8List.forEach { date8 ->
                getEsLowestBid(date8, adType, bid,
                        onLow = {
                            count.add(it)
                            lllogI("moreThanLowestBid count.add $it  ")
                            if (count.size == date8List.size) {  // 마지막까지 다 받은 뒤에.
                                onResult(!count.contains(false))
                            }
                        })
            }
        }

        private fun getEsLowestBid(date8: Int, adType: AdType, bid: Bid, onLow: (Boolean) -> Unit) {
            val query = ESquery.bidStatus(adType.stack, 0, adType.no, date8, Status.BID_BIDDING)
            ESget.getBidList("getEsLowestBid", query,
                    success = {
                        lllogI("moreThanLowestBid $date8 ${it?.size}")

                        if (it == null || it.size < adType.stack) {
                            onLow(true)   // 스택보다 적으면 무조건 낙찰가능이니까 true
                        } else {
                            val lowBid = it[adType.stack - 1]
                            lllogI("moreThanLowestBid bid ${bid.amount} lowBid ${lowBid.amount}")
                            onLow(bid.amount ?: 0 > lowBid.amount ?: 0)
                        }
                    },
                    fail = {}
            )
        }
        /*********************************************************************************************/


        /****************************************************************************************
         ******************************  낙찰처리 및 유찰처리  ************************************/
        fun getColAdTypeBid(adTypeNo: Int, date8: Int, status: Status) {
            val query = ESquery.bidAdTypeHit(adTypeNo, date8, status)
            ESget.getBidList("getColAdTypeBid", query, success = { handleAuction(it, adTypeNo) }, fail = {})
        }


        fun handleAuction(it: ArrayList<Bid>?, adTypeNo: Int) {
            if (!it.isNullOrEmpty()) {
                val adType = adTypeNo.getAdtype()
                batch().apply {
                    it.forEachIndexed { i, bid ->
                        if (i + 1 <= adType.stack) update(docRef(EES_BID, bid.key), CH_STATUS_NO, Status.BID_SUCCESS.no)  // -> SUCCESS
                        else {
                            update(docRef(EES_BID, bid.key), CH_STATUS_NO, Status.BID_FAILED.no) // -> FAILED
                            val cost = Cost.bidRefund(bid)   // 환불 cost
                            set(docRef(EES_PEER, bid.uid, EES_COST, cost.key), cost)
                        }
                    }
                    myCommit("handleAuction", success = { toastNormal(R.string.status_msg_warn_handled) }, fail = { toastNormal(R.string.error_handle_admin) })
                }
            } else toastAlert(R.string.info_no_result_myroom)
        }

        fun recoverAuction(it: ArrayList<Bid>?) {
            if (!it.isNullOrEmpty()) {
                batch().apply {
                    it.forEach { bid -> update(docRef(EES_BID, bid.key), CH_STATUS_NO, Status.BID_BIDDING.no) }
                }.myCommit("recoverAuction", success = { toastNormal(R.string.status_msg_warn_handled) }, fail = { toastNormal(R.string.error_handle_admin) })
            } else toastAlert(R.string.info_no_result_myroom)
    }


        //todo : 개별 보드 개런티는 일주일에 한번 관리자가 부여하는 걸로.


        /** 유효성 체크 */
        fun checkValidBid(finalBid: Bid) = finalBid.biddingInValid()


        /** 내 크레딧 결제가능 여부 체크 */
        fun notPossiblePayment(vBills: SubBidBills?): Boolean {
            vBills?.apply {
                return (selectPayType?.isNotEnough(sum)
                        ?: true).checkAndToast(TOAST_ALERT, R.string.error_not_enough_credit)
//                sum.notAvailPayTotal(payWithBonus)
            } ?: toastAlert(R.string.error_bills_check)
            return true  // 결제 불가능.
        }


        /** 가장빠른 날짜 체크 & 입찰시간 체크 */
        fun checkValidDate(context: Context, date8List: ArrayList<Int>): Boolean {
            date8List.sort() // 낮은 날짜순으로 정렬.
            return date8List[0].isTimeOver(context)
        }


    }

}