package com.tkip.mycode.ad

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import com.firebase.ui.firestore.FirestoreRecyclerOptions
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.firestore.Query
import com.tkip.mycode.R
import com.tkip.mycode.ad.my_banner.ADDBannerDialog
import com.tkip.mycode.ad.my_banner.MyBannerActivity
import com.tkip.mycode.dialog.toast.toastNormal
import com.tkip.mycode.funs.common.lllogD
import com.tkip.mycode.funs.common.lllogM
import com.tkip.mycode.init.*
import com.tkip.mycode.model.bid.Bid
import com.tkip.mycode.model.grant.Grant
import com.tkip.mycode.model.my_enum.Status
import com.tkip.mycode.model.my_enum.gteNormalStatus
import com.tkip.mycode.model.peer.isAnonymous
import com.tkip.mycode.util.lib.TransitionHelper
import com.tkip.mycode.util.lib.photo.util.PhotoUtil
import com.tkip.mycode.util.tools.fire.*


class OnBanner {
    companion object {

        /****************************************************************************************
         ******************************   ************************************************/

        /**
         * bannerTypeNo : 배너타입 고정되어 tab 이동 불가.
         * return : banner
         */
        fun gotoMyBannerActivity(activity: Activity, bannerTypeNo: Int) {
            val intent = Intent(activity, MyBannerActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
            intent.putExtra(PUT_MANAGE, false)
            intent.putExtra(PUT_BANNER_NO, bannerTypeNo)

            activity.startActivityForResult(intent, REQ_BANNER)
        }

        fun gotoMyBannerActivity(context: Context, uid: String?, arrPair: Array<androidx.core.util.Pair<View, String>>?) {

            /*** 업로드 가능 여부 체크 */
            if (isAnonymous(context)) return

            val intent = Intent(context, MyBannerActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
            intent.putExtra(PUT_UID, uid)
            intent.putExtra(PUT_MANAGE, true)

            TransitionHelper.startTran(context, intent, arrPair)
        }

        fun openADDBannerDialog(activity: AppCompatActivity, addType: Int, bid: Bid?, onOk: ((Bid, Grant?) -> Unit)) {
            lllogD("OnBanner openADDBannerDialog activity ${activity.localClassName}")
            ADDBannerDialog.newInstance(addType, bid, onOk).show(activity.supportFragmentManager, "dialog")
//            MyAlert.showADDBanner(activity,bid,onResult)
        }

        /******************************************************************************
         ****************************** upload ****************************************
         *****************************************************************************/

        fun addPhotoAndBanner(context: Context, addType: Int, banner: Bid, grant: Grant?, success: () -> Unit, fail: () -> Unit) {
            lllogM("OnBanner addPhotoAndBanner addType:$addType bid:$banner")
            PhotoUtil.uploadUriSingle(context, banner,
                    successUploading = { result ->

                        result?.let { foto ->
                            banner.photoThumb = foto.thumbUrl
                            banner.photoUrl = foto.url
                        }

                        batch().apply {
                            /*** 승인도 함께 올릴때 : ADD */
                            grant?.let {
                                if(it.statusNo == Status.GRANT_HANDLED.no) this.addGrant(it)     // 완료처리 체크박스on 일때만.
                            }
                           set(docRef(EES_BANNER, banner.key), banner)   // add Banner
                        }.myCommit("addPhotoAndBanner ",
                                success = {
                                    lllogD("OnBanner addPhotoAndBanner grant:$grant  ")
                                    success()
                                    toastNormal(R.string.complete_upload)
                                    if (banner.needGranted(false) && banner.gteNormalStatus()) Firee.deepDelGrant( banner.key, {}, {})   // Action_Admin 정상 status로 처리시 : push 날린 뒤 삭제.
                                }, fail = {})

//                        bid.adTypeNo = AdType.NOTHING.no   // adTypeNo 넣기.

//                        Firee.addBanner(bid,
//                                success = {
//                                    success()
//                                    toastNormal(R.string.complete_upload)

//                                    MyCircle.cancel()
//                                }, fail = { fail() })
                    }, fail = { })
        }


        /**
         *  실시간 Banner
         */
        fun getOptions(uid: String, bannerType: BannerType) = FirestoreRecyclerOptions.Builder<Bid>()
                .setQuery(FirebaseFirestore.getInstance().collection(EES_PEER).document(uid).collection(EES_BANNER)
                        .whereEqualTo(CH_BANNER_TYPE_NO, bannerType.no)
                        .orderBy(CH_BANNER_TYPE_NO, Query.Direction.DESCENDING)
                        .orderBy(CH_ADD_DATE, Query.Direction.ASCENDING), Bid::class.java)
                .build()


        /******************************************************************************
         ************ Fixed  고정불변 **************************************************
         ******************************************************************************/
//        fun createArrPairFrag(manage: Boolean, bannerTypeNo: Int ): Array<Pair<Fragment, String>> =
//
//                if (manage) arrayOf(
//                        Pair(MyBannerFrag.newInstance(manage,BannerType.TOP.no), R.string.title_top_banner.getStr()),
//                        Pair(MyBannerFrag.newInstance(manage,BannerType.MENU.no), R.string.title_menu_banner.getStr()),
//                        Pair(MyBannerFrag.newInstance(manage,BannerType.MAIN.no), R.string.title_main_banner.getStr()))
//
//                else arrayOf(Pair(MyBannerFrag.newInstance(manage,bannerTypeNo), bannerTypeNo.getBannerType().title))


    }

}