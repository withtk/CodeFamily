package com.tkip.mycode.ad.bid

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.widget.LinearLayout
import android.widget.RadioGroup
import androidx.databinding.DataBindingUtil
import com.tkip.mycode.R
import com.tkip.mycode.ad.AdType
import com.tkip.mycode.ad.BannerType
import com.tkip.mycode.ad.getAdtype
import com.tkip.mycode.admin.*
import com.tkip.mycode.databinding.SubSelectAdtypeBinding
import com.tkip.mycode.databinding.ViewRadioBox2Binding
import com.tkip.mycode.databinding.ViewRadioBoxBinding
import com.tkip.mycode.dialog.OnDDD
import com.tkip.mycode.dialog.alert.MyAlert
import com.tkip.mycode.model.manage.SystemMan
import com.tkip.mycode.model.my_enum.DataType
import com.tkip.mycode.model.peer.isAdmin
import com.tkip.mycode.model.peer.isNotAdmin
import com.tkip.mycode.util.tools.anim.showSlideUp
import com.tkip.mycode.util.tools.anim.visi
import com.tkip.mycode.util.tools.etc.OnMyClick

/**
 * RG 2층으로
 * for addAdActivity only
 *
 * info : 상단 안내메세지
 * hasDesc : 상세설명 표시 여부.
 */
class SubSelectAdType : LinearLayout {
    private lateinit var b: SubSelectAdtypeBinding
    private lateinit var listen1: RadioGroup.OnCheckedChangeListener
    private lateinit var listen2: RadioGroup.OnCheckedChangeListener
    private var hasDesc: Boolean = false

    private var selectedAdType: AdType? = null

    constructor(context: Context) : super(context)
    constructor(context: Context, info: String?, hasDesc: Boolean, dataType: DataType, onClickRb: (AdType) -> Unit) : super(context) {
        b = DataBindingUtil.inflate(LayoutInflater.from(context), R.layout.sub_select_adtype, this, true)
        b.subView = this
        b.dataType = dataType
        b.info = info
        b.hasDesc = hasDesc
        this.hasDesc = hasDesc

//        b.btnMore.onClick {
//            /**  adType 상세설명 오픈  */
//            selectedAdType?.let { MyAlert.showAdTypeManual(context, it) }
//        }

        /***  set Radio Group  */
        setRadioGroup(dataType)

        /***  라디오 리스너 세팅 */
        listen1 = RadioGroup.OnCheckedChangeListener { _, id ->
            if (id != -1) {
                b.rg2.setOnCheckedChangeListener(null)
                b.rg2.clearCheck()
                b.rg2.setOnCheckedChangeListener(listen2)
                setSelectedAdType(id, onClickRb)
            }
        }

        listen2 = RadioGroup.OnCheckedChangeListener { _, id ->
            if (id != -1) {
                b.rg1.setOnCheckedChangeListener(null)
                b.rg1.clearCheck()
                b.rg1.setOnCheckedChangeListener(listen1)
                setSelectedAdType(id, onClickRb)
            }
        }

        b.rg1.setOnCheckedChangeListener(listen1)
        b.rg2.setOnCheckedChangeListener(listen2)

    }

    fun onClickManual(v: View) {
        OnMyClick.setDisableAWhileBTN(v)
        /**  adType 상세설명 오픈  */
        selectedAdType?.let { MyAlert.showAdTypeManual(context, it) }
    }


    /**
     * 광고종류 셋팅
     */
    private fun setRadioGroup(dataType: DataType) {

        b.rg1.addView(newRadio(AdType.TOP_TOTAL_BOARD))
        b.rg1.addView(newRadio(AdType.TOP_BOARD))
        b.rg1.addView(newRadio(AdType.TOP_FLEA))
        b.rg1.addView(newRadio(AdType.TOP_ROOM))

        if (isAdmin()) {
            if (dataType == DataType.FLEA || dataType == DataType.ALL) b.rg1.addView(newRadio(AdType.RANK_FLEA))
            if (dataType == DataType.ROOM || dataType == DataType.ALL) b.rg1.addView(newRadio(AdType.RANK_ROOM))
        }

        b.rg2.addView(newRadioBig(AdType.MENU))
        b.rg2.addView(newRadioBig(AdType.MAIN))
        if (dataType == DataType.STORE || dataType == DataType.FLEA || dataType == DataType.ROOM || dataType == DataType.ALL) b.rg2.addView(newRadio(AdType.AD_CREDIT))   // set AdCredit

    }

    private fun setSelectedAdType(id: Int, onClickRb: (AdType) -> Unit) {
//        id.getAdTypeFromResID().apply {

        val adType = id.getAdtype()

        /*** 일반유저 */
        if (isNotAdmin(null, null)) {
            when (adType) {
                AdType.MAIN -> if (notAllowAdMain()) return
                AdType.MENU -> if (notAllowAdMenu()) return
                AdType.RANK_FLEA, AdType.RANK_ROOM -> {
                    OnDDD.info(context, R.string.info_prepare_developing)
                    b.rg1.clearCheck()
                    b.rg2.clearCheck()
                    return
                }
                AdType.TOP_TOTAL_BOARD, AdType.TOP_BOARD, AdType.TOP_FLEA, AdType.TOP_ROOM -> if (notAllowAdTop()) return
                else -> {
                }
            }
        }

        adType.apply {
            selectedAdType = this

            /** 설명창 열기*/
            if (hasDesc) {
                b.tvAdtypeDesc.text = this.info
                b.tvAdtypeDesc.showSlideUp(true)
                b.tvAdtypeDesc.visi()
                b.btnMore.visi()
            }

            /** AdType 리턴 */
            onClickRb(this)
        }
    }

    private fun newRadio(adType: AdType) =
            ViewRadioBoxBinding.inflate(LayoutInflater.from(context))
                    .apply {
                        this.rb.id = adType.no
                        this.title = adType.title
                    }.root

    private fun newRadioBig(adType: AdType) =
            ViewRadioBox2Binding.inflate(LayoutInflater.from(context))
                    .apply {
                        this.rb.id = adType.no
                        this.title = adType.title
                    }.root

//    private fun getAdType(id: Int) =
//            when (id) {
//                R.id.rb_main -> AdType.BANNER_MAIN
//                R.id.rb_drawer -> AdType.BANNER_MAIN
//                R.id.rb_first_flea -> AdType.FIRST_FLEA
//                R.id.rb_first_room -> AdType.FIRST_ROOM
//                R.id.rb_total -> AdType.TOP_TOTAL_BOARD
//                R.id.rb_post -> AdType.TOP_BOARD
//                R.id.rb_flea -> AdType.TOP_FLEA
//                R.id.rb_room -> AdType.TOP_ROOM
//                R.id.rb_ad_credit -> AdType.AD_CREDIT
//                else -> AdType.NOTHING
//            }


}