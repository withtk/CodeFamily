package com.tkip.mycode.ad.my_banner

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.RecyclerView
import com.tkip.mycode.R
import com.tkip.mycode.ad.BannerType
import com.tkip.mycode.ad.OnBanner
import com.tkip.mycode.ad.getBannerType
import com.tkip.mycode.databinding.HolderBannerMainBinding
import com.tkip.mycode.databinding.HolderBannerMenuBinding
import com.tkip.mycode.databinding.HolderBannerTopBinding
import com.tkip.mycode.dialog.OnDDD
import com.tkip.mycode.dialog.progress.WAIT_TYPE
import com.tkip.mycode.dialog.toast.toastNormal
import com.tkip.mycode.init.ACTION_ADMIN
import com.tkip.mycode.init.ACTION_UPDATE
import com.tkip.mycode.init.EES_BANNER
import com.tkip.mycode.init.inter.interBid
import com.tkip.mycode.init.inter.interHolderBasic
import com.tkip.mycode.model.bid.Bid
import com.tkip.mycode.model.grant.OnGrant
import com.tkip.mycode.model.my_enum.Status
import com.tkip.mycode.model.my_enum.getStatus
import com.tkip.mycode.model.my_enum.ltNormalStatus
import com.tkip.mycode.model.peer.isAdmin
import com.tkip.mycode.util.tools.anim.hide
import com.tkip.mycode.util.tools.etc.OnMyClick
import com.tkip.mycode.util.tools.fire.*
import java.util.*

/**
 * 1. Banner list - HORI.MAIN, HORI.MENU, VERT.TOP
 * 1. BidManagerActivity
 * manage : 유저가 수정할 때
 */
class BannerAdapter(val items: ArrayList<Bid>, val isAdmin: Boolean, val manage: Boolean, val onItemClick: ((Bid) -> Unit)?) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    private lateinit var context: Context
    private val rvBannerMain = 10
    private val rvBannerMenu = 20
    private val rvBannerTop = 30

    override fun getItemViewType(position: Int): Int {
        val bid = items[position]
        return when (bid.bannerTypeNo.getBannerType()) {
            BannerType.MAIN -> rvBannerMain
            BannerType.MENU -> rvBannerMenu
            BannerType.TOP -> rvBannerTop
            else -> rvBannerMain
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        context = parent.context
        return when (viewType) {
            rvBannerMain -> BannerMainViewHolder(HolderBannerMainBinding.inflate(LayoutInflater.from(parent.context), parent, false))
            rvBannerMenu -> BannerMenuViewHolder(HolderBannerMenuBinding.inflate(LayoutInflater.from(parent.context), parent, false))
            rvBannerTop -> BannerTopViewHolder(HolderBannerTopBinding.inflate(LayoutInflater.from(parent.context), parent, false))
            else -> BannerMainViewHolder(HolderBannerMainBinding.inflate(LayoutInflater.from(parent.context), parent, false))
        }
    }

    override fun onBindViewHolder(h: RecyclerView.ViewHolder, p: Int) {
        when (getItemViewType(p)) {
            rvBannerMain -> (h as BannerMainViewHolder).bind(items[p])
            rvBannerMenu -> (h as BannerMenuViewHolder).bind(items[p])
            rvBannerTop -> (h as BannerTopViewHolder).bind(items[p])
            else -> (h as BannerMainViewHolder).bind(items[p])
        }
    }

    override fun getItemId(position: Int): Long {
        return items[position].hashCode().toLong()
    }

    override fun getItemCount(): Int {
        return items.size
    }

    /******************************************************************************************
     ***** common *****************************************************************************/

    private fun commonUpdateBanner(v: View, banner: Bid, p: Int) {
        OnMyClick.setDisableAWhileBTN(v)
        /** bid 승인 요청 가능한 상태인지 체크. */
        if (!OnGrant.availRequestGrant(context, banner)) {
            /*** 알림 show */
            OnDDD.confirm(context, R.string.ask_grant_delete_model, ok = { commonStatusUpdate(EES_BANNER, banner.key, Status.DELETED_USER, success = { notifyItemRemoved(p) }, fail = { }) }, cancel = {})
            return
        }
        OnBanner.openADDBannerDialog(context as AppCompatActivity, if (isAdmin) ACTION_ADMIN else ACTION_UPDATE, banner, onOk = { b,_->notifyItemChanged(p, b) })
    }

    private fun commonRequestGrant(banner: Bid, onSuccess: () -> Unit) {
        /*** 승인요청 버튼 */
        OnGrant.requestBanner(context, banner, onSuccess)         // 승인 요청.
    }

    private fun commonDel(v: View, banner: Bid, p: Int) {
        OnMyClick.setDisableAWhileBTN(v)
        OnDDD.delete(context,
                ok = {
                    WAIT_TYPE.FULL_CIRCLE.show(context)
                    Firee.deleteBanner(banner, success = {
                        items.removeAt(p)
                        notifyItemRemoved(p)
                        toastNormal(R.string.complete_delete)
                    }, fail = {})
                }, cancel = {})
    }

    private fun commonDeepDel(v: View, banner: Bid, p: Int) {
        OnMyClick.setDisableAWhileBTN(v)

        if (isAdmin()) {
            OnDDD.deepDel(context, ok = {
                batch().apply {
                    delBanner(banner.key)
                    delGrant(banner.key)
                }.myCommit("banner commonDeepDel",
                        success = {
                            items.removeAt(p)
                            notifyItemRemoved(p)
                            toastNormal(R.string.complete_delete)
                        }, fail = {})
//            Firee.deepDelBanner(context, banner, success = {
//                items.removeAt(p)
//                notifyItemRemoved(p)
//                toastNormal(R.string.complete_delete)
//            }, fail = {})
            }, cancel = {})
        }
    }


    /*******************************************************************************************
     *  banner TOP
     *********************************************************************************************/
    inner class BannerTopViewHolder(val b: HolderBannerTopBinding) : RecyclerView.ViewHolder(b.root) {

        private lateinit var banner: Bid

        fun bind(banner: Bid) {
            this.banner = banner
            b.banner = banner
            b.manage = manage
            b.inter = interHolderBasic(
                    null,
                    onClickGo = {
                        /*** 승인요청 버튼 */
                        commonRequestGrant(banner, onSuccess = { notifyItemChanged(adapterPosition) })
//                        MyAlert.showRequestGrant(context,banner, onSuccess = {}, onFail = {}) // 승인 요청.
                    },
                    onClickUpdate = { commonUpdateBanner(it, banner, adapterPosition) },
                    onClickDel = { commonDel(it, banner, adapterPosition) },
                    onDeepDel = { commonDeepDel(it, banner, adapterPosition) },
                    onClickStatus = null
            )

            b.inBannerTop.adSign = manage
            b.inBannerTop.bid = banner
            b.inBannerTop.inter = interBid {
                if (manage) commonUpdateBanner(it, this.banner, adapterPosition)
                else onItemClick?.let { it(this.banner) }
            }

            b.executePendingBindings()

            /***  reason 셋팅 */
            when (banner.statusNo.getStatus()) {
                Status.BANNER_REQUESTED -> b.tvReason.hide()
                Status.BANNER_GRANTED -> b.tvReason.hide()
                else -> OnGrant.getGrant(banner) { b.grant = it }
            }

        }
    }


    /*******************************************************************************************
     *  banner MENU
     *********************************************************************************************/
    inner class BannerMenuViewHolder(val b: HolderBannerMenuBinding) : RecyclerView.ViewHolder(b.root) {
        private lateinit var banner: Bid

        fun bind(banner: Bid) {
            this.banner = banner
            b.banner = banner
            b.manage = manage
            b.inter = interHolderBasic(
                    null,
                    onClickGo = {
                        /*** 승인요청 버튼 */
                        commonRequestGrant(banner, onSuccess = { notifyItemChanged(adapterPosition) })
//                        OnGrant.request(context, banner, onSuccess = { b.btnGrant.gone() })         // 승인 요청.
//                        MyAlert.showRequestGrant(context, banner, onSuccess = {}, onFail = {}) // 승인 요청.
                    },
                    onClickUpdate = { commonUpdateBanner(it, banner, adapterPosition) },
                    onClickDel = { commonDel(it, banner, adapterPosition) },
                    onDeepDel = { commonDeepDel(it, banner, adapterPosition) },
                    onClickStatus = null
            )

            b.inBannerMenu.adSign = manage
            b.inBannerMenu.bid = banner
            b.inBannerMenu.inter = interBid {
                if (manage) commonUpdateBanner(it, banner, adapterPosition)
                else onItemClick?.let { it(banner) }
            }

            b.executePendingBindings()

            /*** reason 셋팅 */
            b.tvReason.hide()
            if (banner.ltNormalStatus()) OnGrant.getGrant(banner) { b.grant = it }
        }

    }


    /**********************************************************************************************
     *  banner MAIN
     *********************************************************************************************/
    inner class BannerMainViewHolder(val b: HolderBannerMainBinding) : RecyclerView.ViewHolder(b.root) {
        private lateinit var banner: Bid

        fun bind(banner: Bid) {
            this.banner = banner
            b.banner = banner
            b.manage = manage
            b.inter = interHolderBasic(
                    null,
                    onClickGo = {
                        /*** 승인요청 버튼 */
                        commonRequestGrant(banner, onSuccess = { notifyItemChanged(adapterPosition) })
//                        OnGrant.request(context, banner, onSuccess = { b.btnGrant.gone() })         // 승인 요청.
//                        MyAlert.showRequestGrant(context, banner, onSuccess = {}, onFail = {}) // 승인 요청.
                    },
                    onClickUpdate = { commonUpdateBanner(it, this.banner, adapterPosition) },
                    onClickDel = { commonDel(it, this.banner, adapterPosition) },
                    onDeepDel = { commonDeepDel(it, this.banner, adapterPosition) },
                    onClickStatus = null
            )


            b.inBannerMain.adSign = manage
            b.inBannerMain.bid = this.banner
            b.inBannerMain.inter = interBid {
                /** manage 때는 수정
                 *  select 때는 bid 전달 */
                if (manage) commonUpdateBanner(it, this.banner, adapterPosition)
                else onItemClick?.let { it(this.banner) }
            }

            b.executePendingBindings()

            /*** reason 셋팅 */
            b.tvReason.hide()
            if (banner.ltNormalStatus()) OnGrant.getGrant(banner) { b.grant = it }
        }

    }
}