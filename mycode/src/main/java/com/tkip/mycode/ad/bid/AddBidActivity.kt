package com.tkip.mycode.ad.bid

import android.os.Build
import android.os.Bundle
import android.os.Handler
import android.view.View
import android.widget.LinearLayout
import androidx.annotation.LayoutRes
import com.tkip.mycode.R
import com.tkip.mycode.ad.AdType
import com.tkip.mycode.ad.OnBid
import com.tkip.mycode.ad.biddingInValid
import com.tkip.mycode.ad.setModel
import com.tkip.mycode.databinding.ActivityAddBidBinding
import com.tkip.mycode.dialog.OnDDD
import com.tkip.mycode.dialog.alert.MyAlert
import com.tkip.mycode.dialog.progress.WAIT_TYPE
import com.tkip.mycode.dialog.toast.toastAlert
import com.tkip.mycode.dialog.toast.toastNormal
import com.tkip.mycode.funs.common.lllogI
import com.tkip.mycode.funs.common.remoteStr
import com.tkip.mycode.funs.custom.expandable.ViewExpandableCardAd
import com.tkip.mycode.funs.custom.manual.LinearManualMore
import com.tkip.mycode.funs.custom.text.ViewTvInfo
import com.tkip.mycode.funs.custom.toolbar.ViewToolbarEmpty
import com.tkip.mycode.funs.search.view.CustomSearchAll4Bid
import com.tkip.mycode.init.ATTACH_NO
import com.tkip.mycode.init.EES_BID
import com.tkip.mycode.init.PUT_BID
import com.tkip.mycode.init.base.getStr
import com.tkip.mycode.init.base.getStrTerm
import com.tkip.mycode.init.base.getStringComma
import com.tkip.mycode.init.base.setAlign
import com.tkip.mycode.init.my_bind.BindingActivityPhoto
import com.tkip.mycode.model.bid.Bid
import com.tkip.mycode.model.cost.Cost
import com.tkip.mycode.model.my_enum.*
import com.tkip.mycode.util.lib.photo.glide.setUrlGlide
import com.tkip.mycode.util.my_view.ViewUtil
import com.tkip.mycode.util.tools.anim.*
import com.tkip.mycode.util.tools.etc.OnMyClick
import com.tkip.mycode.util.tools.fire.addCost
import com.tkip.mycode.util.tools.fire.batch
import com.tkip.mycode.util.tools.fire.docRef
import com.tkip.mycode.util.tools.fire.myCommit
import com.tkip.mycode.util.tools.radio.CustomRGbasic
import java.util.*


class AddBidActivity : BindingActivityPhoto<ActivityAddBidBinding>() {
    @LayoutRes
    override fun getLayoutResId() = R.layout.activity_add_bid

    private val bid by lazy { intent.getParcelableExtra<Bid>(PUT_BID) }
    private lateinit var linearManual: LinearManualMore      // 동의사항 -----
    private var manualHeight = 60  // 스크롤 이동시 필요.

    private lateinit var finalBid: Bid
    private lateinit var date8List: ArrayList<Int>

    private lateinit var vTitleModel: ViewExpandableCardAd     // 모델검색 -----
    private lateinit var vTitleAdType: ViewExpandableCardAd    // 광고종류 -----

    //    private lateinit var vTitleCheck: ViewExpandableCardAd   // 입찰현황 -----
//    private lateinit var vConCalendar: SubTermsCredit        // 달력 -----
//    private lateinit var vTitleCredit: ViewExpandableCardAd  // 입찰크레딧 -----
    private lateinit var vTitleBanner: ViewExpandableCardAd    // 배너만들기 -----
    private lateinit var vConBanner: SubCreateBanner           // 배너만들기 -----
    private lateinit var vTitleTerm: ViewExpandableCardAd      // 기간설정 -----
    private var vBills: SubBidBills? = null                    // 계산서 -----

    private var chosenModel: Any? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        finalBid = bid?.let { Bid(bid) } ?: Bid()
        initView()
        initData()

        /********************* superListener *****************************/
//        listenerActivityResult = interActivityResult(
//                onPick = null, onAlbum = null, onMap = null,
//                onBanner = { banner ->
//                    lllogI("CustomMyBannerList    addbidacti  result banner $banner")
//
//                    finalBid.setBanner(banner!!)
//                    vConBanner.setBid()
//                }, onData = null)

    }

    override fun onBackPressed() {
        lllogI("onBackPressed finalbid $finalBid")
        if (finalBid.modelKey != null) MyAlert.showConfirm(this@AddBidActivity, getString(R.string.info_wanna_leave_add_common), ok = { super.onBackPressed() }, cancel = {})
        else super.onBackPressed()
    }

    private fun initView() {
        b.activity = this


        /** Toolbar */
        ViewToolbarEmpty(this@AddBidActivity, R.string.title_add_ad.getStr(), onBack = { onBackPressed() }, onMore = null).apply { this@AddBidActivity.b.frameToolbar.addView(this) }

        /*** 매뉴얼  */
        linearManual = LinearManualMore(this@AddBidActivity, R.string.remote_agree_bid.remoteStr().setAlign(), onAgree = {})
        b.lineCover.addView(linearManual, 0)
        manualHeight = linearManual.b.lineManual.height

//        b.btnMoreInfo.onClick {
//            MyAlert.showManualMore(this@AddBidActivity, R.string.title_manual_advertisement.getStr(), R.string.remote_agree_bid.remoteStr())
//        }

        /******  광고할 게시물 선택 *************************************************************/
        vTitleModel = ViewUtil.addViewExpandableCardAd(this@AddBidActivity, R.string.title_make_ad_model.getStr(), b.lineCover)
        /***  광고종류 *******************************************************************/
        vTitleAdType = ViewUtil.addViewExpandableCardAd(this@AddBidActivity, R.string.title_ad_type.getStr(), b.lineCover)
        /***  배너만들기 ****************************************************************/
        vTitleBanner = ViewUtil.addViewExpandableCardAd(this@AddBidActivity, R.string.title_make_ad_banner.getStr(), b.lineCover)
        /***  기간 설정 ****************************************************************/
        vTitleTerm = ViewUtil.addViewExpandableCardAd(this@AddBidActivity, R.string.title_ad_term.getStr(), b.lineCover)
        /********************************************************************************/
    }

//    override fun onSaveInstanceState(outState: Bundle) {
//        super.onSaveInstanceState(outState)
//        outState.putParcelable(PUT_RESULT_OBJECT, finalBid)
//    }
//
//    override fun onRestoreInstanceState(save: Bundle) {
//        super.onRestoreInstanceState(save)
//        save.getParcelable<Bid>(PUT_RESULT_OBJECT)?.let { Bid1 ->
//            finalBid = bid1
//            initData()
//        } ?: finish()
//    }

    private fun initData() {
        if (Build.VERSION.SDK_INT > Build.VERSION_CODES.N) {
            Handler().postDelayed({
                vTitleModel.toggle()  //   열기
//            b.rbAdCredit.isChecked = true
            }, 500)
        }

        startVmodel()

    }

    /**     * child View 삭제     */
    private fun LinearLayout.removeMyViewAt(i: Int) {
        if (this.getChildAt(i) != null) this.removeViewAt(i)
    }

    private fun nestedScrollTo(vTitle: LinearLayout) {
        val height = when (vTitle) {
            vTitleModel -> linearManual.b.lineManual.height
            vTitleAdType -> linearManual.b.lineManual.height + vTitleModel.height
            vTitleBanner -> linearManual.b.lineManual.height + vTitleModel.height + vTitleAdType.height
            vTitleTerm -> linearManual.b.lineManual.height + vTitleModel.height + vTitleAdType.height + vTitleBanner.height
            else -> linearManual.b.lineManual.height
        }
        b.nestedScrollView.smoothScrollTo(0, height)
    }

    private fun ViewExpandableCardAd.reset() {
        when (this) {
            vTitleModel ->
                vTitleModel.b.apply {
                    AnimUtil.hideNormal(tvResult1, tvResult2, iv1, tvResultTitle)
                    finalBid.apply {
                        dataTypeNo = null
                        modelKey = null
                        modelTitle = null
                        modelThumb = null

                        title = null
                        comment = null
                        photoThumb = null
                        photoUrl = null
                    }
                    vTitleAdType.apply {
                        b.lineContents.removeAllViews()
                        reset()
                        close()
                    }
                }
            vTitleAdType -> vTitleAdType.b.apply {
                AnimUtil.hideNormal(tvResult1, tvResult2, iv1, tvResultTitle)
                finalBid.apply {
                    adTypeNo = null
                    boardKey = null
                    boardName = null
                }

                vTitleBanner.apply {
                    b.lineContents.removeAllViews()
                    reset()
                    close()
                }
            }

            vTitleBanner -> vTitleBanner.b.apply {
                AnimUtil.hideNormal(tvResult1, tvResult2, iv1, tvResultTitle)
//                    finalBid.apply {
//                        title = null
//                        comment = null
//                        photoThumb = null
//                        photoUrl = null
//                }

                vTitleTerm.apply {
                    b.lineContents.removeAllViews()
                    reset()
                    close()
                }
            }
            vTitleTerm -> vTitleTerm.b.apply {
                AnimUtil.hideNormal(tvResult1, tvResult2, iv1, tvResultTitle)
                finalBid.apply {
                    amount = 0
                    term = 0
                    startDate = 0
                    endDate = 0
                }


                vBills?.b?.line?.gone()
            }
        }
    }

    /************************************************************************************************/
    /******  sttModel 광고할 게시물 선택 :   dataTypeNo, modelkey,   ***********************************/
    private fun startVmodel() {

        vTitleModel.b.apply {
            /**         * 안내         */
            lineContents.addView(ViewTvInfo(this@AddBidActivity, R.string.title_make_ad_model_manual.getStr()))

            /*** Model 선택 라디오 삽입 */
            val rgModelList = arrayOf(DataType.BOARD, DataType.POST, DataType.FLEA, DataType.ROOM, DataType.STORE)
            val list = arrayListOf<String>().apply { rgModelList.forEach { add(it.title) } }
            CustomRGbasic(this@AddBidActivity, list, true,
                    onSelected = { listIndex ->
                        vTitleModel.reset()

                        /*** 검색창 붙이기 (내 데이타) */
                        vTitleModel.b.lineContents.removeMyViewAt(2)
                        CustomSearchAll4Bid(this@AddBidActivity, null, true, rgModelList[listIndex],
                                onSelected = { any ->
                                    vTitleModel.reset()
                                    chosenModel = any
                                    completeVmodel(any)
                                }).apply { lineContents.addView(this) }

                        nestedScrollTo(vTitleModel)
                    }).apply { lineContents.addView(this) }
        }
    }

    private fun completeVmodel(any: Any) {
        finalBid.setModel(any)

        /*** 모델선택 완료표시 */
        vTitleModel.b.apply {
            tvResult1.showOverUp(false)
            iv1.setUrlGlide(any.anyThumb(0))
            tvResultTitle.text = any.anyTitle()
            tvResultTitle.visi()
        }
        startVadType()
//        nestedScrollTo(vTitleAdType)
        vTitleModel.close()

        lllogI("#####addAdActivity completeVmodel finalbid $finalBid")
    }

    /******************************************************************************************************/
    /***  sttAdtype 광고종류 : adType, bannerTypeNo, boardKey, boardName  *********************************/
    private fun startVadType() {
        /**     * 광고종류 : RG 붙이기 & 오픈     */
//        vTitleAdType.b.lineContents.removeAllViews()
        SubSelectAdType(this@AddBidActivity, R.string.title_ad_type_manual.getStr(), true, finalBid.dataTypeNo!!.dataType(),
                onClickRb = { adType ->

                    vTitleAdType.reset()

                    lllogI("#####addAdActivity ViewRGadType adType: $adType finalbid $finalBid")

                    // Board 검색창 붙이기
                    vTitleAdType.b.lineContents.removeMyViewAt(1)
                    if (adType == AdType.TOP_BOARD) {
                        CustomSearchAll4Bid(this@AddBidActivity, R.string.info_search_board.getStr(), false, DataType.BOARD,
                                onSelected = { completeVAdType(adType, it) })
                                .apply { vTitleAdType.b.lineContents.addView(this) }
                    } else completeVAdType(adType)
                })
                .apply {
                    vTitleAdType.b.lineContents.addView(this)
                    vTitleAdType.open()  //   열기
                }
    }

    /** 개별보드 광고선택용 */
    private fun completeVAdType(adType: AdType, any: Any) {
        finalBid.adTypeNo = adType.no
        finalBid.bannerTypeNo = adType.bannerType.no
        finalBid.boardKey = any.anyKey()
        finalBid.boardName = any.anyTitle()

        /*** 광고위치 보드 완료표시 */
        vTitleAdType.b.apply {
            tvResult1.showOverUp(false)
            tvResult2.text = adType.title
            tvResult2.visi()

            iv1.setUrlGlide(any.anyThumb(0))
            tvResultTitle.text = any.anyTitle()
            tvResultTitle.visi()

            vTitleAdType.close()
        }

        startVbanner(adType)

        lllogI("#####addAdActivity completeVAdType finalbid $finalBid")
    }

    /** 전체 광고용 */
    private fun completeVAdType(adType: AdType) {
        finalBid.adTypeNo = adType.no
        finalBid.bannerTypeNo = adType.bannerType.no

        /*** 광고위치 보드 완료표시 */
        vTitleAdType.b.apply {
            tvResult1.showOverUp(false)
            tvResult2.text = adType.title
            tvResult2.visi()
        }
        startVbanner(adType)
        lllogI("#####addAdActivity completeVAdType finalbid $finalBid")
    }

    /************************************************************************************************/
    /***  sttBanner 배너만들기 : title, comment, comment2, photoUrl, photoThumb **********************/
    private fun startVbanner(adType: AdType) {
        vTitleBanner.b.lineContents.removeAllViews()

        if (adType.isNeedBanner()) {

            /**   * 안내   */
//            vTitleBanner.b.lineContents.addView(viewTvInfo(R.string.title_make_ad_banner_manual))

            /**   * 배너 만들기  */
            vConBanner = SubCreateBanner(this@AddBidActivity, finalBid,
                    onFinish = { nestedScrollTo(vTitleBanner) },
                    onOk = { completeVBanner() })
                    .apply {
                        vTitleBanner.b.lineContents.addView(this)
                        vTitleBanner.open()  //   열기
                    }

            /** 배너없는 광고 */
        } else {
            /**   * 안내   */
            vTitleBanner.b.lineContents.addView(ViewTvInfo(this@AddBidActivity, R.string.title_make_ad_banner_no.getStr()))
//            completeVBanner()

            /*** 배너 완료표시 */
            vTitleBanner.b.tvResult1.showOverUp(false)
            vTitleModel.close()
            startVTerm()
//            vTitleAdType.close()
//            vTitleBanner.close()
//            nestedScrollTo(vTitleModel)
        }

    }

    private fun completeVBanner() {
        /** banner 내용기입 : banner내용이 널이면 model의 데이타를 쓰라.*/

        /*** 배너 완료표시 */
        vTitleBanner.b.tvResult1.showOverUp(false)

        vTitleModel.close()
        vTitleAdType.close()
        vTitleBanner.close()
        nestedScrollTo(vTitleBanner)
        startVTerm()

        lllogI("#####addAdActivitycompleted finalbid $finalBid")
    }


    /******************************************************************************************************/
    /***  sttTerm 기간 설정 : term, amount 세팅**************************************************************/
    private fun startVTerm() {
        vTitleTerm.b.lineContents.removeAllViews()


        /**  * 캘린더   */
        SubTermsCredit(this@AddBidActivity, finalBid,
                onOpenRange = { },
                onOpenPicker = { },
                onSelected = { selList ->

                    lllogI("SubTermsCredit selList ${selList?.size}")

                    selList?.let { date8List = it }    // 전체 날짜 리스트 가져오기
                    /** bid term 기입 */
                    finalBid.term = selList?.size ?: 0
                    vTitleTerm.b.tvResult1.text = (finalBid.term ?: 0).getStrTerm()


                    /*** size가 0이면 title 내역 hide */
                    if (finalBid.term ?: 0 > 0) vTitleTerm.b.tvResult1.showOverUp(false)
                    else vTitleTerm.b.tvResult1.hideOverDown(false)

                    completeVTerm()
                },
                onCompleted = { amount ->
                    /** bid amount 기입 */
                    finalBid.amount = amount
                    completeVTerm()
                })
                .apply {
                    vTitleTerm.b.lineContents.addView(this)
                    vTitleTerm.open()
                }
    }

    private fun completeVTerm() {
        if (finalBid.amount != 0) {
            if (finalBid.term ?: 0 > 0) {
                finalBid.startDate = date8List[0]
                finalBid.endDate = date8List[date8List.size - 1]

                /***  amount 작성완료시 */
                vTitleTerm.b.apply {
                    tvResult1.text = (finalBid.term ?: 0).getStrTerm()
                    tvResult1.showOverUp(false)
                    tvResult2.text = (finalBid.amount ?: 0).getStringComma()
                    tvResult2.visi()
                }
                addBills()

                vTitleModel.close()
                vTitleAdType.close()
                vTitleBanner.close()
                vTitleTerm.close()
//                nestedScrollTo(vTitleModel)

                lllogI("#####addAdActivitycompleted finalbid $finalBid")
            } else toastNormal(R.string.title_ad_term_check_status)
        } else toastNormal(R.string.title_ad_term_manual_amount)


    }

    /****************************************************************************************************/
    /***  sttBill 최종결제내역  : paidBonusCredit, paidCredit, amountBack ****************************************/

    private fun addBills() {

        /**
         *  banner : main, menu는 이미지 필수.
         * 입찰할때 key는 무조건 새로 만들어서 들어가야..  내 배너와 겹칠 우려 있음.
         */

        /**         * 계산서 만들기         */
        vBills?.setReceipt(finalBid)
                ?: let {
                    vBills = SubBidBills(this@AddBidActivity, finalBid)
                    b.lineCover.addView(vBills)
                }

//        vBills = CustombidBills(this@AddAdActivity, finalbid)
//        b.frameBill.removeAllViews()
//        b.frameBill.addView(vBills)

        b.btnSubmit.isEnabled = true
    }

    /****************************************************************************************************/
    /***  업로드 직전  : hitDate  ****************************************************************************/

    fun onClickSubmit(v: View) {
        OnMyClick.setDisableAWhileBTN(v)
        lllogI("onClickSubmit finalbid $finalBid")

        /** 동의 체크 */
        if (!linearManual.b.cbAgree.isChecked) {
            b.nestedScrollView.fullScroll(View.FOCUS_UP)
            toastAlert(R.string.alert_agree_bid)
            return
        }

        /** 결제 가능여부 체크 */
        if (OnBid.notPossiblePayment(vBills)) return


        /** 결제수단 선택 체크 */
//        if (vBills!!.b.lineBtn.isVisi()) {
//            toastAlert(R.string.error_select_payment)
//            return
//        }


        /** 어드민이 기본광고 업로드 할 때 */
//        if (OnPeer.isAdmin(false) && date8List[0] == HIT_DATE_DEFAULT_BID) {
//            MyAlert.showConfirm(this@AddBidActivity, R.string.info_add_basic_ad.getStr(),
//                    ok = {
//                        /** 체크 */
//                        if (finalBid.biddingInValid() || OnBid.notPossiblePayment(vBills)) return@showConfirm
//                        addBid()
//                    }, cancel = {})
//            return
//        }

        MyAlert.showConfirm(this@AddBidActivity, R.string.info_wanna_upload_add_bidding.getStr(),
                ok = {

                    /** 체크 */
                    if (finalBid.biddingInValid() || OnBid.checkValidDate(this@AddBidActivity, date8List) || OnBid.notPossiblePayment(vBills)) return@showConfirm

                    /** AD_CREDIT은  하한가 체크 불요 */
                    if (finalBid.adTypeNo == AdType.AD_CREDIT.no) {
                        addAdCredit()
                    } else {
                        /** 하한가 체크 */
                        OnBid.moreThanLowestBid(date8List, finalBid) { ok ->
                            if (ok) addBid()
                            else OnDDD.info(this@AddBidActivity, R.string.error_lowest_bidding)
                        }
                    }

                }, cancel = {})

    }


    private fun addAdCredit() {
        WAIT_TYPE.FULL_AV.show(this@AddBidActivity)
        batch().apply {

            /*** 1. add bid
             *
             * functions :
             *       1. 매일 12시에 당일 model.adCredit, totalAdCredit 변경. (store, flea, room)
             */

            val mainKey = finalBid.key  // key+숫자 : 새로 만들 기준키.
            //                                finalbid.amount = -finalbid.amount   // 마이너스로 바꾸기.
            date8List.forEachIndexed { i, date ->
                finalBid.key = mainKey + ATTACH_NO + i
                finalBid.hitDate = date
                finalBid.statusNo = Status.BID_BIDDING.no
//                this.addCost(Cost.bid(finalBid))    // add cost
                set(docRef(EES_BID, finalBid.key), finalBid)    // add bid
            }

            finalBid.amount = date8List.size * (finalBid.amount ?: 0)
            addCost(Cost.bid(finalBid))   // add cost 한개로 일괄.
//            addAct(Cost.actBid(ActType.BID_ADCREDIT_ADD, finalBid))   // add cost 한개로 일괄.

            //                                val cost = Cost.create(null,ActType.ADD_BID,)

            myCommit("addAdCredit",
                    success = {
                        toastNormal(R.string.complete_adcredit)
                        finish()
                    }, fail = { })
        }
    }


    private fun addBid() {

//        toastNormal("조금만 더 기다려줘영...")
//        return


        WAIT_TYPE.FULL_AV.show(this@AddBidActivity)
        batch().apply {
            /**
             * 1. add bid (모든 개수마다)
             * 2. add cost(통합)
             * 3. add act(통합)
             *
             * functions :
             *       1. 유찰 bid.statusNo 수정.
             *       2. 유찰 cost add.
             *       3. peer.credit 수정.
             */
            val mainKey = finalBid.key  // key+숫자 : 새로 만들 기준키.
            //                                finalbid.amount = -finalbid.amount   // 마이너스로 바꾸기.
            date8List.forEachIndexed { i, date ->
                finalBid.key = mainKey + ATTACH_NO + i
                finalBid.hitDate = date
                finalBid.statusNo = Status.BID_BIDDING.no
//                this.addCost(Cost.bid(finalBid))    // add cost

                set(docRef(EES_BID, finalBid.key), finalBid)    // add bid
            }

            finalBid.amount = date8List.size * (finalBid.amount ?: 0)
            addCost(Cost.bid(finalBid))   // add cost 한개로 일괄.
//            addAct(Cost.actBid(ActType.BID_ADD, finalBid))   // add cost 한개로 일괄.

            //                                val cost = Cost.create(null,ActType.ADD_BID,)

            myCommit("addBid",
                    success = {
                        toastNormal(R.string.complete_bidding)
                        finish()
                    }, fail = { })
        }
    }


}
