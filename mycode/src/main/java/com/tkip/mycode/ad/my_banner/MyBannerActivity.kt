package com.tkip.mycode.ad.my_banner

import android.os.Bundle
import android.view.View
import androidx.annotation.LayoutRes
import androidx.fragment.app.Fragment
import com.tkip.mycode.R
import com.tkip.mycode.ad.BannerType
import com.tkip.mycode.ad.OnBanner
import com.tkip.mycode.ad.getBannerType
import com.tkip.mycode.ad.getBannerTypeFromPosition
import com.tkip.mycode.databinding.ActivityMyBannerBinding
import com.tkip.mycode.funs.common.onPageChange
import com.tkip.mycode.init.ACTION_NEW
import com.tkip.mycode.init.PUT_MANAGE
import com.tkip.mycode.init.PUT_UID
import com.tkip.mycode.init.base.getStr
import com.tkip.mycode.init.inter.interToolbarIcon
import com.tkip.mycode.init.my_bind.BindingActivity
import com.tkip.mycode.model.bid.Bid
import com.tkip.mycode.util.tools.etc.OnMyClick
import com.tkip.mycode.util.tools.view_pager.OnPager

class MyBannerActivity : BindingActivity<ActivityMyBannerBinding>() {
    @LayoutRes
    override fun getLayoutResId() = R.layout.activity_my_banner

    private val uid by lazy { intent?.getStringExtra(PUT_UID) }    // null이면 for admin 모든 배너 리스트
    private val manage by lazy { intent.getBooleanExtra(PUT_MANAGE, false) }
    private lateinit var fragList: ArrayList<Pair<Fragment, String>>
    private var currentBannerType = BannerType.TOP  // viewPager Position

//    private val rvType by lazy { intent?.getIntExtra(PUT_RV_TYPE, RV_MY_CONTENTS) ?: RV_MY_CONTENTS }
//    private val bannerTypeNo by lazy { intent.getIntExtra(PUT_BANNER_NO, 0) }  // 0이면 manage모드
//    private var typeNo: Int? = BannerType.TOP.no    // bannerTypeNo를 null로 대입하기 위해.

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        initView()
    }

    private fun initView() {
        b.activity = this

        /** 툴바 */
        b.inToolbar.title = R.string.title_banner_manage.getStr()
        b.inToolbar.backIcon = true
        b.inToolbar.menuText = R.string.add_ad_unit_manage.getStr()
        b.inToolbar.inter = interToolbarIcon(
                onFirst = { onBackPressed() }, onTopTitle = {},
                onTextMenu = { onClickAdd(it) }, onTextMenu2 = null, onIcon = {}, onMore = {})


        /** viewPager */
//        b.frame.addView(CustomMyBannerPager(this@MyBannerActivity, supportFragmentManager, manage, onSelected = { }))
//        fragList = arrayListOf<Pair<Fragment, String>>().apply {
//            add(Pair(MyBannerFrag.newInstance(uid, manage, BannerType.TOP.no, onItemClick = { }), R.string.title_top_banner.getStr()))
//            add(Pair(MyBannerFrag.newInstance(uid, manage, BannerType.MENU.no, onItemClick = { }), R.string.title_menu_banner.getStr()))
//            add(Pair(MyBannerFrag.newInstance(uid, manage, BannerType.MAIN.no, onItemClick = { }), R.string.title_main_banner.getStr()))
//        }

        /** farg리스트 셋팅 */
        fragList = arrayListOf<Pair<Fragment, String>>()
                .apply {
                    add(Pair(MyBannerFrag.newInstance(uid, manage, BannerType.TOP.no, onItemClick = { }), R.string.title_top_banner.getStr()))
                    add(Pair(MyBannerFrag.newInstance(uid, manage, BannerType.MENU.no, onItemClick = { }), R.string.title_menu_banner.getStr()))
                    add(Pair(MyBannerFrag.newInstance(uid, manage, BannerType.MAIN.no, onItemClick = { }), R.string.title_main_banner.getStr()))
                }


        OnPager.commonSetPager(supportFragmentManager, fragList, b.viewPager, b.tabLayout)
        b.viewPager.onPageChange({}, { _, _, _ -> }, onSelected = { p -> currentBannerType = p.getBannerTypeFromPosition() })

    }


    fun onClickAdd(v: View) {
        OnMyClick.setDisableAWhileBTN(v)

        /**   * 배너 생성/수정 다이얼로그  */
        OnBanner.openADDBannerDialog(this@MyBannerActivity, ACTION_NEW, Bid.banner(currentBannerType), onOk = { b, _ -> addToItems(b) })
    }

    /**
     * 방금 업로드한 배너
     */
    fun addToItems(banner: Bid) {
        val index = when (banner.bannerTypeNo.getBannerType()) {
            BannerType.TOP -> 0
            BannerType.MENU -> 1
            BannerType.MAIN -> 2
            else -> return
        }

        (fragList[index].first as? MyBannerFrag)?.let { myBannerFrag ->
            myBannerFrag.customMyBannerList.items.add(banner)
            myBannerFrag.customMyBannerList.adapter.notifyItemInserted(myBannerFrag.customMyBannerList.items.size - 1)    // 마지막 데이터만 추가.
            myBannerFrag.customMyBannerList.removeTvNoData()    //  tvNoData.gone()
        }


    }


}
