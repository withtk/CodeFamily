package com.tkip.mycode.ad.ad_show

import android.content.Context
import android.view.LayoutInflater
import android.widget.LinearLayout
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import com.tkip.mycode.R
import com.tkip.mycode.ad.OnBanner
import com.tkip.mycode.databinding.ConsBannerMenuBinding
import com.tkip.mycode.funs.common.tranPair
import com.tkip.mycode.init.ACTION_NEW
import com.tkip.mycode.init.ACTION_UPDATE
import com.tkip.mycode.init.inter.interBid
import com.tkip.mycode.model.bid.Bid
import com.tkip.mycode.model.my_enum.checkAndGo

/**
 */
class ViewBannerMENU : LinearLayout {
    lateinit var b: ConsBannerMenuBinding

    constructor(context: Context) : super(context)
    constructor(context: Context, adSign:Boolean ,bid: Bid, isBidding: Boolean, onClick: ((Bid) -> Unit)?) : super(context) {
        b = DataBindingUtil.inflate(LayoutInflater.from(context), R.layout.cons_banner_menu, this, true)
        b.bid = bid
        b.adSign = adSign
        b.inter = interBid {

            onClick?.let {
                it(bid)
                return@interBid
            }

            if (isBidding) OnBanner.openADDBannerDialog((context as AppCompatActivity), ACTION_UPDATE, bid, onOk = { bid1, _ -> b.bid = bid1 })

            bid.checkAndGo(context,false,arrayOf(b.cons.tranPair(R.string.tran_linear)), onSuccess = {})
        }

    }


}

