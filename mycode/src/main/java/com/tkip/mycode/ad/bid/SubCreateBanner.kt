package com.tkip.mycode.ad.bid

import android.content.Context
import android.view.LayoutInflater
import android.widget.LinearLayout
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import com.tkip.mycode.R
import com.tkip.mycode.ad.*
import com.tkip.mycode.ad.ad_show.ViewBannerMAIN
import com.tkip.mycode.ad.ad_show.ViewBannerMENU
import com.tkip.mycode.ad.ad_show.ViewBannerTop
import com.tkip.mycode.databinding.SubCreateBannerBinding
import com.tkip.mycode.dialog.alert.MyAlert
import com.tkip.mycode.funs.common.lllogI
import com.tkip.mycode.funs.common.onClick
import com.tkip.mycode.init.ACTION_NEW
import com.tkip.mycode.init.ACTION_UPDATE
import com.tkip.mycode.init.base.getStr
import com.tkip.mycode.model.bid.Bid
import com.tkip.mycode.model.peer.myuid
import com.tkip.mycode.util.tools.anim.gone
import com.tkip.mycode.util.tools.anim.visi
import com.tkip.mycode.util.tools.etc.OnMyClick

/**
 * < 배너 : 쇼 or 수정 >
 *    onFinish : 배너 셋팅 완료시 콜백.
 *    onOk : 배너 설정 완료.
 */
class SubCreateBanner : LinearLayout {
    private lateinit var b: SubCreateBannerBinding
    private lateinit var bid: Bid
    private lateinit var onFinish: () -> Unit
    private var chosenBanner: Bid? = null // 내배너 리스트에서 선택시.

    constructor(context: Context) : super(context)
    constructor(context: Context, bid1: Bid, onFinish: () -> Unit, onOk: () -> Unit) : super(context) {
        lllogI("startVadType bid $bid1")
        this.bid = bid1
        this.onFinish = onFinish
        b = DataBindingUtil.inflate(LayoutInflater.from(context), R.layout.sub_create_banner, this, true)
        b.bannerTypeNo = bid.bannerTypeNo


        /*** 버튼 셋팅 ***************************************************/
        b.btnOpen.onClick {
            OnMyClick.setDisableAWhileBTN(it)

            /** 내 배너 리스트 다이얼로그 */
            MyAlert.showMyBannerList(context, myuid(), false, bid.bannerTypeNo,
                    onSelected = { banner ->
                        lllogI("SubCreateBanner showMyBannerList banner  $banner")
                        if (banner.isNotGranted(true)) return@showMyBannerList
                        bid.setBanner(banner)
                        chosenBanner = banner
                        resetBanner()
                    })

            /*** 내 배너 선택 : 리턴은 곧장 activity로.*/
//            lllogI("BannerManageActivity SubCreateBanner getBannerType ${bid.bannerTypeNo.getBannerType()}")
//            OnBanner.gotoMyBannerActivity((context as AppCompatActivity),  bid.bannerTypeNo )
        }

        b.btnOk.onClick {
            OnMyClick.setDisableAWhileBTN(it)
            onOk()
        }


        /*** 승인 필수 유무 체크 ******************************************/
        if (bid.needGranted(false)) {
            b.tvInfo.text = R.string.title_make_ad_banner_manual_main.getStr()
            b.btnOk.gone()
        }

        /*** 배너 셋팅 *****************************************************/
        resetBanner()

    }

    private fun resetBanner() {
        lllogI("SubCreateBanner addBanner chosenBanner  $chosenBanner bid  $bid")

        val banner1 = chosenBanner ?: bid
        b.frame.removeAllViews()
        b.frame.addView(
                when (bid.adTypeNo!!.getAdtype()) {
                    AdType.TOP_TOTAL_BOARD, AdType.TOP_FLEA, AdType.TOP_ROOM, AdType.TOP_BOARD -> ViewBannerTop(context, true, banner1, true, null)
                    AdType.MENU -> ViewBannerMENU(context, true, banner1,false,null)
                    AdType.MAIN -> ViewBannerMAIN(context, false, banner1,false,null)
                    else -> ViewBannerTop(context, true, banner1, false, null)
                })
        b.btnOk.visi()
        onFinish()
    }

    /*** 배너 클릭시 */
    private fun openADDBanner() {
        /**   * 배너 생성/수정 다이얼로그  */
        OnBanner.openADDBannerDialog(context as AppCompatActivity,
                if (chosenBanner == null) ACTION_NEW else ACTION_UPDATE,
                chosenBanner ?: bid,
                onOk = { banner, _ ->
                    bid.setBanner(banner)
                    chosenBanner = banner
                    resetBanner()
                })
    }


}