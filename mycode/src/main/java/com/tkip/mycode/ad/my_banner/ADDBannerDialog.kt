package com.tkip.mycode.ad.my_banner

import android.app.Dialog
import android.content.Context
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.text.InputType
import android.view.View
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import com.tkip.mycode.R
import com.tkip.mycode.ad.*
import com.tkip.mycode.databinding.DialogAddBannerBinding
import com.tkip.mycode.dialog.OnDDD
import com.tkip.mycode.dialog.alert.MyAlert
import com.tkip.mycode.dialog.toast.toastNormal
import com.tkip.mycode.funs.common.getNullStr
import com.tkip.mycode.funs.common.lllogD
import com.tkip.mycode.funs.common.lllogI
import com.tkip.mycode.init.*
import com.tkip.mycode.init.base.getStr
import com.tkip.mycode.init.base.isNullBlankAndSetError
import com.tkip.mycode.init.inter.interDialogBasicClick
import com.tkip.mycode.init.my_bind.BindingDialog
import com.tkip.mycode.model.bid.Bid
import com.tkip.mycode.model.grant.Grant
import com.tkip.mycode.model.grant.OnGrant
import com.tkip.mycode.model.grant.handle
import com.tkip.mycode.model.my_enum.STATUS_LIST_BANNER
import com.tkip.mycode.model.my_enum.Status
import com.tkip.mycode.model.my_enum.getStatus
import com.tkip.mycode.model.peer.isAdmin
import com.tkip.mycode.util.lib.photo.view.LinearAddPhotoOne
import com.tkip.mycode.util.tools.anim.hide
import com.tkip.mycode.util.tools.anim.hideFadeOut
import com.tkip.mycode.util.tools.anim.showFadeIn
import com.tkip.mycode.util.tools.etc.OnMyClick
import com.tkip.mycode.util.tools.spinner.LinearSpinnerStatus


/**
 * banner.status 변화
 *    1. when ACTION_NEW : DELETED
 *    2. after Add : REQUESTED
 *    3. for admin ; 승인처리.
 *
 *    addType - NEW, UPDATE, ADMIN
 */
class ADDBannerDialog : BindingDialog<DialogAddBannerBinding>() {
    override fun getLayoutResId(): Int = R.layout.dialog_add_banner
    override fun getSize(): Pair<Float?, Float?> = Pair(0.95f, null)

    private var onSuccess: ((Bid, Grant?) -> Unit)? = null
    private val addType: Int by lazy { arguments?.getInt(PUT_ACTION) ?: ACTION_NEW }
    private val banner by lazy { arguments?.getParcelable(PUT_BID) ?: Bid() }
    private var grant: Grant? = null
    private var selectedStatus = Status.BANNER_REQUESTED

    companion object {
        @JvmStatic
        fun newInstance(addType: Int, bid: Bid?, onOk1: ((Bid, Grant?) -> Unit)?) = ADDBannerDialog().apply {
            this.onSuccess = onOk1
            arguments = Bundle().apply {
                putInt(PUT_ACTION, addType)    // addType 셋팅.
                putParcelable(PUT_BID, bid)
            }
        }
    }

    override fun onResume() {
        super.onResume()
        if (onSuccess == null) dismiss()
    }

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
//        return super.onCreateDialog(savedInstanceState)

        initView()

        val builder = AlertDialog.Builder(activity as Context).apply {
            setView(b.root)
            setCancelable(true)
//            setOnCancelListener(DialogInterface.OnCancelListener { dialog -> mListener.onCancel()  })
        }

        return builder.create().apply {
            setCanceledOnTouchOutside(true)
            window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        }

    }

    private fun initView() {
        b.inter = interDialogBasicClick(onDel = null,
                onCancel = { onClickCancel(it) },
                onOk = { onClickOk(it) })
        b.banner = banner

        lllogD("ADDBannerDialog ADDBannerDialog   banner $banner addType $addType")

        /**  배너종류에 따라 분기   */
        when (banner.bannerTypeNo.getBannerType()) {
            BannerType.MAIN -> {
                b.etTitle.maxLines = 2
                b.etTitle.inputType = InputType.TYPE_TEXT_FLAG_MULTI_LINE
                b.etComment.maxLines = 4
                b.etComment.inputType = InputType.TYPE_TEXT_FLAG_MULTI_LINE
            }
            else -> {
            }
        }


        /**  타이틀 셋팅   */
        when (addType) {
            ACTION_NEW -> b.tvTitle.text = R.string.title_add_banner_new.getStr()
            ACTION_UPDATE -> b.tvTitle.text = R.string.title_add_banner_update.getStr()
            ACTION_ADMIN -> {
                b.tvTitle.text = R.string.title_ad_unit_dialog.getStr()
                OnGrant.getGrant(banner) {
                    grant = it
                    b.grant = it
                }
            }
            else -> b.tvTitle.text = R.string.title_ad_unit_dialog.getStr()
        }

        /*** 배너타입이 NOTHING 이면 선택 RG 만들기.  */
        if (banner.bannerTypeNo == BannerType.NOTHING.no) b.frameRg.addView(CustomRGbanner(mContext, banner.bannerTypeNo, onRadio = { banner.bannerTypeNo = it.no }))

        /**   status 스피너  : for admin   */
        b.layReason.hide()
        if (isAdmin()) b.spin.addView(LinearSpinnerStatus(mContext, false, STATUS_LIST_BANNER, banner.statusNo, onClick = {
            banner.statusNo = it.no
            selectedStatus = it
            if (it == Status.NORMAL) {
                b.layReason.hideFadeOut(false)
                b.etReason.setText("")
            } else b.layReason.showFadeIn(false)
        }))

        b.etTitle.setOnFocusChangeListener { _, isOn -> if (!isOn) b.etTitle.isNullBlankAndSetError(b.layTitle, R.string.error_add_ad_unit_title) }
        //        b.etComment.setOnFocusChangeListener { _, isOn -> if (!isOn) b.etComment.isNullBlankAndSetError(b.layComment, R.string.error_add_store_menu_price) }

        /**   사진 올리기   */
        viewAddPhotoOne = LinearAddPhotoOne(activity as AppCompatActivity, banner.photoThumb, banner.photoUrl, null, onGallery = { onClickOpenGallery() }).apply { this@ADDBannerDialog.b.frameAddPhotoOne.addView(this) }
    }

    fun onClickCancel(v: View) {
        OnMyClick.setDisableAWhileBTN(v)
        this.dialog?.cancel()
    }


    fun onClickOk(v: View) {
        OnMyClick.setDisableAWhileBTN(v)

        //            if (b.etTitle.isNullBlankAndSetError(b.layTitle, R.string.error_add_ad_unit_title)) return
//            if (OnValid.hasNullET(b.etTitle)) return

        banner.title = b.etTitle.getNullStr()
        banner.comment = b.etComment.getNullStr()

        lllogI("ADDBannerDialog onClickOk 11")

        if (banner.bannerInValid()) return
        if (banner.bannerTypeNo.getBannerType() == BannerType.MAIN && viewAddPhotoOne?.b?.iv?.visibility != View.VISIBLE) {  // 포토 필요한 배너 : 포토 없으면 true
            toastNormal(R.string.alert_photo_necessarily)
            return
        }

        v.isEnabled = false

        /** 이전 이미지 삭제하기
         * todo : 체크할 것 : 광고중인 이미지가 삭제되면 안 되는데...
         * */
//            viewAddPhotoOne?.let {
//                if (it.hasToBeDeletedPhoto) {
//                    PhotoUtil.deletePhoto(bid.photoUrl, bid.photoThumb)
//                    bid.photoUrl = null
//                    bid.photoThumb = null
//                }
//            }

        lllogI("ADDBannerDialog onClickOk 22")

        when (addType) {
            ACTION_ADMIN -> {
                OnGrant.getGrant(banner) {
                    //                        val ggg = OnGrant.finalGrant(true, it, banner, b.etReason.getNullStr())
                    val ggg = it ?: Grant(banner)
                    ggg.modelStatus = banner.statusNo
                    ggg.handle(b.cbHandle.isChecked, b.cbPush.isChecked, b.etReason.getNullStr())
                    lllogD("ADDBannerDialog ADDBannerDialog ggg $ggg banner $banner")
                    MyAlert.showConfirm(mContext, banner.statusNo.getStatus().dialogMsg, ok = { finalAdd(ggg) }, cancel = {})
                }
            }

//                ACTION_NEW -> {
//                    val ggg = OnGrant.finalGrant(false, null, banner, null)
//                    MyAlert.showConfirm(mContext, R.string.info_wanna_upload_add_banner_need_grant.getStr(), ok = { finalAdd(ggg) }, cancel = {})
//                }
//                ACTION_UPDATE -> {
//                    OnGrant.getGrant(banner) {
//                        val ggg = OnGrant.finalGrant(false, it, banner, null)
//                        MyAlert.showConfirm(mContext, R.string.info_wanna_upload_udpate_banner_need_grant.getStr(), ok = { finalAdd(ggg) }, cancel = {})
//                    }
//                }

            else -> {

                lllogI("ADDBannerDialog onClickOk 33")
                /*** 승인 필요한 배너*/
                if (banner.needGranted(false)) {
                    OnGrant.getGrant(banner) {
                        banner.statusNo = selectedStatus.no
                        val ggg = OnGrant.finalGrant(false, it, banner, null)
                        lllogD("addPhotoAndBanner addPhotoAndBanner ACTION_NEW ggg $ggg")
                        val dialogMsg = if (addType == ACTION_NEW) R.string.info_wanna_upload_add_banner_need_grant.getStr() else R.string.info_wanna_upload_udpate_banner_need_grant.getStr()
                        MyAlert.showConfirm(mContext, dialogMsg, ok = { finalAdd(ggg) }, cancel = {})
                    }
                } else {
                    /*** 자동승인 배너*/
                    banner.statusNo = Status.BANNER_GRANTED.no
                    val dialogMsg = if (addType == ACTION_NEW) R.string.info_wanna_upload_add_banner.getStr() else R.string.info_wanna_upload_update_banner.getStr()
                    MyAlert.showConfirm(mContext, dialogMsg, ok = { finalAdd(null) }, cancel = {})
                }
            }


        }

    }


    private fun finalAdd(grant: Grant?) {

//        OnGrant.getGrant(banner) {
//            val ggg = OnGrant.finalGrant(isHandle, it, banner, null)

        OnBanner.addPhotoAndBanner(mContext, addType, banner, grant,
                success = {
                    onSuccess?.invoke(banner, grant)
                    this.dialog?.cancel()
                },
                fail = { b.btnOk.isEnabled = true })
//        }

    }


}