package com.tkip.mycode.ad

import com.tkip.mycode.R
import com.tkip.mycode.init.base.getDrawable
import com.tkip.mycode.init.base.getInt
import com.tkip.mycode.init.base.getStr
import com.tkip.mycode.util.tools.date.getIntDate8TodayPH


/******************************** ad 기본 ***********************************************************/
val HIT_DATE_DEFAULT_BID by lazy { 20200101 }  // 기본 광고의 hitDate
val HIT_DATE by lazy { getIntDate8TodayPH() } // 오늘의 hitDate

val AD_AMOUNT_BACK_PERCENT: Float by lazy { 0.2f }    // 출석여부에 따른 낙찰광고의 리워드.
fun Int.amountBack() = (this * AD_AMOUNT_BACK_PERCENT).toInt()

private const val stack_NOTHINGT = 0
private const val stack_AD_CREDIT = 9999
private val stack_MAIN by lazy { R.integer.ad_stack_main.getInt() }
private val stack_MENU by lazy { R.integer.ad_stack_menu.getInt() }
private val stack_RANK_FLEA by lazy { R.integer.ad_stack_rank_flea.getInt() }
private val stack_RANK_ROOM by lazy { R.integer.ad_stack_rank_room.getInt() }
private val stack_TOP_FLEA by lazy { R.integer.ad_stack_top_flea.getInt() }
private val stack_TOP_ROOM by lazy { R.integer.ad_stack_top_room.getInt() }
private val stack_TOP_BOARD by lazy { R.integer.ad_stack_top_board.getInt() }
private val stack_TOP_TOTAL_BOARD by lazy { R.integer.ad_stack_top_total_board.getInt() }

//todo: stack은 remote 로 수정할 것.


/***************************************************************************************************************/
/***************************************************************************************************************/
/******************************** advertisement 타입 ***********************************************************/
/**
 * 배너광고	한개만 가능. 1등만 게재 나머지는 취소됨.
 * 게시판 광고	여러개 가능. 입찰한 금액에따라 순위 정해짐. 그 외의 순위는 토탈 포인트로 정해짐.
 * 태그 광고 2개	한개만 가능. 1등만 게재 나머지는 취소됨.
 * stack : 광고 로테이션 개수 (fucntions 연동 : 스택 수정하면 functions도 수정)
 * minCredit : 입찰 단위
 * todo : grant : 광고 올릴 수 있는 회원 레벨
 */
enum class AdType(val no: Int, val stack: Int, val minCredit: Int, val bannerType: BannerType, val grant: Int, val title: String, val info: String, val preview: Int) {
//    TAG(10, R.string.ad_title_auction_tag.getStr()),
//    BOARD(20, R.string.ad_title_board.getStr()),

//    NOTHING(1000, stack_NOTHINGT, 1, BannerType.NOTHING,0, R.string.ad_type_nothing.getStr(), R.string.ad_type_nothing_info.getStr(), R.drawable.ic_board),     // 배너만 저장할때

    AD_CREDIT(1200, stack_AD_CREDIT, 10, BannerType.NOTHING, 0, R.string.ad_ad_credit.getStr(), R.string.ad_ad_credit_info.getStr(), R.drawable.img_ad_adcredit),    // 메인보드 홈 배너
//    BOARD_CREDIT(20, R.string.ad_board_ad_credit.getStr(), R.string.ad_board_ad_credit11.getStr(), R.string.ad_board_ad_credit_info.getStr(), R.drawable.ic_board),    // 메인보드 홈 배너
//    FLEA_CREDIT(30, R.string.ad_flea_ad_credit.getStr(), R.string.ad_flea_ad_credit.getStr(), R.string.ad_flea_ad_credit_info.getStr(), R.drawable.ic_board),    // 메인보드 홈 배너
//    ROOM_CREDIT(40, R.string.ad_room_ad_credit.getStr(), R.string.ad_room_ad_credit.getStr(), R.string.ad_room_ad_credit_info.getStr(), R.drawable.ic_board),    // 메인보드 홈 배너
//    STORE_CREDIT(50, R.string.ad_store_ad_credit.getStr(), R.string.ad_store_ad_credit11.getStr(), R.string.ad_store_ad_credit_info.getStr(), R.drawable.ic_board),     // 메인 홈 배너

    MAIN(2100, stack_MAIN, 1000, BannerType.MAIN, 0, R.string.ad_type_main.getStr(), R.string.ad_type_main_info.getStr(), R.drawable.img_ad_main),     // 메인 홈 배너
    MENU(2200, stack_MENU, 100, BannerType.MENU, 0, R.string.ad_type_menu.getStr(), R.string.ad_type_menu_info.getStr(), R.drawable.img_ad_menu),   // 내비게이션 메뉴 상단

    RANK_FLEA(4200, stack_RANK_FLEA, 100, BannerType.FLEA, 0, R.string.ad_type_rank_flea.getStr(), R.string.ad_type_rank_flea_info.getStr(), R.drawable.img_ad_top),   // 중고거래 최상위 3개 프리미엄
    RANK_ROOM(4600, stack_RANK_ROOM, 100, BannerType.ROOM, 0, R.string.ad_type_rank_room.getStr(), R.string.ad_type_rank_room_info.getStr(), R.drawable.img_ad_top),   // 부동산 최상위 2개 프리미엄

    TOP_FLEA(6000, stack_TOP_FLEA, 100, BannerType.TOP, 0, R.string.ad_type_holder_flea.getStr(), R.string.ad_type_holder_flea_info.getStr(), R.drawable.img_ad_top),          // 중고마켓 상단홀더
    TOP_ROOM(6200, stack_TOP_ROOM, 100, BannerType.TOP, 0, R.string.ad_type_holder_room.getStr(), R.string.ad_type_holder_room_info.getStr(), R.drawable.img_ad_top),          // 부동산 상단홀더
    TOP_BOARD(6400, stack_TOP_BOARD, 100, BannerType.TOP, 0, R.string.ad_type_holder_board.getStr(), R.string.ad_type_holder_board_info.getStr(), R.drawable.img_ad_top),         // 개별 보드 상단홀더
    TOP_TOTAL_BOARD(6800, stack_TOP_TOTAL_BOARD, 100, BannerType.TOP, 0, R.string.ad_type_holder_total_board.getStr(), R.string.ad_type_holder_total_board_info.getStr(), R.drawable.img_ad_top),        // 모든 보드 상단홀더
    ;


    fun getIcon() = preview.getDrawable()

    fun isNeedBanner() =
            when (this) {
                MAIN, MENU, TOP_FLEA, TOP_ROOM, TOP_BOARD, TOP_TOTAL_BOARD -> true
                else -> false
            }

//    fun getBannerTypeNo() =
//            when (this) {
//                MAIN, MENU -> BannerType.SQUARE.no
//                FIRST_FLEA -> BannerType.FLEA.no
//                FIRST_ROOM -> BannerType.ROOM.no
//                TOP_FLEA, TOP_ROOM, TOP_BOARD, TOP_TOTAL_BOARD -> BannerType.TOP.no
//                else -> BannerType.NOTHING.no
//            }

}

/** * stack(광고개수)을 포함하는 STring 받아오기 */
fun setFormatInfo(strID: Int, stack: Int) = String.format(strID.getStr(), stack)

fun Int.getAdtype(): AdType {
    AdType.values().forEach { if (this == it.no) return it }
    return AdType.TOP_TOTAL_BOARD
}

fun String.getAdtype(): AdType {
    AdType.values().forEach { if (this == it.title) return it }
    return AdType.TOP_TOTAL_BOARD
}

/**
 * 라디오버튼 ID로 adType 받아오기
 */
//fun Int.getAdTypeFromResID() =
//        when (this) {
//            R.id.rb_main -> AdType.MAIN
//            R.id.rb_menu -> AdType.MENU
//            R.id.rb_first_flea -> AdType.RANK_FLEA
//            R.id.rb_first_room -> AdType.RANK_ROOM
//            R.id.rb_total -> AdType.TOP_TOTAL_BOARD
//            R.id.rb_board -> AdType.TOP_BOARD
//            R.id.rb_flea -> AdType.TOP_FLEA
//            R.id.rb_room -> AdType.TOP_ROOM
//            R.id.rb_ad_credit -> AdType.AD_CREDIT
//            else -> AdType.NOTHING
//        }


enum class BannerType(val no: Int, val title: String) {

    NOTHING(10, R.string.title_main_banner.getStr()),  // 사실상 필요없는 듯.
    MAIN(20, R.string.title_main_banner.getStr()),
    MENU(30, R.string.title_menu_banner.getStr()),
    TOP(40, R.string.title_top_banner.getStr()),
    FLEA(50, R.string.title_flea_banner.getStr()),    // 사실상 필요없는 듯.
    ROOM(60, R.string.title_room_banner.getStr()),    // 사실상 필요없는 듯.
    ;

}

fun Int.getBannerTypeFromPosition(): BannerType =
        when (this) {
            0 -> BannerType.TOP
            1 -> BannerType.MENU
            2 -> BannerType.MAIN
            else -> BannerType.TOP
        }

fun Int.getBannerType(): BannerType {
    BannerType.values().forEach { if (this == it.no) return it }
    return BannerType.TOP
}

fun String.getBannerType(): AdType {
    AdType.values().forEach { if (this == it.title) return it }
    return AdType.TOP_TOTAL_BOARD
}

//val AD_MODEL_LIST = arrayOf(DataType.STORE, DataType.FLEA, DataType.ROOM, DataType.BOARD, DataType.POST)


//fun Array<DataType>.getAdTypeLIST(i: Int) :Pair< Array<AdType>,  Array<AdType>> =
//        when (this[i]) {
//            DataType.STORE -> Pair(arrayOf(AdType.BANNER_MAIN, AdType.BANNER_DRAWER, AdType.TOP_FLEA, AdType.TOP_ROOM),arrayOf( AdType.TOP_POST, AdType.TOP_TOTAL, AdType.AD_CREDIT))
//            DataType.BOARD -> Pair(arrayOf(AdType.BANNER_MAIN, AdType.BANNER_DRAWER, AdType.TOP_FLEA, AdType.TOP_ROOM),arrayOf( AdType.TOP_POST, AdType.TOP_TOTAL, AdType.AD_CREDIT))
//            DataType.BOARD -> Pair(arrayOf(AdType.BANNER_MAIN, AdType.BANNER_DRAWER, AdType.FIRST_FLEA, AdType.TOP_FLEA),arrayOf( AdType.TOP_ROOM,AdType.TOP_POST, AdType.TOP_TOTAL, AdType.AD_CREDIT))
//            DataType.FLEA -> arrayOf(AdType.BANNER_MAIN, AdType.BANNER_DRAWER, AdType.FIRST_FLEA, AdType.TOP_FLEA, AdType.TOP_ROOM, AdType.TOP_POST, AdType.TOP_TOTAL, AdType.AD_CREDIT)
//            DataType.ROOM -> arrayOf(AdType.BANNER_MAIN, AdType.BANNER_DRAWER, AdType.FIRST_ROOM, AdType.TOP_FLEA, AdType.TOP_ROOM, AdType.TOP_POST, AdType.TOP_TOTAL, AdType.AD_CREDIT)
//            DataType.POST -> arrayOf(AdType.BANNER_MAIN, AdType.BANNER_DRAWER,  AdType.TOP_FLEA, AdType.TOP_ROOM, AdType.TOP_POST, AdType.TOP_TOTAL )
//            else -> arrayOf(AdType.BANNER_MAIN)
//        }

