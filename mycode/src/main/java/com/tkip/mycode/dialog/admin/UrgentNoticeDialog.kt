package com.tkip.mycode.dialog.admin

import android.app.Dialog
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AlertDialog
import com.tkip.mycode.R
import com.tkip.mycode.admin.systemMan
import com.tkip.mycode.databinding.DialogUrgentNoticeBinding
import com.tkip.mycode.dialog.alert.MyAlert
import com.tkip.mycode.dialog.toast.toastNormal
import com.tkip.mycode.funs.common.OnValid
import com.tkip.mycode.funs.common.getNullStr
import com.tkip.mycode.funs.common.getStr
import com.tkip.mycode.funs.common.lllogD
import com.tkip.mycode.init.base.getStr
import com.tkip.mycode.init.my_bind.BindingDialog
import com.tkip.mycode.model.manage.SystemMan
import com.tkip.mycode.util.tools.etc.OnMyClick
import com.tkip.mycode.util.tools.fire.Firee

class UrgentNoticeDialog : BindingDialog<DialogUrgentNoticeBinding>() {
    override fun getLayoutResId(): Int = R.layout.dialog_urgent_notice
    override fun getSize(): Pair<Float?, Float?> = Pair(0.95f, null)

    //    private val warn by lazy { arguments?.getParcelable<Warn>(PUT_WARN) ?: Warn() }
//    private var onOk: ((Warn) -> Unit)? = null

    companion object {
        @JvmStatic
        fun newInstance() = UrgentNoticeDialog()
    }

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
//        if (onOk == null) dismiss()
        b.dialog = this
        b.systemMan = systemMan
        lllogD("UrgentNoticeDialog systemMan $systemMan")

        /***************************** BUILD ***************************************/
        val builder = AlertDialog.Builder(mContext).apply {
            setView(b.root)
            setCancelable(true)
//            setOnCancelListener(DialogInterface.OnCancelListener { dialog -> mListener.onCancel()  })
        }

        return builder.create().apply {
            setCanceledOnTouchOutside(true)
            window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        }

    }


    fun onClickPreview(v: View) {
        OnMyClick.setDisableAWhileBTN(v)
        MyAlert.showConfirm(mContext, b.etTitle.getStr(), b.etComment.getStr(), null, isCancel = true, isCancelTouch = true, ok = {}, cancel = {})
    }

    fun onClickCancel(v: View) {
        OnMyClick.setDisableAWhileBTN(v)
        dialog?.dismiss()
    }

    fun onClickOk(v: View) {
        OnMyClick.setDisableAWhileBTN(v)

//        onOk?.let {

        if (OnValid.hasError(b.etTitle, b.etComment)) return


        /**
         * 올리기
         */
        MyAlert.showConfirm(mContext, R.string.status_dialog_notice_handled.getStr(),
                ok = {
                    Firee.addSystemMan(getFinalData(),
                            success = {
                                toastNormal(R.string.complete_upload)
                                dismiss()
                            }, fail = {})
                }, cancel = {})


//        } ?: OnDiaClog.alertNullListner()

    }

    private fun getFinalData() =
            SystemMan(systemMan?:SystemMan())
                    .apply {
                        notice(b.cbShow.isChecked, this.noticeNo
                                + 1, b.etTitle.getNullStr(), b.etComment.getNullStr(), b.cbUrgent.isChecked)
                    }


}