package com.tkip.mycode.dialog.result

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.tkip.mycode.databinding.HolderResultStoreBinding
import com.tkip.mycode.model.tag.StoreMin

class ResultStoreAdapter(val items: ArrayList<StoreMin>, var onSelect: (StoreMin) -> Unit) : RecyclerView.Adapter<ResultStoreAdapter.ResultStoreViewHolder>() {


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ResultStoreViewHolder {
        val binding = HolderResultStoreBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return ResultStoreViewHolder(binding)
    }

    override fun onBindViewHolder(h: ResultStoreViewHolder, p: Int) {

        h.bind(items[p])

    }

    override fun getItemCount(): Int {
        return items.size
    }


    inner class ResultStoreViewHolder(val b:    HolderResultStoreBinding) : RecyclerView.ViewHolder(b.root) {

        lateinit var storeMin: StoreMin

        fun bind(storeMin: StoreMin) {
            this.storeMin = storeMin
            b.holder = this
            b.storeMin = storeMin

        }

        fun onClickCard() {
            onSelect(storeMin)
        }

    }
}