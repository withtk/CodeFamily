package com.tkip.mycode.dialog

import android.app.Dialog
import android.content.Intent
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.view.LayoutInflater
import androidx.appcompat.app.AlertDialog
import androidx.databinding.DataBindingUtil
import com.tkip.mycode.R
import com.tkip.mycode.databinding.DialogChoiceBinding
import com.tkip.mycode.init.my_bind.BaseDialog
import com.tkip.mycode.nav.store.StoreActivity
import com.tkip.mycode.util.lib.map.MyMapActivity

class ChoiceDialog : BaseDialog() {
    lateinit var b: DialogChoiceBinding

    companion object {

        private var name: String? = null

        @JvmStatic
        fun newInstance(name: String?) =
                ChoiceDialog().apply {
                    arguments = Bundle().apply {
                        ChoiceDialog.name = name
                    }
                }
    }

    override fun getSize(): Pair<Float?, Float?> {
        return Pair(0.75f, null)
    }

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {

        b = DataBindingUtil.inflate(LayoutInflater.from(mContext), R.layout.dialog_choice, null, false)
        b.dialog = this
        b.name = name


        val builder = AlertDialog.Builder(mContext).apply {
            setView(b.root)
            setCancelable(true)
        }

        return builder.create().apply {
            setCanceledOnTouchOutside(true)
            window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        }

    }


    fun onClickCancel() {
        dismiss()
    }

    fun onClickOk1() {

        val intent = Intent(mContext, MyMapActivity::class.java)
        startActivity(intent)

        dismiss()
    }

    fun onClickOk2() {

        val intent = Intent(mContext, StoreActivity::class.java)
        startActivity(intent)

        dismiss()
    }


}