package com.tkip.mycode.dialog.progress

import android.app.Dialog
import android.content.Context
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.view.LayoutInflater
import android.view.Window
import com.tkip.mycode.R
import com.tkip.mycode.databinding.DialogMyCircleBinding
import com.tkip.mycode.funs.common.lllogI
import com.tkip.mycode.funs.common.onClick

object MyCircle {

    var dialog: Dialog? = null

    fun init() {

    }

    fun show(context: Context, waitType: WAIT_TYPE) {
        lllogI("MyCircle MyCircle show start $waitType")
        dialog?.let {
            if (it.isShowing) cancel()
        }
        lllogI("MyCircle MyCircle show isNotShowing $waitType")

        val b = DialogMyCircleBinding.inflate(LayoutInflater.from(context))
        b.waitType = waitType
        b.btnHide.onClick { cancel() }

        /**
         * 완전 투명하게
         */
        dialog = if (waitType.transparent) Dialog(context, R.style.MyCircleDialog)
        else Dialog(context)


        dialog?.apply {
            requestWindowFeature(Window.FEATURE_NO_TITLE)
            setContentView(b.root)
            setCancelable(false)
            setCanceledOnTouchOutside(false)
            window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
            show()
        }

        /*** 30초 동안 계속 돌면 그냥 다 finish() */
//        Handler().postDelayed({  cancel() },40000)

    }

    fun cancel() {
        dialog?.let {
            if (it.isShowing) {
                it.dismiss()
                dialog = null
                lllogI("MyCircle MyCircle show dismiss ")
            }
        }
    }

}