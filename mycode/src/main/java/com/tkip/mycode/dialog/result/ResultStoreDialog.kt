package com.tkip.mycode.dialog.result

import android.app.Dialog
import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.view.LayoutInflater
import androidx.appcompat.app.AlertDialog
import androidx.databinding.DataBindingUtil
import com.tkip.mycode.R
import com.tkip.mycode.init.my_bind.BaseDialog
import com.tkip.mycode.util.lib.map.MyMapActivity
import com.tkip.mycode.model.tag.StoreMin
import com.tkip.mycode.nav.store.StoreActivity

class ResultStoreDialog : BaseDialog() {

    lateinit var b: com.tkip.mycode.databinding.DialogResultStoreBinding
    private lateinit var adapter: ResultStoreAdapter

    companion object {

        private var typeTag: String? = null
        private var items = arrayListOf<StoreMin>()

        @JvmStatic
        fun newInstance(typeTag: String?, items: ArrayList<StoreMin>) =
                ResultStoreDialog().apply {
                    arguments = Bundle().apply {
                        Companion.typeTag = typeTag
                        Companion.items = items
                    }
                }
    }

    override fun getSize(): Pair<Float?, Float?> {
        return Pair(0.85f, null)
    }

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {

        b = DataBindingUtil.inflate(LayoutInflater.from(activity), R.layout.dialog_result_store, null, false)
        b.dialog = this
        b.tag = typeTag

        adapter = ResultStoreAdapter(items,
                onSelect = {

                    val intent = Intent(activity, StoreActivity::class.java)
//                    intent.putExtra(PUT_STORE,)
                    startActivity(intent)

                })

        b.rv.adapter = adapter


        val builder = AlertDialog.Builder(activity as Context).apply {
            setView(b.root)
            setCancelable(true)
        }

        return builder.create().apply {
            setCanceledOnTouchOutside(true)
            window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        }

    }


    fun onClickCancel() {
        dismiss()
    }

    fun onClickOk1() {

        val intent = Intent(activity, MyMapActivity::class.java)
        startActivity(intent)

        dismiss()
    }

    fun onClickOk2() {

        val intent = Intent(activity, StoreActivity::class.java)
        startActivity(intent)

        dismiss()
    }


}