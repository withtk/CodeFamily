package com.tkip.mycode.dialog.progress

import android.app.Dialog
import android.content.Context
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.view.LayoutInflater
import android.view.View
import android.view.Window
import android.widget.Button
import com.tkip.mycode.R

object MyProgress {

    var dialog: Dialog? = null

    fun init() {

    }

    fun show(context: Context, ppro: PPro?) {
        if (dialog?.isShowing == true) return
        var view: View? = null

        if (ppro == null) {
            view = LayoutInflater.from(context).inflate(R.layout.dialog_my_progress, null)
        } else {
            when (ppro.type) {
                PPro.PROG_BASIC -> {
                    view = LayoutInflater.from(context).inflate(R.layout.dialog_my_progress, null)

                    if (ppro.hasBtn) {
                        val btn = view.findViewById(R.id.btn1) as Button
                        btn.visibility = View.VISIBLE
                        btn.setOnClickListener {
                            cancel()
                        }
                    }
                }
                PPro.PROG_PERCENT -> {
                    view = LayoutInflater.from(context).inflate(R.layout.dialog_my_percent, null)
                    ppro.info?.let {
                        // 필요하면 적어넣어.
                    }
                    if (ppro.hasBtn) {
                        val btn = view.findViewById(R.id.btn_get) as Button
                        btn.visibility = View.VISIBLE
                        btn.setOnClickListener {
                            cancel()
                        }
                    }
                }
            }
        }


//        val view = LayoutInflater.from(activity).inflate(R.layout.dialog_my_progress, null)
//        if (hasBtn) {
//            val btn = view.findViewById(R.id.btn1) as Button
//            btn.visibility = View.VISIBLE
//            btn.setOnClickListener {
//                cancel()
//            }
//        }

        dialog = Dialog(context).apply {
            requestWindowFeature(Window.FEATURE_NO_TITLE)
            setContentView(view!!)
            setCancelable(false)
            setCanceledOnTouchOutside(false)
            window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
            show()

        }

        /*  Dialog dialog = new Dialog(getActivity(), R.style.TransparentProgressDialog);
                dialog.setCancelable(false);
                dialog.setCanceledOnTouchOutside(false);
                dialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
                dialog.setContentView(R.layout.layout_progress);
                dialog.show();*/
    }

    fun cancel() {
        dialog?.let {
            if (it.isShowing) {
                it.dismiss()
                dialog = null
            }
        }
    }


    fun onDisableButton(view: View) {
        (view as? Button)?.let {
            it.isEnabled = false
        }
    }

    fun onEnbableButton(view: View) {
        (view as? Button)?.let {
            it.isEnabled = true
        }
    }


}