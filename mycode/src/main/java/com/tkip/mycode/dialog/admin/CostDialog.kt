package com.tkip.mycode.dialog.admin

import android.app.Dialog
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AlertDialog
import com.tkip.mycode.R
import com.tkip.mycode.databinding.DialogCostBinding
import com.tkip.mycode.dialog.OnDDD
import com.tkip.mycode.dialog.alert.MyAlert
import com.tkip.mycode.dialog.toast.toastNormal
import com.tkip.mycode.funs.common.getNullStr
import com.tkip.mycode.init.PUT_COST
import com.tkip.mycode.init.base.getStr
import com.tkip.mycode.init.my_bind.BindingDialog
import com.tkip.mycode.model.cost.Cost
import com.tkip.mycode.model.my_enum.PayType
import com.tkip.mycode.util.tools.etc.OnMyClick
import com.tkip.mycode.util.tools.fire.Firee
import com.tkip.mycode.util.tools.spinner.LinearSpinnerString
import com.tkip.mycode.util.tools.spinner.TypeList

class CostDialog : BindingDialog<DialogCostBinding>() {
    override fun getLayoutResId(): Int = R.layout.dialog_cost
    override fun getSize(): Pair<Float?, Float?> = Pair(0.95f, null)

    private val cost by lazy { arguments?.getParcelable<Cost>(PUT_COST) ?: Cost() }
    private val typeList by lazy { arrayListOf<String>().apply { PayType.values().forEach { add(it.title) } } }
    private var onOk: (() -> Unit)? = null

    private lateinit var spinActType: LinearSpinnerString

    private lateinit var spinPayType: LinearSpinnerString
    private lateinit var spinCostType: LinearSpinnerString
    private lateinit var spinModelDataType: LinearSpinnerString

    companion object {
        @JvmStatic
        fun newInstance(cost1: Cost?, onOk1: (() -> Unit) ) = CostDialog().apply {
            arguments = Bundle().apply { putParcelable(PUT_COST, cost1) }
            onOk = onOk1
        }
    }

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        b.dialog = this
        b.cost = cost
//        b.inAllCredit.peer = peer


        /**  스피너 :     */
        spinActType = LinearSpinnerString(mContext, true, null, TypeList.ACT_TYPE, cost.actTypeNo,
                onSelected = {
                    cost.actTypeNo = it

                    /*** 해당사항 실시간 변경 */
                    //todo : actType을 변경했다. 이거 다시 만들던가. 손 봐야지.
//                    val actType = it.actType()
//                    spinPayType.changeSelect(actType.payTypeNo.payType().title)
//                    spinCostType.changeSelect(actType.costType.title)
//                    b.cbMinus.isChecked = actType.sign < 0

                }).apply { this@CostDialog.b.lineSpin1.addView(this) }

//        spinPayType = LinearSpinnerString(mContext, false, null, TypeList.PAY_TYPE, cost.payTypeNo, onSelected = { cost.payTypeNo = it }).apply { this@CostDialog.b.lineSpin.addView(this) }
        spinCostType = LinearSpinnerString(mContext, false, null, TypeList.COST_TYPE, cost.costTypeNo, onSelected = { cost.costTypeNo = it }).apply { this@CostDialog.b.lineSpin.addView(this) }
        cost.dataTypeNo?.let { spinModelDataType = LinearSpinnerString(mContext, false, null, TypeList.DATA_TYPE, cost.dataTypeNo, onSelected = { cost.dataTypeNo = it }).apply { this@CostDialog.b.lineSpin.addView(this) } }


        /***************************** BUILD ***************************************/
        val builder = AlertDialog.Builder(mContext).apply {
            setView(b.root)
            setCancelable(true)
//            setOnCancelListener(DialogInterface.OnCancelListener { dialog -> mListener.onCancel()  })
        }

        return builder.create().apply {
            setCanceledOnTouchOutside(true)
            window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        }

    }


    fun onClickCancel(v: View) {
        OnMyClick.setDisableAWhileBTN(v)
        dialog?.dismiss()
    }

    fun onClickOk(v: View) {
        OnMyClick.setDisableAWhileBTN(v)

        onOk?.let {

            //            if (OnValid.hasError(b.etAmount)) return

            /*** amount null 체크 */
            b.etAmount.getNullStr()?.toInt()?.let { amount ->

//                val sign = if (b.cbMinus.isChecked) -1 else 1
//                cost.amount = amount * sign

                /*** type은 이미 cost에 저장됨. */


                /**             * Cost 올리기             */
                MyAlert.showConfirm(mContext, R.string.status_dialog_credit_handled.getStr(),
                        ok = {
                            Firee.addCost(cost, success = {
                                toastNormal(R.string.complete_upload)
                                dismiss()
                            }, fail = {})
                        }, cancel = {})


            } ?: toastNormal(R.string.error_edittext_null)


        } ?: OnDDD.alertNullListner()

    }


}