package com.tkip.mycode.dialog.warn

import android.app.Dialog
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import com.tkip.mycode.R
import com.tkip.mycode.databinding.DialogWarnHandleBinding
import com.tkip.mycode.dialog.OnDDD
import com.tkip.mycode.dialog.admin.PeerDialog
import com.tkip.mycode.dialog.progress.MyCircle
import com.tkip.mycode.funs.common.OnValid
import com.tkip.mycode.funs.common.getNullStr
import com.tkip.mycode.funs.common.textListener
import com.tkip.mycode.init.PUT_WARN
import com.tkip.mycode.init.my_bind.BindingDialog
import com.tkip.mycode.model.my_enum.STATUS_LIST_BASIC
import com.tkip.mycode.model.peer.mynick
import com.tkip.mycode.model.peer.myuid
import com.tkip.mycode.model.warn.OnWarn
import com.tkip.mycode.model.warn.Warn
import com.tkip.mycode.util.tools.anim.visi
import com.tkip.mycode.util.tools.date.rightNow
import com.tkip.mycode.util.tools.etc.OnMyClick
import com.tkip.mycode.util.tools.fire.Firee
import com.tkip.mycode.util.tools.spinner.LinearSpinnerStatus
import com.tkip.mycode.util.tools.spinner.LinearSpinnerString
import com.tkip.mycode.util.tools.spinner.TypeList
import java.util.concurrent.TimeUnit

class WarnHandleDialog : BindingDialog<DialogWarnHandleBinding>() {
    override fun getLayoutResId(): Int = R.layout.dialog_warn_handle
    override fun getSize(): Pair<Float?, Float?> = Pair(0.95f, null)

    private val warn by lazy { arguments?.getParcelable<Warn>(PUT_WARN) ?: Warn() }
    private var onOk: ((Warn) -> Unit)? = null

    companion object {
        @JvmStatic
        fun newInstance(warn: Warn, onOk: ((Warn) -> Unit)?) =
                WarnHandleDialog().apply {
                    arguments = Bundle().apply { putParcelable(PUT_WARN, warn) }
                    this.onOk = onOk
                }
    }


    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
//        if (onOk == null) dismiss()
        b.dialog = this
        b.warn = warn

        b.inWarn.warn = warn


        /**         * String 스피너 : TAG type    */
        val spinString = LinearSpinnerString(mContext, true, null, TypeList.WARN, warn.warnTypeNo, onSelected = { type -> warn.warnTypeNo = type })
                .apply { this.b.spin.isEnabled = false }
        b.lineSpin1.addView(spinString)

        /**         * model Status 스피너     */
        val spinStatusModel = LinearSpinnerStatus(mContext,true, STATUS_LIST_BASIC, warn.modelStatus, onClick = { status -> warn.modelStatus = status.no })
        b.lineSpin2.addView(spinStatusModel)

        b.etPeriod.textListener(after = {}, before = { _, _, _, _ -> }, on = { s, _, _, _ ->
            if (!s.isNullOrEmpty()) {
                b.lineEndDate.visi()
                s.toString().toInt().let { b.tvDate8.text = OnWarn.getDateLastDate(it).toString() } // lastDate set
            }
        })

//        b.tvComment.movementMethod = ScrollingMovementMethod()

//        /*** 패널티amount 자동기입 */
//        b.etAmount.setText(warn.warnTypeNo.getWarnType().amount.toString())


        /***************************** BUILD ***************************************/
        val builder = AlertDialog.Builder(mContext).apply {
            setView(b.root)
            setCancelable(true)
//            setOnCancelListener(DialogInterface.OnCancelListener { dialog -> mListener.onCancel()  })
        }

        return builder.create().apply {
            setCanceledOnTouchOutside(true)
            window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        }

    }


    fun onClickAuthorNick(v: View) {
        OnMyClick.setDisableAWhileBTN(v)
        /**         *  신고자 Dialog 오픈         */
        Firee.getPeer(warn.authorUID, success = { it?.let { peer -> PeerDialog.newInstance(peer).show((mContext as AppCompatActivity).supportFragmentManager, "dialog") } }, fail = {})
    }

    fun onClickTargetNick(v: View) {
        OnMyClick.setDisableAWhileBTN(v)
        /**         *  타겟유저 Dialog 오픈         */
        warn.targetUID?.let { targetUid -> Firee.getPeer(targetUid, success = { it?.let { peer -> PeerDialog.newInstance(peer).show((mContext as AppCompatActivity).supportFragmentManager, "dialog") } }, fail = {}) }
    }

    fun onClickCancel(v: View) {
        OnMyClick.setDisableAWhileBTN(v)
        dialog?.cancel()
    }

    fun onClickOk(v: View) {
        OnMyClick.setDisableAWhileBTN(v)

        onOk?.let {

            if (OnValid.hasError(b.etAmount, b.etPeriod)) return

            OnDDD.upload(mContext, ok = {

                /*** 모델 Status 변경완료 in Spinner */

                /*** 패널티 관련 */
                warn.amount = b.etAmount.getNullStr()?.toInt()
                warn.period = b.etPeriod.getNullStr()?.toInt()
                warn.period?.let { warn.lastDate = rightNow() + TimeUnit.DAYS.toMillis(it.toLong()) }
                warn.reason = b.etReason.getNullStr()


                /*** 처리한 어드민 */
                warn.adminUID = myuid()
                warn.adminNick = mynick()

                OnWarn.handle(mContext, warn,
                        success = {
                            MyCircle.cancel()
                            dialog?.dismiss()
                            it(warn)
                        }, fail = { })

            }, cancel = {})


        } ?: OnDDD.alertNullListner()


    }


}