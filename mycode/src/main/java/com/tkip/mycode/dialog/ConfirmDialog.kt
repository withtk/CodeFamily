package com.tkip.mycode.dialog

import android.app.Dialog
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.view.LayoutInflater
import androidx.appcompat.app.AlertDialog
import androidx.databinding.DataBindingUtil
import com.tkip.mycode.R
import com.tkip.mycode.databinding.DialogConfirmBinding
import com.tkip.mycode.init.base.Cons
import com.tkip.mycode.init.my_bind.BaseDialog

class ConfirmDialog : BaseDialog() {
    lateinit var b: DialogConfirmBinding

private val title: String by lazy { arguments?.getString(Cons.PUT_TITLE) ?: Cons.PUT_TITLE }
    private val description: String by lazy {
        arguments?.getString(Cons.PUT_DESCRIPTION) ?: Cons.PUT_DESCRIPTION
    }
    private val isCancel: Boolean by lazy { arguments?.getBoolean(Cons.PUT_IS_CANCEL) ?: false }
    private val isCancelTouch: Boolean by lazy {
        arguments?.getBoolean(Cons.PUT_IS_CANCEL_TOUCH) ?: true
    }

    companion object {

        private lateinit var ok: () -> Unit
        private lateinit var deny: () -> Unit


        @JvmStatic
        fun newInstance(title: String?, desc: String?, isCancel: Boolean, isCancelTouch: Boolean, ok: () -> Unit, deny: () -> Unit) =
                ConfirmDialog().apply {
                    arguments = Bundle().apply {
                        ConfirmDialog.ok = ok
                        ConfirmDialog.deny = deny

                        putString(Cons.PUT_TITLE, title)
                        putString(Cons.PUT_DESCRIPTION, desc)
                        putBoolean(Cons.PUT_IS_CANCEL, isCancel)
                        putBoolean(Cons.PUT_IS_CANCEL_TOUCH, isCancelTouch)

                    }
                }
    }

/*
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        b = DataBindingUtil.inflate(inflater, R.layout.dialog_confirm, container, false)
        return b.root
    }*/

    override fun getSize(): Pair<Float?, Float?> {
        return Pair(0.75f, null)
    }

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
//        return super.onCreateDialog(savedInstanceState)

        b = DataBindingUtil.inflate(LayoutInflater.from(mContext), R.layout.dialog_confirm, null, false)
        b.dialog = this
        b.title = title
        b.description = description


        val builder = AlertDialog.Builder(mContext ).apply {
            setView(b.root)
            setCancelable(isCancel)
//            setOnCancelListener(DialogInterface.OnCancelListener { dialog -> mListener.onCancel()  })
        }

        return builder.create().apply {
            setCanceledOnTouchOutside(isCancelTouch)
            window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        }

    }


    fun onClickCancel() {
        deny()
//        listener.onClickCancel()
//        dismiss()
        dialog?.cancel()
    }

    fun onClickOk() {
        ok()

//        listener.onClickOk()
//        dismiss()
        dialog?.cancel()
    }


}