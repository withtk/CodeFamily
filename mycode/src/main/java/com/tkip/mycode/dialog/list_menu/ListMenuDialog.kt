package com.tkip.mycode.dialog.list_menu

import android.app.Dialog
import android.content.Context
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.util.DisplayMetrics
import android.view.LayoutInflater
import android.view.WindowManager
import androidx.appcompat.app.AlertDialog
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.DialogFragment
import com.tkip.mycode.R
import com.tkip.mycode.databinding.DialogListMenuBinding

class ListMenuDialog : DialogFragment() {

    lateinit var b: DialogListMenuBinding
    lateinit var adapter: ListMenuAdapter
    var items = arrayListOf<String>()

    companion object {
        private lateinit var selection: (String) -> Unit
        private lateinit var deny: () -> Unit

        private var title: String? = null
        private var description: String? = null
        private var arrayId: Int? = null
        private var isCancel: Boolean = false
        private var isCancelTouch: Boolean = true

        @JvmStatic
        fun newInstance(title: String?, description: String?, arrayId: Int, isCancel: Boolean, isCancelTouch: Boolean, selection: (String) -> Unit, deny: () -> Unit) =
                ListMenuDialog().apply {
                    arguments = Bundle().apply {
                        Companion.title = title
                        Companion.description = description
                        Companion.arrayId = arrayId
                        Companion.isCancel = isCancel
                        Companion.isCancelTouch = isCancelTouch

                        Companion.selection = selection
                        Companion.deny = deny
                    }
                }
    }

    /*override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.dialog_list_menu, container, false)
    }*/

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
//        return super.onCreateDialog(savedInstanceState)

        b = DataBindingUtil.inflate(LayoutInflater.from(activity), R.layout.dialog_list_menu, null, false)
        b.dialog = this
        b.title = title
        b.description = description


        items.addAll(activity?.resources?.getStringArray(arrayId!!)!!)
        adapter = ListMenuAdapter(items) { menuTitle ->
            selection(menuTitle)
            dismiss()
        }
        b.rv.adapter = adapter

        val builder = AlertDialog.Builder(activity as Context).apply {
            setView(b.root)
            setCancelable(isCancel)
        }
        val dialog = builder.create().apply {
            setCanceledOnTouchOutside(isCancelTouch)
            window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        }

        return dialog

    }


    override fun onResume() {
        // Get screen width and height in pixels
        val displayMetrics = DisplayMetrics()
        activity?.windowManager?.defaultDisplay?.getMetrics(displayMetrics)
        // The absolute width of the available display size in pixels.
        val displayWidth = displayMetrics.widthPixels
        // The absolute height of the available display size in pixels.
        val displayHeight = displayMetrics.heightPixels

        // Initialize a new window manager layout parameters
        val layoutParams = WindowManager.LayoutParams()
        // Copy the alert dialog window attributes id new layout parameter instance
        layoutParams.copyFrom(dialog?.window?.attributes)

        // Set the alert dialog window width and height
        // Set alert dialog width equal id screen width 90%
        // int dialogWindowWidth = (int) (displayWidth * 0.9f);
        // Set alert dialog height equal id screen height 90%
        // int dialogWindowHeight = (int) (displayHeight * 0.9f);

        // Set alert dialog width equal id screen width 70%
        val dialogWindowWidth = (displayWidth * 0.65f).toInt()
        // Set alert dialog height equal id screen height 70%
//        val dialogWindowHeight = (displayHeight * 0.85f).toInt()


        // Set the width and height for the layout parameters
        // This will bet the width and height of alert dialog

        layoutParams.width = dialogWindowWidth
//        layoutParams.height = dialogWindowHeight;
//        layoutParams.width = WindowManager.LayoutParams.WRAP_CONTENT
        layoutParams.height = WindowManager.LayoutParams.WRAP_CONTENT

        // Apply the newly created layout parameters id the alert dialog window
        dialog?.window?.attributes = layoutParams


        super.onResume()
    }

}