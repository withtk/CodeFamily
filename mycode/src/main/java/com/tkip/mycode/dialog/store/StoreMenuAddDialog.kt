package com.tkip.mycode.dialog.store

import android.app.Dialog
import android.content.Context
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import com.tkip.mycode.R
import com.tkip.mycode.databinding.DialogStoreMenuAddBinding
import com.tkip.mycode.dialog.OnDDD
import com.tkip.mycode.funs.common.OnValid
import com.tkip.mycode.funs.common.getNullStr
import com.tkip.mycode.funs.common.getStr
import com.tkip.mycode.funs.common.lllogI
import com.tkip.mycode.init.PUT_STORE_MENU
import com.tkip.mycode.init.base.isNullBlankAndSetError
import com.tkip.mycode.init.my_bind.BindingDialog
import com.tkip.mycode.model.store.StoreMenu
import com.tkip.mycode.nav.store.store_menu.OnStoreMenu
import com.tkip.mycode.util.lib.photo.util.PhotoUtil
import com.tkip.mycode.util.lib.photo.view.LinearAddPhotoOne
import com.tkip.mycode.util.tools.etc.OnMyClick

class StoreMenuAddDialog : BindingDialog<DialogStoreMenuAddBinding>() {
    override fun getLayoutResId(): Int = R.layout.dialog_store_menu_add
    override fun getSize(): Pair<Float?, Float?> = Pair(0.85f, null)


//    private val storeKey by lazy { arguments?.getString(PUT_STORE_KEY) ?: "" }
    private val storeMenu by lazy { arguments?.getParcelable(PUT_STORE_MENU) ?: StoreMenu() }
//    private lateinit var finalStoreMenu: StoreMenu

    private var onOk: ((StoreMenu) -> Unit)? = null

    companion object {
        @JvmStatic
        fun newInstance(storeMenu: StoreMenu?, onOk: ((StoreMenu) -> Unit)?) = StoreMenuAddDialog().apply {
            arguments = Bundle().apply { putParcelable(PUT_STORE_MENU, storeMenu) }
            this.onOk = onOk
        }
    }

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
//        return super.onCreateDialog(savedInstanceState)
        if(onOk == null) dismiss()

        b.dialog = this
        b.storeMenu = storeMenu


        b.etTitle.setOnFocusChangeListener { _, isOn -> if (!isOn) b.etTitle.isNullBlankAndSetError(b.layTitle, R.string.error_add_store_menu_name) }
        b.etPrice.setOnFocusChangeListener { _, isOn -> if (!isOn) b.etPrice.isNullBlankAndSetError(b.layPrice, R.string.error_add_store_menu_price) }

//        b.etTitle.setOnFocusChangeListener { _, isOn ->
//            if (!isOn)
//                when {
//                    b.etTitle.getStr().isEmpty() -> b.layTitle.error = R.string.error_add_store_menu_name.getStr()
//                    else -> b.layTitle.error = null
//                }
//        }
//
//        b.etPrice.setOnFocusChangeListener { _, isOn ->
//            if (!isOn)
//                when {
//                    b.etPrice.getStr().isEmpty() -> b.layPrice.error = R.string.error_add_store_menu_price.getStr()
//                    else -> b.layPrice.error = null
//                }
//        }

        /**         * 사진 1장 올리기 박스         */
        viewAddPhotoOne = LinearAddPhotoOne(activity as AppCompatActivity, storeMenu.photoThumb, storeMenu.photoUrl,
                onDelete = { },
                onGallery = { onClickOpenGallery() })
                .apply {
                    this@StoreMenuAddDialog.b.frameAddPhotoOne.addView(this)
                }


        val builder = AlertDialog.Builder(activity as Context).apply {
            setView(b.root)
            setCancelable(true)
//            setOnCancelListener(DialogInterface.OnCancelListener { dialog -> mListener.onCancel()  })
        }

        return builder.create().apply {
            setCanceledOnTouchOutside(true)
            window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        }

    }


//
//    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
//        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
//
//        when (requestCode) {
//            OnPermission.REQ_READ_EXSTORAGE -> {
//                lllogI("StoreMenuAddDialog onRequestPermissionsResult REQ_READ_EXSTORAGE $requestCode")
//                // 다시 들어가서 체크하고 method 실행
//                OnPermission.onOpenGallery(grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED, activity as Activity)
////                // If request is cancelled, the result arrays are empty.
////                if (grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
////                    onClickOpenGallery()
////                } else {
////                    OnPermission.informAbout_READ_PERMISSION(activity as AppCompatActivity)
////                }
//            }
//        }
//    }
//
//    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
//        super.onActivityResult(requestCode, resultCode, data)
//        lllogI("StoreMenuAddDialog onActivityResult  ")
//        if (resultCode == Activity.RESULT_OK) {
//            when (requestCode) {
//                OnPermission.REQ_PICK -> {
//
//                    lllogI("StoreMenuAddDialog onActivityResult REQ_PICK ${data?.data}")
//                    data?.data?.let { uri ->
//                        b.inAddPhoto.iv.setImageURI(uri)
//                        AnimUtil.showOverUp(b.inAddPhoto.iv, true)
//
//                        PhotoUtil.resetAllUriList()
//                        PhotoUtil.photoUris.add(uri)
//                    }
//
//                }
//            }
//        }
//    }
//
//    fun onClickOpenGallery() {
//        OnPermission.onOpenGallery(true, activity as AppCompatActivity)
////        OnMyClick.setDisableAWhileBTN(v)
////        if (OnMyClick.isDoubleClick()) return
//
//        val readPermission = ContextCompat.checkSelfPermission(mContext, READ_EXTERNAL_STORAGE)
//
//        if (readPermission == PackageManager.PERMISSION_GRANTED) {
//            startActivityForResult(OnPermission.intentPick(), OnPermission.REQ_PICK)
//        } else {
////            TToast.showAlert(R.string.info_just_permission_denied)
//            ActivityCompat.requestPermissions(activity as AppCompatActivity, arrayOf(READ_EXTERNAL_STORAGE), OnPermission.REQ_READ_EXSTORAGE)
//        }
//    }


    fun onClickCancel(v: View) {
        OnMyClick.setDisableAWhileBTN(v)
        this.dialog?.cancel()
    }

    fun onClickOk(v: View) {
        OnMyClick.setDisableAWhileBTN(v)

        onOk?.let {

            if (b.etTitle.isNullBlankAndSetError(b.layTitle, R.string.error_add_store_menu_name) || b.etPrice.isNullBlankAndSetError(b.layPrice, R.string.error_add_store_menu_price)) return
            if (OnValid.hasNullET(b.etTitle, b.etPrice)) return

            storeMenu.name = b.etTitle.getStr()
            storeMenu.price = b.etPrice.getStr().toInt()
            storeMenu.order = b.etIndex.getNullStr()?.toInt() ?: 0

            /** <기존 이미지 삭제하기>
             * 1. 그냥 이전 이미지만 삭제.
             * 2. 새로운 이미지 올림.  */
            viewAddPhotoOne?.let {
                lllogI("StoreMenuAddDialog viewAddPhotoOne ")
                if (it.hasToBeDeletedPhoto) {
                lllogI("StoreMenuAddDialog viewAddPhotoOne  toBeDeletedFoto")
                    PhotoUtil.deletePhoto(storeMenu.photoUrl, storeMenu.photoThumb)
                    storeMenu.photoUrl = null
                    storeMenu.photoThumb = null
                }
            }

            v.isEnabled = false
            OnStoreMenu.addPhotoAndStoreMenu(mContext, storeMenu,
                    success = {
                        onOk?.invoke(storeMenu)
                        dialog?.dismiss()
                    },
                    fail = { v.isEnabled = true })


        } ?: OnDDD.alertNullListner()


    }


}