package com.tkip.mycode.dialog.alert

import android.content.Context
import com.tkip.mycode.init.base.getStr


/******************************************************************************
 *****************************   ************************************
 *****************************************************************************/

/**
 * SHOW AlertInfo Dialog
 *     true : 통과 못함.
 *     false : 통과.
 */
fun Boolean.checkAndAlertInfo(context:Context?, strId:Int) :Boolean{
    if (this)  context?.let { MyAlert.showInfo(it, strId.getStr())}
    return this
}

fun Boolean.checkAndAlertInfo(context:Context?, str:String) :Boolean{
    if (this)  context?.let { MyAlert.showInfo(it, str )}
    return this
}


//
//
///**
// * SHOW AlertConfirm Dialog
// *     true : 통과 못함.
// *     false : 통과.
// */
//fun Boolean.checkAndAlertConfirm(context:Context?, strId:Int, okText: String, ok: () -> Unit ) :Boolean{
//    if (this)  context?.let { MyAlert.showConfirm(it, strId.getStr(),okText,ok)}
//    return this
//}
//
//fun Boolean.checkAndAlertConfirm(context:Context?, str:String) :Boolean{
//    if (this)  context?.let { MyAlert.showInfo(it, str )}
//    return this
//}
