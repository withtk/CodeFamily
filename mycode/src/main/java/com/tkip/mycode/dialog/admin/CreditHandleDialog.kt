package com.tkip.mycode.dialog.admin

import android.app.Dialog
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.view.View
import android.widget.EditText
import androidx.appcompat.app.AlertDialog
import com.tkip.mycode.R
import com.tkip.mycode.databinding.DialogCreditHandleBinding
import com.tkip.mycode.dialog.OnDDD
import com.tkip.mycode.dialog.toast.toastNormal
import com.tkip.mycode.funs.common.getNullStr
import com.tkip.mycode.init.PUT_PEER
import com.tkip.mycode.init.my_bind.BindingDialog
import com.tkip.mycode.model.cost.Cost
import com.tkip.mycode.model.my_enum.ActType
import com.tkip.mycode.model.peer.Peer
import com.tkip.mycode.util.tools.etc.OnMyClick
import com.tkip.mycode.util.tools.fire.addCost
import com.tkip.mycode.util.tools.fire.batch
import com.tkip.mycode.util.tools.fire.myCommit
import com.tkip.mycode.util.tools.spinner.LinearSpinnerActType

/**
 * for admin
 * credit 부여 혹은 회수
 *
 */
class CreditHandleDialog : BindingDialog<DialogCreditHandleBinding>() {
    override fun getLayoutResId(): Int = R.layout.dialog_credit_handle
    override fun getSize(): Pair<Float?, Float?> = Pair(0.95f, null)

    private val peer by lazy { arguments?.getParcelable<Peer>(PUT_PEER) ?: Peer() }
    //    private val typeList by lazy { arrayListOf<String>().apply { PayType.values().forEach { add(it.title) } } }
    private var onSuccess: (() -> Unit)? = null
    private var selectedActType = ActType.NOTHING

    companion object {
        @JvmStatic
        fun newInstance(peer1: Peer, onOk1: (() -> Unit)) = CreditHandleDialog().apply {
            arguments = Bundle().apply { putParcelable(PUT_PEER, peer1) }
            onSuccess = onOk1
        }
    }

    override fun onResume() {
        super.onResume()
        if (onSuccess == null) dismiss()
    }

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
//        if (onOk == null) dismiss()
        b.dialog = this
        b.peer = peer
        b.inAllCredit.peer = peer


        /**   ActType 스피너  : for admin   */
        val actTypeList = ActType.values().toList() as ArrayList<ActType>
        b.frame.addView(LinearSpinnerActType(mContext, actTypeList, null, onClick = {
            selectedActType = it
//            it.balance()?.apply {
//                if (bcrd == null && crd == null && pnt == null && crt == null) b.lineAmount.visi()
//                else b.lineAmount.gone()
//            }
        }))


        /***************************** BUILD ***************************************/
        val builder = AlertDialog.Builder(mContext).apply {
            setView(b.root)
            setCancelable(true)
//            setOnCancelListener(DialogInterface.OnCancelListener { dialog -> mListener.onCancel()  })
        }

        return builder.create().apply {
            setCanceledOnTouchOutside(true)
            window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        }

    }


    fun onClickType(v: View) {
        OnMyClick.setDisableAWhileBTN(v)
//        MyAlert.showSelectString(mContext, null, typeList, onSelected = { b.btnType.text = typeList[it] })
    }

    fun onClickCancel(v: View) {
        OnMyClick.setDisableAWhileBTN(v)
        dialog?.dismiss()
    }

    fun onClickOk(v: View) {
        OnMyClick.setDisableAWhileBTN(v)

        onSuccess?.let {

            //        if (OnValid.hasError(b.etTitle, b.etComment)) return
//            val amount = b.etAmount.getNullStr() ?: 0
//            val payType = b.btnType.text.toString().payType()

            if (selectedActType == ActType.NOTHING) {
                toastNormal(R.string.alert_select_act_type)
                return
            }


            OnDDD.confirm(mContext, R.string.info_wanna_upload_user_credit,
                    ok = {
                        /*** Cost 올리기 */
                        batch().apply {
                            addCost(Cost.new(peer.uid, selectedActType, null, b.etPnt.digit(), b.etBcrd.digit(), b.etCrd.digit(), b.etCrt.digit()))
                        }.myCommit("CreditHandleDialog",
                                success = {
                                    toastNormal(R.string.complete_upload)
                                    dismiss()
                                }, fail = {})
                    }, cancel = {})


        } ?: OnDDD.alertNullListner()

    }

    /**
     * 1이하는 null로 바꿔야한다.
     */
    private fun EditText.digit(): Int? {
        this.getNullStr()?.let {
            val final = it.toInt()
            return if (final < 1) null else final
        }
        return null
    }


}