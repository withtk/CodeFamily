package com.tkip.mycode.dialog.admin

import android.app.Dialog
import android.content.Context
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import com.tkip.mycode.R
import com.tkip.mycode.databinding.DialogPeerBinding
import com.tkip.mycode.dialog.OnDDD
import com.tkip.mycode.dialog.toast.toastNormal
import com.tkip.mycode.dialog.warn.PenaltyDialog
import com.tkip.mycode.funs.common.lllogD
import com.tkip.mycode.init.PUT_PEER
import com.tkip.mycode.init.base.getTypePosition
import com.tkip.mycode.init.my_bind.BindingDialog
import com.tkip.mycode.model.peer.Peer
import com.tkip.mycode.model.ticket.OnBill
import com.tkip.mycode.util.tools.etc.OnMyClick
import com.tkip.mycode.util.tools.fire.Firee
import com.tkip.mycode.util.tools.spinner.OnSpinner

class PeerDialog : BindingDialog<DialogPeerBinding>() {

    override fun getLayoutResId(): Int = R.layout.dialog_peer
    override fun getSize(): Pair<Float?, Float?> = Pair(null, null)

    private val peer by lazy { arguments?.getParcelable<Peer>(PUT_PEER) ?: Peer() }

    companion object {
        @JvmStatic
        fun newInstance(peer1: Peer) =
                PeerDialog().apply { arguments = Bundle().apply { putParcelable(PUT_PEER, peer1) } }
    }


    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
//        return super.onCreateDialog(savedInstanceState)

//        b = DataBindingUtil.inflate(LayoutInflater.from(activity as AppCompatActivity), R.layout.dialog_peer, null, false)

        b.dialog = this
        b.peer = peer
        b.inAllCredit.peer = peer


        val array = OnSpinner.getArrayPeerType()
        val spinAdapter = ArrayAdapter(activity as AppCompatActivity, android.R.layout.simple_spinner_item, array)
        spinAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        b.spinner.adapter = spinAdapter
        b.spinner.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onNothingSelected(parent: AdapterView<*>?) {
                lllogD("spin onNothingSelected")
            }

            override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
                lllogD("spin onItemSelected")
                lllogD("spinnerStatus : ${b.spinner.selectedItem}")
            }
        }

        b.spinner.setSelection(peer.type.getTypePosition(array))

        val builder = AlertDialog.Builder(activity as Context).apply {
            setView(b.root)
            setCancelable(true)
//            setOnCancelListener(DialogInterface.OnCancelListener { dialog -> mListener.onCancel()  })
        }

        return builder.create().apply {
            setCanceledOnTouchOutside(true)
            window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        }

    }

    /**     * 완전 삭제  :  child 데이타도 같이 지우는 로직으로 짜야..   */
    fun onClickDelete(v: View) {
        OnMyClick.setDisableAWhileBTN(v)
        OnDDD.pw(mContext ) {
            OnDDD.confirm(mContext,R.string.desc_delete_peer, ok = {
                Firee.deletePeer(peer,onSuccess = {
                    toastNormal(R.string.complete_delete)
                    dismiss()
                }, onFail = {})
            }, cancel = {})
        }
    }

    fun onClickCancel(v: View) {
        dialog?.cancel()
    }


    fun onClickOpenPenalty(v: View) {
        OnMyClick.setDisableAWhileBTN(v)

        /**         *  패널티 Dialog 오픈         */
        PenaltyDialog.newInstance(null, peer, onOk = {}).show((mContext as AppCompatActivity).supportFragmentManager, "dialog")
    }


    fun onClickOpenCost(v: View) {
        OnMyClick.setDisableAWhileBTN(v)

        /**         *  크레딧 부여 Dialog 오픈         */
        CreditHandleDialog.newInstance(peer, onOk1 = {}).show((mContext as AppCompatActivity).supportFragmentManager, "dialog")
    }

    fun onClickOpenBillList(v: View) {
        OnMyClick.setDisableAWhileBTN(v)
        OnBill.gotoBillListActivity(mContext, peer.uid)
    }


//    fun onClickOk() {
//        if (OnMyClick.isDoubleClick()) return
//    }


}