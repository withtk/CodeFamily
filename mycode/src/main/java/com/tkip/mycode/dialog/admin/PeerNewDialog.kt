package com.tkip.mycode.dialog.admin

import android.Manifest
import android.app.Activity
import android.app.Dialog
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.databinding.DataBindingUtil
import com.tkip.mycode.R
import com.tkip.mycode.databinding.DialogPeerNewBinding
import com.tkip.mycode.dialog.alert.MyAlert
import com.tkip.mycode.dialog.toast.TToast
import com.tkip.mycode.funs.common.OnPermission
import com.tkip.mycode.funs.common.getNullStr
import com.tkip.mycode.funs.common.getStr
import com.tkip.mycode.init.REQ_PICK
import com.tkip.mycode.init.REQ_READ_EXSTORAGE
import com.tkip.mycode.init.base.Cons
import com.tkip.mycode.init.base.getInt
import com.tkip.mycode.init.base.getStr
import com.tkip.mycode.init.my_bind.BaseDialog
import com.tkip.mycode.model.my_enum.LoginFrom
import com.tkip.mycode.model.my_enum.PeerType
import com.tkip.mycode.model.peer.Peer
import com.tkip.mycode.util.lib.photo.util.PhotoUtil
import com.tkip.mycode.util.tools.etc.OnMyClick
import com.tkip.mycode.util.tools.spinner.OnSpinner

class PeerNewDialog : BaseDialog() {

    lateinit var b: DialogPeerNewBinding

    val nickUnder = R.integer.count_nick_text_minimum.getInt()
    val nickMoreThan = R.integer.count_nick_text_maximum.getInt()

    companion object {
        @JvmStatic
        fun newInstance() =
                PeerNewDialog().apply {
                    arguments = Bundle().apply {
                    }
                }
    }

    override fun getSize(): Pair<Float?, Float?> {
        return Pair(null, null)
    }

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
//        return super.onCreateDialog(savedInstanceState)

        b = DataBindingUtil.inflate(LayoutInflater.from(activity as AppCompatActivity), R.layout.dialog_peer_new, null, false)
        b.dialog = this

        initView()

        val builder = AlertDialog.Builder(activity as Context).apply {
            setView(b.root)
            setCancelable(false)
//            setOnCancelListener(DialogInterface.OnCancelListener { dialog -> mListener.onCancel()  })
        }

        return builder.create().apply {
            setCanceledOnTouchOutside(false)
            window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        }

    }

    private fun initView() {

        b.etNickName.setOnFocusChangeListener { _, isOn ->
            if (!isOn) {
                val text = b.etNickName.getStr()
                when {
                    text.length < nickUnder -> b.etlayNickName.error = String.format(R.string.info_nick_count_under.getStr(), nickUnder)
                    text.length > nickMoreThan -> b.etlayNickName.error = String.format(R.string.info_nick_count_more_than.getStr(), nickMoreThan)
                    else -> b.etlayNickName.error = null
                }
            }
        }

        val spinAdapter = ArrayAdapter(activity as AppCompatActivity, android.R.layout.simple_spinner_item, OnSpinner.getArrayPeerType())
        spinAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        b.spinner1.adapter = spinAdapter
        b.spinner1.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onNothingSelected(parent: AdapterView<*>?) {
                Log.d(Cons.TAG, "spin onNothingSelected")
            }

            override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
                Log.d(Cons.TAG, "spinnerStatus : ${b.spinner1.selectedItem}")
            }
        }
        b.spinner1.setSelection(4)  // 기본값


        val array2 = LoginFrom.values().asList()
        val spinAdapter2 = ArrayAdapter(activity as AppCompatActivity, android.R.layout.simple_spinner_item, array2)
        spinAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        b.spinner2.adapter = spinAdapter2
        b.spinner2.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onNothingSelected(parent: AdapterView<*>?) {
                Log.d(Cons.TAG, "spin onNothingSelected")
            }

            override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
            }
        }
        b.spinner2.setSelection(3)  // 기본값
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        when (requestCode) {
            REQ_READ_EXSTORAGE -> {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    onClickOpenGallery()    // 다시 들어가서 체크하고 method 실행
                } else {
                    OnPermission.informAbout_READ_PERMISSION(activity as AppCompatActivity)
                }
            }
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (resultCode == Activity.RESULT_OK) {
            when (requestCode) {
                REQ_PICK -> {
                    data?.data?.let { uri ->
                        b.iv.setImageURI(uri)

                        PhotoUtil.resetAllUriList()
                        PhotoUtil.photoUris.add(uri)
                        Log.d(Cons.TAG, "REQ_PICK uri : $uri")
                    }

                }
            }
        }

    }

    fun onClickOpenGallery() {
        val readPermission = ContextCompat.checkSelfPermission(activity as AppCompatActivity, Manifest.permission.READ_EXTERNAL_STORAGE)

        if (readPermission == PackageManager.PERMISSION_GRANTED) {
            startActivityForResult(OnPermission.intentPickSingle(), REQ_PICK)
        } else {
            TToast.showAlert(R.string.info_just_permission_denied)
            ActivityCompat.requestPermissions(activity as AppCompatActivity, arrayOf(Manifest.permission.READ_EXTERNAL_STORAGE), REQ_READ_EXSTORAGE)
        }
    }

    fun onClickCancel() {
        dismiss()
    }

    fun onClickOk() {
        if (OnMyClick.isDoubleClick()) return

        MyAlert.showConfirm(mContext, R.string.info_wanna_upload_add_store.getStr(), { arrangeInputs() }, {})
    }


    fun arrangeInputs() {
        if (OnMyClick.isDoubleClick()) return

        val peer = Peer()

        val nick = b.etNickName.getNullStr()
        val displayName = b.etDisplayName.getNullStr()
        val comment = b.etComment.getNullStr()
        val peerType = b.spinner1.selectedItem as PeerType
        val loginFrom = b.spinner2.selectedItem as LoginFrom

        peer.nick = nick ?: "nick"
        peer.dpName = displayName
        peer.comment = comment
        peer.type = peerType.no

        PhotoUtil.uploadUriSingle(activity as AppCompatActivity, peer,
                successUploading = { foto ->

                    if (foto != null) {
                        peer.faceThumb = foto.thumbUrl
                        peer.face = foto.url
                    }

//                    Firee.addBatchPeer(peer,
//                            success = {
//                                MyProgress.cancel()
//                                TToast.showNormal(R.string.complete_upload)
//                                dismiss()
//                            }, fail = {})
                },
                fail = { })

        Log.d(Cons.TAG, "arrangeInputs peer : ")

    }


}