package com.tkip.mycode.dialog.list_menu

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.RecyclerView
import com.tkip.mycode.databinding.HolderListMenuBinding

class ListMenuAdapter(var items: ArrayList<String>, var selection: (String) -> Unit) : RecyclerView.Adapter<ListMenuAdapter.ListMenuViewHolder>() {
    private lateinit var activity: AppCompatActivity

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ListMenuViewHolder {
        this.activity = parent.context as AppCompatActivity
        val binding = HolderListMenuBinding.inflate(LayoutInflater.from(parent.context),parent,false)
        return ListMenuViewHolder(binding)
    }

    override fun getItemCount(): Int {
        return items.size
    }

    override fun onBindViewHolder(h: ListMenuViewHolder, p: Int) {
        h.bind(items[p])
    }


    inner class ListMenuViewHolder(val b: HolderListMenuBinding) : RecyclerView.ViewHolder(b.root) {

        private lateinit var menuTitle: String

        fun bind(menuTitle: String) {
            this.menuTitle = menuTitle
            b.holder = this
            b.menuTitle = menuTitle
            b.executePendingBindings()
        }

        fun onClickCard() {

            selection(menuTitle)

        }
    }
}