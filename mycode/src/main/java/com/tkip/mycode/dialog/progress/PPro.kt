package com.tkip.mycode.dialog.progress

import android.os.Parcel
import android.os.Parcelable

data class PPro(

        var type: Int = PROG_BASIC,
        var info: String?,
        var hasBtn: Boolean = false

) : Parcelable {

    constructor() : this(info = null)
    constructor(hasBtn: Boolean) : this(
            type = PROG_BASIC,
            info = null,
            hasBtn = hasBtn
    )
    constructor(type: Int, hasBtn: Boolean) : this(
            type = type,
            info = null,
            hasBtn = hasBtn
    )


    constructor(source: Parcel) : this(

            source.readInt(),
            source.readString(),
            1 == source.readInt()

    )

    override fun describeContents() = 0

    override fun writeToParcel(dest: Parcel, flags: Int) = with(dest) {
        writeInt(type)
        writeString(info)
        writeInt((if (hasBtn) 1 else 0))
    }

    companion object {

        /********** progress ********************/
        const val PROG_BASIC = 100
        const val PROG_PERCENT = 200


        @JvmField
        val CREATOR: Parcelable.Creator<PPro> = object : Parcelable.Creator<PPro> {
            override fun createFromParcel(source: Parcel): PPro = PPro(source)
            override fun newArray(size: Int): Array<PPro?> = arrayOfNulls(size)
        }
    }
}









