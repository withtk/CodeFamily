package com.tkip.mycode.dialog.peer

import android.Manifest.permission.READ_EXTERNAL_STORAGE
import android.app.Activity
import android.app.Dialog
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.inputmethod.EditorInfo
import android.widget.TextView
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.databinding.DataBindingUtil
import com.tkip.mycode.R
import com.tkip.mycode.databinding.DialogPeerUpdateBinding
import com.tkip.mycode.dialog.OnDDD
import com.tkip.mycode.dialog.progress.MyCircle
import com.tkip.mycode.dialog.progress.WAIT_TYPE
import com.tkip.mycode.dialog.toast.TToast
import com.tkip.mycode.dialog.toast.toastNormal
import com.tkip.mycode.funs.common.OnPermission
import com.tkip.mycode.init.CH_NICK
import com.tkip.mycode.init.EES_PEER
import com.tkip.mycode.init.REQ_PICK
import com.tkip.mycode.init.REQ_READ_EXSTORAGE
import com.tkip.mycode.init.my_bind.BaseDialog
import com.tkip.mycode.model.peer.mypeer
import com.tkip.mycode.model.peer.myuid
import com.tkip.mycode.util.lib.photo.util.PhotoUtil
import com.tkip.mycode.util.tools.etc.OnMyClick
import com.tkip.mycode.util.tools.fire.Firee
import com.tkip.mycode.util.tools.fire.batch
import com.tkip.mycode.util.tools.fire.docRef
import com.tkip.mycode.util.tools.fire.myCommit

class PeerUpdateDialog : BaseDialog() {

    lateinit var b: DialogPeerUpdateBinding

    companion object {

        private lateinit var okListener: (nick: String) -> Unit
        private var nickCountMin: Int = 4
        private var nickCountMax: Int = 10
        private var strMin: String = ""
        private var strMax: String = ""

        @JvmStatic
        fun newInstance(ok: (name: String) -> Unit) =
                PeerUpdateDialog().apply {
                    arguments = Bundle().apply {
                        okListener = ok
                    }
                }
    }

    override fun getSize(): Pair<Float?, Float?> {
        return Pair(0.75f, null)
    }

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {

        b = DataBindingUtil.inflate(LayoutInflater.from(activity), R.layout.dialog_peer_update, null, false)
        b.dialog = this
        b.peer = mypeer()


        activity?.let {
            nickCountMin = it.resources?.getInteger(R.integer.count_nick_text_minimum)!!
            nickCountMax = it.resources?.getInteger(R.integer.count_nick_text_maximum)!!
            strMin = String.format(it.getString(R.string.info_nick_count_under), nickCountMin)
            strMax = String.format(it.getString(R.string.info_nick_count_more_than), nickCountMax)
            b.etNick.hint = strMin
        }


        b.etNick.setOnEditorActionListener(TextView.OnEditorActionListener { _, actionId, _ ->
            if (actionId == EditorInfo.IME_ACTION_DONE) {
                onClickOk()
                return@OnEditorActionListener true
            }
            false
        })

        val builder = AlertDialog.Builder(activity as AppCompatActivity).apply {
            setView(b.root)
            setCancelable(true)
//            setOnCancelListener(DialogInterface.OnCancelListener { dialog -> mListener.onCancel()  })
        }

        return builder.create().apply {
            setCanceledOnTouchOutside(true)
            window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        }

    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (resultCode == Activity.RESULT_OK && requestCode == REQ_PICK)
            data?.data?.let { uri ->
                b.ivFace.setImageURI(uri)
                PhotoUtil.resetAllUriList()
                PhotoUtil.photoUris.add(uri)   // 선택한 uri list목록에 추가하기
                uploadFace()        // foto upload하고 peer 수정하기.
            }
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if (requestCode == REQ_READ_EXSTORAGE)
            if (grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED) onClickFace()    // 다시 들어가서 체크하고 startActi하면 됨.
            else OnPermission.informAbout_READ_PERMISSION(activity as AppCompatActivity)
    }

    fun onClickFace( ) {
        /*** openGallery */
        val readPermission = ContextCompat.checkSelfPermission(activity as AppCompatActivity, READ_EXTERNAL_STORAGE)
        if (readPermission == PackageManager.PERMISSION_GRANTED) startActivityForResult(OnPermission.intentPickSingle(), REQ_PICK)
//        if (readPermission == PackageManager.PERMISSION_GRANTED) startActivityForResult(OnPermission.intentPickMulti(), REQ_PICK_MULTI)
        else ActivityCompat.requestPermissions(activity as AppCompatActivity, arrayOf(READ_EXTERNAL_STORAGE), REQ_READ_EXSTORAGE)
    }

    fun onClickDelete(v: View) {
        OnMyClick.setDisableAWhileBTN(v)
        OnDDD.delete(mContext,
                ok = {
                    Firee.updatePeerFace(myuid(),null,null, mypeer().faceThumb, mypeer().face,
                            success = {
                                toastNormal(R.string.complete_delete)
                                dismiss()
                            }, fail = {
                    })
                }, cancel = {})
    }

    private fun uploadFace() {
        PhotoUtil.uploadUriSingle(activity as AppCompatActivity, mypeer(),
                successUploading = {
                    Firee.updatePeerFace(myuid(),PhotoUtil.urlMap[0],PhotoUtil.urlMap[1], mypeer().faceThumb, mypeer().face,
                            success = {
                                MyCircle.cancel()
                                toastNormal(R.string.complete_update)
                                dismiss()
                            }, fail = {
                    })
                },
                fail = { })
    }


    fun onClickCancel() {
        dismiss()
    }

    fun onClickOk() {

        val strNick = b.etNick.text.trim().toString().replace("\\s".toRegex(), "") // 공백 제거.

        /** 변경없으면 dismiss.*/
        if (strNick == mypeer().nick) {
            dismiss()
            return
        }

        if (strNick.length < nickCountMin) {  // 글자수 적을때
            TToast.showAlert(strMin)
            b.etNick.setText(strNick)
            b.etNick.setSelection(b.etNick.length())
            return
        }

        if (strNick.length > nickCountMax) { // 글자수 많을때
            TToast.showAlert(strMax)
            b.etNick.setText(strNick)
            b.etNick.setSelection(b.etNick.length())
//            b.etNick.setText("")
            return
        }

        WAIT_TYPE.FULL_AV.show(mContext)
//        MyProgress.show(activity as AppCompatActivity, PPro(false))

        Firee.existNickName(strNick,
                success = { isExist ->
                    if (isExist) {
//                        MyProgress.cancel()
                        MyCircle.cancel()
                        TToast.showAlert(R.string.error_nick_name_already)
                    } else {
                        batch().apply {
                            update(docRef(EES_PEER, myuid()), CH_NICK, strNick)
                            myCommit("updatePeerNick",
                                    success = {
                                        MyCircle.cancel()
//                                        MyProgress.cancel()
                                        okListener(strNick)
                                        TToast.showNormal(R.string.complete_update)
                                        dismiss()
                                    }, fail = {})
                        }
                    }
                },
                fail = { })

    }


}
