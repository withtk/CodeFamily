package com.tkip.mycode.dialog.warn

import android.app.Dialog
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AlertDialog
import com.tkip.mycode.R
import com.tkip.mycode.databinding.DialogPenaltyBinding
import com.tkip.mycode.dialog.OnDDD
import com.tkip.mycode.dialog.progress.MyCircle
import com.tkip.mycode.funs.common.OnValid
import com.tkip.mycode.funs.common.getNullStr
import com.tkip.mycode.funs.common.textListener
import com.tkip.mycode.init.PUT_PEER
import com.tkip.mycode.init.PUT_WARN
import com.tkip.mycode.init.my_bind.BindingDialog
import com.tkip.mycode.model.peer.Peer
import com.tkip.mycode.model.peer.mynick
import com.tkip.mycode.model.peer.myuid
import com.tkip.mycode.model.warn.OnPenalty
import com.tkip.mycode.model.warn.OnWarn
import com.tkip.mycode.model.warn.Warn
import com.tkip.mycode.util.tools.anim.visi
import com.tkip.mycode.util.tools.date.rightNow
import com.tkip.mycode.util.tools.etc.OnMyClick
import java.util.concurrent.TimeUnit

class PenaltyDialog : BindingDialog<DialogPenaltyBinding>() {
    override fun getLayoutResId(): Int = R.layout.dialog_penalty
    override fun getSize(): Pair<Float?, Float?> = Pair(0.95f, null)

    private val warn by lazy { arguments?.getParcelable<Warn>(PUT_WARN) ?: Warn() }
    private val peer by lazy { arguments?.getParcelable<Peer>(PUT_PEER) }
    private var onOk: ((Warn) -> Unit)? = null

    companion object {
        @JvmStatic
        fun newInstance(warn: Warn?, peer1: Peer?, onOk: ((Warn) -> Unit)?) =
                PenaltyDialog().apply {
                    arguments = Bundle().apply {
                        putParcelable(PUT_WARN, warn)
                        putParcelable(PUT_PEER, peer1)
                    }
                    this.onOk = onOk
                }
    }


    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
//        if (onOk == null) dismiss()

        b.dialog = this
        b.warn = warn
        b.peer = peer

        b.etPeriod.textListener(after = {}, before = { _, _, _, _ -> }, on = { s, _, _, c ->
            if (!s.isNullOrEmpty()) {
                b.lineEndDate.visi()
                s.toString().toInt().let { b.tvDate8.text = OnWarn.getDateLastDate(it).toString() } // lastDate set
            }
        })


        /***************************** BUILD ***************************************/
        val builder = AlertDialog.Builder(mContext).apply {
            setView(b.root)
            setCancelable(true)
//            setOnCancelListener(DialogInterface.OnCancelListener { dialog -> mListener.onCancel()  })
        }

        return builder.create().apply {
            setCanceledOnTouchOutside(true)
            window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        }

    }


    fun onClickCancel(v: View) {
        dialog?.dismiss()
    }

    fun onClickOk(v: View) {
        OnMyClick.setDisableAWhileBTN(v)


        onOk?.let {

            if (OnValid.hasError(b.etAmount, b.etPeriod)) return

            OnDDD.upload(mContext, ok = {
                warn.apply {

                    /*** 패널티 관련 */
                    amount = b.etAmount.getNullStr()?.toInt()
                    period = b.etPeriod.getNullStr()?.toInt()
                    period?.let { lastDate = rightNow() + TimeUnit.DAYS.toMillis(it.toLong()) }
                    reason = b.etReason.getNullStr()

                    /*** 타겟 어드민 */
                    peer?.let {
                        targetUID = it.uid
                        targetNick = it.nick
                    }

                    /*** 처리한 어드민 */
                    adminUID = myuid()
                    adminNick = mynick()

                    OnPenalty.handle(mContext, this,
                            success = {
                                MyCircle.cancel()
                                dialog?.dismiss()
                                it(this)
                            }, fail = {})
                }
            }, cancel = {})


        } ?: OnDDD.alertNullListner()


    }


}