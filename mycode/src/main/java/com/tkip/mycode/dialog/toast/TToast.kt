package com.tkip.mycode.dialog.toast

import android.view.Gravity
import android.view.LayoutInflater
import android.widget.Toast
import androidx.core.content.ContextCompat
import com.tkip.mycode.R
import com.tkip.mycode.databinding.ToastIconTextBinding
import com.tkip.mycode.init.base.getStr
import com.tkip.mycode.init.part.CodeApp

class TToast {


    companion object {

        const val T_HEIGHT = 200

        private fun showToastIconText(message: String?, colorRes: Int?, drawableRes: Int?, duration: Int?, gravity: Int?, x: Int, y: Int) {

            val context = CodeApp.getAppContext()
//            val view = LayoutInflater.from(context).inflate(  R.layout.toast_icon_text, null )

            val b = ToastIconTextBinding.inflate(LayoutInflater.from(context), null, false)


            b.tvMessage.text = message
//            b.tv_message.text = title
            colorRes?.let {
                b.card.setCardBackgroundColor(ContextCompat.getColor(context, it))
            }

            drawableRes?.let {
                //                view.iv_icon.setImageDrawable(ContextCompat.getDrawable(context, it))
                b.ivIcon.setImageDrawable(ContextCompat.getDrawable(context, it))
            }

            val toast = Toast(context)
            gravity?.let {
                toast.setGravity(gravity, x, y)
            }
            duration?.let {
                toast.duration = it
            }
            toast.view = b.root
            toast.show()
        }


        private fun showToastImageText(message: String, colorRes: Int?, drawableRes: Int?, duration: Int, gravity: Int, x: Int, y: Int) {
            val context = CodeApp.getAppContext()

//            val view = LayoutInflater.from(context).inflate(com.tkip.mycode.R.layout.toast_icon_text, null)
            val b = ToastIconTextBinding.inflate(LayoutInflater.from(context), null, false)

            b.tvMessage.text = message
//            view.tv_message.text = title
            colorRes?.let {
                b.card.setCardBackgroundColor(ContextCompat.getColor(context, it))
//                view.card.setCardBackgroundColor(ContextCompat.getColor(context, it))
            }

            drawableRes?.let {
                b.ivIcon.setImageDrawable(ContextCompat.getDrawable(context, it))
//                view.iv_icon.setImageDrawable(ContextCompat.getDrawable(context, it))
            }

            val toast = Toast(context)
            toast.setGravity(gravity, x, y)
            toast.duration = duration
            toast.view = b.root
            toast.show()
        }


        fun showNormal(stringId: Int) {
            val message = stringId.getStr()
            showToastIconText(message, R.color.toast_icon_text_background_normal, null, Toast.LENGTH_SHORT, Gravity.TOP, 0, T_HEIGHT)
        }

        fun showNormal(message: String?) {
            showToastIconText(message, R.color.toast_icon_text_background_normal, null, Toast.LENGTH_SHORT, Gravity.TOP, 0, T_HEIGHT)
        }

        fun showAlert(stringId: Int) {
            val message = stringId.getStr()
            showToastIconText(message, R.color.toast_icon_text_background_alert, R.drawable.ic_whatshot, Toast.LENGTH_LONG, Gravity.TOP, 0, T_HEIGHT)
        }

        fun showAlert(message: String) {
            showToastIconText(message, R.color.toast_icon_text_background_alert, R.drawable.ic_whatshot, Toast.LENGTH_LONG, Gravity.TOP, 0, T_HEIGHT)
        }

        fun showWarning(stringId: Int) {
            val message = stringId.getStr()
            showToastIconText(message, R.color.toast_icon_text_background_warning, R.drawable.ic_whatshot, Toast.LENGTH_LONG, Gravity.TOP, 0, T_HEIGHT)
        }

        fun showWarning(message: String) {
            showToastIconText(message, R.color.toast_icon_text_background_warning, R.drawable.ic_whatshot, Toast.LENGTH_LONG, Gravity.TOP, 0, T_HEIGHT)
        }


    }


}