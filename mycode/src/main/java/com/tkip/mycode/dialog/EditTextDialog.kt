package com.tkip.mycode.dialog

import android.app.Dialog
import android.content.Context
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.view.KeyEvent
import android.view.LayoutInflater
import android.view.inputmethod.EditorInfo
import android.widget.TextView
import androidx.appcompat.app.AlertDialog
import androidx.databinding.DataBindingUtil
import com.tkip.mycode.R
import com.tkip.mycode.databinding.DialogEditTextBinding
import com.tkip.mycode.dialog.progress.MyProgress
import com.tkip.mycode.dialog.progress.PPro
import com.tkip.mycode.dialog.toast.TToast
import com.tkip.mycode.init.CH_NICK
import com.tkip.mycode.init.base.getInt
import com.tkip.mycode.init.my_bind.BaseDialog
import com.tkip.mycode.model.peer.OnPeer


class EditTextDialog : BaseDialog() {

    lateinit var b: DialogEditTextBinding

    companion object {
        private lateinit var okListener: (nick: String) -> Unit

        private var title: String = "Title"
        private var description: String? = null
        private var isCancel: Boolean = false
        private var isCancelTouch: Boolean = false

        //todo: 나중에 dimens.xml로 변경할 것.
        private var nickCountMin: Int = 4
        private var nickCountMax: Int = 10
        private var strMin: String = ""
        private var strMax: String = ""

        @JvmStatic
        fun newInstance(title: String, description: String, isCancel: Boolean, isCancelTouch: Boolean, ok: (name: String) -> Unit) =
                EditTextDialog().apply {
                    arguments = Bundle().apply {
                        EditTextDialog.title = title
                        EditTextDialog.description = description
                        EditTextDialog.isCancel = isCancel
                        EditTextDialog.isCancelTouch = isCancelTouch
                        EditTextDialog.okListener = ok
                    }
                }
    }

    /*override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(com.tkip.mycode.R.layout.dialog_edit_text, container, false)
    }*/

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
//        return super.onCreateDialog(savedInstanceState)

        b = DataBindingUtil.inflate(LayoutInflater.from(activity), R.layout.dialog_edit_text, null, false)
        b.dialog = this
        b.title = title
        b.description = description

        activity?.let {
            nickCountMin = R.integer.count_nick_text_minimum.getInt()
            nickCountMax = R.integer.count_nick_text_maximum.getInt()
            strMin = String.format(it.getString(R.string.info_nick_count_under), nickCountMin)
            strMax = String.format(it.getString(R.string.info_nick_count_more_than), nickCountMax)
            b.etNick.hint = strMin
        }

/*

        b.etNick.addTextChangedListener(object : TextWatcher {

            override fun afterTextChanged(s: Editable?) {
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
                b.btnOk.isEnabled = nickCountMin!! < count && count < nickCountMax!!
            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
            }
        })
*/

        b.etNick.setOnEditorActionListener(TextView.OnEditorActionListener { _, actionId, event ->
            if (actionId == EditorInfo.IME_ACTION_DONE
                    || actionId == EditorInfo.IME_ACTION_DONE
                    || event.action == KeyEvent.ACTION_DOWN
                    || event.keyCode == KeyEvent.KEYCODE_ENTER
            ) {
                onClickOk()
                return@OnEditorActionListener true
            }
            false
        })

        val builder = AlertDialog.Builder(activity as Context).apply {
            setView(b.root)
            setCancelable(isCancel)
//            setOnCancelListener(DialogInterface.OnCancelListener { dialog -> mListener.onCancel()  })
        }
        val dialog = builder.create().apply {
            setCanceledOnTouchOutside(isCancelTouch)
            window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        }

//        b.etNick.isFocusableInTouchMode = true
//        b.etNick.requestFocus()

//        MyKeyboard.show(b.etNick)

        return dialog


/*
        var dialog = Dialog(context).apply {
            requestWindowFeature(Window.FEATURE_NO_TITLE)
            setContentView(b.root)
            setCancelable(isCancelable)
            setCanceledOnTouchOutside(false)
            window.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        }


        return dialog*/

    }


    fun onClickCancel() {
        dismiss()
    }

    fun onClickOk() {

//        val strNick = b.etNick.text.toString().replace("\\s".toRegex(), "") // 공백 제거.
//        val strNick = b.etNick.text.toString().discard0() // 공백 제거.
        val strNick = b.etNick.text.toString()

        if (strNick.length < nickCountMin) {  // 글자수 적을때
            TToast.showAlert(strMin)
            b.etNick.setText(strNick)
            b.etNick.setSelection(b.etNick.length())
            return
        }
        if (strNick.length > nickCountMax) { // 글자수 많을때
            TToast.showAlert(strMax)
            b.etNick.setText(strNick)
            b.etNick.setSelection(b.etNick.length())
//            b.etNick.setText("")
            return
        }

        MyProgress.show(mContext, PPro(false))

        OnPeer.existPeerBody(CH_NICK, strNick,
                success = {
                    MyProgress.cancel()
                    if (it) {
                        TToast.showAlert(R.string.error_nick_name_already)
                    } else {
                        dismiss()
                        okListener(strNick)
                    }

                }, fail = { })


      /*  Firee.existNickName(strNick,
                { isExist ->
                    if (isExist) {
                        lllogI("isExist true")
                        MyProgress.cancel()
                        TToast.showAlert(R.string.error_nick_name_already)
                    } else {
                        lllogI("isExist false")
//                        MyKeyboard.defaultHide(b.etNick)
                        dismiss()
                        okListener(strNick)
                    }
                }, { })*/


//                    targetFragment?.onActivityResult(targetRequestCode, Activity.RESULT_OK, activity?.intent)   // ok() 리턴
//                    dismiss()


    }

    override fun getSize(): Pair<Float?, Float?> {
        return Pair(0.75f, null)
    }

}