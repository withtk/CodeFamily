package com.tkip.mycode.dialog

import android.app.Dialog
import android.content.Context
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.view.LayoutInflater
import android.view.Window
import com.tkip.mycode.databinding.DialogPlainBinding


object PlainDialog {

    private var dialog: Dialog? = null

    fun init(context: Context) {

    }

    fun show(context: Context, titleId: Int, descId: Int, ok: () -> Unit) {
        if (dialog?.isShowing == true) return

        val b = DialogPlainBinding.inflate(LayoutInflater.from(context), null, false)
        b.dialog = this
        b.title = context.getString(titleId)
        b.description = context.getString(descId)

        b.btnOk.setOnClickListener {
            ok()
            this.cancel()
        }
        b.btnCancel.setOnClickListener {
            this.cancel()
        }


/*
        if (hasBtn) {
            val btn = view.findViewById(com.tkip.mycode.R.id.btn1) as Button
            btn.visibility = View.VISIBLE
            btn.setOnClickListener {
                cancel()
            }
        }*/

        dialog = Dialog(context).apply {
            requestWindowFeature(Window.FEATURE_NO_TITLE)
            setContentView(b.root)
            setCancelable(false)
            setCanceledOnTouchOutside(false)
            window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
            show()
        }

    }

    fun cancel() {
        dialog?.let {
            if (it.isShowing) {
                it.dismiss()
                dialog = null
            }
        }
    }

/*

    fun onMyClick(v: View) {
        when (v.id) {
           R.id.btn_cancel -> {

//               (dialog?.context as AppCompatActivity).finish()
//                TToast.showWarning("cancel")
                cancel()
            }

            R.id.btn_ok -> {
//                TToast.showWarning("okokok")
                try {
                    val intent = Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS)
                            .setData(Uri.parse("package:" + (dialog?.context as AppCompatActivity).packageName))
                    (dialog?.context as AppCompatActivity).startActivity(intent)
                } catch (e: ActivityNotFoundException) {
                    val intent = Intent(Settings.ACTION_MANAGE_APPLICATIONS_SETTINGS)
                    (dialog?.context as AppCompatActivity).startActivity(intent)
                }
                cancel()
            }
        }
    }
*/


}