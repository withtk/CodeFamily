package com.tkip.mycode.dialog.admin

import android.app.Dialog
import android.content.Context
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AlertDialog
import com.tkip.mycode.R
import com.tkip.mycode.databinding.DialogSearchPeerBinding
import com.tkip.mycode.dialog.toast.TOAST_NORMAL
import com.tkip.mycode.dialog.toast.TOAST_WARN
import com.tkip.mycode.dialog.toast.checkAndToast
import com.tkip.mycode.funs.common.*
import com.tkip.mycode.init.CH_EMAIL
import com.tkip.mycode.init.CH_NICK
import com.tkip.mycode.init.CH_UID
import com.tkip.mycode.init.RV_SEARCH
import com.tkip.mycode.init.base.getStr
import com.tkip.mycode.init.my_bind.BindingDialog
import com.tkip.mycode.model.peer.Peer
import com.tkip.mycode.model.peer.PeerAdapter
import com.tkip.mycode.model.peer.isAdmin
import com.tkip.mycode.model.peer.isMe
import com.tkip.mycode.util.lib.retrofit2.ESget
import com.tkip.mycode.util.lib.retrofit2.ESquery
import com.tkip.mycode.util.tools.anim.*
import com.tkip.mycode.util.tools.etc.OnMyClick

/**
 * 유저 선택하기
 * for recommend
 */
class SearchPeerDialog : BindingDialog<DialogSearchPeerBinding>() {
    override fun getLayoutResId(): Int = R.layout.dialog_search_peer
    override fun getSize(): Pair<Float?, Float?> = Pair(null, null)

    private lateinit var peerAdapter: PeerAdapter
    private val peerList = arrayListOf<Peer>()
    private var onSuccess: ((Peer) -> Unit)? = null

    private var selPeer: Peer? = null

    companion object {
        @JvmStatic
        fun newInstance(onOk1: ((Peer) -> Unit)) = SearchPeerDialog().apply {
//            arguments = Bundle().apply {   }
            onSuccess = onOk1
        }
    }

    override fun onResume() {
        super.onResume()
        if (onSuccess == null) dismiss()
    }


    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        b.dialog = this

        initView()

        val builder = AlertDialog.Builder(activity as Context).apply {
            setView(b.root)
            setCancelable(true)
//            setOnCancelListener(DialogInterface.OnCancelListener { dialog -> mListener.onCancel()  })
        }

        return builder.create().apply {
            setCanceledOnTouchOutside(true)
            window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        }

    }

    private fun initView() {

        b.manual = R.string.title_search_peer_nick.getStr()

        /** *   adapter  */
        peerAdapter = PeerAdapter(peerList, RV_SEARCH, onItemClick = { peer1 ->
            if (peer1.uid.isMe().checkAndToast(TOAST_NORMAL, R.string.alert_search_peer_me)) return@PeerAdapter   // 본인선택시
            selPeer = peer1
            b.tvManual.goneFadeOut(false)   // 매뉴얼 사라지기.
            b.peer = peer1
            b.btnOk.isEnabled = true
            b.linePeer.visi()
            b.rv.gone()
            b.ivPeerFace.showOverUp(true)
        })
        b.rv.adapter = peerAdapter


        /** * 실시간 태그 검색 */
        b.etSearch.textListener(after = { s ->

            lllogD("########### SearchPeerDialog textListener s ${s.toString()}")


            OnVisible.toggleSimple(!s.isNullOrBlank(), b.btnClearEt)   // x버튼 토글
            b.btnSearch.isEnabled = !s.isNullOrBlank()        // search버튼 가능/불가


            b.etSearch.keyword()?.let { keyword ->
                if (keyword.length < 2) return@textListener

                val query = if (isAdmin()) ESquery.peerSearch(10, keyword, CH_NICK, CH_EMAIL) else ESquery.peerSearch(6, keyword, CH_NICK)
                ESget.getPeerList(query,
                        success = { list ->
                            lllogD("########### SearchPeerDialog list ${list?.size}")

                            peerList.clear()
                            list?.let { peerList.addAll(it) }
                            afterSearch()
                        }, fail = {})

            } ?: let {
                // keyword 널이면 list 지워.
                peerList.clear()
                afterSearch()
            }
        }, before = { _, _, _, _ -> }, on = { _, _, _, _ -> })


        b.btnClearEt.onClick {
            b.etSearch.setText("")
            hideRv()
//            gotoSearch()
        }
    }

    private fun afterSearch() {
        AnimUtil.hideFadeOut(b.avLoading, true)
        if (peerList.isEmpty()) b.rv.gone()
        else b.rv.visi()
        peerAdapter.notifyDataSetChanged()
    }

    private fun hideRv() {
        b.rv.gone()
    }

    fun onCancel(v: View) {
        OnMyClick.setDisableAWhileBTN(v)
        dismiss()
    }

    fun onOk(v: View) {
        OnMyClick.setDisableAWhileBTN(v)
        if ((onSuccess == null).checkAndToast(TOAST_WARN, R.string.error_null_listener)) dismiss()

        lllogD("SearchPeerDialog onOk selPeer $selPeer")
        selPeer?.let {
            onSuccess?.invoke(it)
        }

        dismiss()
    }


}