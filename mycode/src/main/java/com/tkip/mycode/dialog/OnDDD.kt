package com.tkip.mycode.dialog

import android.content.Context
import com.tkip.mycode.R
import com.tkip.mycode.dialog.alert.MyAlert
import com.tkip.mycode.dialog.progress.MyCircle
import com.tkip.mycode.dialog.progress.WAIT_TYPE
import com.tkip.mycode.dialog.toast.toastAlert
import com.tkip.mycode.init.base.getStr
import com.tkip.mycode.model.peer.isAdmin
import com.tkip.mycode.model.peer.isSupervisor

class OnDDD {

    companion object {

        fun info(context: Context, strRes: Int) {
            info(context,strRes.getStr())
        }
        fun info(context: Context, str:String) {
            MyAlert.showInfo(context, str)
        }

        fun confirm(context: Context, str: String?, ok: () -> Unit, cancel: (() -> Unit)?) {
            MyAlert.showConfirm(context, str
                    ?: R.string.status_dialog_warn_handled.getStr(), ok = ok, cancel = cancel)
        }

        fun confirm(context: Context, strRes: Int?, ok: () -> Unit, cancel: (() -> Unit)?) {
            MyAlert.showConfirm(context, strRes?.getStr()
                    ?: R.string.status_dialog_warn_handled.getStr(), ok = ok, cancel = cancel)
        }

        fun delete(context: Context, ok: () -> Unit, cancel: () -> Unit) {
            MyAlert.showConfirm(context, R.string.alert_delete.getStr(), ok = { ok() }, cancel = { cancel() })
        }

        fun deepDel(context: Context, ok: () -> Unit, cancel: () -> Unit) {
            if(!isAdmin()) return
            confirm(context, R.string.desc_deep_delete_basic,
                    ok = { pw(context) { ok() } },
                    cancel = {
                        MyCircle.cancel()
                        cancel()
                    })
        }

        fun delete(context: Context, msg: String, ok: () -> Unit, cancel: () -> Unit) {
            MyAlert.showConfirm(context, msg, ok = { ok() }, cancel = { cancel() })
        }

        fun upload(context: Context, ok: () -> Unit, cancel: () -> Unit) {
            MyAlert.showConfirm(context, R.string.alert_add_object.getStr(), ok = { ok() }, cancel = { cancel() })
        }


        /**
         * admin권한으로 열어봅니다.
         */
        fun allowAdmin(context: Context, ok: () -> Unit) {
            MyAlert.showConfirm(context, R.string.info_open_admin_authorize.getStr(), ok = { ok() }, cancel = null)
        }


        /**
         * dialog에서 listener 잃어버렸을 때
         */
        fun alertNullListner() {
            toastAlert(R.string.error_null_listener)
        }


        /**
         * adminAction pw
         */
        fun pw(context: Context, ok: () -> Unit) {
            MyAlert.showPassword(context, R.string.remote_super_execute, ok)
        }

    }

}