package com.tkip.mycode.dialog.alert

import android.annotation.SuppressLint
import android.content.Context
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Handler
import android.text.method.ScrollingMovementMethod
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.Window
import android.widget.Button
import android.widget.EditText
import android.widget.RadioButton
import android.widget.Switch
import androidx.appcompat.app.AlertDialog
import com.cunoraz.tagview.TagView
import com.tkip.mycode.R
import com.tkip.mycode.ad.*
import com.tkip.mycode.ad.bid.BidAdapter
import com.tkip.mycode.ad.bid.SubSelectAdType
import com.tkip.mycode.ad.my_banner.CustomMyBannerList
import com.tkip.mycode.admin.menu.AMenu
import com.tkip.mycode.admin.notAllowAdAll
import com.tkip.mycode.databinding.*
import com.tkip.mycode.dialog.OnDDD
import com.tkip.mycode.dialog.progress.MyCircle
import com.tkip.mycode.dialog.progress.WAIT_TYPE
import com.tkip.mycode.dialog.toast.*
import com.tkip.mycode.funs.common.*
import com.tkip.mycode.funs.custom.number.ViewNumberBtn
import com.tkip.mycode.funs.search.view.CustomSearchAll4Bid
import com.tkip.mycode.init.CH_NICK
import com.tkip.mycode.init.base.*
import com.tkip.mycode.init.inter.interDialogBasicClick
import com.tkip.mycode.init.myTossList
import com.tkip.mycode.init.part.CodeApp
import com.tkip.mycode.model.bid.Bid
import com.tkip.mycode.model.board.Board
import com.tkip.mycode.model.cost.Cost
import com.tkip.mycode.model.cost.act.ActPeer
import com.tkip.mycode.model.cost.act.ActPeerAdapter
import com.tkip.mycode.model.cost.credit.OnPnt
import com.tkip.mycode.model.cost.pay.CustomPay
import com.tkip.mycode.model.grant.Grant
import com.tkip.mycode.model.grant.OnGrant
import com.tkip.mycode.model.grant.handle
import com.tkip.mycode.model.manage.SystemMan
import com.tkip.mycode.model.my_enum.*
import com.tkip.mycode.model.mytag.*
import com.tkip.mycode.model.peer.OnPeer
import com.tkip.mycode.model.peer.isPenalty
import com.tkip.mycode.model.peer.mypeer
import com.tkip.mycode.model.peer.myuid
import com.tkip.mycode.model.ticket.Ticket
import com.tkip.mycode.model.toss.Toss
import com.tkip.mycode.model.toss.TossAdapter
import com.tkip.mycode.model.warn.WarnType
import com.tkip.mycode.util.lib.calendar.CustomCalendarNormal
import com.tkip.mycode.util.lib.calendar.CustomCalendarViewRange
import com.tkip.mycode.util.lib.number_picker.getNumberView
import com.tkip.mycode.util.lib.number_picker.underMin
import com.tkip.mycode.util.lib.photo.Foto
import com.tkip.mycode.util.lib.photo.OnFoto
import com.tkip.mycode.util.lib.photo.glide.setThumbImageGlide
import com.tkip.mycode.util.my_view.MySquareImageView
import com.tkip.mycode.util.my_view.ViewUtil
import com.tkip.mycode.util.tools.anim.*
import com.tkip.mycode.util.tools.etc.MyKeyboard
import com.tkip.mycode.util.tools.fire.*
import com.tkip.mycode.util.tools.picker.addNumberPicker
import com.tkip.mycode.util.tools.picker.getAllPickerValue
import com.tkip.mycode.util.tools.picker.setupNumberPicker
import com.tkip.mycode.util.tools.picker.toggleBtn
import com.tkip.mycode.util.tools.radio.CustomRGstring
import com.tkip.mycode.util.tools.spinner.LinearSpinnerStatus


class MyAlert {

    companion object {

        /**         * small size         */
        fun getBuilderSmall(context: Context): AlertDialog.Builder = AlertDialog.Builder(context, R.style.MyAlertStyleSmall)

        /**         * normal size         */
        fun getBuilderNormal(context: Context): AlertDialog.Builder = AlertDialog.Builder(context, R.style.MyAlertStyleNormal)

        fun getBuilderBig(context: Context): AlertDialog.Builder = AlertDialog.Builder(context)

        fun commonShowMyAlert(dialog: AlertDialog, isCancel: Boolean, isCancelTouch: Boolean) {

//             WindowManager.LayoutParams().apply {
//
//            copyFrom(dialog.window.getAttributes())
//            width = 800
//            height = WindowManager.LayoutParams.WRAP_CONTENT
//             }

//            dialog.show()
//            val window = dialog.window
//            window.setAttributes(lp)


            dialog.apply {
                requestWindowFeature(Window.FEATURE_NO_TITLE)
                //                setContentView(view)
                setCancelable(isCancel)
                setCanceledOnTouchOutside(isCancelTouch)

                window?.setLayout(200, ViewGroup.LayoutParams.MATCH_PARENT)
                window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
                show()

            }
        }

        /******************************************************************************
         ***************************** showConfirm ************************************
         *****************************************************************************/
        fun showConfirm(context: Context, title: String?, description: String?, okText: String?, isCancel: Boolean, isCancelTouch: Boolean, ok: () -> Unit, cancel: (() -> Unit)?) {
            commomConfirm(context, title, description, okText, ok, cancel, isCancel, isCancelTouch)
        }

        fun showConfirm(context: Context, description: String?, ok: () -> Unit, cancel: (() -> Unit)?) {
            commomConfirm(context, null, description, null, ok, cancel, isCancel = true, isCancelTouch = true)
        }

        fun showConfirm(context: Context, description: String?) {
            commomConfirm(context, null, description, null, null, null, isCancel = true, isCancelTouch = true)
        }

        fun showConfirm(context: Context, description: String?, okText: String?, ok: () -> Unit, cancel: () -> Unit) {
            commomConfirm(context, null, description, okText, ok, cancel, isCancel = true, isCancelTouch = true)
        }

        fun showDelete(context: Context, ok: () -> Unit, cancel: () -> Unit) {
            val deleteMsg = R.string.alert_delete.getStr()
            showConfirm(context, deleteMsg, ok = { ok() }, cancel = { cancel() })
        }

        private fun commomConfirm(context: Context, title: String?, description: String?, okText: String?, ok: (() -> Unit)?, cancel: (() -> Unit)?, isCancel: Boolean, isCancelTouch: Boolean) {
            var dialog: AlertDialog? = null
            val builder = getBuilderSmall(context)
            val b = AlertConfirmBinding.inflate(LayoutInflater.from(context))
            b.hasCancel = (cancel != null)

            title?.let {
                b.tvTitle.text = it
                b.tvTitle.visibility = View.VISIBLE
            }
            description?.let {
                b.tvDescription.text = it
                b.tvDescription.visibility = View.VISIBLE
            }

            okText?.let {
                b.btnOk.text = it
            }
            b.btnOk.onClick {
                dialog?.dismiss()
                ok?.invoke()
            }

            if (cancel == null) b.btnCancel.gone() else b.btnCancel.visi()
            b.btnCancel.onClick {
                dialog?.dismiss()
                cancel?.invoke()
            }

            //            builder.setPositiveButton("sdf") { _, _ ->            }
            //            builder.setNeutralButton("sdf") { _, _ ->            }
            //            builder.setNegativeButton("sdf") { _, _ ->            }

            builder.setView(b.root)
            dialog = builder.create()
            commonShowMyAlert(dialog, isCancel, isCancelTouch)

            //                 시간 지나면 자동으로 dismiss
            //                        android.os.Handler().postDelayed({
            //                            cancel()
            //                        }, 3000)

//            dialog.setOnCancelListener {
//                TToast.showAlert("myalert cancel listener~!!")
//            }
        }

        /******************************************************************************
         ***************************** showInfo ************************************
         *****************************************************************************/
        fun showInfo(context: Context, description: String?) {
            commonInfo(context, null, description, null, null, null, isCancel = true, isCancelTouch = true)
        }

        fun showInfo(context: Context, title: String?, description: String?) {
            commonInfo(context, title, description, null, null, null, isCancel = true, isCancelTouch = true)
        }

        private fun commonInfo(context: Context, title: String?, description: String?, okText: String?, ok: (() -> Unit)?, cancel: (() -> Unit)?, isCancel: Boolean, isCancelTouch: Boolean) {
            var dialog: AlertDialog? = null
            val builder = getBuilderSmall(context)
            val b = AlertInfoBinding.inflate(LayoutInflater.from(context))
            b.title = title
            b.desc = description
            b.hasCancel = (cancel != null)

            title?.let {
                b.tvTitle.text = it
                b.tvTitle.visibility = View.VISIBLE
            }
            description?.let {
                b.tvDescription.text = it
                b.tvDescription.visibility = View.VISIBLE
            }
            okText?.let { b.btnOk.text = it }

            b.btnOk.onClick {
                dialog?.dismiss()
                ok?.invoke()
            }
            b.btnCancel.onClick {
                dialog?.dismiss()
                cancel?.invoke()
            }

            builder.setView(b.root)
            dialog = builder.create()
            commonShowMyAlert(dialog, isCancel, isCancelTouch)
        }

        fun showInfoLibrary(context: Context, title: String?, description: String?) {
            var dialog: AlertDialog? = null
            val builder = getBuilderBig(context)
            val b = AlertInfoLibraryBinding.inflate(LayoutInflater.from(context))

            b.title = title
            b.desc = description

            title?.let {
                b.tvTitle.text = it
                b.tvTitle.visibility = View.VISIBLE
            }
            description?.let {
                b.tvDescription.text = it
                b.tvDescription.visibility = View.VISIBLE
            }

            /*** list 만들기 */
            OnBasic.setLibraryList(context, b.lineCover)

            builder.setView(b.root)
            dialog = builder.create()
            commonShowMyAlert(dialog, true, true)
        }

        /**
         * 업데이트 알림
         */
        @SuppressLint("SetTextI18n")
        fun showUpdate(context: Context, onOk: () -> Unit, onForceCancel: () -> Unit, onCancel: () -> Unit) {
            var dialog: AlertDialog? = null
            val builder = getBuilderSmall(context)
            val b = AlertUpdateBinding.inflate(LayoutInflater.from(context))


            val myVerName = CodeApp.versionName
            val myVer = CodeApp.versionCode
            val forceVer = R.string.remote_force_version.remoteInt()
            val forceName = R.string.remote_force_version_name.remoteStr()
            val latestName = R.string.remote_latest_version_name.remoteStr()
            val isForce = myVer.lt(forceVer)

            lllogD("showUpdate myVer $myVer forceVer $forceVer isForce $isForce")

            b.force = isForce
            b.myVersionName = myVerName
            b.latestName = if (isForce) forceName else latestName

            if (isForce) Handler().postDelayed({ AnimUtil.showOverRight(b.tvForceDesc, true) }, 500)

            b.btnClose.onClick {
                dialog?.dismiss()
                onCancel()
            }
            b.btnShutdown.onClick {
                dialog?.dismiss()
                onForceCancel()
            }
            b.btnOk.onClick {
                dialog?.dismiss()
                onOk()
            }
            builder.setView(b.root)
            dialog = builder.create()
            commonShowMyAlert(dialog, false, false)
        }

        fun showNotice(context: Context, sys: SystemMan, okText: String?, ok: () -> Unit, cancel: (() -> Unit)?) {
//        fun showNotice(context: Context, notice: Notice) {
            var dialog: AlertDialog? = null
            val builder = getBuilderNormal(context)
            val b = AlertNoticeBinding.inflate(LayoutInflater.from(context))
            b.systemMan = sys
            b.okText = okText
            b.hasCancel = cancel != null

            b.btnCancel.onClick { dialog?.dismiss() }
            b.btnOk.onClick {
                ok()
                dialog?.dismiss()
            }
            builder.setView(b.root)
            dialog = builder.create()
            commonShowMyAlert(dialog, false, false)
        }

        /******************************************************************************
         ***************************** showWarn ************************************
         *****************************************************************************/
        fun showAddWarn(context: Context, arr: Array<WarnType>, onOk: (WarnType, String?) -> Unit, onCancel: () -> Unit) {
            var dialog: AlertDialog? = null
            val builder = getBuilderSmall(context)
            val b = AlertAddWarnBinding.inflate(LayoutInflater.from(context))

            arr.forEachIndexed { i, warnType ->
                b.rg.addView(RadioButton(context).apply {
                    id = i
                    text = warnType.title
                })
            }

            b.btnOk.onClick {

                /** 마지막으로 더 물어보기 */
                showConfirm(context, R.string.info_add_warn_last.getStr(),
                        ok = {
                            val resultWarnType = arr[b.rg.checkedRadioButtonId]
                            dialog?.dismiss()
                            onOk(resultWarnType, b.etComment.getNullStr())
                        }, cancel = {})

            }
            b.btnCancel.onClick {
                dialog?.dismiss()
                onCancel()
            }

            builder.setView(b.root)
            dialog = builder.create()
            commonShowMyAlert(dialog, true, false)

        }


        /******************************************************************************
         ***************************** showMenuContext ************************************
         *****************************************************************************/
        fun showMenuList(context: Context, arr: Array<AMenu>, onSelect: (AMenu) -> Unit) {
            var dialog: AlertDialog? = null
            val builder = getBuilderSmall(context)
            val b = AlertMenuBinding.inflate(LayoutInflater.from(context))
//            val v = LayoutInflater.from(context).inflate(R.layout.alert_menu, null)

//            val line = v.findViewById<LinearLayout>(R.id.line)
//            val style = ContextThemeWrapper(context, R.style.btnText_menu)

//            val arr = context.resources.getStringArray(arrayID)
            arr.forEach { amenu ->

                ViewUtil.addViewButtonLight(context, b.line, amenu, onClick = {
                    onSelect(it)
                    dialog?.dismiss()
                })

//                line.addView(LayoutInflater.from(context).inflate(R.layout.view_button_light, null).apply {
//                    val btn = (this as ViewGroup).getChildAt(0) as Button
//                    btn.onClick {
//                        if (OnMyClick.isDoubleClick()) return@setOnClickListener
//                        onSelect(aMenu.title)
//                    }
//                    btn.text = aMenu.title
//                })
            }

            builder.setView(b.root)
            dialog = builder.create()
            commonShowMyAlert(dialog, true, true)

        }


        /******************************************************************************
         ***************************** showInfoBoard ************************************
         *****************************************************************************/
        fun showBoardInfo(context: Context, board: Board) {
            var dialog: AlertDialog? = null
            val builder = getBuilderNormal(context)
            val b = AlertBoardInfoBinding.inflate(LayoutInflater.from(context))
            b.board = board
            b.tvDescription.movementMethod = ScrollingMovementMethod()

            builder.setView(b.root)
            dialog = builder.create()
            commonShowMyAlert(dialog, true, true)
        }

        fun showBoardPassword(context: Context, board: Board, onPass: () -> Unit) {
            var dialog: AlertDialog? = null
            val builder = getBuilderNormal(context)

            val b = AlertBoardPasswordBinding.inflate(LayoutInflater.from(context))

            b.btnOk.onClick {
            }

            builder.setView(b.root)
            dialog = builder.create()
            commonShowMyAlert(dialog, true, true)
        }

        /******************************************************************************
         ***************************** sttPeer ************************************
         *****************************************************************************/
        /**         * 다른 Peer 정보        */
        fun showPeerInfo(context: Context, uid: String, onLobby: () -> Unit) {
            var dialog: AlertDialog? = null
            val builder = getBuilderSmall(context)
            val b = AlertPeerInfoBinding.inflate(LayoutInflater.from(context))
            Firee.getPeer(uid,
                    success = { peer ->
                        b.peer = peer
                        b.btnBlock.onClick {
                            //todo : 차단하기
                        }

                    }, fail = {})

            builder.setView(b.root)
            dialog = builder.create()
            commonShowMyAlert(dialog, true, true)
        }

        /******************************************************************************
         ***************************** sttFoto ************************************
         *****************************************************************************/
//        fun showFotoCodmment(context: Context, items: ArrayList<Foto>, foto: Foto, onCancel: () -> Unit) {
//            commonFotoComment(context, items, foto, onCancel)
//        }

        fun showFotoComment(context: Context, items: ArrayList<Foto>, foto: Foto, onCancel: () -> Unit) {
            var dialog: AlertDialog? = null
            val builder = getBuilderNormal(context)
            val view = LayoutInflater.from(context).inflate(R.layout.alert_foto_comment, null)

            val iv = view.findViewById<MySquareImageView>(R.id.iv_photo)
            iv.setThumbImageGlide(foto)
            val sw = view.findViewById<Switch>(R.id.sw_main)
            if (items[0].key == foto.key) sw.gone()
            val et = view.findViewById<EditText>(R.id.et_comment)
            et.setText(foto.comment)
            view.findViewById<Button>(R.id.btn_ok).apply { setOnClickListener { dialog?.cancel() } }

            builder.setView(view)
            dialog = builder.create()
            dialog.setOnCancelListener {
                if (sw.isChecked) OnFoto.resetMainFoto(items, foto)
                foto.comment = et.getNullStr()
                onCancel()
            }
            commonShowMyAlert(dialog, true, true)

        }

        /******************************************************************************
         ***************************** sttPhoto ************************************
         *****************************************************************************/
        fun showPhoto(context: Context, url: String?) {
            var dialog: AlertDialog? = null
            val builder = getBuilderNormal(context)
            val b = AlertStoreMenuBinding.inflate(LayoutInflater.from(context))
            b.url = url

            builder.setView(b.root)
            dialog = builder.create()
            commonShowMyAlert(dialog, true, true)

        }

        /******************************************************************************
         ***************************** nick ************************************
         *****************************************************************************/
        fun showCreateNick(context: Context, pass: (String) -> Unit) {
            var dialog: AlertDialog? = null
            val builder = getBuilderNormal(context)
            val b = AlertCreateNickBinding.inflate(LayoutInflater.from(context))
            b.title = R.string.title_setup_nick_name.getStr()
            b.helper = R.string.desc_setup_nick_name.getStr()
            b.hint = R.string.name_setup_nick_name.getStr()

            val minCount = R.integer.count_nick_text_minimum.getInt()

//            b.layNick.setOnFocusChangeListener { _, isOn ->
//                /** 미니멈 length 체크 */
//                if (isOn && b.etNick.getStr().length < minCount) b.layNick.error = String.format(R.string.info_nick_count_under.getStr(), minCount)
//            }
//            b.etNick.textListener(after = {}, before = { _, _, _, _ -> }, on = { s, _, _, c ->
//                b.btnOk.isEnabled = (c > 2)
//            })

            b.btnOk.onClick {
                if ((b.etNick.getStr().length < minCount).checkAndToast(TOAST_NORMAL, String.format(R.string.info_nick_count_under.getStr(), minCount))) return@onClick

//                if (b.etNick.getNullStr() == null || OnValid.hasError(b.layNick)) return@onClick

                WAIT_TYPE.FULL_AV.show(context)
                OnPeer.existPeerBody(CH_NICK, b.etNick.getStr(),
                        success = {
                            if (it) {
                                TToast.showAlert(R.string.error_nick_name_already)
                                MyCircle.cancel()
                            } else {
                                pass(b.etNick.getStr())
                                dialog?.dismiss()
                            }
                        }, fail = { MyCircle.cancel() })
            }

            builder.setView(b.root)
            dialog = builder.create()
            commonShowMyAlert(dialog, true, true)

        }


        /******************************************************************************
         ***************************** showFotoComment ************************************
         *****************************************************************************/

        /**         * 결제창         */
//        fun showCreditPay(context: Context, actType: ActType, ok: () -> Unit, cancel: () -> Unit) {
//            var dialog: AlertDialog? = null
//            val builder = getBuilderNormal(context)
//
//            val b = AlertMyCreditBinding.inflate(LayoutInflater.from(context))
//            b.inMyCredit.credit = mycredit()
//            b.actType = actType
//            b.peer = mypeer()
//            val reqCredit = actType.getCrdAmount()
//            b.requireCredit = reqCredit
//
//            if (mycredit().gte(reqCredit)) {
//                b.msg = R.string.query_wanna_pay_credit.getStr()
//            } else {
//                b.tvMsg.setTextColor(R.color.colorAccent2.getClr())
//                b.msg = R.string.error_not_enough_credit.getStr()
//            }
//
//            b.btnOk.onClick {
//                dialog?.dismiss()
//                ok()
//            }
//            b.btnCancel.onClick {
//                dialog?.dismiss()
//                cancel()
//            }
//
//            builder.setView(b.root)
//            dialog = builder.create()
//            commonShowMyAlert(dialog, true, true)
//        }

        /*** 크레딧 전환 */
        fun selectPrice(context: Context,any:Any, price: Int, min: Int, onSuccess: (String, Int) -> Unit) {
            var dialog: AlertDialog? = null
            val b = AlertSelectPriceBinding.inflate(LayoutInflater.from(context))

            /*** 금액 버튼 뷰 붙이기 */
//            val viewNumber = ViewNumberBtn(context, null, null, null, onMaan = { OnPnt.calAndReset(b, it) }, onChun = { OnPnt.calAndReset(b, it) }, onBaek = { OnPnt.calAndReset(b, it) }, onShip = { OnPnt.calAndReset(b, it) }, null, null, onInit = { OnPnt.calAndReset(b, it) })

            b.price = price
            b.lineNumber.addView(b.getNumberView(context,any))

            b.btnOk.onClick {
                if (b.underMin(min)) return@onClick
                onSuccess(b.tvPrice.getStr(), b.tvPrice.getInt())
                dialog?.dismiss()
            }
            b.btnClose.onClick { dialog?.dismiss() }

            val builder = getBuilderBig(context)
            builder.setView(b.root)
            dialog = builder.create()
            commonShowMyAlert(dialog, isCancel = true, isCancelTouch = false)
        }

        /*** 크레딧 전환 */
        fun convertCredit(context: Context, ok: () -> Unit) {
            var dialog: AlertDialog? = null
            val b = AlertChangeCreditBinding.inflate(LayoutInflater.from(context))

            b.peer = mypeer()
            val viewNumber = ViewNumberBtn(context, null,  null, null, null, onMaan = { OnPnt.calAndReset(b, it) }, onChun = { OnPnt.calAndReset(b, it) }, onBaek = { OnPnt.calAndReset(b, it) }, onShip = { OnPnt.calAndReset(b, it) }, null, null, onInit = { OnPnt.calAndReset(b, it) })
            b.lineNumber.addView(viewNumber)

            b.btnChange.onClick {
                val msg = String.format(R.string.ask_change_pnt.getStr(), b.tvPnt.text, b.tvBcrd.text)
                OnDDD.confirm(context, msg,
                        ok = {
                            b.progress.visi()
                            OnVisible.enable(false, b.btnChange)
                            viewNumber.resetEnable(false)
                            b.tvInfo.text = R.string.info_change_pnt_ing.getStr()
                            b.tvInfo.showFadeIn(true)

                            batch().apply {
                                addCost(Cost.new(null, ActType.PNT_TO_BCRD, null, b.tvPnt.getInt(), b.tvBcrd.getInt(), null, null))
                                myCommit("changeCredit",
                                        success = {
                                            lllogD("changeCredit changeCredit btnChange ok ")
                                            hhh(4000) {
                                                toastNormal(R.string.info_change_pnt_completed)
                                                ok()
                                                dialog?.dismiss()
                                            }
                                        }, fail = {})
                            }
                        }, cancel = {})
            }
            b.btnClose.onClick { dialog?.dismiss() }

            val builder = getBuilderBig(context)
            builder.setView(b.root)
            dialog = builder.create()
            commonShowMyAlert(dialog, isCancel = true, isCancelTouch = false)
        }


        /**         * 결제 명세서         */
        fun showPay(context: Context, ok: () -> Unit, cancel: () -> Unit) {
            var dialog: AlertDialog? = null
            val b = ViewEmptyCardviewBinding.inflate(LayoutInflater.from(context))
            b.line.addView(
                    CustomPay(context, onOk = {
                        dialog?.dismiss()
                        ok()
                    }, onCancel = {
                        dialog?.dismiss()
                        cancel()
                    }))

            val builder = getBuilderNormal(context)
            builder.setView(b.root)
            dialog = builder.create()
            commonShowMyAlert(dialog, isCancel = true, isCancelTouch = true)
        }


        /**      * 구매완료 안내 다이얼로그     */
        fun showBillSuccess(context: Context, ticket: Ticket) {
            var dialog: AlertDialog? = null
            val builder = getBuilderSmall(context)

            val b = AlertBillSuccessBinding.inflate(LayoutInflater.from(context))
            b.ticket = ticket

            b.btnCancel.onClick { dialog?.dismiss() }

            builder.setView(b.root)
            dialog = builder.create()
            commonShowMyAlert(dialog, true, false)
        }

        /**         * 비밀번호 기입 다이얼로그    */
        fun showPassword(context: Context, remoteId: Int, onSuccess: () -> Unit) {
            var dialog: AlertDialog? = null
            val builder = getBuilderSmall(context)
            val b = AlertPasswordBinding.inflate(LayoutInflater.from(context))
            b.etPw.textListener(after = { s ->
                if (!s.isNullOrEmpty() && s.toString() == remoteId.remoteStr()) {
                    onSuccess()
                    dialog?.dismiss()
//                    MyKeyboard.hide(b.etPw)
                }
            }, before = { _, _, _, _ -> }, on = { _, _, _, _ -> })
//            MyKeyboard.show(b.etPw)  // 키보드 보이기
            builder.setView(b.root)
            dialog = builder.create()
            commonShowMyAlert(dialog, true, true)
        }


        /******************************************************************************
         ***************************** sttToss ************************************
         *****************************************************************************/
        fun showTossList(context: Context, onClick: ((Toss) -> Unit?)) {
            var dialog: AlertDialog? = null
            val builder = getBuilderNormal(context)

            val b = AlertTossListBinding.inflate(LayoutInflater.from(context))


            if (myTossList.isEmpty()) {
                AnimUtil.showSlideUp(b.tvNoData, false)
            } else {
                AnimUtil.hideNormal(b.tvNoData)
                val adapter = TossAdapter(myTossList,
                        onClick = { toss ->
                            onClick.invoke(toss)
                            dialog?.dismiss()
                        })
                b.rv.adapter = adapter
            }

//            b.btnOk.onClick {
//                dialog?.dismiss()
//            }

            builder.setView(b.root)
            dialog = builder.create()
            commonShowMyAlert(dialog, true, true)

        }

        fun showTossConfirm(context: Context, toss: Toss) {
            var dialog: AlertDialog? = null
            val builder = getBuilderNormal(context)

            val b = AlertTossConfirmBinding.inflate(LayoutInflater.from(context))
            b.toss = toss
            b.inToss.toss = toss

//            b.btnPaste.onClick {
//                onPaste()
//                dialog?.dismiss()
//            }

            builder.setView(b.root)
            dialog = builder.create()
            commonShowMyAlert(dialog, true, true)

        }

        fun showManualMore(context: Context, title: String, manual: String) {
            var dialog: AlertDialog? = null
            val builder = getBuilderBig(context)

            val b = AlertManualMoreBinding.inflate(LayoutInflater.from(context))
            b.title = title
            b.manual = manual

            builder.setView(b.root)
            dialog = builder.create()
            commonShowMyAlert(dialog, true, true)

        }


        /**
         * omit : 생략된 자릿수. 0, 10, 100, 1000
         * initCount : 초기화 picker 개수.추가버튼 gone. (null이면 추가버튼 생성.)
         * preValue : null 여부에 따라 resume인지 확인가능.
         */
        fun showNumberPicker(context: Context, pickerFactory: PickerFactory, preValue: String?, onOk: (Int, String) -> Unit) {
            var dialog: AlertDialog? = null
            val builder = getBuilderBig(context)

            val b = AlertNumberPickerBinding.inflate(LayoutInflater.from(context))

            pickerFactory.titleUnit?.let { b.tvUnit.text = getFormatMinCredit(it, pickerFactory.omitNumber) }
            b.linePicker.setupNumberPicker(context, pickerFactory, preValue)  // update
            b.linePicker.toggleBtn(pickerFactory.maxPicker, b.btnPlus)

            b.tvZero.text = pickerFactory.omitNumber.toString().substring(1)


            b.btnPlus.setOnClickListener {
                b.linePicker.addNumberPicker(context, pickerFactory, 0)
                b.linePicker.toggleBtn(pickerFactory.maxPicker, b.btnPlus)
            }

            b.btnInit.setOnClickListener {
                b.linePicker.removeAllViews()
                b.linePicker.setupNumberPicker(context, pickerFactory, null)
                b.linePicker.toggleBtn(pickerFactory.maxPicker, b.btnPlus)
//                b.linePicker.initAllPicker()
            }

            b.btnOk.onClick {
                val result = b.linePicker.getAllPickerValue()  // picker의 모든 숫자 붙여와.
                val resultInt = result * pickerFactory.omitNumber
                onOk(resultInt, (result * pickerFactory.omitNumber).getStringComma())
                dialog?.dismiss()
            }

            builder.setView(b.root)
            dialog = builder.create()
            commonShowMyAlert(dialog, false, false)
        }


        /**
         * tagView까지 모두 한번에 수정.
         * isOne : 한개만 필요할 때,
         * oneStr : update할 때. tagView에 셋팅해줘야 할 값.
         */
        fun showAddTag(context: Context, isOne: Boolean, oneStr: String?, tagType: TagTYPE, tagView: TagView?, onSingleResult: (String) -> Unit) {
            var dialog: AlertDialog? = null
            val builder = getBuilderBig(context)

//            OnTag.clearMyTagList()  // 업로드할 tagList 초기화.
            val b = AlertTagBinding.inflate(LayoutInflater.from(context))
            b.tagView.addTags(tagView?.tags)
            oneStr?.let { b.tagView.checkAdd(TAG_1, it) }  // 한개만 필요할 때 : tagView에 초기값 셋팅.

            //set click listener
            b.tagView.setOnTagClickListener { _, p ->
                /**   업로드할 tagList에서도 삭제   */
                OnTag.myUploadList.removeOne(b.tagView.tags[p].text)   // 업로드리스트에서 삭제
                b.tagView.remove(p)
                tagView?.remove(p)
            }

//            b.tagView.setOnTagDeleteListener { _, tag, _ ->            }
//            b.tagView.setOnTagLongClickListener { _, p -> b.tagView.remove(p) }

            /**             * init adapter             */
            val items = arrayListOf<MyTag>()
            val adapter = MyTagAdapter(items, MyTagAdapter.rvNormal,
                    /**   * MyTagAdapter에서  아이템 클릭.    */
                    onItemClick = { adap, p, myTag ->
                        if (b.tagView.isLimit10(true)) return@MyTagAdapter   // 태그  10개까지만.
                        if (isOne) b.tagView.removeAll()   // 한개만 필요할 때
                        b.tagView.checkAdd(TAG_1, myTag.name)
                        tagView?.checkAdd(TAG_1, myTag.name)
                        b.etSearch.setText("")
                        items.removeAt(p)
                        adap.notifyDataSetChanged()
                    })
            b.rv.adapter = adapter

            /**             * items 초기화             */
            adapter.setTagList(tagType, "", items, b.tagView, b.avLoading, b.tvNoResult)

            /**             * 실시간 검색 */
            b.etSearch.textListener(after = { s ->
                val keyword = s.toString()
                lllogI("etSearch keyword $keyword")
                OnVisible.toggleSimple(keyword.isNotEmpty(), b.btnClearEt)    // show/hide btnClear
                b.btnAdd.isEnabled = keyword.isNotEmpty() && (!b.tagView.has(keyword))   // btnAdd 활성화/비활성화
                adapter.setTagList(tagType, keyword, items, b.tagView, b.avLoading, b.tvNoResult)   // tagList 받아오기
            }, before = { _, _, _, _ -> }, on = { _, _, _, _ -> })

            /**             * 추가 버튼 */
            b.btnAdd.setOnClickListener {
                val keyword = b.etSearch.getStr().cutName(10)   //  트림
                if (!keyword.isBlank()) {
                    if (b.tagView.has(keyword)) {
                        toastNormal(R.string.alert_has_tag)    // 이미 있다. 알림.
                    } else {
                        /**   업로드할 tagList에 추가   */
                        OnTag.addNewTag(tagType, keyword)    // 업로드 리스트에 추가
                        if (isOne) b.tagView.removeAll()   // 한개만 필요할 때
                        b.tagView.checkAdd(TAG_1, keyword)
                        tagView?.checkAdd(TAG_1, keyword)
                    }
                }
                b.etSearch.setText("")
            }

//            b.btnSearch.setOnClickListener { adapter.setTagList(index, b.etSearch.text.toString().trim(), items, tagView, b.tagView, b.avLoading) }
            b.btnComplete.onClick {
                b.tagView.checkBanWord(
                        hasBan = {
                            if (it) {
                                toastAlert(R.string.alert_has_ban_tag)
                            } else {
                                dialog?.dismiss()
                                MyKeyboard.hide(b.etSearch)
                                if (isOne) if (b.tagView.tags.isNotEmpty()) onSingleResult(b.tagView.tags[0].text)
                            }
                        })
            }

            b.btnClearTag.onClick {
                b.tagView.removeAll()
                tagView?.removeAll()
            }

            b.btnClearEt.onClick {
                b.etSearch.setText("")
                items.clear()
                adapter.notifyDataSetChanged()
            }


            builder.setView(b.root)
            dialog = builder.create()
            commonShowMyAlert(dialog, false, false)

        }


        /******************************************************************************
         ***************************** sttActPeer ************************************
         *****************************************************************************/
        fun showActPeerList(context: Context, msg: String?, actPeerList: ArrayList<ActPeer>, onOk: (() -> Unit?)) {
            var dialog: AlertDialog? = null
            val builder = getBuilderNormal(context)

            val b = AlertActpeerListBinding.inflate(LayoutInflater.from(context))
            b.desc = msg
            val adapter = ActPeerAdapter(actPeerList, null)
            b.rv.adapter = adapter
            adapter.notifyDataSetChanged()

            b.btnCancel.onClick {
                dialog?.dismiss()
            }
            b.btnOk.onClick {
                onOk()
                dialog?.dismiss()
            }

            builder.setView(b.root)
            dialog = builder.create()
            commonShowMyAlert(dialog, true, true)

        }

        /******************************************************************************
         ***************************** sttstatus ************************************
         *****************************************************************************/

        /**         * for admin         */
        fun showSelectStatus(context: Context, any: Any, onSelect: (status: Status) -> Unit, onCancel: () -> Unit) {
            var dialog: AlertDialog? = null
            val builder = getBuilderNormal(context)
            val b = AlertSelectStatusBinding.inflate(LayoutInflater.from(context))

            lllogD("showUpdateStatus showSelectStatus any $any")
            val statusList = any.getMyDataType().statusList()
            var status = Status.NOTHING

            /**   status 스피너     */
            b.frame.addView(LinearSpinnerStatus(context, true, statusList, any.anyStatusNo(), onClick = { status = it }))

            b.btnCancel.onClick { dialog?.dismiss() }
            b.btnOk.onClick {
                if (status != Status.NOTHING) onSelect(status)
                else {
                    onCancel()
                    toastNormal(R.string.info_not_selected_status)
                }
                dialog?.dismiss()
            }

            builder.setView(b.root)
            dialog = builder.create()
            commonShowMyAlert(dialog, true, true)
        }

        /**         * for admin         */
        fun showUpdateStatus(context: Context, any: Any, onSuccess: ((status: Status?, grant: Grant?) -> Unit)?) {
            var dialog: AlertDialog? = null
            val builder = getBuilderNormal(context)
            val b = AlertUpdateStatusBinding.inflate(LayoutInflater.from(context))

            lllogD("showUpdateStatus showUpdateStatus any $any")
            val statusList = any.getMyDataType().statusList()
            var status = any.anyStatusNo().getStatus()

            /**   status 스피너     */
            b.frame.addView(LinearSpinnerStatus(context, true, statusList, any.anyStatusNo(), onClick = {
                status = it
                if (it == Status.NORMAL) {
                    b.layReason.hideFadeOut(false)
                    b.etReason.setText("")
                } else b.layReason.showFadeIn(false)
            }))

            b.etReason.setOnFocusChangeListener { _, _ ->
                b.linePush.visi()
            }

            b.btnCancel.onClick { dialog?.dismiss() }
            b.btnOk.onClick {
                showConfirm(context, R.string.warn_handle_desc.getStr(),
                        ok = {
                            b.avLoading.visi()

                            batch().apply {
                                OnGrant.getGrant(any) { g ->
                                    val ggg = OnGrant.finalGrant(true, g, any, b.etReason.getNullStr()).apply {
                                        statusNo = if (b.cbHandle.isChecked) Status.GRANT_HANDLED.no else Status.GRANT_REQUESTED.no  // check 핸들 처리
                                        if (b.cbPush.isChecked) push += 1
                                    }

                                    lllogD("showUpdateStatus showUpdateStatus g $g \nggg $ggg")
                                    this.addGrant(ggg)   // add Grant
                                    this.updateStatus(any.anyChild(), any.anyKey(), status)   // update status

                                    myCommit("showUpdateStatus ", success = {
                                        lllogD("OnBanner addPhotoAndBanner grant:$ggg  ")
                                        toastNormal(R.string.complete_upload)
                                        onSuccess?.invoke(status, ggg)
                                        OnGrant.deepDel(status, any)
                                        dialog?.dismiss()
                                    }, fail = { b.avLoading.hide() })
                                }
                            }



                            /*** reason값이 있으면 add grant -> get or New -> add
                             *  Grant는 reason과 comment값을 저장하기 위해 존재. */
//                            val reason = b.etReason.getNullStr()
//                            if (reason == null)
//                                commonStatusUpdate(any.anyChild(), any.anyKey(), status,
//                                        success = {
//                                            onSuccess?.invoke(status, null)
//                                                    ?: OnGrant.deepDel(status, any)
//
//                                            dialog?.dismiss()
//                                        }, fail = { b.avLoading.hide() })
//                            else
//                                batch().apply {
//                                    OnGrant.getGrant(any) { g ->
//                                        val ggg = OnGrant.finalGrant(true, g, any, b.etReason.getNullStr()).apply {
//                                            statusNo = if (b.cbHandle.isChecked) Status.GRANT_HANDLED.no else Status.GRANT_REQUESTED.no  // check 핸들 처리
//                                            if (b.cbPush.isChecked) push += 1
//                                        }
//
//                                        lllogD("showUpdateStatus showUpdateStatus g $g \nggg $ggg")
//                                        this.addGrant(ggg)   // add Grant
//                                        this.updateStatus(any.anyChild(), any.anyKey(), status)   // update status
//
//                                        myCommit("showUpdateStatus ", success = {
//                                            lllogD("OnBanner addPhotoAndBanner grant:$ggg  ")
//                                            toastNormal(R.string.complete_upload)
//                                            onSuccess?.invoke(status, ggg)
//                                            OnGrant.deepDel(status, any)
//                                            dialog?.dismiss()
//                                        }, fail = { b.avLoading.hide() })
//                                    }
//                                }
                        }, cancel = {})
            }

            builder.setView(b.root)
            dialog = builder.create()
            commonShowMyAlert(dialog, true, true)

        }

        fun showInfoGrant(context: Context, grant: Grant?, any: Any, onGoto: () -> Unit, onDeleted: () -> Unit) {
            var dialog: AlertDialog? = null
            val builder = getBuilderNormal(context)
            val b = AlertInfoStatusBinding.inflate(LayoutInflater.from(context))
            val statusNo = any.anyStatusNo()
            val availGrant = OnGrant.availRequestGrant(null, statusNo)
            lllogD("showInfoGrant showInfoGrant grant $grant")
            b.grant = grant
            b.statusNo = statusNo
            b.availGrant = availGrant

            b.btnCancel.onClick {
                dialog?.dismiss()
            }
            b.btnOk.onClick {
                dialog?.dismiss()
                if (availGrant) onGoto()
                else commonStatusUpdate(any.anyChild(), any.anyKey(), Status.DELETED_USER, success = { onDeleted() }, fail = { })
            }

            builder.setView(b.root)
            dialog = builder.create()
            commonShowMyAlert(dialog, true, true)

        }

        /**
         * AlertGrantBinding 공유
         * for admin
         */
        fun showGrant(context: Context, grant: Grant, onSuccess: (Grant) -> Unit, onFail: () -> Unit) {
            var dialog: AlertDialog? = null
            val builder = getBuilderNormal(context)
            val b = AlertGrantBinding.inflate(LayoutInflater.from(context))
            lllogD("showGrant  grant $grant")
            b.grant = grant
            b.isHandle = true
            b.modelTitle = grant.modelTitle
//            /**  스피너 : grantType   */
//            if(isAdmin()) LinearSpinnerString(context, true, null, TypeList.GRANT_TYPE, grant.grantTypeNo, onSelected = { grant.grantTypeNo = it }).apply { b.line.addView(this) }

//            /**         * Status 스피너     */
//            if(isAdmin())   b.line.addView(LinearSpinnerStatus(context, STATUS_LIST_GRANT, ticket.statusNo, onClick = { it.let { ticket.statusNo = it.no } }))

            lllogD("showGrant2  grant $grant")

            b.btnCancel.onClick { dialog?.dismiss() }
            b.btnOk.onClick {
                OnDDD.confirm(
                        context,
                        R.string.ask_grant_update_handled,
                        ok = {
                            //                            grant.statusNo = if (b.cbHandle.isChecked) Status.GRANT_REQUESTED.no else Status.GRANT_HANDLED.no   // setStatusNo
//                            if (b.cbPush.isChecked) grant.push += 1   // 푸시 설정

                            grant.handle(b.cbHandle.isChecked, b.cbPush.isChecked, b.etReason.getNullStr())
                            lllogD("showGrant showGrant  .b.cbHandle.isChecked  ${b.cbHandle.isChecked}")
                            lllogD("showGrant showGrant grant.handle $grant")
                            OnGrant.addGrant1111(true, grant,
                                    onSuccess = {
                                        onSuccess(it)
                                        dialog?.dismiss()
                                    },
                                    onFail = {
                                        onFail()
                                        dialog?.dismiss()
                                    })
                        },
                        cancel = {}
                )

            }

            builder.setView(b.root)
            dialog = builder.create()
            commonShowMyAlert(dialog, true, true)
        }

        /**
         * AlertGrantBinding 공유
         * for user
         */
        fun showRequestGrant(context: Context, any: Any, onSuccess: (String?) -> Unit) {
            var dialog: AlertDialog? = null
            val builder = getBuilderNormal(context)
            val b = AlertGrantBinding.inflate(LayoutInflater.from(context))
            lllogD("showRequestGrant any $any")
            b.isHandle = false
            b.modelTitle = any.anyTitle()
            b.btnCancel.onClick { dialog?.dismiss() }
            b.btnOk.onClick {

                onSuccess(b.etComment.getNullStr())
                dialog?.dismiss()

//                OnGrant.request(false, any, b.etComment.getNullStr(), onSuccess = {
//                    onSuccess()
//                    dialog?.dismiss()
//                }, onFail = {
//                    onFail()
//                    dialog?.dismiss()
//                })
            }

            builder.setView(b.root)
            dialog = builder.create()
            commonShowMyAlert(dialog, true, true)
        }


        /******************************************************************************
         ***************************** sttBid ************************************
         *****************************************************************************/
        fun showBidMenu(context: Context, bid: Bid?) {
            var dialog: AlertDialog? = null
            val builder = getBuilderNormal(context)
            val b = AlertBidMenuBinding.inflate(LayoutInflater.from(context))
            b.btnStatus.onClick { OnBid.gotoBidStateActivity(context, bid, BidAdapter.rvBidState) }
            b.btnAdd.onClick {
                if (!isPenalty(context)) {
                    if (notAllowAdAll()) return@onClick
                    OnBid.gotoAddBidActivity(context)
                }
            }
            b.btnMyBanner.onClick { if (!isPenalty(context)) OnBanner.gotoMyBannerActivity(context, myuid(), null) }
            b.btnMyBid.onClick { OnBid.gotoBidStateActivity(context, bid, BidAdapter.rvMyBid) }

            /*** for admin */
            b.btnAllBid.onClick { OnBid.gotoBidStateActivity(context, bid, BidAdapter.rvAllBid) }

            builder.setView(b.root)
            dialog = builder.create()
            commonShowMyAlert(dialog, true, true)

        }

        fun showAdTypeManual(context: Context, adType: AdType) {
            var dialog: AlertDialog? = null
            val builder = getBuilderBig(context)
            val b = AlertAdtypeManualBinding.inflate(LayoutInflater.from(context))
            b.adType = adType

            builder.setView(b.root)
            dialog = builder.create()
            commonShowMyAlert(dialog, true, true)

        }

        fun showADDBanner(context: Context, bid: Bid, onResult: (Bid) -> Unit) {
            var dialog: AlertDialog? = null
            val builder = getBuilderNormal(context)
            val b = AlertAddBannerBinding.inflate(LayoutInflater.from(context))

            b.bid = bid
            b.inter = interDialogBasicClick(onDel = null,
                    onCancel = { dialog?.cancel() },
                    onOk = {
                        bid.title = b.etTitle.getStr()
                        bid.comment = b.etComment.getStr()
                        onResult(bid)
                        dialog?.cancel()
                    })

//            LinearAddPhotoOne(context, bid.photoThumb, null,
//                    onGallery = { onClickOpenGallery() })
//                    .apply { b.frameAddPhotoOne.addView(this) }


            builder.setView(b.root)
            dialog = builder.create()
            commonShowMyAlert(dialog, true, true)

        }


        fun showSelectCalendar(context: Context, hasLimit: Boolean, onSelected: (date8: Int) -> Unit) {
            var dialog: AlertDialog? = null
            val builder = getBuilderBig(context)
            val b = AlertBasicFrameBinding.inflate(LayoutInflater.from(context))

            CustomCalendarNormal(context, hasLimit) {
                onSelected(it)
                dialog?.cancel()
            }.apply { b.frame.addView(this) }

            builder.setView(b.root)
            dialog = builder.create()
            commonShowMyAlert(dialog, true, true)
        }

        fun showSelectAdType(context: Context, info: String?, hasDesc: Boolean, dataType: DataType, onSelected: (AdType, Board?) -> Unit) {
            var dialog: AlertDialog? = null
            val builder = getBuilderBig(context)
            val b = AlertBasicFrameBinding.inflate(LayoutInflater.from(context))

            SubSelectAdType(context, info, hasDesc, dataType,
                    onClickRb = { adType ->
                        b.frame2.removeAllViews()
                        if (adType == AdType.TOP_BOARD) {
                            CustomSearchAll4Bid(context, null, false, DataType.BOARD,
                                    onSelected = {
                                        onSelected(adType, it as? Board)
                                        dialog?.cancel()
                                    })
                                    .apply { b.frame2.addView(this) }
                        } else {
                            onSelected(adType, null)
                            dialog?.cancel()
                        }
                    })
                    .apply { b.frame.addView(this) }


            builder.setView(b.root)
            dialog = builder.create()
            commonShowMyAlert(dialog, true, true)
        }

        /**
         * string 선택하는 다이얼로그
         */
        fun showSelectString(context: Context, info: String?, list: ArrayList<String>, onSelected: (Int) -> Unit) {
            var dialog: AlertDialog? = null
            val builder = getBuilderSmall(context)
            val b = AlertBasicFrameBinding.inflate(LayoutInflater.from(context))

            CustomRGstring(context, true, info, list, null,
                    onClickRb = { position ->
                        onSelected(position)
                        dialog?.cancel()
                    }).apply { b.frame.addView(this) }

            builder.setView(b.root)
            dialog = builder.create()
            commonShowMyAlert(dialog, true, true)
        }

        fun showMyBannerList(context: Context, uid: String?, manage: Boolean, bannerTypeNo: Int, onSelected: (Bid) -> Unit) {
            var dialog: AlertDialog? = null
            val builder = getBuilderBig(context)
            val b = AlertMyBannerListBinding.inflate(LayoutInflater.from(context))
            b.title = bannerTypeNo.getBannerType().title

//            b.frame.addView(CustomMyBannerPager(context,fragmentManager,manage,bannerTypeNo, onSelected = {onSelected(it)
//                dialog?.dismiss()
//            }))

            b.frame.addView(CustomMyBannerList(context, uid, manage, bannerTypeNo,
                    onSelected = {
                        if (it.ltNormalStatus()) {
                            toastNormal(it.statusNo.getStatus().msg)
                            return@CustomMyBannerList
                        }

//                        if (it.needGranted(true)) return@CustomMyBannerList
                        onSelected(it)
                        dialog?.dismiss()
                    }))

            builder.setView(b.root)
            dialog = builder.create()
            commonShowMyAlert(dialog, true, true)
        }

        fun showCalendarRange(context: Context, onResult: (iSet: ArrayList<Int>?) -> Unit) {
            var dialog: AlertDialog? = null
            val builder = getBuilderBig(context)
            val b = AlertCalendarRangeBinding.inflate(LayoutInflater.from(context))
            b.title = R.string.btn_open_range_dialog.getStr()
            var list: ArrayList<Int>? = null

            /**     * 달력 붙이기  : 멀티   */
            val vCal = CustomCalendarViewRange(context, false,
                    onSelected = { selList, selDate ->
                        list = selList
                    })

            b.frame.addView(vCal)

            b.btnOk.onClick {
                lllogI("SubTermsCredit list ${list?.size}")

                onResult(list)
                dialog?.dismiss()
            }

            b.btnCancel.onClick { dialog?.dismiss() }

            b.btnDefaultDate.onClick {
                onResult(arrayListOf<Int>().apply { add(HIT_DATE_DEFAULT_BID) })
                dialog?.dismiss()
            }

            builder.setView(b.root)
            dialog = builder.create()
            commonShowMyAlert(dialog, true, true)
        }

        /******************************************************************************
         ***************************** sttSearch ************************************
         *****************************************************************************/


    }
}

