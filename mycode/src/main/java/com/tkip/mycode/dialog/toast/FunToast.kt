package com.tkip.mycode.dialog.toast

import com.tkip.mycode.funs.common.lllogE
import com.tkip.mycode.funs.common.lllogI
import com.tkip.mycode.funs.common.lllogW
import com.tkip.mycode.init.base.getStr

fun toastNormal(stringId: Int) {
    TToast.showNormal(stringId)
    lllogI(stringId)
}

fun toastNormal(message: String?) {
    message?.let { TToast.showNormal(it) }
    lllogI(message)
}

fun toastAlert(stringId: Int) {
    TToast.showAlert(stringId)
    lllogE(stringId)
}

fun toastAlert(message: String) {
    TToast.showAlert(message)
    lllogE(message)
}

fun toastWarning(stringId: Int) {
    TToast.showWarning(stringId)
    lllogW(stringId)
}

fun toastWarning(message: String) {
    TToast.showWarning(message)
    lllogW(message)
}

/*******************************************************************
 *******************************************************************/


val TOAST_NORMAL = 100
val TOAST_ALERT = 200
val TOAST_WARN = 300

/**
 * this : check한 결과
 *      true && toastType -> 토스트 날림
 * toastType : 널이면 토스트 안 날림.
 * return this
 */
fun Boolean.checkAndToast(toastType: Int?, strId: Int?): Boolean {
    return checkAndToast(toastType,strId?.getStr())
}
fun Boolean.checkAndToast(toastType: Int?, str: String?): Boolean {
    if (this)
        when (toastType) {
            TOAST_NORMAL -> str?.let { toastNormal(it) }
            TOAST_ALERT -> str?.let { toastAlert(it) }
            TOAST_WARN -> str?.let { toastWarning(it) }
        }
    return this
}

//fun Boolean.checkAndToast(isNormal: Boolean, str: String): Boolean {
//    if (this) if (isNormal) toastNormal(str) else toastAlert(str)
//    return this
//}


/*******************************************************************
 ******************** 반복적인 토스트 ********************************/

/** * status 변경 후 */
fun milkStatusAdmin(message: String) {
    toastNormal(message)
}

