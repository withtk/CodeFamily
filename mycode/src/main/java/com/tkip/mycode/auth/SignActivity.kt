package com.tkip.mycode.auth

import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.annotation.LayoutRes
import com.google.android.gms.auth.api.signin.GoogleSignIn
import com.google.android.gms.auth.api.signin.GoogleSignInAccount
import com.google.android.gms.common.api.ApiException
import com.google.firebase.auth.FirebaseAuth
import com.tkip.mycode.R
import com.tkip.mycode.databinding.ActivitySignBinding
import com.tkip.mycode.dialog.OnDDD
import com.tkip.mycode.dialog.alert.MyAlert
import com.tkip.mycode.dialog.progress.MyCircle
import com.tkip.mycode.dialog.progress.WAIT_TYPE
import com.tkip.mycode.dialog.toast.TToast
import com.tkip.mycode.dialog.toast.toastNormal
import com.tkip.mycode.funs.common.lllogD
import com.tkip.mycode.funs.common.lllogI
import com.tkip.mycode.funs.common.lllogW
import com.tkip.mycode.init.PUT_ACTION
import com.tkip.mycode.init.base.getStr
import com.tkip.mycode.init.my_bind.BindingActivity
import com.tkip.mycode.init.part.CodeApp
import com.tkip.mycode.model.my_enum.LoginFrom
import com.tkip.mycode.util.tools.anim.hideAllFadeOut
import com.tkip.mycode.util.tools.etc.BackPressCloseHandler
import com.tkip.mycode.util.tools.etc.OnMyClick


class SignActivity : BindingActivity<ActivitySignBinding>() {
    @LayoutRes
    override fun getLayoutResId() = R.layout.activity_sign

    //    private var nick: String? = null
    private var isNew: Boolean = false

    //    private var actionType: Int = TYPE_CREATE
    private val actionType by lazy { intent.getIntExtra(PUT_ACTION, TYPE_NORMAL) }

//    private val type by lazy { intent.getIntExtra(PUT_ACTION, TYPE_CREATE) }

    companion object {
        const val TYPE_NORMAL = 100               // 최초 가입
        const val TYPE_CHANGE_USER = 200          // 구글로 전환
        const val TYPE_DELETE = 300               // 탈퇴
        const val TYPE_DUPLICATE_LOGIN = 400      // 중복로그인으로 인한 open SignActivity
        const val REQUEST_CODE_GOOGLE_AUTH = 9001 // 구글 로그인 RequestCode
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        initView()

        /*** 초기화중입니다. 잠시만 기다려주세요. */
//          OnAuth.setInfo(b.tvInfo, R.string.info_data_setting.getStr())

        /*** 기본 보드 셋팅 : 준비되면 버튼 활성화 */
//        Handler().postDelayed({ OnSystem.initBoard(  onSuccess = { readyToStart() }, onFail = {}) }, 1000)
        readyToStart()

    }

    override fun onBackPressed() {
        BackPressCloseHandler.onBackPressed(this)
    }

    private fun readyToStart() {
        when (actionType) {
            TYPE_DELETE -> {
//                initButtonShowInfo()
            }
            TYPE_DUPLICATE_LOGIN -> {
                MyAlert.showConfirm(this, R.string.ing_login_duplicated_re_signin.getStr())
                initButtonShowInfo()
            }
            else ->
                initButtonShowInfo() // for login
        }
    }


    private fun initView() {
        b.activity = this
        b.actionType = actionType
        lllogD("SignActivity actionType $actionType")


        /** 동의 사항: 세팅 */
        setCbAgree()

        /** 버튼 : 구글 로그인 */
        b.googleButton.setOnClickListener {
            OnMyClick.setDisableAWhileBTN(it)

            lllogI("SignActivity clientId ${CodeApp.clientId}")

            /*** 유저 생성일때만 동의 요함.*/
            if (actionType == TYPE_NORMAL && !b.cbAgree.isChecked) {
                  OnAuth.setInfo(b.tvInfo, R.string.alert_agree.getStr())
                return@setOnClickListener
            }

            /** 구글 로그인 실행하기 */
            val signInIntent = OnAuth.getGoogleSignInClient(this).signInIntent
            startActivityForResult(signInIntent, REQUEST_CODE_GOOGLE_AUTH)
        }

    }


    /**     * sign super     */
    fun onClickSignSuper(v: View) {
        if (OnMyClick.is5Click()) OnDDD.pw(this) { OnAuthUi.gotoAuthUiActivityAndFinish(this, true) }
    }

    /**     * 익명 로그인 버튼     */
    fun onClickAnonymousSignIn(v: View) {
        OnMyClick.setDisableAWhileBTN(v)

        if (!b.cbAgree.isChecked) {
              OnAuth.setInfo(b.tvInfo, R.string.alert_agree.getStr())
            return
        }

        MyAlert.showCreateNick(this,
                pass = { nick ->
                    WAIT_TYPE.FULL_AV.show(this)
                    OnSign.signInWithAnonymous(
                            success = {

                                  OnAuth.setInfo(b.tvInfo, R.string.ing_login.getStr())
                                isNew = it.additionalUserInfo?.isNewUser ?: false

                                OnAuth.currentFireUser("onClickAnonymousSignIn")?.let { user ->
                                    // 최초 Peer 셋팅하기
                                    OnAuth.firstAddPeer(user, nick, LoginFrom.ANONYMOUS,
                                            success = { OnAuth.gotoMainActivity(this ) },
                                            fail = { failInit() })
                                } ?: failInit()

                            }, fail = { initButtonShowInfo() })
                })


//        EditTextDialog.newInstance(title = getString(R.string.title_setup_nick_name), description = getString(R.string.desc_setup_nick_name), isCancel = true, isCancelTouch = false,
//                ok = { nick ->
//                    this.nick = nick
//                    signInWithAnonymous()
//                }).show(supportFragmentManager, "dialog")
    }


    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        lllogI("SignActivity onActivityResult startExecute   ")

        // Result returned from launching the Intent from GoogleSignInApi.getSignInIntent(...);
        if (requestCode == REQUEST_CODE_GOOGLE_AUTH) {
            lllogI("SignActivity requestCode Ok")

            val task = GoogleSignIn.getSignedInAccountFromIntent(data)
            try {
                // Google Sign In was successful, authenticate with Firebase
                val account = task.getResult(ApiException::class.java)  // null이면 exception발생하여 취소됨. SHA1키 등록할 것.
                lllogI("SignActivity account Ok")
                lllogI("SignActivity onActivityResult startExecute    fireUser  ${FirebaseAuth.getInstance().currentUser} ")

                /*** 로그인 성공 */
                startExecute(account)

            } catch (e: ApiException) {
                lllogW("SignActivity Google sign in failed", e)
                MyCircle.cancel()
                b.lineMsg.hideAllFadeOut()
                initButtonShowInfo()
            }
            return
        }
        lllogW("SignActivity request code ERROR 00000 ")
        MyCircle.cancel()

    }

    /*** 구글 로그인 버튼 클릭 후 */
    private fun startExecute(account: GoogleSignInAccount?) {
        lllogI("SignActivity startExecute ")

        WAIT_TYPE.FULL_AV.show(this)

        b.googleButton.isEnabled = false
        b.btnAnonymous.isEnabled = false

        /**  * 익명 로그인 -> 구글 전환   */
        lllogI("SignActivity   startExecute    fireUser  ${FirebaseAuth.getInstance().currentUser} ")
        when (OnAuth.curLoginFrom()) {
            LoginFrom.ANONYMOUS -> {
                lllogI("SignActivity   startExecute    LoginFrom.ANONYMOUS")
                changeFromAnonymous(account!!,
                        success = {
                            isNew = false
                            readyMain()
                        },
                        onSignOut = {
                            /*** 현재 로그인 상태 체크 */
                            OnAuth.currentFireUser("startExecute")?.let {
                                MyAlert.showConfirm(this, R.string.info_sign_out.getStr(), {

                                    /*** 로그아웃 */
                                    WAIT_TYPE.FULL_AV.show(this)
                                    OnAuth.signOut(this, false,
                                            success = {
                                                //                                            actionType = TYPE_CREATE   // 로그아웃 후 다시 '최초가입'으로 변경.  버튼 리셋을 위해.
//                                            initButtonShowInfo()
//                                            ViewUtil.hideViewChoiceBox(b.lineMsg)
                                                toastNormal(R.string.complete_sign_out)
                                                OnSign.gotoSignActivityAndFinish(this, TYPE_NORMAL, false)   // 로그아웃 후 다시 SignActivity 실행.
                                            },
                                            fail = { initButtonShowInfo() })

                                }, {})
                            } ?: initButtonShowInfo()
                        })
            }
            else -> {
                lllogI("SignActivity   startExecute   else ")

                /**
                 * 1. 구글 신규로그인
                 * 2. 구글 재로그인
                 * 3. 구글 계정탈퇴     */
                OnSign.signInWithGoogle(account!!,
                        success = { authResult ->
                            if (actionType == TYPE_DELETE) {  // 탈퇴
                                OnAuth.executeDeleteProcess(this, {})
                            } else { // 로그인

                                OnAuth.setInfo(b.tvInfo, R.string.ing_login.getStr())
                                isNew = authResult.additionalUserInfo?.isNewUser ?: false
                                readyMain()
                            }
                        }, fail = {})
            }
        }

    }


    /**
     * 1. New 구글
     * 2. 재로그인 구글
     * 3. 구글로 전환
     */
    private fun readyMain() {
        OnAuth.currentFireUser("readyMain")?.let { fireUser ->
            /*** 1. New 구글  */
            if (isNew) OnAuth.firstAddPeer(fireUser, null, LoginFrom.GOOGLE,
                    success = { OnAuth.gotoMainActivity(this) },
                    fail = { failInit() })
            /*** 2. 재로그인 구글  3. 구글로 전환*/
            else OnAuth.checkUpdatePeer(fireUser, LoginFrom.GOOGLE, success = { OnAuth.gotoMainActivity(this) }, fail = { failInit() })
        } ?: failInit()
    }

    /** * fail로 인한 초기화  */
    private fun failInit() {
        MyCircle.cancel()
        b.lineMsg.hideAllFadeOut()
        OnAuth.setInfo(b.tvInfo, R.string.error_logins.getStr())
        TToast.showWarning(R.string.error_logins)
        initButtonShowInfo()
    }


}