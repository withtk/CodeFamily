package com.tkip.mycode.auth

import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.annotation.LayoutRes
import com.google.android.gms.auth.api.signin.GoogleSignIn
import com.google.android.gms.auth.api.signin.GoogleSignInAccount
import com.google.android.gms.common.api.ApiException
import com.google.firebase.auth.FirebaseAuth
import com.tkip.mycode.R
import com.tkip.mycode.auth.SignActivity.Companion.REQUEST_CODE_GOOGLE_AUTH
import com.tkip.mycode.databinding.ActivityDeletePeerBinding
import com.tkip.mycode.databinding.ActivitySignBinding
import com.tkip.mycode.dialog.OnDDD
import com.tkip.mycode.dialog.alert.MyAlert
import com.tkip.mycode.dialog.progress.MyCircle
import com.tkip.mycode.dialog.progress.WAIT_TYPE
import com.tkip.mycode.dialog.toast.TToast
import com.tkip.mycode.dialog.toast.toastNormal
import com.tkip.mycode.funs.common.lllogD
import com.tkip.mycode.funs.common.lllogI
import com.tkip.mycode.funs.common.lllogW
import com.tkip.mycode.init.PUT_ACTION
import com.tkip.mycode.init.base.getStr
import com.tkip.mycode.init.my_bind.BindingActivity
import com.tkip.mycode.init.part.CodeApp
import com.tkip.mycode.model.my_enum.LoginFrom
import com.tkip.mycode.util.tools.anim.gone
import com.tkip.mycode.util.tools.anim.hideAllFadeOut
import com.tkip.mycode.util.tools.etc.BackPressCloseHandler
import com.tkip.mycode.util.tools.etc.OnMyClick


class DeletePeerActivity : BindingActivity<ActivityDeletePeerBinding>() {
    @LayoutRes
    override fun getLayoutResId() = R.layout.activity_delete_peer

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        initView()

    }

    override fun onBackPressed() {
        BackPressCloseHandler.onBackPressed(this)
    }

    private fun initView() {
        b.activity = this


        /** 버튼 : 구글 로그인 */
        b.googleButton.setOnClickListener {
            OnMyClick.setDisableAWhileBTN(it)

            /** 구글 로그인 실행하기 */
            val signInIntent = OnAuth.getGoogleSignInClient(this).signInIntent
            startActivityForResult(signInIntent, REQUEST_CODE_GOOGLE_AUTH)
        }

    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        lllogI("DeletePeerActivity onActivityResult startExecute resultCode:$resultCode  requestCode:$requestCode  ")
        if (requestCode == REQUEST_CODE_GOOGLE_AUTH) {
            val task = GoogleSignIn.getSignedInAccountFromIntent(data)
            try {
                val account = task.getResult(ApiException::class.java)  // null이면 exception발생하여 취소됨. SHA1키 등록할 것.
                lllogI("DeletePeerActivity onActivityResult startExecute  fireUser  ${FirebaseAuth.getInstance().currentUser} ")

                /*** 로그인 성공 */
                startExecute(account)

            } catch (e: ApiException) {
                lllogW("DeletePeerActivity Google sign in failed", e)
                MyCircle.cancel()
            }
            return
        }
        lllogW("DeletePeerActivity request code ERROR 00000 ")
        MyCircle.cancel()

    }

    /*** 구글 로그인 버튼 클릭 후 */
    private fun startExecute(account: GoogleSignInAccount?) {
        lllogI("DeletePeerActivity startExecute ")

        WAIT_TYPE.FULL_AV.show(this@DeletePeerActivity)

        b.googleButton.isEnabled = false

        /*** 3. 구글 계정탈퇴 */
        OnSign.signInWithGoogle(account!!,
                success = {
                    OnAuth.executeDeleteProcess(this@DeletePeerActivity,
                            onSuccess = {
                                OnAuth.setInfo(b.tvInfo,R.string.info_completed_deletion.getStr())
                                b.googleButton.gone()
                            })
                }, fail = {})

    }


    /** * fail로 인한 초기화  */
//    private fun failInit() {
//        MyCircle.cancel()
//        OnAuth.setInfo(b.tvInfo, R.string.error_logins.getStr())
//        TToast.showWarning(R.string.error_logins)
//    }


}