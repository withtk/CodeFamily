package com.tkip.mycode.auth

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.annotation.LayoutRes
import com.firebase.ui.auth.IdpResponse
import com.tkip.mycode.R
import com.tkip.mycode.databinding.ActivityAuthUiBinding
import com.tkip.mycode.dialog.OnDDD
import com.tkip.mycode.dialog.progress.MyCircle
import com.tkip.mycode.dialog.progress.WAIT_TYPE
import com.tkip.mycode.dialog.toast.toastWarning
import com.tkip.mycode.funs.common.lllogI
import com.tkip.mycode.funs.common.lllogM
import com.tkip.mycode.funs.common.lllogW
import com.tkip.mycode.init.REQ_AUTH_UI
import com.tkip.mycode.init.my_bind.BindingActivity
import com.tkip.mycode.model.my_enum.LoginFrom
import com.tkip.mycode.util.tools.anim.showOverUp
import com.tkip.mycode.util.tools.etc.OnMyClick

class AuthUiActivity : BindingActivity<ActivityAuthUiBinding>() {
    @LayoutRes
    override fun getLayoutResId() = R.layout.activity_auth_ui

//    companion object {
//        const val SIGN_NEW = 100                  // 최초 가입
//        const val SIGN_CHANGE = 200               // 구글로 전환
//        const val SIGN_DELETE = 300               // 탈퇴
//        const val SIGN_DUPLICATE_LOGIN = 400      // 중복로그인으로 인한 open SignActivity
//    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        initView()
    }

    private fun initView() {
        b.activity = this
        resetCurrentLogin()
    }


    private fun resetCurrentLogin() {
        val fireUser = OnAuth.currentFireUser("authUiActivity")
        b.tvCurrent.text = fireUser?.email ?: "none"
        b.btnSignout.isEnabled = fireUser != null
        b.btnDelete.isEnabled = fireUser != null

    }


    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (requestCode == REQ_AUTH_UI) {
            val response = IdpResponse.fromResultIntent(data)
            lllogI("AuthUiActivity AuthUI# response $response")

            if (resultCode == Activity.RESULT_OK) {
                OnAuthUi.afterSuccess()
                /** 1. email 가입
                 *  2. email 재로그인 */
                OnAuth.currentFireUser(null)?.let { fireUser ->

                    /*** 로그인 표시 */
                    b.tvCurrent.text = fireUser.displayName
                    b.tvCurrent.showOverUp(true)
                    WAIT_TYPE.FULL_CIRCLE.show(this@AuthUiActivity)
                    lllogI("AuthUiActivity AuthUI# currentFireUser $fireUser")

                    /*** 유저 생성 or 로그인 for DB */
                    OnAuth.checkUpdatePeer(fireUser, LoginFrom.EMAIL, success = {
                        OnAuth.gotoMainActivity(this@AuthUiActivity)
                    }, fail = {
                        toastWarning("error authUi")
                        MyCircle.cancel()
                    })
                }
            } else lllogW("AuthUiActivity request code ERROR ${response?.error?.errorCode} ")
        }

//        lllogW("AuthUiActivity request code ERROR 00000 ")
//        MyCircle.cancel()
    }


    fun onClickOpenAuth(v: View) {
        OnMyClick.setDisableAWhileBTN(v)
        OnAuthUi.signIn(this@AuthUiActivity, REQ_AUTH_UI)
    }

    fun onClickSignout(v: View) {
        OnMyClick.setDisableAWhileBTN(v)
        OnDDD.confirm(this@AuthUiActivity, R.string.info_sign_out, ok = { OnAuthUi.signOut(this@AuthUiActivity, { resetCurrentLogin() }, {}) }, cancel = {})
    }

    fun onClickDeleteUID(v: View) {
        OnMyClick.setDisableAWhileBTN(v)
        OnDDD.confirm(this@AuthUiActivity, R.string.info_delete_peer_last, ok = { OnAuthUi.delete(this@AuthUiActivity, { resetCurrentLogin() }, {}) }, cancel = {})
    }


}