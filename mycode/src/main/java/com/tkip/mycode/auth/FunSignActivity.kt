package com.tkip.mycode.auth

import android.text.Spannable
import android.text.Spanned
import android.text.method.LinkMovementMethod
import android.text.style.URLSpan
import com.google.android.gms.auth.api.signin.GoogleSignInAccount
import com.google.firebase.auth.AuthResult
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.GoogleAuthProvider
import com.tkip.mycode.R
import com.tkip.mycode.dialog.progress.MyCircle
import com.tkip.mycode.funs.common.*
import com.tkip.mycode.init.base.getStr
import com.tkip.mycode.model.my_enum.LoginFrom
import com.tkip.mycode.util.my_view.ViewUtil
import com.tkip.mycode.util.tools.anim.gone
import com.tkip.mycode.util.tools.anim.showFadeIn
import com.tkip.mycode.util.tools.anim.showSlideUp
import com.tkip.mycode.util.tools.fire.Firee


//var iii = 0
//fun currentFireUser(method: String): FirebaseUser? {
//    iii++
//    lllogI("SignActivity  $iii  $method startExecute    currentFireUser  ${FirebaseAuth.getInstance().currentUser} ")
//    return FirebaseAuth.getInstance().currentUser
//}


/** 버튼 세팅 */
fun SignActivity.initButtonShowInfo() {

    b.googleButton.isEnabled = true
    b.cbAgree.isEnabled = true

    when (OnAuth.curLoginFrom()) {
        LoginFrom.GOOGLE -> {
            OnAuth.setInfo(b.tvInfo,String.format(R.string.info_current_user_is_google.getStr(), OnAuth.currentFireUser("initButtonShowInfo")?.email))
            b.googleButton.gone()
            b.btnAnonymous.gone()
        }
        LoginFrom.ANONYMOUS -> {
            OnAuth.setInfo(b.tvInfo,R.string.info_current_user_is_anonymous.getStr())
            b.googleButton.showSlideUp(false)
            b.btnAnonymous.gone()
        }
        else -> {
//            OnAuth.setInfo(b.tvInfo,R.string.info_please_log_in.getStr())
            OnAuth.setInfo(b.tvInfo,R.string.description_login.getStr())
            b.googleButton.showSlideUp(false)
            b.btnAnonymous.showSlideUp(false)
            b.btnAnonymous.isEnabled = true
        }
    }
}

/** * 동의사항 셋팅 */
fun SignActivity.setCbAgree() {
    val privacy = R.string.remote_privacy_policy.remoteStr()
    val termsLoc = R.string.remote_location_services.remoteStr()
    val span = this.b.tvAgree.text as Spannable
    span.setSpan(URLSpan(privacy), 13, 21, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE)
    span.setSpan(URLSpan(termsLoc), 23, 34, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE)
    b.tvAgree.movementMethod = LinkMovementMethod.getInstance()

    /**
     * todo : 나중에 이걸로 바꾸자.
     */
//    span.setSpan(object : ClickableSpan() {
//        override fun onClick(widget: View) {
//            OnWeb.gotoWebViewActivity(widget.context,privacy)
//        }
//    }, 13, 21, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE)
//    b.tvAgree.movementMethod = LinkMovementMethod.getInstance()


    b.cbAgree.onCheckedChange { _, isOn ->
        if (isOn) {
            OnAuth.setInfo(b.tvInfo,R.string.info_please_log_in.getStr())
            OnVisible.enable(true, )
            enableAllView(this.b.btnAnonymous, this.b.googleButton)
        } else OnAuth.setInfo(b.tvInfo,R.string.alert_agree.getStr())
    }
}


//fun ActivitySignBinding.initDeleteAction() {
//    this.appbar.background = R.drawable.shape_back_linear_gray_dark.getDrawable()
//    this.tvTitle.text = R.string.title_delete_peer.getStr()
//    this.tvDescription.text = R.string.description_delete_peer.getStr()
//}


/***************************************************************
 ************** Add to DB ************************************
 ****************************************************************/


///**   최초로 가입시 한번 : 익명, 구글, 이메일 */
//fun SignActivity.initPeerFromGoogle(user: FirebaseUser, nick: String?, loginFrom: LoginFrom, success: () -> Unit, fail: () -> Unit) {
//    lllogI("SignActivity initPeerFromGoogle  start nick $nick loginFrom $loginFrom  ")
//
//    when (loginFrom) {
//        LoginFrom.ANONYMOUS -> {
//            val peer = Peer(user, nick, loginFrom)
//            OnPeer.setUpPeer(peer)    // Sred Setup
//            Firee.addPeer(peer, success = { success() }, fail = { fail() })
//        }
//        else -> {
//            /*** 재가입여부 체크 */
//            OnSign.checkPeerDeleteDate(user.email!!, success = { endDate ->
//                lllogI("SignActivity checkPeerDeleteDate allow true")
//                val peer = Peer(user, nick, loginFrom)
//                peer.delDate = endDate
//                OnPeer.setUpPeer(peer)    // Sred Setup
//                Firee.addPeer(peer, success = { success() }, fail = { fail() })
//            }, fail = { fail() })
//
//        }
//    }
//}

//
///**  재 로그인 or 구글로 전환   */
//fun SignActivity.updatePeerFromGoogle(user: FirebaseUser, success: () -> Unit, fail: () -> Unit) {
//    lllogI("SignActivity updatePeerFromGoogle  start  user.uid:${user.uid} ")
//
//    // 기존 peer 받아오기.
//    OnAuth.checkUpdatePeer(user, success, fail)
//
//}


/*** 익명 -> 구글로 전환 */
fun SignActivity.changeFromAnonymous(acct: GoogleSignInAccount, success: (AuthResult) -> Unit, onSignOut: () -> Unit) {
    lllogI("SignActivity changeFromAnonymous:")

    val credential = GoogleAuthProvider.getCredential(acct.idToken, null)

    FirebaseAuth.getInstance().currentUser?.let {
        it.linkWithCredential(credential)
                .addOnSuccessListener { authResult ->
                    lllogI("SignActivity changeFromAnonymous success")
                    success(authResult)
                }
//                        .addOnCompleteListener(this) { task ->
//                            isNew = false
//                            if (task.isSuccessful) {
//                                lllogI("SignActivity linkWithCredential:success")
////                            val user = it.result?.user
//                                gotoMain(LoginFrom.GOOGLE_CHANGED)
//                            }
//                        }

                .addOnFailureListener {
                    lllogI("SignActivity changeFromAnonymous fail")
                    OnException.loginChange("changeFromAnonymous:addOnFailureListener", it)
                    MyCircle.cancel()

                    ViewUtil.hideViewInfoStatus(b.lineMsg)

                    /* if (it.title!!.contains("same")) {}*/

                    /** 1. 이미 가입된 구글 아이디, 다른아이디로?  */
                    ViewUtil.addViewChoiceBox(this, b.lineMsg,
                            null, null, R.string.info_already_signed_up_email.getStr(), true, null,
                            ok = {
                                /** ok : 안내멘트 : 현재 게스트계정로그인 중, 구글 아이디로 로그인 해줭. */
                                OnAuth.revokeAccess(this)
                                b.googleButton.isEnabled = true
                                b.btnAnonymous.isEnabled = true
                                OnAuth.setInfo(b.tvInfo,R.string.info_current_user_is_anonymous.getStr())
                            },
                            cancel = {
                                /** 2. 게스트계정 완전삭제 및 새 구글로그인? */
                                ViewUtil.addViewChoiceBox(this, b.lineMsg,
                                        null, null, R.string.info_logout_and_wanna_try_to_google_login.getStr(), true, null,
                                        ok = {
                                            /** ok : 로그아웃할 것. */
                                            onSignOut()
                                        },
                                        cancel = {
                                            /**  cancel : 안내멘트 : 현재 게스트계정로그인 중, 구글 아이디로 로그인 해줭. */
                                            OnAuth.revokeAccess(this)
                                            b.googleButton.isEnabled = true
                                            b.btnAnonymous.isEnabled = true
                                            OnAuth.setInfo(b.tvInfo,R.string.info_current_user_is_anonymous.getStr())
                                        })
                            })

                }
    }

}
