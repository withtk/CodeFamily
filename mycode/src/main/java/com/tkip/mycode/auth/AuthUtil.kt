package com.tkip.mycode.auth
//
//import android.app.Activity
//import com.google.android.gms.auth.api.signin.GoogleSignIn
//import com.google.android.gms.auth.api.signin.GoogleSignInClient
//import com.google.android.gms.auth.api.signin.GoogleSignInOptions
//import com.google.firebase.auth.FirebaseAuth
//import com.tkip.mycode.R
//import com.tkip.mycode.dialog.progress.MyCircle
//import com.tkip.mycode.dialog.progress.MyProgress
//import com.tkip.mycode.dialog.toast.TToast
//import com.tkip.mycode.funs.common.OnException
//import com.tkip.mycode.funs.common.lllogI
//import com.tkip.mycode.init.part.OnApp
//import com.tkip.mycode.model.my_enum.LoginFrom
//import com.tkip.mycode.model.peer.OnPeer
//import com.tkip.mycode.model.peer.Peer
//import com.tkip.mycode.util.tools.fire.Firee
//
//
//class AuthUtil {
//
//    companion object {
////        const val REQUEST_CODE_GOOGLE_AUTH = 9001
//
//        var auth: FirebaseAuth? = null
//
//
//        fun curLoginFrom(): LoginFrom? {
//            currentFireUser()?.let {
//                return if (it.isAnonymous) LoginFrom.ANONYMOUS else LoginFrom.GOOGLE
//            }
//            return null
//        }
//
//
//        fun getGoogleSignInClient(activity: Activity): GoogleSignInClient {
//
//            /**  Configure Google Sign In */
//            val gso = GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
////                    .requestIdToken(activity.getString(R.string.default_web_client_id))
//                    .requestIdToken(OnApp.clientId)
//                    .requestEmail()
//                    .build()
//            return GoogleSignIn.getClient(activity, gso)
//
//        }
//
//
//        /**
//         * google login id 선택창 초기화.
//         */
//        fun revokeAccess(activity: Activity) {
//            getGoogleSignInClient(activity).revokeAccess()
//        }
//
//        fun signOut(activity: Activity, success: () -> Unit, fail: () -> Unit) {
//
//            /**  DB의 peer 수정 :  fcmToken 삭제, invalid true */
//            Firee.peerDeleteFcmToken(success = {}, fail = {})
//
//            /**  Firebase sign out */
//            FirebaseAuth.getInstance().signOut()
//
//            /**  Google sign out */
//            getGoogleSignInClient(activity).signOut()
//                    .addOnSuccessListener {
//                        MyCircle.cancel()
//
//                        OnPeer.peer = Peer()      // null peer 로 대입.
//                        OnPeer.setUpPeer(null)    // sign Out 했을때
//
//                        success()
////                TToast.showAlert(R.string.complete_sign_out)
//
//                    }
//                    .addOnFailureListener {
//                        MyCircle.cancel()
//                        fail()
//                        TToast.showAlert(R.string.error_sign_out)
//                    }
//
//        }
//
//        fun deletePeer(success: () -> Unit
//                       , fail: () -> Unit) {
//            if (OnPeer.notAllowedDB()) return
//
//            FirebaseAuth.getInstance().currentUser?.let {
//                it.delete()
//                        .addOnSuccessListener {
//                            lllogI("deletePeer success")
//                            MyProgress.cancel()
//                            TToast.showNormal(OnApp.getAppContext().getString(R.string.info_completed_deletion))
//                            success()
//                        }
//                        .addOnFailureListener {
//                            OnException.dbfailed("deletePeer", it)
//                            MyProgress.cancel()
//                            fail()
//                            TToast.showNormal(OnApp.getAppContext().getString(R.string.db_error_delete_peer))
//                        }
//            }
//
//        }
//
//
//    }
//
//}