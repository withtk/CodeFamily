package com.tkip.mycode.auth

import android.app.Activity
import android.content.Context
import android.content.Intent
import com.firebase.ui.auth.AuthUI
import com.google.firebase.auth.FirebaseAuth
import com.tkip.mycode.dialog.toast.toastNormal
import com.tkip.mycode.dialog.toast.toastWarning
import com.tkip.mycode.funs.common.lllogI
import com.tkip.mycode.funs.common.lllogW

/**
 * Hi~ Created by fabulous on 20-07-08
 */
class OnAuthUi {

    companion object {

        /*******************************************************************************************
         * Auth Ui ************************************************************************
         *******************************************************************************************/

        fun gotoAuthUiActivityAndFinish(activity: Activity, isFinish: Boolean) {
            val intent = Intent(activity, AuthUiActivity::class.java)
//            intent.putExtra(PUT_ACTION, actionType)  // 중요.
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
            activity.startActivity(intent)
            if (isFinish) activity.finish()
        }


        private val providers by lazy {
            arrayListOf(
                    AuthUI.IdpConfig.EmailBuilder().build()
//                    AuthUI.IdpConfig.PhoneBuilder().build(),
//                    AuthUI.IdpConfig.GoogleBuilder().build() ,
//                    AuthUI.IdpConfig.FacebookBuilder().build(),
//                    AuthUI.IdpConfig.TwitterBuilder().build(),
//                    AuthUI.IdpConfig.AnonymousBuilder().build()
            )
        }

        fun signIn(activity: Activity, requestCode: Int) {

            // Create and launch sign-in intent
            activity.startActivityForResult(
                    AuthUI.getInstance()
                            .createSignInIntentBuilder()
                            .setAvailableProviders(providers)
                            .build(),
                    requestCode)

        }

        fun signOut(context: Context, onSuccess: () -> Unit, onFail: () -> Unit) {
            AuthUI.getInstance()
                    .signOut(context)
                    .addOnSuccessListener {
                        lllogI("AUTHUI onClickSignout signout successful")
                        toastNormal("sign Out")
                        onSuccess()
                    }
                    .addOnFailureListener {
                        lllogI("AUTHUI onClickSignout signout fail")
                        toastWarning("failed")
                        onFail()
                    }
        }

        fun delete(context: Context, onSuccess: () -> Unit, onFail: () -> Unit) {
            AuthUI.getInstance()
                    .delete(context)
                    .addOnSuccessListener {
                        lllogI("AUTHUI user Delete successful")
                        toastNormal("user Deleted")
                        onSuccess()
                    }
                    .addOnFailureListener {
                        lllogI("AUTHUI user Delete fail")
                        toastWarning("failed")
                        onFail()
                    }
        }


        fun afterSuccess( ) {
            FirebaseAuth.getInstance().currentUser?.let { user ->

                lllogI("AuthUiActivity AuthUI# isAnonymous ${user.isAnonymous}")
                lllogI("AuthUiActivity AuthUI# uid ${user.uid}")
                lllogI("AuthUiActivity AuthUI# displayName ${user.displayName}")
                lllogI("AuthUiActivity AuthUI# email ${user.email}")
                lllogI("AuthUiActivity AuthUI# isEmailVerified ${user.isEmailVerified}")
                lllogI("AuthUiActivity AuthUI# photoUrl ${user.photoUrl}")
                lllogI("AuthUiActivity AuthUI# providerId ${user.providerId}")

            } ?: lllogW("AuthUiActivity onActivityResult user null")
        }


    }
}