package com.tkip.mycode.auth

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.os.Handler
import android.widget.TextView
import com.google.android.gms.auth.api.signin.GoogleSignIn
import com.google.android.gms.auth.api.signin.GoogleSignInClient
import com.google.android.gms.auth.api.signin.GoogleSignInOptions
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.FirebaseUser
import com.tkip.mycode.R
import com.tkip.mycode.dialog.progress.MyCircle
import com.tkip.mycode.dialog.progress.MyProgress
import com.tkip.mycode.dialog.toast.TToast
import com.tkip.mycode.funs.common.OnException
import com.tkip.mycode.funs.common.lllogI
import com.tkip.mycode.funs.common.lllogM
import com.tkip.mycode.init.MainActivity
import com.tkip.mycode.init.base.Sred
import com.tkip.mycode.init.base.getStr
import com.tkip.mycode.init.part.CodeApp
import com.tkip.mycode.init.sredString
import com.tkip.mycode.model.my_enum.LoginFrom
import com.tkip.mycode.model.my_enum.PeerType
import com.tkip.mycode.model.peer.OnPeer
import com.tkip.mycode.model.peer.Peer
import com.tkip.mycode.util.tools.anim.gone
import com.tkip.mycode.util.tools.anim.showFadeIn
import com.tkip.mycode.util.tools.fire.Firee


class OnAuth {

    companion object {


        var iii = 0

        fun currentFireUser(method: String?): FirebaseUser? {
            iii++
            lllogI("SignActivity  $iii  $method startExecute  currentFireUser anony : ${FirebaseAuth.getInstance().currentUser?.isAnonymous} ")
            return FirebaseAuth.getInstance().currentUser
        }

        fun curLoginFrom(): LoginFrom? {
//            WAIT_TYPE.FULL_CIRCLE.show(context)
            lllogI("SignActivity startExecute  curLoginFrom fireUser  ${FirebaseAuth.getInstance().currentUser} ")
            currentFireUser("curLoginFrom")?.let {
                return if (it.isAnonymous) LoginFrom.ANONYMOUS else LoginFrom.GOOGLE
            }
            return null
        }


        fun getGoogleSignInClient(activity: Activity): GoogleSignInClient {
            /**  Configure Google Sign In */
            val gso = GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                    .requestIdToken(activity.getString(R.string.default_web_client_id))
//                    .requestIdToken(OnApp.clientId)
                    .requestEmail()
                    .build()
            return GoogleSignIn.getClient(activity, gso)

        }


        /**
         * google login id 선택창 초기화.
         */
        fun revokeAccess(activity: Activity) {
            getGoogleSignInClient(activity).revokeAccess()
        }

        /**
         * isDuplicate : 중복으로 인한 강제로그아웃
         */
        fun signOut(activity: Activity, isDuplicate: Boolean, success: () -> Unit, fail: () -> Unit) {

            /**  DB의 peer 수정 :  fcmToken 삭제, invalid true */
            Firee.peerDeleteFcmToken(isDuplicate, success = {}, fail = {})

            /**  Firebase sign out */
            FirebaseAuth.getInstance().signOut()

            /**  Google sign out */
            getGoogleSignInClient(activity).signOut()
                    .addOnSuccessListener {
                        MyCircle.cancel()

                        OnPeer.peer = Peer()      // null peer 로 대입.
                        OnPeer.setUpPeer(null)    // sign Out 했을때

                        success()
//                TToast.showAlert(R.string.complete_sign_out)

                    }
                    .addOnFailureListener {
                        MyCircle.cancel()
                        fail()
                        TToast.showAlert(R.string.error_sign_out)
                    }

        }

        fun deletePeer(success: () -> Unit, fail: () -> Unit) {
            if (OnPeer.notAllowedDB()) return

            FirebaseAuth.getInstance().currentUser?.let {
                it.delete()
                        .addOnSuccessListener {
                            lllogI("SignActivity deletePeer success")
                            MyProgress.cancel()
                            TToast.showNormal(CodeApp.getAppContext().getString(R.string.info_completed_deletion))
                            success()
                        }
                        .addOnFailureListener {
                            OnException.dbfailed("deletePeer", it)
                            MyProgress.cancel()
                            fail()
                            TToast.showNormal(CodeApp.getAppContext().getString(R.string.db_error_delete_peer))
                        }
            }

        }


        /**   최초로 가입시 한번 : 익명, 구글, 이메일 */
        fun firstAddPeer(user: FirebaseUser, nick: String?, loginFrom: LoginFrom, success: () -> Unit, fail: () -> Unit) {
            lllogI("SignActivity initPeerFromGoogle  start nick $nick loginFrom $loginFrom  ")

            when (loginFrom) {
                LoginFrom.ANONYMOUS -> {
                    val peer = Peer(user, nick, loginFrom)
                    OnPeer.setUpPeer(peer)    // Sred Setup
                    Firee.addPeer(peer, success = { success() }, fail = { fail() })
                }
                else -> {
                    /*** 재가입 여부 체크 */
                    OnSign.checkPeerDeleteDate(user.email!!, success = { endDate ->
                        lllogI("SignActivity checkPeerDeleteDate allow true")
                        val peer = Peer(user, nick, loginFrom)
                        peer.delDate = endDate
                        OnPeer.setUpPeer(peer)    // Sred Setup
                        Firee.addPeer(peer, success = { success() }, fail = { fail() })
                    }, fail = { fail() })

                }
            }
        }

        /**   재로그인 : 익명, 구글, 이메일 */
        fun checkUpdatePeer(user: FirebaseUser, loginFrom: LoginFrom, success: () -> Unit, fail: () -> Unit) {
            lllogM("AuthUiActivity checkUpdatePeer start ")
            // 기존 peer 받아오기.
            Firee.getPeer(user.uid,
                    success = { oldPeer ->
//                        lllogM("AuthUiActivity checkUpdatePeer  oldPeer $oldPeer ")

                        if (oldPeer == null) firstAddPeer(user, user.displayName, loginFrom, success, fail)       // 최초 로그인
                        else {
                            // 재로그인
                            val newPeer = Peer(oldPeer)
//                            lllogI("SignActivity updatePeerFromGoogle oldPeer:  $oldPeer")
//                            lllogI("SignActivity updatePeerFromGoogle user.displayName:  ${user.displayName}")
//                            lllogI("SignActivity updatePeerFromGoogle user.email:  ${user.email}")

                            /** 일반적인 재로그인 & 전환 */
                            newPeer.dpName = user.displayName
                            newPeer.fcmToken = Sred.DEVICE_FCM_TOKEN.sredString()

                            /** 구글로 전환일때만 (최초 한번) */
                            if (oldPeer.type == PeerType.BASIC_1.no) {   // 익명전환인지 체크하는 유일한 수단.
                                newPeer.type = PeerType.BASIC_2.no       // 익명전환일 때만 type 변경.
                                newPeer.loginFrom = LoginFrom.GOOGLE.no  // 익명전환일 때만 type 변경.
                                newPeer.email = user.email
                            }

//                            lllogI("SignActivity updatePeerFromGoogle newPeer:  $newPeer")

                            /** 업데이트 peer */
                            Firee.updatePeer(oldPeer, newPeer, success = {
                                /** 업데이트 Sred.Peer */
                                OnPeer.setUpPeer(newPeer)    // updatePeer보다 먼저해야. addAct할때 uid를 제대로 받아올 수 있다.
                                success()
                            }, fail = { fail() })


//                    /*** 재가입여부 체크 */
//                    OnSign.checkPeerDeleteDate(user.email!!, success = { endDate -> }, fail = { fail() })

                        }
                    },
                    /** isNew가 false인데 peer값이 없을수가 없다.*/
                    fail = {
                        firstAddPeer(user, user.displayName, loginFrom, success, fail)       // 최초 로그인
                    })
        }


        fun gotoMainActivity(activity: Activity) {
            Handler().post {
                val intent = Intent(activity, MainActivity::class.java)
                activity.startActivity(intent)
                MyCircle.cancel()
                activity.finish()
            }
        }


        /********************************************************************************************
         ****** 안내 멘트 ****************************************************************************
         ********************************************************************************************/
        fun setInfo(tv: TextView, str: String) {
            tv.text = str
            tv.showFadeIn(true)
        }


        /********************************************************************************************
         ****** 계정 삭제 ****************************************************************************
         ********************************************************************************************/

        /**  구글 재로그인 후 탈퇴 처리 */
        fun executeDeleteProcess(activity: Activity, onSuccess: () -> Unit) {
            /**
             * (중요) 로그아웃 하기 전에 peer.invalid를 true로 만들어야 한다.
             */
            Firee.leavePeer({}, {})   // db에서 peer 탈퇴 처리.
            deletePeer(
                    success = {

                        revokeAccess(activity)
                        /** 마지막 잘가 메세지 보여주기 */
                        onSuccess()
                        MyCircle.cancel()

                    }, fail = {})
        }


    }

}