package com.tkip.mycode.auth

import android.app.Activity
import android.content.Context
import android.content.Intent
import com.google.android.gms.auth.api.signin.GoogleSignInAccount
import com.google.firebase.auth.AuthResult
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.GoogleAuthProvider
import com.tkip.mycode.R
import com.tkip.mycode.dialog.alert.MyAlert
import com.tkip.mycode.dialog.progress.MyCircle
import com.tkip.mycode.dialog.progress.WAIT_TYPE
import com.tkip.mycode.dialog.toast.toastNormal
import com.tkip.mycode.funs.common.OnBasic
import com.tkip.mycode.funs.common.OnException
import com.tkip.mycode.funs.common.lllogI
import com.tkip.mycode.init.PUT_ACTION
import com.tkip.mycode.init.base.getInt
import com.tkip.mycode.init.base.getStr
import com.tkip.mycode.model.my_enum.LoginFrom
import com.tkip.mycode.model.peer.myloginFrom
import com.tkip.mycode.util.lib.retrofit2.ESget
import com.tkip.mycode.util.lib.retrofit2.ESquery


class OnSign {

    companion object {

        val postponeMonth = R.integer.postpone_month.getInt()    // 탈퇴 후 재가입 가능 개월

        fun gotoSignActivityAndFinish(activity: Activity, actionType: Int, isFinish: Boolean) {

            /*** 이메일 가입 분기 */
            val intent =
                    if (myloginFrom() == LoginFrom.EMAIL.no) Intent(activity, AuthUiActivity::class.java)
                    else Intent(activity, SignActivity::class.java)

            intent.putExtra(PUT_ACTION, actionType)  // 중요.
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
            activity.startActivity(intent)
//            if (isFinish) activity.finish()

            if (isFinish ) OnBasic.finishAllActivity( )
        }

        fun gotoDeletePeerActivityAndFinish(activity: Activity) {
            val intent = Intent(activity, DeletePeerActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
            activity.startActivity(intent)
            OnBasic.finishAllActivity( )
        }



        /*****************************************************************
         ********************** with Google ******************************
         ****************************************************************/
        fun signInWithGoogle(acct: GoogleSignInAccount, success: (AuthResult) -> Unit, fail: () -> Unit) {
            lllogI("SignActivity signInWithGoogle  start")

            val credential = GoogleAuthProvider.getCredential(acct.idToken, null)
            FirebaseAuth.getInstance().signInWithCredential(credential)
                    .addOnSuccessListener {
                        lllogI("SignActivity signInWithGoogle success")
                        success(it)
                    }
                    .addOnFailureListener {
                        lllogI("SignActivity signInWithGoogle fail")
                        OnException.dbfailed("signInWithGoogle:addOnFailureListener", it)
                        MyCircle.cancel()
                        fail()
                    }
        }


        fun signInWithAnonymous(success: (AuthResult) -> Unit, fail: () -> Unit) {
            lllogI("SignActivity signInWithAnonymous start")

            FirebaseAuth.getInstance().signInAnonymously()
                    .addOnSuccessListener {
                        lllogI("SignActivity signInWithGoogle success")
                        success(it)
                    }
                    .addOnFailureListener {
                        lllogI("SignActivity signInWithGoogle fail")
                        OnException.dbfailed("signInWithAnonymous:addOnFailureListener", it)
                        fail()
                        MyCircle.cancel()
                    }
        }

//        /*** 익명 -> 구글로 전환 */
//        fun changeFromAnonymous(acct: GoogleSignInAccount, activity: Activity, b: ActivitySignBinding, success: (AuthResult) -> Unit, onSignOut: () -> Unit) {
//            lllogI("SignActivity changeFromAnonymous:")
//
//            val credential = GoogleAuthProvider.getCredential(acct.idToken, null)
//
//            FirebaseAuth.getInstance().currentUser?.let {
//                it.linkWithCredential(credential)
//                        .addOnSuccessListener { authResult ->
//                            lllogI("SignActivity changeFromAnonymous success")
//                            success(authResult)
//                        }
////                        .addOnCompleteListener(this) { task ->
////                            isNew = false
////                            if (task.isSuccessful) {
////                                lllogI("SignActivity linkWithCredential:success")
//////                            val user = it.result?.user
////                                gotoMain(LoginFrom.GOOGLE_CHANGED)
////                            }
////                        }
//
//                        .addOnFailureListener {
//                            lllogI("SignActivity changeFromAnonymous fail")
//                            OnException.loginChange("changeFromAnonymous:addOnFailureListener", it)
//                            MyCircle.cancel()
//
//                            ViewUtil.hideViewInfoStatus(b.lineMsg)
//
//                            /* if (it.title!!.contains("same")) {}*/
//
//                            /** 1. 이미 가입된 구글 아이디, 다른아이디로?  */
//                            ViewUtil.addViewChoiceBox(activity, b.lineMsg,
//                                    null, null, R.string.info_already_signed_up_email.getStr(), true, null,
//                                    ok = {
//                                        /** ok : 안내멘트 : 현재 게스트계정로그인 중, 구글 아이디로 로그인 해줭. */
//                                        OnAuth.revokeAccess(activity)
//                                        b.googleButton.isEnabled = true
//                                        b.btnAnonymous.isEnabled = true
//                                        ViewUtil.addViewInfoStatus(activity, b.lineMsg, R.string.info_current_user_is_anonymous.getStr())
//                                    },
//                                    cancel = {
//                                        /** 2. 게스트계정 완전삭제 및 새 구글로그인? */
//                                        ViewUtil.addViewChoiceBox(activity, b.lineMsg,
//                                                null, null, R.string.info_logout_and_wanna_try_to_google_login.getStr(), true, null,
//                                                ok = {
//                                                    /** ok : 로그아웃할 것. */
//                                                    onSignOut()
//                                                },
//                                                cancel = {
//                                                    /**  cancel : 안내멘트 : 현재 게스트계정로그인 중, 구글 아이디로 로그인 해줭. */
//                                                    OnAuth.revokeAccess(activity)
//                                                    b.googleButton.isEnabled = true
//                                                    b.btnAnonymous.isEnabled = true
//                                                    ViewUtil.addViewInfoStatus(activity, b.lineMsg, R.string.info_current_user_is_anonymous.getStr())
//                                                })
//                                    })
//
//                        }
//            }
//
//        }


        /**  로그아웃 */
        fun askSignOut(activity: Activity) {
            OnAuth.currentFireUser("askSignOut")?.let {
                val msg =
                        if (it.isAnonymous) R.string.info_wanna_try_to_google_login.getStr()
                        else {
                            val peerEmail = FirebaseAuth.getInstance().currentUser?.email
                            val infoStr = R.string.info_current_user_is_google.getStr()
                            String.format(infoStr, peerEmail) + "\n\n" + R.string.info_sign_out.getStr()
                        }

                MyAlert.showConfirm(activity, msg,
                        ok = {
                            /** <구글로 전환>
                             *   1. 오픈 SignActivity   addType : SignActivity.TYPE_CHANGE_USER
                             *   2. Google 로그인      */
                            if (it.isAnonymous) gotoSignActivityAndFinish(activity, SignActivity.TYPE_CHANGE_USER, true)


                            /** <구글계정 로그아웃> */
                            else {
                                WAIT_TYPE.FULL_AV.show(activity)
                                OnAuth.signOut(activity, false,
                                        success = {
                                            toastNormal(R.string.complete_sign_out)
                                            gotoSignActivityAndFinish(activity, SignActivity.TYPE_NORMAL, true)   // 로그아웃 후 다시 SignActivity 실행.
                                        }, fail = {})
                            }
                        }, cancel = {})
            }
        }


        /**  탈퇴하기  */
        fun deletePeer(activity: Activity) {
            val peerEmail = String.format(R.string.info_current_user_is_google.getStr(), FirebaseAuth.getInstance().currentUser?.email)
            val infoPostpone = String.format(R.string.info_delete_peer.getStr(), postponeMonth)
            val strMsg = "$peerEmail \n\n $infoPostpone"
            MyAlert.showConfirm(activity, strMsg,
                    ok = {
                        val msg = String.format(R.string.info_delete_peer_last.getStr(), postponeMonth)
                        MyAlert.showConfirm(activity, msg, ok = {
//                            gotoSignActivityAndFinish(activity, SignActivity.TYPE_DELETE, true)
                            gotoDeletePeerActivityAndFinish(activity)
                        }, cancel = {})
                    }, cancel = {})
        }


        /**
         * <탈퇴시 3개월 재가입 불가>
         *     1. 탈퇴했는지 체크
         *     2. 3개월 경과 체크
         **/
        fun checkPeerDeleteDate(email: String, success: (endDate: Long?) -> Unit, fail: () -> Unit) {
            val query = ESquery.peerDeletedDate(email)
            ESget.getPeerList(query,
                    success = { list ->
                        lllogI("SignActivity checkPeerDeleteDate list.size:${list?.size}")
                        val endDate = list?.get(0)?.delDate
                        success(endDate)
                    }, fail = { fail() })
        }


    }

}