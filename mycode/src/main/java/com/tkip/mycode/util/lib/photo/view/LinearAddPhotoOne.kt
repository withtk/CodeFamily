package com.tkip.mycode.util.lib.photo.view

import android.content.Context
import android.net.Uri
import android.view.LayoutInflater
import android.widget.LinearLayout
import androidx.databinding.DataBindingUtil
import com.tkip.mycode.R
import com.tkip.mycode.databinding.LinearAddPhotoOneBinding
import com.tkip.mycode.dialog.alert.MyAlert
import com.tkip.mycode.funs.common.lllogI
import com.tkip.mycode.funs.common.tranPair
import com.tkip.mycode.init.base.getStr
import com.tkip.mycode.init.inter.interAddPhotoOne
import com.tkip.mycode.util.lib.photo.photoview.OnPhotoView
import com.tkip.mycode.util.lib.photo.util.PhotoUtil
import com.tkip.mycode.util.tools.anim.goneFadeOut
import com.tkip.mycode.util.tools.anim.showOverUp
import com.tkip.mycode.util.tools.anim.visi

/**
 * photo 1개 올리는 거
 */
class LinearAddPhotoOne : LinearLayout {
    lateinit var b: LinearAddPhotoOneBinding

    var hasToBeDeletedPhoto = false  // 기존사진 삭제되어야 하는지 여부.
    var photoThumb:String? = null// 삭제될 url
    var photoUrl:String? = null// 삭제될 url

    constructor(context: Context) : super(context)
    constructor(context: Context, photoThumb: String?, photoUrl: String?, onDelete: (() -> Unit)?, onGallery: () -> Unit) : super(context) {
        b = DataBindingUtil.inflate(LayoutInflater.from(context), R.layout.linear_add_photo_one, this, true)
        this.photoThumb = photoThumb
        this.photoUrl = photoUrl
        b.thumbUrl = photoThumb

        b.inter = interAddPhotoOne(
                onClickDelete = { deleteImage() },
                onClickOpenGallery = { onGallery() })

        b.iv.setOnClickListener {
            OnPhotoView.gotoPhotoViewActivity(context, photoThumb, photoUrl, arrayOf(b.iv.tranPair(R.string.tran_image)))
        }

    }

    fun setImageUri(uri: Uri) {
        b.iv.setImageURI(uri)
        b.iv.visi()
        b.cons.showOverUp(true)
        PhotoUtil.resetAllUriList()
        PhotoUtil.photoUris.add(uri)
        hasToBeDeletedPhoto = true
        lllogI("LinearAddPhotoOne toBeDeletedFoto $hasToBeDeletedPhoto ")
    }

    fun deleteImage() {
        MyAlert.showConfirm(context, R.string.info_delete_store_menu.getStr(),
                ok = {
                    PhotoUtil.resetAllUriList()
                    b.cons.goneFadeOut(true)
                    b.iv.setImageDrawable(null)
                    hasToBeDeletedPhoto = true
                }, cancel = {})

    }

    /** <기존 이미지 삭제하기>
     * 1. 그냥 이전 이미지만 삭제.
     * 2. 새로운 이미지 올림.  */
//    fun checkDelete() {
//        if (toBeDeletedFoto) {
//            PhotoUtil.deletePhoto(photoUrl, photoThumb)
//            photoUrl = null
//            photoThumb = null
//        }
//    }


}