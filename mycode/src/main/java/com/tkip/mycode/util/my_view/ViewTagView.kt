package com.tkip.mycode.util.my_view

import android.content.Context
import android.view.LayoutInflater
import android.widget.LinearLayout
import androidx.databinding.DataBindingUtil
import com.tkip.mycode.R
import com.tkip.mycode.databinding.ViewTagViewBinding
import com.tkip.mycode.model.mytag.TAG_4
import com.tkip.mycode.model.mytag.setAll

/**
 * for store info
 */
class ViewTagView : LinearLayout {
    lateinit var b: ViewTagViewBinding

    constructor(context: Context) : super(context)
    constructor(context: Context, tagList: ArrayList<String>?) : super(context) {
        b = DataBindingUtil.inflate(LayoutInflater.from(context), R.layout.view_tag_view, this, true)

        b.tagView.setAll(TAG_4, tagList)

    }
}