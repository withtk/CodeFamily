package com.tkip.mycode.util.lib.map

import android.Manifest
import android.annotation.SuppressLint
import android.content.Intent
import android.content.pm.PackageManager
import android.os.Bundle
import android.os.Looper
import android.view.View
import androidx.annotation.LayoutRes
import androidx.core.app.ActivityCompat
import com.google.android.gms.location.*
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.Marker
import com.google.android.libraries.places.api.Places
import com.google.android.libraries.places.api.model.AutocompletePrediction
import com.google.android.libraries.places.api.net.PlacesClient
import com.tkip.mycode.R
import com.tkip.mycode.databinding.ActivityMainMapBinding
import com.tkip.mycode.dialog.toast.TToast
import com.tkip.mycode.funs.common.OnPermission
import com.tkip.mycode.funs.common.lllogD
import com.tkip.mycode.funs.common.lllogI
import com.tkip.mycode.funs.common.lllogM
import com.tkip.mycode.funs.search.OnSearch
import com.tkip.mycode.init.REQ_FINE_LOC
import com.tkip.mycode.init.my_bind.BindingActivity
import com.tkip.mycode.util.lib.map.google_map.PlaceAutoAdapter
import com.tkip.mycode.util.my_view.ViewUtil
import com.tkip.mycode.util.tools.anim.AnimUtil
import com.tkip.mycode.util.tools.etc.MyKeyboard


/**
 * 1. ADD, UPDATE : 위치 선택할 때 사용
 * 2. NORMAL : 위치 확인 할 때
 */
class MainMapActivity : BindingActivity<ActivityMainMapBinding>(), OnMapReadyCallback {
    @LayoutRes
    override fun getLayoutResId() = R.layout.activity_main_map
//    private lateinit var b: ActivityMainMapBinding

    private val actType: Int by lazy { intent.getIntExtra(PUT_MAP_ACT, MAP_ACT_ADD) }
    private val latlng: LatLng? by lazy { intent.getParcelableExtra<LatLng>(PUT_MAP_LATLNG) }

    private var map: GoogleMap? = null
    private var fusedLocationProviderClient: FusedLocationProviderClient? = null
    private lateinit var locationRequest: LocationRequest
    private lateinit var locationCallback: LocationCallback
    private lateinit var placesClient: PlacesClient

    private var currentMarker: Marker? = null
    private var currentAddress: String? = null
    private var currentPostalCode: String? = null

    private lateinit var poiAdapter: PlaceAutoAdapter

    private val items = arrayListOf<AutocompletePrediction>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
//        b = DataBindingUtil.setContentView(this, R.layout.activity_main_map)
        b.activity = this
        b.actType = actType

        initView()
        initMap()
    }

    override fun onMapReady(googleMap: GoogleMap) {
        lllogM("MainMapActivity onMapReady onMapReady")

        OnMap.isInitMapStyle(this@MainMapActivity, googleMap)

        map = googleMap
        map?.run {

            setPadding(15, 0, 0, 0)
            animateCamera(CameraUpdateFactory.zoomTo(15f))

            if (actType != MAP_ACT_NORMAL) {

                setOnMapClickListener {
                    lllogI("MainMapActivity onMapReady setOnMapClickListener : $it")
                }

                setOnMapLongClickListener { it.startMoveMap() }

                /*  setOnInfoWindowClickListener {
                      lllogD("setOnInfoWindowClickListener : ${it.position.toString()}")
                  }

                  setOnMarkerClickListener {
                      lllogD("setOnMarkerClickListener : ${it.position.toString()}")
                      it?.showInfoWindow()
                      return@setOnMarkerClickListener true
                  }*/
                setOnPoiClickListener {
                    lllogD("setOnPoiClickListener : ${it.name}, ${it.latLng}, ${it.placeId}")
                    it.latLng.startMoveMap()
                }
            }


            // 초기 위치.
            map?.moveMap(LATLNG_MANILA_AIRPORT, ZOOM_DEFAULT, false)

            // intent로 latlng가 있으면 지정되어 있으면 그 위치로 이동.
            latlng?.startMoveMap()

        }


        if (OnPermission.checkLocation()) {

            /**      * 현재위치 업데이트 후 이동    */
//            startLocationUpdates()

            /**      * 디바이스 마지막 위치로 이동    */
//            fusedLocationProvierClient?.getDeviceLocation(this@MainMapActivity,
//                    success = {
//                        OnMap.moveMap(map!!, LatLng(it.latitude, it.longitude), null, false)
//                    })
        } else {
            getLocationPermission()
        }

    }

    private fun initView() {

        poiAdapter = PlaceAutoAdapter(items,
                onSelect = {
                    OnMap.getPlaceLatlng(placesClient, it.placeId) {
                        map?.moveMap(it.place.latLng!!, ZOOM_POI, true)
                        OnSearch.resetSearch(b.lineSearchBar, b.rvPlace, b.tvNoResultPlace)
                        MyKeyboard.hide(b.lineAddress)
                    }
                })

        b.rvPlace.adapter = poiAdapter

    }

    private fun initMap() {
        val mapFragment = supportFragmentManager.findFragmentById(R.id.map) as SupportMapFragment
        mapFragment.getMapAsync(this)


        fusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(this)

        locationRequest = LocationRequest()
                .setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY)
//                .setInterval(5000)    //5초에 한번씩
        //                .setFastestInterval(500)

        LocationSettingsRequest.Builder().addLocationRequest(locationRequest)

        locationCallback = object : LocationCallback() {
            override fun onLocationResult(locationResult: LocationResult?) {
                super.onLocationResult(locationResult)

                locationResult?.let {
                    it.locations.let { locationList ->
                        if (!locationList.isEmpty()) {
                            val size = locationList.size
                            locationList[size - 1]?.let { location ->
                                MAP_CURRENT_LOCATION = location

                                // 마커 생성
//                                 val locaP = LatLng(mainLoc.latitude, mainLoc.longitude)
//                                 val markerTitle = OnMap.getGeocoder(this@MyMapActivity, locaP)
//                                 val markerSnippet = ("위도:" + mainLoc.latitude.toString() + " 경도:" + mainLoc.longitude.toString())
//                                 map?.addMarker(OnMap.getMarkerOpt(locaP, markerTitle, markerSnippet))
                            }
                        }
                    }
                }
            }
        }

        placesClient = Places.createClient(this)

    }

    private fun LatLng.startMoveMap() {
        when (actType) {
            MAP_ACT_ADD -> {
                AnimUtil.hideSlideUp(b.tvInfoLongClick, false)

                if (MAP_BOUNDARY.contains(this)) map?.setClickedLocation(this, true)
                else TToast.showNormal(R.string.error_not_boundary)
            }
            MAP_ACT_NORMAL -> {
                map?.setClickedLocation(this, false)
            }
        }
    }

    private fun GoogleMap.setClickedLocation(latlng1: LatLng, isShowLine: Boolean) {
        this.moveMap(latlng1, cameraPosition?.zoom, true)
        replaceCurrentMarker(latlng1, currentAddress, null, isShowLine)
    }

    /**
     * 현재 마커 클릭지점으로 수정하기.
     */
    private fun replaceCurrentMarker(latlng1: LatLng, title: String?, desc: String?, isShowLine: Boolean) {

        latlng1.getGeocoder(this@MainMapActivity,
                result = { address ->
                    /**                     * getAddress                     */
                    currentAddress = address.getAddressLine(0).toString()
                    address.postalCode?.let { currentPostalCode = it }

                    /**                     * setMarket                     */
                    currentMarker?.remove()
                    currentMarker = map?.addMarker(latlng1.getMarkerOpt(title, desc))
                    currentMarker?.showInfoWindow()

                    /**                     * show line address                     */
                    if (isShowLine) showLineAddress()
                })


    }

    private fun showLineAddress() {
        b.tvAddress.text = currentAddress
        AnimUtil.showSlideUp(b.lineAddress, false)
    }

    @SuppressLint("MissingPermission")
    override fun onResume() {
        super.onResume()
        if (OnPermission.checkLocation()) {
            fusedLocationProviderClient?.requestLocationUpdates(locationRequest, locationCallback, null)
            map?.isMyLocationEnabled = true
        }

    }

    override fun onStop() {
        super.onStop()
        fusedLocationProviderClient?.removeLocationUpdates(locationCallback)
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        when (requestCode) {
            REQ_FINE_LOC -> {
                if (grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED && grantResults[1] == PackageManager.PERMISSION_GRANTED) {
                    startLocationUpdates()
                } // else   OnPermission.informAbout_FINE_LOC_PERMISSION(this)
            }
        }
    }

    /**
     * getPermission
     */
    private fun getLocationPermission() {
        ActivityCompat.requestPermissions(this, arrayOf(Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION), REQ_FINE_LOC)
    }


    /**
     * 내 위치 받아오기.
     */
    @SuppressLint("MissingPermission")
    private fun startLocationUpdates() {
        map?.isMyLocationEnabled = true
        map?.uiSettings?.isMyLocationButtonEnabled = true
        if (OnMap.checkGPSandNETWORK(this))
            fusedLocationProviderClient?.requestLocationUpdates(locationRequest, locationCallback, Looper.myLooper())
    }


    fun onClickSubmitLocation() {
//        val latitude = currentMarker?.position?.latitude!!
//        val longitude = currentMarker?.position?.longitude!!

        val intent = Intent()
        intent.putExtra(PUT_MAP_ADDRESS, currentAddress)
        intent.putExtra(PUT_MAP_POSTAL_CODE, currentPostalCode)
        currentMarker?.let { intent.putExtra(PUT_MAP_LATLNG, it.position) }
        setResult(RESULT_OK, intent)
        finish()
    }

    fun onClickOpenSearchBar(view: View) {
        lllogD("onClickOpenSearchBar :")

        ViewUtil.addViewSearchBar(this@MainMapActivity, b.lineSearchBar,
                btnMenu = {
                    lllogD("btnMenu :")
                },
                search = { keyword ->
                    OnSearch.getPrediction_inList(placesClient, items, keyword,
                            result = {
                                poiAdapter.notifyDataSetChanged()
                                OnSearch.controlVisibility(items, b.rvPlace, b.tvNoResultPlace)
                            })
                },
                cancel = {
                    OnSearch.resetSearch(b.lineSearchBar, b.rvPlace, b.tvNoResultPlace)
                    AnimUtil.showSlideLeft(b.fabSearch, true)
                    MyKeyboard.hide(b.lineAddress)
                })

        AnimUtil.hideSlideRight(view, true)
    }

}