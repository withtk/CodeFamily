package com.tkip.mycode.util.lib.photo.glide

import android.app.Activity
import android.graphics.drawable.Drawable
import android.view.View
import android.widget.ImageView
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.tkip.mycode.R
import com.tkip.mycode.init.base.getDrawable
import com.tkip.mycode.util.lib.photo.Foto
import com.tkip.mycode.util.tools.anim.gone
import com.tkip.mycode.util.tools.anim.visi

/***********************************************************************************************
 ************************************ RequestOptions *********************************************
 ***********************************************************************************************/

//todo: 로딩중 icon 제작 할 것.
private val sharedOptions: RequestOptions = RequestOptions()
//        .placeholder(R.drawable.ic_no_image)
        .placeholder(R.drawable.ic_image_dark)
        .error(R.drawable.ic_error_image)
        .fitCenter()

private val requestOptions = RequestOptions()
//        .placeholder(R.drawable.ic_no_image)
        .placeholder(R.drawable.ic_image_dark)
        .error(R.drawable.ic_error_image)

private val requestOptionsGone = RequestOptions()
//        .placeholder(R.drawable.ic_no_image)
        .placeholder(R.drawable.ic_image_dark)

fun getGlideRequestOptions(resId: Int) = RequestOptions()
        .placeholder(resId)
        .error(R.drawable.ic_error_image)


/***********************************************************************************************
 ************************************ sttImageView *********************************************
 ***********************************************************************************************/

fun ImageView.setThumbImageGlide(foto: Foto?) {
    // 앨범 이미지일때
    foto?.thumbUri?.let {
        this.setImageURI(it)
        foto.orientation?.let { orientation -> this.rotation = orientation.toFloat() }
        visi()
    }

    // DB 이미지일때
    foto?.thumbUrl?.let { commonGlide(it, null, null) }
}

fun ImageView.setOriginImageGlide(foto: Foto?) {
    foto?.uri?.let { setImageURI(it) } // 앨범 이미지일때
    foto?.url?.let { commonGlide(it, null, null) } // DB 이미지일때
}

fun ImageView.setUrlGlide(url: String?) {
    commonGlide(url, null, View.GONE)
}

fun ImageView.setUrlGlideGone(url: String?) {
    commonGlide(url, null, View.GONE)
}

fun ImageView.setFaceGlide(url: String?) {
//    (context as? Activity)?.let { activity ->
//        if (!activity.isFinishing) {
            commonGlide(url, R.drawable.ic_person, null)
//        }
//    }
}

fun ImageView.commonGlide(url: String?, drawableId: Int?, visible: Int?) {
//    (context as? Activity)?.let { activity ->
//        if (!activity.isFinishing) {
    url?.let {
        Glide.with(context)
                .setDefaultRequestOptions(requestOptions)
                .load(it)
                .thumbnail(0.1f)
                .into(this)
        visi()
    } ?: let {
        if (drawableId == null) visible?.let { visibility = it }
        else this.setImageDrawable(drawableId.getDrawable())
    }
//        }
//    }
}
