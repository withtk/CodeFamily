package com.tkip.mycode.util.lib.photo

import android.graphics.Bitmap
import android.net.Uri
import android.os.Parcel
import android.os.Parcelable

/**
 * Created by Deviation on 2018-05-28.
 */

class MyPhoto(

        var uri: Uri? = null,
        var thumbUri: Uri? = null,    // 나중에 adapter내에서 썸을 가져오고나서 넣을 것임.
        var bitmap: Bitmap? = null,    //  android Q 비트맵 만들기.   나중에 async에서 추가함.  AlbumSelectedAdapter에서만 사용. intent넘길때는 삭제할 것.
        var photoPath: String? = null,
        var photoId: String? = null,
        var date: String? = null,
        var size: String? = null,
        var orientation: String? = null,

        var folder: String? = null,
        var isSel: Boolean = false

) : Parcelable {
    /**
     * 기본
     */
    constructor(uri: Uri?, thumbUri: Uri?, photoPath: String?, photoId: String?, date: String?, size: String?, orientation: String?, folder: String?, isSel: Boolean) : this(
            uri = uri,
            thumbUri = thumbUri,
            bitmap = null,
            photoPath = photoPath,
            photoId = photoId,
            date = date,
            size = size,
            orientation = orientation,
            folder = folder,
            isSel = isSel
    )

    constructor(source: Parcel) : this(
            source.readParcelable<Uri>(Uri::class.java.classLoader),
            source.readParcelable<Uri>(Uri::class.java.classLoader),
            source.readParcelable<Bitmap>(Bitmap::class.java.classLoader),
            source.readString(),
            source.readString(),
            source.readString(),
            source.readString(),
            source.readString(),
            source.readString(),
            1 == source.readInt()
    )

    override fun describeContents() = 0

    override fun writeToParcel(dest: Parcel, flags: Int) = with(dest) {
        writeParcelable(uri, 0)
        writeParcelable(thumbUri, 0)
        writeParcelable(bitmap, 0)
        writeString(photoPath)
        writeString(photoId)
        writeString(date)
        writeString(size)
        writeString(orientation)
        writeString(folder)
        writeInt((if (isSel) 1 else 0))
    }

    companion object {
        @JvmField
        val CREATOR: Parcelable.Creator<MyPhoto> = object : Parcelable.Creator<MyPhoto> {
            override fun createFromParcel(source: Parcel): MyPhoto = MyPhoto(source)
            override fun newArray(size: Int): Array<MyPhoto?> = arrayOfNulls(size)
        }
    }
}


