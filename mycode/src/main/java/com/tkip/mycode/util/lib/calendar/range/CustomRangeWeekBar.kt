package com.tkip.mycode.util.lib.calendar.range

import android.content.Context
import android.view.LayoutInflater
import android.widget.TextView
import com.haibin.calendarview.WeekBar
import com.tkip.mycode.R
import com.tkip.mycode.init.base.getClr

/**
 */
class CustomRangeWeekBar(context: Context) : WeekBar(context) {

    init {
        LayoutInflater.from(context).inflate(R.layout.cal_range_week_bar, this, true)
//        setBackgroundColor(R.color.md_amber_400.getClr())
        setBackgroundColor(R.color.transparent.getClr())
    }

    override fun onWeekStartChange(weekStart: Int) {
        for (i in 0 until childCount) {
            (getChildAt(i) as TextView).text = getWeekString(i, weekStart)
        }
    }

    private fun getWeekString(index: Int, weekStart: Int): String {
        val weeks = context.resources.getStringArray(R.array.english_week_string_array)
        if (weekStart == 1) {
            return weeks[index]
        }
        return if (weekStart == 2) {
            weeks[if (index == 6) 0 else index + 1]
        } else weeks[if (index == 0) 6 else index - 1]
    }

}