package com.tkip.mycode.util.system.notification

import android.os.Parcel
import android.os.Parcelable
import com.google.firebase.messaging.RemoteMessage
import com.tkip.mycode.R
import com.tkip.mycode.funs.common.lllogD
import com.tkip.mycode.init.base.cutName
import com.tkip.mycode.init.base.getStr
import com.tkip.mycode.init.base.isNumeric
import com.tkip.mycode.model.my_enum.DataType
import com.tkip.mycode.model.my_enum.Status
import com.tkip.mycode.model.my_enum.dataType
import com.tkip.mycode.model.my_enum.getStatus

data class Noti(

//        var type: Int? = null,    // notiChannel.no 와 동일.
        var nch: NotiChannel = NotiChannel.INFO,

        var senderUid: String? = null,
        var senderName: String? = null,
        var title: String? = null,
        var body: String? = null,
        var extra: String? = null,

        ////////////////// model /////////////////
        var dataTypeNo: Int? = null,
        var modelKey: String? = null,
        var modelTitle: String? = null,
        var photoThumb: String? = null

) : Parcelable {

    constructor(remoteMessage: RemoteMessage) : this(

            nch = remoteMessage.data["type"]?.toInt()?.getNotiChannel() ?: NotiChannel.INFO,

            senderUid = remoteMessage.data["senderUid"].let { if (it.isNullOrBlank()) null else it },
            senderName = remoteMessage.data["senderName"].let { if (it.isNullOrBlank()) null else it },
            title = remoteMessage.data["title"].let { if (it.isNullOrBlank()) null else it },
            body = remoteMessage.data["body"].let { if (it.isNullOrBlank()) null else it },
            extra = remoteMessage.data["extra"].let { if (it.isNullOrBlank()) null else it },

            dataTypeNo = if (remoteMessage.data["dataTypeNo"].isNullOrBlank()) 0 else remoteMessage.data["dataTypeNo"]?.toInt(),
            modelKey = remoteMessage.data["modelKey"].let { if (it.isNullOrBlank()) null else it },
            modelTitle = remoteMessage.data["modelTitle"].let { if (it.isNullOrBlank()) null else it },
            photoThumb = remoteMessage.data["photoThumb"].let { if (it.isNullOrBlank()) null else it }
    )

    constructor(source: Parcel) : this(
            NotiChannel.values()[source.readInt()],
            source.readString(),
            source.readString(),
            source.readString(),
            source.readString(),
            source.readString(),
            source.readValue(Int::class.java.classLoader) as Int?,
            source.readString(),
            source.readString(),
            source.readString()
    )

    override fun describeContents() = 0

    override fun writeToParcel(dest: Parcel, flags: Int) = with(dest) {
        writeInt(nch.ordinal)
        writeString(senderUid)
        writeString(senderName)
        writeString(title)
        writeString(body)
        writeString(extra)
        writeValue(dataTypeNo)
        writeString(modelKey)
        writeString(modelTitle)
        writeString(photoThumb)
    }

    companion object {
        fun create(remoteMessage: RemoteMessage) =
                Noti(remoteMessage).apply {
                    lllogD("Noti create Noti remoteMessage $this")
                    setNotiTitle()
                    setNotiBody()
                    lllogD("Noti create Noti $this")
                }

        private fun Noti.setNotiTitle() {
            this.title = when (this.nch) {
                NotiChannel.REPLY -> String.format(R.string.noti_noti_reply.getStr(), modelTitle)
                NotiChannel.POST -> String.format(R.string.noti_notititle_post.getStr(), title?.cutName(20))
                NotiChannel.FLEA -> String.format(R.string.noti_notititle_flea.getStr(), title?.cutName(12))
                NotiChannel.ROOM -> String.format(R.string.noti_notititle_room.getStr(), title?.cutName(12))

                NotiChannel.DEFAULT ->
                    if (dataTypeNo == DataType.PEER.no) String.format(R.string.noti_notititle_default_login.getStr(), senderName)
                    else R.string.noti_desc_info.getStr()
                NotiChannel.INFO -> R.string.noti_notititle_info.getStr()
                NotiChannel.WARN -> "${this.senderName} : ${R.string.noti_notititle_warn.getStr()}"
                NotiChannel.GRANT ->
                    /*** extra == grant.statusNo */
                    when {
                        extra?.toInt()?.getStatus() == Status.GRANT_REQUESTED -> "${this.dataTypeNo?.dataType()?.title} ${R.string.noti_notititle_grant_requested.getStr()}"
//                        extra?.toInt()?.getStatus() == Status.GRANT_HANDLED -> "${this.dataTypeNo?.dataType()?.title} ${R.string.noti_notititle_grant_handled.getStr()}"
                        extra?.toInt()?.getStatus() == Status.GRANT_HANDLED -> this.title
                        else -> R.string.noti_desc_info.getStr()
                    }
                NotiChannel.PENALTY -> R.string.noti_notititle_penalty.getStr()

                NotiChannel.BID_FAIL -> R.string.noti_notititle_bid_fail.getStr()
                NotiChannel.BID_SUCCESS -> R.string.noti_notititle_bid_success.getStr()
                else -> ""
            }
        }

        private fun Noti.setNotiBody() {

            this.body = when (this.nch) {
//                NotiChannel.CHAT -> this.body
                NotiChannel.REPLY -> "${if (this.senderName.isNullOrBlank()) R.string.nick_anonymous_noti.getStr() else this.senderName} : ${this.body}"
                NotiChannel.POST -> this.body
                NotiChannel.FLEA -> this.body
                NotiChannel.ROOM -> this.body

                NotiChannel.DEFAULT -> this.body
                NotiChannel.INFO -> this.body
                NotiChannel.WARN -> "${this.extra} : ${this.modelTitle}"
                NotiChannel.GRANT ->
                    /** extra == grant.statusNo
                     *  senderName == grant.modelStatusNo
                     *  body == comment or reason
                     * */
                    this.body
//                    when {
//                        extra?.toInt()?.getStatus() == Status.GRANT_REQUESTED -> if (body == null) "${this.modelTitle}" else body
//                        extra?.toInt()?.getStatus() == Status.GRANT_HANDLED -> if (body == null) if (senderName?.isNumeric() == true) senderName?.toInt()?.getStatus()?.msg else null else body
////                        extra?.toInt()?.getStatus() == Status.GRANT_HANDLED -> if (body == null) if (senderName?.isNumeric() == true) senderName?.toInt()?.getStatus()?.msg else null else body
//                        else -> R.string.noti_desc_info.getStr()
//                    }
                NotiChannel.PENALTY -> this.body

                NotiChannel.BID_FAIL -> this.body
                NotiChannel.BID_SUCCESS -> this.body
                else -> ""
            }
        }

        @JvmField
        val CREATOR: Parcelable.Creator<Noti> = object : Parcelable.Creator<Noti> {
            override fun createFromParcel(source: Parcel): Noti = Noti(source)
            override fun newArray(size: Int): Array<Noti?> = arrayOfNulls(size)
        }
    }
}
