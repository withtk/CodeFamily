package com.tkip.mycode.util.my_view

import android.content.Context
import android.view.LayoutInflater
import android.widget.LinearLayout
import androidx.databinding.DataBindingUtil
import com.tkip.mycode.R
import com.tkip.mycode.admin.menu.AMenu
import com.tkip.mycode.databinding.ViewButtonLightBinding
import com.tkip.mycode.util.tools.etc.OnMyClick


class ViewButtonLight : LinearLayout {
    private lateinit var b: ViewButtonLightBinding

    constructor(context: Context) : super(context)
    constructor(context: Context, amenu: AMenu, onClick: (AMenu) -> Unit ) : super(context) {

        b = DataBindingUtil.inflate(LayoutInflater.from(context), R.layout.view_button_light, this, true)
        b.amenu = amenu

        b.btn.setOnClickListener {
            if (OnMyClick.isDoubleClick()) return@setOnClickListener
            onClick(amenu)
        }
    }


//
//    constructor(context: Context, attrs: AttributeSet?) : super(context, attrs) {
//        init()
//    }
//
//    constructor(context: Context, attrs: AttributeSet?, defStyleAttr: Int) : super(context, attrs, defStyleAttr) {
//        init()
//    }
//


}