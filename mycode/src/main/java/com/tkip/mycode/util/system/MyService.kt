package com.tkip.mycode.util.system

import android.app.Service
import android.content.Intent
import android.os.IBinder
import com.tkip.mycode.init.part.CodeApp
import com.tkip.mycode.funs.common.lllogI

class MyService : Service() {

    companion object {
        private val serviceIntent: Intent = Intent(CodeApp.getAppContext(), MyService::class.java)

        fun startMyService( ) {
            val context = CodeApp.getAppContext()
            context.startService(serviceIntent)
        }
    }

    override fun onBind(intent: Intent?): IBinder? {
        return null
    }

    override fun onCreate() {
        super.onCreate()
        lllogI("MyService onCreate")


        /** 실시간 StoreMin List */
//        OnServer.realTimeStoreMinList()
        /** 실시간 Store List */
//        OnServer.realTimeStoreList()
        /** 실시간 AskSearch */
//        OnServer.realTimeAskSearch()

    }

    override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int {
        lllogI("MyService onStartCommand")
        return super.onStartCommand(intent, flags, startId)
    }

    override fun onDestroy() {
        lllogI("MyService onDestroy")

        super.onDestroy()

    }


/*

    fun setAlarm(){
        val c = Calendar.getInstance()
        c.timeInMillis = System.currentTimeMillis()
        c.add(Calendar.SECOND,1)

        Intent intent = new Intent(this, AlarmRecever.class);
        PendingIntent sender = PendingIntent.getBroadcast(this, 0,intent,0);



    }

    protected void setAlarmTimer() {
        final Calendar c = Calendar.getInstance();
        c.setTimeInMillis(System.currentTimeMillis());
        c.add(Calendar.SECOND, 1);
        Intent intent = new Intent(this, AlarmRecever.class);
        PendingIntent sender = PendingIntent.getBroadcast(this, 0,intent,0);

        AlarmManager mAlarmManager = (AlarmManager) getSystemService(Context.ALARM_SERVICE);
        mAlarmManager.set(AlarmManager.RTC_WAKEUP, c.getTimeInMillis(), sender);
    }


*/


}