package com.tkip.mycode.util.lib.photo.photoview

import android.os.Bundle
import android.os.Handler
import android.view.View
import androidx.databinding.DataBindingUtil
import com.tkip.mycode.R
import com.tkip.mycode.databinding.ActivityPhotoViewBinding
import com.tkip.mycode.init.PUT_ARRAY_FOTO
import com.tkip.mycode.init.PUT_ARRAY_MYPHOTO
import com.tkip.mycode.init.PUT_INT
import com.tkip.mycode.init.my_bind.BaseActivity
import com.tkip.mycode.util.lib.photo.Foto
import com.tkip.mycode.util.lib.photo.MyPhoto


/**
 *
 *  1. myPhoto list
 *
 *  2. url list
 *  3. url + uri list
 *  url와 myPhoto가 섞여서 들어올 수도 있기 때문에 모두 PHOTO로 변환해서 써야 한다.
 */
class PhotoViewActivity : BaseActivity() {

    private lateinit var b: ActivityPhotoViewBinding
    private lateinit var pager: PhotoViewUrlPhotoPager
    private lateinit var items: ArrayList<Foto>

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        b = DataBindingUtil.setContentView(this, R.layout.activity_photo_view)
        b.activity = this

        // 화면 계속 켜놓기.
//        getWindow().setFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON, WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON)


        val position = intent.getIntExtra(PUT_INT, 0)

        intent.getParcelableArrayListExtra<MyPhoto>(PUT_ARRAY_MYPHOTO)?.let {
            items = arrayListOf()
            it.forEach { myPhoto -> items.add(Foto(myPhoto)) }
        }

        intent.getParcelableArrayListExtra<Foto>(PUT_ARRAY_FOTO)?.let {
            items = it
        }

        /*** PhotoPager 셋팅 */
        pager = PhotoViewUrlPhotoPager(items, click = { toggle() })
        b.viewPager.adapter = pager
        b.viewPager.currentItem = position

        /**         * 리프레쉬 : 가끔 안 보일때가 있다.         */
        Handler().postDelayed({
            pager.notifyDataSetChanged()
        }, 1000)

        if (items.size > 10) {  // 너무 많으면 이상하니까.. 나중에 숫자로 바꾸든가 해야지.
            b.pageIndicator.visibility = View.GONE
        }

        toggle()

    }


    var onStatusBar = false
    private fun toggle() {
        if (onStatusBar) {
            hideSystemUI()
            onStatusBar = false
        } else {
            showSystemUI()
            onStatusBar = true
        }
    }

    private fun hideSystemUI() {
        // Enables regular immersive mode.
        // For "lean back" mode, remove SYSTEM_UI_FLAG_IMMERSIVE.
        // Or for "sticky immersive," replace it with SYSTEM_UI_FLAG_IMMERSIVE
        val decorView = window.decorView
        decorView.systemUiVisibility = (View.SYSTEM_UI_FLAG_IMMERSIVE
                // Set the content to appear under the system bars so that the
                // content doesn't resize when the system bars hide and show.
                or View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                or View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                or View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                // Hide the nav bar and statusNo bar
                or View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                or View.SYSTEM_UI_FLAG_FULLSCREEN)

    }

    // Shows the system bars by removing all the flags
    // except for the ones that make the content appear under the system bars.
    private fun showSystemUI() {
        val decorView = window.decorView
        decorView.systemUiVisibility = (View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                or View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                or View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN)
    }


}
