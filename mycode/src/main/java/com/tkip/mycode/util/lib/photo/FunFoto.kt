package com.tkip.mycode.util.lib.photo

import com.tkip.mycode.R
import com.tkip.mycode.dialog.toast.TOAST_NORMAL
import com.tkip.mycode.dialog.toast.TToast
import com.tkip.mycode.dialog.toast.checkAndToast
import com.tkip.mycode.init.base.getI
import com.tkip.mycode.init.base.getInt
import com.tkip.mycode.init.base.getStr
import com.tkip.mycode.init.base.isAnyNull
import com.tkip.mycode.model.flea.inValid
import java.util.*


/************************************** * stFOTO *************************************/

/** * update시 or recreate시 기존list를 selectedList에 세팅. */
fun ArrayList<Foto>.initSelectedFotoList(list: ArrayList<Foto>) {
    clear()
    addAll(list)
//    list.forEachIndexed { i, f -> add(i, f) }
}


/** * 앨범에서 돌아온 뒤 selectedFotoList 재세팅 : 중복체크 및 추가 */
fun ArrayList<Foto>.resetFromAlbum(albumList: ArrayList<MyPhoto>) {
    val newList = arrayListOf<Foto>()
    /** 중복 체크 후 add */
    albumList.forEach { myP -> if (!this.has(myP)) newList.add(Foto(myP)) }
    addAll(newList)
}

//fun ArrayList<Foto>.resetFromAlbum(uri: Uri) {
//    /** 중복 체크 후 add */
//    this.forEach { f -> if (f.uri == uri) return }
//    val thumb = uri.createMyThumb()
//    this.add(Foto(uri, thumb))
//}


fun ArrayList<Foto>.has(myPhoto: MyPhoto): Boolean {
    for (foto in this) {
        if (myPhoto.uri == foto.uri) return true
    }
    return false
}

fun ArrayList<Foto>.has(foto: Foto): Boolean {
    for (f in this) {
        if (f.key == foto.key) return true
    }
    return false
}

/**
 * <fotoList 체크>
 * 1. 한번에 올릴 수 있는 수량 제한.
 * 2. 1개 미만. */
fun ArrayList<Foto>?.invalidFotoList(): Boolean {
    return when {
        (this.getI(0).isAnyNull(R.string.alert_need_to_photo)) -> true
        (this != null && size > R.integer.limit_upload_photo.getInt()).checkAndToast(TOAST_NORMAL, R.string.info_limit_upload) -> true
        else -> false
    }
}

/**
 * <fotoList 체크>
 * 1. 한번에 올릴 수 있는 수량 제한. */
fun ArrayList<Foto>?.isOverLimit(): Boolean {
    return when {
        (this != null && size > R.integer.limit_upload_photo.getInt()).checkAndToast(TOAST_NORMAL, R.string.info_limit_upload) -> true
        else -> false
    }
}