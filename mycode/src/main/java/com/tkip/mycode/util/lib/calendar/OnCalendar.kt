package com.tkip.mycode.util.lib.calendar

import com.haibin.calendarview.Calendar
import com.tkip.mycode.funs.common.lllogI
import java.util.*

/**
 * 결제 관련. 포인트 사용 관련
 */
class OnCalendar {

    companion object {

        private val aDAY = 1000 * 3600 * 24
        private val date = java.util.Calendar.getInstance()


        /**************************************************************
         **************************************************************/
        private fun getDates(start: Long, end: Long) = ((start - end) / aDAY).toInt()

        private fun getMillis(calendar: Calendar): Long {
            date.set(calendar.year, calendar.month, calendar.day)
            return date.timeInMillis
        }

//        fun getMillis(year: Int, month: Int, day: Int): Long {
//            date.set(year, month, day)
//            return date.timeInMillis
//        }


        /**
         * 날짜 계산 (숙박 수)
         */
        fun gap(cal1: Calendar, cal2: Calendar): Int {
            val start = getMillis(cal1)
            val end = getMillis(cal2)
            return getDates(start, end)
        }
//        fun gap(tri1: Triple<Int, Int, Int>, tri2: Triple<Int, Int, Int>): Int {
//            val start = getMillis(tri1.first, tri1.second, tri1.third)
//            val end = getMillis(tri2.first, tri2.second, tri2.third)
//            return getDates(start, end)
//        }


        /**
         * 오늘보다 과거인지 체크.
         */
        fun isPast(calendar: Calendar) = getDates(System.currentTimeMillis(), getMillis(calendar)) > 0


        /** * range 선택 : 모두 set에 넣기 */
        fun setSetFromRange(term: Int, startDate8: Int, iSet: ArrayList<Int>) {
            iSet.clear()
            iSet.add(startDate8)
            lllogI("CustomCalendarViewRange startDate8 $startDate8")

            val gCal = startDate8.convertCalendar()
            (1..term).forEach {
                gCal.add(java.util.Calendar.DATE, 1)
                iSet.add(gCal.toDate8())
                lllogI("CustomCalendarViewRange gCal.toDate8() : ${gCal.toDate8()}")
            }
        }



        /**
         * return : 특정나라의 현재시간long - 특정시간long
         * hourOfDay : 24시간제. null이면 특정나라의 현재시간.
         */
        fun getLocaleLong(locale: String, hourOfDay: Int ): Long {
            val timezone = TimeZone.getTimeZone(locale)
            val cal2 = java.util.Calendar.getInstance()
            cal2.timeZone = timezone
            val nowLong = cal2.timeInMillis    // 그 나라 현재시간
            lllogI("timetest nowLong ${cal2.get(java.util.Calendar.YEAR)}Y ${cal2.get(java.util.Calendar.MONTH)}M ${cal2.get(java.util.Calendar.DATE)}D ${cal2.get(java.util.Calendar.HOUR_OF_DAY)}:${cal2.get(java.util.Calendar.MINUTE)}:${cal2.get(java.util.Calendar.SECOND)}")
            cal2.set(java.util.Calendar.MINUTE, 0)
            cal2.set(java.util.Calendar.SECOND, 0)
            cal2.set(java.util.Calendar.HOUR_OF_DAY, hourOfDay)
            val nowTimeLong = cal2.timeInMillis   // 그 나라 특정시간
            lllogI("timetest nowLong ${cal2.get(java.util.Calendar.YEAR)}Y ${cal2.get(java.util.Calendar.MONTH)}M ${cal2.get(java.util.Calendar.DATE)}D ${cal2.get(java.util.Calendar.HOUR_OF_DAY)}:${cal2.get(java.util.Calendar.MINUTE)}:${cal2.get(java.util.Calendar.SECOND)}")

//    (nowLong - nowTimeLong) > 0
            return cal2.timeInMillis
        }


        /**
         *
         */
        fun getLongAfterMonth(month: Int ): Long {
            val cal2 = java.util.Calendar.getInstance()
            val nowLong = cal2.timeInMillis    //   현재시간
            cal2.add(java.util.Calendar.MONTH,month)
            val afterLong = cal2.timeInMillis  //   현재시간 + month
            lllogI("getLongAfterMonth nowLong $nowLong  afterLong $afterLong " )
            return afterLong
        }



    }

}