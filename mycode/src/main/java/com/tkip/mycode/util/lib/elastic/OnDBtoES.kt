package com.tkip.mycode.util.lib.elastic

import android.os.Handler
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.database.ValueEventListener
import com.google.firebase.firestore.DocumentReference
import com.tkip.mycode.funs.common.lllogD
import com.tkip.mycode.funs.common.lllogI
import com.tkip.mycode.funs.common.lllogM
import com.tkip.mycode.init.*
import com.tkip.mycode.model.bid.Bid
import com.tkip.mycode.model.board.Board
import com.tkip.mycode.model.cost.Cost
import com.tkip.mycode.model.count.Count
import com.tkip.mycode.model.count.CountType
import com.tkip.mycode.model.flea.Flea
import com.tkip.mycode.model.my_enum.DataType
import com.tkip.mycode.model.my_enum.anyAddDate
import com.tkip.mycode.model.my_enum.anyKey
import com.tkip.mycode.model.mytag.MyTag
import com.tkip.mycode.model.peer.Peer
import com.tkip.mycode.model.post.Post
import com.tkip.mycode.model.reply.Reply
import com.tkip.mycode.model.room.Room
import com.tkip.mycode.model.store.Store
import com.tkip.mycode.model.store.StoreMenu
import com.tkip.mycode.model.warn.Warn
import com.tkip.mycode.util.lib.retrofit2.ESget
import com.tkip.mycode.util.lib.retrofit2.ESquery
import com.tkip.mycode.util.lib.retrofit2.Retro
import com.tkip.mycode.util.tools.fire.*
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch

/**
 * Hi~ Created by fabulous on 20-07-06
 *
 * 데이터 옮기기
 *    1. 전체맵핑.
 *    2. 전체 es To es 실행.
 *    3. functions config 수정.
 *    4. remote 수정.
 */
class OnDBtoES {
    companion object {


        /****************************************************************************************************************************************
         *************************** 데이타 업데이트 : FireStore or RealDB -> Es ******************************************************************
         *****************************************************************************************************************************************/
        private val MAX_BATCH_LENGTH = 500
        private val DELAY_TIME = 1000L
        private val queryMatchAll by lazy { ESquery.matchAll(10000, 0) }

        fun updateAllData() {
            lllogM("updateAllData updateAllData start")

//          Handler().postDelayed({ GlobalScope.launch { allUpdatePeerInner() }},DELAY_TIME)
//          Handler().postDelayed({ GlobalScope.launch { allUpdateModelAndReply() }},DELAY_TIME)
//          Handler().postDelayed({ GlobalScope.launch { allUpdateSingleChild() }},DELAY_TIME)
//          Handler().postDelayed({ GlobalScope.launch { realDBupdateAll() }},DELAY_TIME)

            Handler().postDelayed({ GlobalScope.launch { singleIndexAddToElasticSearch("updateAllData", EES_COST, queryMatchAll) } }, DELAY_TIME)

        }

        val esList by lazy { arrayListOf<Triple<DocumentReference, String?, Any?>>() }   // 업로드할 리스트.

        fun clear() {
            esList.clear()
        }

        fun add(tri: Triple<DocumentReference, String?, Any?>) {
            esList.add(tri)
        }

        /**
         * 500개씩 나눠서 batch로 만들어서 commit
         * tri.second == null -> SET
         *               else -> UPDATE
         */
        fun addToFireS(method: String?, onSuccess: (() -> Unit)?) {
            esList.chunked(MAX_BATCH_LENGTH).forEachIndexed { i, list500 ->
                lllogI("ones gotoBatch list500 ($i:${list500.size})")
                batch().apply {
                    list500.forEach { tri ->
                        lllogI("ones gotoBatch list500  ${tri.first}")

                        /*** tri.second의 유무로 add or update 구분. */
                        when {
                            tri.second == null && tri.third == null -> {
                                lllogI("ones gotoBatch list500 delete  ")
                                delete(tri.first)   // 삭제
                            }
                            tri.second == null -> {
                                lllogI("ones gotoBatch list500 set  ")
                                set(tri.first, tri.third!!)   // 삽입
                            }
                            else -> {
                                lllogI("ones gotoBatch list500 update  ")
                                update(tri.first, tri.second!!, tri.third)  // 수정
                            }
                        }
                    }
                }.myCommit(method, success = {
                    lllogI("OnEs gotoBatch $method success $i ")
                    onSuccess?.invoke()
                }, fail = { lllogI("OnEs $method fail $i ") })
            }
        }

        fun checkListSize() {
            lllogD("OnDBtoES checkListSize size: ${esList.size}")
        }

        /*****************************************************************************************
         *****************************************************************************************
         *****************************************************************************************/

        /*** peer 내부 data 넣기.*/
        private fun allUpdatePeerInner() {
            val method = "allUpdatePeer"


            /*** 모든 유저 받아오기 */
            arrayOf(Pair(EES_PEER, Peer::class.java)
            ).forEach { ttt ->
                colRef(ttt.first).get()
                        .addOnSuccessListener {
                            it.forEach { peerSnap ->
                                lllogI("Ones $method ${peerSnap.id}")
                                val d = peerSnap.toObject(ttt.second)
                                esList.add(Triple(docRef(ttt.first, peerSnap.id), CH_ADD_DATE, d.addDate + 1))   // add to List

                                /*** 유저내의 cost, act, quick, banner 받아오기 */
                                arrayOf(
//                                        Pair(EES_COST, Cost::class.java),
                                        Pair(EES_QUICK, Cost::class.java)
//                                        Pair(EES_BANNER, Bid::class.java)
                                ).forEach { rrr ->
                                    lllogI("Ones $method (${peerSnap.id}:${rrr.first}) ")
                                    colRef(ttt.first, peerSnap.id, rrr.first).get()
                                            .addOnSuccessListener { qSnap ->
                                                qSnap.forEach { snap ->
                                                    val d2 = snap.toObject(rrr.second)
                                                    lllogI("Ones $method (${peerSnap.id}:${rrr.first}:${d2.anyKey()})")
                                                    esList.add(Triple(docRef(EES_PEER, peerSnap.id, rrr.first, snap.id), CH_ADD_DATE, d2.anyAddDate() + 1))   // add to List
                                                }
                                            }
                                            .addOnFailureListener { lllogI("Ones $method fail ${rrr.second}") }
                                }
                            }
//                            gotoBatch(method, list)   // add to DB
                        }.addOnFailureListener { lllogI("changePeerInner fail 1 ") }
            }
        }


        /*** model & reply */
        private fun allUpdateModelAndReply() {
            val method = "allUpdateModelInner"

            /*** topChild 받아오기 */
            arrayOf(
                    Pair(EES_BOARD, Board::class.java),
                    Pair(EES_POST, Post::class.java),
                    Pair(EES_FLEA, Flea::class.java),
                    Pair(EES_ROOM, Room::class.java),
                    Pair(EES_STORE, Store::class.java)
            ).forEach { ppp ->
                colRef(ppp.first).get()
                        .addOnSuccessListener { qSnap1 ->
                            qSnap1.forEach { snap1 ->
                                lllogI("Ones $method (${ppp.first}:${snap1.id})")
                                val d1 = snap1.toObject(ppp.second)
                                esList.add(Triple(docRef(ppp.first, snap1.id), CH_ADD_DATE, d1.anyAddDate() + 1))   // add to List

                                /*** secondChild 받아오기 */
                                arrayOf(
                                        Pair(EES_REPLY, Reply::class.java)
                                ).forEach { rrr ->
                                    lllogI("Ones $method (${snap1.id}:${rrr.first}) ")
                                    colRef(ppp.first, snap1.id, rrr.first).get()
                                            .addOnSuccessListener { qSnap2 ->
                                                qSnap2.forEach { snap2 ->
                                                    val d2 = snap2.toObject(rrr.second)
                                                    lllogI("Ones $method (${ppp.first} : ${snap1.id} : ${rrr.first} : ${snap2.id})")
                                                    esList.add(Triple(docRef(ppp.first, snap1.id, rrr.first, snap2.id), CH_ADD_DATE, d2.anyAddDate() + 1))   // add to List
                                                }
                                            }
                                            .addOnFailureListener { lllogI("Ones $method fail ${rrr.second}") }
                                }
                            }
//                            gotoBatch(method, list)   // add to DB
                        }.addOnFailureListener { lllogI("changePeerInner fail 1 ") }
            }
        }


        /*** 1개짜리 Child */
        private fun allUpdateSingleChild() {
            val method = "allUpdateSingleChild"

            /*** topChild 받아오기 */
            arrayOf(

                    Pair(EES_COST, Cost::class.java),
                    Pair(EES_BANNER, Bid::class.java),

                    Pair(EES_STORE_MENU, StoreMenu::class.java),

                    Pair(EES_WARN, Warn::class.java),
                    Pair(EES_BID, Bid::class.java),
                    Pair(EES_TAG, MyTag::class.java),
//                    Pair(EES_CHAT, Chat::class.java),
//                    Pair(EES_LOBBY, Lobby::class.java),


                    /*** Board */
                    Pair(CountType.BOOK.getTopChild(DataType.BOARD), Count::class.java),
                    Pair(CountType.PUSH.getTopChild(DataType.BOARD), Count::class.java),

                    /*** Post */
                    Pair(CountType.VIEW_UID.getTopChild(DataType.POST), Count::class.java),
                    Pair(CountType.UP.getTopChild(DataType.POST), Count::class.java),
                    Pair(CountType.DOWN.getTopChild(DataType.POST), Count::class.java),
                    Pair(CountType.BOOK.getTopChild(DataType.POST), Count::class.java),
                    Pair(CountType.PUSH.getTopChild(DataType.POST), Count::class.java),


                    /*** Flea */
                    Pair(CountType.VIEW_UID.getTopChild(DataType.FLEA), Count::class.java),
                    Pair(CountType.BOOK.getTopChild(DataType.FLEA), Count::class.java),
                    Pair(CountType.PUSH.getTopChild(DataType.FLEA), Count::class.java),

                    /*** Room */
                    Pair(CountType.VIEW_UID.getTopChild(DataType.ROOM), Count::class.java),
                    Pair(CountType.BOOK.getTopChild(DataType.ROOM), Count::class.java),
                    Pair(CountType.PUSH.getTopChild(DataType.ROOM), Count::class.java),

                    /*** Store */
                    Pair(CountType.BOOK.getTopChild(DataType.STORE), Count::class.java),
                    Pair(CountType.PUSH.getTopChild(DataType.STORE), Count::class.java)

            ).forEach { ttt ->
                colRef(ttt.first).get().addOnSuccessListener { qSnap1 ->
                    qSnap1.forEach { snap1 ->
                        lllogI("Ones $method (${ttt.first}:${snap1.id})")
                        val d1 = snap1.toObject(ttt.second)
                        esList.add(Triple(docRef(ttt.first, snap1.id), CH_ADD_DATE, d1.anyAddDate() + 1))   // add to List
                    }
//                    gotoBatch(method, list)   // add to DB
                }.addOnFailureListener { lllogI("ones $method fail ${ttt.first} : \n${it.message}") }
            }
        }


        /****         * for real DB         */
        private fun realDBupdateAll() {
            val method = "realDBupdateAll"
            val list = arrayListOf<Pair<String, Any?>>()
            arrayOf(
                    /*** ViewCount : post, flea, room */
                    Pair(CountType.VIEW.getTopChild(DataType.POST), Count::class.java),
                    Pair(CountType.VIEW.getTopChild(DataType.FLEA), Count::class.java),
                    Pair(CountType.VIEW.getTopChild(DataType.ROOM), Count::class.java),
                    Pair(CountType.VIEW.getTopChild(DataType.STORE), Count::class.java)
            ).forEach { ttt ->
                FirebaseDatabase.getInstance().getReference(ttt.first)
                        .addValueEventListener(object : ValueEventListener {
                            override fun onCancelled(p0: DatabaseError) {
                            }

                            override fun onDataChange(snap: DataSnapshot) {
                                lllogI("ones $method (${ttt.first} : snap.size:${snap.childrenCount})")
                                snap.children.forEach { d ->
                                    d.getValue(ttt.second)?.let { ddd ->
                                        val key = makeRealKey(ttt.first, ddd.key)                //  경로 설정 : 슬래시를 이용하여 정확히 설정할 것.
//                                        val key = "${ttt.first}/${d.key}"                 //  경로 설정 : 슬래시를 이용하여 정확히 설정할 것.
                                        ddd.addDate = ddd.addDate + 1                       // 데이터 수정하기.
                                        list.add(Pair(key, ddd))                          // add to list
                                        lllogI("ones $method (key:$key)")
                                    }
                                }

                                gotoRealDB(method, list)      // goto Real DB
                            }
                        })
            }
        }

        /*** 나눠서 DB 들어가기 */
        private fun gotoRealDB(method: String, list: ArrayList<Pair<String, Any?>>) {
            if (list.isNotEmpty()) {
                list.chunked(500).forEach { list500 ->
                    val map = hashMapOf<String, Any?>()
                    list500.forEach { p -> map[p.first] = p.second }
                    map.real("ones $method", success = { lllogI("ones $method success  ") }, fail = { lllogI("ones $method fail ${it.message} ") })
                }
            }
        }


        /*****************************************************************************************************************************************************
         *****************************************************************************************************************************************************
         *****************************************************************************************************************************************************/


        /*** add all */
        private fun esAllModel() {
            val method = "esAllUpdatePeerInner"
//            val queryMatchAll by lazy { ESquery.matchAll(10000, 0) }

            elasticIndice.forEach { index ->
                singleIndexAddToElasticSearch(method, index, queryMatchAll)
            }

        }

        /*** Add To ES.*/
        private fun singleIndexAddToElasticSearch(method: String, index: String, queryMatchAll: String) {
            when (index) {

                EES_PEER -> ESget.getPeerList(queryMatchAll, success = { it.completeAddList(index) }, fail = {})   // uid 주의.
                EES_DEVICE -> ESget.getDeviceList(queryMatchAll, success = { it.completeAddList(index) }, fail = {})

                EES_BOARD -> ESget.getBoardList(queryMatchAll, success = { it.completeAddList(index) }, fail = {})
                EES_POST -> ESget.getPostList(queryMatchAll, success = { it.completeAddList(index) }, fail = {})
                EES_FLEA -> ESget.getFleaList(queryMatchAll, success = { it.completeAddList(index) }, fail = {})
                EES_ROOM -> ESget.getRoomList(queryMatchAll, success = { it.completeAddList(index) }, fail = {})
                EES_STORE -> ESget.getStoreList(queryMatchAll, success = { it.completeAddList(index) }, fail = {})
                EES_STORE_MENU -> ESget.getStoreMenuList(queryMatchAll, success = { it.completeAddList(index) }, fail = {})
                //                    EES_BOARD_ALLOW -> ESget.getBoardAllowList(queryMatchAll, success = {  it.tttttteee123d(index)}, fail = {})

                CountType.BOOK.getTopChild(DataType.BOARD) -> ESget.getCountList(method, index, queryMatchAll, success = { it.completeAddList(index) }, fail = {})
                CountType.PUSH.getTopChild(DataType.BOARD) -> ESget.getCountList(method, index, queryMatchAll, success = { it.completeAddList(index) }, fail = {})

                CountType.VIEW_UID.getTopChild(DataType.POST) -> ESget.getCountList(method, index, queryMatchAll, success = { it.completeAddList(index) }, fail = {})
                CountType.UP.getTopChild(DataType.POST) -> ESget.getCountList(method, index, queryMatchAll, success = { it.completeAddList(index) }, fail = {})
                CountType.DOWN.getTopChild(DataType.POST) -> ESget.getCountList(method, index, queryMatchAll, success = { it.completeAddList(index) }, fail = {})
                CountType.BOOK.getTopChild(DataType.POST) -> ESget.getCountList(method, index, queryMatchAll, success = { it.completeAddList(index) }, fail = {})
                CountType.PUSH.getTopChild(DataType.POST) -> ESget.getCountList(method, index, queryMatchAll, success = { it.completeAddList(index) }, fail = {})

                CountType.VIEW_UID.getTopChild(DataType.FLEA) -> ESget.getCountList(method, index, queryMatchAll, success = { it.completeAddList(index) }, fail = {})
                CountType.BOOK.getTopChild(DataType.FLEA) -> ESget.getCountList(method, index, queryMatchAll, success = { it.completeAddList(index) }, fail = {})
                CountType.PUSH.getTopChild(DataType.FLEA) -> ESget.getCountList(method, index, queryMatchAll, success = { it.completeAddList(index) }, fail = {})

                CountType.VIEW_UID.getTopChild(DataType.ROOM) -> ESget.getCountList(method, index, queryMatchAll, success = { it.completeAddList(index) }, fail = {})
                CountType.BOOK.getTopChild(DataType.ROOM) -> ESget.getCountList(method, index, queryMatchAll, success = { it.completeAddList(index) }, fail = {})
                CountType.PUSH.getTopChild(DataType.ROOM) -> ESget.getCountList(method, index, queryMatchAll, success = { it.completeAddList(index) }, fail = {})

                CountType.BOOK.getTopChild(DataType.STORE) -> ESget.getCountList(method, index, queryMatchAll, success = { it.completeAddList(index) }, fail = {})
                CountType.PUSH.getTopChild(DataType.STORE) -> ESget.getCountList(method, index, queryMatchAll, success = { it.completeAddList(index) }, fail = {})

                EES_QUICK -> ESget.getCountList(method, index, queryMatchAll, success = { it.completeAddList(index) }, fail = {})

                CountType.VIEW.getTopChild(DataType.POST) -> ESget.getCountList(method, index, queryMatchAll, success = { it.completeAddList(index) }, fail = {})
                CountType.VIEW.getTopChild(DataType.FLEA) -> ESget.getCountList(method, index, queryMatchAll, success = { it.completeAddList(index) }, fail = {})
                CountType.VIEW.getTopChild(DataType.ROOM) -> ESget.getCountList(method, index, queryMatchAll, success = { it.completeAddList(index) }, fail = {})
                CountType.VIEW.getTopChild(DataType.STORE) -> ESget.getCountList(method, index, queryMatchAll, success = { it.completeAddList(index) }, fail = {})

                EES_REPLY -> ESget.getReplyList(queryMatchAll, success = { it.completeAddList(index) }, fail = {})

                EES_WARN -> ESget.getWarnList(index, queryMatchAll, success = { it.completeAddList(index) }, fail = {})
                EES_PENALTY -> ESget.getWarnList(index, queryMatchAll, success = { it.completeAddList(index) }, fail = {})
                EES_COST -> ESget.getCostList(queryMatchAll, success = { it.completeAddList(index) }, fail = {})
                EES_ACT -> ESget.getActList(queryMatchAll, success = { it.completeAddList(index) }, fail = {})

                EES_TAG -> ESget.getMyTagList(queryMatchAll, success = { it.completeAddList(index) }, fail = {})

                EES_BID -> ESget.getBidList(method, queryMatchAll, success = { it.completeAddList(index) }, fail = {})
                EES_BANNER -> ESget.getBannerList(queryMatchAll, success = { it.completeAddList(index) }, fail = {})

                EES_BILL -> ESget.getTicketList(EES_BILL, queryMatchAll, success = { it.completeAddList(index) }, fail = {})
                EES_TICKET -> ESget.getTicketList(EES_TICKET, queryMatchAll, success = { it.completeAddList(index) }, fail = {})

            }
        }

        private val map by lazy { hashMapOf<String, ArrayList<*>?>() }    // 올릴 list 모음 <index,list>

        private fun <T> ArrayList<T>?.completeAddList(index: String) {
            map[index] = this
            lllogD("ones completeAddList size : ${this?.size}   pairList : ${map.size} ")
        }

        fun gotoAddAllMap() {
            map.forEach { (index, li) -> Handler().postDelayed({ li.addAllToEs(index) }, DELAY_TIME) }
        }


        /**
         * 한개씩 add
         */
        private fun <T> ArrayList<T>?.addAllToEs(index: String) {
            val esApi = Retro.esApi
            this?.forEachIndexed { i, data ->
                val d = data as Any
                ESget.addModel(esApi, index, d.anyKey(), d, success = { result -> lllogI("Ones ($index:$i) success:$result ") }, fail = { lllogI("Ones ($index:$i) fail ") })
            }
            lllogI("@@@@@@@@@@ Ones addAllToEs ($index) list.size:${this?.size} @@@@@@@@@@@@@@@@@@@@@@@@@")
        }


    }
}