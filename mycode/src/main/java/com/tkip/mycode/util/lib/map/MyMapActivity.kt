package com.tkip.mycode.util.lib.map

import android.Manifest.permission.ACCESS_COARSE_LOCATION
import android.Manifest.permission.ACCESS_FINE_LOCATION
import android.annotation.SuppressLint
import android.content.Intent
import android.content.pm.PackageManager
import android.os.Bundle
import android.os.Looper
import android.util.Log
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.databinding.DataBindingUtil
import com.google.android.gms.location.*
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.Marker
import com.google.android.libraries.places.api.Places
import com.google.android.libraries.places.api.net.PlacesClient
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.tkip.mycode.R
import com.tkip.mycode.databinding.ActivityMymapBinding
import com.tkip.mycode.init.PUT_STORE
import com.tkip.mycode.init.REQ_FINE_LOC
import com.tkip.mycode.model.store.Store
import com.tkip.mycode.nav.store.StoreActivity
import com.tkip.mycode.util.tools.anim.AnimUtil
import com.tkip.mycode.funs.common.OnException
import com.tkip.mycode.funs.common.OnPermission
import com.tkip.mycode.init.base.Cons
import com.tkip.mycode.util.my_view.ViewUtil

class MyMapActivity : AppCompatActivity(), OnMapReadyCallback {

    //todo : marker custom,    place로 현재위치 받아오기?, 특정위치에서 범위 한정하기.

    private lateinit var b: ActivityMymapBinding

    private var map: GoogleMap? = null

    private var fusedLocationProvierClient: FusedLocationProviderClient? = null
    private lateinit var locationRequest: LocationRequest
    private lateinit var locationCallback: LocationCallback
    private lateinit var placesClient: PlacesClient


    private var currentMarker: Marker? = null
    private var poiStore: Store? = null
    private var type: Int = TYPE_STORE
    private lateinit var mBehavior: BottomSheetBehavior<View>

//    private lateinit var mAdapter2: POIphotoAdapter
//    private val items2 = arrayListOf<Bitmap>()

    companion object {
        const val TYPE_PLACE_ID = 100       // mainStore에서 검색후 구글Place를 클릭하여 왔을때
        const val TYPE_STORE = 200         // store activity(StoreInfoFrag)에서 왔을 때.
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

//        window.addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON)  // 화면 계속 켜지게.
        b = DataBindingUtil.setContentView(this, R.layout.activity_mymap)
        b.activity = this

        type = intent.getIntExtra(Cons.PUT_TYPE, TYPE_STORE)

        initMap()
        initView()
        initBottom()


        /*
        if (store == null) {
            // db오류로 store가 null일때 store 다시 받아오기.
            intent.getStringExtra(Cons.PUT_STRING)?.let {placeId->
                OnStore.checkStoreAndAddStoreFromPOI(placesClient,placeId) {
                    poiStore = it

                    onClickPoi()
                }
            }
        } else {
        }*/

    }

    private fun startIntent() {
        when (type) {
            TYPE_PLACE_ID -> {
                intent.getStringExtra(Cons.PUT_STRING)?.let { placeId ->
                    /* OnStore.checkStoreAndAddStoreFromPOI(placesClient, placeId,
                             success = {
                                 OnMap.moveMap(map!!, it.locaP!!, map?.cameraPosition?.zoom, true)
                                 resetBottomShow(it)
                             })*/
                }
            }
            TYPE_STORE -> {
                intent.getParcelableExtra<Store>(PUT_STORE)?.let { store ->
                    Log.d(Cons.TAG, "TYPE_STORE : $store")
                    OnMap.moveMap(map!!, LatLng(store.lat!!, store.lng!!), map?.cameraPosition?.zoom, true)
                    resetBottomShow(store)
                }
            }

            else -> {
                if (OnPermission.checkLocation()) {     // permission 있으면 현재 사용자 위치 or 최종 디바이스위치로 이동.
                    startLocationUpdates()
                    fusedLocationProvierClient?.getDeviceLocation(this@MyMapActivity,
                            success = {

                            })
                } else {
                    getLocationPermission()
                }
            }
        }
    }

    private fun initMap() {
        val mapFragment = supportFragmentManager.findFragmentById(R.id.map) as SupportMapFragment
        mapFragment.getMapAsync(this)


        fusedLocationProvierClient = LocationServices.getFusedLocationProviderClient(this)

        locationRequest = LocationRequest()
                .setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY)
//                .setInterval(5000)    //5초에 한번씩
        //                .setFastestInterval(500)

        LocationSettingsRequest.Builder().addLocationRequest(locationRequest)

        locationCallback = object : LocationCallback() {
            override fun onLocationResult(locationResult: LocationResult?) {
                super.onLocationResult(locationResult)

                locationResult?.let {
                    it.locations.let { locationList ->
                        if (locationList.isNotEmpty()) {
                            val size = locationList.size
                            locationList[size - 1]?.let { location ->
                                MAP_CURRENT_LOCATION = location

                                // 마커 생성
//                                 val locaP = LatLng(mainLoc.latitude, mainLoc.longitude)
//                                 val markerTitle = OnMap.getGeocoder(this@MyMapActivity, locaP)
//                                 val markerSnippet = ("위도:" + mainLoc.latitude.toString() + " 경도:" + mainLoc.longitude.toString())
//                                 map?.addMarker(OnMap.getMarkerOpt(locaP, markerTitle, markerSnippet))
                            }
                        }
                    }
                }
            }
        }


        // Initialize Places.
//        Places.initialize(applicationContext, getString(R.string.google_maps_api_key))
        // Create a new Places client instance.
        placesClient = Places.createClient(this)
    }

    private fun initView() {

    }

    private fun initBottom() {

        mBehavior = BottomSheetBehavior.from(b.coordiBottom)

        mBehavior.state = BottomSheetBehavior.STATE_HIDDEN
        mBehavior.peekHeight = BottomSheetBehavior.PEEK_HEIGHT_AUTO
//        mBehavior?.peekHeight = 100

        mBehavior.setBottomSheetCallback(object : BottomSheetBehavior.BottomSheetCallback() {
            override fun onSlide(bottom: View, slideOffset: Float) {
            }

            override fun onStateChanged(bottom: View, newState: Int) {
                if (BottomSheetBehavior.STATE_EXPANDED == newState) {
                    Log.d(Cons.TAG, "STATE_EXPANDED : $ ")
                }
                if (BottomSheetBehavior.STATE_COLLAPSED == newState) {
                    Log.d(Cons.TAG, "STATE_COLLAPSED : $ ")
                }
                if (BottomSheetBehavior.STATE_HIDDEN == newState) {
                    Log.d(Cons.TAG, "STATE_HIDDEN : $ ")
                }
            }
        })


    }

    override fun onMapReady(googleMap: GoogleMap?) {

        if (googleMap == null) {
            Log.d(Cons.TAG, "googleMap null")
            OnException.failMapReady("onMapReady")
        } else {

            OnMap.isInitMapStyle(this@MyMapActivity, googleMap)

            map = googleMap
            map?.run {

                setPadding(15, 0, 0, 0)
                animateCamera(CameraUpdateFactory.zoomTo(15f))

                setOnMapClickListener {
                    Log.d(Cons.TAG, "setOnMapClickListener : $it")
//                    OnMap.moveMap(map!!, it, cameraPosition?.zoom)
                }

                setOnMapLongClickListener {
                    Log.d(Cons.TAG, "setOnMapLongClickListener : ${it.toString()}")

                    OnMap.moveMap(map!!, it, cameraPosition?.zoom, true)

//                    /** 주변 위치 받아오기 */
//                    OnMap.getLikelihood(placesClient,
//                            success = {
//                            }, fail = {})

                    /** 지오코딩 받고 currentMarker 꽂기 */
                    OnMap.getGeocoder(this@MyMapActivity, it) { result ->
                        val address = result.getAddressLine(0).toString()
                        replaceCurrentMarker(it, address, null)
                    }

                }

                setOnInfoWindowClickListener {
                    Log.d(Cons.TAG, "setOnInfoWindowClickListener : ${it.position.toString()}")
                }

                setOnMarkerClickListener {
                    Log.d(Cons.TAG, "setOnMarkerClickListener : ${it.position.toString()}")
                    it?.showInfoWindow()
                    return@setOnMarkerClickListener true
                }

                setOnPoiClickListener {
                    if (MAP_BOUNDARY.contains(it.latLng)) {
                        Log.d(Cons.TAG, "setOnPoiClickListener : ${it.name}, ${it.latLng}, ${it.placeId}")
                        onClickPoi(it.latLng, it.placeId)
                    }
                }

                /** 디폴트 위치 */
                OnMap.moveMap(map!!, LATLNG_MANILA_AIRPORT, ZOOM_DEFAULT, false)

                startIntent()
            }
        }


    }


    @SuppressLint("MissingPermission")
    override fun onResume() {
        super.onResume()
        if (OnPermission.checkLocation()) {
            fusedLocationProvierClient?.requestLocationUpdates(locationRequest, locationCallback, null)
            map?.isMyLocationEnabled = true
        }

    }


    override fun onStop() {
        super.onStop()
        fusedLocationProvierClient?.removeLocationUpdates(locationCallback)
    }


    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        when (requestCode) {
            REQ_FINE_LOC -> {
                if (grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED && grantResults[1] == PackageManager.PERMISSION_GRANTED) {
                    startLocationUpdates()
                } else {
                    OnPermission.informAbout_FINE_LOC_PERMISSION(this)
                }
            }
        }


    }

    /**
     * getPermission
     */
    private fun getLocationPermission() {
        ActivityCompat.requestPermissions(this, arrayOf(ACCESS_FINE_LOCATION, ACCESS_COARSE_LOCATION), REQ_FINE_LOC)
    }


    /**
     * 내 위치 받아오기.
     */
    @SuppressLint("MissingPermission")
    private fun startLocationUpdates() {
        map?.isMyLocationEnabled = true
        map?.uiSettings?.isMyLocationButtonEnabled = true
        if (OnMap.checkGPSandNETWORK(this))
            fusedLocationProvierClient?.requestLocationUpdates(locationRequest, locationCallback, Looper.myLooper())
    }


    /**
     * 바텀 플레이스 기본정보 보이기
     */
    private fun resetBottomShow(s: Store) {

        poiStore = s
        replaceCurrentMarker(LatLng(s.lat!!, s.lng!!), s.title, s.address!!)

        b.store = s
//        b.simpleRatingBar.rating = s.rating?.toFloat() ?: 3f


        /*  s.photoMetadatas?.let {
              OnMap.getPhotoBitmap(placesClient, it[0],
                      success = { bitmap ->
                          MyBinding.src_photoMetaData(b.ivPhoto, bitmap)
  //                                                items2.add(bitmap)
  //                                                mAdapter2.notifyDataSetChanged()
                      }, fail = {})
          }*/
        mBehavior.state = BottomSheetBehavior.STATE_EXPANDED

    }

    /**
     * 현재 마커 클릭지점으로 수정하기.
     */
    private fun replaceCurrentMarker(latLng: LatLng, str1: String?, str2: String?) {
        currentMarker?.remove()
        currentMarker = map?.addMarker(OnMap.getMarkerOpt(latLng, str1, str2))
        currentMarker?.showInfoWindow()
    }

    private fun onClickPoi(latLng: LatLng, placeId: String) {
        /** BottomSheet 열기 */
        if (currentMarker?.position == latLng) {   // 동일한 poi이면
            if (mBehavior.state != BottomSheetBehavior.STATE_EXPANDED) {
                mBehavior.state = BottomSheetBehavior.STATE_EXPANDED
            }
            return
        }

        OnMap.moveMap(map!!, latLng, map?.cameraPosition?.zoom, true)

        /* OnStore.checkStoreAndAddStoreFromPOI(placesClient, placeId,
                 success = { store ->
                     resetBottomShow(store)
                 })*/
    }


    fun onClickFab2(view: View) {

    }

    fun onClickOpenSearchBar(view: View) {
        Log.d(Cons.TAG, "onClickOpenSearchBar :")

        ViewUtil.addViewSearchBar(this@MyMapActivity, b.lineSearchBar,
                btnMenu = {
                    Log.d(Cons.TAG, "btnMenu :")

                },
                search = {
                    Log.d(Cons.TAG, "search : $it")

                },
                cancel = {
                    AnimUtil.showSlideLeft(b.fabLoc, true)
                })

        AnimUtil.hideSlideRight(view, true)
    }


    fun onClickOpenStore() {

        val intent = Intent(this@MyMapActivity, StoreActivity::class.java)
        intent.putExtra(PUT_STORE, poiStore)
//        intent.putExtra(Cons.PUT_PLACE,currentPOI)
        startActivity(intent)

    }

    fun onClickClose() {

        mBehavior.state = BottomSheetBehavior.STATE_HIDDEN

        Log.d(Cons.TAG, "onClickClose :")

    }


}


