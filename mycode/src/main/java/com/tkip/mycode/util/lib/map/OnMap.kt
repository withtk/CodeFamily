package com.tkip.mycode.util.lib.map

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.content.res.Resources
import android.graphics.Bitmap
import android.location.Address
import android.location.Geocoder
import android.location.Location
import android.location.LocationManager
import android.provider.Settings
import androidx.appcompat.app.AppCompatActivity
import com.google.android.gms.common.api.ApiException
import com.google.android.gms.location.FusedLocationProviderClient
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MapStyleOptions
import com.google.android.gms.maps.model.MarkerOptions
import com.google.android.libraries.places.api.model.*
import com.google.android.libraries.places.api.net.*
import com.tkip.mycode.R
import com.tkip.mycode.dialog.alert.MyAlert
import com.tkip.mycode.dialog.toast.TToast
import com.tkip.mycode.funs.common.OnException
import com.tkip.mycode.funs.common.lllogD
import com.tkip.mycode.funs.common.lllogE
import com.tkip.mycode.funs.common.lllogI
import com.tkip.mycode.init.REQ_MAP_LOCATION
import com.tkip.mycode.util.tools.date.FORMAT_TIME_ONLY
import com.tkip.mycode.util.tools.date.getDateTimeOnly
import com.tkip.mycode.util.tools.date.getLocalTimeString
import java.io.IOException
import java.util.*


class OnMap {

    companion object {

        /**
         *  모델의 lat, lng로 latlng 만들기.
         */
        fun createLatlng(lat:Double?,lng:Double?) : LatLng?{
            lat?.let {
                lng?.let {
                    return LatLng(lat,lng)
                }
            }
            return  null
        }

        /**
         *  지도열기.
         */
        fun gotoMainMap(activity: Activity,actType:Int, latlng: LatLng?) {
            val intent = Intent(activity, MainMapActivity::class.java)
            intent.putExtra(PUT_MAP_ACT, actType)
            intent.putExtra(PUT_MAP_LATLNG, latlng)

            activity.startActivityForResult(intent, REQ_MAP_LOCATION)
        }

        /**
         * 단말기의 GPS와 Network ON/OFF 체크
         */
        fun checkGPSandNETWORK(activity: Activity): Boolean {
            val locationManager = activity.getSystemService(Context.LOCATION_SERVICE) as LocationManager
            val result = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER) || locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER)
            if (result) {

            } else {
                val title = activity.getString(com.tkip.mycode.R.string.title_off_gps_or_network)
                val desc = activity.getString(com.tkip.mycode.R.string.desc_off_gps_or_network)
                MyAlert.showConfirm(activity, title, desc, null, isCancel = true, isCancelTouch = true,
                        ok = {
                            val intent = Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS)
                            (activity as AppCompatActivity).startActivity(intent)
                        }, cancel = {})
            }
            return result
        }


        private const val MORNING = "06:00"
        private const val EVENING = "20:00"
        /**
         *  지도 스타일 야간모드 적용 여부를 위해.
         *  낮 = true
         */
        private fun isDayLight(): Boolean {

            val nowStr = System.currentTimeMillis().getLocalTimeString(FORMAT_TIME_ONLY)
            val nowDate = nowStr.getDateTimeOnly()
            val morning = MORNING.getDateTimeOnly()
            val evening = EVENING.getDateTimeOnly()
//            lllogD("nowStr : $nowStr  ")
//            lllogD("nowDate : $nowDate  ")
//            lllogD("morning : $morning  ")
//            lllogD("evening : $evening  ")
//            lllogD("morning re : ${morning.before(nowDate)}  ")
//            lllogD("evening re : ${evening.after(nowDate)}  ")

            return morning.before(nowDate) && evening.after(nowDate)
        }

        /**
         *  지도 스타일 적용완료시 true
         */
        fun isInitMapStyle(context: Context, googleMap: GoogleMap) {
            var rawId = R.raw.style_json_dark  //야간
            if (isDayLight()) rawId = R.raw.style_json   // 주간이면 주간스타일로.
            val mapStyleOptions = MapStyleOptions.loadRawResourceStyle(context, rawId)

            try {
                if (!googleMap.setMapStyle(mapStyleOptions))
                    OnException.failMapStyle("isInitMapStyle : Style parsing failed.")
            } catch (e: Resources.NotFoundException) {
                lllogD("Can't find style. Error: ", e)
            }
        }


        /**
         * 지도이동
         */
        fun moveMap(map: GoogleMap, latlng: LatLng, zoom: Float?, animation: Boolean) {
            val cameraUpdate = CameraUpdateFactory.newLatLngZoom(latlng, zoom ?: ZOOM_DEFAULT)
            if (animation) map.animateCamera(cameraUpdate)
            else map.moveCamera(cameraUpdate)
        }

        /**
         * 지도 현지 내 위치로 이동
         */
        fun moveCurrentLocation(map: GoogleMap) {
            var latlng = LATLNG_MANILA_AIRPORT
            MAP_CURRENT_LOCATION?.let {
                latlng = LatLng(it.latitude, it.longitude)
            }
            moveMap(map, latlng, map.cameraPosition.zoom, false)
        }


        /**
         * Device의 마지막 위치 받아오기.
         */
        fun getDeviceLocation(activity: Activity, fusedLocationProvierClient: FusedLocationProviderClient, success: (Location) -> Unit) {

            try {
                fusedLocationProvierClient.lastLocation?.addOnSuccessListener(activity) { deviceLoc ->
                    if (deviceLoc != null) {
                        MAP_CURRENT_LOCATION = deviceLoc
                        success(deviceLoc)
//                        moveMap(map, LatLng(deviceLoc.latitude, deviceLoc.longitude), zoomDefault,false)
                    }
                }
            } catch (e: SecurityException) {
                lllogD(e.message)
            }
        }


        /**
         * 마커옵션 만들기
         */
        fun getMarkerOpt(latLng: LatLng, title: String?, snippets: String?): MarkerOptions {

            return MarkerOptions().apply {
                position(latLng)
                title(title)
                snippet(snippets)
                alpha(0.5f)
                draggable(true)
                icon(null)
//                icon(BitmapDescriptorFactory.fromBitmap(R.drawable.ic_whatshot))
//                icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_RED))
            }

        }


        /**
         * 플레이스: Autocomplete 순간검색 결과 가져오기
         */
        fun getPrediction(placesClient: PlacesClient, query: String, success: (FindAutocompletePredictionsResponse) -> Unit, fail: (Exception) -> Unit) {

            val token = AutocompleteSessionToken.newInstance()

            val bounds = RectangularBounds.newInstance(
                    LatLng(-33.880490, 151.184363),
                    LatLng(-33.858754, 151.229596))

            val request = FindAutocompletePredictionsRequest.builder()
                    // Call either setLocationBias() OR setLocationRestriction().
//                .setLocationBias(bounds)
//                .setLocationRestriction(bounds)

                    .setCountry(MAP_COUNTRY)
//                    .setTypeFilter(TypeFilter.CITIES)
//                .setTypeFilter(TypeFilter.ADDRESS)
                    .setSessionToken(token)
                    .setQuery(query)
                    .build()


            placesClient.findAutocompletePredictions(request)
                    .addOnSuccessListener { response ->
                        success(response)

                        // log출력
                        for (prediction in response.autocompletePredictions) {
                            lllogI("prediction.getFullText : ${prediction.getFullText(null)}")
                            lllogI("prediction.getPrimaryText : ${prediction.getPrimaryText(null)}")
                            lllogI("prediction.getSecondaryText : ${prediction.getSecondaryText(null)}")
                            for ((i, type) in prediction.placeTypes.withIndex()) {
                                lllogI("placeTypes$i : " + type.toString())
                            }
                        }

                    }
                    .addOnFailureListener { e ->
                        fail(e)
                        OnException.failMapData("getPrediction")
                        if (e is ApiException) {
                            lllogE("Place not found: " + e.statusCode)
                        }
                    }

        }


        /**
         * 플레이스: Autocomplete
         * prediction.placeId로 Place  가져오기
         */
        fun getPlace(placesClient: PlacesClient, placeId: String, success: (Place) -> Unit, fail: (Exception) -> Unit) {

            val fields = (Arrays.asList(
                    Place.Field.ADDRESS,
                    Place.Field.ADDRESS_COMPONENTS,
                    Place.Field.ID,
                    Place.Field.LAT_LNG,
                    Place.Field.NAME,
                    Place.Field.OPENING_HOURS,
                    Place.Field.PHONE_NUMBER,
                    Place.Field.PHOTO_METADATAS,

                    Place.Field.PLUS_CODE,
                    Place.Field.PRICE_LEVEL,
                    Place.Field.RATING,
                    Place.Field.USER_RATINGS_TOTAL,
                    Place.Field.WEBSITE_URI,

                    Place.Field.TYPES,
                    Place.Field.VIEWPORT))

            val request = FetchPlaceRequest.builder(placeId, fields).build()

            placesClient.fetchPlace(request)
                    .addOnSuccessListener { it ->

                        success(it.place)

                        lllogI("========== place ========== : ")

                        lllogI("id : " + it.place.id.toString())
                        lllogI("latLng : " + it.place.latLng.toString())
                        lllogI("name : " + it.place.name.toString())
                        lllogI("address : " + it.place.address)
                        lllogI("addressComponents : " + it.place.addressComponents)
                        val addressComponents = it.place.addressComponents?.asList()
                        addressComponents?.let {
                            val list = addressComponents as MutableCollection<AddressComponent>

                            for (component in list) {
                                lllogI("addressComponent - name : ${component.name}, shortName : ${component.shortName} ")

                                val typeList = component.types as MutableCollection<String>
                                for (type in typeList) {
                                    lllogI("      no : $type")
                                    if (type == Place.Type.POSTAL_CODE.name.toLowerCase()) {
                                        lllogI("address postal_code: ${component.name}")
                                    }
                                }
                            }
                        }

                        lllogI("types==== : " + it.place.types.toString())
                        it.place.types?.let { types ->
                            for (type in types) {
                                lllogI("no : " + type.name)
                            }
                        }

                        lllogI("plusCode.globalCode : " + it.place.plusCode?.globalCode.toString())
                        lllogI("viewport.northeast : " + it.place.viewport?.northeast.toString())
                        lllogI("viewport.southwest : " + it.place.viewport?.southwest.toString())


                        lllogI("priceLevel : " + it.place.priceLevel.toString())
                        lllogI("rating : " + it.place.rating.toString())
                        lllogI("userRatingsTotal : " + it.place.userRatingsTotal.toString())
                        lllogI("websiteUri : " + it.place.websiteUri.toString())
                        lllogI("phoneNumber : " + it.place.phoneNumber.toString())

                        lllogI("openingHours===== : " + it.place.openingHours.toString())
                        val openingHours = it.place.openingHours
                        openingHours?.periods?.let {
                            for (period in it) {
                                lllogI("open : ${period.open?.day?.name} ${period.open?.time?.hours} : ${period.open?.time?.minutes}   ")
                                lllogI("close : ${period.close?.day?.name} ${period.close?.time?.hours} : ${period.close?.time?.minutes}   ")
                            }
                        }
                        openingHours?.weekdayText?.let { weekDays ->
                            for (str in weekDays) {
                                lllogI("weekdayText : $str")
                            }
                        }

                        it.place.photoMetadatas?.let {
                            for (photoMetaData in it) {
                                lllogI("attributions : " + photoMetaData.attributions)
                                lllogI("width : " + photoMetaData.width.toString())
                                lllogI("height : " + photoMetaData.height.toString())
                                lllogI("zza() : " + photoMetaData.zza())
                            }
                        }
                        lllogI("=============== : =============== : ")


                    }
                    .addOnFailureListener { e ->
                        fail(e)
                        OnException.failMapData("getPlace")
                        if (e is ApiException) {
                            lllogE("getPlace Place not found: " + e.statusCode)
                        }

                    }
        }

        /**
         * 플레이스: Autocomplete
         * prediction.placeId로 Place latlng만 가져오기
         */
        fun getPlaceLatlng(placesClient: PlacesClient, placeId: String, success: (FetchPlaceResponse) -> Unit) {

            val fields = (Arrays.asList(
                    Place.Field.ADDRESS,
                    Place.Field.LAT_LNG))

            val request = FetchPlaceRequest.builder(placeId, fields).build()

            placesClient.fetchPlace(request)
                    .addOnSuccessListener { it ->
                        success(it)
                    }

        }


        /**
         * 플레이스: 사용자의 현재 장소가 될 가능성이 높은 주변장소 가져오기
         */
        /* @SuppressLint("MissingPermission")
         fun getLikelihood(placesClient: PlacesClient, success: (List<PlaceLikelihood>) -> Unit, fail: (Exception) -> Unit) {

             val placeFields = Arrays.asList(
                     Place.Field.ADDRESS,
                     Place.Field.ADDRESS_COMPONENTS,
                     Place.Field.ID,
                     Place.Field.LAT_LNG,
                     Place.Field.NAME,
                     Place.Field.PHOTO_METADATAS,
                     Place.Field.PLUS_CODE,
                     Place.Field.PRICE_LEVEL,
                     Place.Field.RATING,
                     Place.Field.TYPES,
                     Place.Field.USER_RATINGS_TOTAL,
                     Place.Field.VIEWPORT)
             val request = FindCurrentPlaceRequest.builder(placeFields).build()

             placesClient.findCurrentPlace(request)
                     .addOnSuccessListener { response ->

                         success(response.placeLikelihoods)

                         val buffer = StringBuffer()

                         for ((i, likelihood) in response.placeLikelihoods.withIndex()) {

                             buffer.append("no : ${i} ==========================================\n")
                             buffer.append("name : ${likelihood.place.name} \n")
                             buffer.append("id : ${likelihood.place.id} \n")
                             buffer.append("address : ${likelihood.place.address} \n")
                             buffer.append("addressComponents :--------------\n")

                             val addressComponents = likelihood.place.addressComponents?.asList()
                             addressComponents?.let {

                                 for (component in addressComponents as MutableCollection<AddressComponent>) {
                                     buffer.append("component name: ${component.name} ----!!!! \n")
                                     buffer.append("component shortName: ${component.shortName} \n")

                                     val typeList = component.types as MutableCollection<String>
                                     for (type in typeList) {
                                         buffer.append("component no: $type \n")
                                     }
                                 }
                             }

                             buffer.append("latLng : ${likelihood.place.latLng} \n")
                             buffer.append("photoMetadatas : ${likelihood.place.photoMetadatas} \n")
                             buffer.append("plusCode : ${likelihood.place.plusCode} \n")
                             buffer.append("priceLevel : ${likelihood.place.priceLevel} \n")
                             buffer.append("rating : ${likelihood.place.rating} \n")
                             buffer.append("types : ${likelihood.place.types} \n")
                             buffer.append("userRatingsTotal : ${likelihood.place.userRatingsTotal} \n")
                             buffer.append("viewport : ${likelihood.place.viewport} \n")
                             buffer.append("likelihood : ${likelihood.likelihood} \n")
                             buffer.append(" ===== ===== ===== ===== =====\n")

                         }

                         lllogI(buffer.toString())

                     }
                     .addOnFailureListener { e ->
                         fail(e)
                         OnException.failMapLikelyHood("getLikelihood")
                         if (e is ApiException) {
                             lllogE("Place not found: " + e.statusCode)
                         }
                     }

         }*/


        /**
         *   플레이스: Foto.Bitmap 가져오기
         */
        fun getPhotoBitmap(placesClient: PlacesClient, photoMetadata: PhotoMetadata, success: (Bitmap) -> Unit, fail: (Exception) -> Unit) {

            val photoRequest = FetchPhotoRequest.builder(photoMetadata)
                    .setMaxWidth(500) // Optional.
                    .setMaxHeight(300) // Optional.
                    .build()

            placesClient.fetchPhoto(photoRequest)
                    .addOnSuccessListener { fetchPhotoResponse ->
                        val bitmap = fetchPhotoResponse.bitmap
                        success(bitmap)
//                        imageView.setImageBitmap(bitmap)
                    }
                    .addOnFailureListener { e ->
                        fail(e)
                        OnException.failMapFetchPhoto("getPhotoBitmap")
                        if (e is ApiException) {
                            lllogE("getPhotoBitmap Place not found: " + e.statusCode)
                        }
                    }
        }


        /**
         * 주소:값 받아오기 geocoder
         */
        fun getGeocoder(context: Context, latlng: LatLng, result: (Address) -> Unit) {

            val geocoder = Geocoder(context, Locale.getDefault())
            val addresses: List<Address>?

            try {
                addresses = geocoder.getFromLocation(
                        latlng.latitude,
                        latlng.longitude,
                        1)

                if (addresses == null || addresses.isEmpty()) {
                    val strADDRESS = context.getString(com.tkip.mycode.R.string.error_address)
                    TToast.showWarning(strADDRESS)
                    lllogD(strADDRESS)

                } else {
                    val address = addresses[0]

                    lllogD("============== start address ")
                    lllogD("maxAddressLineIndex : " + address.maxAddressLineIndex.toString())
                    lllogD("getAddressLine 0 : " + address.getAddressLine(0).toString())
                    lllogD("featureName : " + address.featureName)
                    lllogD("latitude : " + address.latitude)
                    lllogD("longitude : " + address.longitude)
                    lllogD("adminArea : " + address.adminArea)
                    lllogD("countryCode : " + address.countryCode)
                    lllogD("countryName : " + address.countryName)
                    lllogD("extras : " + address.extras)
                    lllogD("locale : " + address.locale)
                    lllogD("locality : " + address.locality)
                    lllogD("phone : " + address.phone)
                    lllogD("postalCode : " + address.postalCode)
                    lllogD("premises : " + address.premises)
                    lllogD("subAdminArea : " + address.subAdminArea)
                    lllogD("subLocality : " + address.subLocality)
                    lllogD("subThoroughfare : " + address.subThoroughfare)
                    lllogD("thoroughfare : " + address.thoroughfare)
                    lllogD("url : " + address.url)
                    lllogD("==============end address")

                    result(address)
                }

            } catch (ioException: IOException) {
                val strNet = context.getString(com.tkip.mycode.R.string.error_network_map)
                TToast.showWarning(strNet)
                lllogD(strNet)

            } catch (illegalArgumentException: IllegalArgumentException) {
                val strGPS = context.getString(com.tkip.mycode.R.string.error_gps_map)
                TToast.showWarning(strGPS)
                lllogD(strGPS)
            }

        }


    }

}