package com.tkip.mycode.util.lib.retrofit2

import com.cunoraz.tagview.TagView
import com.tkip.mycode.R
import com.tkip.mycode.init.*
import com.tkip.mycode.init.base.getInt
import com.tkip.mycode.model.board.BoardType
import com.tkip.mycode.model.my_enum.CostType
import com.tkip.mycode.model.my_enum.Status
import com.tkip.mycode.model.mytag.TagTYPE
import com.tkip.mycode.model.peer.myuid
import com.tkip.mycode.util.tools.date.getLongDayBefore
import com.tkip.mycode.util.tools.date.rightNow


class ESquery {

    companion object {


        /*********************************  common  *******************************/

        /*private const val SIZE = "size"
        private const val FROM = "from"
        private const val SORT = "sort"
        private const val COMMA = ","
        private const val DESC = "desc"
        private const val ASC = "asc"
        private fun size(size: Int) = "\"size\": $size,"
        private fun from(size: Int) = "\"from\": $size,"
        private fun order(value: String) = ":{\"order\":\"$value\"}"
        private fun range(gte: Int): String {
            return "{\"range\":{\"statusNo\":{\"gte\":10,\"lt\":5000}}}"
        }

        private fun sort(arr: Array<Pair<String, String>>): String {
            val default = "\"sort\": [{"
            var result = ""
            arr.forEach {
                result = default + "\"${it.first}\"" + order(it.second)
            }
            "}]"
            return "{\"range\":{\"statusNo\":{\"gte\":10,\"lt\":5000}}}"
        }
*/

        /*********************************  sttCommon  *******************************/
        /** * 최근 인기 포스트 날짜 기간 */
        var DAYS_RECENT_POST = R.integer.days_recent_post.getInt()

        private val MAX by lazy { 10000 }
        private val MIN_SIZE by lazy { 10 }
        const val CH_DESC = "desc"
        const val CH_ASC = "asc"


        private fun size(size: Int) = "\"size\": $size,"
        private fun from(from: Int) = "\"from\": $from,"
        private fun range(child: String, value: Any) = "{\"range\":{\"$child\":{\"gte\":$value}}}"
        private fun matchUID(uid: String) = "{\"match\":{\"uid\":\"$uid\"}}"
        private fun match(key: String, value: Any) = "{\"match\":{\"$key\":\"$value\"}}"
        private fun sort(child: String, align: String) = "{\"$child\":{\"order\":\"$align\"}}"
        private fun sortAddDateDesc() = sort(CH_ADD_DATE, CH_DESC)
        private fun sortAddDateAsc() = sort(CH_ADD_DATE, CH_ASC)
        private fun sortSelect(sel: Boolean) = if (sel) CH_ASC else CH_DESC

        //  "name", "description"
        fun getMultiField(arr: Array<String>) =
                StringBuffer().apply {
                    arr.forEach {
                        if (!this.isBlank()) this.append(",")
                        this.append("\"$it\"")
                    }
                }.toString().trim()

        fun matchUID(size: Int, from: Int, authorUID: String, status: Status) = "{\"size\":$size,\"from\":$from,\"query\":{\"bool\":{\"must\":[{\"match\":{\"authorUID\":\"$authorUID\"}}],\"filter\":[{\"range\":{\"statusNo\":{\"gte\":${status.no}}}}]}},\"sort\":[{\"addDate\":{\"order\":\"desc\"}}]}"

        fun matchUIDwithKeyword(size: Int, from: Int, authorUID: String, childArr: Array<String>, keyword: String, status: Status) = "{\"size\":$size,\"from\":$from,\"query\":{\"bool\":{\"must\":[{\"match\":{\"authorUID\":\"$authorUID\"}},{\"query_string\":{\"fields\":[${getMultiField(childArr)}],\"query\":\"*$keyword*\"}}],\"filter\":[{\"range\":{\"statusNo\":{\"gte\":${status.no}}}}]}},\"sort\":[{\"addDate\":{\"order\":\"desc\"}}]}"
        fun matchKeyword(size: Int, from: Int, childArr: Array<String>, keyword: String, status: Status) = "{\"size\":$size,\"from\":$from,\"query\":{\"bool\":{\"must\":[{\"query_string\":{\"fields\":[${getMultiField(childArr)}],\"query\":\"*$keyword*\"}}],\"filter\":[{\"range\":{\"statusNo\":{\"gte\":${status.no}}}}]}},\"sort\":[{\"addDate\":{\"order\":\"desc\"}}]}"
        fun matchAll(size: Int?, from: Int?) = "{\"size\":${size ?: MAX},\"from\":${from ?: 0},\"query\":{\"match_all\":{}},\"sort\":[{\"addDate\":{\"order\":\"desc\"}}]}"

        /*** multi match **/
        fun searchMultiMatch(size: Int, from: Int, childArr: Array<String>, wordList: ArrayList<String>, status: Status, totalPoint: Boolean, addDate: Boolean) = "{\"size\":$size,\"from\":$from,\"query\":{\"bool\":{\"filter\":[${getMultiMatch(childArr, wordList)}{\"range\":{\"statusNo\":{\"gte\":\"${status.no}\"}}}]}},\"sort\":[{\"totalPoint\":{\"order\":\"${sortSelect(totalPoint)}\"}},{\"addDate\":{\"order\":\"${sortSelect(addDate)}\"}}]}"
        fun searchMultiMatch4Post(size: Int, from: Int, childArr: Array<String>, wordList: ArrayList<String>, status: Status, totalPoint: Boolean, addDate: Boolean) = "{\"size\":$size,\"from\":$from,\"query\":{\"bool\":{\"filter\":[{\"match\":{\"allowSearch\":true}},${getMultiMatch(childArr, wordList)}{\"range\":{\"statusNo\":{\"gte\":\"${status.no}\"}}}]}},\"sort\":[{\"totalPoint\":{\"order\":\"${sortSelect(totalPoint)}\"}},{\"addDate\":{\"order\":\"${sortSelect(addDate)}\"}}]}"
        fun searchMultiMatch4Board(size: Int, from: Int, allowSearch: Boolean, childArr: Array<String>, wordList: ArrayList<String>, status: Status) = "{\"size\":$size,\"from\":$from,\"query\":{\"bool\":{\"filter\":[{\"match\":{\"allowSearch\":$allowSearch}},${getMultiMatch(childArr, wordList)}{\"range\":{\"statusNo\":{\"gte\":\"${status.no}\"}}}]}},\"sort\":[{\"totalPoint\":{\"order\":\"desc\"}},{\"addDate\":{\"order\":\"asc\"}}]}"
        private fun multiMatch(str: String, childArr: Array<String>) = "{\"multi_match\":{\"query\":\"$str\",\"fields\":[${getMultiField(childArr)}]}},"
        private fun getMultiMatch(childArr: Array<String>, wordList: ArrayList<String>) = StringBuffer().apply {
            wordList.forEach {
                this.append(multiMatch(it, childArr))
            }
        }


        fun searchAdCredit(size: Int, from: Int, childArr: Array<String>, wordList: ArrayList<String>, status: Status, addDate: Boolean) = "{\"size\":$size,\"from\":$from,\"query\":{\"bool\":{\"filter\":[${getMultiMatch(childArr, wordList)}{\"range\":{\"statusNo\":{\"gte\":\"${status.no}\"}}}]}},\"sort\":[{\"adCredit\":{\"order\":\"desc\"}},{\"totalAdCredit\":{\"order\":\"desc\"}},{\"addDate\":{\"order\":\"${sortSelect(addDate)}\"}}]}"


        /*** query string **/
        fun searchQueryString(size: Int, from: Int, childArr: Array<String>, wordList: ArrayList<String>, status: Status, totalPoint: Boolean, addDate: Boolean) = "{\"size\":$size,\"from\":$from,\"query\":{\"bool\":{\"filter\":[${queryString(childArr, wordList)},{\"range\":{\"statusNo\":{\"gte\":\"${status.no}\"}}}]}},\"sort\":[{\"totalPoint\":{\"order\":\"${sortSelect(totalPoint)}\"}},{\"addDate\":{\"order\":\"${sortSelect(addDate)}\"}}]}"
        private fun queryString(childArr: Array<String>, wordList: ArrayList<String>) = "{\"query_string\":{\"fields\":[${getMultiField(childArr)}],\"query\":\"*${wordList.toKeywords()}*\"}}"
        private fun ArrayList<String>.toKeywords() = StringBuffer().apply { this@toKeywords.forEach { this.append("$it ") } }.toString().trim()


        /*********************************  sttBoard  *******************************/
        // best, lastUpdated
        fun boardList(size: Int, boardTypeNo: Int, child: String, status: Status) = "{\"size\":$size,\"query\":{\"bool\":{\"must\":[{\"match\":{\"boardTypeNo\":$boardTypeNo}}],\"filter\":[{\"range\":{\"statusNo\":{\"gte\":\"${status.no}\"}}}]}},\"sort\":[{\"$child\":{\"order\":\"desc\"}}]}"

        /*** 새로운 게시판 */
        fun boardListNews(size: Int, allowSearch: Boolean) = "{\"size\":$size,\"query\":{\"bool\":{\"must\":[{\"match\":{\"allowSearch\":$allowSearch}}],\"filter\":[{\"range\":{\"statusNo\":{\"gte\":${Status.NORMAL.no}}}},{\"range\":{\"boardTypeNo\":{\"gte\":${BoardType.USER.no}}}}]}},\"sort\":[{\"addDate\":{\"order\":\"desc\"}}]}"
        fun boardPopular(size: Int) = "{\"size\":$size,\"query\":{\"bool\":{\"must\":[{\"match\":{\"statusNo\":${Status.NORMAL.no}}},{\"match\":{\"allowSearch\":true}}],\"filter\":[{\"range\":{\"boardTypeNo\":{\"gte\":${BoardType.USER.no}}}}]}},\"sort\":[{\"totalPoint\":{\"order\":\"desc\"}}]}"
        fun isBoardIDcheck(boardId: String) = "{\"query\":{\"bool\":{\"filter\":[{\"match\":{\"boardId\":\"$boardId\"}}]}}}"
        fun boardMatchUID(size: Int, from: Int, uid: String, childArr: Array<String>, keyword: String, status: Status) = "{\"size\":$size,\"from\":$from,\"query\":{\"bool\":{\"must\":[{\"match\":{\"authorUID\":\"$uid\"}},{\"query_string\":{\"fields\":[${getMultiField(childArr)}],\"query\":\"*$keyword*\"}}],\"filter\":[{\"range\":{\"statusNo\":{\"gte\":\"${status.no}\"}}}]}},\"sort\":[{\"addDate\":{\"order\":\"desc\"}}]}"
        fun boardMatchKeyword(size: Int, from: Int, childArr: Array<String>, keyword: String, status: Status) = "{\"size\":$size,\"from\":$from,\"query\":{\"bool\":{\"must\":[{\"query_string\":{\"fields\":[${getMultiField(childArr)}],\"query\":\"*$keyword*\"}}],\"filter\":[{\"range\":{\"statusNo\":{\"gte\":\"${status.no}\"}}}]}},\"sort\":[{\"addDate\":{\"order\":\"desc\"}}]}"
        fun boardMatchKeywordTotalPoint(size: Int, from: Int, childArr: Array<String>, keyword: String, status: Status) = "{\"size\":$size,\"from\":$from,\"query\":{\"bool\":{\"must\":[{\"match\":{\"allowSearch\":true}},{\"query_string\":{\"fields\":[${getMultiField(childArr)}],\"query\":\"*$keyword*\"}}],\"filter\":[${range(CH_STATUS_NO, status.no)}]}},\"sort\":[${sort(CH_TOTAL_POINT, CH_DESC)},${sortAddDateAsc()}]}"
        fun boardMatchAll(size: Int, status: Status) = "{\"size\":$size,\"query\":{\"bool\":{\"must\":[{\"range\":{\"statusNo\":{\"gte\":${status.no}}}}]}},\"sort\":[${sortAddDateDesc()}]}"

        /*********************************  sttPost  *******************************/
        fun postList(size: Int, from: Int, boardKey: String, status: Status) = "{\"size\":$size,\"from\":$from,\"query\":{\"bool\":{\"filter\":[{\"match\":{\"boardKey\":\"$boardKey\"}},{\"range\":{\"statusNo\":{\"gte\":${status.no}}}}]}},\"sort\":[{\"addDate\":{\"order\":\"desc\"}}]}"

        fun postRecentList(size: Int, from: Int) = "{\"size\":$size,\"from\":$from,\"query\":{\"bool\":{\"filter\":[{\"bool\":{\"must\":[{\"range\":{\"addDate\":{\"gte\":${getLongDayBefore(DAYS_RECENT_POST)},\"lte\":${rightNow()}}}},{\"range\":{\"statusNo\":{\"gte\":${Status.NORMAL.no}}}}]}}]}},\"sort\":[{\"totalPoint\":\"desc\"}]}"
        fun postListCsCenter(size: Int, from: Int, boardKey: String, boardMasterUid: String, status: Status) = "{\"size\":$size,\"from\":$from,\"query\":{\"bool\":{\"must\":[{\"match\":{\"boardKey\":\"$boardKey\"}},{\"terms\":{\"authorUID\":[\"$boardMasterUid\",\"${myuid()}\"]}},{\"range\":{\"statusNo\":{\"gte\":${status.no}}}}]}},\"sort\":[{\"addDate\":{\"order\":\"desc\"}}]}"

        /*********************************  sttFlea  *******************************/
        fun fleaList(size: Int, from: Int, status: Status) = "{\"size\":$size,\"from\":$from,\"query\":{\"bool\":{\"filter\":[{\"range\":{\"statusNo\":{\"gte\":${status.no}}}}]}},\"sort\":[{\"addDate\":{\"order\":\"desc\"}}]}"

        /*********************************  sttRoom  *******************************/
        fun roomList(size: Int, from: Int, status: Status) = "{\"size\":$size,\"from\":$from,\"query\":{\"bool\":{\"filter\":[{\"range\":{\"statusNo\":{\"gte\":${status.no}}}}]}},\"sort\":[{\"addDate\":{\"order\":\"desc\"}}]}"

        /*********************************  sttStore  *******************************/
        fun storeList(size: Int, from: Int, status: Status) = "{\"size\":$size,\"from\":$from,\"query\":{\"bool\":{\"filter\":[{\"range\":{\"statusNo\":{\"gte\":${status.no}}}}]}},\"sort\":[{\"addDate\":{\"order\":\"desc\"}}]}"

        //        fun storeSearchTest()="{\"size\":10,\"from\":0,\"query\":{\"bool\":{\"must\":[{\"term\":{\"tagList\":\"마닐라\"}},{\"term\":{\"tagList\":\"당구\"}},{\"query_string\":{\"fields\":[\"name\"],\"query\":\"*우*\"}}],\"filter\":[{\"range\":{\"statusNo\":{\"gte\":1000}}}]}},\"sort\":[{\"adCredit\":{\"order\":\"desc\"}},{\"totalAdCredit\":{\"order\":\"desc\"}}]}"
        fun storeSearch(size: Int, from: Int, must: String, status: Status) = "{\"size\":$size,\"from\":$from,\"query\":{\"bool\":{\"must\":[$must],\"filter\":[{\"range\":{\"statusNo\":{\"gte\":${status.no}}}}]}},\"sort\":[{\"adCredit\":{\"order\":\"desc\"}},{\"totalAdCredit\":{\"order\":\"desc\"}}]}"

        fun getTermTaglist(tagView: TagView, keyword: String?) = StringBuffer().apply {
            tagView.tags.forEachIndexed { _, t ->
                if (!this.isBlank()) this.append(",")
                this.append(makeTerm(t.text))
            }
            keyword?.let {
                if (!this.isBlank()) this.append(",")
                this.append(getQueryString(CH_TITLE, keyword))
            }
        }.toString()

        fun makeTerm(tag: String) = "{\"term\":{\"tagList\":\"$tag\"}}"
        fun getQueryString(child: String, keyword: String) = "{\"query_string\":{\"fields\":[\"$child\"],\"query\":\"*$keyword*\"}}"
        fun storeMenuList(size: Int, from: Int, storeKey: String, status: Status) = "{\"size\":$size,\"from\":$from,\"query\":{\"bool\":{\"must\":[{\"match\":{\"storeKey\":\"$storeKey\"}}],\"filter\":[{\"range\":{\"statusNo\":{\"gte\":${status.no}}}}]}},\"sort\":[{\"order\":{\"order\":\"asc\"}}]}"

        /*********************************  sttCount  *******************************/
        //단일 count
        fun countUidModelKey(modelKey: String) = "{\"query\":{\"bool\":{\"must\":[{\"match\":{\"uid\":\"${myuid()}\"}},{\"match\":{\"modelKey\":\"$modelKey\"}}]}}}"

        //count list
        fun countUID(size: Int, from: Int, uid: String) = "{\"size\":$size,\"from\":$from,\"query\":{\"bool\":{\"must\":[{\"match\":{\"uid\":\"$uid\"}}]}},\"sort\":[{\"order\":\"desc\"}]}"
        fun countModel(size: Int?, from: Int?, modelKey: String) = "{\"size\":${size ?: MAX},\"from\":${from ?: 0},\"query\":{\"bool\":{\"must\":[{\"match\":{\"modelKey\":\"$modelKey\"}}]}},\"sort\":[{\"order\":\"desc\"}]}"

        fun countPushAll(size: Int, modelKey: String) = "{\"size\":$size,\"query\":{\"bool\":{\"must\":[{\"match\":{\"modelKey\":\"$modelKey\"}}]}},\"sort\":[{\"addDate\":{\"order\":\"desc\"}}]}"
        fun quickUID(size: Int?, uid: String) = "{\"size\":${size ?: MAX},\"query\":{\"bool\":{\"must\":[{\"match\":{\"uid\":\"$uid\"}}]}},\"sort\":[{\"order\":{\"order\":\"asc\"}}]}"
        fun quickModel(size: Int?, modelKey: String) = "{\"size\":${size ?: MAX},\"query\":{\"bool\":{\"must\":[{\"match\":{\"modelKey\":\"$modelKey\"}}]}},\"sort\":[{\"order\":{\"order\":\"asc\"}}]}"

        /*********************************  sttReply  *******************************/
        fun replyModel(size: Int?, modelKey: String) = "{\"size\":${size ?: MAX},\"query\":{\"bool\":{\"must\":[{\"match\":{\"modelKey\":\"$modelKey\"}}]}}}"

        /*********************************  sttMyRoom  *******************************/
        fun myCount(size: Int, from: Int, uid: String, dataTypeNo: Int) = "{\"size\":$size,\"from\":$from,\"query\":{\"bool\":{\"must\":[{\"match\":{\"uid\":\"$uid\"}},{\"match\":{\"dataTypeNo\":$dataTypeNo}}]}},\"sort\":[{\"addDate\":{\"order\":\"desc\"}}]}"

        fun myContentsList(size: Int, from: Int, uid: String, status: Status) = "{\"size\":$size,\"from\":$from,\"query\":{\"bool\":{\"filter\":[{\"match\":{\"authorUID\":\"$uid\"}},{\"range\":{\"statusNo\":{\"gte\":${status.no}}}}]}},\"sort\":[{\"addDate\":{\"order\":\"desc\"}}]}"

        /*********************************  sttWarn  *******************************/
        fun warn(modelKey: String) = "{\"query\":{\"bool\":{\"filter\":[{\"match\":{\"modelKey\":\"$modelKey\"}},{\"match\":{\"authorUID\":\"${myuid()}\"}}]}}}"
        fun warnList(size: Int, from: Int) = "{\"size\":$size,\"from\":$from,\"sort\":[{\"addDate\":\"desc\"}]}"
        fun penaltyList(size: Int, from: Int) = "{\"size\":$size,\"from\":$from,\"sort\":[{\"addDate\":\"desc\"}]}"

        /*********************************  sttvCost  *******************************/
        fun costList(size: Int, from: Int, uid: String) = "{\"size\":$size,\"from\":$from,\"query\":{\"bool\":{\"filter\":[{\"match\":{\"uid\":\"$uid\"}}]}},\"sort\":[{\"addDate\":{\"order\":\"desc\"}}]}"
        fun costTypeList(size: Int, from: Int, uid: String, costType: CostType) = "{\"size\":$size,\"from\":$from,\"query\":{\"bool\":{\"filter\":[{\"match\":{\"uid\":\"$uid\"}},{\"match\":{\"costTypeNo\":${costType.no}}}]}},\"sort\":[{\"addDate\":{\"order\":\"desc\"}}]}"

        fun actCount(size: Int, actTypeNo: Int, dateGte: Long, dateLt: Long) = "{\"size\":$size,\"query\":{\"bool\":{\"must\":[{\"match\":{\"actTypeNo\":$actTypeNo}}],\"filter\":[{\"range\":{\"addDate\":{\"gte\":$dateGte,\"lt\":$dateLt}}}]}}}"
        fun costAll(size: Int, from: Int, uid: String) = "{\"size\":$size,\"from\":$from,\"query\":{\"bool\":{\"filter\":[{\"match\":{\"uid\":\"$uid\"}}]}},\"sort\":[{\"addDate\":{\"order\":\"desc\"}}]}"
        fun costAllNoAct(size: Int, from: Int, uid: String, costType1: CostType, costType2: CostType) = "{\"size\":$size,\"from\":$from,\"query\":{\"bool\":{\"filter\":[{\"match\":{\"uid\":\"$uid\"}},{\"range\":{\"costTypeNo\":{\"gte\":${costType1.no},\"lt\":${costType2.no + 1}}}}]}},\"sort\":[{\"addDate\":{\"order\":\"desc\"}}]}"
        fun betaCheck(uid: String) = "{\"query\":{\"bool\":{\"must\":[{\"match\":{\"uid\":\"$uid\"}}],\"filter\":[{\"range\":{\"actTypeNo\":{\"gte\":3701,\"lt\":3704}}}]}}}"

        /*********************************  sttPeer  *******************************/
        // nick, email 체크
        fun peerAllArrange(size: Int, from: Int, field: String, sort: Boolean) = "{\"size\":$size,\"from\":$from,\"sort\":[{\"$field\":{\"order\":\"${sortSelect(sort)}\"}}]}"
        fun peerExist(child: String, value: String) = "{\"query\":{\"bool\":{\"must\":[{\"match\":{\"$child\":\"$value\"}}]}}}"
        fun peerMatch(size: Int, child: String, email: String) = "{\"size\":$size,\"query\":{\"match\":{\"$child\":\"$email\"}}}"
        fun peerDeletedDate(email: String) = "{\"size\":1,\"query\":{\"match\":{\"email\":\"$email\"}},\"sort\":[{\"deletedDate\":{\"order\":\"desc\"}}]}"
        fun peerSearch(size: Int?,keyword:String, vararg child: String) = "{\"size\":${size ?: MIN_SIZE},\"query\":{\"bool\":{\"must\":[{\"query_string\":{\"fields\":[${getFields(*child)}],\"query\":\"*$keyword*\"}}]}}}"
        private fun getFields(vararg children: String) = StringBuffer().apply {
            children.forEach {
                if (!this.isBlank()) this.append(",")
                this.append("\"$it\"")
            }
        }.toString()

        /*********************************  sttMiniPeer  *******************************/
//        fun boardAllow(child: String, value: String) = " "

        /*********************************  sttTag  *******************************/
        fun tagSearchAll(size: Int, from: Int, keyword: String, status: Status) = "{\"size\":$size,\"from\":$from,\"query\":{\"bool\":{\"must\":[{\"query_string\":{\"fields\":[\"name\"],\"query\":\"*$keyword*\"}}],\"filter\":[{\"range\":{\"tagTypeNo\":{\"gte\":${TagTYPE.LOC.no}}}},{\"range\":{\"statusNo\":{\"gte\":${status.no}}}}]}},\"sort\":[{\"totalPoint\":{\"order\":\"desc\"}}]}"

        fun tagSearchAllforAdmin(size: Int, from: Int, keyword: String, status: Status) = "{\"size\":$size,\"from\":$from,\"query\":{\"bool\":{\"must\":[{\"query_string\":{\"fields\":[\"name\"],\"query\":\"*$keyword*\"}}],\"filter\":[{\"range\":{\"tagTypeNo\":{\"gte\":${TagTYPE.ALL.no}}}},{\"range\":{\"statusNo\":{\"gte\":${status.no}}}}]}},\"sort\":[{\"addDate\":{\"order\":\"desc\"}}]}"
//                "{\"size\":$size,\"from\":$from,\"query\":{\"bool\":{\"must\":[{\"query_string\":{\"fields\":[\"name\"],\"query\":\"*$keyword*\"}}],\"filter\":[{\"range\":{\"statusNo\":{\"gte\":\"${status.no}\"}}}]}},\"sort\":[{\"addDate\":{\"order\":\"desc\"}},{\"totalPoint\":{\"order\":\"desc\"}}]}"

        fun mytagList(size: Int, from: Int, queryTypes: String, keyword: String, status: Status) = "{\"size\":$size,\"from\":$from,\"query\":{\"bool\":{\"must\":[{\"terms\":{\"tagTypeNo\":[$queryTypes]}},{\"query_string\":{\"fields\":[\"name\"],\"query\":\"*$keyword*\"}}],\"filter\":[{\"range\":{\"statusNo\":{\"gte\":\"${status.no}\"}}}]}},\"sort\":[{\"totalPoint\":{\"order\":\"desc\"}}]}"
        fun mytagListManage(size: Int, from: Int, queryTypes: String, keyword: String, status: Status) = "{\"size\":$size,\"from\":$from,\"query\":{\"bool\":{\"must\":[{\"terms\":{\"tagTypeNo\":[$queryTypes]}},{\"query_string\":{\"fields\":[\"name\"],\"query\":\"*$keyword*\"}}],\"filter\":[{\"range\":{\"statusNo\":{\"gte\":\"${status.no}\"}}}]}},\"sort\":[{\"addDate\":{\"order\":\"desc\"}}]}"
        fun mytagManage(size: Int, from: Int, tagType: TagTYPE, keyword: String) = "{\"size\":$size,\"from\":$from,\"query\":{\"bool\":{\"must\":[{\"terms\":{\"tagTypeNo\":[${tagType.queryTypes}]}},{\"query_string\":{\"fields\":[\"name\"],\"query\":\"*$keyword*\"}}]}},\"sort\":[{\"totalPoint\":{\"order\":\"desc\"}},{\"addDate\":{\"order\":\"desc\"}}]}"
        fun mytagCheckBan(size: Int, keywords: String, status: Status) = "{\"size\":$size,\"query\":{\"bool\":{\"must\":[{\"match\":{\"tagTypeNo\":1000}},{\"match\":{\"name\":\"$keywords\"}}],\"filter\":[{\"range\":{\"statusNo\":{\"gte\":${status.no}}}}]}}}"

        /*********************************  sttAdunit  *******************************/
        fun bidStatus(size: Int, from: Int, adTypeNo: Int, hitDate: Int, status: Status) = "{\"size\":$size,\"from\":$from,\"query\":{\"bool\":{\"must\":[{\"match\":{\"adTypeNo\":$adTypeNo}},{\"match\":{\"hitDate\":$hitDate}}],\"filter\":[{\"range\":{\"statusNo\":{\"gte\":${status.no}}}}]}},\"sort\":[{\"amount\":{\"order\":\"desc\"}},{\"addDate\":{\"order\":\"asc\"}}]}"

        //        fun bidStatusAdmin(size: Int, from: Int,   adTypeNo: Int?, hitDate: Int?, term: Int?, status: Status?): String {
//            adTypeNo?.let { ${ match(CH_ADTYPE_NO, adTypeNo) } }
//            return "{${size(size)}${from(from)}\"query\":{\"bool\":{\"must\":[${match(CH_ADTYPE_NO, adTypeNo)},]}},\"sort\": [${sortAddDateDesc()}]}"
//        }

        fun bidTopBoard(size: Int, from: Int, adTypeNo: Int, hitDate: Int, boardKey: String, status: Status) = "{\"size\":$size,\"from\":$from,\"query\":{\"bool\":{\"must\":[{\"match\":{\"adTypeNo\":$adTypeNo}},{\"match\":{\"hitDate\":$hitDate}},{\"match\":{\"$CH_BOARD_KEY\":\"$boardKey\"}}],\"filter\":[{\"range\":{\"statusNo\":{\"gte\":${status.no}}}}]}},\"sort\":[{\"amount\":{\"order\":\"desc\"}},{\"addDate\":{\"order\":\"asc\"}}]}"
        fun bidSuccess(size: Int, hitDate: Int, adTypeNo: Int, status: Status) = "{${size(size)}\"query\":{\"bool\":{\"must\":[${match(CH_HIT_DATE, hitDate)},${match(CH_ADTYPE_NO, adTypeNo)},${match(CH_STATUS_NO, status.no)}]}},\"sort\": [${sort(CH_AMOUNT, CH_DESC)},${sort(CH_ADD_DATE, CH_ASC)}]}"
        fun bidSuccessBoard(size: Int, hitDate: Int, adTypeNo: Int, boardKey: String, status: Status) = "{${size(size)}\"query\":{\"bool\":{\"must\":[${match(CH_HIT_DATE, hitDate)},${match(CH_ADTYPE_NO, adTypeNo)},,${match(CH_BOARD_KEY, boardKey)}${match(CH_STATUS_NO, status.no)}]}},\"sort\": [${sort(CH_AMOUNT, CH_DESC)},${sort(CH_ADD_DATE, CH_ASC)}]}"
        fun bidManage(size: Int, from: Int, uid: String, status: Status) = "{\"size\":$size,\"from\":$from,\"query\":{\"bool\":{\"must\":[{\"match\":{\"uid\":\"$uid\"}}],\"filter\":[{\"range\":{\"statusNo\":{\"gte\":${status.no}}}}]}},\"sort\":[{\"addDate\":{\"order\":\"desc\"}}]}"
        fun bidManageAdmin(size: Int, from: Int, adTypeNo: Int, hitDate: Int, status: Status) = "{\"size\":$size,\"from\":$from,\"query\":{\"bool\":{\"must\":[{\"match\":{\"adTypeNo\":$adTypeNo}},{\"match\":{\"hitDate\":$hitDate}}],\"filter\":[{\"range\":{\"statusNo\":{\"gte\":${status.no}}}}]}},\"sort\":[{\"amount\":{\"order\":\"desc\"}},{\"addDate\":{\"order\":\"asc\"}}]}"
        fun bidPhotoUsed(photoThumb: String, hitDate: Int, status: Status) = "{\"query\":{\"bool\":{\"must\":[{\"match\":{\"photoThumb\":\"$photoThumb\"}}],\"filter\":[{\"range\":{\"statusNo\":{\"gte\":${status.no}}}},{\"range\":{\"hitDate\":{\"gte\":$hitDate}}}]}}}"
        fun bidAdTypeHit(adTypeNo: Int, hitDate: Int, status: Status) = "{\"size\":500,\"from\":0,\"query\":{\"bool\":{\"must\":[{\"match\":{\"adTypeNo\":$adTypeNo}},{\"match\":{\"hitDate\":$hitDate}}],\"filter\":[{\"range\":{\"statusNo\":{\"gte\":${status.no}}}}]}},\"sort\":[{\"amount\":{\"order\":\"desc\"}},{\"addDate\":{\"order\":\"asc\"}}]}"

        /*********************************  sttBanner  *******************************/
        fun myBannerList(size: Int, from: Int, uid: String, bannerTypeNo: Int, status: Status) = "{\"size\":$size,\"from\":$from,\"query\":{\"bool\":{\"must\":[{\"match\":{\"bannerTypeNo\":$bannerTypeNo}},{\"match\":{\"uid\":\"$uid\"}}],\"filter\":[{\"range\":{\"statusNo\":{\"gte\":${status.no}}}}]}},\"sort\":[{\"addDate\":{\"order\":\"desc\"}}]}"
        fun bannerList(size: Int, from: Int, bannerTypeNo: Int) = "{${size(size)}${from(from)}\"query\":{\"bool\":{\"must\":[${match(CH_BANNER_TYPE_NO, bannerTypeNo)}]}},\"sort\": [${sortAddDateDesc()}]}"

        /*********************************  sttLobby  *******************************/

        /*********************************  sttTicket & Bill  *******************************/
        fun ticketListAdmin() = "{\"size\":100,\"query\":{\"match_all\":{}},\"sort\":[{\"order\":{\"order\":\"asc\"}}]}"
        fun ticketList(status: Status) = "{\"size\":100,\"query\":{\"match\":{\"statusNo\":${status.no}}},\"sort\":[{\"order\":{\"order\":\"asc\"}}]}"
        fun billList(size: Int, from: Int, uid: String) = "{\"size\":$size,\"from\":$from,\"query\":{\"match\":{\"uid\":\"$uid\"}}}"

        /*********************************  sttGrant  *******************************/
        fun grantInfo(size: Int, modelKey: String) = "{\"size\":$size,\"query\":{\"bool\":{\"filter\":[{\"match\":{\"modelKey\":\"$modelKey\"}}]}},\"sort\":[{\"addDate\":{\"order\":\"desc\"}}]}"
        fun grantListLately(size: Int, from: Int) = "{\"size\":$size,\"from\":$from,\"query\":{\"match_all\":{}},\"sort\":[{\"statusNo\":{\"order\":\"asc\"}},{\"addDate\":{\"order\":\"asc\"}}]}"

    }
}