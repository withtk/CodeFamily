package com.tkip.mycode.util.lib.calendar

import com.haibin.calendarview.Calendar
import com.haibin.calendarview.CalendarView
import com.tkip.mycode.R
import com.tkip.mycode.funs.common.lllogD
import com.tkip.mycode.init.base.format02
import com.tkip.mycode.init.base.getStr


/************************************************
 * CalendarView  hainan
 *************************************************/
//fun Calendar.monthDate(): String = String.format(R.string.cal_month_date.getStr(), this.month, this.day)

fun Calendar.year(): String = "${this.year}."

fun Calendar.month(): String = "${this.month}."
fun Calendar.getDate8() = "${this.year}${this.month.format02()}${this.day.format02()}".toInt()


fun CalendarView.year(): String = "${this.curYear}."
fun CalendarView.month(): String = "${this.curMonth}."
fun CalendarView.monthDate(): String = String.format(R.string.cal_month_date.getStr(), this.curMonth, this.curDay)
//fun CalendarView.year(): String = String.format(R.string.cal_year.getStr(), this.curYear)

fun CalendarView.onSelect(onSelect: (calendar: Calendar?, isClick: Boolean) -> Unit, onOutOfRange: (calendar: Calendar?) -> Unit) {
    setOnCalendarSelectListener(object : CalendarView.OnCalendarSelectListener {
        override fun onCalendarSelect(calendar: Calendar?, isClick: Boolean) {
            lllogD("calendarView onCalendarSelect")
            onSelect(calendar, isClick)
        }

        override fun onCalendarOutOfRange(calendar: Calendar?) {
            lllogD("calendarView onCalendarOutOfRange")
            onOutOfRange(calendar)
        }
    })
}

fun CalendarView.onRange(onOutOfRange: (calendar: Calendar?) -> Unit, onSelect: (calendar: Calendar?, isEnd: Boolean) -> Unit, onOutOfMax: (calendar: Calendar?, isOutOfMinRange: Boolean) -> Unit) {
    setOnCalendarRangeSelectListener(object : CalendarView.OnCalendarRangeSelectListener {
        override fun onCalendarSelectOutOfRange(calendar: Calendar?) {
            // setRange의 범위가 아닐때
            lllogD("calendarView onCalendarSelectOutOfRange")
            onOutOfRange(calendar)
        }

        override fun onCalendarRangeSelect(calendar: Calendar?, isEnd: Boolean) {
            // 정상 범위 클릭.
            lllogD("calendarView onCalendarRangeSelect isEnd:$isEnd")
            onSelect(calendar, isEnd)
        }

        override fun onSelectOutOfRange(calendar: Calendar?, isOutOfMinRange: Boolean) {
            // setRange의 범위이지만 max_select_range를 벗어났을 때
            lllogD("calendarView onSelectOutOfRange   isOutOfMinRange:$isOutOfMinRange ")
            onOutOfMax(calendar, isOutOfMinRange)

        }

    })
}

fun CalendarView.onMulti(onOutOfRange: (calendar: Calendar?) -> Unit, onSelect: (calendar: Calendar?, curSize: Int, maxSize: Int) -> Unit, onOutOfMax: (calendar: Calendar?, maxSize: Int) -> Unit) {
    setOnCalendarMultiSelectListener(object : CalendarView.OnCalendarMultiSelectListener {
        override fun onCalendarMultiSelectOutOfRange(calendar: Calendar?) {
            // setRange의 범위가 아닐때
            lllogD("calendarView onCalendarMultiSelectOutOfRange")
            onOutOfRange(calendar)
        }

        override fun onCalendarMultiSelect(calendar: Calendar?, curSize: Int, maxSize: Int) {
            // 정상 범위 클릭.
            lllogD("calendarView onCalendarMultiSelect curSize:$curSize maxSize:$maxSize")
            onSelect(calendar, curSize, maxSize)
        }

        override fun onMultiSelectOutOfSize(calendar: Calendar?, maxSize: Int) {
            // setRange의 범위이지만 maxSize를 초과했을 때
            lllogD("calendarView onMultiSelectOutOfSize maxSize:$maxSize")
            onOutOfMax(calendar, maxSize)
        }

    })
}

