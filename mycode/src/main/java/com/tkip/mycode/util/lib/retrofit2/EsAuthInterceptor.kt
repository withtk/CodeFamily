package com.tkip.mycode.util.lib.retrofit2

import com.tkip.mycode.R
import com.tkip.mycode.funs.common.lllogD
import com.tkip.mycode.funs.common.remoteStr
import com.tkip.mycode.init.part.CodeApp
import okhttp3.Credentials
import okhttp3.Interceptor
import okhttp3.Request
import okhttp3.Response
import java.io.IOException


class EsAuthInterceptor
(
//        val user: String? = null,
//        val password: String? = null
)
    : Interceptor {

    @Throws(IOException::class)
    override fun intercept(chain: Interceptor.Chain): Response {
//val HEA = hashMapOf<String, String>("Authorization" to Credentials.basic(R.string.remote_elasticsearch_id.remoteStr(), R.string.remote_elasticsearch_pw.remoteStr()))

        val id =CodeApp.esId?: R.string.remote_elasticsearch_id.remoteStr()
        val pw = CodeApp.esPw?:R.string.remote_elasticsearch_pw.remoteStr()

//        lllogD("EsAuthInterceptor EsAuthInterceptor id $id pw $pw")

        val credentials = Credentials.basic(id,pw)
        val authenticatedRequest: Request = chain.request().newBuilder().header("Authorization", credentials).build()
        return chain.proceed(authenticatedRequest)


//        val credentials = Credentials.basic(R.string.remote_elasticsearch_id.remoteStr(), R.string.remote_elasticsearch_pw.remoteStr())
//        try {
//            val authenticatedRequest: Request = chain.request().newBuilder().header("Authorization", credentials).build()
//            return chain.proceed(authenticatedRequest)
//        } catch (e: SocketTimeoutException) {
//            e.printStackTrace()
//            lllogE("EsAuthInterceptor intercept timeout ${e.message}")
//        }
//        return chain.proceed(chain.request())

    }

}