package com.tkip.mycode.util.lib.photo.util

import android.content.ContentUris
import android.content.Context
import android.net.Uri
import android.provider.MediaStore
import android.widget.TextView
import com.google.firebase.storage.FirebaseStorage
import com.tkip.mycode.R
import com.tkip.mycode.dialog.progress.MyCircle
import com.tkip.mycode.dialog.progress.WAIT_TYPE
import com.tkip.mycode.funs.common.lllogD
import com.tkip.mycode.funs.common.lllogI
import com.tkip.mycode.init.WIDTH_MID
import com.tkip.mycode.model.my_enum.getStorageChild
import com.tkip.mycode.model.peer.OnPeer
import com.tkip.mycode.util.lib.photo.Foto
import com.tkip.mycode.util.lib.photo.FotoInfo
import com.tkip.mycode.util.lib.photo.saf.isExist2
import com.tkip.mycode.util.tools.fire.real
import java.io.File

/**
 * uri를 업로드 하는 유틸.
 */
class PhotoUtil {

    companion object {


        val photoUris = arrayListOf<Uri>()    // 사용자가 선택한 총 uri
        private val percentMap = mutableMapOf<Int, Int>()     // Async에서 만든 index에 따라 percent를 기록하는 map
        val urlMap = mutableMapOf<Int, String>()    // Async에서 만든 index에 따라 업로드 완료된 url을 기록하는 map -  짝수0:thumb  홀수1:origin

//        var mainFoto: Foto? = null   // 최초 foto 선택 후 메인포토 자동 설정


        /**
         * 1. photoUris uploadIndexList 만들기.
         * 1. deletedList  -> 삭제 -> 기존리스트에서 삭제.
         *
         *  successUploading : urlMap에서 두개씩 간추려서 list로 리턴.
         *  widthOrigin : originFoto 사이즈 설정.
         */
        fun uploadUriList(context: Context,
                          widthOrigin: Int?,
//                          child: String,
                          any: Any,
                          dbFotoList: ArrayList<Foto>?,
                          selectedFotoList: ArrayList<Foto>,  // 현재 보여지는 fotoList(즉, 최종적리스트)
                          successUploading: (ArrayList<Foto>?) -> Unit,
                          fail: (Exception) -> Unit) {

            if (OnPeer.notAllowedDB()) return
            val child = any.getStorageChild()

            lllogI("uploadUriList selectedFotoList size:${selectedFotoList.size}")

            /** selectedFotoList. uri 존재 체크 */
//            selectedFotoList.checkUriExistance(context)

            /** 업로드 list 만들기 */
            val uploadIndexList = selectedFotoList.getUploadIndexList()  // selectedFotoList의 번호중에서 upload될 foto들의 번호
            lllogI("uploadUriList uploadIndexList size:${uploadIndexList.size}")

            /** DB에서 삭제 */
            dbFotoList?.makeDeleteList(selectedFotoList)?.let { deleteList -> deletePhoto(deleteList) }

            if (photoUris.isEmpty()) {
                lllogI("uploadUriList photoUris isEmpty")
//                MyProgress.show(activity, PPro(false))
                WAIT_TYPE.BASIC_1.show(context)
                successUploading(selectedFotoList)
            } else {
                lllogI("uploadUriList photoUris size:${photoUris.size}")

                /** 업로드 시작 */
//                MyProgress.show(activity, PPro(PPro.PROG_PERCENT, false))
                WAIT_TYPE.PERCENT.show(context)

                var index = photoUris.size * 2  // 업로드될 전체파일수(썸네일포함). 업로드완료시마다 -1

//                if (versionGteQ)
//                    AsyncUploadUriSAF(context, widthOrigin ?: WIDTH_MID, child,
//                            onProgress = { i, percent ->
//                                calculatePercent(i, percent)
////                            onProgress(strPercent)
//                            },
//                            success = { i, photoUrl ->
//                                lllogI("uploadUriList  success ")
//
//                                /**
//                                 * selectedFotoList의 상태는 변하지 않는다.
//                                 * 업로드 된 fotoUrl2개를 list안의 똑같은 index에 그대로 넣어주면 된다.
//                                 */
//                                // 짝수이면(썸네일이면)
//                                if (i % 2 == 0) selectedFotoList[uploadIndexList[i / 2]].thumbUrl = photoUrl
//                                else selectedFotoList[uploadIndexList[i / 2]].url = photoUrl
//
//                                if (index == 1) { // 1이면 마지막 업로드완료된 파일임.
//                                    successUploading(selectedFotoList)
//                                    resetAllUriList()   //  모두 초기화.
//                                } else index--
//
//                            },
//                            fail = { fail(it) })
//                            .execute()
//                else {
                AsyncUploadUri(context, widthOrigin ?: WIDTH_MID, child,
                        onProgress = { i, percent ->
                            calculatePercent(i, percent)
//                            onProgress(strPercent)
                        },
                        success = { i, photoUrl ->
                            lllogI("uploadUriList  success ")
                            /**
                             * selectedFotoList의 상태는 변하지 않는다.
                             * 업로드 된 fotoUrl2개를 list안의 똑같은 index에 그대로 넣어주면 된다.
                             */
                            // 짝수이면(썸네일이면)
                            if (i % 2 == 0) selectedFotoList[uploadIndexList[i / 2]].thumbUrl = photoUrl
                            else {
                                selectedFotoList[uploadIndexList[i / 2]].url = photoUrl
                                val f = selectedFotoList[uploadIndexList[i / 2]]
                                FotoInfo(any,f).real()   // 명세서 올리기
                            }

                            if (index == 1) { // 1이면 마지막 업로드완료된 파일임.
                                successUploading(selectedFotoList)
                                resetAllUriList()   //  모두 초기화.
                            } else index--

                        },
                        fail = { fail(it) })
                        .execute()
//                        .execute(*photoUris.toTypedArray())

//                }
            }

        }

        /**
         * 1개짜리 uri 올리기
         */
        fun uploadUriSingle(context: Context,
                            any: Any,
//                            child: String,
                            successUploading: (Foto?) -> Unit,
                            fail: (Exception) -> Unit) {

            if (OnPeer.notAllowedDB()) return

            val child = any.getStorageChild()

            if (photoUris.isEmpty()) {
//                MyProgress.show(activity, PPro(false))
                WAIT_TYPE.BASIC_1.show(context)
                successUploading(null)
            } else {
                WAIT_TYPE.PERCENT.show(context)
//                MyProgress.show(activity, PPro(PPro.PROG_PERCENT, false))
                AsyncUploadUri(context, WIDTH_MID, child,
                        onProgress = { i, percent ->
                            calculatePercent(i, percent)
//                            onProgress(strPercent)
                        },
                        success = { i, photoUrl ->
                            lllogI("uploadUriList  success ")

                            // 각각의 successUploading 때마다 매번 호출됨.
                            urlMap[i] = photoUrl

                            if (urlMap.size == photoUris.size * 2) {        // 마지막 success가 왔는지 확인.
                                for ((j, url) in urlMap) {
                                    if (j % 2 == 0) {                       // 짝수이면(썸네일이면)
                                        val foto = Foto(thumbUrl = url, url = urlMap[j + 1])
                                        FotoInfo(any,urlMap[0],urlMap[1]).real()  // 명세서 올리기.
                                        successUploading(foto)
                                    }
                                }
                                resetAllUriList()
                            }

                        },
                        fail = { fail(it) })
                        .execute()
            }

        }

        private fun calculatePercent(i: Int, percent: Int) {
            percentMap[i] = percent
            var percents = 0L
            percentMap.forEach { (_, v) ->
                percents += v   // 각각의 % 가져오기.
            }

            val resultPercent = 100 * percents / (photoUris.size * 2 * 100)   // 각각의 %를 전체%로 계산
            val strPercent = "$resultPercent%"

//            MyProgress.dialog?.findViewById<TextView>(R.id.tv_percent)?.text = strPercent
            MyCircle.dialog?.findViewById<TextView>(R.id.tv_percent)?.text = strPercent

            lllogI("calculatePercent strPercent $strPercent")
        }

        /**
         * selectedFotoList 중에서 Uri 유효성 체크
         */
        private fun ArrayList<Foto>.checkUriExistance(context: Context) {
            if (this.isNullOrEmpty()) return
            val list = arrayListOf<Int>()  // this중 유효하지 않은 foto index.
            forEachIndexed { i, f1 ->
                f1.uri?.let { uri ->
                    val valid = uri.isExist2(context)
                    if (!valid) list.add(i)
                    lllogI("checkUriExistance valid $valid")
                }
            }
            lllogI("checkUriExistance list ${list.size}")
            list.reverse()
            list.forEach {
                lllogI("checkUriExistance list removeAt $it")
                this.removeAt(it)
            }
        }

        /**
         * selectedFotoList 중에서.
         */
        private fun ArrayList<Foto>.getUploadIndexList(): ArrayList<Int> {
            val uploadIndexList = arrayListOf<Int>()  // this중 업로드될 파일의 index.
            photoUris.clear()
//            if (versionGteQ)
//                forEachIndexed { i, f1 ->
//                    f1.uri?.let { uri ->
//                        photoUris.add(uri) // 업로드 될 Foto
//                        uploadIndexList.add(i)   // 업로드될 foto의 index 목록에 추가.
//                        f1.clear()   // MyPhoto적 요소들을 모두 삭제. 나중에 이 리스트 그대로 DB에 올라가기 때문. (uri때문에 오류발생)
//                    }
//                }
//            else
            this.forEachIndexed { i, f1 ->
                lllogI("uploadUriList getUploadIndexList foto ${f1.photoId}")
                if (f1.photoId != null) {
                    photoUris.add(getResourceUri(f1.photoId!!)) // 업로드 될 Foto
                    uploadIndexList.add(i)   // 업로드될 foto의 index 목록에 추가.
                    f1.clearNoDB()   // MyPhoto적 요소들을 모두 삭제. 나중에 이 리스트 그대로 DB에 올라가기 때문. (uri때문에 오류발생)
                }
            }

            return uploadIndexList
        }


        /**
         * dbFotoList중에서 selectedFotoList에 없는 것. : 삭제될 foto.
         */
        private fun ArrayList<Foto>.makeDeleteList(selectedFotoList: ArrayList<Foto>): ArrayList<Foto> {
            val deletedList = arrayListOf<Foto>()
            this.forEach { f1 ->
                run loop@{
                    selectedFotoList.forEach { f2 ->
                        if (f1.key == f2.key) return@loop
                    }
                    deletedList.add(f1)
                }
            }
            lllogI("PhotoUtil makeDeleteList list.size ${deletedList.size}")
            return deletedList
        }


        /**
         * photoID로 Uri받아오기 : uri를 content로 변경하는 작업
         * 이 작업을 하지 않으면 uri를 쓸 수가 없다.
         * 단 pick으로 사진을 불러왔을때는 제외.
         */
        private fun getResourceUri(photoId: String) = ContentUris.withAppendedId(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, photoId.toLong())

        val temps = arrayListOf<File>()
        /**
         * 크랍완료한 삭제할 File을 FileList에 추가.
         */
        fun addDeleteFile(file: File) {
            temps.add(file)
//            tobeDeleteFileSet.add(file.path)
//            Sred.getInstance().putStringSet(Sred.KEY_SET_COLLECTION_UNDELETED_CAPTURE, tobeDeleteFileSet)
        }

        private fun deleteTempFile() {
            Thread {
                for (f in temps) {
                    if (f.exists()) {
                        f.delete()
                    }
                }
                /*  //Delete file on exit
                  Files.deleteIfExists(path);

                  //Delete file immediately
                  Files.delete(path);*/
//                    tobeDeleteFileSet.clear()
//                    Sred.getInstance().removeSet(Sred.KEY_SET_COLLECTION_UNDELETED_CAPTURE)
            }.start()
        }


        /************************************************************
         *************  dbfoto  *************************************
         ************************************************************/

        /**
         * 업로드할 이미지list 초기화
         */
        fun resetAllUriList() {
            photoUris.clear()
            percentMap.clear()
            urlMap.clear()
//            mainFoto = null
            deleteTempFile()
//            MyProgress.cancel()
//            MyCircle.cancel()
        }

        fun child(vararg children: String): String {
            val result = StringBuffer()
            for (child in children) {
                result.append("$child/")
            }
            val str = result.toString()
            val resultChild = str.substring(0, str.length - 1)
            lllogD("=============================resultChild : $resultChild")
            return resultChild
        }

//        fun uploadPhotoUri(child: String,
//                           index: Int, uploadUri: Uri
//                           , onProgress: (Int, Int) -> Unit
//                           , success: (Int, String) -> Unit
//                           , fail: (Exception) -> Unit) {
//
//            lllogI("uploadPhotoUri  ")
//
//            val uploadName = Firee.getUID6() + System.currentTimeMillis().getKoreanTimeString(FORMAT_BASIC) + index
//            val ref = FirebaseStorage.getInstance().getReference(child)
//                    .child(uploadName)
//            lllogD("=============================start : $uploadName")
//            lllogD("=============================ref : ${ref.toString()}")
//
//            ref.putFile(uploadUri)
//                    .addOnProgressListener {
//                        lllogD("addOnProgressListener")
//                        val transferredByte = it.bytesTransferred
//                        val fileTotalByte = it.totalByteCount
//                        val percent = (100.0 * transferredByte / fileTotalByte)
//                        lllogD("Upload is ${percent.toInt()}% done")
//                        onProgress(index, percent.toInt())
//                    }
//                    .addOnPausedListener {
//                        lllogD("addOnPausedListener")
//                    }
//                    .addOnSuccessListener { task ->
//                        lllogI("addOnSuccessListener  ")
//                        val urlTask = task.storage.downloadUrl
//                        urlTask.addOnSuccessListener { uri ->
//                            val url = uri.toString()
//                            lllogD("addOnSuccessListener.url:" + url)
//                            success(index, url)
//                        }
//                    }
//                    .addOnFailureListener {
//                        lllogD("addOnFailureListener")
//                        fail(it)
//                    }
//        }


        fun deletePhoto(vararg urls: String?) {
            for (url in urls) {
                lllogD("deletePhoto url : $url")
                url?.let { delete(it) }
            }
        }

        fun deletePhoto(fotoList: ArrayList<Foto>) {
            /**             * 지운 걸로 표시해 놓을 때             */
//            OnFotoInfo.addDeleteDateFotoInfo(fotoList)

            /**             * 바로 지울 때             */
            fotoList.forEach { f ->
                lllogD("deletePhoto f : $f")
                f.url?.let { delete(it) }
                f.thumbUrl?.let { delete(it) }
            }
        }


        private fun delete(url: String) {

            lllogD("deletePhoto url : $url")
            if (url.contains("firebasestorage")) {
                FirebaseStorage.getInstance().getReferenceFromUrl(url)   /*** 이슈: 이게 됐다가 안 됐다가 하네.. */
//                FirebaseStorage.getInstance().reference.child(url)
                        .delete()
                        .addOnSuccessListener { lllogD("deletePhoto addOnSuccessListener  ") }
                        .addOnFailureListener { lllogD("deletePhoto addOnFailureListener message:${it.message}") }
            }

        }
/*
        fun uploadPeerPhotoUri(uploadUri: Uri
                               , success: (String) -> Unit
                               , fail: () -> Unit) {

            val uploadName = Firee.getUID6() + System.currentTimeMillis().getKoreanTimeString(FORMAT_BASIC)
            val ref = FirebaseStorage.getInstance().getReference(OnPeer.peer.uid)
                    .child("foto")
                    .child(uploadName)
            val uploadTask = ref.putFile(uploadUri)
                    .addOnProgressListener {
                        lllogD("addOnProgressListener")
                    }
                    .addOnPausedListener {
                        lllogD("addOnPausedListener")
                    }
                    .addOnSuccessListener { task ->
                        lllogD("addOnSuccessListener")

                        val urlTask = task.storage.downloadUrl
                        urlTask.addOnSuccessListener { uri ->
                            val url = uri.toString()
                            lllogD("url.url:" + url)
                            success(url)
                        }

//                        while (! urlTask.isSuccessful());
//                        val downloadUrl = urlTask.getResult()
//                        lllogD("ref.downloadUrl:" + downloadUrl)
//
//                        val sdownload_url = downloadUrl.toString()

//                        val aaaa = task.storage.downloadUrl

                    }
                    .addOnFailureListener {
                        fail()
                        lllogD("addOnFailureListener")
                    }

        }*/


        /************************************************************
         *************  foto list 정리하기  ***************************
         ************************************************************/

        /**
         * 차집합 list1-list2
         * (적은것 - 많은것)으로 하는 것이 더 빠름.
         */
        fun subtract(list1: ArrayList<Foto>, list2: ArrayList<Foto>) =
                arrayListOf<Foto>().apply {
                    firstLabel@ for (foto1 in list1) {
                        for (foto2 in list2) {
                            if (foto1.url == foto2.url) {   // item이 list2에 존재.
                                continue@firstLabel
                            }
                        }
                        add(foto1)
                        // item이 list2에 없음.
                    }
                }


        /**
         * foto list 내의 업로드해야 할 uri가 있는지 체크.
         */
        fun isContainsURI(selectedFotoList: ArrayList<Foto>): Boolean {
            for (photo in selectedFotoList) {
                photo.uri?.let {
                    return true
                }
            }
            return false
        }

    }

}