package com.tkip.mycode.util.lib.photo.album

import android.net.Uri
import android.provider.MediaStore
import com.tkip.mycode.funs.common.lllogD
import com.tkip.mycode.util.lib.photo.MyPhoto

class OnAlbum {

    companion object {

        lateinit var listFolder: ArrayList<String>   // 폴더명
        lateinit var listSpinner: ArrayList<String>   // 폴더명
        lateinit var mapItems: MutableMap<String, ArrayList<MyPhoto>>   // <폴더명,MyPhoto리스트>
//        private var mapItems = mutableMapOf<String, ArrayList<MyPhoto>>()   // <폴더명,MyPhoto리스트>
//        private var listSpinner = arrayListOf<String>()   // 폴더명


        fun initAlbum(activity: AlbumActivity) {
            listFolder = arrayListOf()
            mapItems = mutableMapOf()
            listSpinner = arrayListOf()

            fetchAllImages(activity)
            modifyFolderName()
        }

        fun fetchAllImages(activity: AlbumActivity) {
            // DATA는 이미지 파일의 스트림 데이터 경로를 나타냅니다.
            val projection = arrayOf(
                    MediaStore.Images.Media.DATA,
                    MediaStore.Images.Media._ID,
                    MediaStore.Images.Media.DATE_TAKEN,
                    MediaStore.Images.Media.SIZE,
                    MediaStore.Images.Media.ORIENTATION)

            val cursor = activity.contentResolver.query(
                    MediaStore.Images.Media.EXTERNAL_CONTENT_URI, // 이미지 컨텐트 테이블
                    projection,                                   // DATA를 출력
                    null,                                // 모든 개체 출력
                    null,
                    MediaStore.MediaColumns.DATE_ADDED + " DESC") // 정렬

            if (cursor == null) {
                lllogD("fetchAllImages cursor null")   // Error 발생
                return
            } else {
                val dataColumnIndex = cursor.getColumnIndex(projection[0])
                val dataId = cursor.getColumnIndex(projection[1])
                val dataDateTaken = cursor.getColumnIndex(projection[2])
                val dataSize = cursor.getColumnIndex(projection[3])
                val dataOrientation = cursor.getColumnIndex(projection[4])


                if (cursor.moveToFirst()) {
                    do {
                        val filePath = cursor.getString(dataColumnIndex)
                        val imageUri = Uri.parse(filePath)
                        val id = cursor.getString(dataId)
                        val taken = cursor.getString(dataDateTaken)
                        val size = cursor.getString(dataSize)
                        val orientation = cursor.getString(dataOrientation)

                        val array = filePath.split("/".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()  //folder별 정리.
                        val folder = array[array.size - 2]


                        val myPhoto = MyPhoto(
                                uri = imageUri,
                                thumbUri = null,
                                photoPath = filePath,
                                photoId = id,
                                date = taken,
                                size = size,
                                orientation = orientation,
                                folder = folder,
                                isSel = false
                        )
                        addListFolder(myPhoto)


                    } while (cursor.moveToNext())

                } else {
                    // Cursor가 비었습니다.
                    lllogD("fetchAllImages Cursor가 비었습니다.")
                }
            }
            cursor.close()
        }

        private fun addListFolder(myPhoto: MyPhoto) {

            myPhoto.folder?.let { folder ->

                if (listFolder.contains(folder)) {    // folder가 있을때
                    for ((k, v) in mapItems) {
                        if (k == folder) {   // map에 그 folder 키에 list안에 myphoto 넣기.
                            v.add(myPhoto)
                        }
                    }

                } else {                              // folder가 없을때
                    listFolder.add(folder)
                    val list = ArrayList<MyPhoto>()
                    list.add(myPhoto)
                    mapItems[folder] = list

                }


            }

        }

        fun modifyFolderName() {
            // folder안의 image count를 spinner에 넣기위해.
            for ((k, v) in mapItems) {
                listSpinner.add(k + " (" + v.size + ")")
            }
        }
    }

}