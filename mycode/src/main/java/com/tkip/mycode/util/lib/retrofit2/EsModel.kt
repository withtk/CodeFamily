package com.tkip.mycode.util.lib.retrofit2


import com.google.firebase.firestore.IgnoreExtraProperties
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import com.tkip.mycode.model.bid.Bid
import com.tkip.mycode.model.board.Board
import com.tkip.mycode.model.cost.Cost
import com.tkip.mycode.model.count.Count
import com.tkip.mycode.model.flea.Flea
import com.tkip.mycode.model.grant.Grant
import com.tkip.mycode.model.manage.Device
import com.tkip.mycode.model.manage.SystemMan
import com.tkip.mycode.model.mytag.MyTag
import com.tkip.mycode.model.peer.Peer
import com.tkip.mycode.model.peer.peer_allow.PeerAllow
import com.tkip.mycode.model.peer.recommend.UidDate
import com.tkip.mycode.model.post.Post
import com.tkip.mycode.model.reply.Reply
import com.tkip.mycode.model.room.Room
import com.tkip.mycode.model.store.Store
import com.tkip.mycode.model.store.StoreMenu
import com.tkip.mycode.model.ticket.Ticket
import com.tkip.mycode.model.warn.Warn
import com.tkip.mycode.nav.lobby.Lobby


data class Result(
    var alljson: String? = null
)

data class Total(
    var value: Int? = null,
    var relation: String? = null
)


data class Shards(
    var total: Int? = null,
    var successful: Int? = null,
    var skipped: Int? = null,
    var failed: Int? = null
)

/*********************** esError ********************************/

data class EsError(
        val error: Error,
        val status: Int
)

data class Error(
        val failed_shards: List<FailedShard>,
        val grouped: Boolean,
        val phase: String,
        val reason: String,
        val root_cause: List<RootCause>,
        val type: String
)

data class FailedShard(
        val index: String,
        val node: String,
        val reason: Reason,
        val shard: Int
)

data class Reason(
        val index: String,
        val index_uuid: String,
        val reason: String,
        val type: String
)

data class RootCause(
        val index: String,
        val index_uuid: String,
        val reason: String,
        val type: String
)

/*********************** add resonse ********************************/
data class EsResponseAdd(
    val _id: String? = null,
    val _index: String? = null,
    val _primary_term: Int? = null,
    val _seq_no: Int? = null,
    val _shards: Shards2? = null,
    val _type: String? = null,
    val _version: Int? = null,
    val result: String? = null
)

data class Shards2(
    val failed: Int? = null,
    val successful: Int? = null,
    val total: Int? = null
)

/*********************** delete resonse ********************************/
data class EsResponseDel(
    val _id: String,
    val _index: String,
    val _primary_term: Int,
    val _seq_no: Int,
    val _shards: Shards3,
    val _type: String,
    val _version: Int,
    val result: String
)

data class Shards3(
    val failed: Int,
    val successful: Int,
    val total: Int
)

/*********************** sttSystemManage ********************************/
@IgnoreExtraProperties
data class SystemManageSource(
        @SerializedName("_index") @Expose var index: String? = null,
        @SerializedName("_type") @Expose var type: String? = null,
        @SerializedName("_id") @Expose var id: String? = null,
        @SerializedName("_score") @Expose var score: Float? = null,
        // ID로 받아올 때
        @SerializedName("_version") @Expose var version: Int,
        @SerializedName("_seq_no") @Expose var seq_no: Int,
        @SerializedName("_primary_term") @Expose var primary_term: Int,
        @SerializedName("fount") @Expose var found: Boolean,

        @SerializedName("_source") @Expose var source: SystemMan? = null
)

/*********************** sttPeer ********************************/
@IgnoreExtraProperties
data class PeerObject(
    @SerializedName("took") @Expose var took: Int? = null,
    @SerializedName("timed_out") @Expose var timedOut: Boolean? = null,
    @SerializedName("_shards") @Expose var shards: Shards? = null,
    @SerializedName("hits") @Expose var hits: PeerHitsObject? = null
)

@IgnoreExtraProperties
data class PeerHitsObject(
    @SerializedName("total") @Expose var total: Total? = null,
    @SerializedName("max_score") @Expose var maxScore: Float? = null,
    @SerializedName("hits") @Expose var hits: List<PeerSource>? = null
)

@IgnoreExtraProperties
data class PeerSource(
    @SerializedName("_index") @Expose var index: String? = null,
    @SerializedName("_type") @Expose var type: String? = null,
    @SerializedName("_id") @Expose var id: String? = null,
    @SerializedName("_score") @Expose var score: Float? = null,
    // ID로 받아올 때
    @SerializedName("_version") @Expose var version: Int,
    @SerializedName("_seq_no") @Expose var seq_no: Int,
    @SerializedName("_primary_term") @Expose var primary_term: Int,
    @SerializedName("fount") @Expose var found: Boolean,

    @SerializedName("_source") @Expose var source: Peer? = null
)


/*********************** sttUidDate ********************************/
@IgnoreExtraProperties
data class UidDateObject(
    @SerializedName("took") @Expose var took: Int? = null,
    @SerializedName("timed_out") @Expose var timedOut: Boolean? = null,
    @SerializedName("_shards") @Expose var shards: Shards? = null,
    @SerializedName("hits") @Expose var hits: UidDateHitsObject? = null
)

@IgnoreExtraProperties
data class UidDateHitsObject(
    @SerializedName("total") @Expose var total: Total? = null,
    @SerializedName("max_score") @Expose var maxScore: Float? = null,
    @SerializedName("hits") @Expose var hits: List<UidDateSource>? = null
)

@IgnoreExtraProperties
data class UidDateSource(
    @SerializedName("_index") @Expose var index: String? = null,
    @SerializedName("_type") @Expose var type: String? = null,
    @SerializedName("_id") @Expose var id: String? = null,
    @SerializedName("_score") @Expose var score: Float? = null,
    // ID로 받아올 때
    @SerializedName("_version") @Expose var version: Int,
    @SerializedName("_seq_no") @Expose var seq_no: Int,
    @SerializedName("_primary_term") @Expose var primary_term: Int,
    @SerializedName("fount") @Expose var found: Boolean,

    @SerializedName("_source") @Expose var source: UidDate? = null
)


/*********************** sttDevice ********************************/
@IgnoreExtraProperties
data class DeviceObject(
    @SerializedName("took") @Expose var took: Int? = null,
    @SerializedName("timed_out") @Expose var timedOut: Boolean? = null,
    @SerializedName("_shards") @Expose var shards: Shards? = null,
    @SerializedName("hits") @Expose var hits: DeviceHitsObject? = null
)

@IgnoreExtraProperties
data class DeviceHitsObject(
    @SerializedName("total") @Expose var total: Total? = null,
    @SerializedName("max_score") @Expose var maxScore: Float? = null,
    @SerializedName("hits") @Expose var hits: List<DeviceSource>? = null
)

@IgnoreExtraProperties
data class DeviceSource(
    @SerializedName("_index") @Expose var index: String? = null,
    @SerializedName("_type") @Expose var type: String? = null,
    @SerializedName("_id") @Expose var id: String? = null,
    @SerializedName("_score") @Expose var score: Float? = null,
    // ID로 받아올 때
    @SerializedName("_version") @Expose var version: Int,
    @SerializedName("_seq_no") @Expose var seq_no: Int,
    @SerializedName("_primary_term") @Expose var primary_term: Int,
    @SerializedName("fount") @Expose var found: Boolean,

    @SerializedName("_source") @Expose var source: Device? = null
)


/*********************** sttMiniPeer ********************************/
@IgnoreExtraProperties
data class MiniPeerObject(
    @SerializedName("took") @Expose var took: Int? = null,
    @SerializedName("timed_out") @Expose var timedOut: Boolean? = null,
    @SerializedName("_shards") @Expose var shards: Shards? = null,
    @SerializedName("hits") @Expose var hits: MiniPeerHitsObject? = null
)

@IgnoreExtraProperties
data class MiniPeerHitsObject(
    @SerializedName("total") @Expose var total: Total? = null,
    @SerializedName("max_score") @Expose var maxScore: Float? = null,
    @SerializedName("hits") @Expose var hits: List<MiniPeerSource>? = null
)

@IgnoreExtraProperties
data class MiniPeerSource(
    @SerializedName("_index") @Expose var index: String? = null,
    @SerializedName("_type") @Expose var type: String? = null,
    @SerializedName("_id") @Expose var id: String? = null,
    @SerializedName("_score") @Expose var score: Float? = null,
    // ID로 받아올 때
    @SerializedName("_version") @Expose var version: Int,
    @SerializedName("_seq_no") @Expose var seq_no: Int,
    @SerializedName("_primary_term") @Expose var primary_term: Int,
    @SerializedName("fount") @Expose var found: Boolean,

    @SerializedName("_source") @Expose var source: PeerAllow? = null
)


/*********************** sttBoard ********************************/
@IgnoreExtraProperties
data class BoardObject(
    @SerializedName("took") @Expose var took: Int? = null,
    @SerializedName("timed_out") @Expose var timedOut: Boolean? = null,
    @SerializedName("_shards") @Expose var shards: Shards? = null,
    @SerializedName("hits") @Expose var hits: BoardHitsObject? = null
)

@IgnoreExtraProperties
data class BoardHitsObject(
    @SerializedName("total") @Expose var total: Total? = null,
    @SerializedName("maxScore") @Expose var maxScore: Float? = null,
    @SerializedName("hits") @Expose var hits: List<BoardSource>? = null
)

@IgnoreExtraProperties
data class BoardSource(
    @SerializedName("_index") @Expose var index: String? = null,
    @SerializedName("_type") @Expose var type: String? = null,
    @SerializedName("_id") @Expose var id: String? = null,
    @SerializedName("_score") @Expose var score: Float? = null,

    // ID로 받아올 때
    @SerializedName("_version") @Expose var version: Int,
    @SerializedName("_seq_no") @Expose var seq_no: Int,
    @SerializedName("_primary_term") @Expose var primary_term: Int,
    @SerializedName("fount") @Expose var found: Boolean,

    @SerializedName("_source") @Expose var source: Board? = null
)


/*********************** sttPost ********************************/
@IgnoreExtraProperties
data class PostObject(
    @SerializedName("took") @Expose var took: Int? = null,
    @SerializedName("timed_out") @Expose var timedOut: Boolean? = null,
    @SerializedName("_shards") @Expose var shards: Shards? = null,
    @SerializedName("hits") @Expose var hits: PostHitsObject? = null
)


@IgnoreExtraProperties
data class PostHitsObject(
    @SerializedName("total") @Expose var total: Total? = null,
    @SerializedName("max_score") @Expose var maxScore: Float? = null,
    @SerializedName("hits") @Expose var hits: List<PostSource>? = null
)


@IgnoreExtraProperties
data class PostSource(
    @SerializedName("_index") @Expose var index: String? = null,
    @SerializedName("_type") @Expose var type: String? = null,
    @SerializedName("_id") @Expose var id: String? = null,
    @SerializedName("_score") @Expose var score: Float? = null,
    // ID로 받아올 때
    @SerializedName("_version") @Expose var version: Int,
    @SerializedName("_seq_no") @Expose var seq_no: Int,
    @SerializedName("_primary_term") @Expose var primary_term: Int,
    @SerializedName("fount") @Expose var found: Boolean,

    @SerializedName("_source") @Expose var source: Post? = null
)

/*********************** sttFlea ********************************/
@IgnoreExtraProperties
data class FleaObject(
    @SerializedName("took") @Expose var took: Int? = null,
    @SerializedName("timed_out") @Expose var timedOut: Boolean? = null,
    @SerializedName("_shards") @Expose var shards: Shards? = null,
    @SerializedName("hits") @Expose var hits: FleaHitsObject? = null
)


@IgnoreExtraProperties
data class FleaHitsObject(
    @SerializedName("total") @Expose var total: Total? = null,
    @SerializedName("max_score") @Expose var maxScore: Float? = null,
    @SerializedName("hits") @Expose var hits: List<FleaSource>? = null
)


@IgnoreExtraProperties
data class FleaSource(
    @SerializedName("_index") @Expose var index: String? = null,
    @SerializedName("_type") @Expose var type: String? = null,
    @SerializedName("_id") @Expose var id: String? = null,
    @SerializedName("_score") @Expose var score: Float? = null,
    // ID로 받아올 때
    @SerializedName("_version") @Expose var version: Int,
    @SerializedName("_seq_no") @Expose var seq_no: Int,
    @SerializedName("_primary_term") @Expose var primary_term: Int,
    @SerializedName("fount") @Expose var found: Boolean,

    @SerializedName("_source") @Expose var source: Flea? = null
)

/*********************** sttRoom ********************************/
@IgnoreExtraProperties
data class RoomObject(
    @SerializedName("took") @Expose var took: Int? = null,
    @SerializedName("timed_out") @Expose var timedOut: Boolean? = null,
    @SerializedName("_shards") @Expose var shards: Shards? = null,
    @SerializedName("hits") @Expose var hits: RoomHitsObject? = null
)


@IgnoreExtraProperties
data class RoomHitsObject(
    @SerializedName("total") @Expose var total: Total? = null,
    @SerializedName("max_score") @Expose var maxScore: Float? = null,
    @SerializedName("hits") @Expose var hits: List<RoomSource>? = null
)


@IgnoreExtraProperties
data class RoomSource(
    @SerializedName("_index") @Expose var index: String? = null,
    @SerializedName("_type") @Expose var type: String? = null,
    @SerializedName("_id") @Expose var id: String? = null,
    @SerializedName("_score") @Expose var score: Float? = null,
    // ID로 받아올 때
    @SerializedName("_version") @Expose var version: Int,
    @SerializedName("_seq_no") @Expose var seq_no: Int,
    @SerializedName("_primary_term") @Expose var primary_term: Int,
    @SerializedName("fount") @Expose var found: Boolean,

    @SerializedName("_source") @Expose var source: Room? = null
)

/*********************** sttReply ********************************/
@IgnoreExtraProperties
data class ReplyObject(
    @SerializedName("took") @Expose var took: Int? = null,
    @SerializedName("timed_out") @Expose var timedOut: Boolean? = null,
    @SerializedName("_shards") @Expose var shards: Shards? = null,
    @SerializedName("hits") @Expose var hits: ReplyHitsObject? = null
)


@IgnoreExtraProperties
data class ReplyHitsObject(
    @SerializedName("total") @Expose var total: Total? = null,
    @SerializedName("max_score") @Expose var maxScore: Float? = null,
    @SerializedName("hits") @Expose var hits: List<ReplySource>? = null
)


@IgnoreExtraProperties
data class ReplySource(
    @SerializedName("_index") @Expose var index: String? = null,
    @SerializedName("_type") @Expose var type: String? = null,
    @SerializedName("_id") @Expose var id: String? = null,
    @SerializedName("_score") @Expose var score: Float? = null,
    // ID로 받아올 때
    @SerializedName("_version") @Expose var version: Int,
    @SerializedName("_seq_no") @Expose var seq_no: Int,
    @SerializedName("_primary_term") @Expose var primary_term: Int,
    @SerializedName("fount") @Expose var found: Boolean,

    @SerializedName("_source") @Expose var source: Reply? = null
)


/*********************** sttLobby ********************************/
@IgnoreExtraProperties
data class LobbyObject(
    @SerializedName("took") @Expose var took: Int? = null,
    @SerializedName("timed_out") @Expose var timedOut: Boolean? = null,
    @SerializedName("_shards") @Expose var shards: Shards? = null,
    @SerializedName("hits") @Expose var hits: LobbyHitsObject? = null
)


@IgnoreExtraProperties
data class LobbyHitsObject(
    @SerializedName("total") @Expose var total: Total? = null,
    @SerializedName("max_score") @Expose var maxScore: Float? = null,
    @SerializedName("hits") @Expose var hits: List<LobbySource>? = null
)


@IgnoreExtraProperties
data class LobbySource(
    @SerializedName("_index") @Expose var index: String? = null,
    @SerializedName("_type") @Expose var type: String? = null,
    @SerializedName("_id") @Expose var id: String? = null,
    @SerializedName("_score") @Expose var score: Float? = null,
    // ID로 받아올 때
    @SerializedName("_version") @Expose var version: Int,
    @SerializedName("_seq_no") @Expose var seq_no: Int,
    @SerializedName("_primary_term") @Expose var primary_term: Int,
    @SerializedName("fount") @Expose var found: Boolean,

    @SerializedName("_source") @Expose var source: Lobby? = null
)


/*********************** sttStore ********************************/
@IgnoreExtraProperties
data class StoreObject(
    @SerializedName("took") @Expose var took: Int? = null,
    @SerializedName("timed_out") @Expose var timedOut: Boolean? = null,
    @SerializedName("_shards") @Expose var shards: Shards? = null,
    @SerializedName("hits") @Expose var hits: StoreHitsObject? = null
)


@IgnoreExtraProperties
data class StoreHitsObject(
    @SerializedName("total") @Expose var total: Total? = null,
    @SerializedName("max_score") @Expose var maxScore: Float? = null,
    @SerializedName("hits") @Expose var hits: List<StoreSource>? = null
)


@IgnoreExtraProperties
data class StoreSource(
    @SerializedName("_index") @Expose var index: String? = null,
    @SerializedName("_type") @Expose var type: String? = null,
    @SerializedName("_id") @Expose var id: String? = null,
    @SerializedName("_score") @Expose var score: Float? = null,
    // ID로 받아올 때
    @SerializedName("_version") @Expose var version: Int,
    @SerializedName("_seq_no") @Expose var seq_no: Int,
    @SerializedName("_primary_term") @Expose var primary_term: Int,
    @SerializedName("fount") @Expose var found: Boolean,

    @SerializedName("_source") @Expose var source: Store? = null
)

/*********************** sttStoreMenu ********************************/
@IgnoreExtraProperties
data class StoreMenuObject(
    @SerializedName("took") @Expose var took: Int? = null,
    @SerializedName("timed_out") @Expose var timedOut: Boolean? = null,
    @SerializedName("_shards") @Expose var shards: Shards? = null,
    @SerializedName("hits") @Expose var hits: StoreMenuHitsObject? = null
)


@IgnoreExtraProperties
data class StoreMenuHitsObject(
    @SerializedName("total") @Expose var total: Total? = null,
    @SerializedName("max_score") @Expose var maxScore: Float? = null,
    @SerializedName("hits") @Expose var hits: List<StoreMenuSource>? = null
)


@IgnoreExtraProperties
data class StoreMenuSource(
    @SerializedName("_index") @Expose var index: String? = null,
    @SerializedName("_type") @Expose var type: String? = null,
    @SerializedName("_id") @Expose var id: String? = null,
    @SerializedName("_score") @Expose var score: Float? = null,
    // ID로 받아올 때
    @SerializedName("_version") @Expose var version: Int,
    @SerializedName("_seq_no") @Expose var seq_no: Int,
    @SerializedName("_primary_term") @Expose var primary_term: Int,
    @SerializedName("fount") @Expose var found: Boolean,

    @SerializedName("_source") @Expose var source: StoreMenu? = null
)


/*********************** sttCount ********************************/
@IgnoreExtraProperties
data class CountObject(
    @SerializedName("took") @Expose var took: Int? = null,
    @SerializedName("timed_out") @Expose var timedOut: Boolean? = null,
    @SerializedName("_shards") @Expose var shards: Shards? = null,
    @SerializedName("hits") @Expose var hits: CountHitsObject? = null
)

@IgnoreExtraProperties
data class CountHitsObject(
    @SerializedName("total") @Expose var total: Total? = null,
    @SerializedName("max_score") @Expose var maxScore: Float? = null,
    @SerializedName("hits") @Expose var hits: List<CountSource>? = null
)

@IgnoreExtraProperties
data class CountSource(
    @SerializedName("_index") @Expose var index: String? = null,
    @SerializedName("_type") @Expose var type: String? = null,
    @SerializedName("_id") @Expose var id: String? = null,
    @SerializedName("_score") @Expose var score: Float? = null,
    // ID로 받아올 때
    @SerializedName("_version") @Expose var version: Int,
    @SerializedName("_seq_no") @Expose var seq_no: Int,
    @SerializedName("_primary_term") @Expose var primary_term: Int,
    @SerializedName("fount") @Expose var found: Boolean,

    @SerializedName("_source") @Expose var source: Count? = null
)

/*********************** sttWarn ********************************/
@IgnoreExtraProperties
data class WarnObject(
    @SerializedName("took") @Expose var took: Int? = null,
    @SerializedName("timed_out") @Expose var timedOut: Boolean? = null,
    @SerializedName("_shards") @Expose var shards: Shards? = null,
    @SerializedName("hits") @Expose var hits: WarnHitsObject? = null
)


@IgnoreExtraProperties
data class WarnHitsObject(
    @SerializedName("total") @Expose var total: Total? = null,
    @SerializedName("max_score") @Expose var maxScore: Float? = null,
    @SerializedName("hits") @Expose var hits: List<WarnSource>? = null
)


@IgnoreExtraProperties
data class WarnSource(
    @SerializedName("_index") @Expose var index: String? = null,
    @SerializedName("_type") @Expose var type: String? = null,
    @SerializedName("_id") @Expose var id: String? = null,
    @SerializedName("_score") @Expose var score: Float? = null,
    // ID로 받아올 때
    @SerializedName("_version") @Expose var version: Int,
    @SerializedName("_seq_no") @Expose var seq_no: Int,
    @SerializedName("_primary_term") @Expose var primary_term: Int,
    @SerializedName("fount") @Expose var found: Boolean,

    @SerializedName("_source") @Expose var source: Warn? = null
)

/*********************** sttCost ********************************/
@IgnoreExtraProperties
data class CostObject(
    @SerializedName("took") @Expose var took: Int? = null,
    @SerializedName("timed_out") @Expose var timedOut: Boolean? = null,
    @SerializedName("_shards") @Expose var shards: Shards? = null,
    @SerializedName("hits") @Expose var hits: CostHitsObject? = null
)

@IgnoreExtraProperties
data class CostHitsObject(
    @SerializedName("total") @Expose var total: Total? = null,
    @SerializedName("max_score") @Expose var maxScore: Float? = null,
    @SerializedName("hits") @Expose var hits: List<CostSource>? = null
)


@IgnoreExtraProperties
data class CostSource(
    @SerializedName("_index") @Expose var index: String? = null,
    @SerializedName("_type") @Expose var type: String? = null,
    @SerializedName("_id") @Expose var id: String? = null,
    @SerializedName("_score") @Expose var score: Float? = null,
    // ID로 받아올 때
    @SerializedName("_version") @Expose var version: Int,
    @SerializedName("_seq_no") @Expose var seq_no: Int,
    @SerializedName("_primary_term") @Expose var primary_term: Int,
    @SerializedName("fount") @Expose var found: Boolean,

    @SerializedName("_source") @Expose var source: Cost? = null
)


/*********************** sttTag ********************************/
@IgnoreExtraProperties
data class TagObject(
    @SerializedName("took") @Expose var took: Int? = null,
    @SerializedName("timed_out") @Expose var timedOut: Boolean? = null,
    @SerializedName("_shards") @Expose var shards: Shards? = null,
    @SerializedName("hits") @Expose var hits: TagHitsObject? = null
)

@IgnoreExtraProperties
data class TagHitsObject(
    @SerializedName("total") @Expose var total: Total? = null,
    @SerializedName("max_score") @Expose var maxScore: Float? = null,
    @SerializedName("hits") @Expose var hits: List<TagSource>? = null
)


@IgnoreExtraProperties
data class TagSource(
    @SerializedName("_index") @Expose var index: String? = null,
    @SerializedName("_type") @Expose var type: String? = null,
    @SerializedName("_id") @Expose var id: String? = null,
    @SerializedName("_score") @Expose var score: Float? = null,
    // ID로 받아올 때
    @SerializedName("_version") @Expose var version: Int,
    @SerializedName("_seq_no") @Expose var seq_no: Int,
    @SerializedName("_primary_term") @Expose var primary_term: Int,
    @SerializedName("fount") @Expose var found: Boolean,

    @SerializedName("_source") @Expose var source: MyTag? = null
)


/*********************** sttBid ********************************/
@IgnoreExtraProperties
data class BidObject(
    @SerializedName("took") @Expose var took: Int? = null,
    @SerializedName("timed_out") @Expose var timedOut: Boolean? = null,
    @SerializedName("_shards") @Expose var shards: Shards? = null,
    @SerializedName("hits") @Expose var hits: BidHitsObject? = null
)

@IgnoreExtraProperties
data class BidHitsObject(
    @SerializedName("total") @Expose var total: Total? = null,
    @SerializedName("max_score") @Expose var maxScore: Float? = null,
    @SerializedName("hits") @Expose var hits: List<BidSource>? = null
)


@IgnoreExtraProperties
data class BidSource(
    @SerializedName("_index") @Expose var index: String? = null,
    @SerializedName("_type") @Expose var type: String? = null,
    @SerializedName("_id") @Expose var id: String? = null,
    @SerializedName("_score") @Expose var score: Float? = null,
    // ID로 받아올 때
    @SerializedName("_version") @Expose var version: Int,
    @SerializedName("_seq_no") @Expose var seq_no: Int,
    @SerializedName("_primary_term") @Expose var primary_term: Int,
    @SerializedName("fount") @Expose var found: Boolean,

    @SerializedName("_source") @Expose var source: Bid? = null
)

/*********************** sttTicket ********************************/
@IgnoreExtraProperties
data class TicketObject(
    @SerializedName("took") @Expose var took: Int? = null,
    @SerializedName("timed_out") @Expose var timedOut: Boolean? = null,
    @SerializedName("_shards") @Expose var shards: Shards? = null,
    @SerializedName("hits") @Expose var hits: TicketHitsObject? = null
)

@IgnoreExtraProperties
data class TicketHitsObject(
    @SerializedName("total") @Expose var total: Total? = null,
    @SerializedName("max_score") @Expose var maxScore: Float? = null,
    @SerializedName("hits") @Expose var hits: List<TicketSource>? = null
)


@IgnoreExtraProperties
data class TicketSource(
    @SerializedName("_index") @Expose var index: String? = null,
    @SerializedName("_type") @Expose var type: String? = null,
    @SerializedName("_id") @Expose var id: String? = null,
    @SerializedName("_score") @Expose var score: Float? = null,
    // ID로 받아올 때
    @SerializedName("_version") @Expose var version: Int,
    @SerializedName("_seq_no") @Expose var seq_no: Int,
    @SerializedName("_primary_term") @Expose var primary_term: Int,
    @SerializedName("fount") @Expose var found: Boolean,

    @SerializedName("_source") @Expose var source: Ticket? = null
)

/*********************** sttGrant ********************************/
@IgnoreExtraProperties
data class GrantObject(
    @SerializedName("took") @Expose var took: Int? = null,
    @SerializedName("timed_out") @Expose var timedOut: Boolean? = null,
    @SerializedName("_shards") @Expose var shards: Shards? = null,
    @SerializedName("hits") @Expose var hits: GrantHitsObject? = null
)

@IgnoreExtraProperties
data class GrantHitsObject(
    @SerializedName("total") @Expose var total: Total? = null,
    @SerializedName("max_score") @Expose var maxScore: Float? = null,
    @SerializedName("hits") @Expose var hits: List<GrantSource>? = null
)


@IgnoreExtraProperties
data class GrantSource(
    @SerializedName("_index") @Expose var index: String? = null,
    @SerializedName("_type") @Expose var type: String? = null,
    @SerializedName("_id") @Expose var id: String? = null,
    @SerializedName("_score") @Expose var score: Float? = null,
    // ID로 받아올 때
    @SerializedName("_version") @Expose var version: Int,
    @SerializedName("_seq_no") @Expose var seq_no: Int,
    @SerializedName("_primary_term") @Expose var primary_term: Int,
    @SerializedName("fount") @Expose var found: Boolean,

    @SerializedName("_source") @Expose var source: Grant? = null
)
