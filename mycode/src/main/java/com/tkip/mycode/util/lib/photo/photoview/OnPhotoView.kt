package com.tkip.mycode.util.lib.photo.photoview

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.view.View
import com.tkip.mycode.init.PUT_ARRAY_FOTO
import com.tkip.mycode.init.PUT_ARRAY_MYPHOTO
import com.tkip.mycode.init.PUT_INT
import com.tkip.mycode.util.lib.TransitionHelper
import com.tkip.mycode.util.lib.photo.Foto
import com.tkip.mycode.util.lib.photo.MyPhoto


class OnPhotoView {

    companion object {

        private fun getIntent(context: Context) =
                Intent(context, PhotoViewActivity::class.java).apply {
                    addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
                }

        fun gotoPhotoViewActivity(context: Context, thumbUrl: String?, url: String?, arrPair: Array<androidx.core.util.Pair<View, String>>?) {
            val list = arrayListOf<Foto>().apply { add(Foto(thumbUrl, url)) }
           val intent =  getIntent(context).putParcelableArrayListExtra(PUT_ARRAY_FOTO, list)

            TransitionHelper.startTran(context, intent, arrPair)
        }

        fun gotoPhotoViewActivity(context: Context, foto: Foto, arrPair: Array<androidx.core.util.Pair<View, String>>?) {
//            val intent = Intent(context, PhotoViewActivity::class.java)
//            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
            val list = arrayListOf<Foto>().apply { add(foto) }
           val intent =  getIntent(context).putParcelableArrayListExtra(PUT_ARRAY_FOTO, list)

            TransitionHelper.startTran(context, intent, arrPair)
        }

        fun gotoPhotoViewActivity(context: Context, position: Int, fotoList: ArrayList<Foto>, arrPair: Array<androidx.core.util.Pair<View, String>>?) {
            val intent = Intent(context, PhotoViewActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
            intent.putExtra(PUT_INT, position)
            intent.putParcelableArrayListExtra(PUT_ARRAY_FOTO, fotoList)

            TransitionHelper.startTran(context, intent, arrPair)
        }

        fun gotoPhtoViewActivityMyPhoto(activity: Activity, position: Int, fotoList: ArrayList<MyPhoto>, arrPair: Array<androidx.core.util.Pair<View, String>>?) {
            val intent = Intent(activity, PhotoViewActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
            intent.putExtra(PUT_INT, position)
            intent.putParcelableArrayListExtra(PUT_ARRAY_MYPHOTO, fotoList)

            TransitionHelper.startTran(activity, intent, arrPair)
        }


    }

}