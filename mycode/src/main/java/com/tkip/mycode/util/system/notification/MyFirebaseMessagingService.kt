package com.tkip.mycode.util.system.notification

import com.google.firebase.messaging.FirebaseMessagingService
import com.google.firebase.messaging.RemoteMessage
import com.tkip.mycode.funs.common.lllogD
import com.tkip.mycode.funs.common.lllogI
import com.tkip.mycode.init.base.Sred
import com.tkip.mycode.model.peer.myuid

class MyFirebaseMessagingService : FirebaseMessagingService() {


    override fun onNewToken(token: String) {
        super.onNewToken(token)
//        lllogI("MyFirebaseMessagingService fcm token $token")

            lllogD("MyFirebaseMessagingService token $token")
            Sred.putString(Sred.DEVICE_FCM_TOKEN, token)     // token 저장.
//            Sred.getObjectPeer(Sred.PEER)?.let { peer1 -> OnPeer.setUpPeer(peer1) }   // peer 세팅


    }

    override fun onMessageReceived(remoteMessage: RemoteMessage) {
        super.onMessageReceived(remoteMessage)
        lllogI("onMessageReceived enter")

//        NotiChannelManager.sendNotiDefault(
//                context = applicationContext,
//                id = System.currentTimeMillis().toInt(),
//                channel = "default",
//                noti = Noti(title = "푸시 테스트", body = "PUSH PUSH", clickAction = "com.tkip.mycode.TARGET_MainActivity"))





        Noti.create(remoteMessage).apply {

            /**  내가 보낸 push는 생성 안 함. */
            if (this.senderUid == myuid()) return

            startNoti(applicationContext )
        }






    }
}