package com.tkip.mycode.util.my_view

import android.content.Context
import android.graphics.drawable.Drawable
import android.net.Uri
import android.view.View
import android.widget.FrameLayout
import android.widget.LinearLayout
import android.widget.RadioButton
import com.google.android.gms.maps.model.LatLng
import com.tkip.mycode.admin.menu.AMenu
import com.tkip.mycode.funs.custom.*
import com.tkip.mycode.funs.custom.expandable.ViewExpandableCardAd
import com.tkip.mycode.funs.custom.menu.ViewMenuEditText
import com.tkip.mycode.funs.search.view.CustomSearchKeyword
import com.tkip.mycode.model.media.ViewMyMediaButton
import com.tkip.mycode.model.media.ViewMyMediaEditText
import com.tkip.mycode.model.my_enum.RGint
import com.tkip.mycode.model.my_enum.RGstring
import com.tkip.mycode.model.mytag.LinearAddTag
import com.tkip.mycode.model.mytag.TagTYPE
import com.tkip.mycode.model.reply.LinearReplyAddBox
import com.tkip.mycode.model.reply.Reply
import com.tkip.mycode.model.reply.ViewReplyEarlier
import com.tkip.mycode.nav.store.add.MyMedia
import com.tkip.mycode.util.tools.anim.AnimUtil
import com.tkip.mycode.util.tools.anim.showOverUp
import com.tkip.mycode.util.tools.radio.CustomRGbasic
import com.tkip.mycode.util.tools.radio.ViewRGint
import com.tkip.mycode.util.tools.radio.SubRGstring4Room

class ViewUtil {

    companion object {

        /******************************************
         * hide
         ******************************************/
        fun hideAllViews(vararg linear: LinearLayout) {
            for (li: LinearLayout in linear) {
                if (li.childCount > 0) {
                    AnimUtil.hideSlideRight(li.getChildAt(0), true)
                    li.removeAllViews()
                }
            }
        }

        fun hideViewChoiceBox(linear: LinearLayout) {
            if (linear.childCount > 0) {
                AnimUtil.hideSlideRight(linear.getChildAt(0), true)
                linear.removeAllViews()
            }
        }

        fun hideViewInfoStatus(linear: LinearLayout) {
            if (linear.childCount > 0) {
                AnimUtil.hideFadeOut(linear.getChildAt(0), true)
                linear.removeAllViews()
            }
        }


        /******************************************
         * show
         ******************************************/
        fun addViewChoiceBox(context: Context, parent: LinearLayout, icon: Drawable?, title: String?, description: String?, hasCancel: Boolean, btnText: String?, ok: () -> Unit, cancel: () -> Unit) {
//            this.listener = listener
            val view = ViewChoiceBox(context, icon, title, description, hasCancel, btnText, ok = { ok() }, cancel = { cancel() })

            parent.removeAllViews()
            parent.addView(view)

            AnimUtil.showSlideRight(view, true)
        }





        fun addViewSearchBar(context: Context, parent: LinearLayout, btnMenu: () -> Unit, search: (String) -> Unit, cancel: () -> Unit) {

            val view = ViewSearchBar(context, { btnMenu() }, { search(it) }, { cancel() })

            parent.removeAllViews()
            parent.addView(view)

            AnimUtil.showSlideDown(view, true)

        }

        fun addViewMyMediaEditText(context: Context, parent: LinearLayout, myMedia: MyMedia): View {
            val view = ViewMyMediaEditText(context, myMedia)
//            parent.removeAllViews()
            parent.addView(view)

            AnimUtil.showSlideDown(view, true)
            return view
        }


        fun addViewMyMediaButton(context: Context, parent: LinearLayout, myMedia: MyMedia, mediaId: String, onClick: (MyMedia) -> Unit) {
            val view = ViewMyMediaButton(context, myMedia, mediaId, onClick = { onClick(it) })
//            parent.removeAllViews()
            parent.addView(view)

            AnimUtil.showSlideDown(view, true)
        }


        fun addViewMenuEditText(context: Context, parent: LinearLayout, minus: (ViewMenuEditText) -> Unit): View {
            val view = ViewMenuEditText(context, minus)

//            parent.removeAllViews()
            parent.addView(view)

            AnimUtil.showSlideDown(view, true)

            return view
        }

        /**
         * reply
         */
        fun addViewReplyEarlier(context: Context, parent: LinearLayout, reply: Reply, onClick: (Reply) -> Unit) {
            val view = ViewReplyEarlier(context, reply, onClick)
            parent.removeAllViews()
            parent.addView(view)
            AnimUtil.showSlideUp(view, true)
        }


        fun addViewPhotoUri(context: Context, parent: LinearLayout, uri: Uri, onPhoto: () -> Unit, onClose: () -> Unit) {
            val view = ViewPhotoUri(context, uri, onPhoto, onClose)
            parent.removeAllViews()
            parent.addView(view)
            parent.showOverUp(true)
        }

//        fun addViewAuthorTitle(context: Context, parent: FrameLayout, anonymous: Boolean, uid: String, addDate: Long, onPeer: (Peer?) -> Unit) =
//                ViewAuthorTitle(context, anonymous, uid, addDate, onPeer).apply {
//                    parent.addView(this)
//                }
//
//        fun addViewToolbar(context: Context, parent: FrameLayout, boardTitle: String, title: String?, addDate: Long, onBack: () -> Unit, onBoardTitle: () -> Unit, onMore: () -> Unit) =
//                ViewToolbar(context, boardTitle, title, addDate, onBack, onBoardTitle, onMore).apply {
//                    parent.addView(this)
//                }

//        fun addViewToolbarAdd(context: Context, parent: FrameLayout, boardName: String?, addName: String, bookmarkCount: Int?, pushCount: Int?, onBack: () -> Unit, onBookmark: (View) -> Unit, onPush: (View) -> Unit, onAdd: (View) -> Unit, onMore: (View) -> Unit) =
//                ViewToolbarAdd(context, boardName, addName, bookmarkCount, pushCount, onBack, onBookmark, onPush, onAdd, onMore).apply {
//                    parent.addView(this)
//                }

//        fun addLinearAddPhotoBox(context: Context, parent: FrameLayout, onOpenAlbum: () -> Unit): LinearAddPhotoBox =
//                LinearAddPhotoBox(context, onOpenAlbum).apply {
//                    parent.addView(this)
//                }

//        fun addLinearAddPhotoOne(context: Context, parent: FrameLayout, thumbUrl: String?, onDelete: () -> Unit, onGallery: () -> Unit): LinearAddPhotoOne =
//                LinearAddPhotoOne(context, thumbUrl, onDelete, onGallery).apply {
//                    parent.addView(this)
//                }

        fun addLinearReplyAddBox(context: Context, parent: FrameLayout, allowReply: Boolean, modelKey: String, onClickAddReply: () -> Unit) =
                LinearReplyAddBox(context, allowReply, modelKey, onClickAddReply).apply {
                    parent.addView(this)
                }

        fun addLinearMapAdd(context: Context, parent: LinearLayout, mode: Int?, address: String?, latlng: LatLng?) =
                LinearMapAddBox(context, mode, address, latlng).apply {
                    parent.addView(this)
                }

        fun addLinearMapShow(context: Context, parent: LinearLayout, address: String?, latlng: LatLng?) =
                LinearMapBox(context, address, latlng).apply {
                    parent.addView(this)
                }

        fun addLinearAddTag(context: Context, parent: LinearLayout, tagType: TagTYPE, title: String?, btnName: String, list: ArrayList<String>?) =
                LinearAddTag(context, tagType, title, btnName, list).apply {
                    parent.addView(this)
                }

        /*********************************************************************************
         ************************************* sttSearch **********************************
         *********************************************************************************/

        fun addViewSearchKeyword(context: Context, parent: FrameLayout, onSearch: (String) -> Unit, onLoadMore: (String) -> Unit) =
                CustomSearchKeyword(context, onSearch, onLoadMore).apply {
                    parent.removeAllViews()
                    parent.addView(this)
                }


        /******************************************************************************
         ****************************** menu ******************************************
         ******************************************************************************/

        fun addViewButtonLight(context: Context, parent: LinearLayout, amenu: AMenu, onClick: (AMenu) -> Unit) {
            val view = ViewButtonLight(context, amenu, onClick)
//            parent.removeAllViews()
            parent.addView(view)
            AnimUtil.showNormal(view)
        }

        fun addViewRGint(context: Context, parent: LinearLayout, rgInt: RGint, onMore: (RadioButton) -> Unit) {
            val view = ViewRGint(context, rgInt, onMore)
            parent.addView(view)
            AnimUtil.showNormal(view)
        }

        fun addViewRGstring(context: Context, parent: LinearLayout, rgString: RGstring) =
                SubRGstring4Room(context, rgString).apply {
                    parent.addView(this)
                    AnimUtil.showNormal(this)
                }

//        fun addViewRGbasic(context: Context, parent: FrameLayout, list: ArrayList<String>, onSelect: (Int) -> Unit) =
//                ViewRGbasic(context, list, onSelect).apply {
//                    parent.addView(this)
//                    AnimUtil.showNormal(this)
//                }

        fun addViewExpandableCardAd(context: Context, title: String, parent: LinearLayout) =
                ViewExpandableCardAd(context, title).apply {
                    parent.addView(this)
//                    AnimUtil.showNormal(this)
                }

        fun viewRGbasic(context: Context, parent: LinearLayout, list: ArrayList<String>, typeBasic: Boolean, onSelected: (Int) -> Unit) =
                CustomRGbasic(context, list, typeBasic, onSelected)
                        .apply {
                            parent.addView(this)
                            AnimUtil.showNormal(this)
                        }


    }

}