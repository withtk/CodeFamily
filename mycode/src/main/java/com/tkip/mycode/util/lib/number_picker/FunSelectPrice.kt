package com.tkip.mycode.util.lib.number_picker

import android.content.Context
import com.tkip.mycode.R
import com.tkip.mycode.databinding.AlertSelectPriceBinding
import com.tkip.mycode.funs.common.getInt
import com.tkip.mycode.funs.custom.number.ViewNumberBtn
import com.tkip.mycode.init.base.getStr
import com.tkip.mycode.init.base.getStringComma
import com.tkip.mycode.model.flea.Flea
import com.tkip.mycode.model.room.Room
import com.tkip.mycode.util.tools.anim.*


/********************************************************************************************
 * 금액 실시간 기입.
 */
fun AlertSelectPriceBinding.write(digit: Int?) {

    tvInfo.hideFadeOut(false)

    /*** 초기화 */
    if (digit == null) tvPrice.text = "0"
    else tvPrice.text = (tvPrice.getInt() + digit).getStringComma()

//    tvPrice.showFadeIn(true)
    tvPrice.showOverUp(true)
}

/**
 * 최소 금액 체크 및 안내.
 */
fun AlertSelectPriceBinding.underMin(min: Int): Boolean {
    return if (tvPrice.getInt() < min) {
        tvInfo.text = String.format(R.string.alert_select_price_min.getStr(), min.getStringComma())
        tvInfo.showFadeIn(true)
        true
    } else false
}


/*** Custom금액버튼 가져오기 */
fun AlertSelectPriceBinding.getNumberView(context: Context, any: Any): ViewNumberBtn =
        when (any) {
            is Flea -> ViewNumberBtn(context, info = null, onChunMaan = null, onBaekMaan = { write(it) }, onShipMaan = { write(it) }, onMaan = { write(it) }, onChun = { write(it) }, onBaek = { write(it) }, onShip = { write(it) }, onPlus1 = null, onMinus1 = null, onInit = { write(it) })
            is Room -> ViewNumberBtn(context, info = null, onChunMaan = { write(it) }, onBaekMaan = { write(it) }, onShipMaan = { write(it) }, onMaan = { write(it) }, onChun = { write(it) }, onBaek = null, onShip = null, onPlus1 = null, onMinus1 = null, onInit = { write(it) })
            else -> ViewNumberBtn(context, info = null, onChunMaan = null, onBaekMaan = { write(it) }, onShipMaan = { write(it) }, onMaan = { write(it) }, onChun = { write(it) }, onBaek = { write(it) }, onShip = { write(it) }, onPlus1 = null, onMinus1 = null, onInit = { write(it) })
        }
