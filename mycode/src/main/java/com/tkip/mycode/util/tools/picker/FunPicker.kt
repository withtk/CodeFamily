package com.tkip.mycode.util.tools.picker

import android.app.Activity
import android.app.DatePickerDialog
import android.content.Context
import android.view.LayoutInflater
import android.widget.Button
import android.widget.DatePicker
import android.widget.LinearLayout
import android.widget.NumberPicker
import androidx.core.view.children
import com.tkip.mycode.databinding.ViewNumberPickerBinding
import com.tkip.mycode.databinding.ViewTextviewCommaBinding
import com.tkip.mycode.funs.common.lllogI
import com.tkip.mycode.init.base.oneInt
import com.tkip.mycode.init.base.remove0String
import com.tkip.mycode.init.base.removeComma
import com.tkip.mycode.init.base.reverse
import com.tkip.mycode.model.my_enum.PickerFactory
import com.tkip.mycode.util.tools.anim.AnimUtil
import com.tkip.mycode.util.tools.anim.gone
import java.util.*






/**************************************************************************
 * 날짜 선택하는 피커.
 **************************************************************************/
fun showDatePicker(activity: Activity, onSelect: (v: DatePicker, y: Int, m: Int, d: Int) -> Unit) {
    val c = Calendar.getInstance()
    val mYear: Int = c.get(Calendar.YEAR)
    val mMonth: Int = c.get(Calendar.MONTH)
    val mDay: Int = c.get(Calendar.DAY_OF_MONTH)
    lllogI("showDatePicker today $mYear $mMonth $mDay")

    DatePickerDialog(activity, DatePickerDialog.OnDateSetListener { v, y, m, d ->
        lllogI("showDatePicker onSelect $y $m $d")
        onSelect(v, y, m, d)
    }, mYear, mMonth, mDay).show()
}


/**************************************************************************
 * 숫자 선택하는 피커.
 **************************************************************************/
/**
 * preValue의 null 여부에 따라 setup 후 setValue
 */
fun LinearLayout.setupNumberPicker(context: Context, pickerFactory: PickerFactory, preValue: String?) {
    if (preValue.isNullOrBlank()) {
        // 처음 셋팅
        (0 until pickerFactory.initPicker).forEach { _ -> addNumberPicker(context, pickerFactory, 0) }
    } else {
        val value2 = preValue.removeComma().toInt()/pickerFactory.omitNumber  // 생략된 자릿수는 빼줘야.
        val digitString = value2.toString().removeComma().reverse()   // 모두 숫자만 남기고, 뒤집어.
        (digitString.indices).forEach {
            val digit = digitString.oneInt(it)   // 업데이트할 숫자 index에 맞춰서 하나만 가져와 줘.
            addNumberPicker(context, pickerFactory, digit)
        }
    }
}

/** * 기본 숫자 피커 */
fun LinearLayout.addNumberPicker(context: Context, pickerFactory: PickerFactory, value: Int) {
    val sequence = getPickerCount()
    val commaNum = pickerFactory.omitNumber.toString().length - 1
    val plus = sequence + commaNum
    if (plus == 3 || plus == 6) addTvComma()  // comma 추가

    val bb = ViewNumberPickerBinding.inflate(LayoutInflater.from(context))
    bb.numberPicker.minValue = pickerFactory.min
    bb.numberPicker.maxValue = pickerFactory.max
    bb.numberPicker.value = value
    bb.numberPicker.id = sequence
    addView(bb.numberPicker, 0)
}

/** * 기본 String 피커 */
fun LinearLayout.addNumberPickerString(context: Context, pickerFactory: PickerFactory, value: Int) {
    val sequence = getPickerCount()
    val commaNum = pickerFactory.omitNumber.toString().length - 1
    val plus = sequence + commaNum
    if (plus == 3 || plus == 6) addTvComma()  // comma 추가

    val bb = ViewNumberPickerBinding.inflate(LayoutInflater.from(context))
    bb.numberPicker.minValue = pickerFactory.min
    bb.numberPicker.maxValue = pickerFactory.max
    bb.numberPicker.value = value
    bb.numberPicker.id = sequence
    addView(bb.numberPicker, 0)
}


/**************************************************************************
 * 숫자 선택하는 피커. new
 **************************************************************************/




/** * ,000 picker */
/*
fun LinearLayout.addNumberPicker000(context: Context, commaDigit: Int, min: Int, max: Int, value: Int) {
    val sequence = getPickerCount()
    val bb = ViewNumberPickerBinding.inflate(LayoutInflater.from(context))
    bb.numberPicker.minValue = min
    bb.numberPicker.maxValue = max
//    bb.numberPicker.value = value
    bb.numberPicker.displayedValues = arrayOf("0,000", "1,000", "2,000", "3,000", "4,000", "5,000", "6,000", "7,000", "8,000", "9,000")
    bb.numberPicker.id = sequence
    bb.numberPicker.setPadding(30, 0, 0, 0)
    addView(bb.numberPicker, 0)
}
*/

private fun LinearLayout.addTvComma() {
    val tvCommaBinding = ViewTextviewCommaBinding.inflate(LayoutInflater.from(context))
    addView(tvCommaBinding.tv, 0)
}


/**
 * line이 갖고 있는 picker 수.
 */
fun LinearLayout.getPickerCount(): Int {
    var i = 0
    children.forEach { v ->
        if (v is NumberPicker) i++
    }
    return i
}

/**
 * 최종 선택한 숫자 가져오기.
 */
fun LinearLayout.getAllPickerValue(): Int {
    val buffer = StringBuffer()
    // child중에 numberPicker만 가져와
    children.forEach { v ->
        if (v is NumberPicker) buffer.append(v.value)  // 숫자를 모두 붙여.
    }
    lllogI("buffer $buffer ")
    return buffer.toString().remove0String().toInt()   // 제일 앞의 0을 제거하고.
}

/**
 * picker 5개 이상이면 버튼 제거.
 */
fun LinearLayout.toggleBtn(limit: Int, btn: Button) {
    if (getPickerCount() >= limit) btn.gone()
    else AnimUtil.showNormal(btn)
}

/**
 * 모든 picker 초기화
 */
//fun LinearLayout.initAllPicker(context: Context, commaNum: Int, min: Int, max: Int,  preValue: String?) {
//    removeAllViews()
//    setupNumberPicker(context, commaNum, min, max, preValue)
//    checkRemoveBtnByPicker(b.btnPlus)
//}
