package com.tkip.mycode.util.tools.radio

import android.content.Context
import android.view.LayoutInflater
import android.widget.LinearLayout
import android.widget.RadioButton
import androidx.databinding.DataBindingUtil
import com.tkip.mycode.R
import com.tkip.mycode.databinding.ViewRgIntBinding
import com.tkip.mycode.init.base.getStr
import com.tkip.mycode.model.my_enum.RGint

class ViewRGint : LinearLayout {
    private lateinit var b: ViewRgIntBinding

    private lateinit var radioEtc: RadioButton   // more 라디오.

    constructor(context: Context) : super(context)
    constructor(context: Context, rgInt: RGint, onMore: (RadioButton) -> Unit) : super(context) {

        b = DataBindingUtil.inflate(LayoutInflater.from(context), R.layout.view_rg_int, this, true)
        b.rgInt = rgInt

        /**  제목 클릭시 클리어    */
        b.tvName.setOnClickListener {
            b.radioGroup.clearCheck()
            rgInt.amount = null
        }

        b.radioGroup.setOnCheckedChangeListener { _, checkedId ->
            // checkedId는  position이다.
            if (checkedId < rgInt.list.size) {
                rgInt.amount = checkedId
            }
        }

        /**   초기 셋팅 : 각 Group안에 radio추가.   */
        rgInt.list.forEachIndexed { i, any -> addRadio(any, i) }
        addRadioMore(rgInt.list.size, onMore)

        /**   초기 셋팅 : 초기값 선택   */
        rgInt.amount?.let {amount->
            val listSize = rgInt.list.size

            if (amount < listSize) {
                b.radioGroup.check(amount)
            } else {
                b.radioGroup.check(listSize)
                radioEtc.text = amount.toString()
            }
        }

    }

    private fun addRadio(index: Int, i: Int) {
        val view = LayoutInflater.from(context).inflate(R.layout.view_radio_text, null).apply {
            val radio = findViewById<RadioButton>(R.id.rb)
            radio.text = index.toString()
            radio.id = i
        }
        b.radioGroup.addView(view)
    }


    /**
     * ID 반환. 나중에 이름 바꿔야 하기 때문에.
     */
    private fun addRadioMore(i: Int, onMore: (RadioButton) -> Unit) {
        val view = LayoutInflater.from(context).inflate(R.layout.view_radio_text, null).apply {
            radioEtc = findViewById(R.id.rb)
            radioEtc.text = R.string.ellipsis.getStr()
            radioEtc.id = i
            radioEtc.setOnClickListener {
                onMore(radioEtc)
            }
        }
        b.radioGroup.addView(view)
    }


}