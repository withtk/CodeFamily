package com.tkip.mycode.util.tools.recycler

import androidx.recyclerview.widget.RecyclerView
import com.tkip.mycode.databinding.HolderLastBinding

  class LastViewHolder(val b: HolderLastBinding) : RecyclerView.ViewHolder(b.root) {

    fun bind(msg: String) {
        b.holder = this
        b.msg = msg

        b.executePendingBindings()
    }


}