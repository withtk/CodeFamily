package com.tkip.mycode.util.tools.recycler

import android.os.Handler
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.scwang.smartrefresh.layout.SmartRefreshLayout
import com.tkip.mycode.databinding.LinearNoDataBinding
import com.tkip.mycode.databinding.ViewSmartRefreshBinding
import com.tkip.mycode.funs.common.lllogI
import com.tkip.mycode.init.base.clearAddAll
import com.tkip.mycode.nav.board.TopFotoAdapter
import com.tkip.mycode.util.lib.SnapHelper
import com.tkip.mycode.util.lib.photo.Foto
import com.tkip.mycode.util.tools.anim.*
import com.wang.avi.AVLoadingIndicatorView

class OnRv {

    companion object {

        /**
         * top foto
         */
        fun setTopFotoAdapter(arrayList: ArrayList<Foto>?, rvFoto: RecyclerView) {
            SnapHelper().attachToRecyclerView(rvFoto)
            arrayList?.let {
                val adapter = TopFotoAdapter(it)
                rvFoto.adapter = adapter
                Handler().postDelayed({ adapter.notifyDataSetChanged() }, 1500)   // 2초후에 리셋. 사진이 확대되는 경우가 있다.
            }

        }


        /**
         * 받아온 es리스트 정리하는 메서드
         * post, flea, room, lobby
         */
        fun <T> setRvInFrag(list: ArrayList<T>, inData: LinearNoDataBinding, refreshLayout: SmartRefreshLayout, tvLast: TextView, esLastIndex: Int): Int {
//            lllogI("setRvList  list $list")

            if (list.isEmpty()) {
                inData.tvNoData.showFadeIn(false)
                inData.tvRefresh.showFadeIn(false)
                inData.avLoading.hideFadeOut(true)
            } else {
                inData.tvNoData.hideFadeOut(false)
                inData.tvRefresh.hideFadeOut(false)
                inData.avLoading.hideFadeOut(true)
            }

            refreshLayout.finishRefresh(0)  // swf 닫기
            refreshLayout.finishLoadMore(0) // swf 닫기

            refreshLayout.setEnableLoadMore(esLastIndex != list.size)   // 더이상 data 없으면 loadMore 불능.

            if (list.isNotEmpty() && esLastIndex == list.size) tvLast.visi()   // 마지막 아이템입니다.
            else tvLast.gone()

            return list.size
        }


        /**
         * hasRefresh : 당겨서 새로고침
         */
        fun <T> setRvRefresh(resultList: ArrayList<T>?, from: Int, items: ArrayList<T>, vRefresh: ViewSmartRefreshBinding, hasRefresh: Boolean): Int {
            if (from == 0) {   // 처음 받아올 때
                if (resultList.isNullOrEmpty()) {   // 받아온 값 null
                    lllogI("setSearchResult  11")
                    items.clear()
                    if (hasRefresh) vRefresh.inData.tvRefresh?.showSlideUp(true)
                    vRefresh.inData.tvNoData?.showSlideUp(true)
                    vRefresh.refresh.setEnableLoadMore(false)
                    vRefresh.tvLast.gone()
                } else {
                    lllogI("setSearchResult  22")
                    items.clearAddAll(resultList)
                    vRefresh.inData.tvNoData?.gone()
                    vRefresh.inData.tvRefresh?.gone()
                    vRefresh.refresh.setEnableLoadMore(true)
                    vRefresh.tvLast.gone()
                }
            } else {   // loadMore로 받아올 때
                if (resultList.isNullOrEmpty()) {   // 받아온 값 null
                    lllogI("setSearchResult  33")
                    vRefresh.refresh.setEnableLoadMore(false)
                    vRefresh.tvLast.visi()
                } else {
                    lllogI("setSearchResult  44")
                    items.addAll(resultList)
                    //todo : 첫 리스트 후에 새로운 글이 추가된 뒤 리프레쉬하면 마지막 items이 두번 나오는 버그 고칠 것.
                    vRefresh.inData.tvNoData?.gone()
                    vRefresh.inData.tvRefresh?.gone()
                    vRefresh.refresh.setEnableLoadMore(true)
                    vRefresh.tvLast.gone()
                }
            }
            vRefresh.inData.avLoading.hideFadeOut(true)

            vRefresh.refresh.finishRefresh(0)  // swf 닫기
            vRefresh.refresh.finishLoadMore(0) // swf 닫기
            lllogI("setSearchResult   items.size ${items.size}")
            return items.size
        }



        /**
         * for tag
         *  es리스트 정리하는 메서드 : 새 리스트는 그대로 addAll
         */
        fun <T> setRvTagList(resultList: ArrayList<T>?, from: Int, items: ArrayList<T>, inData: LinearNoDataBinding, noData: String?, refresh: SmartRefreshLayout, tvLast: TextView): Int {
            noData?.let { inData.tvNoData.text = it }

            if (from == 0) {   // 처음 받아올 때
                if (resultList.isNullOrEmpty()) {   // 받아온 값 null
                    lllogI("setSearchResult  11")
                    items.clear()
                    inData.tvNoData?.showSlideUp(true)
                } else {
                    lllogI("setSearchResult  22")
                    items.clearAddAll(resultList)
                    inData.tvNoData?.gone()
                    refresh.setEnableLoadMore(true)
                    tvLast.gone()
                }
            } else {   // loadMore로 받아올 때
                if (resultList.isNullOrEmpty()) {   // 받아온 값 null
                    lllogI("setSearchResult  33")
                    refresh.setEnableLoadMore(false)
                    tvLast.visi()
                } else {
                    lllogI("setSearchResult  44")
                    items.addAll(resultList)
                    inData.tvNoData?.gone()
                    refresh.setEnableLoadMore(true)
                    tvLast.gone()
                }
            }
            inData.avLoading.hideFadeOut(true)

//            if (!resultList.isNullOrEmpty() && from == items.size) tvLast.visi()   // 마지막 아이템입니다.
//            else tvLast.gone()

            refresh.finishRefresh(0)  // swf 닫기
            refresh.finishLoadMore(0) // swf 닫기
            return items.size
        }




        /**
         * 검색 결과 rv정리 : 새 결과는 언제나 clear한 뒤에 뿌려야.
         * resultList : db에서 받아온 list
         * items : rv list
         */
        fun <T> setSearchResult(resultList: ArrayList<T>?, from: Int, items: ArrayList<T>, tvNoData: TextView?, noData: String?, avLoading: AVLoadingIndicatorView?, refresh: SmartRefreshLayout?): Int {
            lllogI("setSearchResult   resultList.size :  ${resultList?.size}")
            noData?.let { tvNoData?.text = it }
            if (from == 0) {   // 처음 받아올 때
                if (resultList.isNullOrEmpty()) {   // 받아온 값 null
                    lllogI("setSearchResult  11")

                    items.clear()
                    tvNoData?.showSlideUp(true)
                } else {
                    lllogI("setSearchResult  22")
                    items.clearAddAll(resultList)
                    tvNoData?.gone()
                    refresh?.setEnableLoadMore(true)
                }
            } else {   // loadMore로 받아올 때
                if (resultList.isNullOrEmpty()) {   // 받아온 값 null
                    lllogI("setSearchResult  33")
                    refresh?.setEnableLoadMore(false)
                } else {
                    lllogI("setSearchResult  44")
                    refresh?.setEnableLoadMore(true)
                    items.addAll(resultList)
                    tvNoData?.gone()
                }
            }
            refresh?.finishRefresh(0)  // swf 닫기
            refresh?.finishLoadMore(0) // swf 닫기
            avLoading?.hideFadeOut(true)
            lllogI("setSearchResult   items.size :  ${items.size}")
            return items.size
        }




    }


}