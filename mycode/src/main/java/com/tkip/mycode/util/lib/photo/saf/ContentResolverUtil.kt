package com.tkip.mycode.util.lib.photo.saf

import android.content.ContentResolver
import android.content.Context
import android.net.Uri
import android.os.ParcelFileDescriptor
import com.tkip.mycode.funs.common.lllogW
import java.io.FileNotFoundException
import java.io.IOException


class ContentResolverUtil(val context: Context) {


    fun Uri.isExist(context: Context): Boolean {
        val contentResolver: ContentResolver = context.contentResolver
        var pfd: ParcelFileDescriptor? = null
        try {
            pfd = contentResolver.openFileDescriptor(this, "r")
            return pfd != null
        } catch (e: FileNotFoundException) {
            lllogW(e.toString())
        } finally {
            try {
                pfd?.close()
            } catch (e: IOException) {
                lllogW(e.toString())
            }
        }
        return false
    }
}