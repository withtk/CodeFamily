package com.tkip.mycode.util.my_view

import android.content.Context
import android.graphics.drawable.Drawable
import android.util.AttributeSet
import android.view.LayoutInflater
import android.view.View
import android.widget.LinearLayout
import androidx.databinding.DataBindingUtil
import com.tkip.mycode.R
import com.tkip.mycode.databinding.ViewChoiceBoxBinding
import com.tkip.mycode.util.tools.anim.AnimUtil
import com.tkip.mycode.util.tools.etc.OnMyClick


class ViewChoiceBox : LinearLayout {
    private lateinit var b: ViewChoiceBoxBinding

    constructor(context: Context?, icon: Drawable?, title: String?, description: String?, hasCancel: Boolean, btnText: String?,ok: () -> Unit, cancel: () -> Unit) : super(context) {
//        this.listener = listener

        b = DataBindingUtil.inflate(LayoutInflater.from(context), R.layout.view_choice_box, this, true)

        icon?.let {
            b.ivIcon.setImageDrawable(icon)
        }
        title?.let {
            b.tvTitle.text = title
            b.tvTitle.visibility = View.VISIBLE
        }
        description?.let {
            b.tvDescription.text = description
            b.tvDescription.visibility = View.VISIBLE
        }

        if (hasCancel) {
            b.btnCancel.visibility = View.VISIBLE
        }
        b.btnCancel.setOnClickListener {
            if (OnMyClick.isDoubleClick()) return@setOnClickListener
            cancel()
            AnimUtil.hideSlideRight(this,true)
        }

        btnText?.let {
            b.btnOk.text = btnText
        }

        b.btnOk.setOnClickListener {
            if (OnMyClick.isDoubleClick()) return@setOnClickListener
            ok()
            AnimUtil.hideSlideRight(this,true)
        }

    }

    constructor(context: Context) : super(context) {
        init( )
    }

    constructor(context: Context, attrs: AttributeSet?) : super(context, attrs) {
        init( )
    }

    constructor(context: Context, attrs: AttributeSet?, defStyleAttr: Int) : super(context, attrs, defStyleAttr) {
        init( )
    }

    private fun init( ) {
        /* // inflate(getContext(), R.layout.beacon_setup_view, this);
         val inflater = context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
         inflater.inflate(R.layout.view_info, this)

         tvTitle = findViewById(R.id.tv_title)
         tvDescription = findViewById(R.id.tv_description)
         ivIcon = findViewById(R.id.iv_icon)
         btn = findViewById<Button>(R.id.btn)*/
    }


}