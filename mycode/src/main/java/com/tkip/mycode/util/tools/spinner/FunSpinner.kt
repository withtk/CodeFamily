package com.tkip.mycode.util.tools.spinner

import com.tkip.mycode.R
import com.tkip.mycode.ad.AdType
import com.tkip.mycode.ad.BannerType
import com.tkip.mycode.ad.getAdtype
import com.tkip.mycode.ad.getBannerType
import com.tkip.mycode.init.base.getStr
import com.tkip.mycode.model.my_enum.*
import com.tkip.mycode.model.mytag.TagTYPE
import com.tkip.mycode.model.mytag.getTagType
import com.tkip.mycode.model.warn.WarnType
import com.tkip.mycode.model.warn.getWarnType




/*************************************************************************************************
 * spinner에 들어갈 STRING list
 *     : 간편한 사용을 위해.
 *************************************************************************************************/
enum class TypeList(val no: Int) {

    MYTAG(100),
    WARN(200),
    STORE(300),
    AD_TYPE(400),
    BANNER_TYPE(500),

    PAY_TYPE(600),
    COST_TYPE(700),
    ACT_TYPE(800),

    DATA_TYPE(900),
    ;  // 반드시 ;을 넣어줘야함.


    /**     * arraylist로 만들기     */
    fun list(info: String?) =
            arrayListOf<String>().apply {
                add(info ?: R.string.info_select_spinner.getStr())   // 기본 안내멘트 추가.
                when (this@TypeList) {
                    MYTAG -> TagTYPE.values().forEach { this@apply.add(it.title) }
                    WARN -> WarnType.values().forEach { this@apply.add(it.title) }
                    STORE -> StoreType.values().forEach { this@apply.add(it.title) }
                    AD_TYPE -> AdType.values().forEach { this@apply.add(it.title) }
                    BANNER_TYPE -> BannerType.values().forEach { this@apply.add(it.title) }

                    PAY_TYPE -> PayType.values().forEach { this@apply.add(it.title) }
                    COST_TYPE -> CostType.values().forEach { this@apply.add(it.title) }
                    ACT_TYPE -> ActType.values().forEach { this@apply.add(it.title) }
                    DATA_TYPE -> DataType.values().forEach { this@apply.add(it.title) }
                }
            }

    /**     * 항목 받아오기 from STring  */
    fun getEnumString(no: Int) =
            when (this) {
                MYTAG -> no.getTagType().title
                WARN -> no.getWarnType().title
                STORE -> no.getStoreType().title
                AD_TYPE -> no.getAdtype().title
                BANNER_TYPE -> no.getBannerType().title

                PAY_TYPE -> no.payType().title
                COST_TYPE -> no.costType().title
                ACT_TYPE -> no.actType().title
                DATA_TYPE -> no.dataType().title

            }

    /**     * 항목 받아오기 from No  */
    fun getEnumNo(title: String) =
            when (this) {
                MYTAG -> title.getTagType().no
                WARN -> title.getWarnType().no
                STORE -> title.getStoreType().no
                AD_TYPE -> title.getAdtype().no
                BANNER_TYPE -> title.getBannerType().no

                PAY_TYPE -> title.payType().no
                COST_TYPE -> title.costType().no
                ACT_TYPE -> title.actType().no
                DATA_TYPE -> title.dataType().no

            }

}