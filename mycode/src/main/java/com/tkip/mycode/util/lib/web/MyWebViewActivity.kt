package com.tkip.mycode.util.lib.web

import android.os.Bundle
import android.widget.LinearLayout
import androidx.annotation.LayoutRes
import com.just.agentweb.AgentWeb
import com.tkip.mycode.R
import com.tkip.mycode.databinding.ActivityMyWebViewBinding
import com.tkip.mycode.funs.common.lllogI
import com.tkip.mycode.init.PUT_URL
import com.tkip.mycode.init.my_bind.BindingActivity


class MyWebViewActivity : BindingActivity<ActivityMyWebViewBinding>() {
    @LayoutRes
    override fun getLayoutResId() = R.layout.activity_my_web_view
    private val url by lazy { intent.getStringExtra(PUT_URL) }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        b.activity = this
        b.url = url

        /** 툴바 */
//        b.inToolbar.title = R.string.menu_title_credit_point_list.getStr()
//        b.inToolbar.backIcon = true
////        b.inToolbar.menuText = R.string.add_ad_unit_manage.getStr()
//        b.inToolbar.inter = interToolbarIcon(onFirst = { onBackPressed() }, onTopTitle = {}, onTextMenu = {}, onIcon = {}, onMore = {})
////        b.frame.addView(CustomSearchTotalPager(this@SearchTotalActivity,supportFragmentManager,manage,uid,countType, onItemClick = {}))

       /* b.webview.apply {

            setNetworkAvailable(true)

            webViewClient = object : WebViewClient() {  // 클릭시 새창 안뜨게
                override fun onPageFinished(view: WebView?, url: String?) {
                    super.onPageFinished(view, url)
                    lllogD("MyWebViewActivity onPageFinished url:$url")
                }
            }

            //세부 세팅 등록
            settings.apply {
//                setRenderPriority(WebSettings.RenderPriority.HIGH)
//                setEnableSmoothTransition(true)

                javaScriptEnabled = true // 웹페이지 자바스클비트 허용 여부
                setSupportMultipleWindows(false) // 새창 띄우기 허용 여부
                javaScriptCanOpenWindowsAutomatically = false // 자바스크립트 새창 띄우기(멀티뷰) 허용 여부
                loadWithOverviewMode = true // 메타태그 허용 여부
                useWideViewPort = true // 화면 사이즈 맞추기 허용 여부
                setSupportZoom(false) // 화면 줌 허용 여부
                builtInZoomControls = false // 화면 확대 축소 허용 여부
                layoutAlgorithm = WebSettings.LayoutAlgorithm.SINGLE_COLUMN // 컨텐츠 사이즈 맞추기
                cacheMode = WebSettings.LOAD_NO_CACHE // 브라우저 캐시 허용 여부
                domStorageEnabled = true // 로컬저장소 허용 여부

            }

            loadUrl(url)
        }
*/
        lllogI("MyWebViewActivity url  $url")


        AgentWeb.with(this)
                .setAgentWebParent(b.lineCover, LinearLayout.LayoutParams(-1, -1))
                .useDefaultIndicator()
                .createAgentWeb()
                .ready()
                .go(url)

    }

}
