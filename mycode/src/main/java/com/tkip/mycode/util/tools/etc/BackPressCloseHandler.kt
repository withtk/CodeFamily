package com.tkip.mycode.util.tools.etc

import android.app.Activity
import android.view.Gravity
import android.view.LayoutInflater
import android.widget.Toast
import androidx.core.content.ContextCompat
import com.tkip.mycode.R
import com.tkip.mycode.databinding.ToastIconTextBinding
import com.tkip.mycode.dialog.toast.TToast
import com.tkip.mycode.funs.common.lllogI
import com.tkip.mycode.init.base.getStr
import com.tkip.mycode.init.part.CodeApp

class BackPressCloseHandler {

    companion object {

        var toast: Toast? = null
        private var backKeyPressedTime: Long = 0

        fun onBackPressed(activity: Activity) {
            lllogI("MainActivity onBackPressed BackPressCloseHandler")

            if (System.currentTimeMillis() <= backKeyPressedTime + 2000) {     // 2초 이내에 클릭
                activity.finish()
                toast?.cancel()
            } else {                                                           // 2초 이후에 클릭
                backKeyPressedTime = System.currentTimeMillis()
//                TToast.showNormal(R.string.info_back_press_close)

                vanishToast(R.string.info_back_press_close.getStr(), R.color.toast_icon_text_background_normal, R.drawable.ic_whatshot)

            }
        }

        /**
         *  어플 종료시 토스트도 종료시키기 위해 이 토스트는 따로 떼어냄.
         */
        private fun vanishToast(message: String, colorRes: Int?, drawableRes: Int?) {

            val context = CodeApp.getAppContext()
//            val view = LayoutInflater.from(context).inflate(R.layout.toast_icon_text, null, false)

            val b =  ToastIconTextBinding.inflate(LayoutInflater.from(context), null, false)

            b.tvMessage.text = message
//            b.tv_message.text = title
            colorRes?.let {
                b.card.setCardBackgroundColor(ContextCompat.getColor(context, it))
            }

            drawableRes?.let {
                //                view.iv_icon.setImageDrawable(ContextCompat.getDrawable(context, it))
                b.ivIcon.setImageDrawable(ContextCompat.getDrawable(context, it))
            }

            /*
            view.tv_message.text = title
            colorRes?.let {
                view.card.setCardBackgroundColor(ContextCompat.getColor(context, it))
            }

            drawableRes?.let {
                view.iv_icon.setImageDrawable(ContextCompat.getDrawable(context, it))
            }*/

            toast = Toast(context)
            toast?.apply {
                setGravity(Gravity.TOP, 0, TToast.T_HEIGHT)
                view = b.root
                show()
            }
        }

    }


}