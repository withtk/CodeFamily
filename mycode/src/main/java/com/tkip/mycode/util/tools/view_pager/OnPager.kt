package com.tkip.mycode.util.tools.view_pager

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.viewpager.widget.ViewPager
import com.google.android.material.tabs.TabLayout

class OnPager {

    companion object {

        fun commonSetPager(fragmentManager: FragmentManager, list: ArrayList<Pair<Fragment, String>>, viewPager: ViewPager, tabLayout: TabLayout) {

            val listFragment = arrayListOf<Fragment>()
            val listTitle = arrayListOf<String>()
            val pagerAdapter = BasePagerAdapter(fragmentManager, listFragment, listTitle)

            list.forEach { pagerAdapter.addFragment(it.first, it.second) }

            viewPager.adapter = pagerAdapter
            viewPager.offscreenPageLimit = listFragment.size - 1   // tab이 리셋되지 않도록.

            tabLayout.setupWithViewPager(viewPager)

            // 시작하는 탭 설정
//        b.tabLayout.setScrollPosition(2, 0f, true)
//        b.viewPager.currentItem = 2
        }


    }
}