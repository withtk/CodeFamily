package com.tkip.mycode.util.lib

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider


class BaseViewModelFactory<T>(val creator: () -> T) : ViewModelProvider.Factory {
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return creator() as T
    }

//    ViewModelProvider.Factory {
//        override fun <T : ViewModel?> create(modelClass: Class<T>): T {
//            return modelClass.getConstructor(Long::class.java).newInstance(initialTime)
//        }

}


//class PeerViewModelFactory (var uid:String): ViewModelProvider.Factory {
//    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
//        return PeerViewModel(uid) as T
//    }
//}
