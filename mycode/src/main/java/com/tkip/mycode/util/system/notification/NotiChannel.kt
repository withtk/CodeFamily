package com.tkip.mycode.util.system.notification

import android.annotation.SuppressLint
import android.app.Notification
import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.graphics.Color
import com.tkip.mycode.R
import com.tkip.mycode.funs.common.lllogD
import com.tkip.mycode.init.MainActivity
import com.tkip.mycode.init.base.getStr
import com.tkip.mycode.model.flea.OnFlea
import com.tkip.mycode.model.grant.OnGrant
import com.tkip.mycode.model.my_enum.*
import com.tkip.mycode.model.post.OnPost
import com.tkip.mycode.model.room.OnRoom
import com.tkip.mycode.model.warn.OnWarn


/*********************************** notiChannel ****************************************************/
enum class NotiGroup(val no: Int, val id: String, val title: String) {
    ACT(100, "basic", R.string.noti_group_act.getStr()),
    BASIC(200, "basic", R.string.noti_group_basic.getStr()),
    BID(300, "bid", R.string.noti_group_bid.getStr()),
    ;
}

enum class NotiChannel(val no: Int, val group: NotiGroup, val id: String, val importance: Int, val screen: Int, val color: Int?, val vib: LongArray?, val iconId: Int?, val title: String, val desc: String) {

    //    CHAT(1000, NotiGroup.ACT, "chat", NotificationManager.IMPORTANCE_HIGH, Notification.VISIBILITY_PRIVATE, Color.CYAN, basicVib(), null, R.string.noti_name_chat.getStr(), R.string.noti_desc_chat.getStr()),
    REPLY(1100, NotiGroup.ACT, "reply", NotificationManager.IMPORTANCE_HIGH, Notification.VISIBILITY_PRIVATE, Color.CYAN, basicVib(), null, R.string.noti_name_reply.getStr(), R.string.noti_desc_reply.getStr()),
    POST(1200, NotiGroup.ACT, "post", NotificationManager.IMPORTANCE_HIGH, Notification.VISIBILITY_PRIVATE, Color.CYAN, basicVib(), null, R.string.noti_name_post.getStr(), R.string.noti_desc_post.getStr()),
    FLEA(1300, NotiGroup.ACT, "flea", NotificationManager.IMPORTANCE_HIGH, Notification.VISIBILITY_PRIVATE, Color.CYAN, basicVib(), null, R.string.noti_name_flea.getStr(), R.string.noti_desc_flea.getStr()),
    ROOM(1400, NotiGroup.ACT, "room", NotificationManager.IMPORTANCE_HIGH, Notification.VISIBILITY_PRIVATE, Color.CYAN, basicVib(), null, R.string.noti_name_room.getStr(), R.string.noti_desc_room.getStr()),

    DEFAULT(2000, NotiGroup.BASIC, "default", NotificationManager.IMPORTANCE_HIGH, Notification.VISIBILITY_PRIVATE, Color.CYAN, basicVib(), null, R.string.noti_name_default.getStr(), R.string.noti_desc_default.getStr()),  // for admin
    INFO(2100, NotiGroup.BASIC, "info", NotificationManager.IMPORTANCE_LOW, Notification.VISIBILITY_PRIVATE, Color.CYAN, basicVib(), null, R.string.noti_name_info.getStr(), R.string.noti_desc_info.getStr()),
    WARN(2400, NotiGroup.BASIC, "warn", NotificationManager.IMPORTANCE_HIGH, Notification.VISIBILITY_PRIVATE, Color.CYAN, basicVib(), null, R.string.noti_name_warn.getStr(), R.string.noti_desc_warn.getStr()),
    GRANT(2500, NotiGroup.BASIC, "grant", NotificationManager.IMPORTANCE_HIGH, Notification.VISIBILITY_PRIVATE, Color.CYAN, basicVib(), null, R.string.noti_name_grant.getStr(), R.string.noti_desc_grant.getStr()),
    PENALTY(2600, NotiGroup.BASIC, "penalty", NotificationManager.IMPORTANCE_HIGH, Notification.VISIBILITY_PRIVATE, Color.CYAN, basicVib(), null, R.string.noti_name_penalty.getStr(), R.string.noti_desc_penalty.getStr()),

    BID_FAIL(3000, NotiGroup.BID, "bid_fail", NotificationManager.IMPORTANCE_HIGH, Notification.VISIBILITY_PRIVATE, Color.CYAN, basicVib(), null, R.string.noti_name_bid_fail.getStr(), R.string.noti_desc_bid_fail.getStr()),
    BID_SUCCESS(3100, NotiGroup.BID, "bid_success", NotificationManager.IMPORTANCE_MIN, Notification.VISIBILITY_PRIVATE, Color.CYAN, basicVib(), null, R.string.noti_name_bid_success.getStr(), R.string.noti_desc_bid_success.getStr()),
    ;

    /*** noti 내용 변경하고 싶을 때 */
    fun resetNoti(noti: Noti) {
        when (this) {
            BID_FAIL -> noti.body = noti.modelTitle
            else -> {
            }
        }
    }


    fun pendingIntent(context: Context, id: Int, noti: Noti): PendingIntent? {
//        val intent = noti.dataTypeNo?.getDataType()?.getModelIntent(context, noti)
//        return PendingIntent.getActivity(context, id, intent, PendingIntent.FLAG_CANCEL_CURRENT) //requestCode = id 일치 필수.
        lllogD("noti noti GRANT pendingIntent start extra : ${noti.extra}")
        val result = when (this) {
            REPLY ->
                when (noti.dataTypeNo?.dataType()) {
                    DataType.POST -> PendingIntent.getActivity(context, id, OnPost.getIntent(context, false, null, noti.modelKey), PendingIntent.FLAG_CANCEL_CURRENT)
                    DataType.FLEA -> PendingIntent.getActivity(context, id, OnFlea.getIntent(context, false, null, noti.modelKey), PendingIntent.FLAG_CANCEL_CURRENT)
                    DataType.ROOM -> PendingIntent.getActivity(context, id, OnRoom.getIntent(context, false, null, noti.modelKey), PendingIntent.FLAG_CANCEL_CURRENT)
                    else -> null
                }
            POST -> PendingIntent.getActivity(context, id, OnPost.getIntent(context, false, null, noti.modelKey), PendingIntent.FLAG_CANCEL_CURRENT)
            FLEA -> PendingIntent.getActivity(context, id, OnFlea.getIntent(context, false, null, noti.modelKey), PendingIntent.FLAG_CANCEL_CURRENT)
            ROOM -> PendingIntent.getActivity(context, id, OnRoom.getIntent(context, false, null, noti.modelKey), PendingIntent.FLAG_CANCEL_CURRENT)
            DEFAULT -> null
            INFO -> null
            WARN -> PendingIntent.getActivity(context, id, OnWarn.getWarnListIntent(context, true), PendingIntent.FLAG_CANCEL_CURRENT)
            GRANT -> {
                lllogD("noti noti GRANT pendingIntent dataType : ${noti.dataTypeNo?.dataType()}")
                if (noti.modelKey == null) null
                else noti.dataTypeNo?.dataType()?.getPendingIntent(context, id, noti.modelKey!!)
//                noti.modelKey?.let { key ->
//                    noti.dataTypeNo?.dataType()?.getPendingIntent(context, id, key) }

//                if (noti.extra?.toInt()?.getStatus() == Status.GRANT_REQUESTED) {
//                    lllogD("noti noti GRANT pendingIntent GRANT_REQUESTED")
//                    null
////                    PendingIntent.getActivity(context, id, OnGrant.getGrantIntent(context), PendingIntent.FLAG_CANCEL_CURRENT)
//                }   // REQUESTED 일때만 acti이동.
//                else {
//                    lllogD("noti noti GRANT pendingIntent null")
//                    null
//                }
            }
            PENALTY -> null
            BID_FAIL -> null
            BID_SUCCESS -> null
        }

        return result ?: let {
            val intent = Intent(context, MainActivity::class.java).apply { flags = Intent.FLAG_ACTIVITY_CLEAR_TOP or Intent.FLAG_ACTIVITY_SINGLE_TOP }
            PendingIntent.getActivity(context, id, intent, PendingIntent.FLAG_CANCEL_CURRENT)
        }
    }


    /**     * 채널 초기세팅     */
    @SuppressLint("NewApi")
    fun applyChannel(context: Context) {
        NotificationChannel(this.id, this.title, this.importance).let { ch ->
            ch.group = this.group.id
            ch.description = this.desc
            ch.lockscreenVisibility = this.screen
            this.color?.let {
                ch.enableLights(true)
                ch.lightColor = it
            }
            this.vib?.let {
                ch.enableVibration(true)
                ch.vibrationPattern = it
            }
            context.notiManager().createNotificationChannel(ch)
        }
    }
}


/**************************** notiChannel END ***********************************************************/


fun Int.getNotiChannel(): NotiChannel {
    NotiChannel.values().forEach {
        if (this == it.no) return it
    }
    return NotiChannel.DEFAULT
}

fun String.getNotiChannel(): NotiChannel {
    NotiChannel.values().forEach {
        if (this == it.title) return it
    }
    return NotiChannel.DEFAULT
}


private fun basicVib() = longArrayOf(100, 1000, 1000, 100, 100, 100, 100, 100)


