package com.tkip.mycode.util.system.notification

import android.app.NotificationChannelGroup
import android.content.Context
import android.os.Build

class NotiChannelManager {

    companion object {
        fun createGroupChannel(context: Context) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                /** create Group */
                arrayOf(NotiGroup.BASIC, NotiGroup.BID).forEach { context.notiManager().createNotificationChannelGroup(NotificationChannelGroup(it.id, it.title)) }

                /** create Channel */
                NotiChannel.values().forEach { it.applyChannel(context) }
            }
        }


        fun deleteChannel(context: Context, channel: NotiChannel) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) context.notiManager().deleteNotificationChannel(channel.id)
        }


//        private val MY_GROUP by lazy { R.string.noti_group_basic.getStr() }
//        private val DEFAULT_ID = 1034
//        fun createGroupChannel(context: Context) {
//            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
//
//                val group1 = NotificationChannelGroup(MY_GROUP, MY_GROUP)
//                context.getManager().createNotificationChannelGroup(group1)
//
//                // 소리는 안 되는 듯.
//                /* val customSoundUri = Uri.parse("android.resource://" + context.packageName + "/" + R.raw.short_tone)
//                 val att = AudioAttributes.Builder()
//                         .setUsage(AudioAttributes.USAGE_NOTIFICATION)
//                         .setContentType(AudioAttributes.CONTENT_TYPE_SPEECH)
//                         .build()*/
//
//                val chDefault = NotificationChannel(
//                        Channel.DEFAULT,
//                        context.getString(R.string.noti_name_default),
//                        NotificationManager.IMPORTANCE_HIGH)
//                chDefault.description = context.getString(R.string.noti_desc_default)
//                chDefault.group = MY_GROUP
//                chDefault.enableLights(true)
//                chDefault.lightColor = Color.MAGENTA
//                chDefault.lockscreenVisibility = Notification.VISIBILITY_PRIVATE
////                chDefault.setSound(customSoundUri, att)
//                chDefault.enableVibration(true)
//                chDefault.vibrationPattern = longArrayOf(100, 1000, 1000, 100, 100, 100, 100, 100)
//                context.getManager().createNotificationChannel(chDefault)
//
//            }
//        }


    }


}