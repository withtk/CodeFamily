package com.tkip.mycode.util.lib.calendar.meizu;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;

import com.haibin.calendarview.Calendar;
import com.haibin.calendarview.MonthView;

public class MeiZuMonthView extends MonthView {


    private int mRadius;
    /**
     * Meizu 표시 텍스트 브러쉬 사용자 정의
     */
    private Paint mTextPaint = new Paint();
    /**
     * 24 태양 용어 브러시
     */
    private Paint mSolarTermTextPaint = new Paint();
    /**
     * 배경 점
     */
    private Paint mPointPaint = new Paint();
    /**
     * 오늘의 배경색
     */
    private Paint mCurrentDayPaint = new Paint();
    /**
     * 도트 반경
     */
    private float mPointRadius;
    private int mPadding;
    private float mCircleRadius;
    /**
     * 맞춤 Meizu 태그의 원형 배경
     */
    private Paint mSchemeBasicPaint = new Paint();
    private float mSchemeBaseLine;

    private int color1 = 0xFF489dff;
    int color2 = 0xff333333;
    int color3 = 0xffCFCFCF;
    int color4 = 0xFFe1e1e1;
    int color5 = 0xFFeaeaea;
    int color6 = 0xFFFFCA28;



    public MeiZuMonthView(Context context) {
        super(context);

        mPointPaint.setAntiAlias(true);
        mPointPaint.setStyle(Paint.Style.FILL);
        mPointPaint.setTextAlign(Paint.Align.CENTER);
        mPointPaint.setColor(Color.RED);


        mCircleRadius = dipToPx(getContext(), 7);
        mPadding = dipToPx(getContext(), 3);
        mPointRadius = dipToPx(context, 2);
    }
    /**
     * dp ~ px
     *
     * @param context context
     * @param dpValue dp
     * @return px
     */
    private static int dipToPx(Context context, float dpValue) {
        final float scale = context.getResources().getDisplayMetrics().density;
        return (int) (dpValue * scale + 0.5f);
    }

    @Override
    protected void onPreviewHook() {
        mRadius = Math.min(mItemWidth, mItemHeight) / 11 * 5;
    }


    /**
     * draw select calendar
     *
     * @param canvas    canvas
     * @param calendar  select calendar
     * @param x         calendar item x start point
     * @param y         calendar item y start point
     * @param hasScheme is calendar has scheme?
     * @return if return true will call onDrawScheme again
     */
    @Override
    protected boolean onDrawSelected(Canvas canvas, Calendar calendar, int x, int y, boolean hasScheme) {
        canvas.drawRect(x , y , x + mItemWidth , y + mItemHeight, mSelectedPaint);
        return true;
    }

    /**
     * draw scheme if calendar has scheme
     *
     * @param canvas   canvas
     * @param calendar calendar has scheme
     * @param x        calendar item x start point
     * @param y        calendar item y start point
     */
    @Override
    protected void onDrawScheme(Canvas canvas, Calendar calendar, int x, int y) {
        canvas.drawCircle(x + mItemWidth / 2, y + mItemHeight - 3 * mPadding, mPointRadius, mPointPaint);
    }

    /**
     * draw text
     *
     * @param canvas     canvas
     * @param calendar   calendar
     * @param x          calendar item x start point
     * @param y          calendar item y start point
     * @param hasScheme  is calendar has scheme?
     * @param isSelected is calendar selected?
     */
    @Override
    protected void onDrawText(Canvas canvas, Calendar calendar, int x, int y, boolean hasScheme, boolean isSelected) {
        float baselineY = mTextBaseLine + y;
        int cx = x + mItemWidth / 2;
        canvas.drawText(String.valueOf(calendar.getDay()),
                cx,
                baselineY,
                calendar.isCurrentDay() ? mCurDayTextPaint :
                        calendar.isCurrentMonth() ? mSchemeTextPaint : mOtherMonthTextPaint);
    }
}