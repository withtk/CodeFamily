package com.tkip.mycode.util.lib.photo.album

import android.Manifest.permission.READ_EXTERNAL_STORAGE
import android.content.Intent
import android.content.pm.PackageManager
import android.os.Bundle
import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.databinding.DataBindingUtil
import com.tkip.mycode.R
import com.tkip.mycode.admin.versionGteQ
import com.tkip.mycode.databinding.ActivityAlbumBinding
import com.tkip.mycode.dialog.alert.MyAlert
import com.tkip.mycode.dialog.toast.TToast
import com.tkip.mycode.funs.common.OnPermission
import com.tkip.mycode.funs.common.OnVisible
import com.tkip.mycode.funs.common.lllogD
import com.tkip.mycode.funs.common.lllogI
import com.tkip.mycode.init.REQ_READ_EXSTORAGE
import com.tkip.mycode.init.base.Cons
import com.tkip.mycode.init.my_bind.BaseActivity
import com.tkip.mycode.util.lib.photo.MyPhoto


class AlbumActivity : BaseActivity() {

    private lateinit var b: ActivityAlbumBinding
    private lateinit var mAdapter: AlbumAdapter
    private lateinit var mAdapter2: AlbumSelectedAdapter

//    private var listFolder = arrayListOf<String>()   // 폴더명
//    private var mapItems = mutableMapOf<String, ArrayList<MyPhoto>>()   // <폴더명,MyPhoto리스트>
//    private var listSpinner = arrayListOf<String>()   // 폴더명

    private var listSelected = arrayListOf<MyPhoto>()   // 선택된 MyPhoto
    private val LIMIT = 10   // toast의 알림 스트링과 함께 수정요.  info_limit_myphoto_album
    var isMax = false


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        b = DataBindingUtil.setContentView(this, R.layout.activity_album)
        b.activity = this
        checkPermissionAndInit()
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        when (requestCode) {
            REQ_READ_EXSTORAGE ->
                if (grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED) checkPermissionAndInit()
                else OnPermission.informAbout_READ_PERMISSION(this@AlbumActivity)
        }
    }

    private fun checkPermissionAndInit() {
        val readPermission = ContextCompat.checkSelfPermission(this@AlbumActivity, READ_EXTERNAL_STORAGE)
        if (readPermission == PackageManager.PERMISSION_GRANTED) {
            initView()
            b.btnGetPermission.visibility = View.GONE
        } else ActivityCompat.requestPermissions(this@AlbumActivity, arrayOf(READ_EXTERNAL_STORAGE), REQ_READ_EXSTORAGE)
    }


    override fun onBackPressed() {
        if (listSelected.isNotEmpty()) {
            val strMsg = getString(R.string.info_cancel_all_album)
            MyAlert.showConfirm(this@AlbumActivity, strMsg, { super.onBackPressed() }, {})
            return
        }
        super.onBackPressed()
    }


    private fun initView() {

        OnAlbum.initAlbum(this@AlbumActivity)

        /*** set adapter & 폴더 spinner */
        setAdapter1()
        setAdapter2()
        setFolderSpinner()

        b.spinner.setSelection(0)

    }


    fun onClickGetPermission() {
        checkPermissionAndInit()
    }

    fun onClickOk() {
        lllogI("AlbumActivity onClickOk")

        if (listSelected.isEmpty()) TToast.showNormal(R.string.info_no_selected_album)
        else {
            val intent = Intent().apply {
                if (versionGteQ) listSelected.forEach { it.bitmap = null }   // bitmap 삭제
                putParcelableArrayListExtra(Cons.PUT_ARRAY_MYPHOTO, listSelected)
            }
            setResult(RESULT_OK, intent)
            finish()
        }

    }

    /**     * 전체 이미지 불러오기     */
    private fun setAdapter1() {
        mAdapter = AlbumAdapter(arrayListOf(), object : AlbumAdapter.OnAlbumListener {

            override fun onSelected(myPhoto: MyPhoto, p: Int) {

                if (!listSelected.contains(myPhoto)) {
                    listSelected.add(myPhoto)
                    handleAdapter2()
                }
                isMax = (listSelected.size >= LIMIT)   // 최대값 여부 기입.
            }

            override fun unSelected(myPhoto: MyPhoto, p: Int) {

                /*** 이미 있으면 remove 후 add */
                if (listSelected.contains(myPhoto)) {
                    val position = listSelected.indexOf(myPhoto)
                    listSelected.remove(myPhoto)
                    handleAdapter2()
                }
                isMax = (listSelected.size >= LIMIT)   // 최대값 여부 기입.
            }

            override fun onPhotoView() {}
        })
        mAdapter.setHasStableIds(true)
        b.rv.adapter = mAdapter
    }

    /**     * 전체 이미지 중 선택/해제 클릭시 */
    private fun handleAdapter2() {
        OnVisible.toggle(listSelected.isEmpty(), arrayOf(b.rv2), null)
        mAdapter2.notifyDataSetChanged()
//        b.rv2.smoothScrollToPosition(listSelected.size - 1)   // 스무스 이동
    }

    /*** 선택한 이미지 하단에 표시 */
    private fun setAdapter2() {
        lllogI("setAdapter2 setAdapter2 listSelected size ${listSelected.size}")
        mAdapter2 = AlbumSelectedAdapter(listSelected,
                onCancelSelected = { myPhotoCanceled ->

                    // check 해제.
                    val lllist = OnAlbum.mapItems[myPhotoCanceled.folder]    // 그 리스트를 가져와서
                    lllist!![lllist.indexOf(myPhotoCanceled)] = myPhotoCanceled  // isSel 부분을 수정한 myPhoto를 넣는다.

                    listSelected.remove(myPhotoCanceled)
                    mAdapter.notifyDataSetChanged()
                    mAdapter2.notifyDataSetChanged()

                })
        mAdapter2.setHasStableIds(true)
        b.rv2.adapter = mAdapter2
    }


    /*** 폴더명 스피너 셋팅 */
    private fun setFolderSpinner() {
        val spinAdapter = ArrayAdapter(this@AlbumActivity, android.R.layout.simple_spinner_item, OnAlbum.listSpinner)
        spinAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        b.spinner.adapter = spinAdapter
        b.spinner.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onNothingSelected(parent: AdapterView<*>?) {
                lllogD("spin onNothingSelected")
            }

            override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {

                val folderName = OnAlbum.listFolder[position]  //  폴더이름 가져오기
                mAdapter.items = OnAlbum.mapItems[folderName]!!     //  폴더이름에 맞는 arraylist 가져와서 세팅하기.

                mAdapter.notifyDataSetChanged()
            }
        }
    }

//    private fun fetchAllImages() {
//        // DATA는 이미지 파일의 스트림 데이터 경로를 나타냅니다.
//        val projection = arrayOf(
//                MediaStore.Images.Media.DATA,
//                MediaStore.Images.Media._ID,
//                MediaStore.Images.Media.DATE_TAKEN,
//                MediaStore.Images.Media.SIZE,
//                MediaStore.Images.Media.ORIENTATION)
//
//        val cursor = contentResolver.query(
//                MediaStore.Images.Media.EXTERNAL_CONTENT_URI, // 이미지 컨텐트 테이블
//                projection,                                   // DATA를 출력
//                null,                                // 모든 개체 출력
//                null,
//                MediaStore.MediaColumns.DATE_ADDED + " DESC") // 정렬
//
//        if (cursor == null) {
//            // Error 발생
//            lllogD("fetchAllImages cursor null")
//            return
//        } else {
//            val dataColumnIndex = cursor.getColumnIndex(projection[0])
//            val dataId = cursor.getColumnIndex(projection[1])
//            val dataDateTaken = cursor.getColumnIndex(projection[2])
//            val dataSize = cursor.getColumnIndex(projection[3])
//            val dataOrientation = cursor.getColumnIndex(projection[4])
//
//
//            if (cursor.moveToFirst()) {
//                do {
//                    val filePath = cursor.getString(dataColumnIndex)
//                    val imageUri = Uri.parse(filePath)
//                    val id = cursor.getString(dataId)
//                    val taken = cursor.getString(dataDateTaken)
//                    val size = cursor.getString(dataSize)
//                    val orientation = cursor.getString(dataOrientation)
//
//                    val array = filePath.split("/".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()  //folder별 정리.
//                    val folder = array[array.size - 2]
//
//
//                    val myPhoto = MyPhoto(
//                            uri = imageUri,
//                            thumbUri = null,
//                            photoPath = filePath,
//                            photoId = id,
//                            date = taken,
//                            size = size,
//                            orientation = orientation,
//                            folder = folder,
//                            isSel = false
//                    )
//                    addListFolder(myPhoto)
//
//
//                } while (cursor.moveToNext())
//
//            } else {
//                // Cursor가 비었습니다.
//                lllogD("fetchAllImages Cursor가 비었습니다.")
//            }
//        }
//        cursor.close()
//    }
//
//    private fun addListFolder(myPhoto: MyPhoto) {
//
//        myPhoto.folder?.let { folder ->
//
//            if (listFolder.contains(folder)) {    // folder가 있을때
//                for ((k, v) in mapItems) {
//                    if (k == folder) {   // map에 그 folder 키에 list안에 myphoto 넣기.
//                        v.add(myPhoto)
//                    }
//                }
//
//            } else {                              // folder가 없을때
//                listFolder.add(folder)
//                val list = ArrayList<MyPhoto>()
//                list.add(myPhoto)
//                mapItems[folder] = list
//
//            }
//
//
//        }
//
//    }
//
//    private fun modifyFolderName() {
//        // folder안의 image count를 spinner에 넣기위해.
//        for ((k, v) in mapItems) {
//            listSpinner.add(k + " (" + v.size + ")")
//        }
//    }


}
