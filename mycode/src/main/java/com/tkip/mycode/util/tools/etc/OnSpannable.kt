package com.tkip.mycode.util.tools.etc

import android.text.SpannableString
import android.text.Spanned
import android.text.style.ForegroundColorSpan
import android.text.style.RelativeSizeSpan
import android.text.style.UnderlineSpan
import android.widget.TextView

/**
 * <store upload 로직>
 *     user add,update -> storeInspection
 *     admin approval -> store
 *
 * <storeMenu 로직>
 *     user add, update -> store>storeMenu : statusNo.inspection
 *     admin approval -> store>storeMenu : statusNo.normal1000
 */
class OnSpannable {

    companion object {


        /**
         * 사이즈 변경
         */
        fun setRelativeSizeSpan(tv:TextView,proportion:Float,start:Int,end:Int){
            val span = tv.text as SpannableString
            span.setSpan(RelativeSizeSpan(proportion), start, end, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE)
        }


        /**
         * 컬러 변경
         */
        fun setForegroundColorSpan(tv:TextView,color:Int,start:Int,end:Int){
            val span = tv.text as SpannableString
            span.setSpan(ForegroundColorSpan(color), start, end, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE)
        }


        /**
         * 밑줄 추가
         */
        fun setUnderlineSpan(tv:TextView, start:Int,end:Int?){
            val span = tv.text as SpannableString
            span.setSpan(UnderlineSpan(), start, end?:tv.text.length, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE)
        }


//
//        fun setRelativeSizeSpan(tv: TextView,  vararg  triples:Triple<Float, Int, Int>) {
//            val span = tv.text as SpannableString
//            triples.forEach { t ->
//                span.setSpan(RelativeSizeSpan(t.first), t.second, t.third, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE)
//            }
//        }
    }

}