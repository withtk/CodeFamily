package com.tkip.mycode.util.lib.map.google_map

import android.graphics.Bitmap
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.tkip.mycode.databinding.HolderPoiPhotoBinding


/**
 * POI의 사진을 Bitmap으로 바꿔서 rv에 넣으려고 만들었다.
 */
class POIphotoAdapter (val items: ArrayList<Bitmap> ) : RecyclerView.Adapter<POIphotoAdapter.POIphotoViewHolder>() {


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): POIphotoViewHolder {
        val binding = HolderPoiPhotoBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return POIphotoViewHolder(binding)
    }

    override fun onBindViewHolder(h: POIphotoViewHolder, p: Int) {

        h.bind(items[p])

    }

    override fun getItemCount(): Int {
        return items.size
    }


    inner class POIphotoViewHolder(val b: HolderPoiPhotoBinding) : RecyclerView.ViewHolder(b.root) {


        fun bind(bitmap: Bitmap) {
            b.holder = this
            b.bitmap = bitmap
        }


    }
}