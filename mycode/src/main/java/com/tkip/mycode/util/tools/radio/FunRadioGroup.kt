package com.tkip.mycode.util.tools.radio

import android.content.Context
import android.view.LayoutInflater
import android.widget.RadioGroup
import com.tkip.mycode.databinding.ViewRadioBoxBinding

/**
 * Hi~ Created by fabulous on 20-07-11
 */

/** 정렬기준 세팅 */
fun RadioGroup.setQueryList(context: Context,arr: Array<String>) {

    arr.forEachIndexed { i, str ->
        this.addView(ViewRadioBoxBinding.inflate(LayoutInflater.from(context))
                .apply {
                    this.rb.id = i
                    this.title = str
                }.root)
    }


}