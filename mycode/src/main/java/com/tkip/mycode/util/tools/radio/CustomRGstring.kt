package com.tkip.mycode.util.tools.radio

import android.content.Context
import android.view.LayoutInflater
import android.widget.LinearLayout
import androidx.databinding.DataBindingUtil
import com.tkip.mycode.R
import com.tkip.mycode.databinding.CustomRgStringBinding
import com.tkip.mycode.databinding.ViewRadioBoxBinding

/**
 * 선택하는 라디오버튼
 * 세로모드
 */
class CustomRGstring : LinearLayout {
    private lateinit var b: CustomRgStringBinding
//    private var selectedAdType: AdType? = null

    constructor(context: Context) : super(context)
    constructor(context: Context, isVert: Boolean, info: String?, list: ArrayList<String>, initPosition: Int?, onClickRb: (Int) -> Unit) : super(context) {
        b = DataBindingUtil.inflate(LayoutInflater.from(context), R.layout.custom_rg_string, this, true)
        b.info = info
        b.rg1.orientation = if (isVert) VERTICAL else HORIZONTAL

        /** * set RadioGroup */
        list.forEachIndexed { i, str ->
            b.rg1.addView(createRadio(i, str))
        }

        b.rg1.setOnCheckedChangeListener { _, id ->
            onClickRb(id)
        }

        /*** 초기 선택 */
        initPosition?.let { b.rg1.check(it) }

    }

    private fun createRadio(id: Int, title: String) =
            ViewRadioBoxBinding.inflate(LayoutInflater.from(context))
                    .apply {
                        this.rb.id = id
                        this.title = title
                    }.root


}