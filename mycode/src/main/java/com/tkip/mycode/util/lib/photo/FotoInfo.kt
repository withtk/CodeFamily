package com.tkip.mycode.util.lib.photo

import android.os.Parcel
import android.os.Parcelable
import com.tkip.mycode.init.base.createNanoKey
import com.tkip.mycode.model.my_enum.anyKey
import com.tkip.mycode.model.my_enum.getMyDataType
import com.tkip.mycode.util.tools.date.rightNow


/**
 *
 * 사진 명세서 : in Storage
 * foto없는 그냥 url은 어떻게 처리해야 하나..?
 *
 */
class FotoInfo(

        var key: String = createNanoKey(),
        var thumbUrl: String? = null,
        var url: String? = null,

        /////////// 사진이 적용된 model/////////////////////
        var dataTypeNo: Int? = null,
        var modelKey: String? = null,
        //////////////////////////////////////////////////

        var addDate: Long = rightNow(),
        var invalid: Boolean = false  // 삭제여부


) : Parcelable {

    /**     * for FOTO     */
    constructor(any: Any?, foto: Foto) : this(
            thumbUrl = foto.thumbUrl,
            url = foto.url,
            dataTypeNo = any?.getMyDataType()?.no,
            modelKey = any?.anyKey()
    )

    /**     * for Photo     */
    constructor(any: Any?, thumbUrl: String?, url: String?) : this(
            thumbUrl = thumbUrl,
            url = url,
            dataTypeNo = any?.getMyDataType()?.no,
            modelKey = any?.anyKey()
    )

    constructor(source: Parcel) : this(
            source.readString()!!,
            source.readString(),
            source.readString(),
            source.readValue(Int::class.java.classLoader) as Int?,
            source.readString(),
            source.readLong(),
            1 == source.readInt()
    )

    override fun describeContents() = 0

    override fun writeToParcel(dest: Parcel, flags: Int) = with(dest) {
        writeString(key)
        writeString(thumbUrl)
        writeString(url)
        writeValue(dataTypeNo)
        writeString(modelKey)
        writeLong(addDate)
        writeInt((if (invalid) 1 else 0))
    }

    companion object {
        @JvmField
        val CREATOR: Parcelable.Creator<FotoInfo> = object : Parcelable.Creator<FotoInfo> {
            override fun createFromParcel(source: Parcel): FotoInfo = FotoInfo(source)
            override fun newArray(size: Int): Array<FotoInfo?> = arrayOfNulls(size)
        }
    }
}

