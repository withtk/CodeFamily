package com.tkip.mycode.util.tools.anim

import android.view.View
import android.view.animation.Animation
import android.view.animation.AnimationUtils
import com.tkip.mycode.R

class AnimUtil {


    companion object {


        private fun executeAnim(view: View, animId: Int, visibilityStart: Int?, visibilityEnd: Int?) {
            val animation = AnimationUtils.loadAnimation(view.context, animId)
            animation.setAnimationListener(object : Animation.AnimationListener {
                override fun onAnimationStart(animation: Animation) {
                    visibilityStart?.let { view.visibility = it }
                }

                override fun onAnimationEnd(animation: Animation) {
                    visibilityEnd?.let { view.visibility = it }
                }
                override fun onAnimationRepeat(animation: Animation) {}
            })
            view.startAnimation(animation)
        }

        fun show(view: View, resId: Int, reAnimate: Boolean) {
            if (reAnimate) executeAnim(view, resId, View.VISIBLE, null)
            else if (!view.isVisi()) executeAnim(view, resId, View.VISIBLE, null)
        }

        fun hide(view: View, resId: Int, reAnimate: Boolean) {
            if (reAnimate) executeAnim(view, resId, null, View.INVISIBLE)
            else if (view.isVisi()) executeAnim(view, resId, null, View.INVISIBLE)
        }

        fun gone(view: View, resId: Int, reAnimate: Boolean) {
            if (reAnimate) executeAnim(view, resId, null, View.GONE)
            else if (view.isVisi()) executeAnim(view, resId, null, View.GONE)
        }

          fun toggle(view: View, showResId: Int, hideResId: Int, reAnimate: Boolean) {
            if (view.isVisi()) gone(view, hideResId, reAnimate)
            else show(view, showResId, reAnimate)
        }


        /********************************************************************
         *********************************************************************
         * 기본 애니메이션 정리 *********************************************
         *********************************************************************
         *********************************************************************/


        /**********************************************
         * acNormal ************************************/
        fun showNormal(vararg views: View) {
            for (view in views) {
                if (!view.isVisi()) view.visi()
            }
        }

        fun hideNormal(vararg views: View) {
            for (view in views) {
                if (view.isVisi()) view.hide()
            }
        }

        fun goneNormal(vararg views: View) {
            for (view in views) {
                if (view.isVisi()) view.gone()
            }
        }

        /**********************************************
         * acSlide ************************************/
        fun showSlideUp(view: View, reAnimate: Boolean) {
            show(view, R.anim.slide_up_show, reAnimate)
        }

        fun hideSlideDown(view: View, reAnimate: Boolean) {
            hide(view, R.anim.slide_right_hide, reAnimate)
        }

        fun goneSlideDown(view: View, reAnimate: Boolean) {
            gone(view, R.anim.slide_right_hide, reAnimate)
        }

        fun hideSlideDown(isGone: Boolean, view: View, reAnimate: Boolean) {
            if (isGone) gone(view, R.anim.slide_right_hide, reAnimate)
            else hide(view, R.anim.slide_right_hide, reAnimate)
        }

        fun showSlideDown(view: View, reAnimate: Boolean) {
            show(view, R.anim.slide_down_show, reAnimate)
        }

        fun hideSlideUp(view: View, reAnimate: Boolean) {
            hide(view, R.anim.slide_up_hide, reAnimate)
        }

        fun hideSlideUp(isGone: Boolean, view: View, reAnimate: Boolean) {
            if (isGone) gone(view, R.anim.slide_up_hide, reAnimate)
            else hide(view, R.anim.slide_up_hide, reAnimate)
        }

        fun showSlideRight(view: View, reAnimate: Boolean) {
            show(view, R.anim.slide_right_show, reAnimate)
        }

        fun showSlideLeft(view: View, reAnimate: Boolean) {
            show(view, R.anim.slide_left_show, reAnimate)
        }

        fun hideSlideRight(view: View, reAnimate: Boolean) {
            hide(view, R.anim.slide_right_hide, reAnimate)
        }


        /****************************************
         * acToggle******************************/
        fun toggleUpDown(view: View, reAnimate: Boolean) {
            toggle(view, R.anim.slide_down_show, R.anim.slide_up_hide, reAnimate)
        }


        /****************************************
         * acVanish******************************/
        fun showVanishSlideLtoR(view: View) {
            executeAnim(view, R.anim.card_show_hide, View.VISIBLE, View.INVISIBLE)
        }


        /****************************************
         * acFade******************************/
        fun showFadeIn(view: View, reAnimate: Boolean) {
            show(view, R.anim.fade_in, reAnimate)
        }

        fun showFadeIn(isShow: Boolean, view: View, reAnimate: Boolean) {
            if (isShow) show(view, R.anim.fade_in, reAnimate)
            else hide(view, R.anim.fade_out, reAnimate)
        }

        fun hideFadeOut(view: View, reAnimate: Boolean) {
            hide(view, R.anim.fade_out, reAnimate)
        }


        /****************************************
         * acOvershoot******************************/
        fun showOverUp(view: View, reAnimate: Boolean) {
            show(view, R.anim.over_up_show, reAnimate)
        }

        //boolean에 따라 보여지고 감추고 매번 작동해야 할 때.
        fun showOverUp(isShow: Boolean, view: View, reAnimate: Boolean) {
            if (isShow) show(view, R.anim.over_up_show, reAnimate)
            else hideOverDown(view, reAnimate)
        }

        fun hideOverDown(view: View, reAnimate: Boolean) {
            hide(view, R.anim.over_down_hide, reAnimate)
        }

        fun showOverRight(view: View, reAnimate: Boolean) {
            show(view, R.anim.over_to_right_show, reAnimate)
        }

        fun showOverRight(vararg views: View) {
            views.forEach { show(it, R.anim.over_to_right_show, true) }
        }

        fun hideOverRight(view: View, reAnimate: Boolean) {
            hide(view, R.anim.over_to_right_hide, reAnimate)
        }

        fun hideOverRight(vararg views: View) {
            views.forEach { hide(it, R.anim.over_to_right_hide, true) }
        }

        /* fun showBounce(view: View) {
             val animBounce = AnimationUtils.loadAnimation(view.context, R.anim.bounce)
             val myBounceInterpolator = MyBounceInterpolator(0.2, 20)
             animBounce.interpolator = myBounceInterpolator
             view.startAnimation(animBounce)
         }
 */


        /********************************************************************
         *********************************************************************
         * 각각의 상황에 따라 분기 *********************************************
         *********************************************************************
         *********************************************************************/


        fun rotateArrow(view: View) {
            if (view.rotation == 0f) {
                view.animate().setDuration(200).rotation(180f)
            } else {
                view.animate().setDuration(200).rotation(0f)
            }
        }



    }
}