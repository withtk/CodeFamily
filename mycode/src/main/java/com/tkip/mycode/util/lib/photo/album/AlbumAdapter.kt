package com.tkip.mycode.util.lib.photo.album


import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.CompoundButton
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.RecyclerView
import com.tkip.mycode.R
import com.tkip.mycode.admin.versionGteQ
import com.tkip.mycode.databinding.HolderAlbumBinding
import com.tkip.mycode.dialog.toast.TToast
import com.tkip.mycode.util.lib.photo.MyPhoto
import com.tkip.mycode.util.lib.photo.photoview.OnPhotoView
import com.tkip.mycode.util.lib.photo.util.AsyncThumb
import com.tkip.mycode.util.lib.photo.util.AsyncThumbQ
import com.tkip.mycode.util.tools.anim.gone
import com.tkip.mycode.util.tools.anim.visi


class AlbumAdapter(var items: ArrayList<MyPhoto>, private val mListener: OnAlbumListener) : RecyclerView.Adapter<AlbumAdapter.AlbumViewHolder>() {

    private lateinit var activity: AppCompatActivity

    interface OnAlbumListener {
        fun onSelected(myPhoto: MyPhoto, p: Int)
        fun unSelected(myPhoto: MyPhoto, p: Int)
        fun onPhotoView()
    }

    override fun getItemId(position: Int): Long {
        return items[position].photoId.hashCode().toLong()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): AlbumViewHolder {
        this.activity = parent.context as AppCompatActivity
        val binding = HolderAlbumBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return AlbumViewHolder(binding)
    }

    override fun onBindViewHolder(h: AlbumViewHolder, p: Int) {
        h.bind(items[p], p)
    }

    override fun getItemCount(): Int {
        return items.size
    }


    inner class AlbumViewHolder(val b: HolderAlbumBinding) : RecyclerView.ViewHolder(b.root) {

        private lateinit var myPhoto: MyPhoto

        fun bind(myPhoto: MyPhoto, position: Int) {
            this.myPhoto = myPhoto
            b.holder = this
            b.myPhoto = myPhoto

            if(versionGteQ) b.btnExpand.gone() else b.btnExpand.visi()    // 확대보기  분기.


            b.cb.setOnCheckedChangeListener(object : CompoundButton.OnCheckedChangeListener {
                override fun onCheckedChanged(buttonView: CompoundButton?, isChecked: Boolean) {

                    if (isChecked) {
                        if ((activity as AlbumActivity).isMax) {
                            TToast.showAlert(R.string.info_limit_myphoto_album)
                            b.cb.isChecked = !isChecked
                            return
                        }
                        myPhoto.isSel = isChecked
                        mListener.onSelected(myPhoto, position)
                    } else {
                        myPhoto.isSel = isChecked
                        mListener.unSelected(myPhoto, position)
                    }

                }
            })

            /**
             * Thumbnail 만들기
             */
            if (versionGteQ) {   // Q일때는 bitmap 만들기.

                // bitmap null 일때만 async 실행.
                if (myPhoto.bitmap == null) {
                    AsyncThumbQ(activity,
                            success = { thumb ->
                                myPhoto.bitmap = thumb
                                setThumbImage()
                            }).execute(myPhoto.photoId)
                } else setThumbImage()

            } else {
                // thumb이 null 일때만 async 실행.
                if (myPhoto.thumbUri == null) {
                    AsyncThumb(activity,
                            success = { uri ->
                                myPhoto.thumbUri = uri
                                setThumbImage()
                            }).execute(myPhoto)
                    setThumbImage()
                } else setThumbImage()
            }

            b.executePendingBindings()
        }

        private fun setThumbImage() {
            if (versionGteQ) {
                myPhoto.bitmap?.let { b.iv.setImageBitmap(it) }
            } else {
                myPhoto.thumbUri?.let { thumbUri -> b.iv.setImageURI(thumbUri) }
                myPhoto.orientation?.let { orientation -> b.iv.rotation = java.lang.Float.parseFloat(orientation) }
            }
        }


        fun onClickCard() {
            b.cb.isChecked = !b.cb.isChecked

        }


        fun onClickPhotoView() {
            mListener.onPhotoView()
            OnPhotoView.gotoPhtoViewActivityMyPhoto(activity, adapterPosition, items, null)
        }


    }
}