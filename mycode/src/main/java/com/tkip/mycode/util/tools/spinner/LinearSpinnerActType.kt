package com.tkip.mycode.util.tools.spinner

import android.content.Context
import android.view.LayoutInflater
import android.widget.LinearLayout
import androidx.databinding.DataBindingUtil
import com.tkip.mycode.R
import com.tkip.mycode.databinding.ViewSpinnerCommonBinding
import com.tkip.mycode.funs.common.lllogI
import com.tkip.mycode.funs.common.onItemSelected
import com.tkip.mycode.model.my_enum.ActType


/**
 *  spinner for costType
 */
class LinearSpinnerActType : LinearLayout {
    private lateinit var b: ViewSpinnerCommonBinding
    private val items: ArrayList<ActType> = arrayListOf()
    var selectedP: Int = 0

    constructor(context: Context) : super(context)
    constructor(context: Context, list: ArrayList<ActType>, initActTypeNo: Int?, onClick: (s: ActType) -> Unit) : super(context) {
        b = DataBindingUtil.inflate(LayoutInflater.from(context), R.layout.view_spinner_common, this, true)

        items.addAll(list)
//        items.forEach { lllogI("LinearSpinnerActType items status: ${it.title}") }

        b.spin.adapter = SpinnerActTypeAdapter(items)
        b.spin.onItemSelected(
                onNothin = {},
                onSelected = { _, _, p, _ ->
                    selectedP = p
//                    if (p > 0)
//                    items[p]?.let {  }
                    onClick(items[p])
                })

        lllogI("LinearSpinnerActType initStatusNo $initActTypeNo")
        initActTypeNo?.let { b.spin.setSelection(getPosition(it)) }

    }

    private fun getPosition(statusNo: Int): Int {
        items.forEachIndexed { i, it ->
            it?.let { status ->
                if (statusNo == status.no) {
                    lllogI("LinearSpinnerActType i $i status $status")
                    return i
                }
            }
        }
        return 0
    }


}