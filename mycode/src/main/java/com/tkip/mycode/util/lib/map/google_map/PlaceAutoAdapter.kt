package com.tkip.mycode.util.lib.map.google_map

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.google.android.libraries.places.api.model.AutocompletePrediction
import com.tkip.mycode.databinding.HolderPlaceAutoBinding

class PlaceAutoAdapter(val items: ArrayList<AutocompletePrediction>, var onSelect: (AutocompletePrediction) -> Unit) : RecyclerView.Adapter<PlaceAutoAdapter.PlaceAutoViewHolder>() {


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PlaceAutoViewHolder {
        val binding = HolderPlaceAutoBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return PlaceAutoViewHolder(binding)
    }

    override fun onBindViewHolder(h: PlaceAutoViewHolder, p: Int) {

//        h.b.prediction = items[p]

        h.bind(items[p])

    }

    override fun getItemCount(): Int {
        return items.size
    }


    /**
     *
     */
    inner class PlaceAutoViewHolder(val b:  HolderPlaceAutoBinding) : RecyclerView.ViewHolder(b.root) {

        lateinit var prediction: AutocompletePrediction
        fun bind(prediction: AutocompletePrediction) {
            this.prediction = prediction
            b.holder = this

            b.full = prediction.getFullText(null).toString()
            b.primary = prediction.getPrimaryText(null).toString()
            b.second = prediction.getSecondaryText(null).toString()
        }

        fun onClickPrediction() {
            onSelect(prediction)
        }


    }
}