package com.tkip.mycode.util.lib.calendar

import android.content.Context
import com.tkip.mycode.R
import com.tkip.mycode.dialog.alert.checkAndAlertInfo
import com.tkip.mycode.dialog.toast.toastAlert
import com.tkip.mycode.funs.common.lllogD
import com.tkip.mycode.funs.common.lllogI
import com.tkip.mycode.init.base.getInt
import com.tkip.mycode.init.base.getStr
import com.tkip.mycode.util.tools.date.*
import java.util.*


/*** 메세지 : 날짜 선택 최대 가능수 초과할 때    "최대 %d일까지 선택할 수 있습니다." */
val MAX_MULTI_CALENDAR = R.integer.bid_max_select_date.getInt()
val MSG_CALENDAR_OVER_LIMIT = String.format(R.string.cal_over_choose_date.getStr(), MAX_MULTI_CALENDAR)
val AD_BASIC_DATE = 20200101

fun Calendar.toDate8() = this.timeInMillis.getLocalTimeString(FORMAT_DATE_8).toInt()

/*** IntDate8 -> calendar */
fun Int.convertCalendar(): GregorianCalendar {
    val ymd = this.getYMD()
    return GregorianCalendar(ymd.first, ymd.second - 1, ymd.third)
}


/** * IntDate8 -> Y M D 로 자르기. */
fun Int.getYMD(): Triple<Int, Int, Int> {
    val str = this.toString()
    val y = str.substring(0, 4).toInt()
    val m = str.substring(4, 6).toInt()
    val d = str.substring(6, 8).toInt()
    lllogI("IntDate8 getYMD $y $m $d")
    return Triple(y, m, d)
}


/** 1. Int = Date8, list[0] 가장 빠른 날짜.*/
/** 2. return true : 입찰시간 아님. */
fun Int?.isTimeOver(context: Context): Boolean {
    lllogI("calendar this: $this  ")
    val localDate8 = getDate8TodayLocal(LOCALE_PH)   // "20200305"
    lllogI("calendar localDate8: $localDate8  ")

    /**     * 최소 입찰날짜 체크     */
    this?.let {
        if (it <= localDate8.toInt()) {
            toastAlert(R.string.error_not_tomorrow)
            return true
        }
    }

    return isNotBidTime(context)


//    /** 입찰종료시간 체크
//     * 1. 현지날짜 23:00를 millis로 가져오기
//     * 2. rightNow()로 비교하기
//     * */
//    val cal = localDate8.toInt().convertCalendar()
//    cal.set(Calendar.HOUR, 23)
//    cal.set(Calendar.MINUTE, 0)
//    cal.set(Calendar.SECOND, 0)
//    val long11PM = cal.timeInMillis   // long 23:00 in PH
//    lllogI("  calendar   time 23: ${cal.timeInMillis.getLocalTimeString(FORMAT_YEAR)}  ")
//    lllogI("  calendar   time 23: ${cal.timeInMillis}  ")
//    cal.set(Calendar.MINUTE, 59)
//    cal.set(Calendar.SECOND, 59)
//    val long59Min = cal.timeInMillis  // long 23:59 in PH
//    lllogI("  calendar   time 59: ${cal.timeInMillis}  ")
//    lllogI("  calendar   time 59: ${cal.timeInMillis.getLocalTimeString(FORMAT_YEAR)}  ")
//    val longLocalNow = rightNow() + TimeUnit.HOURS.toMillis(timeGap)  // UTC + timeGap = 현지시간 long
//    lllogI("  calendar   timenow: ${longLocalNow}  ")
//    lllogI("  calendar   timenow: ${longLocalNow.getLocalTimeString(FORMAT_YEAR)}  ")
//
//    return if (longLocalNow in (long11PM + 1) until long59Min) {
//        toastAlert(R.string.error_timeover_bidding)
//        true
//    } else false

}


fun isNotBidTime(context: Context): Boolean {
    lllogD("isNotBidTime rightNowPHhour $rightNowPHhour")
    return (rightNowPHhour == "23").checkAndAlertInfo(context, R.string.error_timeover_bidding)
}
