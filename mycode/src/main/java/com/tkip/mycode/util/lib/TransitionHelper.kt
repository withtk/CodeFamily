package com.tkip.mycode.util.lib

import android.app.Activity
import android.content.Context
import android.content.ContextWrapper
import android.content.Intent
import android.view.View
import androidx.core.app.ActivityOptionsCompat
import com.tkip.mycode.funs.common.lllogD

class TransitionHelper {

    companion object {


        fun createTran(context: Context, includeStatusBar: Boolean, arrPair: Array<androidx.core.util.Pair<View, String>>?): ActivityOptionsCompat {
            val decor = (context as Activity).window.decorView

            val statusBar: View? =
                    if (includeStatusBar) decor.findViewById(android.R.id.statusBarBackground)
                    else null
            val navBar = decor.findViewById<View>(android.R.id.navigationBarBackground)

            // Create pair of transition participants.
            val participants = ArrayList<androidx.core.util.Pair<View, String>>(arrPair?.size ?: 0
            + 2)
            addNonNullViewToTransitionParticipants(statusBar, participants)
            addNonNullViewToTransitionParticipants(navBar, participants)

            arrPair?.forEach { p ->
                participants.add(p)
            }

//            participants.addAll(Arrays.asList<androidx.core.util.Pair<View, String>>(*otherParticipants))

//            return participants.toTypedArray()
            return ActivityOptionsCompat.makeSceneTransitionAnimation(context, *participants.toTypedArray())
        }

        private fun addNonNullViewToTransitionParticipants(view: View?, participants: MutableList<androidx.core.util.Pair<View, String>>) {
            if (view != null) {
                participants.add(androidx.core.util.Pair(view, view.transitionName))
            }
        }

        fun startTran(context: Context, intent: Intent, arrPair: Array<androidx.core.util.Pair<View, String>>?) {
            lllogD("TransitionHelper startTran context $context")

            if (arrPair == null) context.startActivity(intent)
            else context.startActivity(intent, createTran(context, false, arrPair).toBundle())
        }

        fun startTran(activity: Activity, intent: Intent, arrPair: Array<androidx.core.util.Pair<View, String>>?) {
            if (arrPair == null) activity.startActivity(intent)
            else activity.startActivity(intent, createTran(activity, false, arrPair).toBundle())
        }


//        private fun unwrap(context: Context): Activity  {
//            var context = context
//            while (context !is Activity && context is ContextWrapper) {
//                context = (context as ContextWrapper).baseContext
//            }
//            return context as Activity
//        }


/*
        private fun addNextFragment(  view: View ) {
            val overlap = true

            val sharedElementFragment2 = MyBookmarkFrag.newInstance( )

            val slideTransition = Slide(Gravity.RIGHT)
            slideTransition.setDuration(500)

            val changeBoundsTransition = ChangeBounds()
            changeBoundsTransition.setDuration(300)

            sharedElementFragment2.setEnterTransition(slideTransition)
            sharedElementFragment2.setAllowEnterTransitionOverlap(overlap)
            sharedElementFragment2.setAllowReturnTransitionOverlap(overlap)
            sharedElementFragment2.setSharedElementEnterTransition(changeBoundsTransition)


            childFragmentManager.beginTransaction()
                    .replace(R.id.frame, sharedElementFragment2)
                    .addToBackStack(null)
                    .addSharedElement(view, R.string.transition_title.getStr())
                    .commit()



        }*/


    }


}