package com.tkip.mycode.util.system.notification

import android.app.Notification
import android.app.NotificationManager
import android.content.ContentResolver
import android.content.Context
import android.graphics.BitmapFactory
import android.graphics.Color
import android.graphics.drawable.BitmapDrawable
import android.net.Uri
import android.os.Build
import android.os.Handler
import androidx.core.app.NotificationCompat
import com.tkip.mycode.R
import com.tkip.mycode.admin.versionGteOreo
import com.tkip.mycode.funs.common.lllogI
import com.tkip.mycode.init.base.Sred
import com.tkip.mycode.init.part.CodeApp
import com.tkip.mycode.init.sredBoolean
import com.tkip.mycode.nav.settings.push_time.OnPushTime
import com.tkip.mycode.util.tools.date.rightNow


const val NOTI_ID = 9876

fun Context.notiManager(): NotificationManager = this.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager

fun Noti.startNoti(context: Context) {
//    if(OnPeer.isMyPeer(noti.senderUid)) return  // 내가 보낸 푸쉬면 알리지 말아.

    /**  현재 보고있는 Activity 여부 */
    if (this.modelKey == CodeApp.curModelKey) {
        lllogI("startNoti modelKey $modelKey CodeApp.curModelKey $CodeApp.curModelKey")
        return
    }


    /**  방해 금지 시간 체크 */
    if (OnPushTime.isNoPushTime()) {
        if (versionGteOreo)
            if (Sred.PUSH_IS_MUTE.sredBoolean(true)) this.nch = NotiChannel.INFO
            else return
        else return
    }

    /**  noti 내용 개별 수정 */
    nch.resetNoti(this)

//    /**  delay 여부 체크 */
//    if (this.delayShowNow(context,myId)) return

//    val myId = if (this.modelKey.isNullBlank(null)) this.modelKey?.cutName(8)?.toInt() else null  // 같은 모델key의 댓글은 하나로 표시하기 위해. 모든 키의 앞부분 숫자만 잘라서 id로 만들기.
    showNoti(context, this, null)
}

private fun showNoti(context: Context, noti: Noti, myId: Int?) {

    val icon = noti.nch.iconId ?: R.drawable.ic_app_push
    val id = myId ?: rightNow().toInt()
    val iconBitmap = (CodeApp.icon as BitmapDrawable).bitmap


//    val intent  = Intent(context, PostActivity::class.java).apply { putExtra(PUT_KEY, noti.modelKey)  }
    //                val intent = Intent(context, ChatRoomListActivity::class.java).apply { flags = Intent.FLAG_ACTIVITY_CLEAR_TOP or Intent.FLAG_ACTIVITY_SINGLE_TOP }
//    val pendingIntent = PendingIntent.getActivity(context, 0, intent, PendingIntent.FLAG_CANCEL_CURRENT)

    val pendingIntent = noti.nch.pendingIntent(context, id, noti)

    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O)
    /*** 8.0 오레오 이상 */
        Notification.Builder(context, noti.nch.id).apply {
            setContentTitle(noti.title)
            setContentText(noti.body)
//            ch.pendingIntent(context, noti)?.let { setContentIntent(it) }
            setContentIntent(pendingIntent)
            setSmallIcon(icon)
            setLargeIcon(iconBitmap)
            style = Notification.BigTextStyle().bigText(noti.body)
//                setStyle(Notification.InboxStyle()
//                        .addLine("message first 1111 제공하는 연속된 텍스트 한 줄 대신 각각 한 줄로 잘린 콘텐츠 텍스트")
//                        .addLine("The S&P 500, Dow and Nasdaq tumbled Wednesday as the volatility of the past couple weeks extended further.")
//                        .addLine("The Dow shed more than 1,400 points during the session, sending it more than 20% below its recent high from February and into a bear market.")
//                        .addLine("4 test")
//                        .addLine("5 test")
//                        .addLine("6 test")
//                        .addLine("7 test") )
//                setLargeIcon()
//                setWhen()
            setShowWhen(true)
            setAutoCancel(true)  // 알림 터치시 반응 후 삭제
            build().apply {
                context.notiManager().notify(id, this)
            }
        }

    /*** 하위버전 */
    else NotificationCompat.Builder(context, noti.nch.id).apply {

        setAutoCancel(true) // 알림 터치시 반응 후 삭제
        setSmallIcon(icon)
        setLargeIcon(BitmapFactory.decodeResource(context.resources, icon))
        setTicker(noti.title)
        setContentTitle(noti.title)
        setContentText(noti.body)
        //todo: pending 수정 할 것.
        noti.nch.pendingIntent(context, id, noti)?.let { setContentIntent(it) }
        setWhen(System.currentTimeMillis())
        setLights(Color.RED, 3000, 3000)
        setSound(Uri.parse(ContentResolver.SCHEME_ANDROID_RESOURCE + "://" + context.packageName + "/raw/default_ios_sms"))
        setVibrate(longArrayOf(100, 1000, 1000, 100, 100, 100, 100, 100))
        build().apply {
            context.notiManager().notify(id, this)
        }
    }
}


/****************************************************************************************
 * 중복된 알림 한번만 보여주기
 ****************************************************************************************/
var notiCount = 0

/**
 * true : 노티 캔슬.
 */
fun Noti.delayShowNow(context: Context, myId: Int?): Boolean {
    return when (this.nch) {
        NotiChannel.BID_FAIL -> {
            /** 유찰은 무조건 딜레이 10초후에 푸쉬 한개만 날리기. */
            Handler().postDelayed({
                lllogI("startNoti dontShowNow  Handler  ")
                this.body = "$this.modelTitle 외 ${notiCount}개 유찰"
                showNoti(context, this, myId)
                notiCount = 0
            }, 10000)
            notiCount++
            true
        }
        else -> false
    }
}





