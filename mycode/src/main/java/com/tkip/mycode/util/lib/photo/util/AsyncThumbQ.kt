package com.tkip.mycode.util.lib.photo.util

import android.content.Context
import android.graphics.Bitmap
import android.net.Uri
import android.os.AsyncTask
import android.provider.MediaStore
import com.tkip.mycode.funs.common.lllogI
import java.lang.ref.WeakReference


/**
 * version Q thumb만들기
 */
class AsyncThumbQ(context: Context, var success: (Bitmap?) -> Unit) : AsyncTask<String, Int, Bitmap>() {

    private var wCon: WeakReference<Context> = WeakReference(context)

    override fun doInBackground(vararg photoId: String?): Bitmap? {
        return getThumb4Q( photoId[0]!!)
    }

    override fun onPostExecute(result: Bitmap?) {
        super.onPostExecute(result)
        lllogI("AsyncThumbQ onPostExecute result $result")
        success(result)
    }

    private fun getThumb4Q(imageId: String): Bitmap? {
        val contentUri = Uri.withAppendedPath(
                MediaStore.Images.Media.EXTERNAL_CONTENT_URI,
                imageId
        )

        var bitmap: Bitmap? = null
        wCon.get()?.let {
            bitmap = contentUri.createMyThumb( )
        }
        return bitmap
    }
}

