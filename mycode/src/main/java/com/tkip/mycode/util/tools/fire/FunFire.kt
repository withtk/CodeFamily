package com.tkip.mycode.util.tools.fire

import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.firestore.QuerySnapshot
import com.google.firebase.firestore.WriteBatch
import com.tkip.mycode.R
import com.tkip.mycode.dialog.progress.MyCircle
import com.tkip.mycode.dialog.toast.toastNormal
import com.tkip.mycode.funs.common.OnException
import com.tkip.mycode.funs.common.lllogD
import com.tkip.mycode.funs.common.lllogI
import com.tkip.mycode.init.*
import com.tkip.mycode.init.base.createKey
import com.tkip.mycode.model.cost.Cost
import com.tkip.mycode.model.cost.pay.PAY_ACT_TYPE_LIST
import com.tkip.mycode.model.cost.pay.getSum
import com.tkip.mycode.model.grant.Grant
import com.tkip.mycode.model.my_enum.Status
import com.tkip.mycode.model.mytag.MyTag
import com.tkip.mycode.model.peer.OnPeer
import com.tkip.mycode.model.peer.isAdmin
import com.tkip.mycode.model.ticket.Ticket


/************************************************
 * FireStore
 *************************************************/


/************************************************
 * QuerySnapshot
 *************************************************/
/** * last Doc 저장. */
fun QuerySnapshot.setLastSnap() = if (size() > 0) documents[size() - 1] else null

/** * db의 마지막 데이터인지 설정. */
fun QuerySnapshot.setEndData(LIMIT: Long) = size() < LIMIT


/**
 * 1개의 doc 받아올 때만 사용.
 * size는 무조건 짝수 (collection, document)
 */
fun docRef(vararg cols: String) =
        when (cols.size) {
            2 -> FirebaseFirestore.getInstance().collection(cols[0]).document(cols[1])
            4 -> FirebaseFirestore.getInstance().collection(cols[0]).document(cols[1]).collection(cols[2]).document(cols[3])
            6 -> FirebaseFirestore.getInstance().collection(cols[0]).document(cols[1]).collection(cols[2]).document(cols[3]).collection(cols[4]).document(cols[5])
            else -> FirebaseFirestore.getInstance().collection("common").document(createKey())   // 기본값은 그냥 넣었음.
        }

/**
 * collection 전체 받아올 Ref
 * size는 무조건 홀수 (collection, document, collection)
 */
fun colRef(vararg cols: String) =
        when (cols.size) {
            1 -> FirebaseFirestore.getInstance().collection(cols[0])
            3 -> FirebaseFirestore.getInstance().collection(cols[0]).document(cols[1]).collection(cols[2])
            5 -> FirebaseFirestore.getInstance().collection(cols[0]).document(cols[1]).collection(cols[2]).document(cols[3]).collection(cols[4])
            else -> FirebaseFirestore.getInstance().collection("<common>")   // 기본값은 그냥 넣었음.
        }

/************************************** * stFiree *************************************/
//fun DocumentReference.gget(success: (DocumentSnapshot?) -> Unit, fail: (Exception) -> Unit) {
//    if (OnPeer.notAllowedDB()) {
//        fail(Exception())
//        return
//    }
//    get().addOnSuccessListener { success(it) }
//            .addOnFailureListener { e ->
//                MyProgress.cancel()
//                fail(e)
//            }
//}
//
//fun DocumentReference.quad(act: Int, key: String?, any: Any?): Quad<Int, DocumentReference, String?, Any?> = Quad(act, this, key, any)


/**         * 정리된 batch를 commit         */
fun WriteBatch.myCommit(methodName: String?, success: () -> Unit, fail: (Exception) -> Unit) {
    lllogI("FunFire.myCommit ($methodName)")
    if (OnPeer.notAllowedDB()) {
        fail(Exception())
        return
    }
    commit()
            .addOnSuccessListener {
                lllogD("myCommit: $methodName success!")
                success()
                MyCircle.cancel()
//                OnTag.clearMyTagList()
            }.addOnFailureListener { e ->
                lllogI("myCommit addOnFailureListener ${e.message} ")
                MyCircle.cancel()
                methodName?.let { OnException.dbfailed("myCommit: $methodName fail :", e) }
                fail(e)
            }
}


/******************** commonStatus : post,flea,room,store *************************************************/
fun commonStatusUpdate(child: String, key: String, status: Status, success: () -> Unit, fail: (Exception) -> Unit) {
    batch().apply { update(docRef(child, key), CH_STATUS_NO, status.no) }
            .myCommit("commonStatusUpdate child: $child ",
                    success = {
                        if (status == Status.DELETED_USER) toastNormal(R.string.complete_delete)
                        if (isAdmin()) toastNormal(status.msg)
                        success()
                    }, fail = { fail(it) })
}

fun commonStatusUpdate(child: String, key: String, child2: String, key2: String, status: Status, success: () -> Unit, fail: (Exception) -> Unit) {
    batch().apply { update(docRef(child, key, child2, key2), CH_STATUS_NO, status.no) }
            .myCommit("commonStatusUpdate child: $child ",
                    success = {
                        if (status == Status.DELETED_USER) toastNormal(R.string.complete_delete)
//                        if (isAdmin()) toastNormal(status.msg)
                        success()
                    }, fail = { fail(it) })
}


/************************************** * stBatch *************************************/
fun batch() = FirebaseFirestore.getInstance().batch()

//fun WriteBatch.addDeviceInfo(deviceInfo: Device) {
//    set(docRef(CH_PEER, deviceInfo.uid, CH_DEVICE_INFO, deviceInfo.key), deviceInfo)
//    set(docRef(CH_DEVICE_INFO, deviceInfo.uid), deviceInfo)
//}

fun WriteBatch.addCost(cost: Cost) {
    set(docRef(EES_COST, cost.key), cost)
}

/** * 일괄 add Cost */
fun WriteBatch.addCostWithList(any: Any) {
    if (PAY_ACT_TYPE_LIST.getSum() > 0) {
        PAY_ACT_TYPE_LIST.forEach { actType -> addCost(Cost.new(null, actType, any)) }
        PAY_ACT_TYPE_LIST.clear()
    }
}

/** act : 비용없이 무관하게 행동만 올릴 때 (COST와 같은 index에 들어가야 한다.) */
fun WriteBatch.addAct(cost: Cost) {
    set(docRef(EES_COST, cost.key), cost)
}

fun WriteBatch.addBill(ticket: Ticket) {
    set(docRef(EES_BILL, ticket.key), ticket)
}

fun WriteBatch.addMytag(mytag: MyTag) {
    set(docRef(EES_TAG, mytag.key), mytag)
}

fun WriteBatch.addGrant(grant: Grant) {
    set(docRef(EES_GRANT, grant.key), grant)
}

fun WriteBatch.delGrant(key: String) {
    delete(docRef(EES_GRANT, key))
}

fun WriteBatch.delBanner(key: String) {
    delete(docRef(EES_BANNER, key))
}

/** 모델 상태변경 */
fun WriteBatch.updateStatus(child: String, key: String, status: Status) {
    update(docRef(child, key), CH_STATUS_NO, status.no)
}






