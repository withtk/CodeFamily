package com.tkip.mycode.util.lib.photo.saf

import android.content.Context
import android.content.Intent
import android.net.Uri
import android.os.ParcelFileDescriptor
import android.provider.MediaStore
import com.tkip.mycode.funs.common.lllogI
import com.tkip.mycode.funs.common.lllogW
import com.tkip.mycode.util.lib.photo.Foto
import com.tkip.mycode.util.lib.photo.util.createMyThumb
import com.tkip.mycode.util.lib.photo.view.LinearAddPhotoBox
import java.io.FileNotFoundException
import java.io.IOException




/** * SAF에서 받은 멀티사진 -> selectedFotoList 재세팅. */
fun LinearAddPhotoBox.resetFromSAFmulti(  intent: Intent?) {
    intent?.let {
        it.clipData?.let { data ->

            val count = data.itemCount
            lllogI("REQ_PICK_MULTI resetFromSAF count $count")

            (0 until count).forEach { i ->
                data.getItemAt(i)?.uri?.let { uri ->
                    lllogI("REQ_PICK_MULTI resetFromSAF uri $uri")
                    val thumb = uri.createMyThumb( )
                    this.selectedFotoList.add(Foto(uri,thumb))
                    lllogI("REQ_PICK_MULTI getThumb bitmap $thumb")
                }
            }
        }
    }

//                    val imgUri0 = data.getItemAt(0)?.uri
//                    val imgUri1 = data.getItemAt(1)?.uri
//                    val imgUri2 = data.getItemAt(2)?.uri
//                    lllogI("REQ_PICK_MULTI imgUri1 $imgUri1")
//                    lllogI("REQ_PICK_MULTI imgUri2 $imgUri2")
//                imgUri0?.let { dumpImageMetaData(it) }


}

//fun Uri.createMyThumb(context: Context ): Bitmap? {
//
//    var thumbBitmap: Bitmap? = null
//    thumbBitmap = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
//        return context.contentResolver.loadThumbnail(this, Size(500, 500), null)
//    } else null
//    return thumbBitmap
//}


/**
 * Uri가 살아있는지 확인
 */
fun Uri.isExist(context: Context): Boolean {
    val contentResolver = context.contentResolver
    var pfd: ParcelFileDescriptor? = null
    try {
        pfd = contentResolver.openFileDescriptor(this, "r")
        return pfd != null
    } catch (e: FileNotFoundException) {
        lllogW(e.toString())
    } finally {
        try {
            pfd?.close()
        } catch (e: IOException) {
            lllogW(e.toString())
        }
    }
    return false
}

fun Uri.isExist2(context: Context): Boolean {
    val cursor = context.contentResolver.query(
            this, // 미디어 Uri
            arrayOf(MediaStore.MediaColumns._ID),
            null,
            null,
            null)

    cursor?.let {
        lllogI("fdssdf isExist2 ${it.count != 0}")
        return it.count != 0
    }
    return false
}