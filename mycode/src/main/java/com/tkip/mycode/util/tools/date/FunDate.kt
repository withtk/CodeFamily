package com.tkip.mycode.util.tools.date

import android.annotation.SuppressLint
import com.tkip.mycode.R
import com.tkip.mycode.funs.common.lllogI
import com.tkip.mycode.init.part.CodeApp
import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.*
import java.util.concurrent.TimeUnit

/*************************************************************************************/

val LOCALE_KR = "Asia/Seoul"
val LOCALE_PH = "Asia/Manila"
val LOCALE_FR = "Europe/Paris"
val LOCALE_LA = "America/Los_Angeles"
val LOCALE_VN = "Asia/Ho_Chi_Minh"

private val MINUTE = TimeUnit.MINUTES.toSeconds(1)
private val HOUR = TimeUnit.HOURS.toSeconds(1)
private val DAY = TimeUnit.DAYS.toSeconds(1)
private val MyWEEK = TimeUnit.DAYS.toSeconds(7)
private val MONTH = TimeUnit.DAYS.toSeconds(30)
private val YEAR = TimeUnit.DAYS.toSeconds(365)

/*************************************************************************************/
/******************** Date ***********************************************************/
/*************************************************************************************/
//val FORMAT_SUMMARY by lazy {  SimpleDateFormat("MMMd, a hh:mm", Locale.getDefault())}
//val FORMAT_BASIC  by lazy {SimpleDateFormat("_yyyy_MMdd_hh:mm:ss_", Locale.getDefault())}
//val FORMAT_YEAR_DAY  by lazy {SimpleDateFormat("MMM d, EEE yyyy a h:mm", Locale.getDefault())}
//val FORMAT_YEAR  by lazy {SimpleDateFormat("yyyy-MM-dd HH:mm", Locale.getDefault())}
//val FORMAT_YEAR_MONTH_DATE  by lazy {SimpleDateFormat("yyMMdd", Locale.getDefault())}
//val FORMAT_MONTH  by lazy {SimpleDateFormat("MM.dd", Locale.getDefault())}
//val FORMAT_DATE_TIME  by lazy {SimpleDateFormat("MM.dd HH:mm", Locale.getDefault())}
//val FORMAT_TIME  by lazy {SimpleDateFormat("a hh:mm", Locale.getDefault())}
//val FORMAT_TIME_ONLY  by lazy {SimpleDateFormat("HH:mm", Locale.getDefault()) }  //24시간 형식
//val FORMAT_HOUR by lazy {SimpleDateFormat("HH", Locale.getDefault()) }  //24시간 형식
//
//val FORMAT_DATE_8  by lazy {SimpleDateFormat("yyyyMMdd", Locale.getDefault())}


@SuppressLint("ConstantLocale")
val FORMAT_SUMMARY = SimpleDateFormat("MMMd, a hh:mm", Locale.getDefault())
@SuppressLint("ConstantLocale")
val FORMAT_BASIC = SimpleDateFormat("_yyyy_MMdd_hh:mm:ss_", Locale.getDefault())
@SuppressLint("ConstantLocale")
val FORMAT_YEAR_DAY = SimpleDateFormat("MMM d, EEE yyyy a h:mm", Locale.getDefault())
@SuppressLint("ConstantLocale")
val FORMAT_YEAR = SimpleDateFormat("yyyy-MM-dd HH:mm", Locale.getDefault())
@SuppressLint("ConstantLocale")
val FORMAT_YMD_STRING = SimpleDateFormat("yyyy.MM.dd.", Locale.getDefault())
@SuppressLint("ConstantLocale")
val FORMAT_YEAR_MONTH_DATE = SimpleDateFormat("yyMMdd", Locale.getDefault())
@SuppressLint("ConstantLocale")
val FORMAT_MONTH = SimpleDateFormat("MM.dd", Locale.getDefault())
@SuppressLint("ConstantLocale")
val FORMAT_DATE_TIME = SimpleDateFormat("MM.dd HH:mm", Locale.getDefault())
@SuppressLint("ConstantLocale")
val FORMAT_TIME = SimpleDateFormat("a hh:mm", Locale.getDefault())
@SuppressLint("ConstantLocale")
val FORMAT_TIME_ONLY = SimpleDateFormat("HH:mm", Locale.getDefault())   //24시간 형식
@SuppressLint("ConstantLocale")
val FORMAT_HOUR = SimpleDateFormat("HH", Locale.getDefault())   //24시간 형식

@SuppressLint("ConstantLocale")
val FORMAT_DATE_8 = SimpleDateFormat("yyyyMMdd", Locale.getDefault())

//private fun getSimpleDateFormat(format:String,locale:Locale) = SimpleDateFormat(format, locale)
//val sdfsdfsd by lazy { SimpleDateFormat("MMMd, a hh:mm", Locale.getDefault()) }


/*******************************************************************************************/
/*******************************************************************************************/

fun rightNow() = System.currentTimeMillis()

val rightNowHour = rightNow().getLocalTimeString(FORMAT_HOUR).toInt()

@SuppressLint("SimpleDateFormat")
val rightNowUTChour = SimpleDateFormat("HH").apply { timeZone = TimeZone.getTimeZone("UTC") }.format(rightNow())


@SuppressLint("SimpleDateFormat")
val rightNowPHhour: String = SimpleDateFormat("HH").apply { timeZone = TimeZone.getTimeZone(LOCALE_PH) }.format(rightNow())   // 현재 PH시간

//fun Long.getYMDstring() = this.getLocalTimeString(FORMAT_YEAR_MONTH_DATE)
//fun getYMDstring(y:Int,m:Int,d:Int) = "$y${m+1}$d"
fun getMilis(y: Int, m: Int, d: Int) = GregorianCalendar(y, m - 1, d).timeInMillis

fun Long.getDiffDay(end: Long) = TimeUnit.MILLISECONDS.toDays(end - this).toInt() + 1
//fun todayString() = rightNow().getYMDstring()
//fun todayDate8()

fun Long.getDate8(locale: String): Int = this.getLocalTimeString(FORMAT_DATE_8, locale).toInt()
fun Long.getDate8(): Int = this.getLocalTimeString(FORMAT_DATE_8).toInt()
fun getIntDate8TodayPH(): Int = rightNow().getLocalTimeString(FORMAT_DATE_8, LOCALE_PH).toInt()
fun getDate8Today(): String = rightNow().getLocalTimeString(FORMAT_DATE_8)
fun getDate8TodayLocal(locale: String): String = rightNow().getLocalTimeString(FORMAT_DATE_8, locale)
/** * 몇 일 전 */
fun getLongDayBefore(day: Int) = rightNow() - TimeUnit.DAYS.toMillis(day.toLong())


/*******************************************************************************************/
/*******************************************************************************************/

/**
 * Long -> String  한국시간
 */
fun Long.getKoreanTimeString(format: SimpleDateFormat): String {
    format.timeZone = TimeZone.getTimeZone(LOCALE_KR)
    return format.format(this)
}


/**
 * Long -> String  단말기 로컬시간
 */
fun Long.getLocalTimeString(format: SimpleDateFormat): String {
    format.timeZone = TimeZone.getDefault()
    return format.format(this)
}

fun Long.getLocalTimeString(format: SimpleDateFormat, id: String): String {
    format.timeZone = TimeZone.getTimeZone(id)
    return format.format(this)
}

fun getStartDateInt() = rightNow().getKoreanTimeString(FORMAT_YEAR_MONTH_DATE).toInt()

/**
 * Long -> String 현재까지 경과시간
 */
fun Long.getElapsedTimeString(): String {

    val context = CodeApp.getAppContext()
    val elapsedSeconds = (rightNow() - this) / 1000
    return when {
        elapsedSeconds >= YEAR -> (elapsedSeconds / YEAR).toString() + context.getString(com.tkip.mycode.R.string.elapsed_years)
        elapsedSeconds >= MONTH * 3 -> (elapsedSeconds / MONTH).toString() + context.getString(com.tkip.mycode.R.string.elapsed_months)
        elapsedSeconds >= MyWEEK -> (elapsedSeconds / MyWEEK).toString() + context.getString(com.tkip.mycode.R.string.elapsed_weeks)
        elapsedSeconds >= DAY -> (elapsedSeconds / DAY).toString() + context.getString(com.tkip.mycode.R.string.elapsed_days)
        elapsedSeconds >= HOUR -> (elapsedSeconds / HOUR).toString() + context.getString(com.tkip.mycode.R.string.elapsed_hours)
        elapsedSeconds >= MINUTE -> (elapsedSeconds / MINUTE).toString() + context.getString(com.tkip.mycode.R.string.elapsed_minutess)
        else -> context.getString(com.tkip.mycode.R.string.elapsed_now)
    }
}

fun Long.getElapsedTimeStringShort(): Pair<String, Int?> {
    val context = CodeApp.getAppContext()
    val elapsedSeconds = (System.currentTimeMillis() - this) / 1000
    return when {
        elapsedSeconds >= YEAR -> Pair((elapsedSeconds / YEAR).toString() + context.getString(R.string.elapsed_eng_years), null)
        elapsedSeconds >= MONTH * 3 -> Pair((elapsedSeconds / MONTH).toString() + context.getString(R.string.elapsed_eng_months), null)
        elapsedSeconds >= MyWEEK -> Pair((elapsedSeconds / MyWEEK).toString() + context.getString(R.string.elapsed_eng_weeks), null)
        elapsedSeconds >= DAY -> Pair((elapsedSeconds / DAY).toString() + context.getString(R.string.elapsed_eng_days), null)
        elapsedSeconds >= HOUR -> Pair((elapsedSeconds / HOUR).toString() + context.getString(R.string.elapsed_eng_hours), R.color.colorAccent2)
        elapsedSeconds >= MINUTE -> Pair((elapsedSeconds / MINUTE).toString() + context.getString(R.string.elapsed_eng_minutess), R.color.colorAccent2)
        else -> Pair(context.getString(R.string.elapsed_now), null)
    }
}



/**
 * String -> Date 포멧형식="hh:mm"
 */
fun String.getDateTimeOnly(): Date {

    FORMAT_TIME_ONLY.timeZone = TimeZone.getDefault()
    try {
        return FORMAT_TIME_ONLY.parse(this)
    } catch (e: ParseException) {
        return Date(0)
    }
}


/**
 * 목표시간이 현재보다 높을때만 사용가능.
 *
 */
fun Long.getDayCount(): Triple<Int? , Int ?, Int ?> {

    val differ = this - rightNow()   // 시간차 long 구하기.
    val mins = differ / 1000 / 60    // 전체 분
    val hours = mins / 60            // 전체 시간


    val day = hours / 24     // 전체 날 수
    val hour = hours % 24    // 날 빼고 남은 시간 수
    val min = mins % 60      // 시간으로 나누고 남은 분 수

    lllogI(" getDays  differ:$differ  mins:$mins  hours:$hours    ")
    lllogI(" getDays  day:$day  hour:$hour  min:$min  ")

    return Triple(if(day==0L)null else day.toInt(),if(hour==0L)null else hour.toInt(),if(min==0L)null else min.toInt())
}


/** Converting from String to Date **/
/*

fun String.getDateWithServerTimeStamp(): Date? {
//    val dateFormat = SimpleDateFormat(MyTime.FORMAT_LocalTimeFormatter )
    val dateFormat = FORMAT_SUMMARY

    dateFormat.timeZone = TimeZone.getDefault()
    try {
        return dateFormat.parse(this)
    } catch (e: ParseException) {
        return null
    }
}

fun Date.getStringTimeStampWithDate(): String {
//    val dateFormat = SimpleDateFormat(FORMAT_LocalTimeFormatter, Locale.getDefault())
    val dateFormat = FORMAT_SUMMARY
    dateFormat.timeZone = TimeZone.getDefault()
    return dateFormat.format(this)
}
*/


/*
fun Date.getMyString(dateFormat : SimpleDateFormat): String {
    dateFormat.timeZone = TimeZone.getDefault()
    return dateFormat.format(this)
}



fun getMyMyLongGMT(strGMTTime: String): Long {
    // formatter는 반드시 같은 format을 줘야 한다.
    var millisecond: Long = 0
    try {
        millisecond = FORMAT_BASIC.parse(strGMTTime).time
    } catch (e: ParseException) {
        if (strGMTTime.length > 0)
            e.printStackTrace()
        else
            System.err.println(e.title)
    }
    return millisecond
}*/



