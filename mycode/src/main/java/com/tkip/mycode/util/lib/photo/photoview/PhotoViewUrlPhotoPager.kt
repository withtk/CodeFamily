package com.tkip.mycode.util.lib.photo.photoview

import android.text.method.ScrollingMovementMethod
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.viewpager.widget.PagerAdapter
import androidx.viewpager.widget.ViewPager
import com.tkip.mycode.databinding.ItemPhotoViewUrlPhotoBinding
import com.tkip.mycode.util.tools.anim.AnimUtil
import com.tkip.mycode.util.lib.photo.Foto


class PhotoViewUrlPhotoPager(val items: ArrayList<Foto>, val click: () -> Unit) : PagerAdapter() {

    lateinit var b: ItemPhotoViewUrlPhotoBinding

    override fun instantiateItem(container: ViewGroup, p: Int): Any {
        b = ItemPhotoViewUrlPhotoBinding.inflate(LayoutInflater.from(container.context), container, false)
        b.pager = this
        b.foto = items[p]

        b.photoView.setOnClickListener {
            showHideTV()
            click()
        }
        b.tvComment.movementMethod = ScrollingMovementMethod()

        container.addView(b.root)
        return b.root
    }

    override fun isViewFromObject(view: View, obj: Any): Boolean {
        return view == obj
    }

    override fun destroyItem(container: ViewGroup, position: Int, `object`: Any) {
        (container as ViewPager).removeView(`object` as View)
    }

    override fun getCount(): Int {
        return items.size
    }


   private fun showHideTV() {
        if (b.tvComment.visibility == View.VISIBLE) AnimUtil.hideSlideDown(b.tvComment, true)
        else AnimUtil.showSlideUp(b.tvComment, true)
    }


}