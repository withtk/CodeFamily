package com.tkip.mycode.util.lib.elastic

import com.tkip.mycode.funs.common.lllogI
import com.tkip.mycode.init.*
import com.tkip.mycode.init.base.Sred
import com.tkip.mycode.model.board.Board
import com.tkip.mycode.model.board.BoardType
import com.tkip.mycode.model.mytag.MyTag
import com.tkip.mycode.model.mytag.TagTYPE
import com.tkip.mycode.util.tools.fire.Firee
import com.tkip.mycode.util.tools.fire.batch
import com.tkip.mycode.util.tools.fire.docRef
import com.tkip.mycode.util.tools.fire.myCommit

/**
 * 필수적인 데이타들 한번에 넣기.
 */
class OnDBBasic {

    companion object {


        /****************************************************************************************************************************************
         *************************** 처음 셋팅할 태그  ********************************************************************************************
         *****************************************************************************************************************************************/

        /**      기본 태그      */
        fun uploadAllTags() {
            val ban = true
            val loc = true
            val flea = true
            val room = true
            val built = true

            /**      * 금칙어 목록         */
            if (ban) {
//            val point =  100
//            val strings = "씨발,개새끼"
                val point = 1000
                val strings = "개넌,개년,개놈,개뇬,개새끼,게세끼,꼬추,늬미,니기미,니미,닝기리,색스,색쑤,색쓰,섹수,섹스,섹쑤,섹쓰,쌍놈,쌍년,쌕수,쌕스,쌕쑤,쌕쓰,썅놈,썅년,씨발,염병,옘병,조까,조또,좆,창녀"
                val arr = strings.split(",")
                var b = batch()
                arr.forEachIndexed { i, str ->
                    val myTag = MyTag(str, TagTYPE.BAN)
                    myTag.addDate = 1584853062758
                    myTag.totalPoint = point   // 중요도
                    b.set(docRef(EES_TAG, myTag.key), myTag)
                    if (i == 499) {
                        b.myCommit("fireeUploadBanTags", {}, {})
                        b = batch()
                    }
                }
                b.myCommit("fireeUploadBanTags", {}, {})
            }
            /**      * 지역명 목록         */
            if (loc) {
                var b = batch()
                val arr1 = arrayOf("마닐라", "세부", "앙헬레스", "마카티", "올티가스", "보니파시오", "막탄", "만달루용", "문틴루파", "바기오", "바스코", "바콜로드", "바탕가스", "팜팡가", "안티폴로", "올롱가포", "카비테", "칼람바", "칼로오칸", "타기그", "투게가라오", "파라냐케", "파사이", "파시그", "파가디안")
                arr1.forEachIndexed { i, str ->
                    val myTag = MyTag(str, TagTYPE.LOC)
                    myTag.totalPoint = 1000 - i
                    b.set(docRef(EES_TAG, myTag.key), myTag)
                    if (i == 499) {
                        b.myCommit("fireeUploadLocTags", {}, {})
                        b = batch()
                    }
                }
                val arr = arrayOf("기완", "나보타스", "다구판", "다바오", "다스마리냐스", "다피탄", "두마게테", "디폴로그", "라스피냐스", "라오아그", "라트리니다드", "라푸라푸", "레가스피", "로하스", "롬블론", "루방", "루세나", "마라위", "마리키나", "마스바테", "만다우에", "말라본", "말라이", "말라이발라이", "말롤로스", "발레르", "보아크", "보카우에", "부투안", "비간", "산티아고", "라우니온", "산호세델몬테", "산후안", "삼보앙가", "수리가오", "시키호르", "실라이", "실랑", "알라방", "오로키에타", "오르모크", "오사미스", "이바", "이사벨라", "일로일로", "일리간", "제너럴산토스", "카가얀데오로", "카바나투안", "카방칼란", "칼라판", "칼리보", "케손시티", "코로나달", "코타바토", "키다파완", "타가이타이", "타굼", "타그빌라란", "타를라크", "타바코", "타부크", "타클로반", "파테로스", "푸에르토프린세사", "프로스페리다드", "홀로")
                arr.forEachIndexed { i, str ->
                    val myTag = MyTag(str, TagTYPE.LOC)
                    myTag.totalPoint = 100
                    b.set(docRef(EES_TAG, myTag.key), myTag)
                    if (i == 499) {
                        b.myCommit("fireeUploadLocTags", {}, {})
                        b = batch()
                    }
                }
                b.myCommit("fireeUploadLocTags", {}, {})
                lllogI("OnTotalDB  fireeUploadLocTags  myCommit!   ")
            }
            /**      * 중고거래 목록         */
            if (flea) {
                val strings = "침대,LEDTV,노트북,PC데스크탑,스마트폰,에어컨,냉장고,세탁기,랜드로버,고프로,갤럭시s20,아이폰,모니터LCD,공기청정기,책상,옷장,라이트"
                val arr = strings.split(",")
                var b = batch()
                arr.forEachIndexed { i, str ->
                    val myTag = MyTag(str, TagTYPE.FLEA)
                    myTag.totalPoint = 100
                    b.set(docRef(EES_TAG, myTag.key), myTag)
                    if (i == 499) {
                        b.myCommit("uploadBuiltTags", {}, {})
                        b = batch()
                    }
                }
                b.myCommit("uploadBuiltTags", {}, {})
            }
            /**      * 부동산 목록         */
            if (room) {
                val strings = "베이뷰,파인크래스트,BF홈,세렌드라 콘도,아비다 베르테"
                val arr = strings.split(",")
                var b = batch()
                var total = 100
                arr.forEachIndexed { i, str ->
                    val myTag = MyTag(str, TagTYPE.BUILT)
                    myTag.totalPoint = total
                    b.set(docRef(EES_TAG, myTag.key), myTag)
                    total--
                    if (i == 499) {
                        b.myCommit("fireeuploadRoomTags", {}, {})
                        b = batch()
                    }
                }
                b.myCommit("fireeuploadRoomTags", {}, {})
            }
            /**      * 빌트인 목록         */
            if (built) {
                val strings = "침대,에어컨,냉장고,세탁기,인덕션,하이라이트,전자렌지,TV(텔레비전),선풍기,에어 서큘레이터,공기청정기,책상,옷장,비데,도어락,신발장,식기세척기,다리미"
                val arr = strings.split(",")
                var b = batch()
                var total = 100
                arr.forEachIndexed { i, str ->
                    val myTag = MyTag(str, TagTYPE.BUILT)
                    myTag.totalPoint = total
                    b.set(docRef(EES_TAG, myTag.key), myTag)
                    total--
                    if (i == 499) {
                        b.myCommit("uploadBuiltTags", {}, {})
                        b = batch()
                    }
                }
                b.myCommit("uploadBuiltTags", {}, {})
            }

        }


        /**************************************************************************************
         *************************** sttBoard  **************************************************
         *************************************************************************************/

        /**
         * 최초 한번 있어야하는 보드 만들기
         * system/board/key
         * postRecent, flea, room
         */
        fun createBoardOnce() {
           val phCodeBoard = Board().apply {
                key = PH_CODE_BOARD_KEY
                boardId = "phcode"
                title = "필리핀코드 공지사항"
                boardTypeNo = BoardType.USER.no
                allowSearch = true
                Sred.putObjectBoard(key, this)   // sred에 저장
            }

            val  recentBoard = Board().apply {
                key = RECENT_BOARD_KEY
                boardId = "recent"
                title = "최근 베스트"
                boardTypeNo = BoardType.FIXED.no
                allowSearch = false
                Sred.putObjectBoard(key, this)   // sred에 저장
            }

            val fleaBoard = Board().apply {
                key = FLEA_BOARD_KEY
                boardId = "flea"
                title = "중고마켓"
                boardTypeNo = BoardType.FIXED.no
                allowSearch = false
                Sred.putObjectBoard(key, this)   // sred에 저장
            }
            val  roomBoard = Board().apply {
                key = ROOM_BOARD_KEY
                boardId = "room"
                title = "부동산"
                boardTypeNo = BoardType.FIXED.no
                allowSearch = false
                Sred.putObjectBoard(key, this)   // sred에 저장
            }
            Firee.addBoard(phCodeBoard!!, { lllogI("createBoardOnce phCodeBoard success") }, { lllogI("createBoardOnce phCodeBoard fail") })
            Firee.addBoard(recentBoard!!, { lllogI("createBoardOnce recentBoard success") }, { lllogI("createBoardOnce recentBoard fail") })
            Firee.addBoard(fleaBoard!!, { lllogI("createBoardOnce fleaBoard success") }, { lllogI("createBoardOnce fleaBoard fail") })
            Firee.addBoard(roomBoard!!, { lllogI("createBoardOnce roomBoard success") }, { lllogI("createBoardOnce roomBoard fail") })
        }
    }

}