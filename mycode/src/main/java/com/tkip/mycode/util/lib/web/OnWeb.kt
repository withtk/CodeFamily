package com.tkip.mycode.util.lib.web

import android.content.Context
import android.content.Intent
import com.tkip.mycode.init.PUT_URL
import com.tkip.mycode.util.lib.TransitionHelper

class OnWeb {

    companion object {

        fun gotoWebViewActivity(context: Context,  url: String  ) {
            val intent = Intent(context, MyWebViewActivity::class.java).apply {
                addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
                putExtra(PUT_URL, url)
            }
            TransitionHelper.startTran(context, intent, null)
        }



    }

}