package com.tkip.mycode.util.tools.radio

import android.content.Context
import android.view.LayoutInflater
import android.widget.LinearLayout
import android.widget.RadioButton
import androidx.databinding.DataBindingUtil
import com.tkip.mycode.R
import com.tkip.mycode.databinding.SubRgString4RoomBinding
import com.tkip.mycode.model.my_enum.RGstring
import com.tkip.mycode.funs.common.lllogI

/**
 * for room
 * 라디오 선택
 */
class SubRGstring4Room : LinearLayout {
    private lateinit var b: SubRgString4RoomBinding


    constructor(context: Context) : super(context)
    constructor(context: Context, rgString: RGstring) : super(context) {

        b = DataBindingUtil.inflate(LayoutInflater.from(context), R.layout.sub_rg_string_4_room, this, true)
        b.rgString = rgString

        b.radioGroup.setOnCheckedChangeListener { _, checkedId ->
            // checkedId 는 position
            rgString.selectedIndex = checkedId
            lllogI("setOnCheckedChangeListener checkedId $checkedId")
        }

        /**   초기 셋팅 : 각 Group안에 radio추가.   */
        rgString.list.forEachIndexed { i, str -> addRadio(str, i) }

        /**   초기 셋팅 : 초기값 선택   */
        b.radioGroup.check(rgString.selectedIndex)

    }


    private fun addRadio(str: String, i: Int) {
        val view = LayoutInflater.from(context).inflate(R.layout.view_radio_text, null).apply {
            val radio = findViewById<RadioButton>(R.id.rb)
            radio.text = str
            radio.id = i
        }
        b.radioGroup.addView(view)
    }


}