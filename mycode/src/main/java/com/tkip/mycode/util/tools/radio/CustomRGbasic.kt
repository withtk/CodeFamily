package com.tkip.mycode.util.tools.radio

import android.content.Context
import android.view.LayoutInflater
import android.widget.LinearLayout
import androidx.databinding.DataBindingUtil
import com.tkip.mycode.R
import com.tkip.mycode.databinding.CustomRgBasicBinding
import com.tkip.mycode.databinding.ViewRadioCornerBinding
import com.tkip.mycode.databinding.ViewRadioTextBinding
import com.tkip.mycode.funs.common.lllogI


/**
 * typeBasic : radio type
 */
class CustomRGbasic : LinearLayout {
    private lateinit var b: CustomRgBasicBinding

    constructor(context: Context) : super(context)
    constructor(context: Context, list: ArrayList<String>, typeBasic: Boolean, onSelected: (id:Int) -> Unit) : super(context) {
        lllogI("ViewRGbasic list: ${list.size}")

        b = DataBindingUtil.inflate(LayoutInflater.from(context), R.layout.custom_rg_basic, this, true)

        b.radioGroup.setOnCheckedChangeListener { _, checkedId ->
            // checkedId 는 position
            lllogI("ViewRGbasic checkedId: $checkedId")
            onSelected(checkedId)
        }

        /**   초기 셋팅 :   Group안에 radio추가.   */
        list.forEachIndexed { i, str -> addRadio(typeBasic, i, str) }

    }


    private fun addRadio(typeBasic: Boolean, i: Int, str: String) {

        if (typeBasic) {
            ViewRadioTextBinding.inflate(LayoutInflater.from(context)).apply {
                title = str
                rb.id = i
                b.radioGroup.addView(this.root)
            }
        } else {
            ViewRadioCornerBinding.inflate(LayoutInflater.from(context)).apply {
                title = str
                rb.id = i
                b.radioGroup.addView(this.root)
            }
        }

//        val view = LayoutInflater.from(context).inflate(R.layout.view_radio_text, null).apply {
//            val radio = findViewById<RadioButton>(R.id.rb)
//            radio.text = str
//            radio.id = i
//        }
//        b.radioGroup.addView(view)
    }


}