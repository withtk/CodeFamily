package com.tkip.mycode.util.tools.etc

import android.content.Context
import android.view.View
import android.view.inputmethod.InputMethodManager



class MyKeyboard {

    companion object {

        fun show (v: View ) {
            v.requestFocus()
            val imm = v.context.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
            imm.toggleSoftInput(InputMethodManager.SHOW_FORCED, 0)
            //        imm.showSoftInput(input, 0);
        }

        fun hide (v: View ) {
            val imm =  v.context.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
            imm.hideSoftInputFromWindow(v.windowToken, 0)
            //        imm.hideSoftInputFromWindow(v.getWindowToken(), InputMethodManager.RESULT_UNCHANGED_SHOWN);

        }
    }
}