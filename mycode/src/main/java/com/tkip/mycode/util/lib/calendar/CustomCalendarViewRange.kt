package com.tkip.mycode.util.lib.calendar

import android.content.Context
import android.view.LayoutInflater
import android.widget.LinearLayout
import androidx.databinding.DataBindingUtil
import com.haibin.calendarview.Calendar
import com.tkip.mycode.R
import com.tkip.mycode.databinding.CustomCalendarviewRangeBinding
import com.tkip.mycode.dialog.toast.toastAlert
import com.tkip.mycode.dialog.toast.toastNormal
import com.tkip.mycode.funs.common.*
import com.tkip.mycode.init.base.appendMonth
import com.tkip.mycode.init.base.getStr
import com.tkip.mycode.util.lib.calendar.multi.CustomMultiMonthView
import com.tkip.mycode.util.lib.calendar.range.CustomRangeMonthView
import com.tkip.mycode.util.tools.date.getIntDate8TodayPH

/**
 * 1. 범위선택
 * 2. 멀티선택
 */

class CustomCalendarViewRange : LinearLayout {
    private lateinit var b: CustomCalendarviewRangeBinding
    private val selList = arrayListOf<Int>()   // for multi
    private var startC: Calendar? = null   // for Range
    private val todayPH = getIntDate8TodayPH()

    constructor(context: Context) : super(context)
    constructor(context: Context, rangeType: Boolean, onSelected: ((selList: ArrayList<Int>, selDate: Int?) -> Unit)?) : super(context) {
        b = DataBindingUtil.inflate(LayoutInflater.from(context), R.layout.custom_calendarview_range, this, true)
        b.range = rangeType

        b.btnClear.onClick { setCalendar(b.sw.isChecked, onSelected) }
        b.sw.setOnCheckedChangeListener { _, isOn -> setCalendar(isOn, onSelected) }

        /*** 달력 세팅 */
        val tri = todayPH.getYMD()
        lllogI("CustomCalendarViewRange tri $tri")
//        val y = tri.first
//        val m = tri.second
//        val d = tri.third
        val y = b.cal.curYear
        val m = b.cal.curMonth
        val d = b.cal.curDay

        b.cal.maxMultiSelectSize = MAX_MULTI_CALENDAR  // max 선택일수
        b.cal.setRange(y, m, d + 1, y, m + 1, d)   // 선택 범위
        lllogI("CustomCalendarViewRange y $y m $m d $d")

        setYearMonth()
        lllogI("CustomCalendarViewRange tri $tri")

        /**         * 스크롤 : year Month 수정하기.         */
        b.cal.monthViewPager.onPageChange(onState = {}, onScrolled = { _, _, _ -> },
                onSelected = { p ->
                    if (p == 0) {
                        // 이번달
                        setYearMonth()
                    } else {
                        // 다음달
                        if (b.cal.curMonth == 12) {
                            val yearPlus = "${b.cal.curYear + 1}"
                            b.tvYear.text = yearPlus
                            b.tvMonth.text = 1.appendMonth()
                        } else {
                            val monthPlus = b.cal.curMonth + 1
                            b.tvMonth.text = monthPlus.appendMonth()
                        }
                    }
                })

        setCalendar(rangeType, onSelected) // 최초 달력 세팅
    }

    private fun setCalendar(rangeType: Boolean, onSelected: ((iSet: ArrayList<Int>, selDate: Int?) -> Unit)?) {
        b.cal.clearMultiSelect()
        b.cal.clearSelectRange()
        selList.clear()
        onSelected?.invoke(selList, null)

        if (rangeType) {
            /**         * 범위선택         */
            b.msg = R.string.title_ad_term_manual.getStr()
            b.cal.setMonthView(CustomRangeMonthView::class.java)
            b.cal.onRange(
                    onOutOfRange = { toastNormal(R.string.cal_info_locale_tomorrow) },
                    onSelect = { endC, isEnd ->
                        if (isEnd) {
                            val diff = endC!!.differ(startC)
//                            endC!!.setSetFromRange(startC!!,iSet)
                            OnCalendar.setSetFromRange(diff, startC!!.getDate8(), selList)
                            onSelected?.invoke(selList, startC!!.getDate8())
                        } else {
                            startC = endC
//                            onRange?.invoke(startC!!.getDate8(), null)
                        }
                    },
                    onOutOfMax = { _, _ -> toastNormal(MSG_CALENDAR_OVER_LIMIT) })
        } else {
            /**         * 개별선택         */
            b.msg = String.format(R.string.title_ad_term_manual_multi.getStr(), MAX_MULTI_CALENDAR)
            b.cal.setMonthView(CustomMultiMonthView::class.java)
            //            setCalMulti(onMulti)

            /**         * 선택한 일수 보내기         */
            b.cal.onMulti(
                    onOutOfRange = { toastNormal(R.string.cal_info_locale_tomorrow) },
                    onSelect = { c, curSize, _ ->
                        c?.getDate8()?.let { selDate ->
                            lllogI("CustomCalendarViewRange selDate $selDate")
                            /*** PH의 오늘이후의 날짜인지 체크.*/
                            if (selDate > todayPH) {
                                if (!selList.contains(selDate)) selList.add(selDate)    //선택

//                                if (curSize > selList.size) if (!selList.contains(selDate)) selList.add(selDate)    //선택
                                else {
                                    //해제
                                    selList.remove(selDate)
                                    lllogI("CustomCalendarViewRange remove $selDate")
                                }
                                onSelected?.invoke(selList, selDate)

                                selList.forEach {
                                    lllogI("CustomCalendarViewRange selList date8 :  $it")
                                }

                            } else toastAlert(R.string.cal_info_locale_tomorrow)
                        }
                    },
                    onOutOfMax = { _, _ -> toastNormal(MSG_CALENDAR_OVER_LIMIT) }
            )
        }
    }

//    private fun ArrayList<T>

    private fun setYearMonth() {
        b.tvYear.text = b.cal.curYear.toString()
        b.tvMonth.text = b.cal.curMonth.appendMonth()
    }

}