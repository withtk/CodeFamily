package com.tkip.mycode.util.lib.photo.saf

import android.app.Activity
import android.content.Intent
import com.tkip.mycode.funs.common.OnPermission
import com.tkip.mycode.init.REQ_SAF


/**
 * - SAF(Storage Access Framework)
 */
class OnSAF {
    companion object {

        /**
         * 이미지 가져오기
         */
        fun openSAF(activity: Activity) {
            val intent = Intent(Intent.ACTION_OPEN_DOCUMENT).apply {
                // 1
                addCategory(Intent.CATEGORY_OPENABLE)   // 2
                type = "image/*"    // 3
            }

            activity.startActivityForResult(intent, REQ_SAF)   // 4
        }


        /**
         * 이미지 여러개 가져오기
         * (여러개 not working..)
         */
        fun openSAFmulti(activity: Activity) {

            /**             * SAF multi */
//            val intent = Intent()
//            intent.type = "image/*"
//            intent.putExtra(Intent.EXTRA_ALLOW_MULTIPLE, true)
//            intent.action = Intent.ACTION_GET_CONTENT
//            activity.startActivityForResult(Intent.createChooser(intent, "Select Picture"), REQ_PICK_MULTI)

            /**             * PICK multi */
            OnPermission.onOpenGallery(activity, isGranted = true, multi = true)

        }



        /**
         * 앨범 테스트
         */
        /*
        data class Video(val uri: Uri,
                         val name: String,
                         val duration: Int,
                         val size: Int
        )

        @SuppressLint("InlinedApi")
        fun albumTest() {
            lllogI("albumTest albumTest  ")
            // Need the READ_EXTERNAL_STORAGE permission if accessing video files that your app didn't create.

            // Container for information about each video.

            val videoList = mutableListOf<Video>()

            val projection = arrayOf(
                    MediaStore.Images.Media._ID,
                    MediaStore.Images.Media.DISPLAY_NAME,
                    MediaStore.Images.Media.DATE_TAKEN,
                    MediaStore.Images.Media.SIZE
            )

            // Show only videos that are at least 5 minutes in duration.
            val selection = "${MediaStore.Video.Media.DURATION} >= ?"
            val selectionArgs = arrayOf(
                    TimeUnit.MILLISECONDS.convert(5, TimeUnit.MINUTES).toString()
            )

            // Display videos in alphabetical order based on their display name.
            val sortOrder = "${MediaStore.Video.Media.DISPLAY_NAME} ASC"


            val query = this@TTTestActivity.contentResolver?.query(
                    MediaStore.Video.Media.EXTERNAL_CONTENT_URI,
                    projection,
                    selection,
                    selectionArgs,
                    sortOrder
            )
            query?.use { cursor ->
                // Cache column indices.
                val idColumn = cursor.getColumnIndexOrThrow(MediaStore.Video.Media._ID)
                val nameColumn = cursor.getColumnIndexOrThrow(MediaStore.Video.Media.DISPLAY_NAME)
                val durationColumn = cursor.getColumnIndexOrThrow(MediaStore.Video.Media.DURATION)
                val sizeColumn = cursor.getColumnIndexOrThrow(MediaStore.Video.Media.SIZE)

                while (cursor.moveToNext()) {
                    // Get values of columns for a given video.
                    val id = cursor.getLong(idColumn)
                    val name = cursor.getString(nameColumn)
                    val duration = cursor.getInt(durationColumn)
                    val size = cursor.getInt(sizeColumn)

                    val contentUri: Uri = ContentUris.withAppendedId(
                            MediaStore.Video.Media.EXTERNAL_CONTENT_URI,
                            id
                    )

                    // Stores column values and the contentUri in a local object
                    // that represents the media file.
                    videoList += Video(contentUri, name, duration, size)
                }
            }

            videoList.forEach { lllogI("videoList videoList $it") }
        }

        //    @SuppressLint("InlinedApi")
        private fun startAlbum() {
            lllogI("startAlbum startAlbum")
            val projection = arrayOf(
                    MediaStore.Images.Media._ID,
                    MediaStore.Images.Media.DISPLAY_NAME,
                    MediaStore.Images.Media.DATE_TAKEN
            )
            val selection = "${MediaStore.Images.Media.DATE_TAKEN} >= ?"
            val selectionArgs = arrayOf(
                    dateToTimestamp(day = 1, month = 1, year = 1970).toString()
            )
            val sortOrder = "${MediaStore.Images.Media.DATE_TAKEN} DESC"
            val query = this@TTTestActivity.contentResolver?.query(
                    MediaStore.Images.Media.EXTERNAL_CONTENT_URI,
                    projection,
                    selection,
                    selectionArgs,
                    sortOrder
            )
            lllogI("startAlbum query $query")

            var i = 0
            query?.use { cursor ->
                lllogI("startAlbum cursor count ${cursor.count}")
                val idColumn = cursor.getColumnIndexOrThrow(MediaStore.Images.Media._ID)
                val dateTakenColumn =
                        cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATE_TAKEN)
                val displayNameColumn =
                        cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DISPLAY_NAME)
                while (cursor.moveToNext()) {
                    val id = cursor.getLong(idColumn)
                    val dateTaken = Date(cursor.getLong(dateTakenColumn))
                    val displayName = cursor.getString(displayNameColumn)
                    val contentUri = Uri.withAppendedPath(
                            MediaStore.Images.Media.EXTERNAL_CONTENT_URI,
                            id.toString()
                    )
                    lllogI("startAlbum id: $id, display_name: $displayName, date_taken: $dateTaken, content_uri: $contentUri")
                    if (i < 10) {
                        ivList.add(contentUri)
                    }
                    i++
                    lllogI("startAlbum index $i")
                }
            }

        }

        val ivList = arrayListOf<Uri>()
        private fun setIvlist() {
            ivList.forEachIndexed { i, uri ->
                val iv = ImageView(this@TTTestActivity)
//            val layoutParams = LinearLayout.LayoutParams(100, 100)
//            iv.layoutParams = layoutParams
                val thumb = uri.createMyThumb()
                iv.setImageBitmap(thumb)
//            iv.layoutParams.width = 20
//            iv.layoutParams.height = 20
                iv.requestLayout()
//            b.flow.addView(iv)
                b.lineIv.addView(iv)
                lllogI("startAlbum setIvlist index $i")
            }
        }

        private fun dateToTimestamp(day: Int, month: Int, year: Int): Long =
                SimpleDateFormat("dd.MM.yyyy").let { formatter ->
                    formatter.parse("$day.$month.$year")?.time ?: 0
                }

*/


    }
}