package com.tkip.mycode.util.tools.fire

import android.util.Log
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.database.ValueEventListener
import com.tkip.mycode.dialog.progress.MyProgress
import com.tkip.mycode.funs.common.OnException
import com.tkip.mycode.funs.common.lllogI
import com.tkip.mycode.init.*
import com.tkip.mycode.init.base.Cons
import com.tkip.mycode.init.base.createKey
import com.tkip.mycode.model.cost.Cost
import com.tkip.mycode.model.count.CountMini
import com.tkip.mycode.model.manage.Device
import com.tkip.mycode.model.manage.Hit
import com.tkip.mycode.model.peer.isSupervisor
import com.tkip.mycode.model.peer.mypeer
import com.tkip.mycode.model.peer.myuid
import com.tkip.mycode.model.peer.peer_uid.PeerUID
import com.tkip.mycode.model.post.Post
import com.tkip.mycode.util.lib.photo.FotoInfo
import java.util.*


/************************************************
 * FireRealtime
 *************************************************/

val realFire = FirebaseDatabase.getInstance()

/**********************************************************
 * <common> 1개 처리
 *********************************************************/
fun FirebaseDatabase.commonADD(children: Array<String>, any: Any, success: () -> Unit, fail: (Exception) -> Unit) {
    this.reference.child(children.getChildString())
            .setValue(any)
            .addOnSuccessListener {
                success()
                Log.d(Cons.TAG, "commonADD : ${children.contentToString()} : success add! ")
            }
            .addOnFailureListener {
                fail(it)
//                MyProgress.cancel()
//                OnException.dbfailed("commonADD : ${children.contentToString()} fail: ", it)
            }

}

fun FirebaseDatabase.commonDEL(children: Array<String>, success: () -> Unit, fail: (Exception) -> Unit) {
    this.reference.child(children.getChildString())
            .removeValue()
            .addOnSuccessListener { success() }
            .addOnFailureListener { fail(it) }
}


/** * childUPdateMap만들기 */
fun fireRealchildMap() = hashMapOf<String, Any?>()

fun makeRealKey(vararg child: String): String =
        StringBuffer().apply {
            child.forEach {
                if (this.isBlank()) this.append(it)
                else this.append("/$it")
            }
        }.toString()

fun HashMap<String, Any?>.real(method: String, success: () -> Unit, fail: (Exception) -> Unit) {
    FirebaseDatabase.getInstance().reference
            .updateChildren(this)
            .addOnSuccessListener {
                lllogI("batchUPDATE $method success size : ${this.size}")
                success()
            }
            .addOnFailureListener {
                MyProgress.cancel()
                OnException.dbfailed("batchUPDATE $method fail:", it)
                fail(it)
            }
}


/************************************************************************************************
 * <fireReal> child
 ************************************************************************************************/
fun Array<String>.getChildString(): String {
    var myChild = ""
    forEach { str -> myChild += "/$str" }
    return myChild
}


/**
 * <fireReal> updateChild
 */
fun HashMap<String, Any>.setChildValue(children: Array<String>, value: Map<String, Any>) {
    this[children.getChildString()] = value
}


/****************************************************************************************
 ************************** 개별 업로드 ***************************************************
 *****************************************************************************************/

///** * 로그인 올리기 */
//fun PeerUID.real() {
//    realFire.commonADD(arrayOf(CH_LOGIN, createKey()), this, success = { }, fail = {})
//}

/** * View_Count 올리기 */
fun CountMini.real() {
    realFire.commonADD(arrayOf(this.topChildName, this.key), this, success = { }, fail = {})
}
//fun Count.real() {
//    realFire().commonADD(arrayOf(this.topChildName, this.key), this, success = { }, fail = {})
//}

/** * Device 올리기 */
fun Device.real() {
    realFire.commonADD(arrayOf(EES_DEVICE, createKey()), this, success = { }, fail = {})
}

/** * Act 올리기 : act > uid > act.key */
fun Cost.real() {
    if(isSupervisor()) return  // 슈퍼는 act 불필요.
    realFire.commonADD(arrayOf(EES_ACT, this.key), this, success = { }, fail = {})
}


/** * FotoInfo 올리기 */
fun FotoInfo.real() {
    realFire.commonADD(arrayOf(EES_FOTO_INFO, this.key), this, success = { }, fail = {})
}

/** * FotoInfo 올리기 */
fun Hit.real() {
    realFire.commonADD(arrayOf(EES_HIT, this.key), this, success = { }, fail = {})
}


/** * onLine / offLine 올리기 */
fun realOnline() {
    val peerUid = PeerUID(mypeer())
    realFire.commonADD(arrayOf(EES_ONLINE, peerUid.key), peerUid, success = { }, fail = {})
}

fun realOffline() {
    realFire.commonDEL(arrayOf(EES_ONLINE, myuid()), success = { }, fail = {})
}


