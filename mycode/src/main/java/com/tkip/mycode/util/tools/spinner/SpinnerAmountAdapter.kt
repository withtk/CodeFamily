package com.tkip.mycode.util.tools.spinner

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import com.tkip.mycode.databinding.HolderSpinnerAmountBinding


class SpinnerAmountAdapter(val items: ArrayList<Int>) : BaseAdapter() {

    override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View {

        val b = HolderSpinnerAmountBinding.inflate(LayoutInflater.from(parent?.context), parent, false)
//        b.str = items[position]
        b.amount = items[position]

        return b.root
    }

    override fun getItem(position: Int): Any? {
        return null
    }

    override fun getItemId(position: Int): Long {
        return 0
    }

    override fun getCount(): Int {
        return items.size
    }

//    inner class SpinnerStatusViewHolder(val b: HolderSpinnerStatusBinding) : RecyclerView.ViewHolder(b.root) {
//
//        private lateinit var warn: Warn
//
//        fun bind(warn: Warn) {
//            this.warn = warn
//            b.holder = this
//            b.warn = warn
//
//            b.executePendingBindings()
//        }
//    }


}