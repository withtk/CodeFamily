package com.tkip.mycode.util.lib.elastic

import com.tkip.mycode.R
import com.tkip.mycode.funs.common.lllogI
import com.tkip.mycode.funs.common.remoteStr
import com.tkip.mycode.init.*
import com.tkip.mycode.model.bid.Bid
import com.tkip.mycode.model.board.Board
import com.tkip.mycode.model.count.Count
import com.tkip.mycode.model.count.CountType
import com.tkip.mycode.model.flea.Flea
import com.tkip.mycode.model.my_enum.DataType
import com.tkip.mycode.model.post.Post
import com.tkip.mycode.model.room.Room
import com.tkip.mycode.model.store.Store
import com.tkip.mycode.util.lib.retrofit2.ESget
import com.tkip.mycode.util.lib.retrofit2.ESquery
import com.tkip.mycode.util.lib.retrofit2.EsApiService
import com.tkip.mycode.util.lib.retrofit2.Retro
import com.tkip.mycode.util.tools.fire.batch
import com.tkip.mycode.util.tools.fire.docRef
import com.tkip.mycode.util.tools.fire.myCommit


/**
 * todo: peerAllow 기입할 것. 아직 미완성.
 * < 어플 개설시 초기화할 데이터들 >
 */
class OnEs {
    companion object {

        /**
         * 최초 한번
         * 중요 : functions셋팅 후에 해야 ES와 동기화가 된다.
         */
        fun init() {

//            fireeUploadFleaTags()
//            fireeUploadBuiltTags()
//            fireeuploadRoomTags()
//            fireeUploadLocTags()

        }


        /******************************************************************************************************************************************
         *************************** 전체 인덱스 init  *********************************************************************************************
         *****************************************************************************************************************************************/

        fun printIndice() {
            val result = StringBuffer().apply { elasticIndice.forEach { append("$it,") } }.toString()
            lllogI("OnEs result \n $result")
        }

        /**
         * 중요 : nori_tokenizer와 user_dictionary 설치 후에 index 생성할 것.
         */
        fun createIndex(api: EsApiService?, onlyOne: Boolean) {
            val method = "createIndex"
            lllogI("OnEs createIndex start ")
            lllogI("OnEs createIndex remote_elasticsearch_url ${R.string.remote_elasticsearch_url.remoteStr()}")
            lllogI("OnEs createIndex elastic ID ${R.string.remote_elasticsearch_id.remoteStr()}")
            lllogI("OnEs createIndex elastic PW ${R.string.remote_elasticsearch_pw.remoteStr()}")


            if (onlyOne) {
                /**           필요한 것만             */
                arrayOf(

                        /************************* Count List *****************************************/
                        /*** Board */
                        Pair(CountType.BOOK.getTopChild(DataType.BOARD), elasticCommonCountMapping),
                        Pair(CountType.PUSH.getTopChild(DataType.BOARD), elasticCommonCountMapping),

                        /*** Post */
                        Pair(CountType.VIEW_UID.getTopChild(DataType.POST), elasticCommonCountMapping),
                        Pair(CountType.UP.getTopChild(DataType.POST), elasticCommonCountMapping),
                        Pair(CountType.DOWN.getTopChild(DataType.POST), elasticCommonCountMapping),
                        Pair(CountType.BOOK.getTopChild(DataType.POST), elasticCommonCountMapping),
                        Pair(CountType.PUSH.getTopChild(DataType.POST), elasticCommonCountMapping),

                        /*** Flea */
                        Pair(CountType.VIEW_UID.getTopChild(DataType.FLEA), elasticCommonCountMapping),
                        Pair(CountType.BOOK.getTopChild(DataType.FLEA), elasticCommonCountMapping),
                        Pair(CountType.PUSH.getTopChild(DataType.FLEA), elasticCommonCountMapping),

                        /*** Room */
                        Pair(CountType.VIEW_UID.getTopChild(DataType.ROOM), elasticCommonCountMapping),
                        Pair(CountType.BOOK.getTopChild(DataType.ROOM), elasticCommonCountMapping),
                        Pair(CountType.PUSH.getTopChild(DataType.ROOM), elasticCommonCountMapping),

                        /*** Store */
                        Pair(CountType.BOOK.getTopChild(DataType.STORE), elasticCommonCountMapping),
                        Pair(CountType.PUSH.getTopChild(DataType.STORE), elasticCommonCountMapping),

                        Pair(EES_QUICK, elasticCommonCountMapping)
                        /************************* Count List *****************************************/


//                Pair(EES_PEER, elasticMapIndexMapping[EES_PEER]!!)
//                        Pair(EES_ACT, elasticMapIndexMapping[EES_ACT]!!)
//                        Pair(CountType.VIEW.getTopChild(DataType.STORE), elasticCommonCountMini)
                ).forEach { p ->
                    lllogI("OnEs arrIndexPair start index:${p.first}")
                    ESget.createIndex(api, p.first, p.second,
                            success = { response -> lllogI("OnEs $method success ${p.first}   $response") },
                            fail = { lllogI("OnEs $method  fail ${p.first} :  ") })
                }
                /*************************************/
            } else {
                elasticMapIndexMapping.forEach { (k, v) ->
                    lllogI("OnEs $method start k:$k")
                    ESget.createIndex(api, k, v,
                            success = { response -> lllogI("OnEs $method success $k ${k} $response") },
                            fail = { lllogI("OnEs $method fail ${k}    ") })
                }

//                arrIndexPair.forEachIndexed { i, pair ->
//                    lllogI("OnEs $method start i:$i")
//                    ESget.createIndex(pair.first, pair.second,
//                            success = { response ->
//                                lllogI("OnEs $method success $i ${pair.first} $response")
//                            },
//                            fail = { lllogI("OnEs $method ${pair.first} fail  ") })
//                }
            }

        }

        fun deleteIndex(api: EsApiService? ) {
            lllogI("OnEs deleteIndex start ")


                 /**           필요한 것만             */
                arrayOf(

                        /************************* Count List *****************************************/
                        /*** Board */
                        Pair(CountType.BOOK.getTopChild(DataType.BOARD), elasticCommonCountMapping),
                        Pair(CountType.PUSH.getTopChild(DataType.BOARD), elasticCommonCountMapping),

                        /*** Post */
                        Pair(CountType.VIEW_UID.getTopChild(DataType.POST), elasticCommonCountMapping),
                        Pair(CountType.UP.getTopChild(DataType.POST), elasticCommonCountMapping),
                        Pair(CountType.DOWN.getTopChild(DataType.POST), elasticCommonCountMapping),
                        Pair(CountType.BOOK.getTopChild(DataType.POST), elasticCommonCountMapping),
                        Pair(CountType.PUSH.getTopChild(DataType.POST), elasticCommonCountMapping),

                        /*** Flea */
                        Pair(CountType.VIEW_UID.getTopChild(DataType.FLEA), elasticCommonCountMapping),
                        Pair(CountType.BOOK.getTopChild(DataType.FLEA), elasticCommonCountMapping),
                        Pair(CountType.PUSH.getTopChild(DataType.FLEA), elasticCommonCountMapping),

                        /*** Room */
                        Pair(CountType.VIEW_UID.getTopChild(DataType.ROOM), elasticCommonCountMapping),
                        Pair(CountType.BOOK.getTopChild(DataType.ROOM), elasticCommonCountMapping),
                        Pair(CountType.PUSH.getTopChild(DataType.ROOM), elasticCommonCountMapping),

                        /*** Store */
                        Pair(CountType.BOOK.getTopChild(DataType.STORE), elasticCommonCountMapping),
                        Pair(CountType.PUSH.getTopChild(DataType.STORE), elasticCommonCountMapping),

                        Pair(EES_QUICK, elasticCommonCountMapping)
                        /************************* Count List *****************************************/


                ).forEach { p ->
                    lllogI("OnEs arrIndexPair start index:${p.first}")
                    ESget.deleteIndex(api,p.first,
                        success = { response -> lllogI("OnEs ${p.first} success $response") },
                        fail = { lllogI("OnEs ${p.first} fail  ") })
                }

//            arrIndexPair.forEach {( i, pair )->
//                lllogI("OnEs deleteIndex arrIndexPair start i:$i")
//                ESget.deleteIndex(pair.first, "{}",
//                        success = { response -> lllogI("OnEs ${pair.first} success $response") },
//                        fail = { lllogI("OnEs ${pair.first} fail  ") })
//            }

        }


        /****************************************************************************************************************************************
         *************************** 데이타 옮기기 Es -> FireStore  *******************************************************************************
         * Es는 기존 주소로 연결.
         * Firebase는 새로운 DB로 연결.
         *****************************************************************************************************************************************/
        var count = 0
        val size = 10000
        val from = 0
        fun copyDBfromES() {
            count = 0
            val arr = arrayOf(EES_BOARD, EES_POST, EES_FLEA, EES_ROOM, EES_STORE, EES_BID, EES_BANNER)
//                    "post_thumbupcount", "post_thumbdowncount", "post_bookmarkcount", "post_pushcount", "board_bookmarkcount", "board_pushcount", "flea_bookmarkcount", "flea_pushcount", "room_bookmarkcount", "room_pushcount", "store_bookmarkcount", "store_pushcount")
            arr.forEach { index ->
                when (index) {
                    EES_BOARD -> ESget.getBoardList(ESquery.matchAll(size, from), success = { it.addDBtoFireStroe(index) }, fail = { lllogI("OnEs copyDBfromES [[$index]] fail@@@@@@ ") })
                    EES_POST -> ESget.getPostList(ESquery.matchAll(size, from), success = { it.addDBtoFireStroe(index) }, fail = { lllogI("OnEs copyDBfromES [[$index]] fail@@@@@@ ") })
                    EES_FLEA -> ESget.getFleaList(ESquery.matchAll(size, from), success = { it.addDBtoFireStroe(index) }, fail = { lllogI("OnEs copyDBfromES [[$index]] fail@@@@@@ ") })
                    EES_ROOM -> ESget.getRoomList(ESquery.matchAll(size, from), success = { it.addDBtoFireStroe(index) }, fail = { lllogI("OnEs copyDBfromES [[$index]] fail@@@@@@ ") })
                    EES_STORE -> ESget.getStoreList(ESquery.matchAll(size, from), success = { it.addDBtoFireStroe(index) }, fail = { lllogI("OnEs copyDBfromES [[$index]] fail@@@@@@ ") })
//                    EES_LOBBY -> ESget.getLobbyList(ESquery.matchAll(size, from), success = { it.addDBtoFireStroe(index) }, fail = { lllogI("OnEs copyDBfromES [[$index]] fail@@@@@@ ") })
                    EES_BID -> ESget.getBidList("copyDBfromES", ESquery.matchAll(size, from), success = { it.addDBtoFireStroe(index) }, fail = { lllogI("OnEs copyDBfromES [[$index]] fail@@@@@@ ") })
                    else -> {
                    }
//                    else -> ESget.getCountList(index,ESquery.matchAll(size, from), success = { it.addDBtoFireStroe(index) }, fail = { lllogI("OnEs copyDBfromES $index fail@@@@@@ ") })
                }
            }
        }


        /**         * firestore 에 넣기         */
        fun <T> ArrayList<T>?.addDBtoFireStroe(index: String) {
            lllogI("OnEs copyDBfromES [[$index]] success ")

            if (!this.isNullOrEmpty()) {
                count += this.size
                val ccc = count + this.size
                val batch = batch()
                this.forEach { data ->
                    when (data) {
                        is Board -> batch.set(docRef(EES_BOARD, data.key), data)
                        is Post -> batch.set(docRef(EES_POST, data.key), data)
                        is Flea -> batch.set(docRef(EES_FLEA, data.key), data)
                        is Room -> batch.set(docRef(EES_ROOM, data.key), data)
                        is Store -> batch.set(docRef(EES_STORE, data.key), data)
//                        is Lobby -> batch.set(docRef(EES_LOBBY, data.key), data)
                        is Bid -> batch.set(docRef(EES_BID, data.key), data)
                        is Count -> batch.set(docRef(EES_BID, data.key), data)
                        else -> batch.set(docRef(EES_BOARD, (data as Board).key), data)
                    }
                }
                batch.myCommit("addDBtoFireStroe", success = { lllogI("OnEs addDBtoFireStroe [[$index]]  success, total : $ccc @@@@@@@@@@") }, fail = { lllogI("OnEs addDBtoFireStroe [[$index]] fail@@@@@@ ") })

            } else lllogI("addDBtoFireStroe $index : null arraylist @@@@@@@@@@@@@@@@@@@@@@@@")

        }


    }
}