package com.tkip.mycode.util.tools.spinner

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import androidx.appcompat.app.AppCompatActivity
import com.tkip.mycode.databinding.HolderSpinnerStoreTypeBinding
import com.tkip.mycode.model.my_enum.StoreType


class SpinnerStoreType(val activity: AppCompatActivity, val items: ArrayList<StoreType>) : BaseAdapter() {

    override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View {

        val b =   HolderSpinnerStoreTypeBinding.inflate(LayoutInflater.from(parent?.context), parent, false)
        b.storeType = items[position]

        return b.root
    }

    override fun getItem(position: Int): Any? {
        return null
    }

    override fun getItemId(position: Int): Long {
        return 0
    }

    override fun getCount(): Int {
        return items.size
    }


}