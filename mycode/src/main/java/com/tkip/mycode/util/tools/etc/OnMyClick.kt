package com.tkip.mycode.util.tools.etc

import android.os.Handler
import android.os.SystemClock
import android.view.View
import android.widget.Button
import com.tkip.mycode.funs.common.lllogD
import com.tkip.mycode.funs.common.lllogM

abstract class OnMyClick : View.OnClickListener {

    abstract fun onMySingleClick(v: View)

    override fun onClick(v: View) {
        if (SystemClock.elapsedRealtime() - mLastClickTime < TIME_INTERVAL) {
            // double click!
            return
        }
        mLastClickTime = SystemClock.elapsedRealtime()
        // single click!
        onMySingleClick(v)
    }

    companion object {

        private const val TIME_INTERVAL: Long = 600
        var mLastClickTime: Long = 0

        fun isDoubleClick(): Boolean {
            if (SystemClock.elapsedRealtime() - mLastClickTime < TIME_INTERVAL) {
                // double click!
//                TToast.showNormal( OnApp.getAppContext().getString(R.string.info_double_click))
                return true
            }
            mLastClickTime = SystemClock.elapsedRealtime()
            // single click!
            return false
        }

        fun disableAWile(vararg vv: View) {
            setDisableAWhileBTN(*vv)
        }

        fun setDisableAWhileBTN(vararg vv: View) {
            vv.forEach {

                /*** 로그 찍기 */
                when (it) {
                    is Button -> lllogM("setDisableAWhileBTN ${it.text}")
                    else -> lllogM("setDisableAWhileBTN ${it.id}")
                }

                it.isEnabled = false

                Handler().postDelayed({
                    it.isEnabled = true
                }, 1000)

            }
        }


        var click5 = 0
        fun is5Click(): Boolean {

            /*** 연속클릭  */
            if (SystemClock.elapsedRealtime() - mLastClickTime < TIME_INTERVAL) {
                click5++
                lllogD("OnMyClick is5Click click5 $click5")
                if (click5 > 1) {  // 요건 충족 : 리셋 & true
                    click5 = 0
                    lllogD("OnMyClick is5Click true")
                    return true
                }
            } else {
                click5 = 0
            }

            mLastClickTime = SystemClock.elapsedRealtime()
            lllogD("OnMyClick is5Click false")
            return false
        }


//        private var lastTypingTime: Long = 0
//        private var isReady = true
//        private var han = Handler()
//        private var runnable = Runnable {  }
//        /**
//         * 연속 타이핑시 쿼리 안 날림.
//         * runnable 동작할 때 isReady이면 검색 시작을 알림.
//         */
//        fun checkTyping(onFinish: () -> Unit) {
//            han.postDelayed({
//                if (isReady) onFinish()
//            }, TIME_INTERVAL)   // 매번 핸들러 생성.
//
//            val now = SystemClock.elapsedRealtime()
//            if ((now - lastTypingTime) < TIME_INTERVAL) {
//                han.re()
//            }else{
//
//            }
//            lastTypingTime = now
//
//
//            lllogD("OnMyClick  isTyping  now:$now  lastTypingTime:$lastTypingTime  result:${now - lastTypingTime}")
//        }

    }

}