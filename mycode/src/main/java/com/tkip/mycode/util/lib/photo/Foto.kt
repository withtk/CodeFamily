package com.tkip.mycode.util.lib.photo

import android.graphics.Bitmap
import android.net.Uri
import android.os.Parcel
import android.os.Parcelable
import com.google.firebase.database.Exclude
import com.tkip.mycode.init.base.createNanoKey
import com.tkip.mycode.util.tools.date.rightNow

/**
 * url과 uri 모두를 한곳에 넣기 위한 pojo class.
 * photoview와 selected foto view 때문에...
 */
class Foto(

        var key: String = createNanoKey(),
        var thumbUrl: String? = null,
        var url: String? = null,
        var comment: String? = null,    // 사진에 설명 넣기.
        var main: Boolean = false,

        //////////////////////// DB에 안 들어가는 부분 ///////////////////////
        var photoId: String? = null,
        var thumbUri: Uri? = null,   // Uri는 Firebase DB에는 절대 안 들어간다. 오류남. 오류 로그도 안 뜨는 오류.
        var uri: Uri? = null,
        var orientation: String? = null,
        var thumbBitmap: Bitmap? = null,   // SAF의 uri를 이용하여 만든 thumbBitmap  중요: 다른 activity로 넘길 수 없음. 지우고 새로 만들어야 함.
        /////////////////////////////////////////////////////////////////////

        var addDate: Long = rightNow(),
        var invalid: Boolean = false  // 삭제여부

) : Parcelable {
    /**
     *  1. url 업로드 할 때 사용
     *  2. PhotoViewActivity로 보낼때 사용.
     */
    constructor(thumbUrl: String?, url: String?) : this(
            key = createNanoKey(),
            thumbUrl = thumbUrl,
            url = url
    )

    /** uri 사용자가 자기 사진 선택할 때 사용.*/
    constructor(myPhoto: MyPhoto) : this(
            key = createNanoKey(),
            photoId = myPhoto.photoId,
            thumbUri = myPhoto.thumbUri,
            uri = myPhoto.uri,
            orientation = myPhoto.orientation
    )

    /**
     *  1. SAF로 받아왔을때
     *  2. 갤러리에서 1개 받아왔을 때
     */
    constructor(uri: Uri, thumbBitmap: Bitmap?) : this(
            key = createNanoKey(),
            uri = uri,
            thumbBitmap = thumbBitmap
    )

    @Exclude
    fun toMap(): Map<String, Any> {
        val result = HashMap<String, Any>()
        result["key"] = key
        result["thumbUrl"] = thumbUrl!!
        result["url"] = url!!
        result["comment"] = comment!!
        result["main"] = main

        result["photoId"] = photoId!!
        result["thumbUri"] = thumbUri!!
        result["uri"] = uri!!
        result["orientation"] = orientation!!

        result["addDate"] = addDate
        result["invalid"] = invalid

        return result
    }

    /**
     * db 안 들어가는 것 모두 삭제.
     */
    fun clearNoDB() {
        photoId = null
        thumbUri = null
        uri = null
        orientation = null
        thumbBitmap = null
    }

    constructor(source: Parcel) : this(
            source.readString()!!,
            source.readString(),
            source.readString(),
            source.readString(),
            1 == source.readInt(),
            source.readString(),
            source.readParcelable<Uri>(Uri::class.java.classLoader),
            source.readParcelable<Uri>(Uri::class.java.classLoader),
            source.readString(),
            source.readParcelable<Bitmap>(Bitmap::class.java.classLoader),
            source.readLong(),
            1 == source.readInt()
    )

    override fun describeContents() = 0

    override fun writeToParcel(dest: Parcel, flags: Int) = with(dest) {
        writeString(key)
        writeString(thumbUrl)
        writeString(url)
        writeString(comment)
        writeInt((if (main) 1 else 0))
        writeString(photoId)
        writeParcelable(thumbUri, 0)
        writeParcelable(uri, 0)
        writeString(orientation)
        writeParcelable(thumbBitmap, 0)
        writeLong(addDate)
        writeInt((if (invalid) 1 else 0))
    }

    companion object {
        @JvmField
        val CREATOR: Parcelable.Creator<Foto> = object : Parcelable.Creator<Foto> {
            override fun createFromParcel(source: Parcel): Foto = Foto(source)
            override fun newArray(size: Int): Array<Foto?> = arrayOfNulls(size)
        }
    }
}


