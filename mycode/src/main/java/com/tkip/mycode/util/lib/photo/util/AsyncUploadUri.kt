package com.tkip.mycode.util.lib.photo.util

import android.content.Context
import android.graphics.Bitmap
import android.net.Uri
import android.os.AsyncTask
import android.provider.MediaStore
import com.google.firebase.storage.FirebaseStorage
import com.tkip.mycode.dialog.progress.MyCircle
import com.tkip.mycode.dialog.progress.MyProgress
import com.tkip.mycode.funs.common.OnException
import com.tkip.mycode.funs.common.lllogD
import com.tkip.mycode.funs.common.lllogI
import com.tkip.mycode.init.WIDTH_THUMB
import com.tkip.mycode.init.base.createKey
import com.tkip.mycode.util.tools.date.FORMAT_YEAR_MONTH_DATE
import com.tkip.mycode.util.tools.date.getKoreanTimeString
import com.tkip.mycode.util.tools.date.rightNow
import java.io.IOException
import java.lang.ref.WeakReference


class AsyncUploadUri(context: Context, val widthOrigin: Int, val child: String, val onProgress: (Int, Int) -> Unit, val success: (Int, String) -> Unit, val fail: (Exception) -> Unit) : AsyncTask<Uri, Int, ArrayList<Uri>>() {

//    private var wActivity: WeakReference<Activity> = WeakReference(activity)
    private var wActivity: WeakReference<Context> = WeakReference(context)

    override fun doInBackground(vararg uris: Uri?): ArrayList<Uri> {
        lllogI("AsyncUploadUri doInBackground  ")

        val uriList = arrayListOf<Uri>()

        for (u in PhotoUtil.photoUris) {
            lllogI("AsyncUploadUri for  ")
            u.let { uri ->

                getCompletedUriFromGallery(uri, WIDTH_THUMB)?.let { thumbUri ->
                    uriList.add(thumbUri)
                }

                getCompletedUriFromGallery(uri, widthOrigin)?.let { photoUri ->
                    uriList.add(photoUri)
                }
            }
        }
        return uriList
    }

    private fun getCompletedUriFromGallery(originUri: Uri, width: Int): Uri? {

        var bitmap2: Bitmap
        try {
            bitmap2 = MediaStore.Images.Media.getBitmap(wActivity.get()?.contentResolver, originUri)  //uri
            val orientation = ExifUtil.getOrientationFromUri(wActivity.get()!!, originUri)  //uri
            bitmap2 = ExifUtil.getRotatedBitmap(bitmap2, orientation)
            val height = (bitmap2.height.toDouble() / (bitmap2.width.toDouble() / width)).toInt()
            return ExifUtil.getUriFixedSizeFromBitmap(wActivity.get()!!, bitmap2, width, height)
        } catch (e: IOException) {
            e.printStackTrace()
        }
        OnException.forceExceptionAsyncResize("getCompletedUriFromGallery")
        return null
    }

    override fun onPostExecute(uriList: ArrayList<Uri>) {
        super.onPostExecute(uriList)
        lllogI("AsyncUploadUri onPostExecute    ")

        for ((index, uri) in uriList.withIndex()) {
            uploadPhotoUri(child, index, uri,
                    onProgress = { i, percent ->
                        onProgress(i, percent)
                    },
                    success = { i, photo ->
                        lllogI("AsyncUploadUri onPostExecute  success ")
                        success(i, photo)
                    },
                    fail = {
                        OnException.uploadFail("AsyncUploadUri onPostExecute fail : ")
                        MyProgress.cancel()
                        MyCircle.cancel()
                        fail(it)
                    })
        }
    }

    private fun uploadPhotoUri(child: String,
                               index: Int, uploadUri: Uri
                               , onProgress: (Int, Int) -> Unit
                               , success: (Int, String) -> Unit
                               , fail: (Exception) -> Unit) {

        lllogI("uploadPhotoUri  ")

        /** * DB의 폴더시간은 무조건 한 국가의 시간으로 통일해야. 왜냐면 로컬시간은 바뀔 수 있기때문에.*/
        val folder =  rightNow().getKoreanTimeString(FORMAT_YEAR_MONTH_DATE)
        val uploadName = createKey() + "_" + System.currentTimeMillis()+ "_" + index
        val ref = FirebaseStorage.getInstance().getReference(child)
//                .child(folder)
                .child(uploadName)
        lllogD("=============================start : $uploadName")
        lllogD("=============================ref : ${ref.toString()}")

        ref.putFile(uploadUri)
                .addOnProgressListener {
                    lllogD("addOnProgressListener")
                    val transferredByte = it.bytesTransferred
                    val fileTotalByte = it.totalByteCount
                    val percent = (100.0 * transferredByte / fileTotalByte)
                    lllogD("Upload is ${percent.toInt()}% done")
                    onProgress(index, percent.toInt())
                }
                .addOnPausedListener {
                    lllogD("addOnPausedListener")
                }
                .addOnSuccessListener { task ->
                    lllogI("addOnSuccessListener  ")
                    val urlTask = task.storage.downloadUrl
                    urlTask.addOnSuccessListener { uri ->
                        val url = uri.toString()
                        lllogD("addOnSuccessListener.url:" + url)
                        success(index, url)
                    }
                }
                .addOnFailureListener {
                    lllogD("addOnFailureListener")
                    fail(it)
                }
    }



}
