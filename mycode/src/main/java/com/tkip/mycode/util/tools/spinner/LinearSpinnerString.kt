package com.tkip.mycode.util.tools.spinner

import android.content.Context
import android.view.LayoutInflater
import android.widget.LinearLayout
import androidx.databinding.DataBindingUtil
import com.tkip.mycode.R
import com.tkip.mycode.databinding.ViewSpinnerCommonBinding
import com.tkip.mycode.funs.common.onItemSelected


/**
 * info : 첫번째 Item
 *      : null이면 기본 문구 "스피너를 선택해주세요."
 */
class LinearSpinnerString : LinearLayout {
    lateinit var b: ViewSpinnerCommonBinding

    private lateinit var list: ArrayList<String>
    var selected: Int = 0

    constructor(context: Context) : super(context)
    constructor(context: Context, enable: Boolean, info: String?, typeList: TypeList, initNo: Int?, onSelected: (typeNo: Int) -> Unit) : super(context) {
        b = DataBindingUtil.inflate(LayoutInflater.from(context), R.layout.view_spinner_common, this, true)

        list = typeList.list(info)
        b.spin.isEnabled = enable
        b.spin.adapter = SpinnerStringAdapter(list)
        b.spin.onItemSelected(
                onNothin = {},
                onSelected = { _, _, p, _ ->
                    selected = p
                    if (p > 0) onSelected(typeList.getEnumNo(list[p]))   // 첫번째는 안내멘트이기 때문에 생략 : STring으로 no받아서 return
                })

        // 초기 선택 position : 해당 type.no 필요.
        initNo?.let { no ->
            val title = typeList.getEnumString(no)
            list.forEachIndexed { i, s ->
                if (s == title) b.spin.setSelection(i)
            }
        }

//        // 초기 선택 position
//        initString?.let {
//            list.forEachIndexed { i, s ->
//                if (s == initString) b.spin.setSelection(i)
//            }
//        }


    }

    /**
     * 선택 바꾸기 : type.no 필요.
     */
    fun changeSelect(title: String) {
        list.forEachIndexed { i, s ->
            if (s == title) b.spin.setSelection(i)
        }
    }




}