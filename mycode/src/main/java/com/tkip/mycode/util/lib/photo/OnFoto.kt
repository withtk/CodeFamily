package com.tkip.mycode.util.lib.photo


class OnFoto {

    companion object {


        /** 메인포토   설정. */
        fun resetMainFoto(items:ArrayList<Foto>, foto:Foto) {
            items.remove(foto)
            for (photo in items) {
                photo.main = false
            }
            foto.main = true
            items.add(0, foto)
        }




        /**
         * 앨범에서 돌아온 뒤 selectedFotoList 세팅.
         */
        fun setSelectedFotoList(listFromAlbum: ArrayList<MyPhoto>, selectedFotoList: ArrayList<Foto>) {
            val newList = arrayListOf<Foto>()

            /**  selectedFotoList에 없는 Uri를 newList에 저장.  */
            for (myPhoto in listFromAlbum) {
                if (!selectedFotoList.has(myPhoto)) {
                    newList.add(Foto(myPhoto))
                }
            }

            /**  추린 newList를 selectedFotoList에 삽입.  */
            for (photo in newList) {
                selectedFotoList.add(photo)
            }

            /** 메인포토 자동 설정. */
//            if (PhotoUtil.mainFoto == null) {
//                selectedFotoList[0].isMain = true
//                PhotoUtil.mainFoto = selectedFotoList[0]
//            }

//        pagerAdapter.selectedFotoList.add(Foto("https://www.bloter.net/wp-content/uploads/2016/08/13239928_1604199256575494_4289308691415234194_n-765x510.jpg", "https://cdn.pixabay.com/photo/2017/04/09/09/56/avenue-2215317_960_720.jpg"))
//        pagerAdapter.selectedFotoList.add(Foto("https://www.cfacdn.com/img/order/menu/Online/Sides%26treats/smallWaffleFries_desktop.png", "https://images.unsplash.com/photo-1561927315-5673ff0235ab?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=1900&q=80"))


        }


//        /**
//         * list1 : selectedFotoList
//         * list2 : DbFotoList
//         *
//         * 중복 : 이미 있는 Foto.
//         * 미중복 : 올릴 Foto.
//         * 업로드하는 Foto의 index 리스트
//         */
//        fun setupUploadUris(selectedFotoList: ArrayList<Foto>, dbFotoList: ArrayList<Foto>): ArrayList<Int> {
//            val intList = arrayListOf<Int>()
//
//            PhotoUtil.photoUris.clear()
//            loop1@ for ((i, f) in selectedFotoList.withIndex()) {
//                for (f2 in dbFotoList) {
//                    if (f.key == f2.key) continue@loop1  // 중복되면 다음f에서부터 다시루프.
//                }
//                PhotoUtil.photoUris.add(PhotoUtil.getResourceUri(f.photoId!!))
//                intList.add(i)   // 업로드될 foto의 index 목록에 추가.
//                f.clear()   // MyPhoto적 요소들을 모두 삭제.
//            }
//            return intList
//        }

        /**
         * db에서 지울 list  리턴.
         * list1 - list2
         */
        private fun ArrayList<Foto>.getDeleteList(dbList: ArrayList<Foto>): ArrayList<Foto> {
            val list = arrayListOf<Foto>()
            dbList.forEach { f1 ->
                forEach { f2 ->
                    if (f1.key == f2.key) list.add(f1)  // 중복되면
                }
            }
            return list
        }

        /**
         * db에서 지울 list 작성.
         * list1 : DbFotoList
         * list2 : selectedFotoList
         */
        fun setupDeleteList(dbFotoList: ArrayList<Foto>, selectedFotoList: ArrayList<Foto>): ArrayList<Foto> {
            val list = arrayListOf<Foto>()

            for (f in dbFotoList) {
                if (!f.isIncluded(selectedFotoList)) list.add(f)
            }
            return list
        }

        private fun Foto.isIncluded(list2: ArrayList<Foto>): Boolean {
            for (f2 in list2) {
                if (f2.key == this.key) return true
            }
            return false
        }


        /**
         * 업로드할 foto 카운트
         */
        fun getCountFoto(selectedFotoList: ArrayList<Foto>, dbFotoList: ArrayList<Foto>): Int {
            var count = 0
            loop1@ for (f in selectedFotoList) {
                for (f2 in dbFotoList) {
                    if (f.key == f2.key) continue@loop1
                }
                count++
            }
            return count
        }


    }

}