package com.tkip.mycode.util.tools.anim

import android.view.View
import android.view.animation.Animation
import android.view.animation.AnimationUtils
import android.widget.LinearLayout
import androidx.core.view.children
import com.tkip.mycode.R
import com.tkip.mycode.funs.common.lllogD
import com.tkip.mycode.funs.common.lllogI


/*************************************************************************************/
/********************  ***********************************************************/
/*************************************************************************************/


private fun executeAnim(view: View, animId: Int, visibilityStart: Int?, visibilityEnd: Int?) {
    val animation = AnimationUtils.loadAnimation(view.context, animId)
    animation.setAnimationListener(object : Animation.AnimationListener {
        override fun onAnimationStart(animation: Animation) {
//            lllogI("funAnim executeAnim onAnimationStart animation : $animation")
            visibilityStart?.let { view.visibility = it }
        }

        override fun onAnimationEnd(animation: Animation) {
//            lllogI("funAnim executeAnim onAnimationEnd animation : $animation")
            visibilityEnd?.let { view.visibility = it }
        }

        override fun onAnimationRepeat(animation: Animation) {}
    })
    view.startAnimation(animation)
}

private fun View.showCommon(resId: Int, reAnimate: Boolean) {
    if (reAnimate) {
//        lllogI("funAnim showCommon reAnimate")
        executeAnim(this, resId, View.VISIBLE, null)
    } else {
//        lllogI("funAnim showCommon false visibility ${this.visibility}")
        if (!this.isVisi()) {  // 안 보이는 상태이면
            lllogI("funAnim showCommon !isVisi  ")
            executeAnim(this, resId, View.VISIBLE, null)
        }
    }
}

private fun View.hideCommon(resId: Int, reAnimate: Boolean) {
//    lllogD("hideCommon hideCommon isVisi ${this.isVisi()}")

    if (reAnimate) executeAnim(this, resId, null, View.INVISIBLE)
    else if (this.isVisi()) executeAnim(this, resId, null, View.INVISIBLE)
}

private fun View.goneCommon(resId: Int, reAnimate: Boolean) {
    if (reAnimate) executeAnim(this, resId, null, View.GONE)
    else if (this.isVisi()) executeAnim(this, resId, null, View.GONE)
}


/**********************************************
 * sttBasic ************************************/
fun View.visi() {
    visibility = View.VISIBLE
}

fun View.hide() {
    visibility = View.INVISIBLE
}

fun View.gone() {
    visibility = View.GONE
}

fun View.isVisi() = visibility == View.VISIBLE

/**********************************************
 * sttSlide ************************************/
fun View.showSlideUp(reAnimate: Boolean) {
    showCommon(R.anim.slide_up_show, reAnimate)
}

fun View.hideSlideDown(reAnimate: Boolean) {
    hideCommon(R.anim.slide_right_hide, reAnimate)
}

fun View.goneSlideDown(reAnimate: Boolean) {
    goneCommon(R.anim.slide_right_hide, reAnimate)
}

fun View.hideSlideDown(isGone: Boolean, reAnimate: Boolean) {
    if (isGone) goneCommon(R.anim.slide_right_hide, reAnimate)
    else hideCommon(R.anim.slide_right_hide, reAnimate)
}

fun View.showSlideDown(reAnimate: Boolean) {
    showCommon(R.anim.slide_down_show, reAnimate)
}

fun View.hideSlideUp(reAnimate: Boolean) {
    hideCommon(R.anim.slide_up_hide, reAnimate)
}

fun View.hideSlideUp(isGone: Boolean, reAnimate: Boolean) {
    if (isGone) goneCommon(R.anim.slide_up_hide, reAnimate)
    else hideCommon(R.anim.slide_up_hide, reAnimate)
}

fun View.showSlideRight(reAnimate: Boolean) {
    showCommon(R.anim.slide_right_show, reAnimate)
}

fun View.showSlideLeft(reAnimate: Boolean) {
    showCommon(R.anim.slide_left_show, reAnimate)
}

fun View.hideSlideRight(reAnimate: Boolean) {
    hideCommon(R.anim.slide_right_hide, reAnimate)
}


/****************************************
 * sttFade******************************/
fun View.showFadeIn(reAnimate: Boolean) {
    showCommon(R.anim.fade_in, reAnimate)
}

fun View.hideFadeOut(reAnimate: Boolean) {
    hideCommon(R.anim.fade_out, reAnimate)
}

fun View.goneFadeOut(reAnimate: Boolean) {
    goneCommon(R.anim.fade_out, reAnimate)
}


fun View.showFadeIn(isShow: Boolean, reAnimate: Boolean) {
    if (isShow) showCommon(R.anim.fade_in, reAnimate)
    else hideCommon(R.anim.fade_out, reAnimate)
}


/****************************************
 * sttToggle******************************/

fun View.animRotateArrow() {
    if (rotation == 0f) this.animRotate180() else this.animRotate0()
}

fun View.animRotate180() {
    animate().setDuration(200).rotation(180f)
}

fun View.animRotate0() {
    animate().setDuration(200).rotation(0f)
}

fun View.animTurning() {
    executeAnim(this,R.anim.turning_infinite,null,null)
}

fun View.animStopTurning() {
     clearAnimation()
}


/****************************************
 * acToggle******************************/
fun View.toggleUpDown(  reAnimate: Boolean) {
    AnimUtil.toggle(this, R.anim.slide_down_show, R.anim.slide_up_hide, reAnimate)
}


/****************************************
 * sttOvershoot******************************/
fun View.showOverUp(reAnimate: Boolean) {
    showCommon(R.anim.over_up_show, reAnimate)
}

//boolean에 따라 보여지고 감추고 매번 작동해야 할 때.
fun View.showOverUp(isShow: Boolean, reAnimate: Boolean) {
    if (isShow) showCommon(R.anim.over_up_show, reAnimate)
    else hideOverDown(reAnimate)
}

fun View.hideOverDown(reAnimate: Boolean) {
    hideCommon(R.anim.over_down_hide, reAnimate)
}

fun View.showOverRight(reAnimate: Boolean) {
    showCommon(R.anim.over_to_right_show, reAnimate)
}

fun View.showOverRight(vararg views: View) {
    views.forEach { it.showCommon(R.anim.over_to_right_show, true) }
}

fun View.hideOverRight(reAnimate: Boolean) {
    hideCommon(R.anim.over_to_right_hide, reAnimate)
}

fun View.hideOverRight(vararg views: View) {
    views.forEach { it.hideCommon(R.anim.over_to_right_hide, true) }
}

/* fun View.showBounce(view: View) {
     val animBounce = AnimationUtils.loadAnimation(view.context, R.anim.bounce)
     val myBounceInterpolator = MyBounceInterpolator(0.2, 20)
     animBounce.interpolator = myBounceInterpolator
     view.startAnimation(animBounce)
 }
*/





/****************************************
 * sttParent******************************/
fun LinearLayout.hideAllFadeOut() {
    this.children.forEach { it.hideFadeOut(false) }
}


