package com.tkip.mycode.util.lib

import androidx.recyclerview.widget.ItemTouchHelper
import androidx.recyclerview.widget.RecyclerView
import com.tkip.mycode.funs.common.lllogI

class MyTouchHelper(val listener: OnMoveListener) : ItemTouchHelper.Callback() {
    interface OnMoveListener {
        fun onItemMove(fromP: Int, toP: Int)
        fun onItemSwipe(p: Int, direction: Int)
    }

    override fun getMovementFlags(recyclerView: RecyclerView, viewHolder: RecyclerView.ViewHolder): Int {
        val drag = ItemTouchHelper.UP.or(ItemTouchHelper.DOWN)
        val swipe = ItemTouchHelper.UP.or(ItemTouchHelper.DOWN)
        return makeMovementFlags(drag, swipe)
    }

    override fun onMove(recyclerView: RecyclerView, viewHolder: RecyclerView.ViewHolder, target: RecyclerView.ViewHolder): Boolean {
        listener.onItemMove(viewHolder.adapterPosition, target.adapterPosition)
        return true
    }

    override fun onSwiped(viewHolder: RecyclerView.ViewHolder, direction: Int) {
        lllogI("dragtest StMenuManageHelper onSwiped direction $direction")
        listener.onItemSwipe(viewHolder.adapterPosition, direction)
    }

}