package com.tkip.mycode.util.tools.etc

import java.net.HttpURLConnection
import java.net.URL

class OnInternet() : Thread() {

    private val CONNECTION_CONFIRM_CLIENT_URL = "http://clients3.google.com/generate_204";
    var success: Boolean = false

    override fun run() {
        var conn: HttpURLConnection? = null
        try {
            conn = URL(CONNECTION_CONFIRM_CLIENT_URL).openConnection() as HttpURLConnection
            conn.setRequestProperty("User-Agent", "Android")
            conn.connectTimeout = 1000
            conn.connect()
            val responseCode = conn.responseCode
            success = responseCode == 204
        } catch (e: Exception) {
            e.printStackTrace()
            success = false
        }

        conn?.disconnect()
    }

    private fun isSuccess(): Boolean {
        return success
    }

    companion object {


        fun isOffLine(): Boolean {
            val cc = OnInternet()
            cc.start()
            try {
                cc.join()

                return if (cc.isSuccess()) {
                    !cc.isSuccess()
                } else {
                    true
                }
            } catch (e: Exception) {
                e.printStackTrace()
            }

            return true
        }

    }
}