package com.tkip.mycode.util.lib.photo.util

import android.annotation.SuppressLint
import android.graphics.Bitmap
import android.net.Uri
import android.provider.MediaStore
import android.util.Size
import com.tkip.mycode.funs.common.lllogI
import com.tkip.mycode.init.part.CodeApp


/**
 * 썸네일 만들기
 */
@SuppressLint("NewApi")
fun Uri.createMyThumb( ): Bitmap? {
    lllogI("createMyThumb Uri $this")

    return   CodeApp.getAppContext().contentResolver.loadThumbnail(this, Size(500, 500), null)
//    return if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) context.contentResolver.loadThumbnail(this, Size(500, 500), null)
//    else null
}


/** * 이미지 띄우기 */
//fun ImageView.showImage(context: Context, uri: Uri) {
//    GlobalScope.launch {
//        withContext(Dispatchers.Main) {
//        val bitmap = uri.createMyThumb(context)
//            this@showImage.setImageBitmap(bitmap)
//        }
//    }
//}



/**
 * 썸네일 만들기
 */
fun getThumb4Q(imageId: String): Bitmap? {
    val contentUri = Uri.withAppendedPath(
            MediaStore.Images.Media.EXTERNAL_CONTENT_URI,
            imageId
    )
    return contentUri.createMyThumb( )
}


