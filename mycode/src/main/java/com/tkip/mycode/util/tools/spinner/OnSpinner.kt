package com.tkip.mycode.util.tools.spinner

import com.tkip.mycode.model.my_enum.PeerType
import com.tkip.mycode.model.peer.OnPeer

class OnSpinner {

    companion object {


        /**
         */
        fun getArrayPeerType(): Array<Int> {

            val list = arrayListOf<Int>()
//            list.add(PeerType.BLOCK_100.no)
//            list.add(PeerType.BLOCK_200.no)
//            list.add(PeerType.BLOCK_300.no)
            list.add(PeerType.BASIC_1.no)
            list.add(PeerType.BASIC_2.no)
            list.add(PeerType.BASIC_3.no)
            list.add(PeerType.VIP_1.no)

            when (OnPeer.peer.type) {
                PeerType.ADMIN_1.no -> {
                }
                PeerType.ADMIN_2.no -> list.add(PeerType.ADMIN_1.no)

                PeerType.ADMIN_SUPERVISOR.no -> {
                    list.add(PeerType.ADMIN_1.no)
                    list.add(PeerType.ADMIN_2.no)
                    list.add(PeerType.ADMIN_SUPERVISOR.no)
                }
                else -> list.clear()
            }

            return list.toTypedArray()

        }


    }

}