package com.tkip.mycode.util.lib.retrofit2

import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit


object Retro {


    var esApi: EsApiService? = null

    /**
     * null이면 만들어서 리턴
     */
    fun esApi(): EsApiService? {
        if (esApi == null) {
            esApi = initEsAPI()
        }
        return esApi
    }

    /**
     * 1.처음 셋팅할 때
     * 2.esAuth 오류일 경우에 다시 만들 때
     */
    fun initEsAPI(): EsApiService {

        val client: OkHttpClient = OkHttpClient.Builder()
                .addInterceptor(EsAuthInterceptor())
                .connectTimeout(10, TimeUnit.SECONDS)
                .readTimeout(30, TimeUnit.SECONDS)
                .build()

//        val elst: String = R.string.remote_elasticsearch_url.remoteStr()
        val elst: String ="https://4a7fd10391234e7b89777b8b109e635f.asia-northeast1.gcp.cloud.es.io:9243"

        val retrofit = Retrofit.Builder()
                .client(client)
                .baseUrl(elst)
                .addConverterFactory(GsonConverterFactory.create())
                .build()

        return retrofit.create(EsApiService::class.java)
    }

    fun resetEsApi() {
        esApi = null
        esApi = initEsAPI()
    }


    /**********************************************************************************
     ***********************이동할때****************************************************/

//    val esApi2: EsApiService by lazy {
//        lllogI("onSystem retro esApi2 esApi2 ")
//
//        val endPoint = "https://4a7fd158662s34e7b35477b8ffeb109e635f.asia-northeast1.gcp.cloud.es.io:9243/"
//        val id = "com8190"
//        val pw = "eOQ4nFD8fj34wR7t9Jof"
//
//        val token = Credentials.basic(id, pw)
//        val client: OkHttpClient = OkHttpClient.Builder()
//                .connectTimeout(5, TimeUnit.SECONDS)
//                .readTimeout(5, TimeUnit.SECONDS)
//                .addInterceptor { chain ->
//                    val newRequest: Request = chain.request().newBuilder()
//                            .addHeader("Authorization", token)
//                            .build()
//                    chain.proceed(newRequest)
//                }.build()
//
//        val retrofit = Retrofit.Builder()
//                .client(client)
//                .baseUrl(endPoint)
//                .addConverterFactory(GsonConverterFactory.create())
//                .build()
//        return@lazy retrofit.create(EsApiService::class.java)
//    }


    /* val esApi: EsApiService by lazy {

         lllogI("onSystem esApi esApi ")

         val client: OkHttpClient = OkHttpClient.Builder()
                 .addInterceptor(EsAuthInterceptor())
                 .connectTimeout(30, TimeUnit.SECONDS)
                 .readTimeout(30, TimeUnit.SECONDS)
                 .build()

 //        val token = Credentials.basic(R.string.remote_elasticsearch_id.remoteStr(), R.string.remote_elasticsearch_pw.remoteStr())
 //        val client: OkHttpClient = OkHttpClient.Builder()
 //                .connectTimeout(30, TimeUnit.SECONDS)
 //                .readTimeout(30, TimeUnit.SECONDS)
 //                .addInterceptor { chain ->
 //                    val newRequest: Request = chain.request().newBuilder()
 //                            .addHeader("Authorization", token)
 //                            .build()
 //                    chain.proceed(newRequest)
 //                }.build()

         val elst: String = R.string.remote_elasticsearch_url.remoteStr()
 //        val elst = "https://d477736aee534e0dbd7585907950a934.asia-northeast1.gcp.cloud.es.io:9243/"


         val retrofit = Retrofit.Builder()
                 .client(client)
                 .baseUrl(elst)
                 .addConverterFactory(GsonConverterFactory.create())
                 .build()
 //        lllogI("Retro ELSU $ELSU")
         return@lazy retrofit.create(EsApiService::class.java)
     }*/

/*
// 싱글톤?
    fun <S> createService(serviceClass: Class<S>): S {
        val retrofit = Retrofit.Builder()
                .baseUrl(ELSU)
                .addConverterFactory(GsonConverterFactory.create())
                .build()
        return retrofit.create(serviceClass)
    }



//    companion object {
//        private val retrofitClient: Retro = Retro()
//
//        fun getInstance(): Retro {
//            return retrofitClient
//        }
//    }


//    private val retrofitClient: Retro? = null


//    fun init() {
//
//        retrofitClient =Retro()
//    }


    fun buildRetrofit(): EsApiService {


        val retrofit: Retrofit? = Retrofit.Builder()
                .baseUrl("http://yongyi1587.tistory.com/")
                .addConverterFactory(GsonConverterFactory.create())
                .build()

        val service: EsApiService = retrofit!!.create(EsApiService::class.java)
        return service
    }
*/
}