package com.tkip.mycode.util.tools.fire

import android.content.Context
import com.google.firebase.firestore.*
import com.tkip.mycode.auth.OnSign
import com.tkip.mycode.dialog.OnDDD
import com.tkip.mycode.dialog.progress.MyCircle
import com.tkip.mycode.dialog.progress.MyProgress
import com.tkip.mycode.funs.common.OnException
import com.tkip.mycode.funs.common.hhh
import com.tkip.mycode.funs.common.lllogD
import com.tkip.mycode.funs.common.lllogI
import com.tkip.mycode.init.*
import com.tkip.mycode.init.base.checkUpdate
import com.tkip.mycode.model.peer.recommend.UidDate
import com.tkip.mycode.model.bid.Bid
import com.tkip.mycode.model.board.Board
import com.tkip.mycode.model.cost.Cost
import com.tkip.mycode.model.count.Count
import com.tkip.mycode.model.count.CountType
import com.tkip.mycode.model.grant.Grant
import com.tkip.mycode.model.manage.Device
import com.tkip.mycode.model.manage.Notice
import com.tkip.mycode.model.manage.SystemMan
import com.tkip.mycode.model.my_enum.ActType
import com.tkip.mycode.model.my_enum.Status
import com.tkip.mycode.model.my_enum.dataType
import com.tkip.mycode.model.mytag.MyTag
import com.tkip.mycode.model.peer.OnPeer
import com.tkip.mycode.model.peer.Peer
import com.tkip.mycode.model.peer.isSupervisor
import com.tkip.mycode.model.peer.myuid
import com.tkip.mycode.model.peer.peer_allow.PeerAllow
import com.tkip.mycode.model.post.Post
import com.tkip.mycode.model.reply.Reply
import com.tkip.mycode.model.room.Room
import com.tkip.mycode.model.store.Store
import com.tkip.mycode.model.store.StoreMenu
import com.tkip.mycode.model.ticket.Ticket
import com.tkip.mycode.model.warn.Warn
import com.tkip.mycode.nav.lobby.Lobby
import com.tkip.mycode.nav.lobby.chat.Chat
import com.tkip.mycode.util.lib.calendar.OnCalendar
import com.tkip.mycode.util.lib.photo.util.PhotoUtil
import com.tkip.mycode.util.lib.retrofit2.ESget
import com.tkip.mycode.util.lib.retrofit2.ESquery
import com.tkip.mycode.util.tools.date.rightNow


class Firee {

    companion object {


        // -----------------------------------------------------------------------------------
        // ------------sttCommon--------------------------------------------------
        // -----------------------------------------------------------------------------------

//        /**
//         * 1개의 doc 받아올 때만 사용.
//         * size는 무조건 짝수 (collection, document)
//         */
//        fun docRef(vararg cols: String) =
//                when (cols.size) {
//                    2 -> FirebaseFirestore.getInstance().collection(cols[0]).document(cols[1])
//                    4 -> FirebaseFirestore.getInstance().collection(cols[0]).document(cols[1]).collection(cols[2]).document(cols[3])
//                    6 -> FirebaseFirestore.getInstance().collection(cols[0]).document(cols[1]).collection(cols[2]).document(cols[3]).collection(cols[4]).document(cols[5])
//                    else -> FirebaseFirestore.getInstance().collection("common").document(createKey())   // 기본값은 그냥 넣었음.
//                }
//
//        /**
//         * collection 전체 받아올 Ref
//         * size는 무조건 홀수 (collection, document, collection)
//         */
//        fun colRef(vararg cols: String) =
//                when (cols.size) {
//                    1 -> FirebaseFirestore.getInstance().collection(cols[0])
//                    3 -> FirebaseFirestore.getInstance().collection(cols[0]).document(cols[1]).collection(cols[2])
//                    5 -> FirebaseFirestore.getInstance().collection(cols[0]).document(cols[1]).collection(cols[2]).document(cols[3]).collection(cols[4])
//                    else -> FirebaseFirestore.getInstance().collection("<common>")   // 기본값은 그냥 넣었음.
//                }

        /**
         * <common> doc 1개 처리
         */
        fun commonADD(methodName: String, ref: DocumentReference, any: Any, success: () -> Unit, fail: (Exception) -> Unit) {

            ref.set(any)
                    .addOnSuccessListener {
                        lllogD("commonADD : success $methodName ")
                        success()
                    }
                    .addOnFailureListener { e ->
                        MyProgress.cancel()
                        fail(e)
                        OnException.dbfailed("commonADD fail: $methodName ---- ", e)
                    }
        }

        private fun commonGET(methodName: String, ref: DocumentReference, success: (DocumentSnapshot?) -> Unit, fail: (Exception) -> Unit) {
            if (OnPeer.notAllowedDB()) {
                fail(Exception())
                return
            }
            ref.get()
                    .addOnSuccessListener {
                        //                        Log.d(TAG, "commonGET : success get! $it")
                        success(it)
                    }
                    .addOnFailureListener { e ->
                        MyProgress.cancel()
                        fail(e)
                        OnException.dbfailed("commonGET fail: $methodName  ", e)
                    }
        }


        private fun commonDEL(methodName: String, ref: DocumentReference, success: (DocumentSnapshot?) -> Unit, fail: (Exception) -> Unit) {
            if (OnPeer.notAllowedDB()) {
                fail(Exception())
                return
            }
            ref.delete()
                    .addOnSuccessListener {
                        lllogD("commonDEL : success delete ")
                        success(null)
                        MyCircle.cancel()
//                                MyProgress.cancel()
                    }.addOnFailureListener { e ->
                        MyProgress.cancel()
                        fail(e)
                        OnException.dbfailed("commonDEL fail: $methodName  ", e)
                    }
        }

        fun commonQueryGet(query: Query, success: (Boolean) -> Unit, fail: (Exception) -> Unit) {
            query.get()
                    .addOnSuccessListener {
                        lllogD("commonQueryGet count : " + it.size())
                        success(it.isEmpty)
                    }
                    .addOnFailureListener {
                        OnException.dbfailed("commonQueryGet fail:", it)
                        fail(it)
                    }
        }

        /** data 존재여부 검색 **/
        private fun commonGETequalTo(ref: CollectionReference, child: String, nick: String, success: (QuerySnapshot) -> Unit, fail: () -> Unit) {
            if (OnPeer.notAllowedDB()) {
                fail()
                return
            }
            ref.whereEqualTo(child, nick)
                    .get()
                    .addOnSuccessListener {
                        success(it)
                    }
                    .addOnFailureListener {
                        fail()
                        MyProgress.cancel()
                        OnException.dbfailed("commonGETequalTo", it)
                    }
        }

        private fun Query.totalQueryGet(success: (QuerySnapshot) -> Unit, fail: (Exception) -> Unit) {
            get()
                    .addOnSuccessListener { querySnapShot ->
                        lllogD("totalQueryGet success querySnapShot.size : ${querySnapShot.size()}")
                        success(querySnapShot)
                    }
                    .addOnFailureListener {
                        fail(it)
                        MyProgress.cancel()
                        OnException.dbfailed("totalQueryGet", it)
                    }
        }

        fun commonFirstQuery(cols: Array<String>, limit: Long?, success: (QuerySnapshot) -> Unit, fail: () -> Unit) {
            val query = colRef(*cols)
                    .orderBy(CH_ADD_DATE, Query.Direction.DESCENDING)
            limit?.let { query.limit(it) }
            query.totalQueryGet({ success(it) }, { fail() })
        }

        fun commonSecondQuery(cols: Array<String>, lastDoc: DocumentSnapshot, limit: Long, success: (QuerySnapshot) -> Unit, fail: () -> Unit) {
            val query = colRef(*cols)
                    .orderBy(CH_ADD_DATE, Query.Direction.DESCENDING)
                    .startAt(lastDoc)
                    .limit(limit)
            query.totalQueryGet({ success(it) }, { fail() })
        }

        /**
         * <common> GET collection 전체 받아오기.
         */
        private fun commonGetCOL(methodName: String, cols: Array<String>, success: (QuerySnapshot?) -> Unit, fail: (Exception) -> Unit) {
            if (OnPeer.notAllowedDB()) {
                fail(Exception())
                return
            }
            colRef(*cols).get()
                    .addOnSuccessListener {
                        lllogD("  $methodName : success get!  ")
                        success(it)
                    }
                    .addOnFailureListener { e ->
                        MyProgress.cancel()
                        fail(e)
                        OnException.dbfailed("  $methodName fail :", e)
                    }
        }


        /******************************************************************************************
         ******************************************************************************************
         ******************************************************************************************
         *****************************************************************************************/

//        /**         * 모든 dataType 적용가능.         */
//        private fun commonBatch(methodName: String, arr: ArrayList<Quad<Int, DocumentReference, String?, Any?>>, success: () -> Unit, fail: (Exception) -> Unit) {
//            if (OnPeer.notAllowedDB()) {
//                fail(Exception())
//                return
//            }
//
//            FirebaseFirestore.getInstance().batch().apply {
//                arr.forEach {
//                    when (it.t1) {
//                        ADD -> set(it.t2, it.t4!!)
//                        UPDATE -> update(it.t2, it.t3!!, it.t4)
//                        DEL -> delete(it.t2)
//                    }
//                }
//                commit()
//                        .addOnSuccessListener {
//                            Log.d(TAG, "$methodName written!")
//                            success()
//                        }.addOnFailureListener {
//                            MyProgress.cancel()
//                            OnException.dbfailed("  $methodName fail :", it)
//                            fail(it)
//                        }
//            }
//        }

//        /**         * batch에 추가할 list 만들기         */
//        fun batchList(vararg quadGroup: Quad<Int, DocumentReference, String?, Any?>) = arrayListOf<Quad<Int, DocumentReference, String?, Any?>>().apply { quadGroup.forEach { add(it) } }

//        fun batch() = FirebaseFirestore.getInstance().batch()

//        /**         * 정리된 batch를 commit         */
//        private fun batchCommit(methodName: String, batch: WriteBatch, success: () -> Unit, fail: (Exception) -> Unit) {
//            if (OnPeer.notAllowedDB()) {
//                fail(Exception())
//                return
//            }
//            batch.commit()
//                    .addOnSuccessListener {
//                        Log.d(TAG, "$methodName written!")
//                        success()
//                    }.addOnFailureListener {
//                        MyProgress.cancel()
//                        OnException.dbfailed("  $methodName fail :", it)
//                        fail(it)
//                    }
//        }


        /**
         * <common> 실시간 리슨
         */
        fun commonRealTimeDoc(methodName: String, cols: Array<String>, success: (DocumentSnapshot) -> Unit, fail: (Exception) -> Unit) =
                docRef(*cols).addSnapshotListener { snapshot, e ->
                    if (e != null) {
                        OnException.dbSyncFailed(methodName, e)
                        fail(e)
                        return@addSnapshotListener
                    }
                    snapshot?.let { snap ->
                        success(snap)
                    }
                }


        fun commonRealTimeCol(methodName: String, query: Query, success: (QuerySnapshot?) -> Unit, fail: (Exception) -> Unit) =
                query.addSnapshotListener { snapshots, e ->
                    if (e != null) {
                        OnException.dbSyncFailed(methodName, e)
                        fail(e)
                        return@addSnapshotListener
                    }
                    success(snapshots)

//                    for (dc in snapshots!!.documentChanges) {
//                        when (dc.no) {
//                            DocumentChange.Type.ADDED -> Log.d(TAG, "New city: ${dc.document.data}")
//                            DocumentChange.Type.MODIFIED -> Log.d(TAG, "Modified city: ${dc.document.data}")
//                            DocumentChange.Type.REMOVED -> Log.d(TAG, "Removed city: ${dc.document.data}")
//                        }
//                    }
                }


        /******************************************************************************
         ************************  sttQuery  *******************************************
         *******************************************************************************/

//        fun firstQuery(col: Array<String>, limit: Long, success: (QuerySnapshot) -> Unit, fail: () -> Unit) {
////            val nextQuery = FirebaseFirestore.getInstance().collection(col)
//
//            val nextQuery = colRef(*col)
//                    .whereEqualTo(CH_TYPE, PostType.FREE)
//                    .whereGreaterThanOrEqualTo(CH_STATUS_POINT, Status.DELETED.no)
//                    .orderBy(CH_STATUS_POINT, Query.Direction.DESCENDING)
//                    .orderBy(CH_ADD_DATE, Query.Direction.DESCENDING)
//                    .limit(limit)
//
////                    .whereLessThanOrEqualTo(CH_STATUS_POINT,Status.DELETED.no)
//
//            totalQueryGet(nextQuery, { success(it) }, { fail() })
//        }
//
//
//        /**
//         *   lastDoc의 다음 위치부터 받아오기 limit 만큼
//         */
//        fun secondQuery(col: String, lastDoc: DocumentSnapshot, limit: Long, success: (QuerySnapshot) -> Unit, fail: () -> Unit) {
//
//            val nextQuery = FirebaseFirestore.getInstance().collection(col)
//                    .whereEqualTo(CH_TYPE, PostType.FREE)
//                    .whereEqualTo(CH_INVALID, false)
//                    .startAfter(lastDoc)
//                    .limit(limit)
//            totalQueryGet(nextQuery, { success(it) }, { fail() })
//        }


        /******************** sttpeer *************************************************/
        /**         *  최초 가입시에만 호출.         */
        fun addPeer(peer: Peer, success: () -> Unit, fail: () -> Unit) {

            batch().apply {
                set(docRef(EES_PEER, peer.uid), peer)          // add peer

//                /*** 베타 테스터 보너스 : 0 이하이면 적용 안 됨. */
//                ActType.BETA_FREE_BONUS_CREDIT.getCrdAmount().let { if (it > 0) addAct(Cost.new(null, ActType.BETA_FREE_BONUS_CREDIT, null)) }

                /*** 중고거래, 부동산 Quick메뉴에 넣기 */
                fleaBoard?.let {
                    val fleaCount = Count(it, CountType.BOOK).apply { quick = true }
                    set(docRef(EES_QUICK, fleaCount.key), fleaCount)    // add quick
                    set(docRef(fleaCount.topChildName, fleaCount.key), fleaCount)   // add Book count
                }
                roomBoard?.let {
                    val roomCount = Count(it, CountType.BOOK).apply { quick = true }
                    set(docRef(EES_QUICK, roomCount.key), roomCount)    // add quick
                    set(docRef(roomCount.topChildName, roomCount.key), roomCount)   // add Book count
                }

                addCost(Cost.new(null, ActType.SIGN_UP, null))  // add act 신규가입(보너스 포인트)

//                DeviceInfo.addToBatch(this, peer.uid, peer.nick)
//                addDeviceInfo(Device.create(peer.uid, peer.nick))
//                addCost(Cost.create(peer.uid, ActType.ADD_REWARD_POST, post, reward))

                myCommit("addPeer",
                        success = {
                            OnPeer.setUpPeer(peer)    // Sred Setup
                            success()
                        }, fail = { fail() })
            }

            Device.create(peer.uid, peer.nick).real()   // real에 올리기.
        }

        /**  * login 시간 기입. */
        fun loginPeer(peer: Peer, success: () -> Unit, fail: () -> Unit) {
            batch().apply {
                update(docRef(EES_PEER, peer.uid), CH_LOGIN_TIME, rightNow())
            }.myCommit(null, success = {
                success()
                Cost.new(null, ActType.LOGIN, null).real()  // 출석체크
            }, fail = { fail() })
//            PeerUID().real()   // 로그인
//            Cost.new(null,ActType.LOGIN, null).real()
        }

        /**         * Peer 받아오기         */
        fun getPeer(uid: String, success: (Peer?) -> Unit, fail: () -> Unit) {
            if (peerMap[uid] == null)
                ESget.getPeer(uid,
                        success = { peer1 ->
                            lllogD("AuthUiActivity peer1 $peer1")
                            if (peer1 != null) {
                                peerMap[peer1.uid] = peer1
                            }// peerMap에 추가.
                            success(peer1)
                        }, fail = { fail() })
            else success(peerMap[uid])
        }

        /*** FireDB에서 받아오기 */
        private fun getFireePeer(uid: String, success: (Peer?) -> Unit, fail: () -> Unit) {
            commonGET("getFireePeer", docRef(EES_PEER, uid),
                    success = {
                        val result = it?.toObject(Peer::class.java)
                        success(result)
                        result?.let { peerMap[result.uid] = result }
                    }, fail = { fail() })
        }

        /**  로그아웃시 fcmToken 삭제  */
        fun peerDeleteFcmToken(isDuplicate: Boolean, success: () -> Unit, fail: () -> Unit) {
            batch().apply {
                if (isDuplicate) Cost.new(null, ActType.SIGN_OUT_DUPLICATE, null).real()
                else {
                    update(docRef(EES_PEER, myuid()), CH_FCM_TOKEN, null)
                    Cost.new(null, ActType.SIGN_OUT, null).real()
                }
                myCommit("peerDeleteFcmToken", success = { success() }, fail = { fail() })
            }
        }

        /**  탈퇴시 delete날짜 기록, invalid true   */
        fun leavePeer(success: () -> Unit, fail: () -> Unit) {
            batch().apply {
                update(docRef(EES_PEER, myuid()), CH_DEL_DATE, OnCalendar.getLongAfterMonth(OnSign.postponeMonth))    // 재가입 가능 날짜long
                update(docRef(EES_PEER, myuid()), CH_INVALID, true)    // invalid 처리.
                myCommit("deletePeer", success = {
                    success()
                    Cost.new(null, ActType.LEAVE, null).real()
                }, fail = { fail() })
            }
        }

        /*** 완전 삭제 */
        fun deletePeer(peer: Peer, onSuccess: () -> Unit, onFail: () -> Unit) {
            commonDEL("deleteBoardMiniPeer", docRef(EES_PEER, peer.uid), success = { onSuccess() }, fail = { onFail() })
        }

        /*** 익명에서 구글로 전환할 때 OR 재로그인  */
        fun updatePeer(oldPeer: Peer, newPeer: Peer, success: () -> Unit, fail: () -> Unit) {

            val map = mutableMapOf<String, Any?>().apply {
                checkUpdate(oldPeer.dpName, newPeer.dpName, CH_DP_NAME)
                checkUpdate(oldPeer.email, newPeer.email, CH_EMAIL)
                checkUpdate(oldPeer.type, newPeer.type, CH_TYPE)
                checkUpdate(oldPeer.loginFrom, newPeer.loginFrom, CH_LOGIN_FROM)
                checkUpdate(oldPeer.delDate, newPeer.delDate, CH_DEL_DATE)
                newPeer.fcmToken?.let { put(CH_FCM_TOKEN, it) }
            }

            map.forEach { (a, b) -> lllogI("updatePeer String $a Any $b") }   // dataCheck

            batch().apply {
                this.update(docRef(EES_PEER, oldPeer.uid), map)
                addAct(Cost.new(oldPeer.uid, ActType.RE_LOGIN, null))
//                DeviceInfo.addToBatch(this, oldPeer.uid, oldPeer.nick)
//                addDeviceInfo(Device.create(oldPeer.uid, oldPeer.nick))
                myCommit("updatePeer",
                        success = { success() }, fail = { fail() })
            }
            Device.create(oldPeer.uid, oldPeer.nick).real()   // real에 올리기.


//            val locale = ConfigurationCompat.getLocales(Resources.getSystem().configuration).get(0)
//            val timeZone = TimeZone.getDefault()
//            val deviceInfo =    DeviceInfo.addToBatch(this,peer1.uid,peer1.nick)
//
//            val deviceInfo = DeviceInfo(peer1, locale, timeZone, peer1.addDate)
//
//            val batch = FirebaseFirestore.getInstance().batch()
//            if (loginStatus == LoginFrom.GOOGLE_CHANGED) {
//                batch.update(docRef(CH_PEER, peer1.uid), CH_EMAIL, peer1.email)
//                batch.update(docRef(CH_PEER, peer1.uid), CH_LOGIN_FROM, peer1.loginFromNo)
//            }
//            batch.update(docRef(CH_PEER, peer1.uid), CH_FCM_TOKEN, peer1.fcmToken)
//            batch.update(docRef(CH_PEER, peer1.uid), CH_TYPE, peer1.type)
//            batch.set(docRef(COL_DEVICE_INFO, deviceInfo.key), deviceInfo)
//            batch.myCommit("updatePeer", { success() }, { fail() })
        }

        /**         * peer face 수정.         */
        fun updatePeerFace(uid: String, nThumb: String?, nUrl: String?, tobeDelThumb: String?, tobeDelFace: String?, success: () -> Unit, fail: () -> Unit) {
            batch().apply {
                update(docRef(EES_PEER, uid), CH_FACE_THUMB, nThumb)
                update(docRef(EES_PEER, uid), CH_FACE, nUrl)
                myCommit("updatePeerFace",
                        success = {
                            PhotoUtil.deletePhoto(tobeDelThumb, tobeDelFace)
                            success()
                        }, fail = { fail() })
            }
        }

        /**  <admin only>       * peer 전체 덮어쓰기 수정.         */
        fun updatePeer(peer: Peer, success: () -> Unit, fail: (Exception) -> Unit) {
            commonADD("updatePeer", docRef(EES_PEER, peer.uid), peer, success = { success() }, fail = { fail(it) })
        }

        /**         * Peer nickName, foto 업데이트         */
        fun updateNickAndPhoto(uid: String, nick: String?, photoThumb: String?, photo: String?, success: () -> Unit, fail: () -> Unit) {
            val batch = FirebaseFirestore.getInstance().batch()
            nick?.let { batch.update(docRef(EES_PEER, uid), CH_NICK, it) }
            photoThumb?.let {
                batch.update(docRef(EES_PEER, uid), CH_FACE_THUMB, photoThumb)
                batch.update(docRef(EES_PEER, uid), CH_FACE, photo)
            }
            batch.myCommit("updateNickAndPhoto", { success() }, { fail() })
        }

        /**         * Nick 존재여부 확인         */
        fun existNickName(nick: String, success: (Boolean) -> Unit, fail: () -> Unit) {
            commonGETequalTo(colRef(EES_PEER), CH_NICK, nick, success = {
                if (it.size() > 0) success(true)
                else success(false)
            }, fail = { fail() })
        }


        /******************** sttRecommend *************************************************/
        fun addRecommend(uidDate: UidDate, success: () -> Unit, fail: (Exception) -> Unit) {
            commonADD("addRecommend", docRef(EES_RECOMMEND, uidDate.key ), uidDate, success = { success() }, fail = { fail(it) })
        }


        /******************** sttPeerAllow *************************************************/
        fun addBoardAllow(peerAllow: PeerAllow, success: () -> Unit, fail: (Exception) -> Unit) {
            commonADD("addBoardMiniPeer", docRef(EES_BOARD, peerAllow.modelKey, EES_BOARD_ALLOW, peerAllow.key), peerAllow, success = { success() }, fail = { fail(it) })
        }

        fun deleteBoardAllow(peerAllow: PeerAllow, success: () -> Unit, fail: (Exception) -> Unit) {
            commonDEL("deleteBoardMiniPeer", docRef(EES_BOARD, peerAllow.modelKey, EES_BOARD_ALLOW, peerAllow.key), success = { success() }, fail = { fail(it) })
        }

        /******************** sttVcost *************************************************/
        fun addCost(cost: Cost, success: () -> Unit, fail: () -> Unit) {
            commonADD("addCost", docRef(EES_COST, cost.key), cost, success = { success() }, fail = { fail() })
        }

        fun deepDelCost(costKey: String, success: () -> Unit, fail: () -> Unit) {
            commonDEL("deepDelCost", docRef(EES_COST, costKey), success = { success() }, fail = { fail() })
        }

        /******************** sttBoard *************************************************/
        fun addBoard(board: Board, success: () -> Unit, fail: (Exception) -> Unit) {
            commonADD("addBoard", docRef(EES_BOARD, board.key), board, success = { success() }, fail = { fail(it) })
        }

        fun updateBoard(batch: WriteBatch, success: () -> Unit, fail: (Exception) -> Unit) {
            batch.myCommit("updateBoard", success = { success() }, fail = { fail(it) })
        }

        /**
         */
        fun getBoard(boardKey: String, success: (Board?) -> Unit, fail: () -> Unit) {
            ESget.getBoard(boardKey, success = { board1 -> success(board1) }, fail = { fail() })

//            commonGET("getBoard", docRef(EES_BOARD, boardKey), success = { success(it?.toObject(Board::class.java)) }, fail = { fail(it) })
            /*if (boardMap[boardKey] == null)
                ESget.getBoard(boardKey,
                        success = { board1 ->
                            if (board1 != null) boardMap[boardKey] = board1  // boardMap 추가.
                            success(board1)
                        }, fail = {fail() })
            else success(boardMap[boardKey])*/

        }

        /**         * BoardID 존재여부 확인   true:존재함.      */
        fun existBoardID(id: String, success: (Boolean) -> Unit, fail: () -> Unit) {
            ESget.sizeBoardList(ESquery.isBoardIDcheck(id), success = { if (it != null && it > 0) success(true) else success(false) }, fail = { fail() })

            // fireStore
//            commonGETequalTo(colRef(EES_BOARD), CH_BOARD_ID, id, success = {
//                it.forEach { snap ->
//                    val b = snap.toObject(Board::class.java)
//                    if (board.key == b.key) success(true)
//                }
//                success(false)
//            }, fail = { fail() })
        }


        /******************** sttPost *************************************************/
        fun addPost(post: Post, success: () -> Unit, fail: (Exception) -> Unit) {
            val batch = batch().apply {
                set(docRef(EES_POST, post.key), post)   // post
//                OnPay.addBatchAct(this, Cost.createAct(null, ActType.ADD_POST, null, post))    // cost addPost
//                post.reward?.let { OnPay.addBatchAct(this, Cost.createAct(null, ActType.ADD_REWARD_POST, -post.reward!!, post)) }    // cost reward post
            }
            batch.myCommit("addPost", success = { success() }, fail = { fail(it) })
//            commonADD("addPost", docRef(EES_POST, post.key), post, success = { success() }, fail = { fail(it) })
        }

        fun updatePost(batch: WriteBatch, success: () -> Unit, fail: (Exception) -> Unit) {
            batch.myCommit("updatePost", success = { success() }, fail = { fail(it) })
        }

        fun getPost(postKey: String, success: (Post?) -> Unit, fail: (Exception) -> Unit) {
            commonGET("getPost", docRef(EES_POST, postKey),
                    success = {
                        if (it == null) {
                            success(null)
                            MyProgress.cancel()
                            OnException.dbNoData("getPost : ")
                        } else success(it.toObject(Post::class.java))
                    }, fail = { fail(it) })
        }

        fun deletePost(post: Post, success: () -> Unit, fail: (Exception) -> Unit) {
            if (!OnPeer.isAdminSupervisor(true)) return
            commonDEL("deletePost", docRef(EES_POST, post.key),
                    success = {
                        success()
                        post.fotoList?.let { PhotoUtil.deletePhoto(it) }
                    }, fail = { fail(it) })
        }


        private fun queryPost(boardKey: String) =
                FirebaseFirestore.getInstance().collection(EES_POST)
                        .whereEqualTo(CH_BOARD_KEY, boardKey)
                        .whereGreaterThanOrEqualTo(CH_STATUS, Status.DELETED_USER.no)  // DELETED부터 모두 보여줘.
                        .orderBy(CH_STATUS_NO, Query.Direction.DESCENDING)
                        .orderBy(CH_ADD_DATE, Query.Direction.DESCENDING)

        fun firstQueryPost(boardKey: String, limit: Long, success: (QuerySnapshot) -> Unit, fail: () -> Unit) {
            val query = queryPost(boardKey).limit(limit)
            query.totalQueryGet({ success(it) }, { fail() })
        }

        fun secondQueryPost(boardKey: String, lastDoc: DocumentSnapshot, limit: Long, success: (QuerySnapshot) -> Unit, fail: () -> Unit) {
            val query = queryPost(boardKey).startAfter(lastDoc).limit(limit)
            query.totalQueryGet({ success(it) }, { fail() })
        }


        /******************** sttFlea *************************************************/
//        fun addFlea(flea:Flea, success: () -> Unit, fail: (Exception) -> Unit) {
//            val batch = batch().apply {
//                set(docRef(CH_FLEA, flea.key), flea)   // flea
//                OnPay.addBatch(this, Cost.modelAct(null, ActType.ADD_POST, null, post))    // cost addPost
//                post.reward?.let { OnPay.addBatch(this, Cost.modelAct(null, ActType.ADD_REWARD_POST, -post.reward!!, post)) }    // cost reward post
//            }
//            batch.myCommit("addPost",  success = { success() }, fail = { fail(it) })
////            commonADD("addPost", docRef(EES_POST, post.key), post, success = { success() }, fail = { fail(it) })
//        }


        /******************** sttRoom *************************************************/
        fun addRoom(room: Room, success: () -> Unit, fail: (Exception) -> Unit) {
            val batch = batch().apply {
                set(docRef(EES_ROOM, room.key), room)   // room
//                OnPay.addBatchAct(this, Cost.createAct(null, ActType.ADD_ROOM, null, room))    // cost ADD_ROOM
            }
            batch.myCommit("addRoom", success = { success() }, fail = { fail(it) })
        }

        fun updateRoom(batch: WriteBatch, success: () -> Unit, fail: (Exception) -> Unit) {
            batch.myCommit("updateRoom", success = { success() }, fail = { fail(it) })
        }

//        fun getRoom(roomKey: String, success: (Room?) -> Unit, fail: (Exception) -> Unit) {
//            commonGET("getRoom", docRef(CH_ROOM, roomKey),
//                    success = {
//                        if (it == null) {
//                            success(null)
//                            MyProgress.cancel()
//                            OnException.dbNoData("getRoom : ")
//                        } else success(it.toObject(Room::class.java))
//                    }, fail = { fail(it) })
//        }

        /******************** sttNotice for admin*************************************************/
        fun addNotice(notice: Notice, success: (Notice) -> Unit, fail: (Exception) -> Unit) {
            commonADD("addNotice", docRef(EES_NOTICE, notice.key), notice, success = { success(notice) }, fail = { fail(it) })
        }

        /******************** sttSystemManage*************************************************/
        fun addSystemMan(systemManage: SystemMan, success: () -> Unit, fail: (Exception) -> Unit) {
            commonADD("addSystemMan", docRef(EES_SYSTEM, SYSTEM_MAN_KEY), systemManage, success = { success() }, fail = { fail(it) })
        }

        fun getSystemMan(success: (SystemMan?) -> Unit, fail: (Exception) -> Unit) {
            commonGET("getSystemMan", docRef(EES_SYSTEM, SYSTEM_MAN_KEY), success = { success(it?.toObject(SystemMan::class.java)) }, fail = { fail(it) })
        }

        /******************** sttCount *************************************************/
        /**       공통 post, board, flea, room, store  :  UP,DOWN,BOOK,PUSH,ONLINE         */
        fun addCount(count: Count, success: (Count) -> Unit, fail: (Exception) -> Unit) {
            commonADD("addCount", docRef(count.topChildName, count.key), count, success = { success(count) }, fail = { fail(it) })
        }

        /*** 바뀐지 체크하고 DB 실행 -> success -> adapter 변경 */
        fun updateCount(count: Count, success: (Count) -> Unit, fail: (Exception) -> Unit) {
            batch().apply {
                update(docRef(count.topChildName, count.key), CH_PHOTO_THUMB, count.photoThumb)             // update count.thumb
                update(docRef(count.topChildName, count.key), CH_MODEL_TITLE, count.modelTitle)             // update count.modelTItle
            }.myCommit("updateQuick", success = { success(count) }, fail = { fail(it) })
        }


        fun getCount(topChildName: String, modelKey: String, success: (Count?) -> Unit, fail: () -> Unit) {
            colRef(topChildName)
                    .whereEqualTo(CH_MODEL_KEY, modelKey)
                    .whereEqualTo(CH_UID, OnPeer.myUid())
                    .orderBy(CH_ADD_DATE, Query.Direction.DESCENDING)
                    .totalQueryGet(success = {
                        if (it.isEmpty) success(null)
                        it.forEach { snap ->
                            success(snap.toObject(Count::class.java))
                        }
                    }, fail = { fail() })
        }

//        fun deleteCount(count: Count, success: (Count) -> Unit, fail: (Exception) -> Unit) {
//            commonDEL("deleteCount", docRef(count.topChildName, count.key), success = { success(count) }, fail = { fail(it) })
//        }

        fun deleteCount(count: Count, success: (Count) -> Unit, fail: (Exception) -> Unit) {
            batch().apply {
                delete(docRef(EES_QUICK, count.key))
                delete(docRef(count.topChildName, count.key))
                myCommit("deleteCount", success = { success(count) }, fail = { fail(it) })
            }
        }

        fun addQuick(count: Count, success: (Count) -> Unit, fail: (Exception) -> Unit) {
            batch().apply {
                count.order = rightNow()
                set(docRef(EES_QUICK, count.key), count)    // add quick
                update(docRef(count.topChildName, count.key), CH_QUICK, true)   // 북마크의 항목의 quick.boolean 수정.
                myCommit("addQuick", success = { success(count) }, fail = { fail(it) })
            }
        }

        /**         * 탈퇴시 : 모든 quickMenu 지우기         */
        fun delQuickAll(uid: String, success: () -> Unit, fail: (Exception) -> Unit) {
            ESget.getCountList("delQuickAll", EES_QUICK, ESquery.countUID(100, 0, uid),
                    success = { list ->
                        if (!list.isNullOrEmpty())
                            batch().apply {
                                list.forEach { count -> delete(docRef(EES_QUICK, count.key)) }    // quick 삭제
                            }.myCommit("delQuickAll", success = { success() }, fail = { fail(it) })
                    }, fail = {})
        }

        /*** 바뀐지 체크하고 DB 실행 -> success -> adapter 변경 */
        fun updateQuick(count: Count, success: (Count) -> Unit, fail: (Exception) -> Unit) {
            batch().apply {
                update(docRef(EES_QUICK, count.key), CH_PHOTO_THUMB, count.photoThumb)   // update quick.thumb
                update(docRef(count.topChildName, count.key), CH_PHOTO_THUMB, count.photoThumb)             // update count.thumb
                update(docRef(EES_QUICK, count.key), CH_MODEL_TITLE, count.modelTitle)   // update quick.modelTItle
                update(docRef(count.topChildName, count.key), CH_MODEL_TITLE, count.modelTitle)             // update count.modelTItle
//                    any.anyThumb(0).let {
//                        if (count.photoThumb != it) {
//                            count.photoThumb = it
//                            update(docRef(EES_PEER, myuid(), EES_QUICK, count.key), CH_PHOTO_THUMB, it)   // update quick.thumb
//                            update(docRef(count.topChildName, count.key), CH_PHOTO_THUMB, it)             // update count.thumb
//                        }
//                    }
//                    any.anyTitle().let {
//                        if (count.modelTitle != it) {
//                            count.modelTitle = it
//                            update(docRef(EES_PEER, myuid(), EES_QUICK, count.key), CH_MODEL_TITLE, it)   // update quick.modelTItle
//                            update(docRef(count.topChildName, count.key), CH_MODEL_TITLE, it)             // update count.modelTItle
//                        }
//                    }
            }.myCommit("updateQuick", success = { success(count) }, fail = { fail(it) })
        }

        /**
         * 순서 변경.
         */
        fun modifyQuick(quickList: ArrayList<Count>, success: () -> Unit, fail: ( ) -> Unit) {
            batch().apply {
                quickList.forEachIndexed { i, count -> update(docRef(EES_QUICK, count.key), CH_ORDER, i) }
                myCommit("modifyQuick", success = { hhh(3000){success()} }, fail = {fail()})
            }
        }

        fun deleteQuick(count: Count, success: (Count) -> Unit, fail: (Exception) -> Unit) {
            batch().apply {
                delete(docRef(EES_QUICK, count.key))   // quick 삭제
                update(docRef(count.topChildName, count.key), CH_QUICK, false)   // count.quick 수정.
                myCommit("delQuick", success = { success(count) }, fail = { fail(it) })
            }
        }

        /**         * ViewUID는 key = uid         */
        fun addViewUID(child: String, count: Count, success: () -> Unit, fail: (Exception) -> Unit) {
            commonADD("addViewUID $child", docRef(child, count.modelKey, CH_VIEW_UID_COUNT, count.uid), count, success = { success() }, fail = { fail(it) })
        }


        /******************** sttWarn *************************************************/
        fun addWarn(warn: Warn, success: () -> Unit, fail: (Exception) -> Unit) {
            commonADD("addWarn", docRef(EES_WARN, warn.key), warn, success = { success() }, fail = { fail(it) })
        }

        fun deepDelWarn(warnKey: String, success: () -> Unit, fail: () -> Unit) {
            commonDEL("deepDelWarn", docRef(EES_WARN, warnKey), success = { success() }, fail = { fail() })
        }


        fun getWarn(key: String, success: (Warn?) -> Unit, fail: (Exception) -> Unit) {
            commonGET("getWarn", docRef(EES_WARN, key), success = { success(it?.toObject(Warn::class.java)) }, fail = { fail(it) })
        }

        fun addPenalty(warn: Warn, success: () -> Unit, fail: (Exception) -> Unit) {
            commonADD("addPenalty", docRef(EES_PENALTY, warn.key), warn, success = { success() }, fail = { fail(it) })
        }

        /** 적용가능 post, board, reply board        */
        fun getWarnModelKey(modelKey: String, success: (Warn?) -> Unit, fail: () -> Unit) {
            colRef(EES_WARN)
                    .whereEqualTo(CH_MODEL_KEY, modelKey)
                    .whereEqualTo(CH_AUTHOR_UID, OnPeer.myUid())
                    .orderBy(CH_ADD_DATE, Query.Direction.DESCENDING)
                    .totalQueryGet(success = {
                        if (it.isEmpty) success(null)
                        it.forEach { snap -> success(snap.toObject(Warn::class.java)) }
                    }, fail = { fail() })
        }

        /******************** sttMyTag *************************************************/
        fun addMyTag(myTag: MyTag, success: (MyTag) -> Unit, fail: (Exception) -> Unit) {
            commonADD("addTag", docRef(EES_TAG, myTag.key), myTag, success = { success(myTag) }, fail = { fail(it) })
        }

        fun deleteMyTag(myTag: MyTag, success: () -> Unit, fail: (Exception) -> Unit) {
            commonDEL("deleteMyTag", docRef(EES_TAG, myTag.key), success = { success() }, fail = { fail(it) })
        }

        /******************** sttReply  모든 model 적용. ************************************************/
        fun addReply(reply: Reply, success: () -> Unit, fail: (Exception) -> Unit) {
            commonADD("addReplyFreePost", docRef(reply.dataTypeNo.dataType().child, reply.modelKey, EES_REPLY, reply.key), reply, success = { success() }, fail = { fail(it) })
        }

        fun updateReplyFreePost(batch: WriteBatch, success: () -> Unit, fail: (Exception) -> Unit) {
            batch.myCommit("updateReplyFreePost", success = { success() }, fail = { fail(it) })
        }

        /******************** sttLobby  ************************************************/
        fun addLobby(lobby: Lobby, success: () -> Unit, fail: (Exception) -> Unit) {
            commonADD("addLobby", docRef(EES_LOBBY, lobby.key), lobby, success = { success() }, fail = { fail(it) })
        }

        /******************** sttChat  ************************************************/
        fun addChat(chat: Chat, success: () -> Unit, fail: (Exception) -> Unit) {
            commonADD("addChat", docRef(EES_LOBBY, chat.lobbyKey, EES_CHAT, chat.key), chat, success = { success() }, fail = { fail(it) })
        }

        /******************** sttTicket  *****************************************1*******/
        fun addTicket(ticket: Ticket, success: () -> Unit, fail: (Exception) -> Unit) {
            commonADD("addTicket", docRef(EES_TICKET, ticket.key), ticket, success = { success() }, fail = { fail(it) })
        }

        fun delTicket(ticket: Ticket, success: () -> Unit, fail: (Exception) -> Unit) {
            commonDEL("delTicket", docRef(EES_TICKET, ticket.key), success = { success() }, fail = { fail(it) })
        }

        /******************** sttGrant  *****************************************1*******/
        fun addGrant(grant: Grant, success: (Grant) -> Unit, fail: (Exception) -> Unit) {
            commonADD("addGrant", docRef(EES_GRANT, grant.key), grant, success = { success(grant) }, fail = { fail(it) })
        }

        fun deepDelGrant(grantKey: String, success: () -> Unit, fail: () -> Unit) {
            commonDEL("deepDelGrant", docRef(EES_GRANT, grantKey), success = { success() }, fail = { fail() })
        }

        /******************** sttStore <admin only> *************************************************/
        fun addStore(store: Store, success: () -> Unit, fail: (Exception) -> Unit) {
            commonADD("addStore", docRef(EES_STORE, store.key), store, success = { success() }, fail = { fail(it) })
        }

//        fun getStore(key: String, success: (Store?) -> Unit, fail: (Exception) -> Unit) {
//            commonGET("getStore", docRef(EES_STORE, key), success = { if (it == null) success(null) else success(it.toObject(Store::class.java)) }, fail = { fail(it) })
//        }

//        fun deleteStore(store: Store, success: (() -> Unit)?, fail: (() -> Unit)?) {
//            if (!OnPeer.isAdminSupervisor(true)) return
////            commonDEL("deleteStore", docRef(EES_STORE, store.key),
////                    success = {
////                        success()
////                        PhotoUtil.deletePhoto(*store.fotoList.toTypedArray())        // todo : functions에서 구현할 것.
////                        deleteAllStoreMenu(store, {}, {})
////                    }, fail = { fail(it) })
//        }

        /******************** sttMenu *************************************************/
        fun addStoreMenu(storeMenu: StoreMenu, success: () -> Unit, fail: (Exception) -> Unit) {
            commonADD("addStoreMenu", docRef(EES_STORE_MENU, storeMenu.key), storeMenu, success = { success() }, fail = { fail(it) })
        }

//        fun getStoreMenu(storeMenu: StoreMenu, success: (StoreMenu?) -> Unit, fail: (Exception) -> Unit) {
//            commonGET("getStoreMenu", docRef(CH_STORE, storeMenu.storeKey, CH_STORE_MENU, storeMenu.key), success = { if (it == null) success(null) else success(it.toObject(StoreMenu::class.java)) }, fail = { fail(it) })
//        }

        fun deleteStoreMenu(storeMenu: StoreMenu, success: () -> Unit, fail: (Exception) -> Unit) {
//            if (!OnPeer.isAdminSupervisor(true)) return
            commonDEL("deleteStoreMenu", docRef(EES_STORE, storeMenu.storeKey, EES_STORE_MENU, storeMenu.key),
                    success = {
                        success()
                        PhotoUtil.deletePhoto(storeMenu.photoThumb, storeMenu.photoUrl)
                    }, fail = { fail(it) })
        }

//        fun deleteAllStoreMenu(store: Store, success: () -> Unit, fail: (Exception) -> Unit) {
//            commonGetCOL("deleteAllStoreMenu", arrayOf(EES_STORE, store.key, EES_STORE_MENU),
//                    success = {
//                        it?.let {
//                            for (snap in it) {
//                                val storeMenu = snap.toObject(StoreMenu::class.java)
//                                deleteStoreMenu(storeMenu, {}, {})
//                            }
//                        }
//                    }, fail = { fail(it) })
//        }


        /******************** sttBid *************************************************/
        fun addBid(bid: Bid, success: () -> Unit, fail: (Exception) -> Unit) {
            commonADD("addBid", docRef(EES_BID, bid.key), bid, success = { success() }, fail = { fail(it) })
        }

        /******************** sttBanner *************************************************/
        fun addBanner(banner: Bid, success: () -> Unit, fail: (Exception) -> Unit) {
            commonADD("addBanner", docRef(EES_BANNER, banner.key), banner, success = { success() }, fail = { fail(it) })
        }

        fun deleteBanner(banner: Bid, success: () -> Unit, fail: (Exception) -> Unit) {
            batch().apply {
                update(docRef(EES_BANNER, banner.key), CH_STATUS_NO, Status.DELETED_USER.no)
                update(docRef(EES_BANNER, banner.key), CH_DEL_DATE, rightNow())
                myCommit("deleteBanner", { success() }, { fail(it) })
            }
//            commonDEL("deleteBanner", docRef(CH_PEER, bid.uid, CH_BANNER, bid.key), success = {
//                PhotoUtil.deletePhoto(bid.photoUrl, bid.photoThumb)
//                success()
//            }, fail = { fail(it) })
        }

        fun deepDelBanner(context: Context, banner: Bid, success: () -> Unit, fail: () -> Unit) {
            OnDDD.deepDel(context,
                    ok = {
                        //todo: 사진 사용중인지 체크하고 삭제. & banner 삭제.
                        commonDEL("deepDelBanner", docRef(EES_BANNER, banner.key), success = { success() }, fail = { fail() })
                    }, cancel = {})
        }

        /**
         * storeRating thumbList  수정하기.
         * UP and DOWN 모두 적용가능.
         * 호스트의 답변.
         */
//        fun updateStoreRating(storeRating: StoreRating
//                              , success: () -> Unit
//                              , fail: () -> Unit) {
//            if (OnPeer.notAllowedDB()) return
//
//            FirebaseFirestore.getInstance().collection(EES_STORE).document(storeRating.storeKey).collection(EES_RATING_UID).document(storeRating.uid)
//                    .set(storeRating)
//                    .addOnSuccessListener {
//                        Log.d(TAG, "updateStoreRating written!")
//                        success()
//                    }.addOnFailureListener {
//                        MyProgress.cancel()
//                        OnException.dbfailed("updateStoreRating", it)
//                        fail()
//                    }
//        }

        /******************** sttStoreRating *************************************************/
//        fun addStoreRating(storeRating: StoreRating, success: () -> Unit, fail: (Exception) -> Unit) {
//            commonADD("addStoreRating", docRef(EES_STORE, storeRating.storeKey, EES_RATING_UID, OnPeer.myUid()), storeRating, success = { success() }, fail = { fail(it) })
//        }

//        fun getMyStoreRating(store: Store, success: (StoreRating?) -> Unit, fail: (Exception) -> Unit) {
//            commonGET("getMyStoreRating", docRef(EES_STORE, store.key, EES_RATING_UID, OnPeer.myUid()), success = { if (it == null) success(null) else success(it.toObject(StoreRating::class.java)) }, fail = { fail(it) })
//        }

        /******************** sttRating *************************************************/
        /**         *  Rating 올리기  <functions>에서만 올릴 것.         */
//        fun addRating(store: Store, rating: Rating, success: () -> Unit, fail: (Exception) -> Unit) {
//            commonADD("addRating", docRef(EES_RATING, store.key), rating, success = { success() }, fail = { fail(it) })
//        }

//        fun getRating(store: Store, success: (Rating?) -> Unit, fail: (Exception) -> Unit) {
//            commonGET("getRating", docRef(EES_RATING, store.key), success = { if (it == null) success(null) else success(it.toObject(Rating::class.java)) }, fail = { fail(it) })
//        }

    }
}