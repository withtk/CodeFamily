package com.tkip.mycode.util.lib.photo.album


import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.RecyclerView
import com.tkip.mycode.databinding.HolderAlbumSelectedBinding
import com.tkip.mycode.util.lib.photo.MyPhoto


class AlbumSelectedAdapter(var items: ArrayList<MyPhoto>, var onCancelSelected: (MyPhoto) -> Unit) : RecyclerView.Adapter<AlbumSelectedAdapter.AlbumSelectedViewHolder>() {

    private lateinit var activity: AppCompatActivity


    override fun getItemId(position: Int): Long {
        return items[position].photoId.hashCode().toLong()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): AlbumSelectedViewHolder {
        this.activity = parent.context as AppCompatActivity
        val binding = HolderAlbumSelectedBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return AlbumSelectedViewHolder(binding)
    }

    override fun onBindViewHolder(h: AlbumSelectedViewHolder, p: Int) {
        h.bind(items[p])
    }

    override fun getItemCount(): Int {
        return items.size
    }


    /**
     *
     */
    inner class AlbumSelectedViewHolder(val b:  HolderAlbumSelectedBinding) : RecyclerView.ViewHolder(b.root) {

        lateinit var myPhoto: MyPhoto

        fun bind(myPhoto: MyPhoto) {
            this.myPhoto = myPhoto
            b.holder = this
            b.myPhoto = myPhoto
            b.executePendingBindings()
        }

        fun onClickCard() {
//            onCancelSelected(myPhoto)
        }

        fun onClickCancel() {
            myPhoto.isSel = false
            onCancelSelected(myPhoto)
        }


    }
}