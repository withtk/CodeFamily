package com.tkip.mycode.util.lib.photo.view

import android.content.Context
import android.content.Intent
import android.view.LayoutInflater
import android.widget.LinearLayout
import androidx.databinding.DataBindingUtil
import com.tkip.mycode.R
import com.tkip.mycode.admin.versionGteQ
import com.tkip.mycode.databinding.LinearAddPhotoBoxBinding
import com.tkip.mycode.dialog.alert.MyAlert
import com.tkip.mycode.funs.common.OnVisible
import com.tkip.mycode.funs.common.lllogI
import com.tkip.mycode.init.REQ_ALBUM
import com.tkip.mycode.init.REQ_PICK
import com.tkip.mycode.init.REQ_SAF
import com.tkip.mycode.init.base.Cons
import com.tkip.mycode.init.inter.interAddPhotoBox
import com.tkip.mycode.nav.store.add.SelectedFotoAdapter
import com.tkip.mycode.util.lib.photo.Foto
import com.tkip.mycode.util.lib.photo.MyPhoto
import com.tkip.mycode.util.lib.photo.initSelectedFotoList
import com.tkip.mycode.util.lib.photo.resetFromAlbum
import com.tkip.mycode.util.lib.photo.util.createMyThumb
import com.tkip.mycode.util.lib.photo.util.getThumb4Q
import com.tkip.mycode.util.tools.anim.showSlideUp
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

/**
 * photo 올리는 박스
 */
class LinearAddPhotoBox : LinearLayout {
    lateinit var b: LinearAddPhotoBoxBinding

    lateinit var selectedFotoAdapter: SelectedFotoAdapter
    var selectedFotoList = arrayListOf<Foto>()

    constructor(context: Context) : super(context)
    constructor(context: Context, onOpenAlbum: () -> Unit) : super(context) {
        b = DataBindingUtil.inflate(LayoutInflater.from(context), R.layout.linear_add_photo_box, this, true)

        b.inter = interAddPhotoBox(onClickOpenAlbum = { onOpenAlbum() })


        /**    rv  초기화    */
//        selectedFotoList.clear()
        selectedFotoAdapter = SelectedFotoAdapter(selectedFotoList,
                onLong = { MyAlert.showFotoComment(context, selectedFotoList, it, onCancel = { selectedFotoAdapter.notifyDataSetChanged() }) })

        selectedFotoAdapter.setHasStableIds(true)
        b.rv.adapter = selectedFotoAdapter


        /*   b.radioMid.setOnCheckedChangeListener { _, isChecked ->
               if (isChecked) {
                   Log.d(Cons.TAG, "radioMid")
                   width = WIDTH_MID
               }
           }

           b.radioOrigin.setOnCheckedChangeListener { _, isChecked ->
               if (isChecked) {
                   //  dialog로  포인트 계산해서 가능여부 알려줄 것.
                   //                width = Cons.WIDTH_ORIGINAL
                   TToast.showAlert(R.string.info_prepare_developing)
                   b.radioMid.isChecked = true
               }
           }

           b.radioMid.isChecked = true*/


    }


    /**      기존 사진 셋팅하기      */
    fun setPhotos(fotoList: ArrayList<Foto>?) {
        fotoList?.let {
            selectedFotoList.initSelectedFotoList(it)
            selectedFotoAdapter.notifyDataSetChanged()
        }
        if (selectedFotoList.isNotEmpty()) OnVisible.showHandler(b.tvInfo)
    }

    /**
     * activityForResult에서 받아온 뒤 실행.
     */
    fun setFotosFromUri(requestCode: Int, intent: Intent?) {
        when (requestCode) {
            REQ_SAF ->
                intent?.data?.let { uri ->
                    lllogI("LinearAddPhotoBox setFotosFromUri REQ_SAF: $requestCode uri: $uri ")
                    val thumb = uri.createMyThumb()
                    val f = Foto(uri, thumb)
                    lllogI("LinearAddPhotoBox setFotosFromUri REQ_SAF:  f: $f ")
                    lllogI("LinearAddPhotoBox setFotosFromUri REQ_SAF:  thumb: $thumb ")
                    selectedFotoList.add(f)
                    selectedFotoAdapter.notifyDataSetChanged()
                    b.tvInfo?.showSlideUp(false)
                }

            REQ_PICK ->
                intent?.data?.let { uri ->
                    lllogI("LinearAddPhotoBox setFotosFromUri REQ_PICK: $requestCode uri: $uri ")
                    val thumb = uri.createMyThumb()
                    selectedFotoList.add(Foto(uri, thumb))
                    selectedFotoAdapter.notifyDataSetChanged()
                    if (selectedFotoList.isNotEmpty()) b.tvInfo?.showSlideUp(false)
                }

            REQ_ALBUM -> {

                intent?.getParcelableArrayListExtra<MyPhoto>(Cons.PUT_ARRAY_MYPHOTO)?.let { albumList ->
                    lllogI("LinearAddPhotoBox setFotosFromUri REQ_ALBUM: $requestCode albumList: ${albumList.size} ")

                    selectedFotoList.resetFromAlbum(albumList)  // selectedFotoList에 추가 및 정리.

                    if (versionGteQ) {   // Q일때는 bitmap 만들기.
                        /**     * thumb 만들기     */
                        selectedFotoList.forEach { foto ->
                            // bitmap null 일때만 async 실행.
                            if (foto.thumbUrl == null) {
                                if (versionGteQ) {
                                    GlobalScope.launch {
                                        foto.photoId?.let {
                                            val thumb = getThumb4Q(it)
                                            foto.thumbBitmap = thumb
                                            withContext(Dispatchers.Main) { selectedFotoAdapter.notifyDataSetChanged() }
                                        }
                                    }
//                                    AsyncThumbQ(context,
//                                            success = { thumb ->
//                                                foto.thumbBitmap = thumb
//                                                selectedFotoAdapter.notifyDataSetChanged()
//                                            }).execute(foto.photoId)
                                }
                            }
                        }

                    } else selectedFotoAdapter.notifyDataSetChanged()

                    if (selectedFotoList.isNotEmpty()) b.tvInfo?.showSlideUp(false)  // 안내 메세지 띄우기
                }
            }
        }

    }

}