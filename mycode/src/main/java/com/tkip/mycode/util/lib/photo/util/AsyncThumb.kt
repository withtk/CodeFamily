package com.tkip.mycode.util.lib.photo.util

import android.app.Activity
import android.net.Uri
import android.os.AsyncTask
import android.provider.MediaStore
import com.tkip.mycode.funs.common.lllogD
import com.tkip.mycode.util.lib.photo.MyPhoto
import java.lang.ref.WeakReference


/**
 * 미디어의 thumb을 받아오거나 새로 만드는 async
 */
class AsyncThumb(activity: Activity, var success: (Uri?) -> Unit) : AsyncTask<MyPhoto, Int, Uri>() {

    private var wActivity: WeakReference<Activity> = WeakReference(activity)

    override fun doInBackground(vararg myPhotos: MyPhoto?): Uri? {

        /**
         * 이건 하나씩 할 수밖에 없다.
         * 결과값을 저장할 곳이 없어.
         * 그래서 하나씩 밖으로 보내줘야 돼
         */

        val myPhoto = myPhotos[0]!!

        return   getThumbUri(myPhoto.photoId!!, 1)

    }

    override fun onPostExecute(result: Uri?) {
        super.onPostExecute(result)
        success(result)
    }

    private fun getThumbUri(imageId: String, count: Int): Uri? {

        var tryCount = count

        val projection = arrayOf(MediaStore.Images.Thumbnails.DATA)
        val cursor = wActivity.get()?.contentResolver?.query(
                MediaStore.Images.Thumbnails.EXTERNAL_CONTENT_URI, // 썸네일 컨텐트 테이블
                projection, // DATA를 출력
                MediaStore.Images.Thumbnails.IMAGE_ID + "=?", // IMAGE_ID는 원본 이미지의 _ID를 나타냅니다.
                arrayOf(imageId),
                null)


        if (cursor != null && cursor.moveToFirst()) {

            val thumbnailColumnIndex = cursor.getColumnIndex(projection[0])
            val thumbnailPath = cursor.getString(thumbnailColumnIndex)
            cursor.close()

            return Uri.parse(thumbnailPath)

        } else {

            // thumbnailCursor가 비었습니다.
            lllogD("getThumbUri thumbnailCursor가 비었습니다.")

            // 썸네일이 존재하지 않을 때에는 아래와 같이 썸네일을 생성하도록 요청합니다
            if (tryCount > 2) {
                return null // 요청은 두번만 하고 그래도 썸네일 안 만들어지면 그냥 null
            }
            tryCount++


            MediaStore.Images.Thumbnails.getThumbnail(
                    wActivity.get()?.contentResolver,
                    java.lang.Long.parseLong(imageId),
                    MediaStore.Images.Thumbnails.MINI_KIND,
                    null)

            cursor!!.close()

            return getThumbUri(imageId, tryCount)

        }
    }

}

/*
class AlbumThumbAsync private constructor(activity: AlbumActivity, private val b: ViewHolderAlbumBinding) : AsyncTask<MyPhoto, Int, Uri>() {

    private val mActivity: WeakReference<AlbumActivity>
    private var foto: MyPhoto? = null

    init {
        mActivity = WeakReference(activity)
    }


    override fun doInBackground(vararg fotoList: MyPhoto): Uri? {
        this.foto = fotoList[0]
        return getThumbUri(foto!!.photoId, 1)
    }

    override fun onPostExecute(uri: Uri) {
        if (AlbumActivity.currentFolder.equals(foto!!.folder)) {
            foto!!.thumbUri = uri
            b.setFoto(foto)
        } else {
            cancel(true)
        }
        super.onPostExecute(uri)
    }

    private fun getThumbUri(imageId: String, count: Int): Uri? {
        var count = count
        val projection = arrayOf(MediaStore.Images.Thumbnails.DATA)

        val cursor = mActivity.get().getContentResolver().query(
                MediaStore.Images.Thumbnails.EXTERNAL_CONTENT_URI, // 썸네일 컨텐트 테이블
                projection, // DATA를 출력
                MediaStore.Images.Thumbnails.IMAGE_ID + "=?", // IMAGE_ID는 원본 이미지의 _ID를 나타냅니다.
                arrayOf(imageId),
                null)


        if (cursor != null && cursor.moveToFirst()) {
            val thumbnailColumnIndex = cursor.getColumnIndex(projection[0])
            val thumbnailPath = cursor.getString(thumbnailColumnIndex)
            cursor.close()

            return Uri.parse(thumbnailPath)
        } else {
            // thumbnailCursor가 비었습니다.
            // 썸네일이 존재하지 않을 때에는 아래와 같이 썸네일을 생성하도록 요청합니다
            if (count > 2) {
                return null // 요청은 두번만 하고 그래도 썸네일 안 만들어지면 그냥 null
            }
            count++

            MediaStore.Images.Thumbnails.getThumbnail(mActivity.get().getContentResolver(),
                    java.lang.Long.parseLong(imageId),
                    MediaStore.Images.Thumbnails.MINI_KIND, null)
            cursor!!.close()
            return getThumbUri(imageId, count)
        }
    }
}
    */
