package com.tkip.mycode.util.lib.map

import android.app.Activity
import android.content.Context
import android.graphics.Bitmap
import android.graphics.Canvas
import android.location.Address
import android.location.Geocoder
import android.location.Location
import android.util.DisplayMetrics
import android.util.Log
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.TextView
import com.google.android.gms.location.FusedLocationProviderClient
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.model.*
import com.google.android.libraries.places.api.model.Place
import com.tkip.mycode.R
import com.tkip.mycode.dialog.toast.TToast
import com.tkip.mycode.funs.common.lllogD
import com.tkip.mycode.funs.common.lllogI
import com.tkip.mycode.init.base.Cons
import com.tkip.mycode.init.part.CodeApp
import com.tkip.mycode.util.tools.anim.visi
import java.io.IOException
import java.util.*


/*** map */
var curMarker: Marker? = null

const val PUT_MAP_ACT = "PUT_MAP_ACT"
const val PUT_MAP_LATLNG = "PUT_LATLNG"  // 해당 위치로 맵 바로이동.
const val PUT_MAP_ADDRESS = "PUT_MAP_ADDRESS"
const val PUT_MAP_POSTAL_CODE = "PUT_MAP_POSTAL_CODE"

const val PUT_TYPE_STORE_LATLNG = 50
const val MAP_ACT_NORMAL = 10
const val MAP_ACT_ADD = 20
const val MAP_ACT_UPDATE = 30


const val MAP_COUNTRY = "ph"
const val ZOOM_DEFAULT = 12f
const val ZOOM_POI = 16f
const val ZOOM_LATLNG = 14f
const val ZOOM_STOP = 14f
//val LATLNG_SEOUL = LatLng(37.56, 126.97)
val LATLNG_MANILA_AIRPORT = LatLng(14.5122739, 121.016508)
val MAP_BOUNDARY = LatLngBounds(LatLng(6.125573, 116.630433), LatLng(18.832216, 126.569999))   // phil boundary
var MAP_CURRENT_LOCATION: Location? = null


/****************************************************
 *************** 마커옵션 만들기     ******************
 ***************************************************/
fun LatLng.getMarkerOpt(title: String?, snippets: String?): MarkerOptions {

    return MarkerOptions().apply {
        position(this@getMarkerOpt)
        title(title)
        snippet(snippets)
        alpha(0.5f)
        draggable(true)
        icon(null)
//                icon(BitmapDescriptorFactory.fromBitmap(R.drawable.ic_whatshot))
//                icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_RED))
    }

}


/**
 * 마커옵션 만들기
 */
fun View.getMarkerOpt(latlng: LatLng, title: String?, snippets: String?): MarkerOptions {

    val tvTitle = findViewById<TextView>(R.id.tv_title)
    tvTitle.text = title
    val tvSnipet = findViewById<TextView>(R.id.tv_snipet)
    tvSnipet.text = snippets

    return MarkerOptions().apply {
        position(latlng)
        title(title)
        snippet(snippets)
        alpha(0.5f)
        draggable(true)
        icon(BitmapDescriptorFactory.fromBitmap(this@getMarkerOpt.createBitmap()))

//        icon(null)
//        icon(BitmapDescriptorFactory.fromBitmap(R.drawable.ic_whatshot))
//                icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_RED))
    }

}


/**
 * bitmap으로
 */
fun View.createBitmap(): Bitmap {

    val displayMetrics = DisplayMetrics()
    (context as Activity).windowManager.defaultDisplay.getMetrics(displayMetrics)

    layoutParams = ViewGroup.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT)
    measure(displayMetrics.widthPixels, displayMetrics.heightPixels)
    layout(0, 0, displayMetrics.widthPixels, displayMetrics.heightPixels)
    val bitmap = Bitmap.createBitmap(measuredWidth, measuredHeight, Bitmap.Config.ARGB_8888)
    draw(Canvas(bitmap))

    return bitmap
}


/*************************************************************************
 *************************** Googlemap *************************************
 **************************************************************************/
/**
 * 지도이동
 */
fun GoogleMap.moveMap(latlng: LatLng, zoom: Float?, animation: Boolean) {
    val cameraUpdate = CameraUpdateFactory.newLatLngZoom(latlng, zoom ?: ZOOM_DEFAULT)
    if (animation) animateCamera(cameraUpdate)
    else moveCamera(cameraUpdate)
}


/**
 * Device의 마지막 위치 받아오기.
 */
fun FusedLocationProviderClient.getDeviceLocation(activity: Activity, success: (Location) -> Unit) {
    try {
        this.lastLocation?.addOnSuccessListener(activity) { deviceLoc ->
            if (deviceLoc != null) {
                MAP_CURRENT_LOCATION = deviceLoc
                success(deviceLoc)
//                        moveMap(map, LatLng(deviceLoc.latitude, deviceLoc.longitude), zoomDefault,false)
            }
        }
    } catch (e: SecurityException) {
        lllogD(e.message)
    }
}


/** stop map에 표시   addRoomActi, roomActi*/
fun GoogleMap.showMap(latLng: LatLng?, address: String?, lineMap: LinearLayout?) {
    latLng?.let {
        this.moveMap(it, ZOOM_STOP, false)
        curMarker?.remove()
        curMarker = this.addMarker(OnMap.getMarkerOpt(it, address, null))
//        lineMap?.showSlideLeft(false)
        lineMap?.visi()
        lllogI(" LinearMapAdd GoogleMap showMap  lineMap visi: ${lineMap?.visibility}")
    }
}


/**
 *  거리받아오기
 */
private fun LatLng.getDistance(loc: LatLng): Float {
    val results = FloatArray(1)
    Location.distanceBetween(latitude, longitude, loc.latitude, loc.longitude, results)
    return results[0]
}

fun LatLng.getDistanceFromCurrentLoc(): String {
    if (MAP_CURRENT_LOCATION == null) {
        return ""
    } else {
        val currentLatlng = LatLng(MAP_CURRENT_LOCATION?.latitude!!, MAP_CURRENT_LOCATION?.longitude!!)
        val float = getDistance(currentLatlng)
        return String.format("%.1f", float) + "km"
    }
}


/**
 * 주소:값 받아오기 geocoder
 */
fun LatLng.getGeocoder(context: Context, result: (Address) -> Unit) {

    val geocoder = Geocoder(context, Locale.getDefault())
    val addresses: List<Address>?

    try {
        addresses = geocoder.getFromLocation(
                this.latitude,
                this.longitude,
                1)

        if (addresses == null || addresses.isEmpty()) {
            val strADDRESS = context.getString(com.tkip.mycode.R.string.error_address)
            TToast.showWarning(strADDRESS)
            Log.d(Cons.TAG, strADDRESS)

        } else {
            val address = addresses[0]

            Log.d(Cons.TAG, "============== start address ")
            Log.d(Cons.TAG, "maxAddressLineIndex : " + address.maxAddressLineIndex.toString())
            Log.d(Cons.TAG, "getAddressLine 0 : " + address.getAddressLine(0).toString())
            Log.d(Cons.TAG, "featureName : " + address.featureName)
            Log.d(Cons.TAG, "latitude : " + address.latitude)
            Log.d(Cons.TAG, "longitude : " + address.longitude)
            Log.d(Cons.TAG, "adminArea : " + address.adminArea)
            Log.d(Cons.TAG, "countryCode : " + address.countryCode)
            Log.d(Cons.TAG, "countryName : " + address.countryName)
            Log.d(Cons.TAG, "extras : " + address.extras)
            Log.d(Cons.TAG, "locale : " + address.locale)
            Log.d(Cons.TAG, "locality : " + address.locality)
            Log.d(Cons.TAG, "phone : " + address.phone)
            Log.d(Cons.TAG, "postalCode : " + address.postalCode)
            Log.d(Cons.TAG, "premises : " + address.premises)
            Log.d(Cons.TAG, "subAdminArea : " + address.subAdminArea)
            Log.d(Cons.TAG, "subLocality : " + address.subLocality)
            Log.d(Cons.TAG, "subThoroughfare : " + address.subThoroughfare)
            Log.d(Cons.TAG, "thoroughfare : " + address.thoroughfare)
            Log.d(Cons.TAG, "url : " + address.url)
            Log.d(Cons.TAG, "==============end address")

            result(address)
        }

    } catch (ioException: IOException) {
        val strNet = context.getString(com.tkip.mycode.R.string.error_network_map)
        TToast.showWarning(strNet)
        Log.d(Cons.TAG, strNet)

    } catch (illegalArgumentException: IllegalArgumentException) {
        val strGPS = context.getString(com.tkip.mycode.R.string.error_gps_map)
        TToast.showWarning(strGPS)
        Log.d(Cons.TAG, strGPS)
    }

}


/***********************************************************************************
 *****************************  Places  ********************************************
 ***********************************************************************************/

fun String.getTypeStr(): String? {
    val strId: Int? = when (this) {
        Place.Type.OTHER.name -> R.string.place_type_other
        Place.Type.ACCOUNTING.name -> R.string.place_type_accounting
        Place.Type.ADMINISTRATIVE_AREA_LEVEL_1.name -> R.string.place_type_administrative_area_level_1
        Place.Type.ADMINISTRATIVE_AREA_LEVEL_2.name -> R.string.place_type_administrative_area_level_2
        Place.Type.ADMINISTRATIVE_AREA_LEVEL_3.name -> R.string.place_type_administrative_area_level_3
        Place.Type.ADMINISTRATIVE_AREA_LEVEL_4.name -> R.string.place_type_administrative_area_level_4
        Place.Type.ADMINISTRATIVE_AREA_LEVEL_5.name -> R.string.place_type_administrative_area_level_5
        Place.Type.AIRPORT.name -> R.string.place_type_airport
        Place.Type.AMUSEMENT_PARK.name -> R.string.place_type_amusement_park
        Place.Type.AQUARIUM.name -> R.string.place_type_aquarium
        Place.Type.ART_GALLERY.name -> R.string.place_type_art_gallery
        Place.Type.ATM.name -> R.string.place_type_atm
        Place.Type.BAKERY.name -> R.string.place_type_bakery
        Place.Type.BANK.name -> R.string.place_type_bank
        Place.Type.BAR.name -> R.string.place_type_bar
        Place.Type.BEAUTY_SALON.name -> R.string.place_type_beauty_salon
        Place.Type.BICYCLE_STORE.name -> R.string.place_type_bicycle_store
        Place.Type.BOOK_STORE.name -> R.string.place_type_book_store
        Place.Type.BOWLING_ALLEY.name -> R.string.place_type_bowling_alley
        Place.Type.BUS_STATION.name -> R.string.place_type_bus_station
        Place.Type.CAFE.name -> R.string.place_type_cafe
        Place.Type.CAMPGROUND.name -> R.string.place_type_campground
        Place.Type.CAR_DEALER.name -> R.string.place_type_car_dealer
        Place.Type.CAR_RENTAL.name -> R.string.place_type_car_rental
        Place.Type.CAR_REPAIR.name -> R.string.place_type_car_repair
        Place.Type.CAR_WASH.name -> R.string.place_type_car_wash
        Place.Type.CASINO.name -> R.string.place_type_casino
        Place.Type.CEMETERY.name -> R.string.place_type_cemetery
        Place.Type.CHURCH.name -> R.string.place_type_church
        Place.Type.CITY_HALL.name -> R.string.place_type_city_hall
        Place.Type.CLOTHING_STORE.name -> R.string.place_type_clothing_store
        Place.Type.COLLOQUIAL_AREA.name -> R.string.place_type_colloquial_area
        Place.Type.CONVENIENCE_STORE.name -> R.string.place_type_convenience_store
        Place.Type.COUNTRY.name -> R.string.place_type_country
        Place.Type.COURTHOUSE.name -> R.string.place_type_courthouse
        Place.Type.DENTIST.name -> R.string.place_type_dentist
        Place.Type.DEPARTMENT_STORE.name -> R.string.place_type_department_store
        Place.Type.DOCTOR.name -> R.string.place_type_doctor
        Place.Type.ELECTRICIAN.name -> R.string.place_type_electrician
        Place.Type.ELECTRONICS_STORE.name -> R.string.place_type_electronics_store
        Place.Type.EMBASSY.name -> R.string.place_type_embassy
        Place.Type.ESTABLISHMENT.name -> R.string.place_type_establishment
        Place.Type.FINANCE.name -> R.string.place_type_finance
        Place.Type.FIRE_STATION.name -> R.string.place_type_fire_station
        Place.Type.FLOOR.name -> R.string.place_type_floor
        Place.Type.FLORIST.name -> R.string.place_type_florist
        Place.Type.FOOD.name -> R.string.place_type_food
        Place.Type.FUNERAL_HOME.name -> R.string.place_type_funeral_home
        Place.Type.FURNITURE_STORE.name -> R.string.place_type_furniture_store
        Place.Type.GAS_STATION.name -> R.string.place_type_gas_station
        Place.Type.GENERAL_CONTRACTOR.name -> R.string.place_type_general_contractor
        Place.Type.GEOCODE.name -> R.string.place_type_geocode
        Place.Type.GROCERY_OR_SUPERMARKET.name -> R.string.place_type_grocery_or_supermarket
        Place.Type.GYM.name -> R.string.place_type_gym
        Place.Type.HAIR_CARE.name -> R.string.place_type_hair_care
        Place.Type.HARDWARE_STORE.name -> R.string.place_type_hardware_store
        Place.Type.HEALTH.name -> R.string.place_type_health
        Place.Type.HINDU_TEMPLE.name -> R.string.place_type_hindu_temple
        Place.Type.HOME_GOODS_STORE.name -> R.string.place_type_home_goods_store
        Place.Type.HOSPITAL.name -> R.string.place_type_hospital
        Place.Type.INSURANCE_AGENCY.name -> R.string.place_type_insurance_agency
        Place.Type.INTERSECTION.name -> R.string.place_type_intersection
        Place.Type.JEWELRY_STORE.name -> R.string.place_type_jewelry_store
        Place.Type.LAUNDRY.name -> R.string.place_type_laundry
        Place.Type.LAWYER.name -> R.string.place_type_lawyer
        Place.Type.LIBRARY.name -> R.string.place_type_library
        Place.Type.LIQUOR_STORE.name -> R.string.place_type_liquor_store
        Place.Type.LOCAL_GOVERNMENT_OFFICE.name -> R.string.place_type_local_government_office
        Place.Type.LOCALITY.name -> R.string.place_type_locality
        Place.Type.LOCKSMITH.name -> R.string.place_type_locksmith
        Place.Type.LODGING.name -> R.string.place_type_lodging
        Place.Type.MEAL_DELIVERY.name -> R.string.place_type_meal_delivery
        Place.Type.MEAL_TAKEAWAY.name -> R.string.place_type_meal_takeaway
        Place.Type.MOSQUE.name -> R.string.place_type_mosque
        Place.Type.MOVIE_RENTAL.name -> R.string.place_type_movie_rental
        Place.Type.MOVIE_THEATER.name -> R.string.place_type_movie_theater
        Place.Type.MOVING_COMPANY.name -> R.string.place_type_moving_company
        Place.Type.MUSEUM.name -> R.string.place_type_museum
        Place.Type.NATURAL_FEATURE.name -> R.string.place_type_natural_feature
        Place.Type.NEIGHBORHOOD.name -> R.string.place_type_neighborhood
        Place.Type.NIGHT_CLUB.name -> R.string.place_type_night_club
        Place.Type.PAINTER.name -> R.string.place_type_painter
        Place.Type.PARK.name -> R.string.place_type_park
        Place.Type.PARKING.name -> R.string.place_type_parking
        Place.Type.PET_STORE.name -> R.string.place_type_pet_store
        Place.Type.PHARMACY.name -> R.string.place_type_pharmacy
        Place.Type.PHYSIOTHERAPIST.name -> R.string.place_type_physiotherapist
        Place.Type.PLACE_OF_WORSHIP.name -> R.string.place_type_place_of_worship
        Place.Type.PLUMBER.name -> R.string.place_type_plumber
        Place.Type.POINT_OF_INTEREST.name -> R.string.place_type_point_of_interest
        Place.Type.POLICE.name -> R.string.place_type_police
        Place.Type.POLITICAL.name -> R.string.place_type_political
        Place.Type.POST_BOX.name -> R.string.place_type_post_box
        Place.Type.POST_OFFICE.name -> R.string.place_type_post_office
        Place.Type.POSTAL_CODE.name -> R.string.place_type_postal_code
        Place.Type.POSTAL_CODE_PREFIX.name -> R.string.place_type_postal_code_prefix
        Place.Type.POSTAL_CODE_SUFFIX.name -> R.string.place_type_postal_code_suffix
        Place.Type.POSTAL_TOWN.name -> R.string.place_type_postal_town
        Place.Type.PREMISE.name -> R.string.place_type_premise
        Place.Type.REAL_ESTATE_AGENCY.name -> R.string.place_type_real_estate_agency
        Place.Type.RESTAURANT.name -> R.string.place_type_restaurant
        Place.Type.ROOFING_CONTRACTOR.name -> R.string.place_type_roofing_contractor
        Place.Type.ROOM.name -> R.string.place_type_room
        Place.Type.ROUTE.name -> R.string.place_type_route
        Place.Type.RV_PARK.name -> R.string.place_type_rv_park
        Place.Type.SCHOOL.name -> R.string.place_type_school
        Place.Type.SHOE_STORE.name -> R.string.place_type_shoe_store
        Place.Type.SHOPPING_MALL.name -> R.string.place_type_shopping_mall
        Place.Type.SPA.name -> R.string.place_type_spa
        Place.Type.STADIUM.name -> R.string.place_type_stadium
        Place.Type.STREET_ADDRESS.name -> R.string.place_type_street_address
        Place.Type.STORAGE.name -> R.string.place_type_storage
        Place.Type.STORE.name -> R.string.place_type_store
        Place.Type.SUBLOCALITY.name -> R.string.place_type_sublocality
        Place.Type.SUBLOCALITY_LEVEL_1.name -> R.string.place_type_sublocality_level_1
        Place.Type.SUBLOCALITY_LEVEL_2.name -> R.string.place_type_sublocality_level_2
        Place.Type.SUBLOCALITY_LEVEL_3.name -> R.string.place_type_sublocality_level_3
        Place.Type.SUBLOCALITY_LEVEL_4.name -> R.string.place_type_sublocality_level_4
        Place.Type.SUBLOCALITY_LEVEL_5.name -> R.string.place_type_sublocality_level_5
        Place.Type.SUBPREMISE.name -> R.string.place_type_subpremise
        Place.Type.SUBWAY_STATION.name -> R.string.place_type_subway_station
        Place.Type.SUPERMARKET.name -> R.string.place_type_supermarket
        Place.Type.SYNAGOGUE.name -> R.string.place_type_synagogue
        Place.Type.TAXI_STAND.name -> R.string.place_type_taxi_stand
        Place.Type.TRAIN_STATION.name -> R.string.place_type_train_station
        Place.Type.TRANSIT_STATION.name -> R.string.place_type_transit_station
        Place.Type.TRAVEL_AGENCY.name -> R.string.place_type_travel_agency
        Place.Type.UNIVERSITY.name -> R.string.place_type_university
        Place.Type.VETERINARY_CARE.name -> R.string.place_type_veterinary_care
        Place.Type.ZOO.name -> R.string.place_type_zoo
        else -> null
    }

    strId?.let {
        return CodeApp.getAppContext().getString(strId)
    }
    return null

}


/*



OTHER ->
ACCOUNTING ->
ADMINISTRATIVE_AREA_LEVEL_1 ->
ADMINISTRATIVE_AREA_LEVEL_2 ->
ADMINISTRATIVE_AREA_LEVEL_3 ->
ADMINISTRATIVE_AREA_LEVEL_4 ->
ADMINISTRATIVE_AREA_LEVEL_5 ->
AIRPORT ->
AMUSEMENT_PARK ->
AQUARIUM ->
ART_GALLERY ->
ATM ->
BAKERY ->
BANK ->
BAR ->
BEAUTY_SALON ->
BICYCLE_STORE ->
BOOK_STORE ->
BOWLING_ALLEY ->
BUS_STATION ->
CAFE ->
CAMPGROUND ->
CAR_DEALER ->
CAR_RENTAL ->
CAR_REPAIR ->
CAR_WASH ->
CASINO ->
CEMETERY ->
CHURCH ->
CITY_HALL ->
CLOTHING_STORE ->
COLLOQUIAL_AREA ->
CONVENIENCE_STORE ->
COUNTRY ->
COURTHOUSE ->
DENTIST ->
DEPARTMENT_STORE ->
DOCTOR ->
ELECTRICIAN ->
ELECTRONICS_STORE ->
EMBASSY ->
ESTABLISHMENT ->
FINANCE ->
FIRE_STATION ->
FLOOR ->
FLORIST ->
FOOD ->
FUNERAL_HOME ->
FURNITURE_STORE ->
GAS_STATION ->
GENERAL_CONTRACTOR ->
GEOCODE ->
GROCERY_OR_SUPERMARKET ->
GYM ->
HAIR_CARE ->
HARDWARE_STORE ->
HEALTH ->
HINDU_TEMPLE ->
HOME_GOODS_STORE ->
HOSPITAL ->
INSURANCE_AGENCY ->
INTERSECTION ->
JEWELRY_STORE ->
LAUNDRY ->
LAWYER ->
LIBRARY ->
LIQUOR_STORE ->
LOCAL_GOVERNMENT_OFFICE ->
LOCALITY ->
LOCKSMITH ->
LODGING ->
MEAL_DELIVERY ->
MEAL_TAKEAWAY ->
MOSQUE ->
MOVIE_RENTAL ->
MOVIE_THEATER ->
MOVING_COMPANY ->
MUSEUM ->
NATURAL_FEATURE ->
NEIGHBORHOOD ->
NIGHT_CLUB ->
PAINTER ->
PARK ->
PARKING ->
PET_STORE ->
PHARMACY ->
PHYSIOTHERAPIST ->
PLACE_OF_WORSHIP ->
PLUMBER ->
POINT_OF_INTEREST ->
POLICE ->
POLITICAL ->
POST_BOX ->
POST_OFFICE ->
POSTAL_CODE ->
POSTAL_CODE_PREFIX ->
POSTAL_CODE_SUFFIX ->
POSTAL_TOWN ->
PREMISE ->
REAL_ESTATE_AGENCY ->
RESTAURANT ->
ROOFING_CONTRACTOR ->
ROOM ->
ROUTE ->
RV_PARK ->
SCHOOL ->
SHOE_STORE ->
SHOPPING_MALL ->
SPA ->
STADIUM ->
STREET_ADDRESS ->
STORAGE ->
STORE ->
SUBLOCALITY ->
SUBLOCALITY_LEVEL_1 ->
SUBLOCALITY_LEVEL_2 ->
SUBLOCALITY_LEVEL_3 ->
SUBLOCALITY_LEVEL_4 ->
SUBLOCALITY_LEVEL_5 ->
SUBPREMISE ->
SUBWAY_STATION ->
SUPERMARKET ->
SYNAGOGUE ->
TAXI_STAND ->
TRAIN_STATION ->
TRANSIT_STATION ->
TRAVEL_AGENCY ->
UNIVERSITY ->
VETERINARY_CARE ->
ZOO ->













other
accounting
administrative_area_level_1
administrative_area_level_2
administrative_area_level_3
administrative_area_level_4
administrative_area_level_5
airport
amusement_park
aquarium
art_gallery
atm
bakery
bank
bar
beauty_salon
bicycle_store
book_store
bowling_alley
bus_station
cafe
campground
car_dealer
car_rental
car_repair
car_wash
casino
cemetery
church
city_hall
clothing_store
colloquial_area
convenience_store
country
courthouse
dentist
department_store
doctor
electrician
electronics_store
embassy
establishment
finance
fire_station
floor
florist
food
funeral_home
furniture_store
gas_station
general_contractor
geocode
grocery_or_supermarket
gym
hair_care
hardware_store
health
hindu_temple
home_goods_store
hospital
insurance_agency
intersection
jewelry_store
laundry
lawyer
library
liquor_store
local_government_office
locality
locksmith
lodging
meal_delivery
meal_takeaway
mosque
movie_rental
movie_theater
moving_company
museum
natural_feature
neighborhood
night_club
painter
park
parking
pet_store
pharmacy
physiotherapist
place_of_worship
plumber
point_of_interest
police
political
post_box
post_office
postal_code
postal_code_prefix
postal_code_suffix
postal_town
premise
real_estate_agency
restaurant
roofing_contractor
room
route
rv_park
school
shoe_store
shopping_mall
spa
stadium
street_address
storage
store
sublocality
sublocality_level_1
sublocality_level_2
sublocality_level_3
sublocality_level_4
sublocality_level_5
subpremise
subway_station
supermarket
synagogue
taxi_stand
train_station
transit_station
travel_agency
university
veterinary_care
zoo



























































*/
