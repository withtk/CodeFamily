package com.tkip.mycode.util.lib.retrofit2

import com.tkip.mycode.R
import com.tkip.mycode.dialog.progress.MyCircle
import com.tkip.mycode.dialog.toast.toastWarning
import com.tkip.mycode.funs.common.lllogD
import com.tkip.mycode.funs.common.lllogE
import com.tkip.mycode.funs.common.lllogI
import com.tkip.mycode.init.*
import com.tkip.mycode.init.base.getStr
import com.tkip.mycode.init.base.requestBody
import com.tkip.mycode.model.bid.Bid
import com.tkip.mycode.model.board.Board
import com.tkip.mycode.model.cost.Cost
import com.tkip.mycode.model.count.Count
import com.tkip.mycode.model.flea.Flea
import com.tkip.mycode.model.grant.Grant
import com.tkip.mycode.model.manage.Device
import com.tkip.mycode.model.manage.SystemMan
import com.tkip.mycode.model.mytag.MyTag
import com.tkip.mycode.model.peer.Peer
import com.tkip.mycode.model.peer.peer_allow.PeerAllow
import com.tkip.mycode.model.peer.recommend.UidDate
import com.tkip.mycode.model.post.Post
import com.tkip.mycode.model.reply.Reply
import com.tkip.mycode.model.room.Room
import com.tkip.mycode.model.store.Store
import com.tkip.mycode.model.store.StoreMenu
import com.tkip.mycode.model.ticket.Ticket
import com.tkip.mycode.model.warn.Warn
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.io.IOException
import java.net.SocketTimeoutException

class ESget {

    companion object {

//        private fun <T> myCallback1(fn: (Throwable?, Response<T>?) -> Unit): Callback<T> {
//            return object : Callback<T> {
//                override fun onResponse(call: Call<T>, response: retrofit2.Response<T>) = fn(null, response)
//                override fun onFailure(call: Call<T>, t: Throwable) = fn(t, null)
//            }
//        }
//        example
//            request.enqueue(callback({ throwable, response ->
//                response?.let { callBack.onResponse(response.body() ?: RegisterResponse()) }
//                throwable?.let { callBack.onFailed(throwable.message!!) })

//        private fun <T> myCallback2(success: ((Response<T>) -> Unit)?, failure: ((t: Throwable) -> Unit)? = null): Callback<T> {
//            return object : Callback<T> {
//                override fun onResponse(call: Call<T>, response: retrofit2.Response<T>) {
//                    success?.invoke(response)
//                }
//
//                override fun onFailure(call: Call<T>, t: Throwable) {
//                    failure?.invoke(t)
//                }
//            }
//        }

//        example
//        request.enqueue(callback2(
//        { r -> callBack.onResponse(r.body()) },
//        { t -> callBack.onFailed(t.message) }))


        //todo: 토스트 안 날리고 조용히 끝낼 수 있게.
        private fun <T> myCallll(method: String, queryOrKey: String?, success: ((Response<T>?) -> Unit)?, failure: (() -> Unit)? = null): Callback<T> {
            lllogI("EsGet myCallll start ($method) \n query : $queryOrKey")

            return object : Callback<T> {
                override fun onResponse(call: Call<T>, response: Response<T>) {
                    if (response.isSuccessful) success?.invoke(response)
                    else {
                        lllogD("EsGet myCallll onResponse error($method) - response.errorBody():${response.body().toString()}   ")
                        response.errorBody()?.let {
                            lllogD("EsGet myCallll  - errorBody:${it.string()}   ")
//                            val err = Gson().fromJson(it.charStream(), EsError::class.java)
//                            lllogD("EsGet myCallll onResponse error($method) - code:${response.code()} \n$err  ")
                        }
                        success?.invoke(null)
                    }
                }

                override fun onFailure(call: Call<T>, t: Throwable) {
                    MyCircle.cancel()
                    failure?.invoke()
                    lllogE("myCalll esfail onFailure error($method)================", t)
                    when (t) {
                        is SocketTimeoutException -> lllogD(R.string.error_auth_error.getStr())  // 통신오류 or 인증오류
                        is IOException -> toastWarning(R.string.error_internet_too_long)  // 시간 초과
                        else -> toastWarning(R.string.db_failure)
                    }
                }
            }
        }

        private fun writeLog(str: String) {
            lllogI("EsGet writeLog query : $str")
        }


        /*********************************  sttIndex  *******************************/
        fun createIndex(esApi: EsApiService?, index: String, mapping: String, success: (String?) -> Unit, fail: (() -> Unit)? = null) {
            writeLog("createIndex $index")
            esApi?.createIndex(index, mapping.requestBody())
                    ?.enqueue(myCallll("createIndex", null, success = { r ->
                        lllogI("$index success r :${r?.body().toString()}")
                        success(r?.body().toString())
                    }, failure = { fail?.invoke() }))
        }

        fun deleteIndex(esApi: EsApiService?, index: String,   success: (String?) -> Unit, fail: (() -> Unit)? = null) {
            writeLog("deleteIndex $index")
            esApi?.deleteIndex(index )
                    ?.enqueue(myCallll("deleteIndex", null, success = { r ->
                        lllogI("$index success r :${r?.body().toString()}")
                        success(r?.body().toString())
                    }, failure = { fail?.invoke() }))
        }

        /*********************************  sttTotal  *******************************/
        fun addModel(esApi: EsApiService?, index: String, key: String, any: Any, success: (Boolean?) -> Unit, fail: (() -> Unit)? = null) {
            esApi?.addModel(index, key, any)?.enqueue(myCallll(index, key, success = { r ->
                if (r == null) success(null) else {
                    r.body()?._shards?.successful?.let { success(it == 1) }
                    lllogI("esget delModel success : ${r.body().toString()}")
                }
            }, failure = { fail?.invoke() }))
        }

        fun delModel(index: String, key: String, success: (Boolean?) -> Unit, fail: (() -> Unit)? = null) {
            Retro.esApi()?.delModel(index, key)?.enqueue(myCallll(index, key, success = { r ->
                if (r == null) success(null) else {
                    r.body()?._shards?.successful?.let { success(it == 1) }
                    lllogI("esget delModel success : ${r.body().toString()}")
                }
            }, failure = { fail?.invoke() }))
        }

        /*********************************  sttSystem  *******************************/
        fun getSystemManage(success: (SystemMan?) -> Unit, fail: (() -> Unit)? = null) {
            Retro.esApi()?.getSystemManage(EES_SYSTEM, SYSTEM_MAN_KEY)?.enqueue(myCallll("getSystemManage:$EES_SYSTEM", null, success = { r ->
                if (r == null) success(null) else r.body()?.source?.let { success(it) }
            }, failure = { fail?.invoke() }))
        }

        /*********************************  sttPeer  *******************************/
        fun getPeer(key: String, success: (Peer?) -> Unit, fail: (() -> Unit)? = null) {
            Retro.esApi()?.getPeer(EES_PEER, key)?.enqueue(myCallll("getPeer:$EES_PEER", key, success = { r -> if (r == null) success(null) else r.body()?.source?.let { success(it) } }, failure = { fail?.invoke() }))
        }

        fun getPeerList(query: String, success: (ArrayList<Peer>?) -> Unit, fail: (() -> Unit)? = null) {
            Retro.esApi()?.getPeerList(EES_PEER, query.requestBody())?.enqueue(myCallll("getPeerList:$EES_PEER", query, success = { r -> if (r == null) success(null) else success(arrayListOf<Peer>().apply { r.body()?.hits?.hits?.let { it.forEach { hits -> hits.source?.let { source -> add(source) } } } }) }, failure = { fail?.invoke() }))
        }

        fun getPeerBody(query: String, success: (PeerObject?) -> Unit, fail: (() -> Unit)? = null) {
            Retro.esApi()?.getPeerList(EES_PEER, query.requestBody())?.enqueue(myCallll("getPeerBody:$EES_PEER", query, success = { r -> success(r?.body()) }, failure = { fail?.invoke() }))
        }

        /*********************************  sttUidDate  *******************************/
        fun getRecommend(key: String, success: (UidDate?) -> Unit, fail: (() -> Unit)? = null) {
            Retro.esApi()?.getRecommend(EES_RECOMMEND, key)?.enqueue(myCallll("getRecommend:$EES_RECOMMEND", key, success = { r -> if (r == null) success(null) else r.body()?.source?.let { success(it) } }, failure = { fail?.invoke() }))
        }

        fun getRecommendList(query: String, success: (ArrayList<UidDate>?) -> Unit, fail: (() -> Unit)? = null) {
            Retro.esApi()?.getRecommendList(EES_RECOMMEND, query.requestBody())?.enqueue(myCallll("getRecommendList:$EES_RECOMMEND", query, success = { r -> if (r == null) success(null) else success(arrayListOf<UidDate>().apply { r.body()?.hits?.hits?.let { it.forEach { hits -> hits.source?.let { source -> add(source) } } } }) }, failure = { fail?.invoke() }))
        }

        /*********************************  sttDevice  *******************************/
        fun getDevice(key: String, success: (Device?) -> Unit, fail: (() -> Unit)? = null) {
            Retro.esApi()?.getDevice(EES_DEVICE, key)?.enqueue(myCallll("getDevice", key, success = { r -> if (r == null) success(null) else r.body()?.source?.let { success(it) } }, failure = { fail?.invoke() }))
        }

        fun getDeviceList(query: String, success: (ArrayList<Device>?) -> Unit, fail: (() -> Unit)? = null) {
            Retro.esApi()?.getDeviceList(EES_DEVICE, query.requestBody())?.enqueue(myCallll("getDeviceList:$EES_DEVICE", query, success = { r -> if (r == null) success(null) else success(arrayListOf<Device>().apply { r.body()?.hits?.hits?.let { it.forEach { hits -> hits.source?.let { source -> add(source) } } } }) }, failure = { fail?.invoke() }))
        }

        /*********************************  sttMiniPeer  *******************************/
        fun getBoardAllow(key: String, success: (PeerAllow?) -> Unit, fail: (() -> Unit)? = null) {
            Retro.esApi()?.getBoardAllow(EES_BOARD_ALLOW, key)?.enqueue(myCallll("getBoardAllow", key, success = { r -> if (r == null) success(null) else r.body()?.source?.let { success(it) } }, failure = { fail?.invoke() }))
        }

        fun getBoardAllowList(query: String, success: (ArrayList<PeerAllow>?) -> Unit, fail: (() -> Unit)? = null) {
            Retro.esApi()?.getBoardAllowList(EES_BOARD_ALLOW, query.requestBody())?.enqueue(myCallll("getBoardAllowList:$EES_BOARD_ALLOW", query, success = { r -> if (r == null) success(null) else success(arrayListOf<PeerAllow>().apply { r.body()?.hits?.hits?.let { it.forEach { hits -> hits.source?.let { source -> add(source) } } } }) }, failure = { fail?.invoke() }))
        }

        /*********************************  sttBoard  *******************************/
        fun getBoard(key: String, success: (Board?) -> Unit, fail: (() -> Unit)? = null) {
            Retro.esApi()?.getBoard(EES_BOARD, key)?.enqueue(myCallll("getBoard", key, success = { r -> if (r == null) success(null) else r.body()?.source?.let { success(it) } }, failure = { fail?.invoke() }))
        }

        fun getBoardList(query: String, success: (ArrayList<Board>?) -> Unit, fail: (() -> Unit)? = null) {
            Retro.esApi()?.getBoardList(EES_BOARD, query.requestBody())?.enqueue(myCallll("getBoardList:$EES_BOARD", query, success = { r -> if (r == null) success(null) else success(arrayListOf<Board>().apply { r.body()?.hits?.hits?.let { it.forEach { hits -> hits.source?.let { source -> add(source) } } } }) }, failure = { fail?.invoke() }))
        }

        fun sizeBoardList(query: String, success: (Int?) -> Unit, fail: (() -> Unit)? = null) {
            Retro.esApi()?.getBoardList(EES_BOARD, query.requestBody())?.enqueue(myCallll("sizeBoardList:$EES_BOARD", query, success = { r -> if (r == null) success(null) else r.body()?.hits?.total?.value?.let { success(it) } }, failure = { fail?.invoke() }))
        }

        /*********************************  sttPost  *******************************/
        fun getPost(key: String, success: (Post?) -> Unit, fail: (() -> Unit)? = null) {
            Retro.esApi()?.getPost(EES_POST, key)?.enqueue(myCallll("getPost", key, success = { r -> if (r == null) success(null) else r.body()?.source?.let { success(it) } }, failure = { fail?.invoke() }))
        }

        fun getPostList(query: String, success: (ArrayList<Post>?) -> Unit, fail: (() -> Unit)? = null) {
            Retro.esApi()?.getPostList(EES_POST, query.requestBody())?.enqueue(myCallll("getPostList:$EES_POST", query, success = { r -> if (r == null) success(null) else success(arrayListOf<Post>().apply { r.body()?.hits?.hits?.let { it.forEach { hits -> hits.source?.let { source -> add(source) } } } }) }, failure = { fail?.invoke() }))
        }

        /*********************************  sttFlea  *******************************/
        fun getFlea(key: String, success: (Flea?) -> Unit, fail: (() -> Unit)? = null) {
            Retro.esApi()?.getFlea(EES_FLEA, key)?.enqueue(myCallll("getPost", key, success = { r -> if (r == null) success(null) else r.body()?.source?.let { success(it) } }, failure = { fail?.invoke() }))
        }

        fun getFleaList(query: String, success: (ArrayList<Flea>?) -> Unit, fail: (() -> Unit)? = null) {
            Retro.esApi()?.getFleaList(EES_FLEA, query.requestBody())?.enqueue(myCallll("getFleaList:$EES_FLEA", query, success = { r -> if (r == null) success(null) else success(arrayListOf<Flea>().apply { r.body()?.hits?.hits?.let { it.forEach { hits -> hits.source?.let { source -> add(source) } } } }) }, failure = { fail?.invoke() }))
        }

        /*********************************  sttRoom  *******************************/
        fun getRoom(key: String, success: (Room?) -> Unit, fail: (() -> Unit)? = null) {
            Retro.esApi()?.getRoom(EES_ROOM, key)?.enqueue(myCallll("getRoom", key, success = { r -> if (r == null) success(null) else r.body()?.source?.let { success(it) } }, failure = { fail?.invoke() }))
        }

        fun getRoomList(query: String, success: (ArrayList<Room>?) -> Unit, fail: (() -> Unit)? = null) {
            Retro.esApi()?.getRoomList(EES_ROOM, query.requestBody())?.enqueue(myCallll("getRoomList:$EES_ROOM", query, success = { r -> if (r == null) success(null) else success(arrayListOf<Room>().apply { r.body()?.hits?.hits?.let { it.forEach { hits -> hits.source?.let { source -> add(source) } } } }) }, failure = { fail?.invoke() }))
        }

        /*********************************  sttLobby  *******************************/
//        fun getLobby(key: String, success: (Lobby?) -> Unit, fail: (() -> Unit)? = null) {
//            Retro.esApi()?.getLobby(EES_LOBBY, key)?.enqueue(myCallll("getLobby", key, success = { r -> if (r == null) success(null) else r.body()?.source?.let { success(it) } }, failure = { fail?.invoke() }))
//        }
//
//        fun getLobbyList(query: String, success: (ArrayList<Lobby>?) -> Unit, fail: (() -> Unit)? = null) {
//            Retro.esApi()?.getLobbyList(EES_LOBBY, query.requestBody())?.enqueue(myCallll("getLobbyList:$EES_LOBBY", query, success = { r -> if (r == null) success(null) else success(arrayListOf<Lobby>().apply { r.body()?.hits?.hits?.let { it.forEach { hits -> hits.source?.let { source -> add(source) } } } }) }, failure = { fail?.invoke() }))
//        }

        /*********************************  sttReply  *******************************/
        fun getReply(key: String, success: (Reply?) -> Unit, fail: (() -> Unit)? = null) {
            Retro.esApi()?.getReply(EES_REPLY, key)?.enqueue(myCallll("getReply", key, success = { r -> if (r == null) success(null) else r.body()?.source?.let { success(it) } }, failure = { fail?.invoke() }))
        }

        fun getReplyList(query: String, success: (ArrayList<Reply>?) -> Unit, fail: (() -> Unit)? = null) {
            Retro.esApi()?.getReplyList(EES_REPLY, query.requestBody())?.enqueue(myCallll("getReplyList:$EES_REPLY", query, success = { r -> if (r == null) success(null) else success(arrayListOf<Reply>().apply { r.body()?.hits?.hits?.let { it.forEach { hits -> hits.source?.let { source -> add(source) } } } }) }, failure = { fail?.invoke() }))
        }

        /*********************************  sttStore  *******************************/
        fun getStore(key: String, success: (Store?) -> Unit, fail: (() -> Unit)? = null) {
            Retro.esApi()?.getStore(EES_STORE, key)?.enqueue(myCallll("getStore", key, success = { r -> if (r == null) success(null) else r.body()?.source?.let { success(it) } }, failure = { fail?.invoke() }))
        }

        fun getStoreList(query: String, success: (ArrayList<Store>?) -> Unit, fail: (() -> Unit)? = null) {
            Retro.esApi()?.getStoreList(EES_STORE, query.requestBody())?.enqueue(myCallll("getStoreList:$EES_STORE", query, success = { r -> if (r == null) success(null) else success(arrayListOf<Store>().apply { r.body()?.hits?.hits?.let { it.forEach { hits -> hits.source?.let { source -> add(source) } } } }) }, failure = { fail?.invoke() }))
        }

        fun getStoreMenu(key: String, success: (StoreMenu?) -> Unit, fail: (() -> Unit)? = null) {
            Retro.esApi()?.getStoreMenu(EES_STORE_MENU, key)?.enqueue(myCallll("getStoreMenu", key, success = { r -> if (r == null) success(null) else r.body()?.source?.let { success(it) } }, failure = { fail?.invoke() }))
        }

        fun getStoreMenuList(query: String, success: (ArrayList<StoreMenu>?) -> Unit, fail: (() -> Unit)? = null) {
            Retro.esApi()?.getStoreMenuList(EES_STORE_MENU, query.requestBody())?.enqueue(myCallll("getStoreMenuList:$EES_STORE_MENU", query, success = { r -> if (r == null) success(null) else success(arrayListOf<StoreMenu>().apply { r.body()?.hits?.hits?.let { it.forEach { hits -> hits.source?.let { source -> add(source) } } } }) }, failure = { fail?.invoke() }))
        }

        /*********************************  sttCount  *******************************/
        fun getCount(index: String, key: String, success: (Count?) -> Unit, fail: (() -> Unit)? = null) {
            Retro.esApi()?.getCount(index, key)?.enqueue(myCallll("getCount", key, success = { r -> if (r == null) success(null) else r.body()?.source?.let { success(it) } }, failure = { fail?.invoke() }))
        }

        fun getCountList(method: String, index: String, query: String, success: (ArrayList<Count>?) -> Unit, fail: (() -> Unit)? = null) {
            Retro.esApi()?.getCountList(index, query.requestBody())?.enqueue(myCallll("getCountList $method-$index ::", query, success = { r -> if (r == null) success(null) else success(arrayListOf<Count>().apply { r.body()?.hits?.hits?.let { it.forEach { hits -> hits.source?.let { source -> add(source) } } } }) }, failure = { fail?.invoke() }))
        }


        /*********************************  sttWarn  *******************************/
        fun getWarnList(index: String, query: String, success: (ArrayList<Warn>?) -> Unit, fail: (() -> Unit)? = null) {
            Retro.esApi()?.getWarnList(index, query.requestBody())?.enqueue(myCallll("getWarnList:$EES_WARN", query, success = { r -> if (r == null) success(null) else success(arrayListOf<Warn>().apply { r.body()?.hits?.hits?.let { it.forEach { hits -> hits.source?.let { source -> add(source) } } } }) }, failure = { fail?.invoke() }))
        }

        /*********************************  sttvCost  *******************************/
        fun getCost(key: String, success: (Cost?) -> Unit, fail: (() -> Unit)? = null) {
            Retro.esApi()?.getCost(EES_COST, key)?.enqueue(myCallll("getCost", key, success = { r -> if (r == null) success(null) else r.body()?.source?.let { success(it) } }, failure = { fail?.invoke() }))
        }

        fun getCostList(query: String, success: (ArrayList<Cost>?) -> Unit, fail: (() -> Unit)? = null) {
            Retro.esApi()?.getCostList(EES_COST, query.requestBody())?.enqueue(myCallll("getCostList:$EES_COST", query, success = { r -> if (r == null) success(null) else success(arrayListOf<Cost>().apply { r.body()?.hits?.hits?.let { it.forEach { hits -> hits.source?.let { source -> add(source) } } } }) }, failure = { fail?.invoke() }))
        }

        fun sizeCostList(query: String, success: (Int?) -> Unit, fail: (() -> Unit)? = null) {
            Retro.esApi()?.getCostList(EES_COST, query.requestBody())?.enqueue(myCallll("sizeCostList:$EES_COST", query, success = { r -> if (r == null) success(null) else r.body()?.hits?.total?.value?.let { success(it) } }, failure = { fail?.invoke() }))
        }

        /*********************************  sttAct  *******************************/
        fun getAct(key: String, success: (Cost?) -> Unit, fail: (() -> Unit)? = null) {
            Retro.esApi()?.getAct(EES_ACT, key)?.enqueue(myCallll("getAct", key, success = { r -> if (r == null) success(null) else r.body()?.source?.let { success(it) } }, failure = { fail?.invoke() }))
        }

        fun getActList(query: String, success: (ArrayList<Cost>?) -> Unit, fail: (() -> Unit)? = null) {
            Retro.esApi()?.getActList(EES_ACT, query.requestBody())?.enqueue(myCallll("getActList:$EES_ACT", query, success = { r -> if (r == null) success(null) else success(arrayListOf<Cost>().apply { r.body()?.hits?.hits?.let { it.forEach { hits -> hits.source?.let { source -> add(source) } } } }) }, failure = { fail?.invoke() }))
        }

        /*********************************  sttTag  *******************************/
        fun getMyTag(key: String, success: (MyTag?) -> Unit, fail: (() -> Unit)? = null) {
            Retro.esApi()?.getMyTag(EES_TAG, key)?.enqueue(myCallll("getCount", key, success = { r -> if (r == null) success(null) else r.body()?.source?.let { success(it) } }, failure = { fail?.invoke() }))
        }

        fun getMyTagList(query: String, success: (ArrayList<MyTag>?) -> Unit, fail: (() -> Unit)? = null) {
            Retro.esApi()?.getMyTagList(EES_TAG, query.requestBody())?.enqueue(myCallll("getTagList:$EES_TAG", query, success = { r -> if (r == null) success(null) else success(arrayListOf<MyTag>().apply { r.body()?.hits?.hits?.let { it.forEach { hits -> hits.source?.let { source -> add(source) } } } }) }, failure = { fail?.invoke() }))
        }

        fun cntMyTagList(query: String, success: (Int?) -> Unit, fail: (() -> Unit)? = null) {
            Retro.esApi()?.getMyTagList(EES_TAG, query.requestBody())?.enqueue(myCallll("cntMyTagList:$EES_TAG", query, success = { r -> if (r == null) success(null) else r.body()?.hits?.total?.value?.let { success(it) } }, failure = { fail?.invoke() }))
        }

        /*********************************  sttBid  *******************************/
        fun getBid(key: String, success: (Bid?) -> Unit, fail: (() -> Unit)? = null) {
            Retro.esApi()?.getBid(EES_BID, key)?.enqueue(myCallll("getBid", key, success = { r -> if (r == null) success(null) else r.body()?.source?.let { success(it) } }, failure = { fail?.invoke() }))
        }

        fun getBidList(method: String, query: String, success: (ArrayList<Bid>?) -> Unit, fail: (() -> Unit)? = null) {
            Retro.esApi()?.getBidList(EES_BID, query.requestBody())?.enqueue(myCallll("getBidList>$method:$EES_BID ::", query, success = { r -> if (r == null) success(null) else success(arrayListOf<Bid>().apply { r.body()?.hits?.hits?.let { it.forEach { hits -> hits.source?.let { source -> add(source) } } } }) }, failure = { fail?.invoke() }))
        }

        fun getBanner(key: String, success: (Bid?) -> Unit, fail: (() -> Unit)? = null) {
            Retro.esApi()?.getBanner(EES_BANNER, key)?.enqueue(myCallll("getBanner", key, success = { r -> if (r == null) success(null) else r.body()?.source?.let { success(it) } }, failure = { fail?.invoke() }))
        }

        fun getBannerList(query: String, success: (ArrayList<Bid>?) -> Unit, fail: (() -> Unit)? = null) {
            Retro.esApi()?.getBannerList(EES_BANNER, query.requestBody())?.enqueue(myCallll("getBannerList:$EES_BANNER", query, success = { r -> if (r == null) success(null) else success(arrayListOf<Bid>().apply { r.body()?.hits?.hits?.let { it.forEach { hits -> hits.source?.let { source -> add(source) } } } }) }, failure = { fail?.invoke() }))
        }


        /*********************************  sttTicket & Bill   ******************************/
        fun getTicket(index: String, key: String, success: (Ticket?) -> Unit, fail: (() -> Unit)? = null) {
            Retro.esApi()?.getTicket(index, key)?.enqueue(myCallll("getTicket", key, success = { r -> if (r == null) success(null) else r.body()?.source?.let { success(it) } }, failure = { fail?.invoke() }))
        }

        fun getTicketList(index: String, query: String, success: (ArrayList<Ticket>?) -> Unit, fail: (() -> Unit)? = null) {
            Retro.esApi()?.getTicketList(index, query.requestBody())?.enqueue(myCallll("getTicketList:$index ::", query, success = { r -> if (r == null) success(null) else success(arrayListOf<Ticket>().apply { r.body()?.hits?.hits?.let { it.forEach { hits -> hits.source?.let { source -> add(source) } } } }) }, failure = { fail?.invoke() }))
        }

        fun sizeTicketList(index: String, query: String, success: (Int?) -> Unit, fail: (() -> Unit)? = null) {
            Retro.esApi()?.getBoardList(index, query.requestBody())?.enqueue(myCallll("sizeTicketList:$index", query, success = { r -> if (r == null) success(null) else r.body()?.hits?.total?.value?.let { success(it) } }, failure = { fail?.invoke() }))
        }

        /*********************************  sttGrant   ******************************/
        fun getGrant(key: String, success: (Grant?) -> Unit, fail: (() -> Unit)? = null) {
            lllogD("esGet getGrant $key")
            Retro.esApi()?.getGrant(EES_GRANT, key)?.enqueue(myCallll("getGrant", key, success = { r -> if (r == null) success(null) else r.body()?.source?.let { success(it) } }, failure = { fail?.invoke() }))
        }

        fun getGrantList(query: String, success: (ArrayList<Grant>?) -> Unit, fail: (() -> Unit)? = null) {
            Retro.esApi()?.getGrantList(EES_GRANT, query.requestBody())?.enqueue(myCallll("getGrantList:$EES_GRANT ::", query, success = { r -> if (r == null) success(null) else success(arrayListOf<Grant>().apply { r.body()?.hits?.hits?.let { it.forEach { hits -> hits.source?.let { source -> add(source) } } } }) }, failure = { fail?.invoke() }))
        }


    }
}