package com.tkip.mycode.util.lib.photo.util

import android.content.Context
import android.graphics.Bitmap
import android.graphics.Matrix
import android.net.Uri
import android.os.Environment
import android.provider.MediaStore
import androidx.core.content.FileProvider
import androidx.exifinterface.media.ExifInterface
import com.tkip.mycode.init.part.CodeApp
import java.io.File
import java.io.FileOutputStream
import java.io.IOException
import java.io.OutputStream


object ExifUtil {

    fun getOrientationFromUri(context1: Context?, bitmapUri: Uri): Int {

        var path: String? = null

        val context = CodeApp.getAppContext()
        if (context.contentResolver != null) {
            context.contentResolver.query(bitmapUri, null, null, null, null)?.use { cursor ->
                cursor.moveToFirst()
                val idx = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA)
                path = cursor.getString(idx)
                cursor.close()
            }
        }
        return getOrientationFromPath(path)
    }

    private fun getOrientationFromPath(path: String?): Int {

        var exifInterface: ExifInterface? = null
        try {
            exifInterface = ExifInterface(path!!)
        } catch (e: IOException) {
            e.printStackTrace()
        }

        return exifInterface?.getAttributeInt(ExifInterface.TAG_ORIENTATION, ExifInterface.ORIENTATION_NORMAL)
                ?: ExifInterface.ORIENTATION_NORMAL
    }


    fun getUriFixedSizeFromBitmap(context: Context, inputBitmap: Bitmap, width: Int, height: Int): Uri {
        var bitmap = inputBitmap

        var file: File? = null
        var out: OutputStream? = null
        try {
            file = File.createTempFile(
                    "Phil_" + System.currentTimeMillis(),
                    ".jpg",
                    context.getExternalFilesDir(Environment.DIRECTORY_PICTURES)
            )
            file.deleteOnExit()
            out = FileOutputStream(file!!)

            if (width < 9000) {   // 사이즈 줄이기
                bitmap = Bitmap.createScaledBitmap(bitmap, width, height, true)
            }

            bitmap.compress(Bitmap.CompressFormat.JPEG, 100, out)
            out.close()
        } catch (e: Exception) {
            e.printStackTrace()
        }

        PhotoUtil.addDeleteFile(file!!)
        return FileProvider.getUriForFile(context, context.applicationContext.packageName, file)
    }

    /**
     * @see 'http://sylvana.net/jpegcrop/exif_orientation.html'
     */
    fun getRotatedBitmap(bitmap: Bitmap, orientation: Int): Bitmap {
        val matrix = Matrix()
        when (orientation) {
            ExifInterface.ORIENTATION_FLIP_HORIZONTAL -> matrix.setScale(-1f, 1f)
            ExifInterface.ORIENTATION_ROTATE_180 -> matrix.setRotate(180f)
            ExifInterface.ORIENTATION_FLIP_VERTICAL -> {
                matrix.setRotate(180f)
                matrix.postScale(-1f, 1f)
            }
            ExifInterface.ORIENTATION_TRANSPOSE -> {
                matrix.setRotate(90f)
                matrix.postScale(-1f, 1f)
            }
            ExifInterface.ORIENTATION_ROTATE_90 -> matrix.setRotate(90f)
            ExifInterface.ORIENTATION_TRANSVERSE -> {
                matrix.setRotate(-90f)
                matrix.postScale(-1f, 1f)
            }
            ExifInterface.ORIENTATION_ROTATE_270 -> matrix.setRotate(-90f)
            else -> return bitmap
        }

        try {
            val oriented = Bitmap.createBitmap(bitmap, 0, 0, bitmap.width, bitmap.height, matrix, true)
            bitmap.recycle()
            return oriented
        } catch (e: OutOfMemoryError) {
            e.printStackTrace()
            return bitmap
        }

    }


    /*

//  @see 'http://sylvana.net/jpegcrop/exif_orientation.html'
    public static Bitmap rotateBitmap(String src, Bitmap bitmap) {
        try {

//            ExifInterface exifInterface = new ExifInterface(src);
//            return exifInterface.getAttributeInt(ExifInterface.TAG_ORIENTATION, ExifInterface.ORIENTATION_NORMAL);


//            int orientation = getExifOrientation(src);

            ExifInterface ei = new ExifInterface(src);
            int orientation = ei.getAttributeInt(ExifInterface.TAG_ORIENTATION,
                    ExifInterface.ORIENTATION_UNDEFINED);


            if (orientation == ExifInterface.ORIENTATION_NORMAL) {
                return bitmap;
            }

            Matrix matrix = new Matrix();
            switch (orientation) {
                case ExifInterface.ORIENTATION_FLIP_HORIZONTAL:
                    matrix.setScale(-1, 1);
                    break;
                case ExifInterface.ORIENTATION_ROTATE_180:
                    matrix.setRotate(180);
                    break;
                case ExifInterface.ORIENTATION_FLIP_VERTICAL:
                    matrix.setRotate(180);
                    matrix.postScale(-1, 1);
                    break;
                case ExifInterface.ORIENTATION_TRANSPOSE:
                    matrix.setRotate(90);
                    matrix.postScale(-1, 1);
                    break;
                case ExifInterface.ORIENTATION_ROTATE_90:
                    matrix.setRotate(90);
                    break;
                case ExifInterface.ORIENTATION_TRANSVERSE:
                    matrix.setRotate(-90);
                    matrix.postScale(-1, 1);
                    break;
                case ExifInterface.ORIENTATION_ROTATE_270:
                    matrix.setRotate(-90);
                    break;
                default:
                    return bitmap;
            }

            try {
                Bitmap oriented = Bitmap.createBitmap(bitmap, 0, 0, bitmap.getWidth(), bitmap.getHeight(), matrix, true);
                bitmap.recycle();
                return oriented;
            } catch (OutOfMemoryError e) {
                e.printStackTrace();
                return bitmap;
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        return bitmap;
    }


    public static String getRealPathFromURI(Context context, Uri uri) {
        String path = "";
        if (context.getContentResolver() != null) {
            Cursor cursor = context.getContentResolver().query(uri, null, null, null, null);
            if (cursor != null) {
                cursor.moveToFirst();
                int idx = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA);
                path = cursor.getString(idx);
                cursor.close();
            }
        }
        return path;
    }

    private static String getPathForPreV19(Context context, Uri contentUri) {
        String[] projection = {MediaStore.Images.MyMedia.DATA};
        Cursor cursor = context.getContentResolver().query(contentUri, projection, null, null, null);
        if (cursor != null && cursor.moveToFirst()) {
            try {
                int columnIndex = cursor.getColumnIndexOrThrow(MediaStore.Images.MyMedia.DATA);
                return cursor.getString(columnIndex);
            } finally {
                cursor.close();
            }
        }
        return null;
    }

    private static String getPathForV19AndUp(Context context, Uri contentUri) {
        String wholeID = DocumentsContract.getDocumentId(contentUri);

        // Split at colon, use second item in the array
        String id = wholeID.split(":")[1];
        String[] column = {MediaStore.Images.MyMedia.DATA};

        // where id is equal to
        String sel = MediaStore.Images.MyMedia._ID + "=?";

        Cursor cursor = context.getContentResolver().query(
                MediaStore.Images.MyMedia.EXTERNAL_CONTENT_URI,
                column,
                sel,
                new String[]{id},
                null);

        String filePath = "";
        int columnIndex = cursor.getColumnIndex(column[0]);
        if (cursor.moveToFirst()) {
            filePath = cursor.getString(columnIndex);
        }

        cursor.close();
        return filePath;
    }*/
}