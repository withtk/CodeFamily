package com.tkip.mycode.util.lib.retrofit2

import com.google.gson.JsonObject
import okhttp3.RequestBody
import retrofit2.Call
import retrofit2.http.*


interface EsApiService {


    //sending data as a url parameter
//    @GET("/group/$CH_ID/users")
//    fun groupList(@Path("id") groupId: Int): List<User>


    /*********************** sttMapping index ***********************************************************************************/
    @PUT("{index}/")
    fun createIndex(@Path("index") index: String, @Body mapping: RequestBody): Call<JsonObject>

    @DELETE("{index}/")
    fun deleteIndex(@Path("index") index: String ): Call<JsonObject>

    /*********************** total common index ***********************************************************************************/
    @POST("{index}/_doc/{id}/")
    fun addModel(@Path("index") index: String, @Path("id") key: String, @Body data: Any): Call<EsResponseAdd>

    @DELETE("{index}/_doc/{id}/")
    fun delModel(@Path("index") index: String, @Path("id") key: String): Call<EsResponseDel>


    /*********************** sttSystem ********************************************************************************************/
    @GET("{index}/_doc/{id}/")
    fun getSystemManage(@Path("index") index: String, @Path("id") key: String): Call<SystemManageSource>

    /*********************** sttPeer ********************************************************************************************/
    @GET("{index}/_doc/{id}/")
    fun getPeer(@Path("index") index: String, @Path("id") key: String): Call<PeerSource>

    @POST("{index}/_search/")
    fun getPeerList(@Path("index") index: String, @Body query: RequestBody): Call<PeerObject>

    /*********************** sttRecommend ********************************************************************************************/
    @GET("{index}/_doc/{id}/")
    fun getRecommend(@Path("index") index: String, @Path("id") key: String): Call<UidDateSource>

    @POST("{index}/_search/")
    fun getRecommendList(@Path("index") index: String, @Body query: RequestBody): Call<UidDateObject>

    /*********************** sttDevice ********************************************************************************************/
    @GET("{index}/_doc/{id}/")
    fun getDevice(@Path("index") index: String, @Path("id") key: String): Call<DeviceSource>

    @POST("{index}/_search/")
    fun getDeviceList(@Path("index") index: String, @Body query: RequestBody): Call<DeviceObject>

    /*********************** sttMiniPeer ********************************************************************************************/
    @GET("{index}/_doc/{id}/")
    fun getBoardAllow(@Path("index") index: String, @Path("id") key: String): Call<MiniPeerSource>

    @POST("{index}/_search/")
    fun getBoardAllowList(@Path("index") index: String, @Body query: RequestBody): Call<MiniPeerObject>


    /*********************** sttBoard ********************************************************************************************/
    @GET("{index}/_doc/{id}/")
    fun getBoard(@Path("index") index: String, @Path("id") key: String): Call<BoardSource>

    @POST("{index}/_search/")
    fun getBoardList(@Path("index") index: String, @Body query: RequestBody): Call<BoardObject>

    /*********************** sttPost ********************************************************************************************/
    @GET("{index}/_doc/{id}/")
    fun getPost(@Path("index") index: String, @Path("id") key: String): Call<PostSource>

    @POST("{index}/_search/")
    fun getPostList(@Path("index") index: String, @Body query: RequestBody): Call<PostObject>

    /*********************** sttFlea ********************************************************************************************/
    @GET("{index}/_doc/{id}/")
    fun getFlea(@Path("index") index: String, @Path("id") key: String): Call<FleaSource>

    @POST("{index}/_search/")
    fun getFleaList(@Path("index") index: String, @Body query: RequestBody): Call<FleaObject>

    /*********************** sttRoom ********************************************************************************************/
    @GET("{index}/_doc/{id}/")
    fun getRoom(@Path("index") index: String, @Path("id") key: String): Call<RoomSource>

    @POST("{index}/_search/")
    fun getRoomList(@Path("index") index: String, @Body query: RequestBody): Call<RoomObject>

    /*********************** sttReply ********************************************************************************************/
    @GET("{index}/_doc/{id}/")
    fun getReply(@Path("index") index: String, @Path("id") key: String): Call<ReplySource>

    @POST("{index}/_search/")
    fun getReplyList(@Path("index") index: String, @Body query: RequestBody): Call<ReplyObject>

    /*********************** sttLobby ********************************************************************************************/
//    @GET("{index}/_doc/{id}/")
//    fun getLobby(@Path("index") index: String, @Path("id") key: String): Call<LobbySource>
//
//    @POST("{index}/_search/")
//    fun getLobbyList(@Path("index") index: String, @Body query: RequestBody): Call<LobbyObject>

    /*********************** sttStore ********************************************************************************************/
    @GET("{index}/_doc/{id}/")
    fun getStore(@Path("index") index: String, @Path("id") key: String): Call<StoreSource>

    @POST("{index}/_search/")
    fun getStoreList(@Path("index") index: String, @Body query: RequestBody): Call<StoreObject>

    @GET("{index}/_doc/{id}/")
    fun getStoreMenu(@Path("index") index: String, @Path("id") key: String): Call<StoreMenuSource>

    @POST("{index}/_search/")
    fun getStoreMenuList(@Path("index") index: String, @Body query: RequestBody): Call<StoreMenuObject>

    /*********************** sttCount ********************************************************************************************/
    @GET("{index}/_doc/{id}/")
    fun getCount(@Path("index") index: String, @Path("id") key: String): Call<CountSource>

    @POST("{count_index}/_search/")
    fun getCountList(@Path("count_index") groupId: String, @Body query: RequestBody): Call<CountObject>

    /*********************** sttWarn ********************************************************************************************/
    @POST("{index}/_search/")
    fun getWarnList(@Path("index") index: String, @Body query: RequestBody): Call<WarnObject>

    /*********************** sttvCost ********************************************************************************************/
    @GET("{index}/_doc/{id}/")
    fun getCost(@Path("index") index: String, @Path("id") key: String): Call<CostSource>

    @POST("{index}/_search/")
    fun getCostList(@Path("index") index: String, @Body query: RequestBody): Call<CostObject>

    @GET("{index}/_doc/{id}/")
    fun getAct(@Path("index") index: String, @Path("id") key: String): Call<CostSource>

    @POST("{index}/_search/")
    fun getActList(@Path("index") index: String, @Body query: RequestBody): Call<CostObject>

    /*********************** sttTag ********************************************************************************************/
    @GET("{index}/_doc/{id}/")
    fun getMyTag(@Path("index") index: String, @Path("id") id: String): Call<TagSource>

    @POST("{index}/_search/")
    fun getMyTagList(@Path("index") index: String, @Body query: RequestBody): Call<TagObject>

    /*********************** sttBbid ********************************************************************************************/
    @GET("{index}/_doc/{id}/")
    fun getBid(@Path("index") index: String, @Path("id") key: String): Call<BidSource>

    @POST("{index}/_search/")
    fun getBidList(@Path("index") index: String, @Body query: RequestBody): Call<BidObject>

    @GET("{index}/_doc/{id}/")
    fun getBanner(@Path("index") index: String, @Path("id") key: String): Call<BidSource>

    @POST("{index}/_search/")
    fun getBannerList(@Path("index") index: String, @Body query: RequestBody): Call<BidObject>


    /*********************** sttTicket ********************************************************************************************/
    @GET("{index}/_doc/{id}/")
    fun getTicket(@Path("index") index: String, @Path("id") key: String): Call<TicketSource>

    @POST("{index}/_search/")
    fun getTicketList(@Path("index") index: String, @Body query: RequestBody): Call<TicketObject>

    /*********************** sttGrant ********************************************************************************************/
    @GET("{index}/_doc/{id}/")
    fun getGrant(@Path("index") index: String, @Path("id") key: String): Call<GrantSource>

    @POST("{index}/_search/")
    fun getGrantList(@Path("index") index: String, @Body query: RequestBody): Call<GrantObject>

}