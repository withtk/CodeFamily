package com.tkip.mycode.util.lib.calendar

import android.content.Context
import android.view.LayoutInflater
import android.widget.LinearLayout
import androidx.databinding.DataBindingUtil
import com.tkip.mycode.R
import com.tkip.mycode.databinding.CustomCalendarNormalBinding
import com.tkip.mycode.util.tools.date.getDate8
import com.tkip.mycode.util.tools.date.getMilis
import java.util.*


/**
 * 기본 달력
 */
class CustomCalendarNormal : LinearLayout {
    private lateinit var b: CustomCalendarNormalBinding

    constructor(context: Context) : super(context)
    constructor(context: Context, hasLimit: Boolean, onDate: (date8: Int) -> Unit) : super(context) {
        b = DataBindingUtil.inflate(LayoutInflater.from(context), R.layout.custom_calendar_normal, this, true)

        val c = GregorianCalendar()
        if (hasLimit) {

            c.set(Calendar.DAY_OF_MONTH, 1)
            b.cal.minDate = c.timeInMillis    // 달력 시작날짜
            c.add(Calendar.MONTH, 1)
            c.add(Calendar.DAY_OF_MONTH, 30)
            b.cal.maxDate = c.timeInMillis    // 달력 마지막날짜
        }

        b.cal.setOnDateChangeListener { _, y, m, d ->
            onDate(getMilis(y, m + 1, d).getDate8())
        }


    }


}