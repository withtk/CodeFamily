package com.tkip.mycode.util.tools.recycler

import com.tkip.mycode.databinding.LinearNoDataBinding
import com.tkip.mycode.util.tools.anim.hide
import com.tkip.mycode.util.tools.anim.hideFadeOut
import com.tkip.mycode.util.tools.anim.showOverUp
import com.tkip.mycode.util.tools.anim.visi

class OnNoData {

    companion object {

        /**
         * 준비단계
         */
        fun ready(inNoData: LinearNoDataBinding) {
            inNoData.avLoading.visi()
            inNoData.tvRefresh.hide()
            inNoData.tvNoData.hide()
        }

        /**
         * true : Data 있을때
         * 당겨서 새로고침 보이기.
         */
        fun handle(hasData: Boolean, inNoData: LinearNoDataBinding) {
            inNoData.avLoading.hideFadeOut(true)
            if (hasData) {
                inNoData.tvNoData.hideFadeOut(false)
            } else {
                inNoData.tvNoData.showOverUp(true)
            }
        }



        /**
         * true : Data 있을때
         * 당겨서 새로고침 보이기.
         */
        fun handlePull(hasData: Boolean, inNoData: LinearNoDataBinding) {
            inNoData.avLoading.hideFadeOut(true)
            if (hasData) {
                inNoData.tvRefresh.hideFadeOut(false)
                inNoData.tvNoData.hideFadeOut(false)
            } else {
                inNoData.tvRefresh.showOverUp(true)
                inNoData.tvNoData.showOverUp(true)
            }
        }





    }

}