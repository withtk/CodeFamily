package com.tkip.mycode.util.lib.photo.saf

import android.content.Context
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.graphics.Matrix
import android.net.Uri
import android.os.Environment
import androidx.core.content.FileProvider
import androidx.exifinterface.media.ExifInterface
import com.tkip.mycode.funs.common.lllogI
import com.tkip.mycode.init.WIDTH_MID
import com.tkip.mycode.init.WIDTH_THUMB
import com.tkip.mycode.util.lib.photo.util.ExifUtil
import com.tkip.mycode.util.lib.photo.util.PhotoUtil
import java.io.File
import java.io.FileNotFoundException
import java.io.FileOutputStream
import java.io.OutputStream


/**
 * 이미지 변환
 */
class OnSAFutil {
    companion object {

        /**
         */
        fun getUriHandled(context: Context, uri: Uri, targetSize: Int): Uri {
//            var resizeBitmap: Bitmap? = null

//            val options = BitmapFactory.Options()
            try {
//                val bbbb = BitmapFactory.decodeStream(context.contentResolver.openInputStream(uri), null, options) // 1번

//                val orientation = ExifUtil.getOrientationFromUri(context, uri)
//                resizeBitmap = getBitmapRotated(context, uri, bbbb!!)
//                val finalBitmap = myScale(bbbb!!, targetSize).myRotate(orientation)

//                BitmapFactory.decodeStream(context.contentResolver.openInputStream(uri), null, options)
//                val originWidth = options.outWidth
//                val originHeight = options.outHeight
                return newUri(context, getBitmapResizedRotated(context, uri, targetSize)!!, targetSize)
            } catch (e: FileNotFoundException) {
                e.printStackTrace()
                return uri
            }
        }

        /*** 이미지 Orientation */
        private fun getBitmapRotated(context: Context, uri: Uri, bitmap: Bitmap): Bitmap {
            val orientation = ExifUtil.getOrientationFromUri(context, uri)
            return ExifUtil.getRotatedBitmap(bitmap, orientation)
        }


        /************ Bitmap 사이즈 재설정 **********************************/
        private fun getAnotherSize(high: Int, low: Int, target: Int) = ((high.toDouble() * target.toDouble()) / low.toDouble()).toInt()

        private fun myScale(bitmap: Bitmap, targetSize: Int): Bitmap {
            var bitmap1 = bitmap
            var targetW = bitmap1.width
            var targetH = bitmap1.height
            if (bitmap1.width > bitmap1.height) { // 세로 사진.
                if (bitmap1.width > targetSize) {  // 원본사진 줄여야 할 때
                    targetW = targetSize
                    targetH = getAnotherSize(bitmap1.width, bitmap1.height, targetSize)
                }
            } else {  // 가로 사진
                if (bitmap1.height > targetSize) { // 원본사진 줄여야 할 때
                    targetW = getAnotherSize(bitmap1.width, bitmap1.height, targetSize)
                    targetH = targetSize
                }
            }

            lllogI("AsyncUploadUriSAF getBitmapRotationScale targetW $targetW")
            lllogI("AsyncUploadUriSAF getBitmapRotationScale targetH $targetH")

            bitmap1 = Bitmap.createScaledBitmap(bitmap1, targetW, targetH, true)
            return bitmap1
        }


        private fun getHigh(a: Int, b: Int) = if (a > b) a else b
        private fun getBitmapResizedRotated(context: Context, uri: Uri, targetSize: Int): Bitmap? {
            var resizeBitmap: Bitmap? = null

            val options = BitmapFactory.Options()
            try {

                BitmapFactory.decodeStream(context.contentResolver.openInputStream(uri), null, options) // 1번

                var width = options.outWidth
                var height = options.outHeight
//                var sampleSize = 1
                val sampleSize =
                        if (getHigh(width, height) > targetSize) {  // 사진 잘라야 할 때
                            when (targetSize) {
                                WIDTH_THUMB -> 4
                                WIDTH_MID -> 2
                                else -> 1
                            }
                        } else 1

//                if (getHigh(width, height) > targetSize) {
//                    val resize2 = 300
//                    while (true) {//2번
//                        if (width / 2 < resize2 || height / 2 < resize2)
//                            break
//                        width /= 2
//                        height /= 2
//                        sampleSize *= 2
//                    }
//                }


                lllogI("dsfsdfs getBitmapResized sampleSize $sampleSize")
                options.inSampleSize = sampleSize

                val bitmap = BitmapFactory.decodeStream(context.contentResolver.openInputStream(uri), null, options) //3번

                /*** 이미지 회전 */
                resizeBitmap = bitmap?.myRotate(ExifUtil.getOrientationFromUri(context, uri))

            } catch (e: FileNotFoundException) {
                e.printStackTrace()
            }
            return resizeBitmap
        }


        /*** Bitmap 회전시키기 */
        private fun Bitmap.myRotate(orientation: Int): Bitmap {

            val matrix = Matrix()
            when (orientation) {
                ExifInterface.ORIENTATION_FLIP_HORIZONTAL -> matrix.setScale(-1f, 1f)
                ExifInterface.ORIENTATION_ROTATE_180 -> matrix.setRotate(180f)
                ExifInterface.ORIENTATION_FLIP_VERTICAL -> {
                    matrix.setRotate(180f)
                    matrix.postScale(-1f, 1f)
                }
                ExifInterface.ORIENTATION_TRANSPOSE -> {
                    matrix.setRotate(90f)
                    matrix.postScale(-1f, 1f)
                }
                ExifInterface.ORIENTATION_ROTATE_90 -> matrix.setRotate(90f)
                ExifInterface.ORIENTATION_TRANSVERSE -> {
                    matrix.setRotate(-90f)
                    matrix.postScale(-1f, 1f)
                }
                ExifInterface.ORIENTATION_ROTATE_270 -> matrix.setRotate(-90f)
                else -> return this
            }

            try {
                val finalBitmap = Bitmap.createBitmap(this, 0, 0, this.width, this.height, matrix, true)
                this.recycle()
                return finalBitmap
            } catch (e: OutOfMemoryError) {
                e.printStackTrace()
                return this
            }
        }

        /*** file생성하여 새로운 Uri 만들기 */
        fun newUri(context: Context, inputBitmap: Bitmap, targetSize: Int): Uri {
            var bitmap = inputBitmap

            var file: File? = null
            var out: OutputStream? = null
            try {
                file = File.createTempFile(
                        "Phil_" + System.currentTimeMillis(),
                        ".jpg",
                        context.getExternalFilesDir(Environment.DIRECTORY_PICTURES)
                )
                file.deleteOnExit()
                out = FileOutputStream(file!!)

                bitmap.compress(Bitmap.CompressFormat.JPEG, 100, out)
                out.close()
            } catch (e: Exception) {
                e.printStackTrace()
            }

            PhotoUtil.addDeleteFile(file!!)
            return FileProvider.getUriForFile(context, context.applicationContext.packageName, file)
        }


        /**         * photo 사이즈 방향 : 세로인가?  :  세로이면 width를 바꿔야 함.         */
        private fun isPortrate(width: Int, height: Int) = width > height

//        private fun getPairSize(w: Int, h: Int, t: Int): Pair<Int, Int> {
//            return if (isPortrate(w, h)) Pair(t, getAnotherSize(w, h, t))
//            else Pair(t, getAnotherSize(h, w, t))
//        }


        /**         * 바뀔 사이즈 수치 받아오기. : width or height         */
//        private fun getReSizeAmount(originSize: Int, targetSize: Int) = if (originSize > targetSize) targetSize else originSize


        /**         * 바꿀 기준이 되는 width인지 height인지  :  return = 값         */
//        private fun getTargetSize(width: Int, height: Int, targetWidth: Int, targetHeight: Int) = if (isPortrate(width, height)) targetWidth else targetHeight


//        private fun getFinalBitmap(context: Context, bitmap: Bitmap, originWidth: Int, originHeight: Int): Bitmap {
//            var targetW = originWidth
//            var targetH = originHeight
//            if (isPortrate(originWidth, originHeight)) targetW = 400
//            else targetH = 400
//
//            return Bitmap.createScaledBitmap(bitmap, targetW, targetH, true)
//        }


//######################################### test ##########################################


//
//        private fun getBitmapResized(uri: Uri, resize: Int): Bitmap? {
//            var resizeBitmap: Bitmap? = null
//
//            val options = BitmapFactory.Options()
//            try {
//
//                BitmapFactory.decodeStream(wActivity.get()!!.contentResolver.openInputStream(uri), null, options) // 1번
//
//                var width = options.outWidth
//                var height = options.outHeight
//                var sampleSize = 1
//
//                while (true) {//2번
//                    if (width / 2 < resize || height / 2 < resize)
//                        break
//                    width /= 2
//                    height /= 2
//                    sampleSize *= 2
//                }
//                options.inSampleSize = sampleSize
//
//                val bitmap = BitmapFactory.decodeStream(wActivity.get()!!.contentResolver.openInputStream(uri), null, options) //3번
//
//                /*** 이미지 회전 */
//                val orientation = ExifUtil.getOrientationFromUri(wActivity.get()!!, uri)  //uri
//                resizeBitmap=   ExifUtil.getRotatedBitmap(bitmap!!, orientation)
//
//            } catch (e: FileNotFoundException) {
//                e.printStackTrace()
//            }
//            return resizeBitmap
//        }


//        @Throws(FileNotFoundException::class, IOException::class)
//        fun getBitmapFormUriTest(ac: Activity, uri: Uri): Bitmap? {
//
//            var input = ac.contentResolver.openInputStream(uri)
//            val options = BitmapFactory.Options().apply {
//                inJustDecodeBounds = true
//                inPreferredConfig = Bitmap.Config.ARGB_8888 //optional
//            }
//            BitmapFactory.decodeStream(input, null, options)
//            input?.close()
//
//
//            val width = options.outWidth
//            val height = options.outHeight
//            if (width == -1 || height == -1) return null
//            val ww = 480f // 여기서 너비를 480f
//            val hh = 800f // 여기서 높이는 800f로 설정
//            // 확대 비율 로 설정합니다 . 고정 스케일이므로 높이 또는 너비의 데이터 하나만 계산에 사용됩니다.
//            var be = 1 // be = 1은
//
//            if (width > height && width > ww) be = (width / ww).toInt()
//            /** 너비가 클 경우 스케일링 이 없음을 의미합니다 . 너비의 고정 된 크기에 따른 스케일*/
//            else if (width < height && height > hh) be = (height / hh).toInt()
//            /** 높이가 높으면 너비의 고정 된 크기에 따라 배율을 조정*/
//
//
//            if (be <= 0) be = 1
//            // 비례 압축
//            val bitmapOptions = BitmapFactory.Options().apply {
//                inSampleSize = be // 스케일링 설정
//                inPreferredConfig = Bitmap.Config.ARGB_8888  // 선택적
//            }
//            input = ac.contentResolver.openInputStream(uri)
//            val bitmap = BitmapFactory.decodeStream(input, null, bitmapOptions)
//            input!!.close()
//            return compressImage(bitmap) // 다시 압축 다시
//        }


//        public fun compressImage(image: Bitmap): Bitmap {
//
//            val output = ByteArrayOutputStream()
//            image.compress(Bitmap.CompressFormat.JPEG, 100, output) // 품질 압축 방법, 여기서 100은 압축이 없음을 의미하며 압축 된 데이터를 BIOS에 저장합니다
//
//            var options = 100
//            while (output.toByteArray().size / 1024 > 100) {// 압축 된 이미지가 100kb보다 큰지, 압축을 계속하는 것보다 큰지 확인하는주기
//                output.reset() // BIOS를 재설정하여 지우십시오
//                // 첫 번째 매개 변수 : 사진 형식, 두 번째 매개 변수 : 사진 품질, 100이 가장 높고, 0이 가장 최악, 세 번째 매개 변수 : 압축 된 데이터 스트림 저장
//                image.compress(Bitmap.CompressFormat.JPEG, options, output) // Here 압축 옵션은 압축 된 데이터를 BIOS
//                options -= 10 // 10 매번
//            }
//
//            val input = ByteArrayInputStream(output.toByteArray()) // 압축 된 데이터를 ByteArrayInputStream에 저장
//            val bitmap = BitmapFactory.decodeStream(input, null, null) // ByteArrayInputStream 데이터에서 이미지 생성
//            return bitmap
//        }


    }
}