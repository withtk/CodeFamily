package com.tkip.mycode.util.tools.fire

import android.util.Log
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.database.ValueEventListener
import com.tkip.mycode.dialog.progress.MyProgress
import com.tkip.mycode.funs.common.OnException
import com.tkip.mycode.funs.common.lllogI
import com.tkip.mycode.init.CH_STATUS
import com.tkip.mycode.init.base.Cons
import com.tkip.mycode.model.my_enum.Status
import com.tkip.mycode.model.peer.OnPeer
import com.tkip.mycode.model.post.Post
import com.tkip.mycode.model.store.Store


class FireReal {

    companion object {

//        private fun getChildString(children: Array<String>): String {
//            var myChild = ""
//            for (c in children) {
//                myChild += "/$c"
//            }
//            return myChild
//        }

//        fun HashMap<String, Any>.setChildValue(children: Array<String>,value : HashMap<String,Any>){
//            this[children.getChildString()] =value
//        }

        val ref by lazy {  FirebaseDatabase.getInstance().reference}

        /**********************************************************
         * <common> 전체 받아오기
         *********************************************************/
        fun getAll(child: String, onSuccess: (DataSnapshot?) -> Unit, onFail: () -> Unit) {
            ref .child(child)
                    .addListenerForSingleValueEvent(object : ValueEventListener {
                        override fun onDataChange(dataSnapshot: DataSnapshot) {
                            lllogI("FirebaseDatabase getAll onDataChange childrenCount:${dataSnapshot.childrenCount}")
                            onSuccess(dataSnapshot)
                        }

                        override fun onCancelled(databaseError: DatabaseError) {
                            lllogI("FirebaseDatabase getAll onCancelled")
                            onFail()
                        }
                    })
        }

        fun delete(child: String, onSuccess: () -> Unit, onFail: () -> Unit) {
            ref.child(child).removeValue().addOnSuccessListener { onSuccess() }.addOnFailureListener{onFail()}
        }

        /**********************************************************
         * <common> 1개 처리
         *********************************************************/
        fun commonADD(children: Array<String>, any: Any, success: (DataSnapshot?) -> Unit, fail: (Exception) -> Unit) {
            ref.child(children.getChildString())
                    .setValue(any)
                    .addOnSuccessListener {
                        success(null)
                        Log.d(Cons.TAG, "commonADD : ${children.contentToString()} : success add! ")
                    }
                    .addOnFailureListener {
                        fail(it)
                        MyProgress.cancel()
                        OnException.dbfailed("commonADD : ${children.contentToString()} fail: ", it)
                    }

        }

        fun commonGET(children: Array<String>, success: (DataSnapshot?) -> Unit, fail: (DatabaseError) -> Unit) {
            ref.child(children.getChildString())
                    .addListenerForSingleValueEvent(object : ValueEventListener {
                        override fun onDataChange(dataSnapshot: DataSnapshot) {
                            Log.d(Cons.TAG, "commonGET : ${children.contentToString()} : success get! ")
                            if (dataSnapshot.exists()) success(dataSnapshot)
                            else success(null)
                        }

                        override fun onCancelled(e: DatabaseError) {
                            OnException.dbfailed("commonGET : ${children.contentToString()} fail: ", e.toException())
                            fail(e)
                        }
                    })
        }

        fun commonDEL(children: Array<String>, success: () -> Unit, fail: (Exception) -> Unit) {
            ref.child(children.getChildString())
                    .removeValue()
                    .addOnSuccessListener {
                        Log.d(Cons.TAG, "${children.contentToString()} : commonDEL success delete!")
                        success()
//                                MyProgress.cancel()
                    }.addOnFailureListener { e ->
                        MyProgress.cancel()
                        OnException.dbfailed("${children.contentToString()} commonDEL fail:", e)
                        fail(e)
                    }
        }

        fun batchUPDATE(childUpdates: HashMap<String, Any>, success: () -> Unit, fail: (Exception) -> Unit) {
            ref
                    .updateChildren(childUpdates)
                    .addOnSuccessListener {
                        Log.d(Cons.TAG, "batchUPDATE success size : ${childUpdates.size}")
                        success()
                    }
                    .addOnFailureListener {
                        MyProgress.cancel()
                        OnException.dbfailed("batchUPDATE fail:", it)
                        fail(it)
                    }
        }


        /**********************************************************
         * <common> 실시간 리슨
         *********************************************************/
        fun commonRealTime(children: Array<String>, success: (DataSnapshot?) -> Unit, fail: (Exception) -> Unit) {
            ref.child(children.getChildString())
                    .addListenerForSingleValueEvent(object : ValueEventListener {
                        override fun onCancelled(e: DatabaseError) {
                            OnException.dbfailed("commonValueListener : ${children.contentToString()} fail: ", e.toException())
                            fail(e.toException())
                        }

                        override fun onDataChange(dataSnapshot: DataSnapshot) {
                            success(dataSnapshot)
                            if (dataSnapshot.exists()) success(dataSnapshot)
                            else success(null)
                        }
                    })
        }

        fun getValueEventListener(methodName: String, success: (DataSnapshot?) -> Unit, fail: (Exception) -> Unit) =
                object : ValueEventListener {
                    override fun onDataChange(dataSnapshot: DataSnapshot) {
                        Log.d(Cons.TAG, "$methodName getValueEventListener : success dataSnapshot.childrenCount : ${dataSnapshot.childrenCount}")
                        if (dataSnapshot.childrenCount < 1) success(null)
                        else success(dataSnapshot)
                    }

                    override fun onCancelled(databaseError: DatabaseError) {
                        Log.d(Cons.TAG, "$methodName getValueEventListener fail : ${databaseError.toException()}")
                        fail(databaseError.toException())
                    }
                }


        /**
         *  <common> batch Status 수정 모든 model에 적용가능.
         */
        fun commonUpdateStatus(children: Array<String>, status: Status, success: () -> Unit, fail: (Exception) -> Unit) {
            val childUpdates = hashMapOf<String, Any>()
            childUpdates.setChildValue(children, hashMapOf(CH_STATUS to status))
            batchUPDATE(childUpdates, { success() }, { fail(it) })
        }


        /*****************************************************************************************
         *****************************************************************************************
         *****************************************************************************************
         *****************************************************************************************
         *****************************************************************************************
         *****************************************************************************************
         *****************************************************************************************/


        fun addStoreTag(store: Store, success: () -> Unit, fail: () -> Unit) {
            if (OnPeer.isNotAdmin(true)) return

        }


        /******************** sttCost   *************************************************/
//        fun addCost111(cost: Cost, success: () -> Unit, fail: () -> Unit) {
//            val childUpdates = hashMapOf<String, Any>()
//            childUpdates.setChildValue(arrayOf(CH_COST, OnPeer.myUid(), cost.key), cost.toMap())
//            batchUPDATE(childUpdates, { success() }, { fail() })
//        }


        /******************** sttTag   *************************************************/
//        fun addMyTag(child: String, myTag: MyTag, success: () -> Unit, fail: (Exception) -> Unit) {
//            commonADD(arrayOf(child,myTag.key), myTag, success = { success() }, fail = { fail(it) })
//        }


    }
}