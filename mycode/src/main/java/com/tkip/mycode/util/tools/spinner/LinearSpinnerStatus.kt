package com.tkip.mycode.util.tools.spinner

import android.content.Context
import android.view.LayoutInflater
import android.widget.LinearLayout
import androidx.databinding.DataBindingUtil
import com.tkip.mycode.R
import com.tkip.mycode.databinding.ViewSpinnerCommonBinding
import com.tkip.mycode.funs.common.lllogI
import com.tkip.mycode.funs.common.onItemSelected
import com.tkip.mycode.model.my_enum.Status


/**
 *  spinner for STATUS
 */
class LinearSpinnerStatus : LinearLayout {
    private lateinit var b: ViewSpinnerCommonBinding
    private val items: ArrayList<Status?> = arrayListOf()    // 첫번째는 status가 아니다.
    var selectedP: Int = 0

    constructor(context: Context) : super(context)
    constructor(context: Context, isTitle: Boolean, list: ArrayList<Status>, initStatusNo: Int?, onClick: (s: Status) -> Unit) : super(context) {
        b = DataBindingUtil.inflate(LayoutInflater.from(context), R.layout.view_spinner_common, this, true)

        items.addAll(list)
        items.forEach { lllogI("LinearSpinnerStatus items status: ${it?.title}") }

        b.spin.adapter = SpinnerStatusAdapter(isTitle, items)
        b.spin.onItemSelected(
                onNothin = {},
                onSelected = { _, _, p, _ ->
                    selectedP = p
//                    if (p > 0)
                        onClick(items[p]!!)
                })

        lllogI("LinearSpinnerStatus initStatusNo $initStatusNo")
        initStatusNo?.let { b.spin.setSelection(getPosition(it)) }

    }

    private fun getPosition(statusNo: Int): Int {
        items.forEachIndexed { i, it ->
            it?.let { status ->
                if (statusNo == status.no) {
                    lllogI("LinearSpinnerStatus i $i status $status")
                    return i
                }
            }
        }
        return 0
    }


}