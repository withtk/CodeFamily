package com.tkip.mycode.nav.board

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.RecyclerView
import com.tkip.mycode.R
import com.tkip.mycode.databinding.HolderTopFotoListBinding
import com.tkip.mycode.util.lib.photo.Foto
import com.tkip.mycode.util.lib.photo.photoview.OnPhotoView
import com.tkip.mycode.funs.common.tranPair

/**
 * 1. posting 상단에 올라갈 rv
 */
class TopFotoAdapter(val items: ArrayList<Foto>) : RecyclerView.Adapter<TopFotoAdapter.TopFotoViewHolder>() {
    private lateinit var activity: AppCompatActivity

    override fun getItemId(position: Int): Long {
        return items[position].hashCode().toLong()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): TopFotoViewHolder {
        this.activity = parent.context as AppCompatActivity
        val binding = HolderTopFotoListBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return TopFotoViewHolder(binding)
    }

    override fun onBindViewHolder(h: TopFotoViewHolder, p: Int) {
        h.bind(items[p])
    }

    override fun getItemCount(): Int {
        return items.size
    }

    inner class TopFotoViewHolder(val b: HolderTopFotoListBinding) : RecyclerView.ViewHolder(b.root) {

        lateinit var foto: Foto

        fun bind(foto: Foto) {
            this.foto = foto
            b.holder = this
            b.foto = foto

            b.executePendingBindings()
        }

        fun onClickHolder(){
            OnPhotoView.gotoPhotoViewActivity(activity, adapterPosition,items, arrayOf(b.ivFace.tranPair(R.string.tran_image)))
        }

    }

}