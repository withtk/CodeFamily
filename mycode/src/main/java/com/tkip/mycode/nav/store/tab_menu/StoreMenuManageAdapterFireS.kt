package com.tkip.mycode.nav.store.tab_menu

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.firebase.ui.firestore.FirestoreRecyclerAdapter
import com.firebase.ui.firestore.FirestoreRecyclerOptions
import com.google.firebase.firestore.FirebaseFirestoreException
import com.tkip.mycode.R
import com.tkip.mycode.databinding.HolderStoreMenuManageBinding
import com.tkip.mycode.dialog.alert.MyAlert
import com.tkip.mycode.dialog.toast.toastNormal
import com.tkip.mycode.init.base.getStr
import com.tkip.mycode.model.store.StoreMenu
import com.tkip.mycode.util.tools.anim.showOverUp
import com.tkip.mycode.util.tools.etc.OnMyClick
import com.tkip.mycode.util.tools.fire.Firee


/**
 * actionType - 일반유저  or  수정하는 유저
 */

class StoreMenuManageAdapterFireS(options: FirestoreRecyclerOptions<StoreMenu>, val onUpdate: (StoreMenu) -> Unit) : FirestoreRecyclerAdapter<StoreMenu, StoreMenuManageAdapterFireS.StoreMenuManageViewHolder>(options)   {
    private lateinit var context: Context

    override fun getItemId(position: Int): Long {
        return getItem(position).hashCode().toLong()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): StoreMenuManageViewHolder {
        context = parent.context
        return StoreMenuManageViewHolder(HolderStoreMenuManageBinding.inflate(LayoutInflater.from(context), parent, false))
    }


    override fun onBindViewHolder(h: StoreMenuManageViewHolder, p: Int, m: StoreMenu) {
        h.bind(m)
    }


    override fun onDataChanged() {
        super.onDataChanged()
    }

    override fun onError(e: FirebaseFirestoreException) {
        super.onError(e)
    }

//class StoreMenuManageAdapter(val items: ArrayList<StoreMenu>) : RecyclerView.Adapter<StoreMenuManageAdapter.StoreMenuManageViewHolder>() {
//    private lateinit var context: Context
//
//    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): StoreMenuManageViewHolder {
//        context = parent.context
//        val binding = HolderStoreMenuManageBinding.inflate(LayoutInflater.from(parent.context), parent, false)
//        return StoreMenuManageViewHolder(binding)
//    }
//
//    override fun getItemId(position: Int): Long {
//        return items[position].hashCode().toLong()
//    }
//
//    override fun onBindViewHolder(h: StoreMenuManageViewHolder, p: Int) {
//        h.bind(items[p])
//    }
//
//    override fun getItemCount(): Int {
//        return items.size
//    }

    inner class StoreMenuManageViewHolder(val b: HolderStoreMenuManageBinding) : RecyclerView.ViewHolder(b.root) {

        private lateinit var storeMenu: StoreMenu

        fun bind(storeMenu: StoreMenu) {
            this.storeMenu = storeMenu
            b.holder = this
            b.storeMenu = storeMenu
            b.executePendingBindings()

            b.line.showOverUp(true)
//            Handler().postDelayed({
//                b.line.showOverUp(true)
//            }, 500)
        }

        fun onClickCard() {
            if (OnMyClick.isDoubleClick()) return
            toastNormal("StoreMenuViewHolder onClickCard ")

//            if (OnPeer.isAdmin(false) || OnPeer.isSamePeer(store.hostUID) || actionType == Cons.ACTION_TYPE_NEW || actionType == Cons.ACTION_TYPE_UPDATE || actionType == Cons.ACTION_TYPE_INSPECTION) {
//                if (b.swiper.isClosed) b.swiper.open(true)
//                else b.swiper.close(true)
//            }
        }

        fun onClickIv(v: View) {
            OnMyClick.setDisableAWhileBTN(v)
            MyAlert.showPhoto(context,storeMenu.photoThumb)
        }
        fun onClickUpdate(v: View) {
            OnMyClick.setDisableAWhileBTN(v)
            onUpdate(storeMenu)
        }

        fun onClickDel(v: View) {
            OnMyClick.setDisableAWhileBTN(v)

            MyAlert.showConfirm(context, R.string.info_delete_store_menu.getStr(),
                    ok = {
                        Firee.deleteStoreMenu(storeMenu,
                                success = {

                                }, fail = {}
                        )

                    }, cancel = {})

        }
    }

}