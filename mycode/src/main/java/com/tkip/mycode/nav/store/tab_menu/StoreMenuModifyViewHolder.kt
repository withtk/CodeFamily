package com.tkip.mycode.nav.store.tab_menu

import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.RecyclerView
import com.chauthai.swipereveallayout.ViewBinderHelper
import com.tkip.mycode.databinding.HolderStoreMenuModifyBinding
import com.tkip.mycode.dialog.alert.MyAlert
import com.tkip.mycode.model.store.Store
import com.tkip.mycode.model.store.StoreMenu
import com.tkip.mycode.model.my_enum.Status
import com.tkip.mycode.util.tools.fire.Firee
import com.tkip.mycode.model.peer.OnPeer
import com.tkip.mycode.init.base.Cons
import com.tkip.mycode.util.tools.etc.OnMyClick

class StoreMenuModifyViewHolder(
        val activity: AppCompatActivity,
        val adapter: RecyclerView.Adapter<RecyclerView.ViewHolder>,
        private val binderHelper: ViewBinderHelper,
        val actionType: Int,
        val store: Store,
        val items: ArrayList<StoreMenu>?,
        val b: HolderStoreMenuModifyBinding
) : RecyclerView.ViewHolder(b.root) {

    private lateinit var storeMenu: StoreMenu

    fun bind(storeMenu: StoreMenu) {
        this.storeMenu = storeMenu
        b.holder = this
        b.actionType = actionType
        b.store = store
        b.storeMenu = storeMenu
        b.executePendingBindings()

//
//        val spinnerStatusItems = OnSpinner.getStatusList()
//        b.spinnerStatus.adapter = SpinnerStatusAdapter( spinnerStatusItems)
//        var check = 0
//        b.spinnerStatus.setListener(
//                onItemSelected = {
//                    if (++check > 1 && OnPeer.isAdmin(false)) {
//                        OnSpinner.updateStoreMenuStatusDialog(activity, spinnerStatusItems[it], storeMenu, {}, deny = { adapter.notifyDataSetChanged() })
//                    }
//                }, onNothingSelected = {})
//        b.spinnerStatus.setSelection(storeMenu.statusNo.getStatus().getPosition(spinnerStatusItems))
//
//        OnSpinner.setWorking(b.spinnerStatus)
//
//        binderHelper.bind(b.swiper, storeMenu.key)
//        binderHelper.setOpenOnlyOne(true)
//
//        // swipe 작동 여부.
////            if (OnPeer.isNotAdmin(false)) {
////                if (storeMenu.statusNo.lessThan(Status.INSPECTING)) b.swiper.setLockDrag(true)
////            }
//
//        OnVisible.setSwipeHeight(b.line, b.lineBottom)   // height 맞추기
    }

    fun onClickCard() {
        if (OnPeer.isAdmin(false) || OnPeer.isMyPeer(store.hostUID) || actionType == Cons.ACTION_TYPE_NEW || actionType == Cons.ACTION_TYPE_UPDATE || actionType == Cons.ACTION_TYPE_INSPECTION) {
            if (b.swiper.isClosed) b.swiper.open(true)
            else b.swiper.close(true)
        }
    }

//        fun onLongClick(): Boolean {
//            /** 딥삭제 */
//            return if (OnPeer.isAdminSupervisor(false)) {
//                ConfirmDialog.newInstance(title = null, desc = R.string.desc_deep_delete_basic.getStr(), isCancel = true, isCancelTouch = true,
//                        ok = { Firee.deleteStoreMenu(storeMenu, {}, {}) }, deny = {})
//                        .show(activity.supportFragmentManager, "dialog")
//                true
//            } else false
//        }

    fun onClickDel() {
        if (OnMyClick.isDoubleClick()) return
        MyAlert.showConfirm(activity, Status.DELETED_USER.dialogMsg, {
                    Firee.deleteStoreMenu(storeMenu,
                            success = {
                                items?.let {
                                    it.remove(storeMenu)
                                    adapter.notifyDataSetChanged()
                                }
                            }, fail = {})
        }, {})

    }

    fun onClickGo() {
        if (OnMyClick.isDoubleClick()) return
        // todo : 리플라이 볼 수 있는 페이지로 이동. 다이얼로그로 or 액티비티
    }

}