package com.tkip.mycode.nav.lobby.chat

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.RecyclerView
import com.firebase.ui.firestore.FirestoreRecyclerAdapter
import com.firebase.ui.firestore.FirestoreRecyclerOptions
import com.google.firebase.firestore.FirebaseFirestoreException
import com.tkip.mycode.databinding.HolderChatMyBinding
import com.tkip.mycode.databinding.HolderChatUserBinding
import com.tkip.mycode.funs.common.OnException
import com.tkip.mycode.model.peer.OnPeer
import com.tkip.mycode.model.peer.Peer
import com.tkip.mycode.model.toss.OnToss
import com.tkip.mycode.util.lib.photo.Foto
import com.tkip.mycode.util.lib.photo.photoview.OnPhotoView
import com.tkip.mycode.util.tools.fire.Firee

class ChatAdapterFireS(options: FirestoreRecyclerOptions<Chat>, val onCreateReReply: (Chat, Int) -> Unit) : FirestoreRecyclerAdapter<Chat, RecyclerView.ViewHolder>(options) {
    private lateinit var activity: AppCompatActivity
    private val VIEW_REPLY_MY = 10
    private val VIEW_REPLY_USER = 20

    override fun onDataChanged() {
        super.onDataChanged()
    }

    override fun onError(e: FirebaseFirestoreException) {
        super.onError(e)
    }

    override fun getItemViewType(position: Int): Int {
        return if (OnPeer.isMyPeer(getItem(position).authorUID)) VIEW_REPLY_MY
        else VIEW_REPLY_USER
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        this.activity = parent.context as AppCompatActivity
        return when (viewType) {
            VIEW_REPLY_USER -> ChatUserViewHolder(HolderChatUserBinding.inflate(LayoutInflater.from(parent.context), parent, false))
            VIEW_REPLY_MY -> ChatMyViewHolder(HolderChatMyBinding.inflate(LayoutInflater.from(parent.context), parent, false))
            else -> ChatUserViewHolder(HolderChatUserBinding.inflate(LayoutInflater.from(parent.context)))
        }
    }

    override fun onBindViewHolder(h: RecyclerView.ViewHolder, p: Int, m: Chat) {

        val preChat = if (p == 0) null else getItem(p - 1)  //이전 Chat 세팅.

        if (preChat == null || preChat.authorUID != m.authorUID) Firee.getPeer(m.authorUID, { bind(h, m, preChat, isShow = true, currentPeer = it) }, {})     //1.첫번째 2.상위와 다른 peer
        else bind(h, m, preChat, isShow = false, currentPeer = null) // 상위와 같은 peer


    }

    private fun bind(h: RecyclerView.ViewHolder, m: Chat, preChat: Chat?, isShow: Boolean, currentPeer: Peer?) {
        when (h) {
            is ChatUserViewHolder -> h.bind(m, preChat, isShow, currentPeer)
            is ChatMyViewHolder -> h.bind(m, preChat)
            else -> OnException.forceException("onBindViewHolder in chatAdapterFireS (chat key:${m.key})")
        }
    }

    /**     * rereply의 chat 받아오기     */
    private fun getEarlier(earlierKey: String): Pair<Chat, Int>? {
        snapshots.forEachIndexed { i, chat -> if (chat.key == earlierKey) return Pair(chat, i) }
        return null
    }

    private fun commonOnClickIv(chat: Chat) {
        chat.url?.let { OnPhotoView.gotoPhotoViewActivity(activity, Foto(chat.thumbUrl, chat.url), null) }
    }


    /**************************************************************
     ********************** User Viewholder *********************
     **************************************************************/
    inner class ChatUserViewHolder(val b: HolderChatUserBinding) : RecyclerView.ViewHolder(b.root) {

        private lateinit var chat: Chat

        fun bind(chat: Chat, preChat: Chat?, isShowPeer: Boolean, currentPeer: Peer?) {
            this.chat = chat
            b.holder = this
            b.chat = chat
            b.preChat = preChat
            b.isShowPeer = isShowPeer
            b.peer = currentPeer


            /**  toss 첨부 */
            chat.tossModelKey?.let {
                OnToss.attachToss(activity, b.lineAttach, chat.tossModelKey, chat.tossDataTypeNo)
            }
//                ViewUtil.addViewTossModel(activity, b.lineAttach, null, chat, onTossClick = { }, onClose = {})
//            chat.toss?.let { commonAddToss(it, b.lineToss, b.lineToss) }


            b.executePendingBindings()
        }

        fun onClickCard() {

        }

        fun onClickIv() {
            commonOnClickIv(chat)
        }

        fun onLongClickChat(): Boolean {
            OnChat.showMenuDialog(activity, chat,
                    onCreateReReply = { onCreateReReply(chat, adapterPosition) },
                    onDeleted = { status ->
                        chat.statusNo = status.no
                        notifyItemChanged(adapterPosition)
                    })
            return true
        }

    }

    /**************************************************************
     ********************** User Viewholder *********************
     **************************************************************/
    inner class ChatMyViewHolder(val b: HolderChatMyBinding) : RecyclerView.ViewHolder(b.root) {

        private lateinit var chat: Chat

        fun bind(chat: Chat, preChat: Chat?) {
            this.chat = chat
            b.holder = this
            b.chat = chat
            b.preChat = preChat


            /**  toss 첨부 */
            chat.tossModelKey?.let {
                OnToss.attachToss(activity, b.lineAttach, chat.tossModelKey, chat.tossDataTypeNo)
            }
//                ViewUtil.addViewTossModel(activity, b.lineAttach, null, chat, onTossClick = { }, onClose = {})
//            chat.toss?.let { commonAddToss(it, b.lineToss, b.lineToss) }

            b.executePendingBindings()
        }

        fun onClickCard() {

        }

        fun onClickIv() {
            commonOnClickIv(chat)
        }

        fun onLongClickChat(): Boolean {
            OnChat.showMenuDialog(activity, chat,
                    onCreateReReply = { onCreateReReply(chat, adapterPosition) },
                    onDeleted = { status ->
                        chat.statusNo = status.no
                        notifyItemChanged(adapterPosition)
                    })
            return true
        }

    }


}
