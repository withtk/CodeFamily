package com.tkip.mycode.nav.board

import android.os.Bundle
import android.util.Log
import android.view.Menu
import android.view.MenuInflater
import android.view.MenuItem
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.size
import androidx.fragment.app.Fragment
import com.tkip.mycode.R
import com.tkip.mycode.databinding.FragmentMainBoardBinding
import com.tkip.mycode.funs.common.lllogM
import com.tkip.mycode.funs.common.onPageChange
import com.tkip.mycode.init.*
import com.tkip.mycode.init.base.Cons
import com.tkip.mycode.init.base.getStr
import com.tkip.mycode.init.my_bind.BindingFragment
import com.tkip.mycode.model.board.Board
import com.tkip.mycode.model.board.OnBoard
import com.tkip.mycode.model.count.CountType
import com.tkip.mycode.model.count.OnCount
import com.tkip.mycode.model.flea.OnFlea
import com.tkip.mycode.model.peer.isAdmin
import com.tkip.mycode.model.post.OnPost
import com.tkip.mycode.model.post.Post
import com.tkip.mycode.model.room.OnRoom
import com.tkip.mycode.nav.flea.tab_flea_market.FleaFrag
import com.tkip.mycode.nav.post.PostFrag
import com.tkip.mycode.nav.room.RoomFrag
import com.tkip.mycode.util.tools.view_pager.OnPager

class MainBoardFrag : BindingFragment<FragmentMainBoardBinding>() {
    override fun getLayoutResId(): Int = R.layout.fragment_main_board

    private var currentPos: Int = 0     // 현재 navHost 포지션
    private var currentBoard: Board? = null  // 현재 보드
    private var mMenu: Menu? = null
    private var menuInflater: MenuInflater? = null

    private val boardPairList = arrayListOf<Pair<Fragment, String>>()
            .apply {
                add(Pair(PostFrag.newInstance(recentBoard), recentBoard?.title?:"최근 베스트"))
                BOARD_NO1?.let { add(Pair(PostFrag.newInstance(it ), "최근 인기 보드")) }
                add(Pair(FleaFrag.newInstance(), fleaBoard?.title ?: R.string.title_flea.getStr()))
                add(Pair(RoomFrag.newInstance(), roomBoard?.title
                        ?: R.string.title_search_myroom.getStr()))
            }

    companion object {
        @JvmStatic
        fun newInstance() = MainBoardFrag()
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        initView()

//        savedInstanceState?.let {
//            currentPos = it.getInt(PUT_SAVE_CURRENT_POS)
////            currentBoard = it.getParcelable(PUT_SAVE_BOARD)
//            /**             * viewPager 위치 원상태로             */
////            b.tabLayout.setScrollPosition(currentPos, 0f, true)
//            b.viewPager.currentItem = currentPos
//        }
    }
//    override fun onSaveInstanceState(outState: Bundle) {
//        super.onSaveInstanceState(outState)
//        outState.putInt(PUT_SAVE_CURRENT_POS, currentPos)
////        outState.putParcelable(PUT_SAVE_BOARD, currentBoard)
//    }

    fun initView() {
        lllogM("MainBoardFrag initView adapter ${b.viewPager.adapter}")
//        /*** 초기화 체크*/
//        if (b.viewPager.adapter == null) {
        b.fragment = this
        (activity as AppCompatActivity).setSupportActionBar(b.toolbar)
        b.toolbar.title = R.string.title_board_home.getStr()
        (activity as AppCompatActivity).supportActionBar?.setDisplayHomeAsUpEnabled(true)
        (activity as AppCompatActivity).supportActionBar?.setHomeAsUpIndicator(R.drawable.ic_menu)

        setHasOptionsMenu(true)    // 메뉴 만들기.
        setupViewPager()
        b.avLoading.hide()
//        }
    }


    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
//        super.onCreateOptionsMenu(menu, inflater)
        mMenu = menu
        menuInflater = inflater
        setToolbarMenu(currentPos)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            android.R.id.home -> (activity as? MainActivity)?.toggleDrawer()
            R.id.add_board -> OnBoard.gotoAddBoardActivity(activity as AppCompatActivity, ACTION_NEW, null, null)
            R.id.add_post -> BOARD_NO1?.let { OnPost.gotoAddPostActivity(mContext, ACTION_NEW, Post(null, it), null) }

            R.id.book_flea -> OnCount.checkAddCountForFleaRoom(fleaBoard, CountType.BOOK)
            R.id.push_flea -> OnCount.checkAddCountForFleaRoom(fleaBoard, CountType.PUSH)
            R.id.add_flea -> OnFlea.gotoAddFleaActivity(mContext, ACTION_NEW, null, null)

            R.id.book_room -> OnCount.checkAddCountForFleaRoom(roomBoard, CountType.BOOK)
            R.id.push_room -> OnCount.checkAddCountForFleaRoom(roomBoard, CountType.PUSH)
            R.id.add_room -> OnRoom.gotoAddRoomActivity(activity as AppCompatActivity, ACTION_NEW, null, null)
        }
        return super.onOptionsItemSelected(item)
    }


    private fun setupViewPager() {
        OnPager.commonSetPager(childFragmentManager, boardPairList, b.viewPager, b.tabLayout)
        b.viewPager.onPageChange({}, { _, _, _ -> },
                onSelected = { position ->
                    currentPos = position
                    mMenu?.clear()
                    Log.d(Cons.TAG, "onPrepareOptionsMenu mMenu $mMenu")
                    setToolbarMenu(position)
                })
    }

    private fun setToolbarMenu(position: Int) {
        when (position) {
//            0 -> {
//                menuInflater?.inflate(R.menu.menu_main_board_home, mMenu)
//                b.toolbar.title = R.string.title_board_home.getStr()
//            }
            0 -> {
                /*** 베스트 포스팅 */
                mMenu?.clear()
//                menuInflater?.inflate(R.menu.menu_tab_post_free, mMenu)
                b.toolbar.title = R.string.title_bar_best_post.getStr()
            }
            1 -> {
                /*** 베스트 보드 */
                if (b.viewPager.size == 4) setMenuBestBoard()
                else setMenuFlea()
            }
            2 -> {
                /*** 중고거래 보드 */
                if (b.viewPager.size == 4) setMenuFlea()
                else setMenuRoom()
            }
            3 -> {
                /*** 부동산 보드 */
                setMenuRoom()
            }
        }
    }

    private fun setMenuRoom() {
        b.toolbar.title = R.string.title_room.getStr()
        if (isAdmin()) menuInflater?.inflate(R.menu.menu_tab_room_admin, mMenu)
        else menuInflater?.inflate(R.menu.menu_tab_room, mMenu)
    }

    private fun setMenuFlea() {
        b.toolbar.title = R.string.title_flea.getStr()
        if (isAdmin()) menuInflater?.inflate(R.menu.menu_tab_flea_admin, mMenu)
        else menuInflater?.inflate(R.menu.menu_tab_flea, mMenu)
    }

    private fun setMenuBestBoard() {
        b.toolbar.title = R.string.title_bar_best_board.getStr()
        menuInflater?.inflate(R.menu.menu_tab_post_free, mMenu)
        //                currentBoard = Board.board2
    }

}
