package com.tkip.mycode.nav.store

import android.app.Dialog
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.google.android.material.bottomsheet.BottomSheetDialog
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import com.tkip.mycode.databinding.BottomStoreBinding
import com.tkip.mycode.funs.common.lllogD


class StoreBottomFrag : BottomSheetDialogFragment() {

    private lateinit var b: BottomStoreBinding

//    val frag0 = StoreInfoFrag.newInstance()
//    val frag1 = StoreInfoFrag.newInstance()


    companion object {
        fun newInstance(): StoreBottomFrag =
                StoreBottomFrag().apply {
                    /*
                    arguments = Bundle().apply {
                        putInt(ARG_ITEM_COUNT, itemCount)
                    }
                     */
                }
    }

//    private lateinit var mBehavior: BottomSheetBehavior<View>

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
//        setStyle(DialogFragment.STYLE_NORMAL, R.style.FullScreenDialogStyle)
    }

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val dialog = super.onCreateDialog(savedInstanceState) as BottomSheetDialog
        val view = View.inflate(context, com.tkip.mycode.R.layout.bottom_store, null)

//        val b = BottomStoreBinding.inflate(LayoutInflater.from(context), null, false)

        dialog.setContentView(view)
//        val mBehavior = BottomSheetBehavior.from(b.coordi)
//        mBehavior.peekHeight = b.appbar.height
        val  mBehavior = BottomSheetBehavior.from(view.parent as View)
        mBehavior.peekHeight = BottomSheetBehavior.PEEK_HEIGHT_AUTO


        mBehavior.setBottomSheetCallback(object : BottomSheetBehavior.BottomSheetCallback() {

            override fun onSlide(bottom: View, slideOffset: Float) {
                lllogD("onSlide : $slideOffset")

            }

            override fun onStateChanged(bottom: View, newState: Int) {
                if (BottomSheetBehavior.STATE_EXPANDED == newState) {
                    lllogD("STATE_EXPANDED : $ ")
//                    b.appbar.visibility = View.GONE
//                    b.line1.visibility = View.GONE
                }
                if (BottomSheetBehavior.STATE_COLLAPSED == newState) {
                    lllogD("STATE_COLLAPSED : $ ")
//                    b.line1.visibility = View.VISIBLE
//                    b.appbar.visibility = View.VISIBLE
                }

                if (BottomSheetBehavior.STATE_HIDDEN == newState) {
                    lllogD("STATE_HIDDEN : $ ")
                    dismiss()
                }
            }
        })

//        hideView(app_bar_layout)
        return dialog
    }


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        b = DataBindingUtil.inflate(inflater, com.tkip.mycode.R.layout.bottom_poi, container, false)
        return b.root
    }

    private fun hideView(view: View) {
        val params = view.layoutParams
        params.height = 0
        view.layoutParams = params
    }

    private fun showView(view: View, size: Int) {
        val params = view.layoutParams
        params.height = size
        view.layoutParams = params
    }


/*


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        b = DataBindingUtil.inflate(inflater,  R.layout.bottom_poi, container, false)
        return b.root
    }


    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        b.fragment = this
        (activity as AppCompatActivity).setSupportActionBar(b.toolbar)
        b.toolbar.title = (activity as AppCompatActivity).getString( R.string.title_board)
        (activity as AppCompatActivity).supportActionBar?.setDisplayHomeAsUpEnabled(true)
        (activity as AppCompatActivity).supportActionBar?.setHomeAsUpIndicator( R.drawable.ic_menu)

        b.tabLayout.addOnTabSelectedListener(object : TabLayout.OnTabSelectedListener {
            override fun onTabSelected(tab: TabLayout.Tab) {
            }

            override fun onTabUnselected(tab: TabLayout.Tab) {
            }

            override fun onTabReselected(tab: TabLayout.Tab) {
            }
        })

        setupViewPager()
    }


    fun setupViewPager() {

        val listFragment: ArrayList<Fragment> = ArrayList()
        listFragment.add(frag0)
        listFragment.add(frag1)

        val listTitle: ArrayList<String> = ArrayList()
        listTitle.add(getString(com.tkip.mycode.R.string.board_tab1))
        listTitle.add(getString(com.tkip.mycode.R.string.board_tab2))

//        val pagerAdapter = activity?.supportFragmentManager?.let { BoardPagerAdapter(it,listFragment,listTitle) }
        val pagerAdapter = BoardPagerAdapter(childFragmentManager, listFragment, listTitle)

        b.viewPager.adapter = pagerAdapter
        b.viewPager.offscreenPageLimit = listFragment.size - 1   // tab이 리셋되지 않도록.

        b.viewPager.addOnPageChangeListener(object : ViewPager.OnPageChangeListener {
            override fun onPageScrollStateChanged(state: Int) {
//                TToast.showNormal("onPageScrollStateChanged"+ state)
            }

            override fun onPageScrolled(position: Int, positionOffset: Float, positionOffsetPixels: Int) {
//                TToast.showNormal("onPageScrolled"+ position + ":"+positionOffset + ":"+positionOffsetPixels )
            }

            override fun onPageSelected(position: Int) {
                TToast.showNormal("onPageSelected : $position")

                when (position) {
                    0 -> {
//                        if (!FreeBoardFrag.isStarted) frag0.startFrag()
                    }
                    1 -> {

                    }

                }
            }
        })

        b.tabLayout.setupWithViewPager(b.viewPager)

        // 시작하는 탭 설정
//        b.tabLayout.setScrollPosition(2, 0f, true)
//        b.viewPager.currentItem = 2
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        super.onCreateOptionsMenu(menu, inflater)
        inflater .inflate(R.menu.menu_tab_post_free, menu)
    }


    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            android.R.id.home -> {
                TToast.showNormal("ttttttest home click")
            }
            */
/* R.id.menu -> {
                 //do something
                 return true
             }*//*

        }
        return super.onOptionsItemSelected(item)
    }
*/



}