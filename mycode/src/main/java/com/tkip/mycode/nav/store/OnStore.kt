package com.tkip.mycode.nav.store

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.view.View
import androidx.fragment.app.Fragment
import com.cunoraz.tagview.TagView
import com.tkip.mycode.R
import com.tkip.mycode.dialog.toast.TToast
import com.tkip.mycode.funs.common.lllogI
import com.tkip.mycode.init.*
import com.tkip.mycode.init.base.checkUpdate
import com.tkip.mycode.init.base.clearAddAll
import com.tkip.mycode.init.base.getStr
import com.tkip.mycode.model.cost.Cost
import com.tkip.mycode.model.my_enum.ActType
import com.tkip.mycode.model.my_enum.Status
import com.tkip.mycode.model.my_enum.gteStatus
import com.tkip.mycode.model.mytag.OnTag
import com.tkip.mycode.model.peer.checkAllAvailUpload
import com.tkip.mycode.model.peer.isNotAdmin
import com.tkip.mycode.model.store.Store
import com.tkip.mycode.model.viewmodel.StoreViewModel
import com.tkip.mycode.nav.store.add.AddStoreActivity
import com.tkip.mycode.nav.store.tab_info.StoreInfoFrag
import com.tkip.mycode.nav.store.tab_menu.StoreMenuFrag
import com.tkip.mycode.util.lib.TransitionHelper
import com.tkip.mycode.util.lib.photo.Foto
import com.tkip.mycode.util.lib.photo.util.PhotoUtil
import com.tkip.mycode.util.lib.retrofit2.ESget
import com.tkip.mycode.util.lib.retrofit2.ESquery
import com.tkip.mycode.util.tools.etc.OnMyClick
import com.tkip.mycode.util.tools.fire.*

/**
 * <store upload 로직>
 *     user add,update -> storeInspection
 *     admin approval -> store
 *
 * <storeMenu 로직>
 *     user add, update -> store>storeMenu : statusNo.inspection
 *     admin approval -> store>storeMenu : statusNo.normal1000
 */
class OnStore {

    companion object {

        fun gotoAddStoreActivity(context: Context, action: Int, store: Store?, arrPair: Array<androidx.core.util.Pair<View, String>>?) {
            /*** 업로드 가능 여부 체크 */
            if (checkAllAvailUpload(context)) return

            val intent = Intent(context, AddStoreActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
            intent.putExtra(PUT_ACTION, action)
            store?.let { intent.putExtra(PUT_STORE, it) }
            TransitionHelper.startTran(context, intent, arrPair)
        }

        fun gotoStoreActivity(context: Context, isTop: Boolean, action: Int?, store: Store, arrPair: Array<androidx.core.util.Pair<View, String>>?) {
            val intent = Intent(context, StoreActivity::class.java)
            if (isTop) intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
            intent.putExtra(PUT_ACTION, action ?: ACTION_NORMAL)
            intent.putExtra(PUT_STORE, store)
            TransitionHelper.startTran(context, intent, null)
        }

//        fun gotoStoreListActivity(context: Context, arrPair: Array<androidx.core.util.Pair<View, String>>?) {
//            val intent = Intent(context, StoreListActivity::class.java)
//            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
//            TransitionHelper.startTran(context, intent, arrPair)
//        }

        /**
         *  스토어 중복검색후 삽입 from poi
         *  tag kewword add
         */
        /* fun checkStoreAndAddStoreFromPOI(placesClient: PlacesClient, placeId: String, success: (Store) -> Unit) {
             Firee.getStore(placeId,
                     success = { store ->
                         if (store == null) { // 없으면 add Store by Place
                             OnMap.getPlace(placesClient, placeId,
                                     success = { place ->
                                         Firee.addStoreForAdmin(Store.build(place),
                                                 success = { store2 ->
                                                     lllogD(" addStoreForAdmin  success")
                                                     success(store2)

                                                     OnTag.addFireStoreTags(store2.tagTypeList!!, store2, {}, {})
                                                 }, fail = {})

                                     }, fail = {})
                         } else {
                             success(store)
                         }
                     }, fail = {})
         }

 */


        /**************************************************************************
         * add 일때는 prePost == null
         *************************************************************************/
        fun addPhotoAndStore(addType: Int, activity: Activity, widthOrigin: Int?, selectedFotoList: ArrayList<Foto>, preStore: Store, store: Store,
                             success: () -> Unit, fail: () -> Unit) {

            PhotoUtil.uploadUriList(activity, widthOrigin, store, preStore.fotoList, selectedFotoList,
                    successUploading = { finalList ->

                        store.fotoList = finalList


                        /** gotoFire */
                        batch().apply {
                            when (addType) {
                                ACTION_NEW -> this.set(docRef(EES_STORE, store.key), store)
                                ACTION_UPDATE -> this.update(docRef(EES_STORE, store.key), makeUpdateMap(preStore, store))
                            }

                            /** 계산서 올리기 */
                            if (isNotAdmin(null, null)) this.addCostWithList(store)
                            /** 신규 MyTag 올리기 */
                            OnTag.addToBatchAllMytag(this)

                        }.myCommit("addPhotoAndStore",
                                success = {

                                    when (addType) {
                                        ACTION_NEW -> {
                                            completedDB(activity, store)
                                            success()
                                            Cost.new(null,ActType.ADD_STORE,store).real()
                                        }
                                        ACTION_UPDATE -> {
                                            completedDB(activity, store)
                                            success()
                                        }
                                    }

                                }, fail = { fail() })


                    },
                    fail = { fail() })
        }


        private fun completedDB(activity: Activity, store: Store) {
            OnTag.clearMyTagList()
//            TToast.showNormal(R.string.complete_upload_refresh_list)
            gotoStoreActivity(activity, true, ACTION_NORMAL, store, null)
            activity.finish()
        }

//        private fun getBatchStore(addType: Int, preStore: Store, store: Store): WriteBatch =
//                batch().apply {
//                    when (addType) {
//                        ACTION_NEW -> {
////                            val board = Board(store)
////                            store.boardKey = board.key
////                            this.set(docRef(EES_BOARD, board.key), board)
//                            this.set(docRef(EES_STORE, store.key), store)
//                            this.addCostWithList(store)
//                        }
//                        ACTION_UPDATE -> this.update(docRef(EES_STORE, store.key), makeUpdateMap(preStore, store))
//                    }
//                    OnTag.addToBatchAllMytag(this)
//                }

        private fun makeUpdateMap(preData: Store, data: Store) =
                mutableMapOf<String, Any?>().apply {
                    checkUpdate(preData.title, data.title, CH_TITLE)
                    checkUpdate(preData.comment, data.comment, CH_COMMENT)
                    checkUpdate(preData.contact, data.contact, CH_CONTACT)

                    checkUpdate(preData.local, data.local, CH_LOCAL)
                    checkUpdate(preData.lat, data.lat, CH_LAT)
                    checkUpdate(preData.lng, data.lng, CH_LNG)
                    checkUpdate(preData.address, data.address, CH_ADDRESS)

                    data.fotoList?.let { put(CH_FOTO_LIST, it) }  // fotolist 는 비교하기가 너무 까다롭다. 그냥 무조건 업데이트할 것.
                    data.mediaMap?.let { put(CH_MEDIA_MAP, it) }
                    data.tagList?.let { put(CH_TAG_LIST, it) }

                    checkUpdate(preData.placeId, data.placeId, CH_PLACE_ID)
//                    data.placeTypeList?.let { put(CH_PLACE_TYPE_LIST, it) }
                    checkUpdate(preData.websiteUri, data.websiteUri, CH_WEBSITE_URI)
                    checkUpdate(preData.phoneNumber, data.phoneNumber, CH_PHONE_NUMBER)
                    checkUpdate(preData.weekdayText, data.weekdayText, CH_WEEKDAY_TEXT)

                    if (isNotAdmin(null, null)) put(CH_UPDATE_DATE, System.currentTimeMillis())
                }


        /**
         * <admin Only>
         *     Store 올리기.
         */
//        fun addStoreForAdmin(activity: Activity, store1: Store, success: () -> Unit, fail: () -> Unit) {
//
//            /** updateDate */
////            OnDate1.addUidDate(Cons.ACTION_TYPE_INSPECTION, store1.dateList)
//
//            /** tagKword upload */
//            FireReal.addStoreTag(store1,
//                    success = {
//                        /** add Store */
//                        Firee.addStore(store1,
//                                success = {
//                                    success()
//                                    TToast.showNormal(R.string.complete_upload)
//                                    MyProgress.cancel()
//                                    activity.finish()
//
//                                    val intent = Intent(activity, StoreListActivity::class.java)
//                                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
//                                    activity.startActivity(intent)
//                                    success()
//
//                                }, fail = { fail() })
//                    }, fail = { fail() })
//        }


        /**************************************************************************
         * post 받고   * status 체크하고         * 정상이면 이동
         *************************************************************************/
        fun checkAndGo(context: Context, isTop: Boolean, key: String, action: Int?, status: Status, arrPair: Array<androidx.core.util.Pair<View, String>>?, onSuccess: ((Store) -> Unit)?, onNotNormal: ((Store) -> Unit)?) {
            if(OnMyClick.isDoubleClick()) return

            ESget.getStore(key,
                    success = { store1 ->

                        if (store1 == null) {
                            TToast.showNormal(R.string.error_data_deleted)
                        } else {
                            if (store1.gteStatus(status, true)) {
                                onSuccess?.invoke(store1)
                                gotoStoreActivity(context, isTop, action, store1, arrPair)
//                                MyCircle.cancel()   // postActivity에서 cancel()
                            } else {
                                onNotNormal?.invoke(store1)
//                                MyCircle.cancel()
                            }
                        }

                    }, fail = {})
        }


        /*** 상태체크 없이 그냥 고 */
//        fun justGo(context: Context, isTop: Boolean, store: Store, arrPair: Array<androidx.core.util.Pair<View, String>>? ) {
//            gotoStoreActivity(context, isTop, null, store, arrPair)
//        }


        /*******************************************************************************
         * storeActivity의
         * 사용자에 따라 storeActivity의 tabLayout 다르게
         *******************************************************************************/
        fun getPairList(store: Store, actionType: Int) =
                arrayListOf<Pair<String, Fragment>>().apply {
                    add(Pair(R.string.title_intro.getStr(), StoreInfoFrag.newInstance(store)))
                    add(Pair(R.string.title_menus.getStr(), StoreMenuFrag.newInstance(store, actionType)))
                }

        /*****************************************************************
         *************************** 검색 **********************************
         ****************************************************************/

        /**         * 검색 셋팅         */
//        fun setupRvSearch(vmStore: StoreViewModel, owner: LifecycleOwner, viewSearchTagView: CustomSearchTagView, esLastIndex: Int, onResultSize: (Int) -> Unit) {
//
//            var searchAdapter: StoreAdapter? = null
//            var init = true
//
//            vmStore.liveSearchList.observe(owner, Observer {
//                if (init) {
//                    searchAdapter = StoreAdapter(it, RvType.VERT, onItemClick = {})
//                    viewSearchTagView.b.rv.adapter = searchAdapter
//                    init = false
//                } else {
//                    lllogI("setupRvSearch list.size : ${it.size}")
//                    OnVisible.toggleSimple(it.isEmpty(), viewSearchTagView.b.tvNoResult)  // 결과없으면 안내멘트
//                    viewSearchTagView.b.refresh.finishLoadMore(0) // swf 닫기
//                    viewSearchTagView.b.refresh.setEnableLoadMore(esLastIndex != it.size)   // 더이상 data 없으면 loadMore 불능.
//                    searchAdapter?.notifyDataSetChanged()
//                    onResultSize(it.size)   // lastIndex 넘기기.
//                }
//            })
//        }

        /*** es 검색하기 */
        fun getEsSearchFromTag(vmStore: StoreViewModel, size: Int, from: Int, tagView: TagView, keyword: String?) {
            vmStore.getEsSearch(size, from, tagView, keyword)
        }

        fun getEsSearchFromKeyword(size: Int, from: Int, keyword: String, status: Status, success: (ArrayList<Store>?) -> Unit, fail: () -> Unit) {
            val query = ESquery.matchKeyword(size, from, arrayOf(CH_TITLE), keyword, status)
            ESget.getStoreList(query,
                    success = {
                        lllogI("getEsSearchFromKeyword list : ${it?.size}")
                        success(it)
                    }, fail = {
                fail()
                lllogI("getEsSearchFromKeyword fail")
            })
        }

        /**
         * 검색 결과 rv정리
         */
        fun setStoreSearchResult(resultList: ArrayList<Store>?, items: ArrayList<Store>, searchAdapter: StoreAdapter?, onLastIndex: (Int) -> Unit) {
            if (resultList.isNullOrEmpty()) items.clear()
            else items.clearAddAll(resultList)
            searchAdapter?.notifyDataSetChanged()
            onLastIndex(items.size)
        }


    }


}