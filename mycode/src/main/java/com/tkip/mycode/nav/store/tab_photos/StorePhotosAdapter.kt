package com.tkip.mycode.nav.store.tab_photos

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.RecyclerView
import com.tkip.mycode.databinding.HolderStorePhotosBinding
import com.tkip.mycode.model.post.Post

class StorePhotosAdapter(var items: ArrayList<Post>) : RecyclerView.Adapter<StorePhotosAdapter.StorePhotosViewHolder>() {

    private lateinit var activity: AppCompatActivity

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): StorePhotosViewHolder {
        this.activity = parent.context as AppCompatActivity
        val binding = HolderStorePhotosBinding.inflate(LayoutInflater.from(parent.context),parent,false)
        return StorePhotosViewHolder(binding)
    }

    override fun getItemCount(): Int {
        return items.size
    }

    override fun onBindViewHolder(h: StorePhotosViewHolder, p: Int) {
        h.bind(items[p])
    }


    inner class StorePhotosViewHolder(val b:  HolderStorePhotosBinding) : RecyclerView.ViewHolder(b.root) {


        fun bind(post: Post) {
            b.holder = this
            b.executePendingBindings()
        }

        fun onClickCard() {

//
//            val intent = Intent(activity, FreePostActivity::class.java)
//            intent.putExtra(Cons.PUT_OBJECT, post)
//            activity.startActivity(intent)
        }
    }
}