package com.tkip.mycode.nav.board.tab_first

import android.content.Intent
import android.os.Bundle
import android.widget.EditText
import androidx.annotation.LayoutRes
import com.tkip.mycode.R
import com.tkip.mycode.databinding.ActivityAddBoardBinding
import com.tkip.mycode.dialog.alert.MyAlert
import com.tkip.mycode.dialog.toast.TToast
import com.tkip.mycode.dialog.toast.toastAlert
import com.tkip.mycode.funs.common.*
import com.tkip.mycode.funs.custom.manual.LinearManualMore
import com.tkip.mycode.funs.custom.toolbar.ViewToolbarEmpty
import com.tkip.mycode.init.*
import com.tkip.mycode.init.base.*
import com.tkip.mycode.init.inter.interActivityResult
import com.tkip.mycode.init.inter.interRequestPermissionsResult
import com.tkip.mycode.init.my_bind.BindingActivityPhoto
import com.tkip.mycode.model.board.*
import com.tkip.mycode.model.mytag.TagTYPE
import com.tkip.mycode.model.mytag.convertStringList
import com.tkip.mycode.model.peer.isAdmin
import com.tkip.mycode.model.peer.isCsBoard
import com.tkip.mycode.util.lib.photo.util.PhotoUtil
import com.tkip.mycode.util.lib.photo.view.LinearAddPhotoOne
import com.tkip.mycode.util.lib.retrofit2.ESget
import com.tkip.mycode.util.lib.retrofit2.ESquery
import com.tkip.mycode.util.my_view.ViewUtil
import com.tkip.mycode.util.tools.etc.OnMyClick
import com.tkip.mycode.util.tools.fire.Firee
import com.tkip.mycode.util.tools.radio.CustomRGstring

/**
 */
class AddBoardActivity : BindingActivityPhoto<ActivityAddBoardBinding>() {
    @LayoutRes
    override fun getLayoutResId() = R.layout.activity_add_board

    private val addType by lazy { intent.getIntExtra(PUT_ACTION, ACTION_NEW) }

    //    private val boardType by lazy { intent.getIntExtra(PUT_BOARD_TYPE_NO,BoardType.USER.no).getBoardType() }
    private val board by lazy { intent.getParcelableExtra(PUT_BOARD) ?: Board() }   // 널이면 초기화
    private lateinit var finalBoard: Board
    private lateinit var manualMore: LinearManualMore

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        lllogD("AddBoardActivity addType $addType")

        finalBoard = Board(board)
        initData(finalBoard)
        initView()
    }

    private fun initView() {

        val title =
                when {
                    finalBoard.isCsBoard() && (addType == ACTION_NEW) -> R.string.title_create_csboard.getStr()
                    finalBoard.isCsBoard() && (addType == ACTION_UPDATE) -> R.string.title_update_csboard.getStr()
                    (addType == ACTION_UPDATE) -> R.string.title_update_add_board.getStr()
                    else -> R.string.title_add_board.getStr()
                }
        b.frameToolbar.addView(ViewToolbarEmpty(this@AddBoardActivity, title, { onBackPressed() }, null))

        b.activity = this
        b.board = finalBoard
        b.addType = addType
        b.isCsBoard = finalBoard.isCsBoard()


        /*** 매뉴얼 */
        val infoManual =
                if (finalBoard.isCsBoard()) R.string.manual_csboard.getStr()
                else R.string.manual_top_add_board.getStr()
        val manual = LinearManualMore(this@AddBoardActivity, infoManual, null)
        b.line.addView(manual, 0)

        /***  보드타입 선택 RG  */
        if (isAdmin()) b.line.addView(CustomRGstring(this@AddBoardActivity, false, R.string.info_select_board_type.getStr(), BOARD_TYPE_STR_LIST_ADMIN,null,
                onClickRb = { p ->
                    val selBoardType = BOARD_TYPE_STR_LIST_ADMIN[p].getBoardType()
                    finalBoard.boardTypeNo = selBoardType.no
                    when (selBoardType) {
                        BoardType.FIXED -> b.cbAllowSearch.isChecked = true
                        BoardType.ADMIN -> b.cbAllowSearch.isChecked = true
                        BoardType.USER -> b.cbAllowSearch.isChecked = true
                        BoardType.CS_CENTER -> b.cbAllowSearch.isChecked = false
                    }
                }), 1)

        /*** EditText 리스너 */
        b.etName.setOnFocusChangeListener { _, isOn -> if (!isOn) b.etName.isNullBlankAndSetError(b.layName, R.string.error_name_add_board) }
        b.etId.setOnFocusChangeListener { v, isOn ->
            if (!isOn && addType == ACTION_NEW) {
                (v as EditText).getNullStr()?.let { id ->
                    if (b.etId.isLtLength(MIN_BOARD_ID, b.layId, R.string.error_id_add_board_min)) return@setOnFocusChangeListener
                    /**     * 아이디 중복 체크.     */
                    Firee.existBoardID(id, success = { if (it) b.layId.error = R.string.error_info_already_exist_id_add_board.getStr() }, fail = {})
                }
            }
        }


//        b.etPw.setOnFocusChangeListener { _, isOn -> if (!isOn) if (b.etPw.isLtLength(R.integer.count_boardid_min.getInt(), b.layPw, R.string.error_password_add_board_min)) return@setOnFocusChangeListener }


        /*** 태그리스트 */
        linearAddTag = ViewUtil.addLinearAddTag(this@AddBoardActivity, b.lineTag, TagTYPE.BOARD, R.string.name_tag_search.getStr(), R.string.btn_open_tag_dialog.getStr(), finalBoard.tagList)

        /**         * 사진 1장 올리기 박스         */
        viewAddPhotoOne = LinearAddPhotoOne(this@AddBoardActivity, finalBoard.photoThumb, finalBoard.photoUrl,
                onDelete = { },
                onGallery = { onClickOpenGallery() })
                .apply { this@AddBoardActivity.b.frameAddPhotoOne.addView(this) }

        /********************* superListener *****************************/
        listenerActivityResult = interActivityResult(
                onPick = {
                    //                    it?.let { uri ->
//                        b.iv.setImageURI(uri)
//                        b.iv.visibility = View.VISIBLE
//                        PhotoUtil.resetAllUriList()
//                        PhotoUtil.photoUris.add(uri)
//                    }
                }, onAlbum = null, onMap = null, onBanner = null, onData = null)

        listenerPermissionsResult = interRequestPermissionsResult(
                onReadExternalStorage = { OnPermission.onOpenGallery(this@AddBoardActivity, it, false) })

    }

    /**     * 사용자가 기입한 board1로 변경.     */
    private fun initData(board1: Board) {
        b.board = board1
    }

    /**     * 올릴 board 준비하기.     */
    private fun getFinalBoard() =
            finalBoard.apply {
                title = b.etName.getStr()
                comment = b.etDescription.getStr()
                boardId = b.etId.getNullStr()?.discard0()
                allowPost = b.cbAllowPost.isChecked
                allowSearch = b.cbAllowSearch.isChecked

//                password = b.etPw.getNullStr()?.discard0()
//                blind = password != null

                linearAddTag?.b?.tagView?.convertStringList()?.let { tagList = tagList.clearAddAll(it) }
            }


    override fun onRestoreInstanceState(save: Bundle) {
        super.onRestoreInstanceState(save)
        save.getParcelable<Board>(PUT_RESULT_OBJECT)?.let { initData(it) } ?: finish()
        PhotoUtil.resetAllUriList()    //앨범에서 가져온 사진은 그냥 초기화.
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        outState.putParcelable(PUT_RESULT_OBJECT, getFinalBoard())
    }

    override fun onBackPressed() {
//        if (OnValid.checkKeyboardHide(b.etName, b.etDescription)) return
        OnValid.removeFocusEt(b.etName,b.etDescription)

        if (!b.etName.isNullBlank(null) || !b.etDescription.isNullBlank(null)) MyAlert.showConfirm(this@AddBoardActivity, R.string.info_wanna_leave_add_common.getStr(), ok = { super.onBackPressed() }, cancel = {})
        else super.onBackPressed()
    }

//    fun onClickAdditional(v: View) {
//        OnMyClick.setDisableAWhileBTN(v)
//        b.btnAdditional.goneSlideDown(false)
//        b.lineAddtional.showSlideDown(false)
//    }


    fun onClickOpenGallery() {
        if (OnMyClick.isDoubleClick()) return
        OnPermission.onOpenGallery(this@AddBoardActivity, true, false)
    }

    fun onClickSubmit() {
        if (OnMyClick.isDoubleClick()) return
        if ((b.etName.isNullBlankAndSetError(b.layName, R.string.error_name_add_board)) || (OnValid.hasError(b.layName, b.layDescription, b.layId))) {
            TToast.showAlert(R.string.error_edittext_null)
            return
        }

        b.etId.getNullStr()?.let {
            if (it.isLt(MIN_BOARD_ID)) {
                toastAlert(String.format(R.string.error_id_add_board_min.getStr(), MIN_BOARD_ID))
                return
            }
        }

        MyAlert.showConfirm(this@AddBoardActivity, R.string.alert_add_object.getStr(),
                ok = {
                    getFinalBoard()

                    if (isAdmin()) {
                        finalAdd()
                        return@showConfirm
                    }

                    when (addType) {
                        ACTION_NEW ->
                            /**     * 아이디 중복 체크.     */
                            if (finalBoard.boardId == null) checkPayment()
                            else finalBoard.boardId?.let { id ->
                                ESget.sizeBoardList(ESquery.isBoardIDcheck(id), success = {
                                    if (it == null || it < 1) {
                                        checkPayment()
                                        b.layId.error = null
                                    } else {
                                        TToast.showAlert(R.string.error_info_already_exist_id_add_board)
                                        b.layId.error = R.string.error_info_already_exist_id_add_board.getStr()
                                    }
                                }, fail = {})
                            }
                        ACTION_UPDATE -> checkPayment()
                    }

                }, cancel = {})
    }

    private fun checkPayment() {
        finalAdd()
    }

    private fun finalAdd() {

        /** <기존 이미지 삭제하기>
         * 1. 그냥 이전 이미지만 삭제.
         * 2. 새로운 이미지 올림.  */
        viewAddPhotoOne?.let {
            if (it.hasToBeDeletedPhoto) {
                lllogI("Addboardactivity viewAddPhotoOne  toBeDeletedFoto")
                PhotoUtil.deletePhoto(finalBoard.photoUrl, finalBoard.photoThumb)
                finalBoard.photoUrl = null
                finalBoard.photoThumb = null
            }
        }


        OnBoard.addPhotoAndBoard(addType, this@AddBoardActivity, board, finalBoard,
                success = {
                    /** 스토어의 cs 보드 만들었을때는 */
                    if (finalBoard.isCsBoard() && (addType == ACTION_NEW)) {
                        lllogI("AddBoardActivity addPhotoAndBoard success board1 ${finalBoard.key}")
                        val intent = Intent()
                        intent.putExtra(PUT_BOARD_KEY, finalBoard.key)
                        setResult(RESULT_OK, intent)
                        finish()
                    } else {
                        OnBoard.completedDB(this@AddBoardActivity, finalBoard)
                    }

                }, fail = {})
    }


}

