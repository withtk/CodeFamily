package com.tkip.mycode.nav.store

import android.os.Bundle
import android.view.View
import android.widget.LinearLayout
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.tkip.mycode.R
import com.tkip.mycode.databinding.FragmentMainStoreBinding
import com.tkip.mycode.funs.custom.flow_layout.ViewBottomTag
import com.tkip.mycode.funs.search.model.CustomSearchStore
import com.tkip.mycode.init.ACTION_NEW
import com.tkip.mycode.init.MainActivity
import com.tkip.mycode.init.RV_STORE_HOST
import com.tkip.mycode.init.base.getStr
import com.tkip.mycode.init.inter.interToolbarIcon
import com.tkip.mycode.init.my_bind.BindingFragment
import com.tkip.mycode.model.mytag.TagTYPE
import com.tkip.mycode.util.tools.etc.OnMyClick


class MainStoreFrag : BindingFragment<FragmentMainStoreBinding>() {
    override fun getLayoutResId(): Int = R.layout.fragment_main_store

    private var customSearchStore: CustomSearchStore? = null
    private var viewFlowBtn: ViewBottomTag? = null

    private lateinit var bottomSheetBehavior: BottomSheetBehavior<LinearLayout>

    companion object {
        @JvmStatic
        fun newInstance() = MainStoreFrag()
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        initView()
    }

    private fun initView() {
        b.fragment = this

        /** 툴바 */
        b.inToolbar.title = R.string.title_store.getStr()
        b.inToolbar.backIcon = false
//        b.inToolbar.icon = R.drawable.ic_search.getDrawable()
        b.inToolbar.menuText = R.string.btn_add_store.getStr()
        b.inToolbar.inter = interToolbarIcon(
                onFirst = { (activity as MainActivity).toggleDrawer() }, onTopTitle = {},
                onTextMenu = { onClickAddStore(it) },onTextMenu2 = null, onIcon = {}, onMore = {})


        /** 검색 view 셋팅 */
        customSearchStore = CustomSearchStore(mContext, false, TagTYPE.STORE, RV_STORE_HOST, onTagView = {}, onSearch = { _, _ -> }, onLoadMore = { _, _ -> })
//        b.lineCover.addView(customSearchStore)
        b.frame.addView(customSearchStore)




        /** 추천 태그 */
//        b.line.addView(ViewBottomTag(mContext, OnTag.loadMyKeywordList(Sred.ARR_STORE_KEYWORD),
//                onOpen = { bottomSheetBehavior.myToggle() },
//                onTagClick = {
//                    lllogD("ViewFlowBtn ViewFlowBtn str:$it")
//                    customSearchStore?.addTagAndSearch(it)
//
//                }))


        /** 바텀 셋팅 */
//        bottomSheetBehavior = BottomSheetBehavior.from(b.line)
//        bottomSheetBehavior.state = BottomSheetBehavior.STATE_COLLAPSED



        /** 리프레쉬 */
//        b.refresh.setOnRefreshListener {
//            //            initView()
//            b.lineSearch.removeAllViews()
//            b.lineList.gone()
//            addCustomSearchStore()
//            b.refresh.finishRefresh(0)  // swf 닫기
//        }

        /** 스토어 리스트 : 신규 */
//        b.lineList.addView(CustomModelListHori(mContext, R.string.title_new_store.getStr(), false, DataType.STORE, RV_HORI_BIG, null, ESquery.storeList(6, 0, Status.NORMAL), onItem = null))

    }

    /**     * 스토어 등록하기.     */
    private fun onClickAddStore(v: View) {
        OnMyClick.setDisableAWhileBTN(v)
        OnStore.gotoAddStoreActivity(mContext, ACTION_NEW, null, null)
    }


    fun onClickSearchStore(v: View) {
//        bottomSheetBehavior.myToggle()
    }

}
