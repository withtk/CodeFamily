package com.tkip.mycode.nav.flea

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.tkip.mycode.R
import com.tkip.mycode.databinding.HolderFleaGridVertBinding
import com.tkip.mycode.databinding.HolderFleaHoriBinding
import com.tkip.mycode.databinding.HolderFleaMyConBinding
import com.tkip.mycode.databinding.HolderFleaVertBinding
import com.tkip.mycode.dialog.alert.MyAlert
import com.tkip.mycode.dialog.toast.milkStatusAdmin
import com.tkip.mycode.funs.common.tranPair
import com.tkip.mycode.init.*
import com.tkip.mycode.init.inter.interHolderBasic
import com.tkip.mycode.model.flea.Flea
import com.tkip.mycode.model.flea.OnFlea
import com.tkip.mycode.model.grant.OnGrant
import com.tkip.mycode.model.my_enum.Status
import com.tkip.mycode.model.my_enum.getStatus
import com.tkip.mycode.model.my_enum.justGo
import com.tkip.mycode.model.my_enum.showStatusDialog
import com.tkip.mycode.model.peer.isAdmin
import com.tkip.mycode.util.tools.anim.gone
import com.tkip.mycode.util.tools.etc.OnMyClick

class FleaAdapter(private val items: ArrayList<Flea>, val rvType: Int, val onItemClick: ((Flea) -> Unit)?) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {
//    companion object {
//        const val rvVert = 50
//        const val rvGridVert = 100
//        const val rvHori = 200
//        const    val rvMyContents=500
//    }

    private lateinit var context: Context

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        this.context = parent.context
        return when (rvType) {
            RV_GRID_VERT -> FleaGridVertHolder(HolderFleaGridVertBinding.inflate(LayoutInflater.from(parent.context), parent, false))
            RV_HORI -> FleaHoriViewHolder(HolderFleaHoriBinding.inflate(LayoutInflater.from(parent.context), parent, false))
            RV_VERT -> FleaVertViewHolder(HolderFleaVertBinding.inflate(LayoutInflater.from(parent.context), parent, false))
            RV_MY_CONTENTS, RV_MY_CONTENTS_MANAGE, RV_MANAGE -> FleaMyConViewHolder(HolderFleaMyConBinding.inflate(LayoutInflater.from(parent.context), parent, false))
            else -> FleaVertViewHolder(HolderFleaVertBinding.inflate(LayoutInflater.from(parent.context), parent, false))
        }
    }

    override fun onBindViewHolder(h: RecyclerView.ViewHolder, p: Int) {
        when (rvType) {
            RV_GRID_VERT -> (h as FleaGridVertHolder).bind(items[p])
            RV_HORI -> (h as FleaHoriViewHolder).bind(items[p])
            RV_VERT -> (h as FleaVertViewHolder).bind(items[p])
            RV_MY_CONTENTS, RV_MY_CONTENTS_MANAGE, RV_MANAGE -> (h as FleaMyConViewHolder).bind(items[p])
            else -> (h as FleaVertViewHolder).bind(items[p])
        }
    }

    override fun getItemCount(): Int {
        return items.size
    }


    /****************************************************************************************
     *  fleaPost에서 에서.
     ****************************************************************************************/
    inner class FleaGridVertHolder(val b: HolderFleaGridVertBinding) : RecyclerView.ViewHolder(b.root) {

        private lateinit var flea: Flea

        fun bind(flea: Flea) {
            this.flea = flea
//            b.holder = this
            b.flea = flea

            b.inter = interHolderBasic(
                    onClickCard = { onClickCard(it) },
                    onClickGo = null,
                    onClickUpdate = { },
                    onClickDel = { }, onDeepDel = {},
                    onClickStatus = { onClickStatus(it) })

            b.executePendingBindings()
        }

        fun onClickCard(v: View) {
            OnMyClick.setDisableAWhileBTN(v)

            onItemClick?.let {
                it(flea)
                return
            }

            OnFlea.checkAndGo(context, false, flea.key, ACTION_NORMAL, Status.NORMAL,
                    arrayOf(b.line.tranPair(R.string.tran_linear)),
                    onSuccess = {},
                    onNotNormal = {
                        items[adapterPosition] = it
                        notifyItemChanged(adapterPosition)
                    })

        }

        fun onClickStatus(v: View) {
            OnMyClick.setDisableAWhileBTN(v)
            if (isAdmin()) {
                milkStatusAdmin(flea.statusNo.getStatus().msg)
                OnFlea.checkAndGo(context, false, flea.key, ACTION_NORMAL, Status.NOTHING,
                        arrayOf(b.line.tranPair(R.string.tran_linear)), null, null)
            }
        }

    }


    /******************************************************************************************
     * vert
     ******************************************************************************************/
    inner class FleaVertViewHolder(val b: HolderFleaVertBinding) : RecyclerView.ViewHolder(b.root) {

        private lateinit var flea: Flea

        fun bind(flea: Flea) {
            this.flea = flea
//            b.holder = this
            b.flea = flea

            b.inter = interHolderBasic(
                    onClickCard = { onClickCard(it) },
                    onClickGo = {},
                    onClickUpdate = { },
                    onClickDel = { }, onDeepDel = {},
                    onClickStatus = null)

            b.executePendingBindings()
        }

        fun onClickCard(v: View) {
            OnMyClick.setDisableAWhileBTN(v)

            onItemClick?.let {
                it(flea)
                return
            }

            OnFlea.checkAndGo(context, false, flea.key, ACTION_NORMAL, Status.NORMAL,
                    arrayOf(b.line.tranPair(R.string.tran_linear)),
                    onSuccess = {},
                    onNotNormal = {
                        items[adapterPosition] = it
                        notifyItemChanged(adapterPosition)
                    })

        }

    }

    /******************************************************************************************
     * mycontents, manage
     ******************************************************************************************/
    inner class FleaMyConViewHolder(val b: HolderFleaMyConBinding) : RecyclerView.ViewHolder(b.root) {

        private lateinit var flea: Flea

        fun bind(flea: Flea) {
            this.flea = flea
//            b.holder = this
            b.flea = flea

            b.inter = interHolderBasic(
                    onClickCard = { onClickCard(it) },
                    onClickGo = {
//                        MyAlert.showRequestGrant(context, flea, onSuccess = {}, onFail = {}) // 승인 요청.
                        OnGrant.request(context, flea, onSuccess = {b.btnGrant.gone()})         // 승인 요청.

//                        OnGrant.request(context, flea, onSuccess = { b.btnGrant.goneFadeOut(false) })
                    },   // 승인 요청.
                    onClickUpdate = null,
                    onClickDel = null, onDeepDel = {},
                    onClickStatus = { if (isAdmin()) flea.showStatusDialog(context,null) else onClickCard(it) })

            b.line.setOnLongClickListener {
                flea.showStatusDialog(context, onSuccess = { status, _ ->
                    status?.let {
                        flea.statusNo = it.no
                        notifyItemChanged(adapterPosition)
                    }
                })
                return@setOnLongClickListener true
            }


            b.executePendingBindings()

            b.btnGrant.gone()   // 이걸 안 해주면 notify했을 때 이상하게 나와.
        }

        fun onClickCard(v: View) {
            OnMyClick.setDisableAWhileBTN(v)

            OnGrant.checkStatus(context, flea, b.btnGrant,
                    onNormal = {                        flea.justGo(context, true, arrayOf(b.tvTitle.tranPair(R.string.tran_linear)))                    },
//                    onAvailRequest = {  hhh(OnGrant.INTERVAL_BTN_SHOW) {if (it) b.btnGrant.visi() else b.btnGrant.gone()}  },
                    onDeleted = {
                        /*** Status.Deleted 완료시 rv에서 제거. */
                        items.removeAt(adapterPosition)
                        notifyItemRemoved(adapterPosition)
                    })

        }

    }

    /****************************************************************************************
     *  searchAddAll 에서.
     ****************************************************************************************/
    inner class FleaHoriViewHolder(val b: HolderFleaHoriBinding) : RecyclerView.ViewHolder(b.root) {

        private lateinit var flea: Flea

        fun bind(flea: Flea) {
            this.flea = flea
            b.flea = flea

            b.inter = interHolderBasic(
                    onClickCard = { onClickCard(it) },
                    onClickGo = null,
                    onClickUpdate = { },
                    onClickDel = { }, onDeepDel = {},
                    onClickStatus = null)

            b.executePendingBindings()
        }

        fun onClickCard(v: View) {
            OnMyClick.setDisableAWhileBTN(v)

            onItemClick?.let {
                it(flea)
                return
            }

            OnFlea.checkAndGo(context, false, flea.key, ACTION_NORMAL, Status.NORMAL,
                    arrayOf(b.line.tranPair(R.string.tran_linear)),
                    onSuccess = {},
                    onNotNormal = {
                        items[adapterPosition] = it
                        notifyItemChanged(adapterPosition)
                    })

        }

    }


}