package com.tkip.mycode.nav.store

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.tkip.mycode.R
import com.tkip.mycode.databinding.HolderStoreGridHoriBinding
import com.tkip.mycode.databinding.HolderStoreHoriBinding
import com.tkip.mycode.databinding.HolderStoreMyConBinding
import com.tkip.mycode.databinding.HolderStoreVertBinding
import com.tkip.mycode.dialog.alert.MyAlert
import com.tkip.mycode.funs.common.lllogI
import com.tkip.mycode.funs.common.tranPair
import com.tkip.mycode.init.*
import com.tkip.mycode.init.inter.interHolderBasic
import com.tkip.mycode.model.grant.OnGrant
import com.tkip.mycode.model.my_enum.Status
import com.tkip.mycode.model.my_enum.justGo
import com.tkip.mycode.model.my_enum.showStatusDialog
import com.tkip.mycode.model.peer.isAdmin
import com.tkip.mycode.model.store.Store
import com.tkip.mycode.util.tools.anim.gone
import com.tkip.mycode.util.tools.etc.OnMyClick

class StoreAdapter(private val items: ArrayList<Store>, val rvType: Int, val onItemClick: ((Store) -> Unit)?) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

//    companion object {
//        const val rvVert = 100
//        const val rvHori = 200
//        const val rvGridHori = 240
//        const    val rvMyContents=500
//    }

    private lateinit var context: Context

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        this.context = parent.context
        return when (rvType) {
            RV_VERT, RV_STORE_HOST -> StoreVertHolder(HolderStoreVertBinding.inflate(LayoutInflater.from(parent.context), parent, false))
            RV_MY_CONTENTS, RV_MY_CONTENTS_MANAGE, RV_MANAGE -> StoreMyConHolder(HolderStoreMyConBinding.inflate(LayoutInflater.from(parent.context), parent, false))
            RV_HORI -> StoreHoriViewHolder(HolderStoreHoriBinding.inflate(LayoutInflater.from(parent.context), parent, false))
            RV_HORI_BIG -> StoreGridHoriHolder(HolderStoreGridHoriBinding.inflate(LayoutInflater.from(parent.context), parent, false))
            else -> StoreVertHolder(HolderStoreVertBinding.inflate(LayoutInflater.from(parent.context), parent, false))
        }
    }

    override fun onBindViewHolder(h: RecyclerView.ViewHolder, p: Int) {
        when (rvType) {
            RV_VERT, RV_STORE_HOST -> (h as StoreVertHolder).bind(items[p])
            RV_MY_CONTENTS, RV_MY_CONTENTS_MANAGE, RV_MANAGE -> (h as StoreMyConHolder).bind(items[p])
            RV_HORI -> (h as StoreHoriViewHolder).bind(items[p])
            RV_HORI_BIG -> (h as StoreGridHoriHolder).bind(items[p])
            else -> (h as StoreVertHolder).bind(items[p])
        }
    }

    override fun getItemCount(): Int {
        return items.size
    }


    /*** Status.Deleted 완료시 rv에서 제거. */
    private fun StoreMyConHolder.commonRemoveItem() {
        items.removeAt(adapterPosition)
        notifyItemRemoved(adapterPosition)
    }

    /*********************************************************************************************
     * Vertical 스토어
     *********************************************************************************************/
    inner class StoreVertHolder(val b: HolderStoreVertBinding) : RecyclerView.ViewHolder(b.root) {

        private lateinit var store: Store

        fun bind(store: Store) {
            this.store = store
            b.store = store
            b.inter = interHolderBasic(
                    onClickCard = { onClickCard(it) },
                    onClickGo = null,
                    onClickUpdate = null,
                    onClickDel = null, onDeepDel = {},
                    onClickStatus = null)


            //todo : 유저별로 분기할 것.
            /*** adCredit 보이기. */
            b.showAdCredit = (rvType == RV_STORE_HOST)


            b.executePendingBindings()
        }

        fun onClickCard(v: View) {
            OnMyClick.setDisableAWhileBTN(v)
            onItemClick?.let {
                (store)
                return
            }

            OnStore.checkAndGo(context, false, store.key, null, Status.NORMAL,
                    arrayOf(b.cons.tranPair(R.string.tran_linear)),
                    onSuccess = { },
                    onNotNormal = {
                        items[adapterPosition] = it
                        notifyItemChanged(adapterPosition)
                    })
        }
    }

    /*********************************************************************************************
     * myContents, manage
     *********************************************************************************************/
    inner class StoreMyConHolder(val b: HolderStoreMyConBinding) : RecyclerView.ViewHolder(b.root) {

        private lateinit var store: Store

        fun bind(store: Store) {
            this.store = store
            b.store = store

            //todo : 유저별로 분기할 것.
//            /*** adCredit 보이기. */
//            b.showAdCredit = (rvType == RV_STORE_HOST)

            b.inter = interHolderBasic(
                    onClickCard = { onClickCard(it) },
                    onClickGo = {
//                        MyAlert.showRequestGrant(context,   store, onSuccess = {}, onFail = {}) // 승인 요청.
                        OnGrant.request(context, store, onSuccess = { b.btnGrant.gone() })         // 승인 요청.

//                        OnGrant.request(context, store, onSuccess = { b.btnGrant.goneFadeOut(false) })
                    },   // 승인 요청.
                    onClickUpdate = null,
                    onClickDel = null, onDeepDel = {},
                    onClickStatus = { if (isAdmin()) store.showStatusDialog(context, null) else onClickCard(it) })

            b.cons.setOnLongClickListener {
                store.showStatusDialog(context, onSuccess = { status, _ ->
                    status?.let {
                        store.statusNo = it.no
                        notifyItemChanged(adapterPosition)
                    }
                })
                return@setOnLongClickListener true
            }

            b.executePendingBindings()
            b.btnGrant.gone()   // 이걸 안 해주면 notify했을 때 이상하게 나와.


            b.executePendingBindings()
        }

        fun onClickCard(v: View) {
            OnMyClick.setDisableAWhileBTN(v)

            OnGrant.checkStatus(context, store, b.btnGrant,
                    onNormal = { store.justGo(context, true, arrayOf(b.tvTitle.tranPair(R.string.tran_linear))) },
//                    onAvailRequest = {  hhh(OnGrant.INTERVAL_BTN_SHOW) {if (it) b.btnGrant.visi() else b.btnGrant.gone()}  },
                    onDeleted = {
                        commonRemoveItem()
                    })
        }


    }


    /*********************************************************************************************
     * Horizontal 스토어
     *********************************************************************************************/
    inner class StoreHoriViewHolder(val b: HolderStoreHoriBinding) : RecyclerView.ViewHolder(b.root) {

        private lateinit var store: Store

        fun bind(store: Store) {
            this.store = store
            b.holder = this
            b.store = store

            b.executePendingBindings()
        }

        fun onClickCard(v: View) {
            OnMyClick.setDisableAWhileBTN(v)
            lllogI("onItemClick onClickCard  ")

            onItemClick?.let {
                lllogI("onItemClick onItemClick  store")
                it(store)
                return
            }

            OnStore.checkAndGo(context, false, store.key, null, Status.NORMAL,
                    arrayOf(b.line.tranPair(R.string.tran_linear)),
                    onSuccess = { },
                    onNotNormal = {
                        items[adapterPosition] = it
                        notifyItemChanged(adapterPosition)
                    })
        }

    }

    /*********************************************************************************************
     * Hori Big 스토어
     *********************************************************************************************/
    inner class StoreGridHoriHolder(val b: HolderStoreGridHoriBinding) : RecyclerView.ViewHolder(b.root) {

        private lateinit var store: Store

        fun bind(store: Store) {
            this.store = store
            b.holder = this
            b.store = store

            b.executePendingBindings()
        }

        fun onClickCard(v: View) {
            OnMyClick.setDisableAWhileBTN(v)
            lllogI("onItemClick onClickCard  ")

            onItemClick?.let {
                lllogI("onItemClick onItemClick  store")
                it(store)
                return
            }

            OnStore.checkAndGo(context, false, store.key, null, Status.NORMAL,
                    arrayOf(b.line.tranPair(R.string.tran_linear)),
                    onSuccess = { },
                    onNotNormal = {
                        items[adapterPosition] = it
                        notifyItemChanged(adapterPosition)
                    })
        }

    }

}