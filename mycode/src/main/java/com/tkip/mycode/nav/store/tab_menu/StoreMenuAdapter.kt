package com.tkip.mycode.nav.store.tab_menu

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.MotionEvent
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.RecyclerView
import com.tkip.mycode.R
import com.tkip.mycode.databinding.HolderStoreMenuBinding
import com.tkip.mycode.databinding.HolderStoreMenuOrderBinding
import com.tkip.mycode.dialog.store.StoreMenuAddDialog
import com.tkip.mycode.funs.common.lllogI
import com.tkip.mycode.funs.common.tranPair
import com.tkip.mycode.model.store.StoreMenu
import com.tkip.mycode.util.lib.MyTouchHelper
import com.tkip.mycode.util.lib.photo.photoview.OnPhotoView
import com.tkip.mycode.util.tools.etc.OnMyClick
import java.util.*


/**
 * actionType - 일반유저  or  수정하는 유저
 */
class StoreMenuAdapter(val items: ArrayList<StoreMenu>, val rvType: Int, val dragListener: OnStartDragListener?) : RecyclerView.Adapter<RecyclerView.ViewHolder>(), MyTouchHelper.OnMoveListener {

    private lateinit var activity: AppCompatActivity
//    private val binderHelper = ViewBinderHelper()

    companion object {
        const val rvNormal = 100
        const val rvOrder = 200
    }

    /***********************************************************************************************
     * 순서변경하는 interface
     ***********************************************************************************************/
    interface OnStartDragListener {
        fun onStartDrag(holder: StoreMenuOrderHolder)
    }

    override fun onItemMove(fromP: Int, toP: Int) {
        Collections.swap(items, fromP, toP)
        notifyItemMoved(fromP, toP)
    }

    override fun onItemSwipe(p: Int, direction: Int) {
        lllogI("dragtest   onItemSwipe   ")
//        items.removeAt(p)
//        notifyItemRemoved(p)
    }

    /****************************************************************************************/


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        activity = parent.context as AppCompatActivity
        return when (rvType) {
            rvOrder -> StoreMenuOrderHolder(HolderStoreMenuOrderBinding.inflate(LayoutInflater.from(parent.context), parent, false))
            rvNormal -> StoreMenuViewHolder(HolderStoreMenuBinding.inflate(LayoutInflater.from(parent.context), parent, false))
            else -> StoreMenuViewHolder(HolderStoreMenuBinding.inflate(LayoutInflater.from(parent.context), parent, false))
        }
    }

    override fun getItemId(position: Int): Long {
        return items[position].hashCode().toLong()
    }

    override fun onBindViewHolder(h: RecyclerView.ViewHolder, p: Int) {
        when (rvType) {
            rvOrder -> (h as StoreMenuOrderHolder).bind(items[p])
            rvNormal -> (h as StoreMenuViewHolder).bind(items[p])
            else -> (h as StoreMenuViewHolder).bind(items[p])
        }
    }

    override fun getItemCount(): Int {
        return items.size
    }

    /***********************************************************************************************
     * rvNormal - Store Activity 페이지
     ***********************************************************************************************/
    inner class StoreMenuViewHolder(val b: HolderStoreMenuBinding) : RecyclerView.ViewHolder(b.root) {

        private lateinit var storeMenu: StoreMenu

        fun bind(storeMenu: StoreMenu) {
            this.storeMenu = storeMenu
            b.holder = this
            b.storeMenu = storeMenu
            b.executePendingBindings()
        }

        fun onClickIv(v: View) {
            OnMyClick.setDisableAWhileBTN(v)
            OnPhotoView.gotoPhotoViewActivity(activity, storeMenu.photoThumb, storeMenu.photoUrl, arrayOf(v.tranPair(R.string.tran_image)))
        }

        fun onClickCard(v: View) {
            OnMyClick.setDisableAWhileBTN(v)
        }
    }


    /*******************************************************************************************
     * rvOrder - 스토어마스터가 메뉴 수정할 때
     *******************************************************************************************/
    inner class StoreMenuOrderHolder(val b: HolderStoreMenuOrderBinding) : RecyclerView.ViewHolder(b.root) {

        private lateinit var storeMenu: StoreMenu

        @SuppressLint("ClickableViewAccessibility")
        fun bind(storeMenu: StoreMenu) {
            this.storeMenu = storeMenu
            b.holder = this
            b.storeMenu = storeMenu
            b.executePendingBindings()

            b.ivDrag.setOnTouchListener { _, event ->
                lllogI("StoreMenuOrderHolder ivDrag")
                if (event.action == MotionEvent.ACTION_DOWN) {
                    dragListener?.onStartDrag(this)
                }
                return@setOnTouchListener false
            }

//            b.cons.setOnLongClickListener{
//                lllogI("StoreMenuOrderHolder cons")
//                return@setOnLongClickListener true
//            }
        }

        fun onClickIv(v: View) {
            OnMyClick.setDisableAWhileBTN(v)
            OnPhotoView.gotoPhotoViewActivity(activity, storeMenu.photoThumb, storeMenu.photoUrl, arrayOf(v.tranPair(R.string.tran_image)))
        }

        fun onClickCard(v: View) {
            OnMyClick.setDisableAWhileBTN(v)

            StoreMenuAddDialog.newInstance(storeMenu,
                    onOk = {
                        notifyItemChanged(adapterPosition)
                    }).show(activity.supportFragmentManager, "dialog")
        }

    }


    /**
     *  수정할 때
     */
//    inner class StoreMenuModifyViewHolder(val b: HolderStoreMenuModifyBinding) : RecyclerView.ViewHolder(b.root) {
//
//        private lateinit var storeMenu: StoreMenu
//
//        fun bind(storeMenu: StoreMenu) {
//            this.storeMenu = storeMenu
//            b.holder = this
//            b.actionType = actionType
//            b.store = store
//            b.storeMenu = storeMenu
//            b.executePendingBindings()
//
//
//            val spinnerStatusItems = OnSpinner.getStatusList()
//            b.spinnerStatus.adapter = SpinnerStatusAdapter(activity, spinnerStatusItems)
//            var check = 0
//            b.spinnerStatus.setListener(
//                    onItemSelected = {
//                        if (++check > 1 && OnPeer.isAdmin(false)) {
//                            OnSpinner.updateStoreMenuStatusDialog(activity, spinnerStatusItems[it], storeMenu, {}, deny = { notifyDataSetChanged() })
//                        }
//                    }, onNothingSelected = {})
//            b.spinnerStatus.setSelection(storeMenu.statusNo.getPosition(spinnerStatusItems))
//
//            OnSpinner.setWorking(b.spinnerStatus)
//
//
//            binderHelper.bind(b.swiper, storeMenu.key)
//            binderHelper.setOpenOnlyOne(true)
//
//            // swipe 작동 여부.
////            if (OnPeer.isNotAdmin(false)) {
////                if (storeMenu.statusNo.lessThan(Status.INSPECTING)) b.swiper.setLockDrag(true)
////            }
//
//            OnVisible.setSwipeHeight(b.line, b.lineBottom)   // height 맞추기
//        }
//
//        fun onClickCard() {
//            if (OnPeer.isAdmin(false) || OnPeer.isSamePeer(store.hostUID) || actionType == Cons.ACTION_TYPE_NEW || actionType == Cons.ACTION_TYPE_UPDATE || actionType == Cons.ACTION_TYPE_INSPECTION) {
//                if (b.swiper.isClosed) b.swiper.open(true)
//                else b.swiper.close(true)
//            }
//        }
//
////        fun onLongClick(): Boolean {
////            /** 딥삭제 */
////            return if (OnPeer.isAdminSupervisor(false)) {
////                ConfirmDialog.newInstance(title = null, desc = R.string.desc_deep_delete_basic.getStr(), isCancel = true, isCancelTouch = true,
////                        ok = { Firee.deleteStoreMenu(storeMenu, {}, {}) }, deny = {})
////                        .show(activity.supportFragmentManager, "dialog")
////                true
////            } else false
////        }
//
//        fun onClickDel() {
//            if (OnMyClick.isDoubleClick()) return
//            ConfirmDialog.newInstance(title = null, desc = Status.DELETED.dialogMsg, isCancel = true, isCancelTouch = true,
//                    ok = { Firee.deleteStoreMenu(storeMenu, {}, {}) }, deny = { })
//                    .show(activity.supportFragmentManager, "dialog")
//        }
//
//        fun onClickGo() {
//            if (OnMyClick.isDoubleClick()) return
//            // todo : 리플라이 볼 수 있는 페이지로 이동. 다이얼로그로 or 액티비티
//        }
//
//    }


}