package com.tkip.mycode.nav.board

import android.os.Bundle
import androidx.annotation.LayoutRes
import com.tkip.mycode.R
import com.tkip.mycode.databinding.ActivityBoardListBinding
import com.tkip.mycode.dialog.toast.toastNormal
import com.tkip.mycode.init.PUT_BOARD
import com.tkip.mycode.init.base.getStr
import com.tkip.mycode.init.base.getViewModel
import com.tkip.mycode.init.inter.interToolbarIcon
import com.tkip.mycode.init.my_bind.BindingActivity
import com.tkip.mycode.model.board.Board
import com.tkip.mycode.model.viewmodel.BoardViewModel

/**
 * 전체 보드리스트 보기
 */
class BoardListActivity : BindingActivity<ActivityBoardListBinding>() {
    @LayoutRes
    override fun getLayoutResId() = R.layout.activity_board_list

    private val board by lazy { intent.getParcelableExtra<Board>(PUT_BOARD) }
    private val vmBoard by lazy { getViewModel<BoardViewModel>() }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        initView()
        setFrag()

    }

    private fun initView() {
        b.activity = this

        /** Toolbar */
        b.inToolbar.title = R.string.title_board_list.getStr()
        b.inToolbar.backIcon = true
        b.inToolbar.inter = interToolbarIcon(onFirst = { onBackPressed() }, onTopTitle = {}, onTextMenu = { }, onTextMenu2 = null,onIcon = {}, onMore = {})



        b.sbb.onItemSelected={
            when (it) {
                0-> toastNormal("position $it")
                1-> toastNormal("position $it")
                2-> toastNormal("position $it")
                3-> toastNormal("position $it")
            }
        }




    }

    private fun setFrag() {
//        val run = Runnable {
//            val fragment = BoardFrag.newInstance(board)
//            val fragTran = supportFragmentManager.beginTransaction()
//
//            fragTran.replace(R.id.frame, fragment, fragment.javaClass.simpleName)
//            fragTran.commitAllowingStateLoss()
//        }
//        Handler().post(run)
    }




}