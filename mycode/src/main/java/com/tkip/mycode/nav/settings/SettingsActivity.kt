package com.tkip.mycode.nav.settings

import android.os.Bundle
import androidx.annotation.LayoutRes
import com.tkip.mycode.R
import com.tkip.mycode.admin.menu.AMenu
import com.tkip.mycode.auth.OnSign
import com.tkip.mycode.databinding.ActivitySettingsBinding
import com.tkip.mycode.dialog.alert.MyAlert
import com.tkip.mycode.dialog.toast.toastNormal
import com.tkip.mycode.funs.common.moveToWeb
import com.tkip.mycode.funs.common.remoteBool
import com.tkip.mycode.funs.common.remoteStr
import com.tkip.mycode.funs.custom.menu.ViewMenuBox
import com.tkip.mycode.funs.custom.menu.ViewMenuNormal
import com.tkip.mycode.funs.custom.toolbar.ViewToolbarEmpty
import com.tkip.mycode.init.BOARD_KEY_ALLIANCE
import com.tkip.mycode.init.BOARD_KEY_SUGGEST
import com.tkip.mycode.init.base.getStr
import com.tkip.mycode.init.base.lt
import com.tkip.mycode.init.my_bind.BindingActivity
import com.tkip.mycode.init.part.CodeApp
import com.tkip.mycode.model.board.OnBoard
import com.tkip.mycode.model.cost.credit.OnCredit
import com.tkip.mycode.model.my_enum.PeerType
import com.tkip.mycode.model.peer.OnPeer
import com.tkip.mycode.model.peer.recommend.OnRecommend
import com.tkip.mycode.nav.settings.push_time.OnPushTime

/**
 */
class SettingsActivity : BindingActivity<ActivitySettingsBinding>() {
    @LayoutRes
    override fun getLayoutResId() = R.layout.activity_settings

    private lateinit var viewToolbar: ViewToolbarEmpty

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        initView()
    }

    private fun initView() {
        b.activity = this


        /** Toolbar */
        viewToolbar = ViewToolbarEmpty(this@SettingsActivity,
                R.string.title_settings.getStr(),
                onBack = { onBackPressed() },
                onMore = null
        ).apply { this@SettingsActivity.b.frameToolbar.addView(this) }


        /*** 메뉴 붙이기  */
        setupBelowMenu()
    }


    private fun setupBelowMenu() {

        b.lineBelow.removeAllViews()
        if (OnPeer.peer.type.lt(PeerType.BASIC_2.no)) {   // 익명로그인 일때

            /*** 익명계정 */
            val group1 = ViewMenuBox(this@SettingsActivity, R.string.menu_group_title_my_data.getStr())
                    .apply { this.b.line.addView(ViewMenuNormal(this@SettingsActivity, null, AMenu.SIGN_OUT_CHANGE, onClick = { _, _ -> OnSign.askSignOut(this@SettingsActivity) })) }

            val group2 = ViewMenuBox(this@SettingsActivity, R.string.menu_group_title_appdata.getStr())
                    .apply {
                        this.b.line.addView(ViewMenuNormal(this@SettingsActivity, null, AMenu.TERMS_AND_CONDITIONS, onClick = { _, _ -> moveToWeb(R.string.remote_operation_policy.remoteStr()) }))
                        this.b.line.addView(ViewMenuNormal(this@SettingsActivity, null, AMenu.PRIVACY_POLICY, onClick = { _, _ -> moveToWeb(R.string.remote_privacy_policy.remoteStr()) }))
                        this.b.line.addView(ViewMenuNormal(this@SettingsActivity, null, AMenu.APP_INFO, onClick = { aMenu, _ -> MyAlert.showInfo(this@SettingsActivity, aMenu.title, CodeApp.versionName) }))
                        this.b.line.addView(ViewMenuNormal(this@SettingsActivity, null, AMenu.INFO_LIBRARY, onClick = { aMenu, _ -> MyAlert.showInfoLibrary(this@SettingsActivity, aMenu.title, null) }))
                    }

            b.lineBelow.addView(group1)
            b.lineBelow.addView(group2)

        } else {

            /*** 일반계정 */
            val group1 = ViewMenuBox(this@SettingsActivity, R.string.menu_group_title_my_data.getStr())
                    .apply {
//                        this.b.line.addView(ViewMenuNormal(this@SettingsActivity,null, AMenu.MY_CREDIT_LIST, onClick = { _, _ -> OnCost.openDialog(this@SettingsActivity, mypeer()) }))
                        this.b.line.addView(ViewMenuNormal(this@SettingsActivity, null, AMenu.CREDIT_MENU, onClick = { _, _ -> OnCredit.gotoCreditMenuActivity(this@SettingsActivity, null) }))
                        this.b.line.addView(ViewMenuNormal(this@SettingsActivity, null, AMenu.PUSH_TIME, onClick = { _, _ -> OnPushTime.gotoPushTimeActivity(this@SettingsActivity, null) }))
                        this.b.line.addView(ViewMenuNormal(this@SettingsActivity, null, AMenu.ADVERTISEMENT, onClick = { _, _ -> AMenu.ADVERTISEMENT.onClickMenu(this@SettingsActivity, null) }))
//                        this.b.line.addView(ViewHoriBinding.inflate(LayoutInflater.from(this@SettingsActivity)).root)
                    }

            val group2 = ViewMenuBox(this@SettingsActivity, R.string.menu_group_title_appdata.getStr())
                    .apply {
                        this.b.line.addView(ViewMenuNormal(this@SettingsActivity, null, AMenu.SUGGEST, onClick = { _, arr -> OnBoard.gotoBoardActivity(this@SettingsActivity, true, null, BOARD_KEY_SUGGEST, arr) }))
                        this.b.line.addView(ViewMenuNormal(this@SettingsActivity, null, AMenu.ALLIANCE, onClick = { _, arr -> OnBoard.gotoBoardActivity(this@SettingsActivity, true, null, BOARD_KEY_ALLIANCE, arr) }))
                        this.b.line.addView(ViewMenuNormal(this@SettingsActivity, null, AMenu.TERMS_AND_CONDITIONS, onClick = { _, _ -> moveToWeb(R.string.remote_operation_policy.remoteStr()) }))
                        this.b.line.addView(ViewMenuNormal(this@SettingsActivity, null, AMenu.PRIVACY_POLICY, onClick = { _, _ -> moveToWeb(R.string.remote_privacy_policy.remoteStr()) }))
                        this.b.line.addView(ViewMenuNormal(this@SettingsActivity, null, AMenu.APP_INFO, onClick = { aMenu, _ -> MyAlert.showInfo(this@SettingsActivity, aMenu.title, CodeApp.versionName) }))
                        this.b.line.addView(ViewMenuNormal(this@SettingsActivity, null, AMenu.INFO_LIBRARY, onClick = { aMenu, _ -> MyAlert.showInfoLibrary(this@SettingsActivity, aMenu.title, null) }))
                    }

            val group3 = ViewMenuBox(this@SettingsActivity, R.string.menu_group_title_my_action.getStr())
                    .apply {
                        if (R.string.remote_on_recommendation.remoteBool()) this.b.line.addView(ViewMenuNormal(this@SettingsActivity, null, AMenu.ADD_RECOMMENDER, onClick = { _, _ -> OnRecommend.checkRecommend(this@SettingsActivity) }))
                        this.b.line.addView(ViewMenuNormal(this@SettingsActivity, null, AMenu.SIGN_OUT, onClick = { _, _ -> OnSign.askSignOut(this@SettingsActivity) }))
                        this.b.line.addView(ViewMenuNormal(this@SettingsActivity, null, AMenu.DELETE_PEER, onClick = { _, _ -> OnSign.deletePeer(this@SettingsActivity) }))
                    }

            b.lineBelow.addView(group1)
            b.lineBelow.addView(group2)
            b.lineBelow.addView(group3)


//            b.lineBelow.addView(ViewTvSubtitle(this@SettingsActivity,R.string.menu_group_title_my_data.getStr()))
//            b.lineBelow.addView(ViewMenuNormal(this@SettingsActivity, AMenu.MY_PAY_LIST, onClick = { OnCost.openDialog(this@SettingsActivity, mypeer()) }))
//            b.lineBelow.addView(ViewMenuNormal(this@SettingsActivity, AMenu.PUSH_TIME, onClick = { OnPushTime.gotoPushTimeActivity(this@SettingsActivity, null) }))
//            b.lineBelow.addView(ViewMenuNormal(this@SettingsActivity, AMenu.SUGGEST, onClick = { developing() }))
//            b.lineBelow.addView(ViewMenuNormal(this@SettingsActivity, AMenu.INVESTMENT, onClick = { developing() }))
//
//            b.lineBelow.addView(ViewTvSubtitle(this@SettingsActivity,R.string.menu_group_title_appdata.getStr()))
//            b.lineBelow.addView(ViewMenuNormal(this@SettingsActivity, AMenu.INFORMATION, onClick = { MyAlert.showInfo(this@SettingsActivity, R.string.title_app_info.getStr(), BuildConfig.VERSION_NAME) }))
//            b.lineBelow.addView(ViewMenuNormal(this@SettingsActivity, AMenu.TERMS_AND_CONDITIONS, onClick = { moveToWeb(R.string.remote_operation_policy.remoteStr()) }))
//            b.lineBelow.addView(ViewMenuNormal(this@SettingsActivity, AMenu.PRIVACY_POLICY, onClick = { moveToWeb(R.string.remote_privacy_policy.remoteStr()) }))
//
//            b.lineBelow.addView(ViewTvSubtitle(this@SettingsActivity,R.string.menu_group_title_my_action.getStr()))
//            b.lineBelow.addView(ViewMenuNormal(this@SettingsActivity, AMenu.SIGN_OUT, onClick = { OnSign.signOut(this@SettingsActivity) }))
//            b.lineBelow.addView(ViewMenuNormal(this@SettingsActivity, AMenu.DELETE_PEER, onClick = { OnSign.deletePeer(this@SettingsActivity) }))
        }

    }

    private fun developing() {
        toastNormal(R.string.info_prepare_developing)
    }


}