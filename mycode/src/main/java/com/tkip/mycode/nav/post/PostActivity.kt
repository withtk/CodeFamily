package com.tkip.mycode.nav.post

import android.os.Bundle
import android.os.Handler
import android.view.View
import androidx.annotation.LayoutRes
import com.tkip.mycode.R
import com.tkip.mycode.admin.menu.AMenu
import com.tkip.mycode.admin.menu.OnMenu
import com.tkip.mycode.databinding.ActivityPostBinding
import com.tkip.mycode.dialog.alert.MyAlert
import com.tkip.mycode.dialog.progress.MyCircle
import com.tkip.mycode.dialog.progress.WAIT_TYPE
import com.tkip.mycode.dialog.toast.toastNormal
import com.tkip.mycode.funs.common.*
import com.tkip.mycode.funs.custom.ViewAuthorTitle
import com.tkip.mycode.funs.custom.toolbar.ViewToolbar
import com.tkip.mycode.init.*
import com.tkip.mycode.init.base.getViewModel
import com.tkip.mycode.init.inter.interActivityResult
import com.tkip.mycode.init.inter.interMenuBox
import com.tkip.mycode.init.inter.interRequestPermissionsResult
import com.tkip.mycode.init.my_bind.BindingActivityPhoto
import com.tkip.mycode.init.part.CodeApp
import com.tkip.mycode.model.board.OnBoard
import com.tkip.mycode.model.cost.Cost
import com.tkip.mycode.model.count.CountType
import com.tkip.mycode.model.count.CountViewModel
import com.tkip.mycode.model.my_enum.*
import com.tkip.mycode.model.mytag.CustomTagList
import com.tkip.mycode.model.peer.isCsBoard
import com.tkip.mycode.model.post.OnPost
import com.tkip.mycode.model.post.Post
import com.tkip.mycode.model.reply.LinearReplyAddBox
import com.tkip.mycode.model.reply.OnReply
import com.tkip.mycode.model.toss.OnToss
import com.tkip.mycode.model.warn.OnWarn
import com.tkip.mycode.util.lib.retrofit2.ESget
import com.tkip.mycode.util.tools.anim.AnimUtil
import com.tkip.mycode.util.tools.fire.commonStatusUpdate
import com.tkip.mycode.util.tools.fire.real
import com.tkip.mycode.util.tools.recycler.OnRv

/**
 * // todo : onStop시에 모두 초기화 할 것....  왜..?
 * photoUtil에서 모두 한개로 하지말고...?
 */
class PostActivity : BindingActivityPhoto<ActivityPostBinding>() {
    @LayoutRes
    override fun getLayoutResId() = R.layout.activity_post

    private val addType by lazy { intent.getIntExtra(PUT_ACTION, ACTION_NOTHING) }   // '올린 직후'를 캐치해야 한다.
    //    private val vmPost by lazy { getViewModel<PostViewModel>() }
    private val vmCount by lazy { getViewModel<CountViewModel>() }
    private val postKey by lazy { intent.getStringExtra(PUT_KEY) }   // 기준 : 이게 null이냐 아니냐에 따라..
    //    private val board by lazy { intent.getParcelableExtra<Board>(PUT_BOARD) }
    private val putPost by lazy { intent.getParcelableExtra<Post>(PUT_POST) ?: Post() }
    private lateinit var viewToolbar: ViewToolbar
    private lateinit var post: Post
//    private lateinit var board: Board  // 필요한 부분 : 1. toss 버튼 visi 2. boardActi 이동  3. onMore 메뉴 설정. 4. 조회수 및 댓글수

    override fun onResume() {
        super.onResume()
        CodeApp.curModelKey = if (::post.isInitialized) post.key else null
    }

    override fun onStop() {
        super.onStop()
        CodeApp.curModelKey = null
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        lllogD("PostActivity onCreate postKey $postKey ")
        if (postKey == null) {
            post = putPost
            beforeInit( )

//            MyCircle.cancel()
        } else {
            WAIT_TYPE.FULL_CIRCLE.show(this@PostActivity)
            ESget.getPost(postKey,
                    success = {
                        it?.let { post1 ->
                            lllogD("PostActivity onCreate post1 $post1 ")

                            /**
                             *  // todo : 이 부분 다시
                             *  정상 아니면 alert로 알려주고.
                             *  author이면 .. author에 맞게 보여주고.
                             *
                             *  initview를 먼저 하고 나서 데이터를 넣어야 되는데..? 왜 이렇게 짰었지..?
                             */
//                            if (post1.isValidStatus(Status.NORMAL)) {
                                post = post1
                                beforeInit( )
                                CodeApp.curModelKey = post.key
//                            }
                        }
                        MyCircle.cancel()

                    }, fail = {})
        }
    }

    private fun beforeInit( ) {
        initView()
        initData()

        Cost.new(null,ActType.OPEN_POST,post).real()

        /** 보드 받아오기 */
//        ESget.getBoard(boardKey, success = {
//            if (it != null) {
//                board = it
//                initView()
//                initData()
//            }
//        }, fail = { })
    }

    private fun initView() {
        b.activity = this

        /** Toolbar */
        viewToolbar = ViewToolbar(this@PostActivity,
                post.boardName, post.title, post.addDate,
                onBack = { onBackPressed() },
                onBoardTitle = { ESget.getBoard(post.boardKey, { OnBoard.gotoBoardActivity(this@PostActivity, false, null, it, null) }, {}) },
                onMore = { onClickMore() })
        b.frameToolbar.addView(viewToolbar)

        /**   * CH_VIEW_UID_COUNT 추가   */
//        vmPost.addViewUID(post, Cntch.VIEW_UID) // addDate가 매번 바뀌기때문에 functions 트리거 작동됨.

        /**  글쓴이 author check anonymous  */
        //        ViewUtil.addViewAuthorTitle(this@PostActivity, b.frameAuthorTitle, post.anonymous, post.authorUID, post.addDate, {})
        b.lineWhite.addView(ViewAuthorTitle(this@PostActivity, post.anonymous, post.authorUID, post.addDate, onPeer = {}), 0)

        /**  top 사진 rv  */
        OnRv.setTopFotoAdapter(post.fotoList, b.rvFoto)

        /**  검색 태그리스트   */
        if (!post.tagList.isNullOrEmpty()) b.frameTagList.addView(CustomTagList(this@PostActivity, post.tagList))

        /** 업데이트 data & 조회수 댓글수   */
        b.inViewCnt.updateDate = post.updateDate
        if (!post.isCsBoard()) {
            b.inViewCnt.viewCount = post.viewCount
            //        b.inViewCount.bookCount = post.bookmarkCount
            b.inViewCnt.replyCount = post.replyCount
            //        b.inViewCount.pushCount = post.pushCount
        }

        /********************************** inInterface ***********************************/
        /**   Count CheckBox 세팅    */
        vmCount.initCount(this@PostActivity, post, arrayOf(CountType.VIEW_UID, CountType.UP, CountType.DOWN, CountType.BOOK, CountType.PUSH), b.inMenuBox.cbThumbUp, b.inMenuBox.cbThumbDown, b.inMenuBox.cbBookmark, b.inMenuBox.cbPush)

        /**   CheckBox 세팅    */
//        vmPost.getAllEsCount(post)
//        vmPost.liveThumbUp.observe(this, Observer {
//            b.inMenuBox.cbThumbUp.isChecked = it != null
//            b.inMenuBox.cbThumbUp.isEnabled = true
//        })
//        vmPost.liveThumbDown.observe(this, Observer {
//            b.inMenuBox.cbThumbDown.isChecked = it != null
//            b.inMenuBox.cbThumbDown.isEnabled = true
//        })
//        vmPost.liveBookmark.observe(this, Observer {
//            b.inMenuBox.cbBookmark.isChecked = it != null
//            b.inMenuBox.cbBookmark.isEnabled = true
//        })
//        vmPost.livePush.observe(this, Observer {
//            b.inMenuBox.cbPush.isChecked = it != null
//            b.inMenuBox.cbPush.isEnabled = true
//        })

        /**  메뉴박스   */
        if (!post.isCsBoard()) {
            b.inMenuBox.thumbUp = post.thumbUpCount
            b.inMenuBox.thumbDown = post.thumbDownCount
        }
        b.inMenuBox.bookmark = post.bookmarkCount
        b.inMenuBox.push = post.pushCount
        b.inMenuBox.boardTypeNo = post.boardTypeNo
        b.inMenuBox.inter = interMenuBox(
                onClickThumbUp = { vmCount.updateCount(post, CountType.UP, it) },
                onClickThumbDown = { vmCount.updateCount(post, CountType.DOWN, it) },
                onClickBookmark = { vmCount.updateCount(post, CountType.BOOK, it) },
                onClickPush = { vmCount.updateCount(post, CountType.PUSH, it) },
                onClickToss = { OnToss.checkAdd(this@PostActivity, post) },
                onClickMore = { onClickMore() }
        )

        /**   * 글 작성 직후일때 : 1초 후 자동으로 PUSH 추가. (문제):재성성시 한번 더 클릭되어 -1이 됨. */
        if (addType == ACTION_NEW) Handler().postDelayed({ toastNormal(R.string.info_set_push) }, 1000)


        /**  댓글 첨부박스  */
        linearReplyAddBox = LinearReplyAddBox(this@PostActivity, post.allowReply, post.key,
                onClickAddReply = { linearReplyAddBox.checkAddReply(this@PostActivity, post, replyAdapter, success = { b.nestedScrollView.fullScroll(View.FOCUS_DOWN) }, fail = {}) })
                .apply { this@PostActivity.b.frameReplyBox.addView(this) }

//        ViewUtil.addLinearReplyAddBox(this@PostActivity, b.frameReplyBox, post.key,
//                onClickAddReply = {
//                    linearReplyAddBox.b.checkAddReply(this@PostActivity, post, replyAdapter,
//                            success = { b.nestedScrollView.fullScroll(View.FOCUS_DOWN) }, fail = {})
//                })

        /** reply  */
        replyAdapter = OnReply.setAdapter(this@PostActivity, EES_POST, post.key, EES_REPLY, post, b.inRvReply, linearReplyAddBox,
                onCompleteAdoption = {
                    post.chosen = true
                    b.post = post
                    AnimUtil.hideSlideDown(b.tvInfo, false)
                })


        /********************* superListener *****************************/
        listenerActivityResult = interActivityResult(
                onPick = {
                    lllogI("listenerActivityResult onPick  $it")
                    linearReplyAddBox.addPhotoUri(it)
//                    it?.let { uri ->
//                        ViewUtil.addViewPhotoUri(this@PostActivity, linearReplyAddBox.b.lineAttachBox, uri, onPhoto = {}, onClose = {})
//                        linearReplyAddBox.b.btnAdd?.visi()
//                    }
                }, onAlbum = {}, onMap = { _, _ -> }, onBanner = null, onData = null)

        listenerPermissionsResult = interRequestPermissionsResult(onReadExternalStorage = { OnPermission.onOpenGallery(this@PostActivity, it, false) })


        /*** Link Preview */
//        OnFunction.setUrlPreview(this@PostActivity, post.comment, b.lineLink)
    }

    private fun initData() {
        b.post = post

        /*** Rich프리뷰 */
        OnFunction.setUrlPreview(this@PostActivity, post.comment, b.lineLink, onSuccess = {
            //            url ->
//            post.comment?.replace(url, "")?.let { b.tvContents.text = it }
        })
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        /**  * 재생성시 그냥 초기화 */
        OnReply.resetAttachReply(linearReplyAddBox.b.lineAttachBox)
    }


    fun onClickMore() {
        MyAlert.showMenuList(this@PostActivity, OnMenu.getMENULIST(post),
                onSelect = { amenu ->
                    when (amenu) {
                        AMenu.UPDATE -> OnPost.gotoAddPostActivity(this@PostActivity, ACTION_UPDATE, post, arrayOf(b.tvTitle.tranPair(R.string.tran_title), b.tvContents.tranPair(R.string.tran_contents)))
                        AMenu.DELETION -> OnReply.deleteModel(this@PostActivity, post.replyCount,
                                onAnonymous = { OnPost.setAnonymous(this@PostActivity, post, success = { b.post = post }, fail = { }) },
                                onGotoDelete = { commonStatusUpdate(EES_POST, post.key, Status.DELETED_USER, success = { finish() }, fail = {}) })
                        AMenu.WARN -> OnWarn.checkAndAdd(this@PostActivity, post, success = {}, fail = {})
                        AMenu.DEEP_DEL -> post.deepDel(this@PostActivity, onSuccess = {})
                        AMenu.STATUS -> post.showStatusDialog(this@PostActivity,null)
                        else -> errorWhen()
                    }
                })
    }

}