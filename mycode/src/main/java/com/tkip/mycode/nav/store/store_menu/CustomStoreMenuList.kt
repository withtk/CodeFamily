package com.tkip.mycode.nav.store.store_menu

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.widget.LinearLayout
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.ItemTouchHelper
import com.tkip.mycode.R
import com.tkip.mycode.databinding.CustomModelListBinding
import com.tkip.mycode.funs.common.lllogI
import com.tkip.mycode.init.MAX_STORE_MENU_SIZE
import com.tkip.mycode.init.base.getStr
import com.tkip.mycode.model.count.Count
import com.tkip.mycode.model.my_enum.Status
import com.tkip.mycode.model.store.StoreMenu
import com.tkip.mycode.nav.store.tab_menu.StoreMenuAdapter
import com.tkip.mycode.util.lib.MyTouchHelper
import com.tkip.mycode.util.lib.retrofit2.ESget
import com.tkip.mycode.util.lib.retrofit2.ESquery
import com.tkip.mycode.util.tools.recycler.OnRv

/**
 * 스토어 메뉴 리스트
 */
class CustomStoreMenuList : LinearLayout {
    private lateinit var b: CustomModelListBinding
    lateinit var storeKey: String

    val items = arrayListOf<StoreMenu>()
    private lateinit var adapter: StoreMenuAdapter
    private lateinit var itemTouchHelper: ItemTouchHelper

    private var esSize = 15
    private var esLastIndex = 0


    constructor(context: Context) : super(context)
    constructor(context: Context, storeKey: String, rvType: Int, onItem: ((Count) -> Unit)?) : super(context) {
        b = DataBindingUtil.inflate(LayoutInflater.from(context), R.layout.custom_model_list, this, true)
        this.storeKey = storeKey

        /** set Adapter */
        if (rvType == StoreMenuAdapter.rvNormal) adapter = StoreMenuAdapter(items, rvType, null)
        else {
            esSize = MAX_STORE_MENU_SIZE
            adapter = StoreMenuAdapter(items, rvType, object : StoreMenuAdapter.OnStartDragListener {
                override fun onStartDrag(holder: StoreMenuAdapter.StoreMenuOrderHolder) {
                    itemTouchHelper.startDrag(holder)
                }
            })
            itemTouchHelper = ItemTouchHelper(MyTouchHelper(adapter))
            itemTouchHelper.attachToRecyclerView(b.inRefresh.rv)
        }


        b.inRefresh.rv.adapter = adapter
        //        SnapHelper().attachToRecyclerView(b.inRefresh.rv)   // 스냅헬퍼

        b.inRefresh.inData.tvNoData.text = R.string.info_no_store_menu.getStr()
        b.inRefresh.tvLast.text = R.string.info_last_msg_store_menu.getStr()

        b.inRefresh.refresh.setOnRefreshListener { getEsDataList(0) }
        b.inRefresh.refresh.setOnLoadMoreListener { getEsDataList(esLastIndex) }
        b.inRefresh.refresh.setEnableRefresh((rvType == StoreMenuAdapter.rvOrder))   //상단 refresh : rvOrder 일때만 가능하게.
//        b.inRefresh.refresh.setEnableLoadMore(false)  // 하단 loadMore 불능.


//        getEsDataList(0)

    }


    /** * 가져오기  */
    fun getEsDataList(from: Int) {

        val query = ESquery.storeMenuList(esSize, from, storeKey, Status.INSPECTING)
        ESget.getStoreMenuList(query,
                success = {
                    lllogI("CustomCountList getEsDataList size   : ${it?.size}")
//                    esLastIndex = OnRv.setRvList2(it,from, items, b.inData, R.string.info_no_my_banner.getStr(), b.refresh,b.tvLast)
                    esLastIndex = OnRv.setRvRefresh(it, from, items, b.inRefresh, true)
                    adapter.notifyDataSetChanged()
                },
                fail = {})

    }

    fun addNewItem(storeMenu: StoreMenu) {
        items.add(storeMenu)
        adapter.notifyDataSetChanged()
        b.inRefresh.nScroll.fullScroll(View.FOCUS_DOWN)
    }

}