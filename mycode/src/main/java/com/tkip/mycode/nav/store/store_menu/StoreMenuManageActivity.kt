package com.tkip.mycode.nav.store.store_menu

import android.os.Bundle
import android.view.View
import androidx.annotation.LayoutRes
import com.tkip.mycode.R
import com.tkip.mycode.databinding.ActivityStoreMenuManageBinding
import com.tkip.mycode.dialog.alert.MyAlert
import com.tkip.mycode.dialog.store.StoreMenuAddDialog
import com.tkip.mycode.dialog.toast.toastNormal
import com.tkip.mycode.funs.common.lllogI
import com.tkip.mycode.init.EES_STORE
import com.tkip.mycode.init.EES_STORE_MENU
import com.tkip.mycode.init.MAX_STORE_MENU_SIZE
import com.tkip.mycode.init.PUT_STORE
import com.tkip.mycode.init.base.getStr
import com.tkip.mycode.init.inter.interToolbarIcon
import com.tkip.mycode.init.my_bind.BindingActivity
import com.tkip.mycode.model.store.Store
import com.tkip.mycode.model.store.StoreMenu
import com.tkip.mycode.nav.store.tab_menu.StoreMenuAdapter
import com.tkip.mycode.util.tools.etc.OnMyClick
import com.tkip.mycode.util.tools.fire.batch
import com.tkip.mycode.util.tools.fire.docRef
import com.tkip.mycode.util.tools.fire.myCommit

class StoreMenuManageActivity : BindingActivity<ActivityStoreMenuManageBinding>() {
    @LayoutRes
    override fun getLayoutResId() = R.layout.activity_store_menu_manage

    private val store by lazy { intent.getParcelableExtra(PUT_STORE) ?: Store() }
    private val oldItems = arrayListOf<StoreMenu>()

    private lateinit var customStoreMenuList: CustomStoreMenuList

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        initView()
    }

    private fun initView() {
        b.activity = this

        /** 툴바 */
        b.inToolbar.title = R.string.title_store_menu_manage.getStr()
        b.inToolbar.backIcon = true
        b.inToolbar.menuText = R.string.add_store_menu_manage.getStr()
        b.inToolbar.inter = interToolbarIcon(
                onFirst = { onBackPressed() }, onTopTitle = {},
                onTextMenu = { onClickAdd(it) },onTextMenu2 = null, onIcon = {}, onMore = {}
        )

        setEsList()

    }

    private fun setEsList() {
        customStoreMenuList = CustomStoreMenuList(this@StoreMenuManageActivity, store.key,StoreMenuAdapter.rvOrder, onItem = {})
        customStoreMenuList.getEsDataList(0)
        b.frame.addView(customStoreMenuList)
    }


    fun onClickAdd(v: View) {
        OnMyClick.setDisableAWhileBTN(v)
        /*** 메뉴 개수 체크  */
        if (customStoreMenuList.items.size >= MAX_STORE_MENU_SIZE) MyAlert.showConfirm(this@StoreMenuManageActivity, R.string.info_limit_store_menu.getStr())
        else StoreMenuAddDialog.newInstance(StoreMenu(store),
                onOk = {
                    customStoreMenuList.addNewItem(it)
                }).show(this@StoreMenuManageActivity.supportFragmentManager, "dialog")
    }

    fun onClickSave(v: View) {
        OnMyClick.setDisableAWhileBTN(v)
        if (isDiff())
            MyAlert.showConfirm(this@StoreMenuManageActivity, R.string.info_wanna_upload_quick.getStr(),
                    ok = {
                        lllogI("QuickManageActivity  ok onClickSave")
                        batch().apply {
                            customStoreMenuList.items.forEachIndexed { i, storeMenu -> set(docRef(EES_STORE, store.key, EES_STORE_MENU, storeMenu.key), storeMenu) }
                            myCommit("QuickManageActivity onClickSave", success = {}, fail = {})
                        }
                        super.onBackPressed()
                    }, cancel = {
                lllogI("QuickManageActivity  cancel onClickSave")
            })
        else toastNormal(R.string.info_storemenu_nochange)
    }

    /**
     * 변경여부 : true = 변경됨.
     */
    private fun isDiff(): Boolean {
        oldItems.forEachIndexed { i, old ->
            if (old.order != customStoreMenuList.items[i].order) return true
        }
        return false
    }


}
