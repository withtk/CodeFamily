package com.tkip.mycode.nav.room

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.tkip.mycode.R
import com.tkip.mycode.databinding.HolderRoomGridVertBinding
import com.tkip.mycode.databinding.HolderRoomHoriBinding
import com.tkip.mycode.databinding.HolderRoomMyConBinding
import com.tkip.mycode.databinding.HolderRoomVertBinding
import com.tkip.mycode.dialog.alert.MyAlert
import com.tkip.mycode.dialog.toast.milkStatusAdmin
import com.tkip.mycode.funs.common.tranPair
import com.tkip.mycode.init.*
import com.tkip.mycode.init.inter.interHolderBasic
import com.tkip.mycode.model.grant.OnGrant
import com.tkip.mycode.model.my_enum.Status
import com.tkip.mycode.model.my_enum.getStatus
import com.tkip.mycode.model.my_enum.justGo
import com.tkip.mycode.model.my_enum.showStatusDialog
import com.tkip.mycode.model.peer.isAdmin
import com.tkip.mycode.model.room.OnRoom
import com.tkip.mycode.model.room.Room
import com.tkip.mycode.util.tools.anim.gone
import com.tkip.mycode.util.tools.etc.OnMyClick

class RoomAdapter(private val items: ArrayList<Room>, val rvType: Int, val onItemClick: ((Room) -> Unit)?) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {
//    companion object {
//        const val rvVert = 50
//        const val rvGridVert = 100
//        const val rvHori = 200
//        const    val rvMyContents=500
//    }

    private lateinit var context: Context

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        this.context = parent.context
        return when (rvType) {
            RV_GRID_VERT -> RoomGridVertHolder(HolderRoomGridVertBinding.inflate(LayoutInflater.from(parent.context), parent, false))
            RV_HORI -> RoomHoriViewHolder(HolderRoomHoriBinding.inflate(LayoutInflater.from(parent.context), parent, false))
            RV_VERT -> RoomVertViewHolder(HolderRoomVertBinding.inflate(LayoutInflater.from(parent.context), parent, false))
            RV_MY_CONTENTS, RV_MY_CONTENTS_MANAGE, RV_MANAGE -> RoomMyConViewHolder(HolderRoomMyConBinding.inflate(LayoutInflater.from(parent.context), parent, false))
            else -> RoomVertViewHolder(HolderRoomVertBinding.inflate(LayoutInflater.from(parent.context), parent, false))
        }
    }

    override fun onBindViewHolder(h: RecyclerView.ViewHolder, p: Int) {
        when (rvType) {
            RV_GRID_VERT -> (h as RoomGridVertHolder).bind(items[p])
            RV_HORI -> (h as RoomHoriViewHolder).bind(items[p])
            RV_VERT -> (h as RoomVertViewHolder).bind(items[p])
            RV_MY_CONTENTS, RV_MY_CONTENTS_MANAGE, RV_MANAGE -> (h as RoomMyConViewHolder).bind(items[p])
            else -> (h as RoomVertViewHolder).bind(items[p])
        }
    }

    override fun getItemCount(): Int {
        return items.size
    }

    /*******************************************************************************************
     * vert
     *******************************************************************************************/
    inner class RoomGridVertHolder(val b: HolderRoomGridVertBinding) : RecyclerView.ViewHolder(b.root) {

        private lateinit var room: Room

        fun bind(room: Room) {
            this.room = room
//            b.holder = this
            b.room = room

            b.inter = interHolderBasic(
                    onClickCard = { onClickCard(it) },
                    onClickGo = null,
                    onClickUpdate = { },
                    onClickDel = { }, onDeepDel = {},
                    onClickStatus = { onClickStatus(it) })

            b.executePendingBindings()
        }

        fun onClickCard(v: View) {
            OnMyClick.setDisableAWhileBTN(v)

            onItemClick?.let {
                it(room)
                return
            }

            OnRoom.checkAndGo(context, false, room.key, ACTION_NORMAL, Status.NORMAL,
                    arrayOf(
                            b.line.tranPair(R.string.tran_linear)
//                            b.iv1.tranPair(R.string.tran_image),
//                            b.tvPrice.tranPair(R.string.tran_room_price),
//                            b.tvContractType.tranPair(R.string.tran_contract_type),
//                            b.tvRoomType.tranPair(R.string.tran_room_type)
                    ),
                    null,
                    onNotNormal = {
                        items[adapterPosition] = it
                        notifyItemChanged(adapterPosition)
                    })

        }


        fun onClickStatus(v: View) {
            OnMyClick.setDisableAWhileBTN(v)
            if (isAdmin()) {
                milkStatusAdmin(room.statusNo.getStatus().msg)
                OnRoom.checkAndGo(context, false, room.key, ACTION_NORMAL, Status.NOTHING,
                        arrayOf(b.line.tranPair(R.string.tran_linear)), null, null)
            }
        }

    }

    /*******************************************************************************************
     * for searchTotal
     *******************************************************************************************/
    inner class RoomVertViewHolder(val b: HolderRoomVertBinding) : RecyclerView.ViewHolder(b.root) {

        private lateinit var room: Room

        fun bind(room: Room) {
            this.room = room
            b.room = room
            b.inter = interHolderBasic(
                    onClickCard = { onClickCard(it) },
                    onClickGo = null,
                    onClickUpdate = { },
                    onClickDel = { }, onDeepDel = {},
                    onClickStatus = { })
            b.executePendingBindings()
        }

        fun onClickCard(v: View) {
            OnMyClick.setDisableAWhileBTN(v)

            onItemClick?.let {
                it(room)
                return
            }

            OnRoom.checkAndGo(context, false, room.key, ACTION_NORMAL, Status.NORMAL,
                    arrayOf(
                            b.line.tranPair(R.string.tran_linear)
//                            b.iv1.tranPair(R.string.tran_image),
//                            b.tvPrice.tranPair(R.string.tran_room_price),
//                            b.tvContractType.tranPair(R.string.tran_contract_type),
//                            b.tvRoomType.tranPair(R.string.tran_room_type)
                    ),
                    null,
                    onNotNormal = {
                        items[adapterPosition] = it
                        notifyItemChanged(adapterPosition)
                    })

        }

    }

    /*******************************************************************************************
     * mycontents, manage
     *******************************************************************************************/
    inner class RoomMyConViewHolder(val b: HolderRoomMyConBinding) : RecyclerView.ViewHolder(b.root) {

        private lateinit var room: Room

        fun bind(room: Room) {
            this.room = room
            b.room = room
            b.inter = interHolderBasic(
                    onClickCard = { onClickCard(it) },
                    onClickGo = {
//                        MyAlert.showRequestGrant(context, room, onSuccess = {}, onFail = {}) // 승인 요청.
                        OnGrant.request(context, room, onSuccess = {b.btnGrant.gone()})         // 승인 요청.

//                        OnGrant.request(context, room, onSuccess = { b.btnGrant.goneFadeOut(false) })
                    },   // 승인 요청.
                    onClickUpdate = null,
                    onClickDel = null, onDeepDel = {},
                    onClickStatus = { if (isAdmin()) room.showStatusDialog(context,null) else onClickCard(it) })

            b.line.setOnLongClickListener {
                room.showStatusDialog(context, onSuccess = { status, _ ->
                    status?.let {
                        room.statusNo = it.no
                        notifyItemChanged(adapterPosition)
                    }
                })
                return@setOnLongClickListener true
            }

            b.executePendingBindings()
            b.btnGrant.gone()   // 이걸 안 해주면 notify했을 때 이상하게 나와.
        }


        fun onClickCard(v: View) {
            OnMyClick.setDisableAWhileBTN(v)

            OnGrant.checkStatus(context, room, b.btnGrant,
                    onNormal = { room.justGo(context, true, arrayOf(b.tvTitle.tranPair(R.string.tran_linear))) },
//                    onAvailRequest = { if (it) b.btnGrant.visi() else b.btnGrant.gone() },
                    onDeleted = {
                        /*** Status.Deleted 완료시 rv에서 제거. */
                        items.removeAt(adapterPosition)
                        notifyItemRemoved(adapterPosition)
                    })

        }

    }

    /*******************************************************************************************
     *
     *******************************************************************************************/
    inner class RoomHoriViewHolder(val b: HolderRoomHoriBinding) : RecyclerView.ViewHolder(b.root) {

        private lateinit var room: Room

        fun bind(room: Room) {
            this.room = room
            b.room = room
            b.inter = interHolderBasic(
                    onClickCard = { onClickCard(it) },
                    onClickGo = null,
                    onClickUpdate = { },
                    onClickDel = { }, onDeepDel = {},
                    onClickStatus = { })
            b.executePendingBindings()
        }

        fun onClickCard(v: View) {
            OnMyClick.setDisableAWhileBTN(v)

            onItemClick?.let {
                it(room)
                return
            }

        }

    }


}