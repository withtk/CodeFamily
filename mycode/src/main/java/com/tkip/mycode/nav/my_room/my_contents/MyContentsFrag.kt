package com.tkip.mycode.nav.my_room.my_contents

import android.os.Bundle
import com.tkip.mycode.R
import com.tkip.mycode.databinding.ViewFrameBinding
import com.tkip.mycode.funs.search.model.CustomModelList
import com.tkip.mycode.init.PUT_DATA_TYPE_NO
import com.tkip.mycode.init.PUT_RV_TYPE
import com.tkip.mycode.init.PUT_UID
import com.tkip.mycode.init.RV_MY_CONTENTS
import com.tkip.mycode.init.my_bind.BindingFragment
import com.tkip.mycode.model.my_enum.DataType
import com.tkip.mycode.model.my_enum.dataType


class MyContentsFrag : BindingFragment<ViewFrameBinding>() {
    override fun getLayoutResId(): Int = R.layout.view_frame
    //    private val manage by lazy { arguments?.getBoolean(PUT_MANAGE, false) ?: false }
    private val uid by lazy { arguments?.getString(PUT_UID) }
    private val dataType by lazy {
        arguments?.getInt(PUT_DATA_TYPE_NO)?.dataType() ?: DataType.POST
    }
    private val rvType by lazy { arguments?.getInt(PUT_RV_TYPE) ?: RV_MY_CONTENTS }

    private var onItemClick: ((Any) -> Unit)? = null

    private var customModelList: CustomModelList? = null

    companion object {
        @JvmStatic
        fun newInstance(uid: String?, dataTypeNo: Int, rvType: Int, onItemClick: ((Any) -> Unit)?) = MyContentsFrag().apply {
            arguments = Bundle().apply {
                //                putBoolean(PUT_MANAGE, manage)
                putString(PUT_UID, uid)
                putInt(PUT_DATA_TYPE_NO, dataTypeNo)
                putInt(PUT_RV_TYPE, rvType)
            }
            this.onItemClick = onItemClick
        }
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        customModelList = CustomModelList(mContext, uid, dataType,null, rvType, onItemClick)
        b.frame.addView(customModelList)


    }

    /**
     * 초기 검색 결과 가져오기 : 비어있을때만
     */
    fun initSearch() {
        customModelList?.let {
            if (it.esLastIndex == 0) {
                it.getEsDataList(0)
            }
        }
    }

}

