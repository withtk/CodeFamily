package com.tkip.mycode.nav

import android.Manifest.permission.READ_EXTERNAL_STORAGE
import android.app.Activity
import android.content.Intent
import android.content.pm.PackageManager
import android.os.Bundle
import android.text.TextUtils
import android.util.Log
import android.view.View
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import com.cunoraz.tagview.Tag
import com.google.gson.JsonObject
import com.tkip.mycode.R
import com.tkip.mycode.ad.bid.AddBidActivity
import com.tkip.mycode.databinding.FragmentTttestBinding
import com.tkip.mycode.dialog.toast.TToast
import com.tkip.mycode.funs.common.OnBoolean
import com.tkip.mycode.funs.common.OnPermission
import com.tkip.mycode.funs.common.lllogI
import com.tkip.mycode.funs.common.textListener
import com.tkip.mycode.init.REQ_ALBUM
import com.tkip.mycode.init.REQ_PICK
import com.tkip.mycode.init.REQ_READ_EXSTORAGE
import com.tkip.mycode.init.base.Cons
import com.tkip.mycode.init.my_bind.BindingFragment
import com.tkip.mycode.model.mytag.MyTag
import com.tkip.mycode.model.store.Store
import com.tkip.mycode.my_test.TTTestActivity
import com.tkip.mycode.util.lib.web.MyWebViewActivity
import com.tkip.mycode.util.lib.map.MyMapActivity
import com.tkip.mycode.util.lib.photo.MyPhoto
import com.tkip.mycode.util.lib.photo.album.AlbumActivity
import com.tkip.mycode.util.lib.photo.util.PhotoUtil
import com.tkip.mycode.util.tools.etc.OnMyClick


class TTTestFrag : BindingFragment<FragmentTttestBinding>() {
    override fun getLayoutResId(): Int = R.layout.fragment_tttest

    private val mybool = OnBoolean(false,
            changed = {
                TToast.showNormal("changed : " + it.toString())
            }
    )
    private var items = arrayListOf<MyTag>()

    companion object {
        @JvmStatic
        fun newInstance() = TTTestFrag()
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        b.fragment = this


        //set click listener
        b.tagView.setOnTagClickListener { _, p -> b.tagView.remove(p) }
        b.tagView.setOnTagDeleteListener { _, _, _ -> }
        b.tagView.setOnTagLongClickListener { _, p -> b.tagView.remove(p) }

//        OnTag.setDataTagView(b.tagView2)






        b.etSearch.textListener(after = {}, before = { _, _, _, _ -> }, on = { s, _, _, c ->

        })


        /* b.tagsEditText.addTextChangedListener(object : TextWatcher {
             override fun afterTextChanged(s: Editable?) {
                 val str = s.toString().trim()
                 if (TextUtils.isEmpty(str)) return
                 val items = mutableListOf<String>()

                 for ((k, v) in Firee.tagMap) {
                     if (k.contains(str)) {
                         items.add(k)
                     }
                 }
                 b.tagView.addTags(items.toTypedArray())
             }

             override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
             }

             override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
             }
         })
 */

        setUpAutoComplete()


/*

        mybool.setOnVariableChanged(object : OnBoolean.MyBoolChanger{
            override fun onVariableChanged(myBool: Boolean) {

                TToast.showNormal("changed : " + mybool.toString())

            }
        })
*/


    }


    private fun setUpAutoComplete() {
    }


    fun onClickBtn1() {
        if (OnMyClick.isDoubleClick()) return

        val intent = Intent(activity, MyMapActivity::class.java)
        startActivity(intent)
    }

    fun onClickBtn2() {
        if (OnMyClick.isDoubleClick()) return

        val address = b.et1.text.toString().trim()
        val intent = Intent(activity, MyWebViewActivity::class.java)
        intent.putExtra(Cons.PUT_STRING, address)
        startActivity(intent)

    }

    fun onClickBtn3() {
    }

    fun onClickBtn4() {
        if (OnMyClick.isDoubleClick()) return

        val intent = Intent(activity,  TTTestActivity::class.java)
        startActivity(intent)
    }

    fun onClickBtn5() {

        val builder = AlertDialog.Builder(mContext, R.style.MyAlertStyleSmall)
        builder.setTitle("확인")
        builder.setMessage("XXX해도 괜찮습니까?")
        builder.setPositiveButton("예", null)
        builder.setNegativeButton("아니오", null)
        builder.setNeutralButton("취소", null)

        val dialog = builder.show()


//        if (OnMyClick.isDoubleClick()) return
/*
        val strList = arrayListOf<String>()
        for (i in 1..1000) {
            strList.add("time $i")
        }
        val askStore = AskSearch(OnPeer.myUid(), strList)

        Firee.addAskSearch(askStore,{},{})*/

        /* val post = Post(PostType.FREE, "post title", "post description")
         val updateAndCounter = BookmarkBefore(post)
         updateAndCounter.key ="key1111"
         Firee.addBookmark(updateAndCounter, {
             Log.d(Cons.TAG, "updateAndCounter  success")
         }, {
             Log.d(Cons.TAG, "updateAndCounter  fail")
         })*/

    }

    fun onClickBtn6() {

//        Firee.getBookmark("key1111", { Log.d(Cons.TAG, "getBookmark   : $it") }, {})
/*
        FirebaseFirestore.getInstance().collection(Firee.CH_SEARCH_ASK).document(OnPeer.myUid())
                .addSnapshotListener { snapshot, e ->
                    if (e != null) {
                        OnException.dbSyncFailed("onClickBtn6", e)
                        return@addSnapshotListener
                    }
                    Log.d(Cons.TAG, "onClickBtn6 addSnapshotListener"  )

                    snapshot?.let {
                        if (it.exists()) {

                            it.toObject(AskSearch::class.java)?.let { askStore ->
                                Log.d(Cons.TAG, "" + askStore)
                            }
                            Log.d(Cons.TAG, "onClickBtn6" + snapshot.data!!)
                        } else {
                            Log.d(Cons.TAG, "onClickBtn6 null")
                        }
                    }

                }*/


    }

    fun onClickBtn7(v: View) {

//        Firee.deleteBookmark("key1111",{Log.d(Cons.TAG, "deleteBookmark2   :  ")},{})


/*
        val set = HashSet<String>()

        val start = System.currentTimeMillis()
        println("tagMap start:$start")
        for (j in 1..1000) {
            set.add("$j te2xcvsddfgdfg34st1")
        }
        println("tagMap 1000:" + (System.currentTimeMillis() - start))

        for (i in 1..10000) {
            tagMap["$i te2xcvsddfgdfg34st1"] = set
        }
        println("tagMap 10000:" + (System.currentTimeMillis() - start))
        println("tagMap size:" + Firee.tagMap.size)*/


//        PlainDialog.show(activity as AppCompatActivity, false)

/*
        NoticeDialog.newInstance(null, "TTTT test", isCancel = false, isCancelTouch = false,
                ok = {
                }, deny = {})
                .show((activity as AppCompatActivity).supportFragmentManager, "dialog")*/

//        val intent = Intent(activity, AddStoreActivity::class.java)
//        startActivity(intent)

//        val intent = Intent(activity, StoreActivity::class.java)
//        startActivity(intent)

    }

    private fun newTag(str: String): Tag? {
//        val str = b.et2.text.toString().trim().replace("\\s".toRegex(), "") // 공백 제거.
        if (TextUtils.isEmpty(str)) return null

        val tag = Tag(str)
        tag.tagTextColor = ContextCompat.getColor(activity as AppCompatActivity, com.tkip.mycode.R.color.text_light)
        tag.layoutColor = ContextCompat.getColor(activity as AppCompatActivity, com.tkip.mycode.R.color.skyblue_800_alpha)
//        tag.layoutBorderColor = ContextCompat.getColor(activity as AppCompatActivity,  R.color.md_pink_500)
//        tag.layoutColorPress = ContextCompat.getColor(activity as AppCompatActivity,  R.color.blue_900)
//        tag.deleteIndicatorColor = ContextCompat.getColor(activity as AppCompatActivity,  R.color.md_cyan_300)
        return tag

    }

    fun onClickMakeTag() {

        val str = b.et2.text.toString().trim().replace("\\s".toRegex(), "") // 공백 제거.
        if (TextUtils.isEmpty(str)) return

        val tag = Tag(str)
        tag.tagTextColor = ContextCompat.getColor(activity as AppCompatActivity, com.tkip.mycode.R.color.text_light)
        tag.layoutBorderColor = ContextCompat.getColor(activity as AppCompatActivity, com.tkip.mycode.R.color.md_pink_500)
        tag.layoutColor = ContextCompat.getColor(activity as AppCompatActivity, com.tkip.mycode.R.color.skyblue_800_alpha)
        tag.layoutColorPress = ContextCompat.getColor(activity as AppCompatActivity, com.tkip.mycode.R.color.blue_900)
        tag.deleteIndicatorColor = ContextCompat.getColor(activity as AppCompatActivity, com.tkip.mycode.R.color.md_cyan_300)
        b.tagView.addTag(tag)


        b.et2.setText("")

//        b.tagView.addTags(ArrayList<TTag> tags)

    }

    fun onClickBtn8() {

        val store = Store()
        store.key = b.et1.text.toString().trim()
        /* Firee.addTagInTagView(b.tagView.tags, store,
                 success = {
                     TToast.showNormal("성공~!  tag add")
                     b.tagView.removeAll()
                     b.et1.setText("")
                 }, fail = {})*/

    }


    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (resultCode == Activity.RESULT_OK) {

            when (requestCode) {
                REQ_PICK -> {
                    data?.data?.let { uri ->
                        b.iv1.setImageURI(uri)

                        PhotoUtil.resetAllUriList()
                        PhotoUtil.photoUris.add(uri)
                        Log.d(Cons.TAG, "REQ_PICK uri : " + uri)
                    }

                }

                REQ_ALBUM -> {
                    Log.d(Cons.TAG, "REQ_ALBUM")
                    /*val bundle = data?.extras
                    bundle?.let {
                        val listSelected = it.getParcelableArrayList<MyPhoto>(Cons.PUT_ARRAY_STRING) as ArrayList<MyPhoto>

                        for (myPhoto in listSelected) {

                            // ID로 Uri받아오기.
                            val resourceUri = ContentUris.withAppendedId(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, java.lang.Long.parseLong(myPhoto.photoId!!))

                            PhotoUtil.photoUris.add(resourceUri)
                        }

//                        b.iv1.setImageURI(listSelected[0].thumbUri)
//                        b.iv2.setImageURI(listSelected[1].thumbUri)
                    }*/


                    data?.getParcelableArrayListExtra<MyPhoto>(Cons.PUT_ARRAY_MYPHOTO)?.let { selectedList ->


                        for (myPhoto in selectedList) {
                            /**
                             * ID로 Uri받아오기 : uri를 content로 변경하는 작업
                             * 이 작업을 하지 않으면 async에서 uri를 쓸 수가 없다.
                             */
//                            val resourceUri = ContentUris.withAppendedId(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, myPhoto.photoId!!.toLong())
//                            PhotoUtil.photoUris.add(PhotoUtil.getResourceUri(myPhoto.photoId!!))
                        }


//                        b.iv1.setImageURI(selectedList[0].uri)
//                        val myuri =selectedList[0].uri
//                        b.foto = Foto("https://www.bloter.net/wp-content/uploads/2016/08/13239928_1604199256575494_4289308691415234194_n-765x510.jpg", "https://www.bloter.net/wp-content/uploads/2016/08/13239928_1604199256575494_4289308691415234194_n-765x510.jpg")

//                        b.foto = Foto(selectedList[0].thumbUri,selectedList[0].uri)
//                        b.iv2.setImageURI(myuri)
//                        b.iv3.setImageURI(selectedList[0].thumbUri)
                    }
                }
            }

        }
    }


    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        when (requestCode) {
            REQ_READ_EXSTORAGE -> {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    onClickBtn9()
                } else {
                    OnPermission.informAbout_READ_PERMISSION(activity as AppCompatActivity)
                }
            }

        }
    }

    fun onClickBtn9() {

        val readPermission = ContextCompat.checkSelfPermission(activity as AppCompatActivity, READ_EXTERNAL_STORAGE)

        if (readPermission == PackageManager.PERMISSION_GRANTED) {
//            startActivityForResult(PhotoUtil.intentPick(), OnPermission.REQ_PICK)
        } else {
            ActivityCompat.requestPermissions(activity as AppCompatActivity, arrayOf(READ_EXTERNAL_STORAGE), REQ_READ_EXSTORAGE)
        }


    }

    fun onClickUploadImages() {
    }

    fun onClickAlbum() {
        val readPermission = ContextCompat.checkSelfPermission(activity as AppCompatActivity, READ_EXTERNAL_STORAGE)
        if (readPermission == PackageManager.PERMISSION_GRANTED) {
            val intent = Intent(activity, AlbumActivity::class.java)
            startActivityForResult(intent, REQ_ALBUM)
        } else {
            ActivityCompat.requestPermissions(activity as AppCompatActivity, arrayOf(READ_EXTERNAL_STORAGE), REQ_READ_EXSTORAGE)
        }
    }


    fun onClickPhotoView() {

//        mybool.log(true)

        val intent = Intent(activity, MyMapActivity::class.java)
        startActivity(intent)

    }

    fun onLongClickLog(): Boolean {
        Log.d(Cons.TAG, "onLongClickLog :  ")
        return true
    }

    fun onClickLog() {
        lllogI("onClickLog   ")


//        batch().myCommit("sdfsdf",
//                success = {
//                    lllog("onClickLog  myCommit ")
//                }, fail = {})

//        val mytag = MyTag("우리우리우리집",TagTYPE.NORMAL)
//
//        batch().apply {
//            mytag.addToBatch(this)
//        }.myCommit("testtest",
//                success = {
//                    lllog("onClickLog success ")
//                }, fail = {})


//        WAIT_TYPE.BASIC_1.show(mContext)
//        Handler().postDelayed({
//            MyCircle.cancel()
//        }, 2000)
    }

    fun onClickTest() {

        val intent = Intent(mContext, AddBidActivity::class.java)
        mContext. startActivity(intent)


//        val intent = Intent()
//        mContext.startActivity(Intent(mContext,AddAdActivity.java::class))

//        MyProgress.show(mContext,null)

//        OnTestDB.FireeUploadRoomTags()
//        OnTestDB.FireeUploadBuiltTags()
    }


    fun onClickSearch() {
        Log.d(Cons.TAG, "=======전체 update 시 활용.==============")


//        val query = " \n" +
//                "{\n" +
//                "  \"query\": {\n" +
//                "    \"bool\": {\n" +
//                "      \"must\": {\n" +
//                "        \"match\": {\n" +
//                "          \"authorUID\": \"EehLkjiCwnQiRpA8hH8vzwzpcvj2\"\n" +
//                "        }\n" +
//                "      } \n" +
//                "    }\n" +
//                "  }\n" +
//                "}\n"

//        val boardCall = Retro.esApi.getHighBoard(HEADER_MAP,query!!.requestBody())
//        boardCall.enqueue(object : Callback<BoardHitsObject> {
//            override fun onFailure(call: Call<BoardHitsObject>, t: Throwable) {
//                Log.d(Cons.TAG, "getHighBoard onFailure :: ")
//            }
//
//            override fun onResponse(call: Call<BoardHitsObject>, response: Response<BoardHitsObject>) {
////                    Log.d(Cons.TAG, "getBoardList onResponse :: " + response.body().toString())
//                if (response.isSuccessful) {
//                    val resultList = ArrayList<Board>()
//                    response.body()?.hitsList?.sourceList?.let {
//                        for (source in it) {
//                            source._source?.let { board ->
//                                resultList.add(board)
//                                Log.d(Cons.TAG, "getHighBoard Response post:: $board")
//                            }
//                        }
//                    for (board in resultList) {
//                        buffer.append(board.name)
//                    }
//                    b.tvResult.text = buffer.toString()
//                    }
//
//                } else {
//                    Log.d(Cons.TAG, "getHighBoard Response is unsuccessful:: " + response.errorBody()?.string())
//                }
//            }
//        })


    }

    private fun setupRetrofit2_all() {


    }


    private fun setupRetrofit2() {
//        val retrofit = Retrofit.Builder()
//                .baseUrl("http://localhost:3000")
//                .addConverterFactory(GsonConverterFactory.create())
//                .build()

//        val service = EsApiService.create()
//
//
//        val queryjson = "{\"query\": {\"match_all\": {}}}"
//
//
//        Log.d(Cons.TAG, "=====================")
//        Log.d(Cons.TAG, "queryjson queryjson :: $queryjson")
//        Log.d(Cons.TAG, "=====================")
//
//
//
//        service.getFirst(HEADER_MAP, queryjson.requestBody()).enqueue(object : Callback<JsonObject> {
//            override fun onFailure(call: Call<JsonObject>, t: Throwable) {
//                Log.d(Cons.TAG, "getLivePostList onFailure :: ")
//            }
//
//            override fun onResponse(call: Call<JsonObject>, response: Response<JsonObject>) {
//                Log.d(Cons.TAG, "getLivePostList Response :: " + response.body().toString())
////                val data = response.body()
//            }
//
//        })


    }

    private fun makeJsonObject() {

        val jsonObject = JsonObject()
        jsonObject.addProperty("title", "Hello")
        jsonObject.addProperty("body", "Retrofit")
        jsonObject.addProperty("userId", "2")

        Log.d(Cons.TAG, "jsonObject :: $jsonObject")


    }

}