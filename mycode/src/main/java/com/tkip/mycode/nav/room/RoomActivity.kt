package com.tkip.mycode.nav.room

import android.os.Bundle
import android.os.Handler
import android.view.View
import androidx.annotation.LayoutRes
import com.tkip.mycode.R
import com.tkip.mycode.admin.menu.AMenu
import com.tkip.mycode.admin.menu.OnMenu
import com.tkip.mycode.databinding.ActivityRoomBinding
import com.tkip.mycode.dialog.OnDDD
import com.tkip.mycode.dialog.alert.MyAlert
import com.tkip.mycode.dialog.toast.toastNormal
import com.tkip.mycode.funs.common.OnFunction
import com.tkip.mycode.funs.common.OnPermission
import com.tkip.mycode.funs.common.lllogI
import com.tkip.mycode.funs.custom.ViewAuthorTitle
import com.tkip.mycode.funs.custom.toolbar.ViewToolbar
import com.tkip.mycode.init.*
import com.tkip.mycode.init.base.getStr
import com.tkip.mycode.init.base.getViewModel
import com.tkip.mycode.init.inter.interActivityResult
import com.tkip.mycode.init.inter.interMenuBox
import com.tkip.mycode.init.inter.interRequestPermissionsResult
import com.tkip.mycode.init.my_bind.BindingActivityPhoto
import com.tkip.mycode.init.part.CodeApp
import com.tkip.mycode.model.count.CountType
import com.tkip.mycode.model.count.CountViewModel
import com.tkip.mycode.model.media.setupMediaButton
import com.tkip.mycode.model.my_enum.Status
import com.tkip.mycode.model.my_enum.deepDel
import com.tkip.mycode.model.my_enum.isValidStatus
import com.tkip.mycode.model.my_enum.showStatusDialog
import com.tkip.mycode.model.mytag.CustomTagList
import com.tkip.mycode.model.reply.LinearReplyAddBox
import com.tkip.mycode.model.reply.OnReply
import com.tkip.mycode.model.room.OnRoom
import com.tkip.mycode.model.room.Room
import com.tkip.mycode.model.room.showAmountInRoomActi
import com.tkip.mycode.model.toss.OnToss
import com.tkip.mycode.model.warn.OnWarn
import com.tkip.mycode.util.lib.map.OnMap
import com.tkip.mycode.util.lib.retrofit2.ESget
import com.tkip.mycode.util.my_view.ViewUtil
import com.tkip.mycode.util.tools.fire.commonStatusUpdate
import com.tkip.mycode.util.tools.recycler.OnRv


class RoomActivity : BindingActivityPhoto<ActivityRoomBinding>() {
    @LayoutRes
    override fun getLayoutResId() = R.layout.activity_room

    private val addType by lazy { intent.getIntExtra(PUT_ACTION, ACTION_NORMAL) }
    //    private val vmRoom by lazy { getViewModel<RoomViewModel>() }
    private val vmCount by lazy { getViewModel<CountViewModel>() }

    private val putRoom by lazy { intent.getParcelableExtra<Room>(PUT_ROOM) }
    private val roomKey by lazy { intent.getStringExtra(PUT_KEY) }

    private lateinit var room: Room
    private lateinit var viewToolbar: ViewToolbar

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        if (roomKey == null) {
            room = putRoom
            initView()
            initData()
        } else {
            ESget.getRoom(roomKey,
                    success = {
                        it?.let { room1 ->
                            if (room1.isValidStatus(Status.NORMAL)) {
                                room = room1
                                initView()
                                initData()
                                CodeApp.curModelKey =  room.key

                            }
                        }
                    }, fail = {})
        }

    }

    override fun onResume() {
        super.onResume()
        CodeApp.curModelKey = if (::room.isInitialized)  room.key else null
    }
    override fun onStop() {
        super.onStop()
        CodeApp.curModelKey = null
    }


    private fun initView() {
        b.activity = this
        b.addType = addType

        /** Toolbar */
        viewToolbar = ViewToolbar(this@RoomActivity,
                R.string.title_room.getStr(), room.title, room.addDate,
                onBack = { onBackPressed() },
                onBoardTitle = { },
                onMore = { onClickMore() }
        )
        b.frameToolbar.addView(viewToolbar)

        /**  글쓴이  */
        b.lineWhite.addView(ViewAuthorTitle(this@RoomActivity, false, room.authorUID, room.addDate, onPeer = {}), 0)

        /**  top 사진 rv  */
        OnRv.setTopFotoAdapter(room.fotoList, b.rvFoto)

        if (addType != ACTION_PREVIEW) initInterface()   // Action_PREVIEW는 하면 안 됨.

    }

    private fun initData() {
        lllogI("RoomActivity initData initData")
        b.room = room

        /*** 방 수  기입 */
        showAmountInRoomActi(room,b.tagViewAmount )

        /*** 태그 : binding에서 기입 */

        /*** 미디어 기입 */
        room.mediaMap.setupMediaButton(this@RoomActivity, b.lineMedia)

        /*** 지도 add & set */
        if (room.address != null || room.lat != null) {
            b.lineMap.removeAllViews()
            linearMapShow = ViewUtil.addLinearMapShow(this@RoomActivity, b.lineMap, room.address, OnMap.createLatlng(room.lat, room.lng))
        }

    }

    private fun initInterface() {

//        /**   * CH_VIEW_UID_COUNT 추가  */
//        vmRoom.addViewUID(room, Cntch.VIEW_UID)

        /** 검색 태그리스트   */
        if (!room.tagList.isNullOrEmpty()) b.frameTagList.addView(CustomTagList(this@RoomActivity, room.tagList))

        /**  업데이트 date & 뷰 카운트   */
        b.inViewCount.updateDate = room.updateDate
        b.inViewCount.viewCount = room.viewCount
//        b.inViewCount.bookCount = room.bookmarkCount
        b.inViewCount.replyCount = room.replyCount
        //        b.inViewCount.pushCount = room.pushCount


        /********************************** inInterface ***********************************/
        /**   CheckBox 세팅    */
        vmCount.initCount(this@RoomActivity, room, arrayOf(CountType.VIEW_UID, CountType.BOOK, CountType.PUSH), null, null, b.inMenuBox.cbBookmark, b.inMenuBox.cbPush)
//        vmRoom.getAllEsCount(room)
//        vmRoom.liveBookmark.observe(this, Observer {
//            b.inMenuBox.cbBookmark.isChecked = it != null
//            b.inMenuBox.cbBookmark.isEnabled = true
//        })
//        vmRoom.livePush.observe(this, Observer {
//            b.inMenuBox.cbPush.isChecked = it != null
//            b.inMenuBox.cbPush.isEnabled = true
//        })

        /**  메뉴박스   */
        b.inMenuBox.bookmark = room.bookmarkCount
        b.inMenuBox.push = room.pushCount
        b.inMenuBox.inter = interMenuBox(
                onClickThumbUp = { },
                onClickThumbDown = { },
                onClickBookmark = { vmCount.updateCount(room, CountType.BOOK, it) },
                onClickPush = { vmCount.updateCount(room, CountType.PUSH, it) },
                onClickToss = { OnToss.checkAdd(this@RoomActivity, room) },
                onClickMore = { onClickMore() }
        )
        /**   * 글 작성 직후일때 : 1초 후 자동으로 PUSH 추가.  */
        if (addType == ACTION_NEW) Handler().postDelayed({ toastNormal(R.string.info_set_push) }, 1000)


        /**  댓글 첨부박스   */
        linearReplyAddBox = LinearReplyAddBox(this@RoomActivity, room.allowReply, room.key,
                onClickAddReply = {
                    linearReplyAddBox.checkAddReply(this@RoomActivity, room, replyAdapter,
                            success = { b.nestedScrollView.fullScroll(View.FOCUS_DOWN) }, fail = {})
                }).apply { this@RoomActivity.b.frameReplyBox.addView(this) }

//        linearReplyAddBox = ViewUtil.addLinearReplyAddBox(this@RoomActivity, b.frameReplyBox, room.key,
//                onClickAddReply = {
//                    linearReplyAddBox.b.checkAddReply(this@RoomActivity, room, replyAdapter,
//                            success = { b.nestedScrollView.fullScroll(View.FOCUS_DOWN) }, fail = {})
//                })
        /** reply  */
        replyAdapter = OnReply.setAdapter(this@RoomActivity, EES_ROOM, room.key, EES_REPLY, room, b.inRvReply, linearReplyAddBox, onCompleteAdoption = { })


        /********************* superListener *****************************/
        listenerActivityResult = interActivityResult(
                onPick = {
                    linearReplyAddBox.addPhotoUri(it)
                }, onAlbum = {}, onMap = { _, _ -> }, onBanner = null, onData = null)

        listenerPermissionsResult = interRequestPermissionsResult(onReadExternalStorage = { OnPermission.onOpenGallery(this@RoomActivity, it, false) })

    }


    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        /**  * 재생성시 그냥 초기화      */
        OnReply.resetAttachReply(linearReplyAddBox.b.lineAttachBox)
    }

    fun onClickContact(v:View) {
        OnFunction.setClipBoardLink(b.tvContact.text.toString())
    }
    fun onClickMore() {
        if (addType == ACTION_PREVIEW) return
        MyAlert.showMenuList(this@RoomActivity, OnMenu.getMENULIST(room),
                onSelect = { amenu ->
                    when (amenu) {
                        AMenu.UPDATE -> OnRoom.gotoAddRoomActivity(this@RoomActivity, ACTION_UPDATE, room, null)
                        AMenu.DELETION -> OnDDD.delete(this@RoomActivity, ok = { commonStatusUpdate(EES_ROOM, room.key, Status.DELETED_USER, success = { finish() }, fail = {}) }, cancel = {})
                        AMenu.TRANSACTION_COMPLETED -> OnDDD.confirm(this@RoomActivity, R.string.status_dialog_transaction_completed, ok = {commonStatusUpdate(EES_ROOM, room.key, Status.TRANSACTION_COMPLETED, success = { finish() }, fail = {})}, cancel = {})
                        AMenu.WARN -> OnWarn.checkAndAdd(this@RoomActivity, room, success = {}, fail = {})
                        AMenu.DEEP_DEL -> room.deepDel(this@RoomActivity, onSuccess = {})
                        AMenu.STATUS -> room.showStatusDialog(this@RoomActivity,null)
                        else -> {
                        }
                    }
                })
    }


}