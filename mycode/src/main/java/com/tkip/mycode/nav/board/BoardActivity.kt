package com.tkip.mycode.nav.board

import android.os.Bundle
import android.os.Handler
import android.os.PersistableBundle
import android.view.View
import android.widget.CheckBox
import androidx.annotation.LayoutRes
import androidx.fragment.app.Fragment
import com.tkip.mycode.R
import com.tkip.mycode.admin.menu.AMenu
import com.tkip.mycode.databinding.ActivityBoardBinding
import com.tkip.mycode.dialog.OnDDD
import com.tkip.mycode.dialog.alert.MyAlert
import com.tkip.mycode.dialog.progress.MyCircle
import com.tkip.mycode.dialog.progress.WAIT_TYPE
import com.tkip.mycode.funs.common.lllogI
import com.tkip.mycode.funs.common.tranPair
import com.tkip.mycode.funs.custom.toolbar.ViewToolbar4Board
import com.tkip.mycode.init.*
import com.tkip.mycode.init.base.getStr
import com.tkip.mycode.init.base.getViewModel
import com.tkip.mycode.init.my_bind.BindingActivity
import com.tkip.mycode.model.board.Board
import com.tkip.mycode.model.board.OnBoard
import com.tkip.mycode.model.count.CountType
import com.tkip.mycode.model.count.CountViewModel
import com.tkip.mycode.model.flea.OnFlea
import com.tkip.mycode.model.my_enum.*
import com.tkip.mycode.model.peer.isAdmin
import com.tkip.mycode.model.peer.isCsBoard
import com.tkip.mycode.model.peer.isMaster
import com.tkip.mycode.model.post.OnPost
import com.tkip.mycode.model.post.Post
import com.tkip.mycode.model.room.OnRoom
import com.tkip.mycode.model.toss.OnToss
import com.tkip.mycode.model.warn.OnWarn
import com.tkip.mycode.nav.flea.tab_flea_market.FleaFrag
import com.tkip.mycode.nav.post.PostFrag
import com.tkip.mycode.nav.room.RoomFrag
import com.tkip.mycode.util.lib.retrofit2.ESget
import com.tkip.mycode.util.tools.etc.OnMyClick
import com.tkip.mycode.util.tools.fire.real


/**
 */
class BoardActivity : BindingActivity<ActivityBoardBinding>() {
    @LayoutRes
    override fun getLayoutResId() = R.layout.activity_board

    //    private val vmBoard by lazy { getViewModel<BoardViewModel>() }
    private val addType by lazy { intent.getIntExtra(PUT_ACTION, ACTION_NOTHING) }   // 생성시에 push를 바로 등록하기 위해.
//    private val board by lazy { intent.getParcelableExtra<Board>(PUT_BOARD ) }

    private val vmCount by lazy { getViewModel<CountViewModel>() }
    private val putBoard by lazy { intent.getParcelableExtra<Board>(PUT_BOARD) }
    private val boardKey by lazy { intent.getStringExtra(PUT_KEY) }
    private lateinit var board: Board
    private lateinit var viewToolbar: ViewToolbar4Board


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

//        board = intent.getParcelableExtra<Board>(PUT_BOARD )
        if (boardKey == null) {
            board = putBoard
            initView()
//            MyCircle.cancel()
        } else {
            WAIT_TYPE.FULL_CIRCLE.show(this@BoardActivity)
            ESget.getBoard(boardKey,
                    success = {
                        it?.let { board1 ->
                            if (board1.isValidStatus(Status.NORMAL)) {
                                board = board1
                                initView()
                            }
                        }
                        MyCircle.cancel()
                    }, fail = {})
        }

    }

    override fun onSaveInstanceState(outState: Bundle, outPersistentState: PersistableBundle) {
        super.onSaveInstanceState(outState, outPersistentState)
    }

    override fun onRestoreInstanceState(savedInstanceState: Bundle) {
        super.onRestoreInstanceState(savedInstanceState)
    }


    private fun initView() {
//        Cost.new(null, ActType.OPEN_BOARD, board).real()
        b.activity = this
        b.board = board

        /** Toolbar */
        when (board.key) {

            FLEA_BOARD_KEY -> {
                viewToolbar = ViewToolbar4Board(this@BoardActivity, board, board.title, R.string.title_add_flea.getStr(),
                        board.bookmarkCount, board.pushCount,
                        onBack = { onBackPressed() },
                        onBookmark = { vmCount.updateCount(board, CountType.BOOK, it) },
                        onPush = null,
                        onAdd = { onClickAdd(it) },
                        onMore =  { onClickMore(it) }
                ).apply { this@BoardActivity.b.frameToolbar.addView(this) }

                /**   Count CheckBox 세팅    */
                vmCount.initCount(this@BoardActivity, board, arrayOf(CountType.BOOK), null, null, viewToolbar.b.cbBookmark, null)

                /*** flea list 붙이기  */
                setFrag(FleaFrag.newInstance())

            }

            ROOM_BOARD_KEY -> {
                viewToolbar = ViewToolbar4Board(this@BoardActivity, board, board.title, R.string.title_add_room.getStr(),
                        board.bookmarkCount, board.pushCount,
                        onBack = { onBackPressed() },
                        onBookmark = { vmCount.updateCount(board, CountType.BOOK, it) },
                        onPush = null,
                        onAdd = { onClickAdd(it) },
                        onMore =  { onClickMore(it) }
                ).apply { this@BoardActivity.b.frameToolbar.addView(this) }

                /**   Count CheckBox 세팅    */
                vmCount.initCount(this@BoardActivity, board, arrayOf(CountType.BOOK), null, null, viewToolbar.b.cbBookmark, null)

                /*** room list 붙이기  */
                setFrag(RoomFrag.newInstance())
            }


            /*** Post_board */
            else -> {
                val addBtnName = if (board.allowPost) R.string.btn_add_post.getStr() else R.string.btn_add_post_not_allow.getStr()
                viewToolbar = ViewToolbar4Board(this@BoardActivity, board, board.title, addBtnName,
                        board.bookmarkCount, board.pushCount,
                        onBack = { onBackPressed() },
                        onBookmark = { vmCount.updateCount(board, CountType.BOOK, it) },
                        onPush = { vmCount.updateCount(board, CountType.PUSH, it) },
                        onAdd = { onClickAdd(it) },
                        onMore = { onClickMore(it) }
                ).apply { this@BoardActivity.b.frameToolbar.addView(this) }

                /**   Count CheckBox 세팅    */
                if (board.isCsBoard()) {
                    if (board.authorUID.isMaster() || isAdmin()) vmCount.initCount(this@BoardActivity, board, arrayOf(CountType.BOOK, CountType.PUSH), null, null, viewToolbar.b.cbBookmark, viewToolbar.b.cbPush)
                    else vmCount.initCount(this@BoardActivity, board, arrayOf(CountType.BOOK, CountType.PUSH), null, null, viewToolbar.b.cbBookmark, null)
                } else vmCount.initCount(this@BoardActivity, board, arrayOf(CountType.BOOK, CountType.PUSH), null, null, viewToolbar.b.cbBookmark, viewToolbar.b.cbPush)


                /*** post list 붙이기  */
                setFrag(PostFrag.newInstance(board))

            }
        }
    }

    private fun setFrag(fragment: Fragment) {

        val run = Runnable {
            lllogI("BoardActivity setFrag Runnable")
            //            val fragment =
//                    when (board.key) {
//                        FLEA_BOARD_KEY -> FleaFrag.newInstance()
//                        ROOM_BOARD_KEY -> RoomFrag.newInstance()
//                        else -> PostFrag.newInstance(board.key)
//                    }

            val fragTran = supportFragmentManager.beginTransaction()

            /*** 삭제 all */
//            for (f in supportFragmentManager.fragments) {
//                lllogI("BoardActivity setupFragmentTransaction remove ${f.javaClass.simpleName} ===")
//                fragTran.remove(f)
//            }


            fragTran.replace(R.id.frame, fragment, fragment.javaClass.simpleName)
            fragTran.commitAllowingStateLoss()
        }
        Handler().post(run)
    }

    fun onClickAdd(v: View) {
        OnMyClick.setDisableAWhileBTN(v)
        /*** 포스트 쓰기 허용 여부 체크 */
        if (board.allowPost) gotoAddActi()
        else {
            /*** 업로드 차단중 : 관리자 권한 체크 */
            if (board.authorUID.isMaster() || isAdmin()) MyAlert.showConfirm(this@BoardActivity, R.string.info_add_as_admin.getStr(), ok = { gotoAddActi() }, cancel = {})
            else OnDDD.confirm(this@BoardActivity, R.string.info_not_allow_add_post, {}, null)
        }
    }

    private fun gotoAddActi() {
        when (board.key) {
            FLEA_BOARD_KEY -> OnFlea.gotoAddFleaActivity(this@BoardActivity, ACTION_NEW, null, null)
            ROOM_BOARD_KEY -> OnRoom.gotoAddRoomActivity(this@BoardActivity, ACTION_NEW, null, null)
            else -> OnPost.gotoAddPostActivity(this@BoardActivity, ACTION_NEW, Post(null, board), null)
        }
    }

    fun onClickMore(v: View) {
        OnMyClick.setDisableAWhileBTN(v)

        OnBoard.showMenuDialog(this@BoardActivity, board,
                onSelect = {
                    when (it) {
                        AMenu.PUSH -> vmCount.updateCount(board, CountType.PUSH, CheckBox(this@BoardActivity).apply { isChecked=true })
                        AMenu.INFO -> MyAlert.showBoardInfo(this@BoardActivity, board)
                        AMenu.UPDATE -> OnBoard.gotoAddBoardActivity(this@BoardActivity, ACTION_UPDATE, board, arrayOf(viewToolbar.b.tvBarTitle.tranPair(R.string.tran_title)))
//                AMenu.DELETION -> MyAlert.showConfirm(this@FreeBoardActivity, R.string.info_delete_board.getStr(), ok = { }, cancel = {})
                        AMenu.TOSS -> OnToss.checkAdd(this@BoardActivity, board)
                        AMenu.WARN -> OnWarn.checkAndAdd(this@BoardActivity, board, {}, {})
                        AMenu.DEEP_DEL -> board.deepDel(this@BoardActivity, onSuccess = {})
                        AMenu.STATUS -> board.showStatusDialog(this@BoardActivity, null)
                        else -> {
                        }
                    }
                })


//        when (board.key) {
//            FLEA_BOARD_KEY -> {
//                if(isAdmin())
//                    OnBoard.showMenuDialog(this@BoardActivity, board,
//                            onSelect = {
//                                when (it) {
//                                    AMenu.INFO -> MyAlert.showBoardInfo(this@BoardActivity, board)
//                                    AMenu.UPDATE -> OnBoard.gotoAddBoardActivity(this@BoardActivity, ACTION_UPDATE, board, arrayOf(viewToolbar.b.tvBarTitle.tranPair(R.string.tran_title)))
//                                    AMenu.TOSS -> OnToss.checkAdd(this@BoardActivity, board)
//                                    AMenu.STATUS -> board.showStatusDialog(this@BoardActivity, null)
//                                    else -> {
//                                    }
//                                }
//                            })
//            }
//            ROOM_BOARD_KEY -> {
//                if(isAdmin())
//                OnBoard.showMenuDialog(this@BoardActivity, board,
//                        onSelect = {
//                            when (it) {
//                                AMenu.INFO -> MyAlert.showBoardInfo(this@BoardActivity, board)
//                                AMenu.UPDATE -> OnBoard.gotoAddBoardActivity(this@BoardActivity, ACTION_UPDATE, board, arrayOf(viewToolbar.b.tvBarTitle.tranPair(R.string.tran_title)))
//                                AMenu.TOSS -> OnToss.checkAdd(this@BoardActivity, board)
//                                AMenu.STATUS -> board.showStatusDialog(this@BoardActivity, null)
//                                else -> {
//                                }
//                            }
//                        })
//            }
//            else -> OnBoard.showMenuDialog(this@BoardActivity, board,
//                    onSelect = {
//                        when (it) {
//                            AMenu.INFO -> MyAlert.showBoardInfo(this@BoardActivity, board)
//                            AMenu.UPDATE -> OnBoard.gotoAddBoardActivity(this@BoardActivity, ACTION_UPDATE, board, arrayOf(viewToolbar.b.tvBarTitle.tranPair(R.string.tran_title)))
////                AMenu.DELETION -> MyAlert.showConfirm(this@FreeBoardActivity, R.string.info_delete_board.getStr(), ok = { }, cancel = {})
//                            AMenu.TOSS -> OnToss.checkAdd(this@BoardActivity, board)
//                            AMenu.WARN -> OnWarn.checkAndAdd(this@BoardActivity, board, {}, {})
//                            AMenu.DEEP_DEL -> board.deepDel(this@BoardActivity, onSuccess = {})
//                            AMenu.STATUS -> board.showStatusDialog(this@BoardActivity, null)
//                            else -> {
//                            }
//                        }
//                    })
//        }
    }

}