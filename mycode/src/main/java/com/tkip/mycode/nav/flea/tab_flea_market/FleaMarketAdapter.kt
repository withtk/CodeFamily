package com.tkip.mycode.nav.flea.tab_flea_market

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.RecyclerView
import com.tkip.mycode.databinding.HolderFleaMarketBinding
import com.tkip.mycode.model.flea.Flea


class FleaMarketAdapter (var items: ArrayList<Flea>) : RecyclerView.Adapter<FleaMarketAdapter.FleaMarketViewHolder>() {

    private lateinit var activity: AppCompatActivity

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): FleaMarketAdapter.FleaMarketViewHolder {
        this.activity = parent.context as AppCompatActivity
        val binding = HolderFleaMarketBinding.inflate(LayoutInflater.from(parent.context))
        return FleaMarketViewHolder(binding)
    }

    override fun getItemCount(): Int {
        return items.size
    }

    override fun onBindViewHolder(h: FleaMarketAdapter.FleaMarketViewHolder, p: Int) {
        h.bind(items[p])
    }


    inner class FleaMarketViewHolder(val b: HolderFleaMarketBinding) : RecyclerView.ViewHolder(b.root) {
        private var flea: Flea? = null

        fun bind(flea: Flea) {
            this.flea = flea
            b.holder = this
            b.flea = flea
            b.executePendingBindings()
        }
        fun onClickCard() {
//            val intent = Intent(activity, FreePostActivity::class.java)
//            intent.putExtra(Cons.PUT_OBJECT, post)
//            activity.startActivity(intent)

        }

    }


}
/*

class FleaMarketAdapter(options: FirestoreRecyclerOptions<Flea>) : FirestoreRecyclerAdapter<Flea, FleaMarketAdapter.FleaMarketViewHolder>(options) {

    private lateinit var activity: AppCompatActivity

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): FleaMarketViewHolder {
        this.activity = parent.context as AppCompatActivity
        val binding = HolderFleaMarketBinding.inflate(LayoutInflater.from(parent.context))
        return FleaMarketViewHolder(binding)
    }

    override fun onBindViewHolder(h: FleaMarketViewHolder, p: Int, m: Flea) {
        h.bind(m)
    }

    inner class FleaMarketViewHolder(val b:  HolderFleaMarketBinding) : RecyclerView.ViewHolder(b.root) {

        private var flea: Flea? = null
        fun bind(flea: Flea) {
            this.flea = flea
            b.holder = this
            b.flea = flea
            b.executePendingBindings()
        }

       */
/* fun onClickCard() {
            val intent = Intent(activity, FreePostActivity::class.java)
            intent.putExtra(Cons.PUT_OBJECT, post)
            activity.startActivity(intent)

        }*//*


    }

}
*/
