package com.tkip.mycode.nav.store.tab_stars

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.RecyclerView
import com.firebase.ui.firestore.FirestoreRecyclerAdapter
import com.firebase.ui.firestore.FirestoreRecyclerOptions
import com.tkip.mycode.databinding.HolderStoreRatingBinding
import com.tkip.mycode.funs.common.getNullStr
import com.tkip.mycode.model.peer.OnPeer
import com.tkip.mycode.model.store.Store
import com.tkip.mycode.model.store.StoreRating
import com.tkip.mycode.util.tools.fire.Firee

class StoreRatingAdapterFireS(val store: Store, options: FirestoreRecyclerOptions<StoreRating>) : FirestoreRecyclerAdapter<StoreRating, StoreRatingAdapterFireS.StoreRatingViewHolder>(options) {

    private lateinit var activity: AppCompatActivity

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): StoreRatingViewHolder {
        this.activity = parent.context as AppCompatActivity
        val binding = HolderStoreRatingBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return StoreRatingViewHolder(binding)
    }

    override fun getItemId(position: Int): Long {
        return getItem(position).uid.hashCode().toLong()
    }

    override fun onBindViewHolder(h: StoreRatingViewHolder, p: Int, m: StoreRating) {
        h.bind(m)
    }

    inner class StoreRatingViewHolder(val b: HolderStoreRatingBinding) : RecyclerView.ViewHolder(b.root) {

        private lateinit var storeRating: StoreRating

        fun bind(storeRating: StoreRating) {
            this.storeRating = storeRating
            b.holder = this
            b.storeRating = storeRating
            b.store = store
            /** 다른 유저의 PEER 정보 받아오기 */
            Firee.getPeer(storeRating.uid,{b.peer = it },{})

            b.executePendingBindings()

            b.starRating.rating = storeRating.rating.toFloat()
        }

        fun onClickThumbUp() {
//            executeThumb(storeRating.thumbUpList)
        }

        fun onClickThumbDown() {
//            executeThumb(storeRating.thumbDownList)
        }

        private fun executeThumb(list: ArrayList<String>) {
            /** 이미 썸을 줬는지 체크 */
            if (list.contains(OnPeer.myUid())) list.remove(OnPeer.myUid())     // 제거
            else list.add(OnPeer.myUid())                                      // 삽입

            uploadStoreRating()
        }


        fun onClickSubmitReply() {
            storeRating.hostAddDate = System.currentTimeMillis()
            storeRating.hostComment = b.etMasterComment.getNullStr()

            uploadStoreRating()
        }

        fun hasMyThumb(list: ArrayList<String>) = list.contains(OnPeer.myUid())


        /**
         *  수정사항 업로드
         **/
        private fun uploadStoreRating() {
//            Firee.updateStoreRating(storeRating,
//                    success = {
//                    }, fail = {})
        }
    }

}