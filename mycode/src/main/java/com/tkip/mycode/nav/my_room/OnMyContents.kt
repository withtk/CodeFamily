package com.tkip.mycode.nav.my_room

import android.content.Context
import android.content.Intent
import android.view.View
import com.tkip.mycode.init.PUT_RV_TYPE
import com.tkip.mycode.init.PUT_UID
import com.tkip.mycode.nav.my_room.my_contents.MyContentsActivity
import com.tkip.mycode.util.lib.TransitionHelper

class OnMyContents {
    companion object {

        fun gotoMyContentsActivity(context: Context,   uid:String?,    rvType:Int?,  arrPair: Array<androidx.core.util.Pair<View, String>>?) {
            Intent(context, MyContentsActivity::class.java).apply {
                addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
//                putExtra(PUT_MANAGE,manage)
                putExtra(PUT_UID,uid)
                putExtra(PUT_RV_TYPE,rvType)
                TransitionHelper.startTran(context, this, arrPair)
            }
        }

    }
}