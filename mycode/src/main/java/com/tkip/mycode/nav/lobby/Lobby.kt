package com.tkip.mycode.nav.lobby

import android.os.Parcel
import android.os.Parcelable
import com.tkip.mycode.init.base.createKey
import com.tkip.mycode.model.my_enum.Status
import com.tkip.mycode.model.peer.OnPeer
import com.tkip.mycode.model.peer.Peer
import com.tkip.mycode.model.peer.mynick
import com.tkip.mycode.model.peer.myuid
import com.tkip.mycode.util.tools.date.rightNow

/**
 * 채팅룸.
 */
data class Lobby(

        var key: String = createKey(),
        var authorUID: String = myuid(),
        var authorNick: String = mynick(),
        var authorThumb: String? = OnPeer.peer.faceThumb,

        //////////////////////// 상대방 ///////////////////////////////////////
        var targetUID: String = "",
        var targetNick: String = "",
        var targetThumb: String? = null,

        var peerList: ArrayList<String>? = null,    // 채팅 참여자 : 검색을 위해 필요.

        ////////////////// for functions ///////////////////////////////////////////
        var notRead: Int = 0,              // 봤는지여부 체크하는 int.
        var lastComment: String? = null,       // 마지막 채팅
        var lastTime: Long = 0,          // 마지막 채팅시간
        ////////////////////////////////////////////////////////////////////////////

        var closedUID: String? = null,    // 채팅 닫은 peer
        var statusNo: Int = Status.NORMAL.no,
        var addDate: Long = rightNow()

) : Parcelable {
    constructor(target: Peer) : this(
            targetUID = target.uid,
            targetNick = target.nick,
            targetThumb = target.faceThumb,

            peerList = createPeerList(target)
    )

    constructor(source: Parcel) : this(
            source.readString()!!,
            source.readString()!!,
            source.readString()!!,
            source.readString(),
            source.readString()!!,
            source.readString()!!,
            source.readString(),
            source.createStringArrayList(),
            source.readInt(),
            source.readString(),
            source.readLong(),
            source.readString(),
            source.readInt(),
            source.readLong()
    )

    override fun describeContents() = 0

    override fun writeToParcel(dest: Parcel, flags: Int) = with(dest) {
        writeString(key)
        writeString(authorUID)
        writeString(authorNick)
        writeString(authorThumb)
        writeString(targetUID)
        writeString(targetNick)
        writeString(targetThumb)
        writeStringList(peerList)
        writeInt(notRead)
        writeString(lastComment)
        writeLong(lastTime)
        writeString(closedUID)
        writeInt(statusNo)
        writeLong(addDate)
    }

    companion object {
        fun createPeerList(target: Peer) =
                arrayListOf<String>().apply {
                    add(myuid())
                    add(target.uid)
                }


        @JvmField
        val CREATOR: Parcelable.Creator<Lobby> = object : Parcelable.Creator<Lobby> {
            override fun createFromParcel(source: Parcel): Lobby = Lobby(source)
            override fun newArray(size: Int): Array<Lobby?> = arrayOfNulls(size)
        }
    }
}
