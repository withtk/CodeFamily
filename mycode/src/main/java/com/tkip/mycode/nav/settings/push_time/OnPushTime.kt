package com.tkip.mycode.nav.settings.push_time

import android.content.Context
import android.content.Intent
import android.view.View
import com.tkip.mycode.funs.common.lllogI
import com.tkip.mycode.init.base.Sred
import com.tkip.mycode.init.sredBoolean
import com.tkip.mycode.init.sredInt
import com.tkip.mycode.util.lib.TransitionHelper
import com.tkip.mycode.util.tools.date.rightNowHour

class OnPushTime {

    companion object {

        fun gotoPushTimeActivity(context: Context, arrPair: Array<androidx.core.util.Pair<View, String>>?) {
            Intent(context, PushTimeActivity::class.java).apply {
                addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
                TransitionHelper.startTran(context, this, arrPair)
            }
        }


        /**  방해 금지 시간이면 true */
        fun isNoPushTime():Boolean{

                if (Sred.PUSH_TIME_SET.sredBoolean(false)) {
                    val nowHour = rightNowHour
                    val t1 = Sred.PUSH_TIME1.sredInt(0)
                    val t2 = Sred.PUSH_TIME2.sredInt(23)
                    lllogI("PushTimeActivity isNoPushTime nowHour $nowHour time1 $t1  time2 $t2")
                    val result =
                            when {
                                (t2 == t1) -> true
                                (t1 < t2) -> (nowHour in t1 until t2)
                                (t1 > t2) ->
                                    when {
                                        (nowHour in t1 until 24) -> true
                                        (nowHour < t2) -> true
                                        else -> false
                                    }
                                else -> true   // 같을때
                            }
                    lllogI(" =======   $result  $nowHour    ")
                    return result
                } else return false
        }

    }
}