package com.tkip.mycode.nav.store.tab_stars

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.tkip.mycode.R
import com.tkip.mycode.databinding.FragmentStoreRatingBinding
import com.tkip.mycode.dialog.progress.MyProgress
import com.tkip.mycode.dialog.progress.PPro
import com.tkip.mycode.funs.common.getNullStr
import com.tkip.mycode.init.base.Cons
import com.tkip.mycode.init.my_bind.BindingFragment
import com.tkip.mycode.model.store.Store
import com.tkip.mycode.model.store.StoreRating
import com.tkip.mycode.util.tools.etc.OnMyClick


class StoreRatingFrag : BindingFragment<FragmentStoreRatingBinding>() {
    override fun getLayoutResId(): Int = R.layout.fragment_store_rating



    //    private lateinit var adapter: StoreRatingAdapterFireS
    private val store: Store by lazy { arguments?.getParcelable(Cons.PUT_OBJECT) ?: Store() }

    companion object {
        @JvmStatic
        fun newInstance(argStore: Store) = StoreRatingFrag().apply { arguments = Bundle().apply { putParcelable(Cons.PUT_OBJECT, argStore) } }
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        initView()

        getEsDataList()
    }

    private fun initView() {
        b.fragment = this
        b.store = store


        /** 내 storeRating 세팅 */
//        Firee.getMyStoreRating(store,
//                success = { myStoreRating ->
//                    myStoreRating?.let {
//                        b.myStoreRating = it
//                        b.myStar.rating = it.rating.toFloat()
//                    }
//                }, fail = {})


        /**  Rating 세팅 */
//        Firee.getRating(store,
//                success = {
//                    it?.let {
//                        b.rating = it
//                        b.totalStar.rating = it.averageFloat
//                    }
//                }, fail = {})
    }


    private fun getEsDataList() {
//        esLastIndex = 0   // 꼭 해야 됨. esLastIndex로 loadMOre을 변경할 것이기 때문에.
//        vmStoreMenu.getEsRoomList(esSize, esLastIndex, Status.BLIND)
//        b.inData.tvRefresh.gone()
    }


    private fun setUpRVfireS() {

//        val query = FirebaseFirestore.getInstance().collection(Firee.COL_STORE).document(store.key).collection(Firee.COL_RATING_UID)
//                .orderBy(Firee.CH_ADD_DATE, Query.Direction.DESCENDING)
//        query.get()
//                .addOnSuccessListener {
//                    Log.d(Cons.TAG, "setUpRVfireS count : " + it.size())
//                    if (it.isEmpty) {
//                        AnimUtil.showNormal(b.tvNoData, b.btnGet)
//                    } else {
//                        AnimUtil.hideNormal(b.tvNoData, b.btnGet)
//                    }
//                }
//                .addOnFailureListener { e ->
//                    Log.d(Cons.TAG, "StoreRatingFrag setUpRVfireS addOnFailureListener ex", e)
//                }
//        val options = FirestoreRecyclerOptions.Builder<StoreRating>()
//                .setQuery(query, StoreRating::class.java)
//                .build()
//
//        adapter = StoreRatingAdapterFireS(store, options)
//        adapter.setHasStableIds(true)
//        b.rv.adapter = adapter
//        b.rv.itemAnimator!!.changeDuration = 0
//
//        adapter.startListening()
    }


    fun onClickReTry() {
        setUpRVfireS()
    }

    fun onClickSubmit() {
        if (OnMyClick.isDoubleClick()) return

        val storeRating = StoreRating(
                store = store,
                comment = b.etComment.getNullStr(),
                rating = b.myStar.rating.toInt())

        MyProgress.show(activity as AppCompatActivity, PPro(false))
//        Firee.addStoreRating(storeRating,
//                success = {
//                    MyProgress.cancel()
//                    TToast.showNormal(R.string.complete_upload)
//                }, fail = {})

    }


}
