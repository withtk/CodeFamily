package com.tkip.mycode.nav.store

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter

class StorePagerAdapter(manager: FragmentManager,
//                        private val list: ArrayList<Pair<String,Fragment>>,
//                        private val listFragment: ArrayList<Fragment>,
//                        private val listTitle: ArrayList<String>
                          val pairList: ArrayList<Pair<String, Fragment>>) : FragmentPagerAdapter(manager, BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT) {

    override fun getItem(position: Int): Fragment {
        return pairList[position].second
    }

    override fun getCount(): Int {
        return pairList.size
    }

    override fun getPageTitle(position: Int): CharSequence? {
        return pairList[position].first
    }
}