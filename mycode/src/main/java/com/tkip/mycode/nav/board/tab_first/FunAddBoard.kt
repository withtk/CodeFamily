package com.tkip.mycode.nav.board.tab_first

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import com.tkip.mycode.funs.common.lllogI
import com.tkip.mycode.funs.common.lllogM
import com.tkip.mycode.init.ACTION_NEW
import com.tkip.mycode.init.PUT_BOARD_KEY
import com.tkip.mycode.model.board.OnBoard
import com.tkip.mycode.model.peer.isCsBoard
import com.tkip.mycode.util.lib.photo.util.PhotoUtil


//fun AddBoardActivity.finalAdd() {
//    lllogM("AddBoardActivity finalAdd   finalBoard$finalBoard")
//
//    /** <기존 이미지 삭제하기>
//     * 1. 그냥 이전 이미지만 삭제.
//     * 2. 새로운 이미지 올림.  */
//    viewAddPhotoOne?.let {
//        if (it.hasToBeDeletedPhoto) {
//            lllogI("Addboardactivity viewAddPhotoOne  toBeDeletedFoto")
//            PhotoUtil.deletePhoto(finalBoard.photoUrl, finalBoard.photoThumb)
//            finalBoard.photoUrl = null
//            finalBoard.photoThumb = null
//        }
//    }
//
//
//    OnBoard.addPhotoAndBoard(addType, this , board, finalBoard,
//            success = {
//                /** 스토어의 cs 보드 만들었을때는 */
//                if (finalBoard.isCsBoard() && (addType == ACTION_NEW)) {
//                    lllogI("AddBoardActivity addPhotoAndBoard success board1 ${finalBoard.key}")
//                    val intent = Intent()
//                    intent.putExtra(PUT_BOARD_KEY, finalBoard.key)
//                    setResult(AppCompatActivity.RESULT_OK, intent)
//                    finish()
//                } else {
//                    OnBoard.completedDB(this , finalBoard)
//                }
//
//            }, fail = {})
//}