package com.tkip.mycode.nav.board.tab_first

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.tkip.mycode.R
import com.tkip.mycode.databinding.*
import com.tkip.mycode.dialog.alert.MyAlert
import com.tkip.mycode.funs.common.lllogI
import com.tkip.mycode.funs.common.tranPair
import com.tkip.mycode.init.*
import com.tkip.mycode.init.inter.interHolderBasic
import com.tkip.mycode.model.board.Board
import com.tkip.mycode.model.board.OnBoard
import com.tkip.mycode.model.grant.OnGrant
import com.tkip.mycode.model.my_enum.justGo
import com.tkip.mycode.model.my_enum.showStatusDialog
import com.tkip.mycode.model.peer.isAdmin
import com.tkip.mycode.util.tools.anim.gone
import com.tkip.mycode.util.tools.etc.OnMyClick

/**
 * board list
 */
class BoardAdapter(private val items: ArrayList<Board>, val rvType: Int, val onItemClick: ((Board) -> Unit)?) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {
//    companion object {
//        const val rvVert = 100
//        const val rvHori = 200
//        const val rvGridHori = 300
//        const val rvMyContents = 500
//    }

    private lateinit var context: Context

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        this.context = parent.context
        return when (rvType) {
            RV_GRID_VERT -> BoardGridVertViewHolder(HolderBoardGridVertBinding.inflate(LayoutInflater.from(parent.context), parent, false))
            RV_GRID_HORI -> BoardGridHoriHolder(HolderBoardGridHoriBinding.inflate(LayoutInflater.from(parent.context), parent, false))
            RV_HORI -> BoardHoriViewHolder(HolderBoardHoriBinding.inflate(LayoutInflater.from(parent.context), parent, false))
            RV_HORI_BIG -> BoardHoriBigHolder(HolderBoardHoriBigBinding.inflate(LayoutInflater.from(parent.context), parent, false))
            RV_MY_CONTENTS, RV_MY_CONTENTS_MANAGE, RV_MANAGE -> BoardMyConViewHolder(HolderBoardMyConBinding.inflate(LayoutInflater.from(parent.context), parent, false))
            RV_VERT -> BoardVertViewHolder(HolderBoardVertBinding.inflate(LayoutInflater.from(parent.context), parent, false))
            else -> BoardVertViewHolder(HolderBoardVertBinding.inflate(LayoutInflater.from(parent.context), parent, false))

        }
    }

    override fun onBindViewHolder(h: RecyclerView.ViewHolder, p: Int) {

        when (rvType) {
            RV_GRID_VERT -> (h as BoardGridVertViewHolder).bind(items[p])
            RV_GRID_HORI -> (h as BoardGridHoriHolder).bind(items[p])
            RV_HORI -> (h as BoardHoriViewHolder).bind(items[p])
            RV_HORI_BIG -> (h as BoardHoriBigHolder).bind(items[p])
            RV_MY_CONTENTS, RV_MY_CONTENTS_MANAGE, RV_MANAGE -> (h as BoardMyConViewHolder).bind(items[p])
            RV_VERT -> (h as BoardVertViewHolder).bind(items[p])
            else -> (h as BoardVertViewHolder).bind(items[p])
        }
    }


    override fun getItemCount(): Int {
        return items.size
    }


    /******************************************************************************************
     * vert
     *******************************************************************************************/
    inner class BoardVertViewHolder(val b: HolderBoardVertBinding) : RecyclerView.ViewHolder(b.root) {

        private lateinit var board: Board

        fun bind(board: Board) {
            this.board = board
            b.board = board
            b.inter = interHolderBasic(
                    onClickCard = { onClickCard(it) },
                    onClickGo = null,
                    onClickUpdate = { },
                    onClickDel = { }, onDeepDel = {},
                    onClickStatus = null)
            b.executePendingBindings()
        }

        fun onClickCard(v: View) {
            OnMyClick.setDisableAWhileBTN(v)

            onItemClick?.let {
                it(board)
                return
            }

            OnBoard.checkAndGo(context, true, board.key, arrayOf(b.tvTitle.tranPair(R.string.tran_toolbar)), null, null)

        }

    }

    /******************************************************************************************
     * gridVert
     *******************************************************************************************/
    inner class BoardGridVertViewHolder(val b: HolderBoardGridVertBinding) : RecyclerView.ViewHolder(b.root) {

        private lateinit var board: Board

        fun bind(board: Board) {
            this.board = board
            b.holder = this
            b.board = board
            b.executePendingBindings()
        }

        fun onClickCard(v: View) {
            OnMyClick.setDisableAWhileBTN(v)
            lllogI("BoardAdapter BoardHoriViewHolder onClickCard")

            onItemClick?.let {
                it(board)
                return
            }

            OnBoard.checkAndGo(context, true, board.key, arrayOf(b.tvTitle.tranPair(R.string.tran_toolbar)), null, null)

        }

    }


    /******************************************************************************************
     * hori
     *******************************************************************************************/
    inner class BoardHoriViewHolder(val b: HolderBoardHoriBinding) : RecyclerView.ViewHolder(b.root) {

        private lateinit var board: Board

        fun bind(board: Board) {
            this.board = board
            b.holder = this
            b.board = board
            b.executePendingBindings()
        }

        fun onClickCard(v: View) {
            OnMyClick.setDisableAWhileBTN(v)
            lllogI("BoardAdapter BoardHoriViewHolder onClickCard")

            onItemClick?.let {
                it(board)
                return
            }

            OnBoard.checkAndGo(context, true, board.key, arrayOf(b.tvTitle.tranPair(R.string.tran_toolbar)), null, null)

        }

    }


    /******************************************************************************************
     * hori BIG
     *******************************************************************************************/
    inner class BoardHoriBigHolder(val b: HolderBoardHoriBigBinding) : RecyclerView.ViewHolder(b.root) {

        private lateinit var board: Board

        fun bind(board: Board) {
            this.board = board
            b.holder = this
            b.board = board
            b.executePendingBindings()
        }

        fun onClickCard(v: View) {
            OnMyClick.setDisableAWhileBTN(v)
            lllogI("BoardAdapter BoardHoriViewHolder onClickCard")

            onItemClick?.let {
                it(board)
                return
            }

            OnBoard.checkAndGo(context, true, board.key, arrayOf(b.tvTitle.tranPair(R.string.tran_toolbar)), null, null)

        }

    }


    /******************************************************************************************
     * grid  hori
     *******************************************************************************************/
    inner class BoardGridHoriHolder(val b: HolderBoardGridHoriBinding) : RecyclerView.ViewHolder(b.root) {

        private lateinit var board: Board

        fun bind(board: Board) {
            this.board = board
            b.holder = this
            b.board = board
            b.executePendingBindings()
        }

        fun onClickCard(v: View) {
            OnMyClick.setDisableAWhileBTN(v)

            onItemClick?.let {
                it(board)
                return
            }
            OnBoard.checkAndGo(context, true, board.key, arrayOf(b.tvTitle.tranPair(R.string.tran_toolbar)), null, null)
        }
    }


    /******************************************************************************************
     * myContents, manage
     *******************************************************************************************/
    inner class BoardMyConViewHolder(val b: HolderBoardMyConBinding) : RecyclerView.ViewHolder(b.root) {

        private lateinit var board: Board

        fun bind(board: Board) {
            this.board = board
            b.board = board
            b.inter = interHolderBasic(
                    onClickCard = { onClickCard(it) },
                    onClickGo = {
                        /*** 승인 요청 버튼 */
                        OnGrant.request(context, board, onSuccess = { b.btnGrant.gone() })         // 승인 요청.
//                        MyAlert.showRequestGrant(context, board, onSuccess = {}, onFail = {})
//                        OnGrant.request(context, board, onSuccess = { b.btnGrant.goneFadeOut(false) })
                    },
                    onClickUpdate = null,
                    onClickDel = null, onDeepDel = {},
                    onClickStatus = { if (isAdmin()) board.showStatusDialog(context, null) else onClickCard(it) })

            b.cons.setOnLongClickListener {
                board.showStatusDialog(context, onSuccess = { status, _ ->
                    status?.let {
                        board.statusNo = it.no
                        notifyItemChanged(adapterPosition)
                    }
                })
                return@setOnLongClickListener true
            }


            b.executePendingBindings()
            b.btnGrant.gone()   // 이걸 안 해주면 notify했을 때 이상하게 나와.
        }

        fun onClickCard(v: View) {
            OnMyClick.setDisableAWhileBTN(v)

            OnGrant.checkStatus(context, board, b.btnGrant,
                    onNormal = { board.justGo(context, true, arrayOf(b.tvTitle.tranPair(R.string.tran_linear))) },
//                    onAvailRequest = { hhh(OnGrant.INTERVAL_BTN_SHOW) {if (it) b.btnGrant.visi() else b.btnGrant.gone()} },
                    onDeleted = {
                        /*** Status.Deleted 완료시 rv에서 제거. */
                        items.removeAt(adapterPosition)
                        notifyItemRemoved(adapterPosition)
                    })

        }

    }


}