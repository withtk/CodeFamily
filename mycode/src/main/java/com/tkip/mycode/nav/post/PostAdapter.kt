package com.tkip.mycode.nav.post

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.tkip.mycode.R
import com.tkip.mycode.databinding.HolderPostBinding
import com.tkip.mycode.databinding.HolderPostMyConBinding
import com.tkip.mycode.dialog.OnDDD
import com.tkip.mycode.dialog.alert.MyAlert
import com.tkip.mycode.dialog.toast.milkStatusAdmin
import com.tkip.mycode.funs.common.tranPair
import com.tkip.mycode.init.RV_MANAGE
import com.tkip.mycode.init.RV_MY_CONTENTS
import com.tkip.mycode.init.RV_MY_CONTENTS_MANAGE
import com.tkip.mycode.init.RV_VERT
import com.tkip.mycode.init.inter.interHolderBasic
import com.tkip.mycode.model.grant.OnGrant
import com.tkip.mycode.model.my_enum.*
import com.tkip.mycode.model.peer.isAdmin
import com.tkip.mycode.model.post.OnPost
import com.tkip.mycode.model.post.Post
import com.tkip.mycode.util.tools.anim.gone
import com.tkip.mycode.util.tools.etc.OnMyClick

class PostAdapter(val items: ArrayList<Post>, val rvType: Int, val onItemClick: ((Post) -> Unit)?) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

//    companion object {
//        const val rvVert = 100
//        const val rvMyContents = 200
//    }

    private lateinit var context: Context

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        this.context = parent.context
        return when (rvType) {
            RV_VERT -> PostViewHolder(HolderPostBinding.inflate(LayoutInflater.from(parent.context), parent, false))
            RV_MY_CONTENTS, RV_MY_CONTENTS_MANAGE, RV_MANAGE -> PostMyConViewHolder(HolderPostMyConBinding.inflate(LayoutInflater.from(parent.context), parent, false))
            else -> PostViewHolder(HolderPostBinding.inflate(LayoutInflater.from(parent.context), parent, false))

        }
    }

    override fun onBindViewHolder(h: RecyclerView.ViewHolder, p: Int) {
        when (rvType) {
            RV_VERT -> (h as PostViewHolder).bind(items[p])
            RV_MY_CONTENTS, RV_MY_CONTENTS_MANAGE, RV_MANAGE -> (h as PostMyConViewHolder).bind(items[p])
            else -> (h as PostViewHolder).bind(items[p])
        }
    }

    override fun getItemCount(): Int {
        return items.size
    }

    /******************************************************************************************
     *  기본 rvVert
     *****************************************************************************************/
    inner class PostViewHolder(val b: HolderPostBinding) : RecyclerView.ViewHolder(b.root) {

        private lateinit var post: Post

        fun bind(post: Post) {
            this.post = post
            b.holder = this
            b.post = post

            b.executePendingBindings()
        }

        fun onClickCard(v: View) {
            OnMyClick.setDisableAWhileBTN(v)

            onItemClick?.let {
                it(post)
                return
            }

            OnPost.checkAndGo(context, false, post.key, Status.NORMAL,
                    arrayOf(b.line.tranPair(R.string.tran_linear)),
                    onSuccess = {
                        // transition이 안 먹어서 수정하면 안 될 듯.

//                        post.mycopy(it)
//                        items[adapterPosition] = it
//                        notifyItemChanged(adapterPosition)
                    },
                    onNotNormal = {
                        items[adapterPosition] = it
                        notifyItemChanged(adapterPosition)
                    })

        }


        fun onClickStatus(v: View) {
            OnMyClick.setDisableAWhileBTN(v)
            if (isAdmin()) {
                milkStatusAdmin(post.statusNo.getStatus().msg)

                OnDDD.allowAdmin(context, ok = { OnPost.checkAndGo(context, false, post.key, Status.NOTHING, arrayOf(b.line.tranPair(R.string.tran_linear)), null, null) })

            }
        }
    }

    /******************************************************************************************
     *  rvMyContents, manage
     *****************************************************************************************/
    inner class PostMyConViewHolder(val b: HolderPostMyConBinding) : RecyclerView.ViewHolder(b.root) {

        private lateinit var post: Post

        fun bind(post: Post) {
            this.post = post
//            b.holder = this
            b.post = post
            b.inter = interHolderBasic(
                    onClickCard = { onClickCard(it) },
                    onClickGo = {
                        /** 승인 요청 */
                        OnGrant.request(context, post, onSuccess = { b.btnGrant.gone() })    // 승인 요청.
                    },
                    onClickUpdate = null,
                    onClickDel = null,
                    onDeepDel = { post.deepDel(context, onSuccess = {}) },
                    onClickStatus = { if (isAdmin()) post.showStatusDialog(context, null) else onClickCard(it) })

            b.line.setOnLongClickListener {
                post.showStatusDialog(context, onSuccess = { status, _ ->
                    status?.let {
                        post.statusNo = it.no
                        notifyItemChanged(adapterPosition)
                    }
                })
                return@setOnLongClickListener true
            }


            b.executePendingBindings()

            b.btnGrant.gone()   // 이걸 안 해주면 notify했을 때 이상하게 나와.
        }

        fun onClickCard(v: View) {
            OnMyClick.setDisableAWhileBTN(v)

            OnGrant.checkStatus(context, post, b.btnGrant,
                    onNormal = { post.justGo(context, true, arrayOf(b.tvTitle.tranPair(R.string.tran_linear))) },
//                    onAvailRequest = { if (it) b.btnGrant.visi() else b.btnGrant.gone() },
                    onDeleted = {
                        /*** Status.Deleted 완료시 rv에서 제거. */
                        items.removeAt(adapterPosition)
                        notifyItemRemoved(adapterPosition)
                    })

        }

    }
}