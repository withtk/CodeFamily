package com.tkip.mycode.nav.room

import android.Manifest.permission.READ_EXTERNAL_STORAGE
import android.content.pm.PackageManager
import android.os.Bundle
import androidx.annotation.LayoutRes
import androidx.core.content.ContextCompat
import com.tkip.mycode.R
import com.tkip.mycode.admin.versionGteQ
import com.tkip.mycode.databinding.ActivityAddRoomBinding
import com.tkip.mycode.dialog.alert.MyAlert
import com.tkip.mycode.funs.common.*
import com.tkip.mycode.init.*
import com.tkip.mycode.init.base.*
import com.tkip.mycode.init.inter.interManualMore
import com.tkip.mycode.init.inter.interRequestPermissionsResult
import com.tkip.mycode.init.my_bind.BindingActivityPhoto
import com.tkip.mycode.model.media.addViewMediaEdittext
import com.tkip.mycode.model.media.addViewMediaSpinner
import com.tkip.mycode.model.media.getFinalMediaMap
import com.tkip.mycode.model.media.setupMediaEditText
import com.tkip.mycode.model.my_enum.PickerFactory
import com.tkip.mycode.model.mytag.TagTYPE
import com.tkip.mycode.model.mytag.convertStringList
import com.tkip.mycode.model.peer.OnPeer
import com.tkip.mycode.model.room.*
import com.tkip.mycode.util.lib.map.OnMap
import com.tkip.mycode.util.lib.photo.Foto
import com.tkip.mycode.util.lib.photo.initSelectedFotoList
import com.tkip.mycode.util.lib.photo.invalidFotoList
import com.tkip.mycode.util.lib.photo.view.LinearAddPhotoBox
import com.tkip.mycode.util.my_view.ViewUtil
import com.tkip.mycode.util.tools.anim.AnimUtil
import com.tkip.mycode.util.tools.etc.OnMyClick
import com.tkip.mycode.util.tools.picker.showDatePicker

/**
 * PUT_POST -> 빈 post or 수정할 post
 * post는 항상 board.key와 name을 잃어버리지 말아야 한다.
 * //todo: 재생성시 radioGroup 다시 셋팅하기는 아직 안 만들었음.
 */
class AddRoomActivity : BindingActivityPhoto<ActivityAddRoomBinding>() {
    @LayoutRes
    override fun getLayoutResId() = R.layout.activity_add_room

    //    private var map: GoogleMap? = null
    private val addType by lazy { intent.getIntExtra(PUT_ACTION, ACTION_NEW) }
    private val room by lazy { intent.getParcelableExtra<Room>(PUT_ROOM) ?: Room() }
    private lateinit var finalRoom: Room

//    private var isGone = false   // db 업로드로 넘어갔을 때

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        initView()
        finalRoom = Room(room)
        initData()
    }

    private fun initView() {
//        val mapFragment = supportFragmentManager.findFragmentById(R.id.map) as SupportMapFragment
//        mapFragment.getMapAsync(this)
        setSupportActionBar(b.toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        b.activity = this
        b.addType = addType
        b.versionGteQ = versionGteQ
        b.inMyCredit.peer = OnPeer.peer


        /*** 매뉴얼  */
        b.inManual.inter = interManualMore {
            b.inManual.tvManual1.maxLines = 100
            AnimUtil.goneNormal(b.inManual.btnMoreInfo)
        }
        b.inManual.manual = R.string.manual_top_add_room.getStr()

        /*** 사진박스  */
        viewAddPhotoBox = LinearAddPhotoBox(this@AddRoomActivity, onOpenAlbum = {
            OnPermission.onOpenAlbum(true, this@AddRoomActivity)
        }).apply { this@AddRoomActivity.b.frameAddPhotoBox.addView(this) }

        /**         * 미디어 스피너 setup         */
        b.lineMedia.addViewMediaSpinner(this@AddRoomActivity,
                onClick = { b.lineMedia.addViewMediaEdittext(this@AddRoomActivity, it) })

        /*** 지도, 태그, 빌트인 리스트 */
        linearAddMap = ViewUtil.addLinearMapAdd(this@AddRoomActivity, b.lineTag, null, room.address, OnMap.createLatlng(room.lat, room.lng))
        linearAddTag = ViewUtil.addLinearAddTag(this@AddRoomActivity, b.lineTag, TagTYPE.ROOM, R.string.name_tag_search.getStr(), R.string.btn_open_tag_dialog.getStr(), room.tagList)
        linearAddTagBuilt = ViewUtil.addLinearAddTag(this@AddRoomActivity, b.lineTag, TagTYPE.BUILT, R.string.name_builtin.getStr(), R.string.btn_open_builtin_dialog.getStr(), room.builtInList)


        /********************* superListener *****************************/
        listenerPermissionsResult = interRequestPermissionsResult(onReadExternalStorage = { OnPermission.onOpenAlbum(it, this@AddRoomActivity) })

    }

    /**
     * 0. 기본정보 : binding에서
     * 1. amount :
     * 2. map
     * 3. tagList  : binding에서
     */
    private fun initData() {
        lllogI("AddRoomActivity initData $finalRoom")
        b.room = finalRoom

        /**         * 라디오 버튼 setup & 초기화        */
        setupAllRadioGroup(finalRoom, b.lineCompulsory, b.lineCompulsoryAmount, b.lineAdditionalRadio)

        /**         * 미디어 스피너 초기화        */
        finalRoom.mediaMap.setupMediaEditText(this@AddRoomActivity, b.lineMedia)  // 초기화.

        /**         *  사진 보이기         */
        viewAddPhotoBox?.setPhotos(finalRoom.fotoList)

        /**         * 추가 설정사항 버튼        */
        if (finalRoom.hasAdditionalData()) onClickAdditional()

        /** * 지도 초기화 : onMapReady 에서 실행됨.  */

    }

    /**     * 올릴 room 준비하기.     */
    private fun getFinalRoom() =

            finalRoom.apply {
                lllogI("AddRoomActivity  finalRoom ${finalRoom}")

                local = b.tvLocal.getNullStr()
                price = b.tvPrice.getNullStr()?.removeComma()?.toInt()
                areaEx = b.etAreaEx.getNullStr()?.replace(",", "")?.toInt() ?: 0
                areaTotal = b.etAreaTotal.getNullStr()?.replace(",", "")?.toInt() ?: 0
                contact = b.etContacts.getNullStr()
                title = b.etTitle.getNullStr()
                comment = b.etComment.getNullStr()
                matchAmountFromRG(this)

                /*** map 정보 삽입*/
                address = linearAddMap?.b?.etAddress?.getNullStr()
                lat = linearAddMap?.latLng?.latitude
                lng = linearAddMap?.latLng?.longitude

                /** 태그 삽입 */
                linearAddTag?.b?.tagView?.convertStringList()?.let { tagList = tagList.clearAddAll(it) }
                linearAddTagBuilt.b.tagView?.convertStringList()?.let { builtInList = builtInList.clearAddAll(it) }

                /** 미디어 아이디 삽입 */
                mediaMap = getFinalMediaMap()

                /*** constructionDate :  onClickConstructionDate 에서 이미 삽입. */

                /** 사진 삽입 */
                fotoList = fotoList.clearAddAll(viewAddPhotoBox?.selectedFotoList)

            }

    /*************************************************************************************
     *************************************************************************************
     *************************************************************************************/
    override fun onRestoreInstanceState(save: Bundle) {
        super.onRestoreInstanceState(save)
        lllogI("addRoomActivity onRestoreInstanceState  ")

        save.getParcelable<Room>(PUT_RESULT_OBJECT)?.let { room1 ->
            lllogI("addRoomActivity PUT_RESULT_OBJECT  ")
            finalRoom = room1
            initData()
        } ?: finish()

        /**         * 퍼미션 있을때에만 앨범에서 사진 가져오기.         */
        //todo: 앨범의 사진이 삭제되면 문제. 그냥 모두 초기화 하는 것이 나아 보인다. 굳이 어렵게 짤 필요가...
        val readPermission = ContextCompat.checkSelfPermission(this@AddRoomActivity, READ_EXTERNAL_STORAGE)
        if (readPermission == PackageManager.PERMISSION_GRANTED) {
            save.getParcelableArrayList<Foto>(PUT_RESULT_LIST)?.let { list ->
                viewAddPhotoBox?.selectedFotoList?.initSelectedFotoList(list)
                viewAddPhotoBox?.selectedFotoAdapter?.notifyDataSetChanged()
            }
        }
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        lllogI("addRoomActivity onSaveInstanceState  ")

        if (!versionGteQ) {
            outState.putParcelable(PUT_RESULT_OBJECT, getFinalRoom())
            outState.putParcelableArrayList(PUT_RESULT_LIST, viewAddPhotoBox?.selectedFotoList)
        }
    }

    /*************************************************************************************
     *************************************************************************************
     *************************************************************************************/
    override fun onBackPressed() {
//        if (OnValid.checkKeyboardHide(b.etComment, b.etAreaEx, b.etAreaTotal, b.etContacts, b.etTitle)) return
        OnValid.removeFocusEt(b.etComment,b.etContacts,b.etTitle, b.etAreaEx, b.etAreaTotal)

        if (finalRoom.isWriting()) {
            MyAlert.showConfirm(this@AddRoomActivity, getString(R.string.info_wanna_leave_add_room),
                    ok = { super.onBackPressed() }, cancel = {})
        } else super.onBackPressed()
    }


    fun onClickTagDialogOne() {
        if (OnMyClick.isDoubleClick()) return
        MyAlert.showAddTag(this@AddRoomActivity, true, b.tvLocal.getNullStr(), TagTYPE.LOC, null,
                onSingleResult = {
                    lllogI("onClickTagDialogOne onSingleResult $it")
                    b.tvLocal.text = it
                    finalRoom.local = it
                })
    }

    fun onClickPrice() {
        if (OnMyClick.isDoubleClick()) return
        MyAlert.selectPrice(this,finalRoom, b.tvPrice.getInt(), 1000, onSuccess = { s, i -> b.tvPrice.text = s })
//        MyAlert.showNumberPicker(this@AddRoomActivity, PickerFactory.PESO, b.tvPrice.getNullStr(), onOk = { amount, commaStr ->
//            lllogI("RoomAmount showNumberPicker $commaStr")
//            b.tvPrice.text = commaStr
//        })
    }


    fun onClickAdditional() {
        if (OnMyClick.isDoubleClick()) return
        AnimUtil.hideSlideDown(b.btnAdditional, false)
        AnimUtil.showSlideDown(b.lineSelective, false)
        AnimUtil.showSlideDown(b.tvInfoAdditional, false)
    }

    fun onClickConstructionDate() {
        if (OnMyClick.isDoubleClick()) return
        showDatePicker(this@AddRoomActivity, onSelect = { _, y, m, _ ->
            val date = "$y${m.format02Plus()}"
            lllogI("RoomAmount DatePickerDialog date $date")
            finalRoom.constructionDate = date.toInt()
            MyBinding.textConstructionDate(b.tvConstruction, date.toInt())
        })
    }

    fun onClickPreview() {
        if (OnMyClick.isDoubleClick()) return
        OnRoom.gotoRoomActivity(this@AddRoomActivity, false, ACTION_PREVIEW, getFinalRoom(), null)
    }

    fun onClickSubmit() {
        if (OnMyClick.isDoubleClick()) return

        if (viewAddPhotoBox?.selectedFotoList.invalidFotoList()) return
        if (getFinalRoom().inValid()) return

        MyAlert.showConfirm(this@AddRoomActivity, R.string.info_wanna_upload_add_post.getStr(),
                ok = {
                    finalAdd()
                }, cancel = {})
    }


    private fun finalAdd() {
        OnRoom.addPhotoAndRoom(addType, this@AddRoomActivity, null, viewAddPhotoBox?.selectedFotoList!!, room, finalRoom, {}, {})
    }

}
