package com.tkip.mycode.nav.my_room.my_contents

import android.os.Bundle
import android.os.Handler
import androidx.annotation.LayoutRes
import androidx.fragment.app.Fragment
import com.tkip.mycode.R
import com.tkip.mycode.databinding.ActivityMyContentsBinding
import com.tkip.mycode.funs.common.onPageChange
import com.tkip.mycode.funs.custom.toolbar.ViewToolbarIcon
import com.tkip.mycode.init.PUT_RV_TYPE
import com.tkip.mycode.init.PUT_UID
import com.tkip.mycode.init.RV_MANAGE
import com.tkip.mycode.init.RV_MY_CONTENTS
import com.tkip.mycode.init.base.getStr
import com.tkip.mycode.init.my_bind.BindingActivity
import com.tkip.mycode.model.my_enum.DataType
import com.tkip.mycode.util.tools.view_pager.OnPager

/**
 * 1. My Contents : board, post, flea, room, store
 * 2. 전체 모델의 내역 : board, post, flea, room, store
 */
class MyContentsActivity : BindingActivity<ActivityMyContentsBinding>() {
    @LayoutRes
    override fun getLayoutResId() = R.layout.activity_my_contents

    //        private val manage by lazy { intent?.getBooleanExtra(PUT_MANAGE, false) ?: false }
//    private val dataType by lazy {  intent?.getIntExtra(PUT_DATA_TYPE_NO,DataType.BOARD.no)?.getDataType()!! }
    private val uid by lazy { intent?.getStringExtra(PUT_UID) }
    private val rvType by lazy {
        intent?.getIntExtra(PUT_RV_TYPE, RV_MY_CONTENTS) ?: RV_MY_CONTENTS
    }
    private val fragList = arrayListOf<Pair<Fragment, String>>()


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        b.activity = this

        /** 툴바 */
        val title = if (rvType == RV_MANAGE) R.string.title_model_list.getStr() else R.string.title_my_contents_list.getStr()
        b.frameToolbar.addView(ViewToolbarIcon(this@MyContentsActivity, title, onBack = { onBackPressed() }))

        /** viewPager */
        when (rvType) {
            RV_MANAGE -> arrayOf(DataType.REPLY, DataType.BOARD, DataType.POST, DataType.FLEA, DataType.ROOM, DataType.STORE).forEach { fragList.add(Pair(MyContentsFrag.newInstance(uid, it.no, rvType, null), it.title)) }
            else -> arrayOf(DataType.BOARD, DataType.POST, DataType.FLEA, DataType.ROOM, DataType.STORE).forEach { fragList.add(Pair(MyContentsFrag.newInstance(uid, it.no, rvType, null), it.title)) }
        }

        OnPager.commonSetPager(supportFragmentManager, fragList, b.viewPager, b.tabLayout)
        b.viewPager.onPageChange(onState = {}, onScrolled = { _, _, _ -> }, onSelected = { p ->
            (fragList[p].first).let { frag -> if (frag is MyContentsFrag) frag.initSearch() }     // viewPager 보여질때 검색시작(최초 한번만).
        })

        /*** 0번 Frag는 바로 초기 검색 시작. */
        Handler().postDelayed({ (fragList[0].first).let { frag -> if (frag is MyContentsFrag) frag.initSearch() } }, 500)

    }


}