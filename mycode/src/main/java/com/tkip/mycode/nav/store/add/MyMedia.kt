package com.tkip.mycode.nav.store.add

import android.content.Context
import android.util.Log
import com.tkip.mycode.init.part.CodeApp
import com.tkip.mycode.R
import com.tkip.mycode.init.base.Cons

enum class MyMedia( ) {
    KAKAO, LINE, SKYPE, WHATSAPP, WECHAT, TELEGRAM, FACEBOOK_MESSENGER, TWITTER, VIBER, SLACK;
}

fun String.getMedia(): MyMedia {
    return when (this) {
        MyMedia.KAKAO.name -> MyMedia.KAKAO
        MyMedia.LINE.name -> MyMedia.LINE
        MyMedia.SKYPE.name -> MyMedia.SKYPE
        MyMedia.WHATSAPP.name -> MyMedia.WHATSAPP
        MyMedia.WECHAT.name -> MyMedia.WECHAT
        MyMedia.TELEGRAM.name -> MyMedia.TELEGRAM
        MyMedia.FACEBOOK_MESSENGER.name -> MyMedia.FACEBOOK_MESSENGER
        MyMedia.TWITTER.name -> MyMedia.TWITTER
        MyMedia.VIBER.name -> MyMedia.VIBER
        MyMedia.SLACK.name -> MyMedia.SLACK
        else->MyMedia.KAKAO
    }

}

fun MyMedia.getMediaResId(): Int {
    return when (this) {
        MyMedia.KAKAO -> R.drawable.em_kakaotalk
        MyMedia.LINE -> R.drawable.em_line
        MyMedia.SKYPE -> R.drawable.em_skype
        MyMedia.WHATSAPP -> R.drawable.em_whatsapp
        MyMedia.WECHAT -> R.drawable.em_wechat
        MyMedia.TELEGRAM -> R.drawable.em_telegram
        MyMedia.FACEBOOK_MESSENGER -> R.drawable.em_facebook_messenger
        MyMedia.TWITTER -> R.drawable.em_twitter
        MyMedia.VIBER -> R.drawable.em_viber
        MyMedia.SLACK -> R.drawable.em_slack
    }

}


fun MyMedia.getMediaName(): String? {
    val strId = when (this) {
        MyMedia.KAKAO -> R.string.media_kakao
        MyMedia.LINE -> R.string.media_line
        MyMedia.SKYPE -> R.string.media_skype
        MyMedia.WHATSAPP -> R.string.media_whatsapp
        MyMedia.WECHAT -> R.string.media_wechat
        MyMedia.TELEGRAM -> R.string.media_telegram
        MyMedia.FACEBOOK_MESSENGER -> R.string.media_facebook_messenger
        MyMedia.TWITTER -> R.string.media_twitter
        MyMedia.VIBER -> R.string.media_viber
        MyMedia.SLACK -> R.string.media_slack
    }

    return CodeApp.getAppContext().getString(strId)
}


fun MyMedia.getMediaHint(): String? {
    val strId = when (this) {
        MyMedia.KAKAO -> R.string.hint_media_kakao
        MyMedia.LINE -> R.string.hint_media_line
        MyMedia.SKYPE -> R.string.hint_media_skype
        MyMedia.WHATSAPP -> R.string.hint_media_whatsapp
        MyMedia.WECHAT -> R.string.hint_media_wechat
        MyMedia.TELEGRAM -> R.string.hint_media_telegram
        MyMedia.FACEBOOK_MESSENGER -> R.string.hint_media_facebook_messenger
        MyMedia.TWITTER -> R.string.hint_media_twitter
        MyMedia.VIBER -> R.string.hint_media_viber
        MyMedia.SLACK -> R.string.hint_media_slack
    }

    return CodeApp.getAppContext().getString(strId)
}


fun MyMedia.onClick(context: Context, mediaId: String) {
    when (this) {
        MyMedia.KAKAO -> Log.d(Cons.TAG, this.name + mediaId)
        MyMedia.LINE -> Log.d(Cons.TAG, this.name + mediaId)
        MyMedia.SKYPE -> Log.d(Cons.TAG, this.name + mediaId)
        MyMedia.WHATSAPP -> Log.d(Cons.TAG, this.name + mediaId)
        MyMedia.WECHAT -> Log.d(Cons.TAG, this.name + mediaId)
        MyMedia.TELEGRAM -> Log.d(Cons.TAG, this.name + mediaId)
        MyMedia.FACEBOOK_MESSENGER -> Log.d(Cons.TAG, this.name + mediaId)
        MyMedia.TWITTER -> Log.d(Cons.TAG, this.name + mediaId)
        MyMedia.VIBER -> Log.d(Cons.TAG, this.name + mediaId)
        MyMedia.SLACK -> Log.d(Cons.TAG, this.name + mediaId)
    }
}

