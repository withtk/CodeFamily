package com.tkip.mycode.nav.post

import android.os.Bundle
import android.view.View
import androidx.annotation.LayoutRes
import com.tkip.mycode.R
import com.tkip.mycode.admin.versionGteQ
import com.tkip.mycode.databinding.ActivityAddPostBinding
import com.tkip.mycode.dialog.alert.MyAlert
import com.tkip.mycode.dialog.progress.MyCircle
import com.tkip.mycode.funs.common.OnPermission
import com.tkip.mycode.funs.common.OnValid
import com.tkip.mycode.funs.common.getStr
import com.tkip.mycode.funs.common.lllogI
import com.tkip.mycode.init.*
import com.tkip.mycode.init.base.clearAddAll
import com.tkip.mycode.init.base.getStr
import com.tkip.mycode.init.base.isNullBlankAndSetError
import com.tkip.mycode.init.inter.interManualMore
import com.tkip.mycode.init.inter.interRequestPermissionsResult
import com.tkip.mycode.init.my_bind.BindingActivityPhoto
import com.tkip.mycode.model.cost.pay.PAY_ACT_TYPE_LIST
import com.tkip.mycode.model.cost.pay.checkAndShowPay
import com.tkip.mycode.model.my_enum.ActType
import com.tkip.mycode.model.mytag.LinearAddTag
import com.tkip.mycode.model.mytag.TagTYPE
import com.tkip.mycode.model.mytag.convertStringList
import com.tkip.mycode.model.peer.isCsBoard
import com.tkip.mycode.model.peer.mypeer
import com.tkip.mycode.model.post.OnPost
import com.tkip.mycode.model.post.Post
import com.tkip.mycode.model.post.inValid
import com.tkip.mycode.util.lib.photo.invalidFotoList
import com.tkip.mycode.util.lib.photo.isOverLimit
import com.tkip.mycode.util.lib.photo.view.LinearAddPhotoBox
import com.tkip.mycode.util.tools.anim.AnimUtil
import com.tkip.mycode.util.tools.anim.goneFadeOut
import com.tkip.mycode.util.tools.anim.showFadeIn
import com.tkip.mycode.util.tools.etc.OnMyClick


/**
 * PUT_POST -> 빈 post or 수정할 post
 * post는 항상 board.key와 name을 잃어버리지 말아야 한다.
 * todo: final post는 깊은 복사로 처리할 것.
 * //todo: functions에 update랑 delete는 따로 처리할 것.
 */
class AddPostActivity : BindingActivityPhoto<ActivityAddPostBinding>() {
    @LayoutRes
    override fun getLayoutResId() = R.layout.activity_add_post

    private val addType by lazy { intent.getIntExtra(PUT_ACTION, ACTION_NEW) }
    private val post by lazy { intent.getParcelableExtra<Post>(PUT_POST) }   // 널이면 새로운 post
    private lateinit var finalPost: Post

    private var isCsBoard = false


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        finalPost = Post(post)
        initView()
        initData(post)
    }


    //    var test = false
    private fun initView() {
        setSupportActionBar(b.toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        b.activity = this
        b.addType = addType
        b.post = finalPost
        b.inMyCredit.peer = mypeer()
        isCsBoard = finalPost.isCsBoard()
        b.isCsBoard = isCsBoard

        /*** 매뉴얼  */
        b.inManual.inter = interManualMore {
            b.inManual.tvManual1.maxLines = 100
            AnimUtil.goneNormal(b.inManual.btnMoreInfo)
        }


        /*** 고객센터 분기 */
        b.cbReply.isEnabled = !isCsBoard
        if (isCsBoard) b.inManual.manual = R.string.manual_top_add_post_csboard.getStr()
        else b.inManual.manual = R.string.manual_top_add_post.getStr()


        /*** 사진박스 */
        viewAddPhotoBox = LinearAddPhotoBox(this@AddPostActivity, onOpenAlbum = {

            //            if (test) {
//                startActivity(Intent(this@AddPostActivity, TTTestActivity::class.java))
//            } else {
            OnPermission.onOpenAlbum(true, this@AddPostActivity)
//                test = true
//            }
        }).apply { this@AddPostActivity.b.frameAddPhotoBox.addView(this) }

        /**         *  사진 보이기         */
        viewAddPhotoBox?.setPhotos(finalPost.fotoList)

        /*** 검색태그 */
        if (!isCsBoard) linearAddTag = LinearAddTag(this@AddPostActivity, TagTYPE.POST, R.string.name_tag_search.getStr(), R.string.btn_open_tag_dialog.getStr(), finalPost.tagList).apply { this@AddPostActivity.b.lineTag.addView(this) }

        /*** EditText 리스너  */
        b.etTitle.setOnFocusChangeListener { _, isOn -> if (!isOn) b.etTitle.isNullBlankAndSetError(b.layTitle, R.string.error_add_ad_unit_title) }
//        b.etReward.setOnFocusChangeListener { _, isOn ->
//            if (!isOn) {
//                val reward = b.etReward.text.toString().remove0String()
//                b.etReward.setText(reward)
//                if (b.etReward.isLtAmount(R.integer.point_minimum_reward_post.getInt(), b.layReward, R.string.error_reward_minimum)) return@setOnFocusChangeListener
//                if (b.etReward.isGtAmount(OnPeer.peer.point, b.layReward, R.string.error_not_enough_point)) return@setOnFocusChangeListener
//            }
//        }

        /********************* superListener *****************************/
        listenerPermissionsResult = interRequestPermissionsResult(
                onReadExternalStorage = { OnPermission.onOpenAlbum(it, this@AddPostActivity) })
    }


    /**     * 사용자가 기입한 post로 변경.     */
    private fun initData(post1: Post) {
        lllogI("BindingBaseActivity onParcel initData $post1 ")
        b.post = post1
    }

    /**     * 올릴 post 준비하기.     */
    private fun getFinalPost() =
            finalPost.apply {
                title = b.etTitle.getStr()
                comment = b.etContents.getStr()
                anonymous = b.cbAnonymous.isChecked
                allowReply = b.cbReply.isChecked
//                reward = b.etReward.getNullStr()?.toInt()

                /** 태그 삽입 */
                linearAddTag?.b?.tagView?.convertStringList()?.let { tagList = tagList.clearAddAll(it) }
            }


    override fun onRestoreInstanceState(save: Bundle) {
        super.onRestoreInstanceState(save)
        save.getParcelable<Post>(PUT_RESULT_OBJECT)?.let { post1 ->
            initData(post1)
        } ?: finish()

        /**         * 퍼미션 있을때에만 앨범에서 사진 가져오기.         */
//        val readPermission = ContextCompat.checkSelfPermission(this@AddPostActivity, READ_EXTERNAL_STORAGE)
//        if (OnPermission.hasPermission(this@AddPostActivity, READ_EXTERNAL_STORAGE)) {
//            save.getParcelableArrayList<Foto>(PUT_RESULT_LIST)?.let { list ->
//                viewAddPhotoBox?.selectedFotoList = list
//                viewAddPhotoBox?.selectedFotoAdapter?.notifyDataSetChanged()
//            }
//        }
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        lllogI("BindingBaseActivity onSaveInstanceState  ")

        /**         * todo 메모 : Q버전에서는 viewAddPhotoBox?.selectedFotoList를 받아올때 intent용량 초과로 error 발생.  대책: 재생성시에는 그냥 finish시켜버리자.       */
        if (!versionGteQ) {
            outState.putParcelable(PUT_RESULT_OBJECT, getFinalPost())
            outState.putParcelableArrayList(PUT_RESULT_LIST, viewAddPhotoBox?.selectedFotoList)
        }
    }

    override fun onBackPressed() {
        OnValid.removeFocusEt(b.etTitle,b.etContents)
//        if (OnValid.checkKeyboardHide(b.etTitle, b.etContents)) return

        if (b.etTitle.getStr().isNotBlank() || b.etContents.getStr().isNotBlank())
            MyAlert.showConfirm(this@AddPostActivity, getString(R.string.info_wanna_leave_add_common), ok = { super.onBackPressed() }, cancel = {})
        else super.onBackPressed()
    }


    fun onClickAdditional(v: View) {
        OnMyClick.setDisableAWhileBTN(v)
        AnimUtil.goneSlideDown(b.btnAdditional, false)
        AnimUtil.showSlideDown(b.lineAddtional, false)
    }


    fun onClickSubmit() {
        if (OnMyClick.isDoubleClick()) return

//        if ((b.etTitle.isNullBlankAndSetError(b.layTitle, R.string.info_no_title))
////                || b.etReward.isLtAmount(R.integer.point_minimum_reward_post.getInt(), b.layReward, R.string.error_reward_minimum)
////                || b.etReward.isGtAmount(OnPeer.peer.point, b.layReward, R.string.error_not_enough_point)
//                || (OnValid.hasError(b.etTitle, b.etContents  /*,b.etReward*/))
//        ) {
//            TToast.showAlert(R.string.error_edittext_null)
//            return
//        }

        if (viewAddPhotoBox?.selectedFotoList.isOverLimit()) return     // 사진 max 개수 체크
        if (getFinalPost().inValid()) return // 마지막 post 담기.

        MyAlert.showConfirm(this@AddPostActivity, getString(R.string.info_wanna_upload_add_post),
                ok = {
                    checkPayment()
                }, cancel = {})

    }

    private fun checkPayment() {
        PAY_ACT_TYPE_LIST.clear()

        when (addType) {
            ACTION_NEW -> {
//                PAY_ACT_TYPE_LIST.reset(ActType.ADD_POST)
                if (finalPost.anonymous) PAY_ACT_TYPE_LIST.add(ActType.ADD_ANONYMOUS_POST)
//                post.reward?.let { reward -> this.addCost(Cost.payment(null, ActType.ADD_REWARD_POST, post, null,reward,null)) }
            }
            ACTION_UPDATE -> {
                if (post.anonymous != finalPost.anonymous) PAY_ACT_TYPE_LIST.add(ActType.ADD_ANONYMOUS_POST)
            }
        }

        /**         * show payDialog         */
        checkAndShowPay(this@AddPostActivity, ok = { finalAdd() }, cancel = {})

    }


    private fun finalAdd() {
//        if (OnPay.noCreditSwitch(b.cbAnonymous, ActType.ADD_ANONYMOUS_POST.price!!)) return@showConfirm
        OnPost.addCheckUpload(addType, this@AddPostActivity, null, viewAddPhotoBox?.selectedFotoList!!, post, finalPost, success = { }, fail = { MyCircle.cancel() })
    }


}
