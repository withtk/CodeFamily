package com.tkip.mycode.nav.settings

import android.content.Context
import android.content.Intent
import android.view.View
import com.tkip.mycode.nav.settings.admin.SettingsAdminActivity
import com.tkip.mycode.util.lib.TransitionHelper

class OnSettings {

    companion object {
        fun gotoSettingsActivity(context: Context, arrPair: Array<androidx.core.util.Pair<View, String>>?) {
            Intent(context, SettingsActivity::class.java).apply {
                addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
                TransitionHelper.startTran(context, this, arrPair)
            }
        }
        fun gotoSettingsAdminActivity(context: Context, arrPair: Array<androidx.core.util.Pair<View, String>>?) {
            Intent(context, SettingsAdminActivity::class.java).apply {
                addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
                TransitionHelper.startTran(context, this, arrPair)
            }
        }
    }
}