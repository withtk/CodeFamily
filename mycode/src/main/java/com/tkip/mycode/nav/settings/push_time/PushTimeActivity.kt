package com.tkip.mycode.nav.settings.push_time

import android.os.Bundle
import androidx.annotation.LayoutRes
import com.tkip.mycode.R
import com.tkip.mycode.admin.versionGteOreo
import com.tkip.mycode.databinding.ActivityPushTimeBinding
import com.tkip.mycode.funs.custom.menu.ViewMenuLineCheck
import com.tkip.mycode.funs.custom.toolbar.ViewToolbarEmpty
import com.tkip.mycode.init.base.Sred
import com.tkip.mycode.init.base.getStr
import com.tkip.mycode.init.my_bind.BindingActivity
import com.tkip.mycode.init.sredBoolean
import com.tkip.mycode.init.sredInt
import com.tkip.mycode.init.sredPutBoolean
import com.tkip.mycode.init.sredPutInt
import com.tkip.mycode.util.tools.anim.gone
import com.tkip.mycode.util.tools.anim.hideSlideDown
import com.tkip.mycode.util.tools.anim.showSlideUp
import com.tkip.mycode.util.tools.anim.visi


/**
 */
class PushTimeActivity : BindingActivity<ActivityPushTimeBinding>() {
    @LayoutRes
    override fun getLayoutResId() = R.layout.activity_push_time

    private lateinit var viewToolbar: ViewToolbarEmpty

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        initView()
    }

    private fun initView() {
        b.activity = this

        /** Toolbar */
        viewToolbar = ViewToolbarEmpty(this@PushTimeActivity,
                R.string.menu_title_push_time.getStr(),
                onBack = { onBackPressed() },
                onMore = null
        ).apply { this@PushTimeActivity.b.frameToolbar.addView(this) }


        /** 메뉴 linear 넣기 : 방해금지 */
        val checked = Sred.PUSH_TIME_SET.sredBoolean(false)
        b.lineCover.addView(ViewMenuLineCheck(this@PushTimeActivity, R.string.menu_title_push_time.getStr(), R.string.menu_desc_push_time.getStr(), checked,
                onClickSW = {
                    Sred.PUSH_TIME_SET.sredPutBoolean(it)
                    showHidePicker(it)
                }, onClickCB = null), 0)

        /** Picker 세팅 */
        showHidePicker(checked)

        b.picker1.minValue = 0
        b.picker1.maxValue = 23
        b.picker1.value = Sred.PUSH_TIME1.sredInt(21)

        b.picker2.minValue = 0
        b.picker2.maxValue = 23
        b.picker2.value = Sred.PUSH_TIME2.sredInt(7)

        /** Radio 세팅 */
        if (versionGteOreo) b.rg.visi() else b.rg.gone()
        b.rg.setOnCheckedChangeListener { _, checkedId ->
            Sred.PUSH_IS_MUTE.sredPutBoolean((checkedId == b.rbMute.id))    // 알림 소리 여부 설정.
        }
        // 초기 라디오버튼 셋팅.
        if (Sred.PUSH_IS_MUTE.sredBoolean(true)) b.rbMute.isChecked = true else b.rbDont.isChecked = true


    }

    override fun onStop() {
        super.onStop()
        setPushTime()
    }

    private fun showHidePicker(checked: Boolean) {
        if (checked) b.lineBottom.showSlideUp(false)
        else b.lineBottom.hideSlideDown(false)
    }

    /*** sred에 PushTime 넣기 */
    private fun setPushTime() {
        val time1 = b.picker1.value
        val time2 = b.picker2.value
        Sred.PUSH_TIME1.sredPutInt(time1)
        Sred.PUSH_TIME2.sredPutInt(time2)
    }


}