package com.tkip.mycode.nav.store.tab_menu

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.RecyclerView
import com.chauthai.swipereveallayout.ViewBinderHelper
import com.firebase.ui.firestore.FirestoreRecyclerAdapter
import com.firebase.ui.firestore.FirestoreRecyclerOptions
import com.google.firebase.firestore.FirebaseFirestoreException
import com.tkip.mycode.R
import com.tkip.mycode.databinding.HolderStoreMenuModifyBinding
import com.tkip.mycode.databinding.HolderStoreMenuNormalBinding
import com.tkip.mycode.dialog.alert.MyAlert
import com.tkip.mycode.init.base.getStr
import com.tkip.mycode.model.store.Store
import com.tkip.mycode.model.store.StoreMenu
import com.tkip.mycode.funs.common.OnException
import com.tkip.mycode.funs.common.OnVisible


/**
 * actionType - 일반유저  or  수정하는 유저
 */
class StoreMenuAdapterFireS(options: FirestoreRecyclerOptions<StoreMenu>, val store: Store, val actionType: Int) : FirestoreRecyclerAdapter<StoreMenu, RecyclerView.ViewHolder>(options) {
    private lateinit var activity: AppCompatActivity
    private val binderHelper = ViewBinderHelper()

    private val VIEW_NORMAL = 100
    private val VIEW_MODIFY = 200

    override fun getItemId(position: Int): Long {
        return getItem(position).hashCode().toLong()
    }

    /** 현재 modify만 적용중. */
//    override fun getItemViewType(position: Int): Int {
//        return when (actionType) {
//            Cons.ACTION_TYPE_NORMAL -> VIEW_MODIFY
//            else -> VIEW_MODIFY
//        }
//    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        this.activity = parent.context as AppCompatActivity
        return  StoreMenuModifyViewHolder(activity, this, binderHelper, actionType, store,null, HolderStoreMenuModifyBinding.inflate(LayoutInflater.from(parent.context), parent, false))

//        return when (viewType) {
//            VIEW_NORMAL -> StoreMenuNormalViewHolder(HolderStoreMenuNormalBinding.inflate(LayoutInflater.from(parent.context), parent, false))
//            VIEW_MODIFY -> StoreMenuModifyViewHolder(activity, this, binderHelper, actionType, store, HolderStoreMenuModifyBinding.inflate(LayoutInflater.from(parent.context), parent, false))
//            else -> StoreMenuNormalViewHolder(HolderStoreMenuNormalBinding.inflate(LayoutInflater.from(parent.context), parent, false))
//        }
    }


    override fun onBindViewHolder(h: RecyclerView.ViewHolder, p: Int, m: StoreMenu) {
        when (h) {
            is StoreMenuNormalViewHolder -> h.bind(m)
            is StoreMenuModifyViewHolder -> h.bind(m)
            else -> OnException.forceException("onBindViewHolder in StoreMenuAdapterFireS  ")
        }
    }

    override fun onDataChanged() {
        super.onDataChanged()
    }

    override fun onError(e: FirebaseFirestoreException) {
        super.onError(e)
    }
    /**
     *  수정할 때
     */
//    inner class StoreMenuModifyViewHolder(val b: HolderStoreMenuModifyBinding) : RecyclerView.ViewHolder(b.root) {
//
//        private lateinit var storeMenu: StoreMenu
//
//        fun bind(storeMenu: StoreMenu) {
//            this.storeMenu = storeMenu
//            b.holder = this
//            b.actionType = actionType
//            b.store = store
//            b.storeMenu = storeMenu
//            b.executePendingBindings()
//
//
//            val spinnerStatusItems = OnSpinner.getStatusList()
//            b.spinnerStatus.adapter = SpinnerStatusAdapter(activity, spinnerStatusItems)
//            var check = 0
//            b.spinnerStatus.setListener(
//                    onItemSelected = {
//                        if (++check > 1 && OnPeer.isAdmin(false)) {
//                            OnSpinner.updateStoreMenuStatusDialog(activity, spinnerStatusItems[it], storeMenu, {}, deny = { notifyDataSetChanged() })
//                        }
//                    }, onNothingSelected = {})
//            b.spinnerStatus.setSelection(storeMenu.statusNo.getPosition(spinnerStatusItems))
//
//            OnSpinner.setWorking(b.spinnerStatus)
//
//
//            binderHelper.bind(b.swiper, storeMenu.key)
//            binderHelper.setOpenOnlyOne(true)
//
//            // swipe 작동 여부.
////            if (OnPeer.isNotAdmin(false)) {
////                if (storeMenu.statusNo.lessThan(Status.INSPECTING)) b.swiper.setLockDrag(true)
////            }
//
//            OnVisible.setSwipeHeight(b.line, b.lineBottom)   // height 맞추기
//        }
//
//        fun onClickCard() {
//            if (OnPeer.isAdmin(false) || OnPeer.isSamePeer(store.hostUID) || actionType == Cons.ACTION_TYPE_NEW || actionType == Cons.ACTION_TYPE_UPDATE || actionType == Cons.ACTION_TYPE_INSPECTION) {
//                if (b.swiper.isClosed) b.swiper.open(true)
//                else b.swiper.close(true)
//            }
//        }
//
////        fun onLongClick(): Boolean {
////            /** 딥삭제 */
////            return if (OnPeer.isAdminSupervisor(false)) {
////                ConfirmDialog.newInstance(title = null, desc = R.string.desc_deep_delete_basic.getStr(), isCancel = true, isCancelTouch = true,
////                        ok = { Firee.deleteStoreMenu(storeMenu, {}, {}) }, deny = {})
////                        .show(activity.supportFragmentManager, "dialog")
////                true
////            } else false
////        }
//
//        fun onClickDel() {
//            if (OnMyClick.isDoubleClick()) return
//            ConfirmDialog.newInstance(title = null, desc = Status.DELETED.dialogMsg, isCancel = true, isCancelTouch = true,
//                    ok = { Firee.deleteStoreMenu(storeMenu, {}, {}) }, deny = { })
//                    .show(activity.supportFragmentManager, "dialog")
//        }
//
//        fun onClickGo() {
//            if (OnMyClick.isDoubleClick()) return
//            // todo : 리플라이 볼 수 있는 페이지로 이동. 다이얼로그로 or 액티비티
//        }
//
//    }



    /**
     *  Normal로 보기만 할 때
     */
    inner class StoreMenuNormalViewHolder(val b: HolderStoreMenuNormalBinding) : RecyclerView.ViewHolder(b.root) {

        private lateinit var storeMenu: StoreMenu

        fun bind(storeMenu: StoreMenu) {
            this.storeMenu = storeMenu
            b.holder = this
            b.actionType = actionType
            b.store = store
            b.storeMenu = storeMenu

            b.executePendingBindings()

            binderHelper.bind(b.swiper, storeMenu.key)
            binderHelper.setOpenOnlyOne(true)

            OnVisible.setSwipeHeight(b.line, b.lineBottom)
        }

        fun onClickCard() {
            if (b.swiper.isClosed) b.swiper.open(true)
            else b.swiper.close(true)
        }

        fun onClickDel() {
            MyAlert.showConfirm(activity, R.string.desc_delete_basic.getStr(), {
//                        Firee.updateStatusStoreMenu(storeMenu, Status.DELETED, {}, {})
            }, {})

        }

        fun onClickGo() {
            // todo : 리플라이 볼 수 있는 페이지로 이동. 다이얼로그로 or 액티비티
        }
    }


}