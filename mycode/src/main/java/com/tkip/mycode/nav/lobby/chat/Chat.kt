package com.tkip.mycode.nav.lobby.chat

import android.os.Parcel
import android.os.Parcelable
import com.tkip.mycode.init.base.createKey
import com.tkip.mycode.model.my_enum.ReplyType
import com.tkip.mycode.model.my_enum.Status
import com.tkip.mycode.model.peer.mynick
import com.tkip.mycode.model.peer.myuid
import com.tkip.mycode.model.reply.getOtherNick
import com.tkip.mycode.model.reply.getOtherUid
import com.tkip.mycode.nav.lobby.Lobby
import com.tkip.mycode.util.tools.date.rightNow

/**
 * Lobby > Chat
 */
data class Chat(

        var key: String = createKey(),
        var authorUID: String = myuid(),
        var authorNick: String = mynick(),

        /////////////////CHAT//////////////////
        var lobbyKey: String = "",
        var targetUID: String = "",
        var targetNick: String? = null,

        var replyTypeNo: Int = ReplyType.TEXT.no,  // 사진,TEXT,TOSS
        var comment: String? = null,               // 수정불가. 원래 한번 뱉은 말은 수정이 안 된다. 삭제는 가능.

        /////////////////PHOTO//////////////////
        var thumbUrl: String? = null,
        var url: String? = null,

        /////////////////TOSS//////////////////
        var tossModelKey: String? = null,
        var tossDataTypeNo: Int? = null,
        //////////////////////////////////////

        var thumbUpCount: Int? = null,     // 썸업을 준 횟수.
        var thumbDownCount: Int? = null,    // 썸다운을 횟수.
        var warnCount: Int? = null,       // 신고 횟수.
        var totalPoint: Long? = null,   // OnPeer.peer.activityPoint : author의 레벨 + thumbUp - thumbDown

        /////////////////rereply//////////////////
        var earlierKey: String? = null,  // 재리플의 경우만 해당. reply.key

        var notRead: Int = 1,        // 봤는지여부 체크하는 int.

        var statusNo: Int = Status.NORMAL.no,
        var addDate: Long = rightNow()

) : Parcelable {
    constructor() : this(createKey())   // adapter에서 사용.

    constructor(lobby: Lobby, replyType: ReplyType, comment: String?) : this(
            lobbyKey = lobby.key,
            targetUID = lobby.getOtherUid(),
            targetNick = lobby.getOtherNick(),
            replyTypeNo = replyType.no,
            comment = comment
    )

    constructor(source: Parcel) : this(
            source.readString()!!,
            source.readString()!!,
            source.readString()!!,
            source.readString()!!,
            source.readString()!!,
            source.readString(),
            source.readInt(),
            source.readString(),
            source.readString(),
            source.readString(),
            source.readString(),
            source.readValue(Int::class.java.classLoader) as Int?,
            source.readValue(Int::class.java.classLoader) as Int?,
            source.readValue(Int::class.java.classLoader) as Int?,
            source.readValue(Int::class.java.classLoader) as Int?,
            source.readValue(Long::class.java.classLoader) as Long?,
            source.readString(),
            source.readInt(),
            source.readInt(),
            source.readLong()
    )

    override fun describeContents() = 0

    override fun writeToParcel(dest: Parcel, flags: Int) = with(dest) {
        writeString(key)
        writeString(authorUID)
        writeString(authorNick)
        writeString(lobbyKey)
        writeString(targetUID)
        writeString(targetNick)
        writeInt(replyTypeNo)
        writeString(comment)
        writeString(thumbUrl)
        writeString(url)
        writeString(tossModelKey)
        writeValue(tossDataTypeNo)
        writeValue(thumbUpCount)
        writeValue(thumbDownCount)
        writeValue(warnCount)
        writeValue(totalPoint)
        writeString(earlierKey)
        writeInt(notRead)
        writeInt(statusNo)
        writeLong(addDate)
    }

    companion object {
        @JvmField
        val CREATOR: Parcelable.Creator<Chat> = object : Parcelable.Creator<Chat> {
            override fun createFromParcel(source: Parcel): Chat = Chat(source)
            override fun newArray(size: Int): Array<Chat?> = arrayOfNulls(size)
        }
    }
}