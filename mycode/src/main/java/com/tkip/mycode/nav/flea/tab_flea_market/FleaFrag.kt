package com.tkip.mycode.nav.flea.tab_flea_market

import android.content.Context
import android.os.Bundle
import androidx.lifecycle.Observer
import com.tkip.mycode.R
import com.tkip.mycode.ad.AdType
import com.tkip.mycode.ad.ad_show.CustomBannerSlide
import com.tkip.mycode.databinding.FragmentFleaBinding
import com.tkip.mycode.funs.common.lllogM
import com.tkip.mycode.init.FLEA_BOARD_KEY
import com.tkip.mycode.init.RV_GRID_VERT
import com.tkip.mycode.init.base.getInt
import com.tkip.mycode.init.base.getStr
import com.tkip.mycode.init.base.getViewModel
import com.tkip.mycode.init.my_bind.BindingFragment
import com.tkip.mycode.model.manage.OnTopNotice
import com.tkip.mycode.model.my_enum.Status
import com.tkip.mycode.model.viewmodel.FleaViewModel
import com.tkip.mycode.nav.flea.FleaAdapter
import com.tkip.mycode.util.tools.anim.gone
import com.tkip.mycode.util.tools.anim.visi
import com.tkip.mycode.util.tools.recycler.OnRv


class FleaFrag : BindingFragment<FragmentFleaBinding>() {
    override fun getLayoutResId(): Int = R.layout.fragment_flea
    private lateinit var vmFlea: FleaViewModel

//    private val vmFlea by lazy { getViewModel<FleaViewModel>() }
    private lateinit var adapter: FleaAdapter
    private val esSize by lazy { R.integer.es_size_flea.getInt() }

    private var esLastIndex = 0

    companion object {
        @JvmStatic
        fun newInstance() = FleaFrag()
    }

    override fun onResume() {
        super.onResume()
        banner?.startSlide("FleaFrag")    // 배너 슬라이드 시작
    }

    override fun onPause() {
        super.onPause()
        banner?.stopSlide()    // 배너 슬라이드 멈춤
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        lllogM("FleaFrag $this onAttach   context :$context")
        mContext = context
        vmFlea = getViewModel()
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        initView()
        initData()
    }

//    override fun onResume() {
//        super.onResume()
//        if (vmFlea.liveFleaList.value?.size == 0) {
//        }
//    }

    private fun initView() {
        b.fragment = this

        /** * topNotice 세팅 */
        OnTopNotice.setupBasicBoardDesc(FLEA_BOARD_KEY, b.inTopNotice)

        /** * noData 세팅 */
        b.inData.tvNoData.text = R.string.info_no_flea.getStr()

        /** * adapter 세팅 */
        var init = true
        vmFlea.liveFleaList.observe(this, Observer {
            if (init) {
                adapter = FleaAdapter(it, RV_GRID_VERT, null)
                b.rv.adapter = adapter
                init = false
            } else {
                esLastIndex = OnRv.setRvInFrag(it, b.inData, b.refreshLayout, b.tvLast, esLastIndex)
                adapter.notifyDataSetChanged()
            }
        })


        b.refreshLayout.setOnRefreshListener { getEsDataList() }
        b.refreshLayout.setOnLoadMoreListener { vmFlea.getEsFleaList(esSize, esLastIndex, Status.DELETED_USER) }
        b.refreshLayout.setEnableLoadMore(false)  // 최초에는 loadMore 불능.

    }

    private fun initData() {

        b.inData.avLoading.visi()
        getEsDataList()

        /**   광고 top */
        banner = CustomBannerSlide(mContext, AdType.TOP_FLEA, null)
        b.frameAd.addView(banner)

    }

    private fun getEsDataList() {
        esLastIndex = 0   // 꼭 해야 됨. esLastIndex로 loadMOre을 변경할 것이기 때문에.
        vmFlea.getEsFleaList(esSize, esLastIndex, Status.DELETED_USER)
        b.inData.tvRefresh.gone()
    }

}
