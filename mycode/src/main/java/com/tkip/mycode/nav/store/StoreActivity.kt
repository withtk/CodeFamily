package com.tkip.mycode.nav.store

import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import androidx.annotation.LayoutRes
import com.tkip.mycode.R
import com.tkip.mycode.admin.menu.AMenu
import com.tkip.mycode.admin.menu.OnMenu
import com.tkip.mycode.databinding.ActivityStoreBinding
import com.tkip.mycode.dialog.alert.MyAlert
import com.tkip.mycode.funs.common.errorWhen
import com.tkip.mycode.funs.common.lllogD
import com.tkip.mycode.init.*
import com.tkip.mycode.init.my_bind.BindingActivityPhoto
import com.tkip.mycode.model.cost.Cost
import com.tkip.mycode.model.my_enum.ActType
import com.tkip.mycode.model.my_enum.Status
import com.tkip.mycode.model.my_enum.deepDel
import com.tkip.mycode.model.my_enum.showStatusDialog
import com.tkip.mycode.model.store.Store
import com.tkip.mycode.model.toss.OnToss
import com.tkip.mycode.model.warn.OnWarn
import com.tkip.mycode.nav.store.add.StoreFotoPager
import com.tkip.mycode.nav.store.store_menu.OnStoreMenu
import com.tkip.mycode.util.tools.fire.commonStatusUpdate
import com.tkip.mycode.util.tools.fire.real

class StoreActivity : BindingActivityPhoto<ActivityStoreBinding>() {
    @LayoutRes
    override fun getLayoutResId() = R.layout.activity_store

    private val actionType by lazy { intent.getIntExtra(PUT_ACTION, ACTION_PREVIEW) }
    //    private val vmStore by lazy { getViewModel<StoreViewModel>() }
    val store by lazy { intent.getParcelableExtra<Store>(PUT_STORE) ?: Store() }

    override fun onCreate(savedInstanceState: Bundle?) {
        lllogD("mContextTest StoreActivity onCreate")

        super.onCreate(savedInstanceState)

        initView()
        setupViewPager()

//        MyCircle.cancel()
    }

    private fun initView() {
        Cost.new(null,ActType.OPEN_STORE, store).real()

        b.activity = this
        b.actionType = actionType
        b.store = store

        setSupportActionBar(b.toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.title = store.title  // 스토어 이름

        /**   Top FOTO in Store  */
        store.fotoList?.let {
            b.viewPagerTop.adapter = StoreFotoPager(it)
            b.viewPager.offscreenPageLimit = it.size // tab이 리셋되지 않도록.
        }

//        /**   * CH_VIEW_UID_COUNT 추가   */
//        vmStore.addViewUID(store, Cntch.VIEW_UID)

        /********************* superListener *****************************/
//        listenerActivityResult = interActivityResult(onPick = null, onAlbum = null, onMap = null, onBanner = null,
//                onData = {
//                    lllogI("Storeactivity listenerActivityResult $it")
//                    it?.getStringExtra(PUT_BOARD_KEY)?.let {boardKey->
//                        store.boardKey = boardKey
//                        Firee.addStore(store, success = {
//                            OnStore.gotoStoreActivity(this@StoreActivity,true,null,store,null)
//                            finish()
//                        }, fail = {})
//                    }
//                })

    }

    private fun setupViewPager() {
        val pairList = OnStore.getPairList(store, actionType)
        b.viewPager.adapter = StorePagerAdapter(supportFragmentManager, pairList)
        b.viewPager.offscreenPageLimit = pairList.size - 1   // tab이 리셋되지 않도록.
//        b.viewPager.onPageChange({}, { _, _, _ -> }, onSelected = { p -> })
        b.tabLayout.setupWithViewPager(b.viewPager)
    }


    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.menu_more, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            android.R.id.home -> onBackPressed()
            R.id.more -> onClickMore()
        }
        return super.onOptionsItemSelected(item)
    }

    fun onClickMore() {
        MyAlert.showMenuList(this@StoreActivity, OnMenu.getMENULIST(store),
                onSelect = { amenu ->
                    when (amenu) {
                        AMenu.STORE_MENU -> OnStoreMenu.gotoMenuManageActivity(this@StoreActivity, store, null)
                        AMenu.UPDATE -> OnStore.gotoAddStoreActivity(this@StoreActivity, ACTION_UPDATE, store, null)
                        AMenu.DELETION -> MyAlert.showDelete(this@StoreActivity, ok = { commonStatusUpdate(EES_STORE, store.key, Status.DELETED_USER, success = { finish() }, fail = {}) }, cancel = {})
                        AMenu.TOSS -> OnToss.checkAdd(this@StoreActivity, store)
                        AMenu.WARN -> OnWarn.checkAndAdd(this@StoreActivity, store, success = {}, fail = {})
                        AMenu.DEEP_DEL -> store.deepDel(this@StoreActivity, onSuccess = {})
                        AMenu.STATUS -> store.showStatusDialog(this@StoreActivity,null)
                        else -> errorWhen()
                    }
                })
    }


}
