package com.tkip.mycode.nav.settings.admin

import android.os.Bundle
import androidx.annotation.LayoutRes
import com.tkip.mycode.R
import com.tkip.mycode.admin.menu.AMenu
import com.tkip.mycode.auth.OnSign
import com.tkip.mycode.databinding.ActivitySettingsAdminBinding
import com.tkip.mycode.dialog.toast.toastNormal
import com.tkip.mycode.funs.custom.menu.ViewMenuBox
import com.tkip.mycode.funs.custom.menu.ViewMenuNormal
import com.tkip.mycode.funs.custom.toolbar.ViewToolbarEmpty
import com.tkip.mycode.init.base.getStr
import com.tkip.mycode.init.base.lt
import com.tkip.mycode.init.my_bind.BindingActivity
import com.tkip.mycode.model.my_enum.PeerType
import com.tkip.mycode.model.peer.OnPeer

/**
 * 어플 관리 for Admin
 */
class SettingsAdminActivity : BindingActivity<ActivitySettingsAdminBinding>() {
    @LayoutRes
    override fun getLayoutResId() = R.layout.activity_settings_admin

    private lateinit var viewToolbar: ViewToolbarEmpty

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        initView()
    }

    private fun initView() {
        b.activity = this

        /** Toolbar */
        viewToolbar = ViewToolbarEmpty(this@SettingsAdminActivity,
                R.string.title_admin_settings.getStr(),
                onBack = { onBackPressed() },
                onMore = null
        ).apply { this@SettingsAdminActivity.b.frameToolbar.addView(this) }

        /*** 메뉴 붙이기  */
        setupBelowMenu()
    }


    private fun setupBelowMenu() {

        b.lineBelow.removeAllViews()
        if (OnPeer.peer.type.lt(PeerType.BASIC_2.no)) {   // 익명로그인 일때
            b.lineBelow.addView(ViewMenuNormal(this@SettingsAdminActivity, null, AMenu.SIGN_OUT_CHANGE, onClick = { _, _ -> OnSign.askSignOut(this@SettingsAdminActivity) }))
        } else {

            b.lineBelow.addView(ViewMenuBox(this@SettingsAdminActivity, R.string.menu_group_title_model.getStr())
                    .apply {
                        this.b.line.addView(ViewMenuNormal(this@SettingsAdminActivity, null, AMenu.PEER_LIST, null))
                        this.b.line.addView(ViewMenuNormal(this@SettingsAdminActivity, null, AMenu.MODEL_LIST, null))
                        this.b.line.addView(ViewMenuNormal(this@SettingsAdminActivity, null, AMenu.TAG_LIST, null))
                        this.b.line.addView(ViewMenuNormal(this@SettingsAdminActivity, null, AMenu.BILL_LIST, null))
                        this.b.line.addView(ViewMenuNormal(this@SettingsAdminActivity, null, AMenu.HIT_ACT, null))
                    })
            b.lineBelow.addView(ViewMenuBox(this@SettingsAdminActivity, R.string.menu_group_title_issue.getStr())
                    .apply {
                        this.b.line.addView(ViewMenuNormal(this@SettingsAdminActivity, null, AMenu.GRANT_LIST, null))
                        this.b.line.addView(ViewMenuNormal(this@SettingsAdminActivity, null, AMenu.BANNER_LIST, null))
                        this.b.line.addView(ViewMenuNormal(this@SettingsAdminActivity, null, AMenu.WARN_LIST, null))
                        this.b.line.addView(ViewMenuNormal(this@SettingsAdminActivity, null, AMenu.PENALTY_LIST, null))
                    })
            b.lineBelow.addView(ViewMenuBox(this@SettingsAdminActivity, R.string.menu_group_title_manage.getStr())
                    .apply {
                        this.b.line.addView(ViewMenuNormal(this@SettingsAdminActivity, null, AMenu.NOTICE, null))
                    })
        }
    }

    private fun developing() {
        toastNormal(R.string.info_prepare_developing)
    }

}