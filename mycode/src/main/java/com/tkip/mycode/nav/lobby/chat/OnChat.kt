package com.tkip.mycode.nav.lobby.chat

import android.content.Context
import com.firebase.ui.firestore.FirestoreRecyclerOptions
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.firestore.Query
import com.google.firebase.firestore.QuerySnapshot
import com.tkip.mycode.R
import com.tkip.mycode.admin.menu.AMenu
import com.tkip.mycode.admin.menu.ARR_CHAT_MY
import com.tkip.mycode.admin.menu.ARR_CHAT_USER
import com.tkip.mycode.databinding.LayoutRvReplyBinding
import com.tkip.mycode.dialog.OnDDD
import com.tkip.mycode.dialog.alert.MyAlert
import com.tkip.mycode.dialog.progress.MyCircle
import com.tkip.mycode.funs.common.OnFunction
import com.tkip.mycode.funs.common.lllogD
import com.tkip.mycode.funs.common.lllogI
import com.tkip.mycode.init.*
import com.tkip.mycode.init.base.getStr
import com.tkip.mycode.model.my_enum.ReplyType
import com.tkip.mycode.model.my_enum.Status
import com.tkip.mycode.model.peer.isMe
import com.tkip.mycode.model.reply.LinearReplyAddBox
import com.tkip.mycode.model.reply.ViewReplyEarlierWrite
import com.tkip.mycode.util.lib.photo.util.PhotoUtil
import com.tkip.mycode.util.tools.fire.Firee
import com.tkip.mycode.util.tools.fire.commonStatusUpdate


class OnChat {

    companion object {
        /**
         *
         */
        fun setAdapter(context: Context, child: String, key: String, inRvReply: LayoutRvReplyBinding, linearReplyAddBox: LinearReplyAddBox, onCompleteAdoption: () -> Unit): ChatAdapterFireS {
            val options = getOptions(child, key, null, null)

            val adapter = ChatAdapterFireS(options,
                    onCreateReReply = { chat1, _ ->
                        ViewReplyEarlierWrite.add(context, linearReplyAddBox.b.lineAttachBox, null, chat1, null, null)
                        REPLY_ATTACH_TYPE = ATTACH_REPLY
                        REPLY_ATTACH_REPLY_KEY = chat1.key
                    })

            inRvReply.rv.adapter = adapter    //set adapter in RV
            inRvReply.inNoData.tvNoData.text = R.string.info_no_data_reply.getStr()     // "댓글 없습니다"

            adapter.startListening()   // stopListening : in SuperClass

            /**         * 리프레쉬 : 가끔 사진이 안 보일때가 있다.         */
            android.os.Handler().postDelayed({
                adapter.notifyDataSetChanged()
            }, 1000)

            return adapter
        }


        /****************************************************************
         * add   *******************************************************
         *****************************************************************/
        fun addPhotoAndChat(context: Context, chat: Chat, success: () -> Unit, fail: () -> Unit) {
            PhotoUtil.uploadUriSingle(context, chat,
                    successUploading = { foto ->
                        foto?.let {
                            chat.thumbUrl = it.thumbUrl
                            chat.url = it.url
                            chat.replyTypeNo = ReplyType.PHOTO.no
                        }
//                        MyProgress.cancel()
                        MyCircle.cancel()
                        Firee.addChat(chat, success = { success() }, fail = { fail() })
                    },
                    fail = { fail() })
        }


        /****************************************************************
         * LongClick Menu
         *****************************************************************/
        fun showMenuDialog(context: Context, chat: Chat, onCreateReReply: () -> Unit, onDeleted: (Status) -> Unit) {
            val menuList = if (chat.authorUID.isMe()) ARR_CHAT_MY else ARR_CHAT_USER
            MyAlert.showMenuList(context, menuList,
                    onSelect = { amenu ->
                        when (amenu) {
                            AMenu.COPY -> OnFunction.setClipBoardLink(chat.comment.toString())
                            AMenu.DELETION -> OnDDD.delete(context, ok = { commonStatusUpdate(EES_LOBBY, chat.lobbyKey, EES_REPLY, chat.key, Status.DELETED_USER, { onDeleted(Status.DELETED_USER) }, {}) }, cancel = {})
                            AMenu.REREPLY -> onCreateReReply()
                            else -> {
                            }
                        }
                    })
        }


        /****************************************************************
         * Reply Listen : 실시간을 위해 post>reply 안에 넣음.
         *****************************************************************/
        fun getOptions(child: String, key: String, onSuccess: ((QuerySnapshot) -> Unit)?, onFail: (() -> Unit)?): FirestoreRecyclerOptions<Chat> {
            val query = FirebaseFirestore.getInstance().collection(child).document(key).collection(EES_CHAT)
                    .orderBy(CH_ADD_DATE, Query.Direction.ASCENDING)

            onSuccess?.let {
                query.get()
                        .addOnSuccessListener {
                            lllogI("addOnSuccessListener size : ${it.size()}")
                            onSuccess.invoke(it)
                        }
                        .addOnFailureListener {
                            lllogD("addOnFailureListener $it")
                            onFail?.invoke()
                        }
            }

            val options = FirestoreRecyclerOptions.Builder<Chat>()
                    .setQuery(query, Chat::class.java)
                    .build()
            return options
        }


    }

}