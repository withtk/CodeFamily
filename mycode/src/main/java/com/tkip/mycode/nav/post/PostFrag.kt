package com.tkip.mycode.nav.post

import android.content.Context
import android.os.Bundle
import androidx.lifecycle.Observer
import com.tkip.mycode.R
import com.tkip.mycode.ad.AdType
import com.tkip.mycode.ad.ad_show.CustomBannerSlide
import com.tkip.mycode.databinding.FragmentPostBinding
import com.tkip.mycode.init.PUT_BOARD
import com.tkip.mycode.init.RECENT_BOARD_KEY
import com.tkip.mycode.init.RV_VERT
import com.tkip.mycode.init.base.cutName
import com.tkip.mycode.init.base.getInt
import com.tkip.mycode.init.base.getStr
import com.tkip.mycode.init.base.getViewModel
import com.tkip.mycode.init.my_bind.BindingFragment
import com.tkip.mycode.init.recentBoard
import com.tkip.mycode.model.board.Board
import com.tkip.mycode.model.manage.OnTopNotice
import com.tkip.mycode.model.my_enum.Status
import com.tkip.mycode.model.viewmodel.PostViewModel
import com.tkip.mycode.nav.board.MainBoardFrag
import com.tkip.mycode.util.tools.anim.gone
import com.tkip.mycode.util.tools.anim.visi
import com.tkip.mycode.util.tools.recycler.OnRv


/**
 * 1. 최근베스트 포스트 : 최신 + totalPoint 순으로
 * 2. 일반 보드
 */
class PostFrag : BindingFragment<FragmentPostBinding>() {
    override fun getLayoutResId(): Int = R.layout.fragment_post

    private lateinit var vmPost: PostViewModel

    private val board by lazy { arguments?.getParcelable<Board>(PUT_BOARD) ?: recentBoard!! }

    //    private val boardKey by lazy { arguments?.getString(PUT_BOARD_KEY) ?: RECENT_BOARD_KEY }
    //    private val vmPost by lazy { getViewModel<PostViewModel>() }
    private lateinit var adapter: PostAdapter
//    private lateinit var board: Board

    private val esSize by lazy { R.integer.es_size_post.getInt() }
    private var esLastIndex = 0

    companion object {
        @JvmStatic
        fun newInstance(board: Board?) = PostFrag().apply { arguments = Bundle().apply { putParcelable(PUT_BOARD, board) } }
//        fun newInstance(boardKey: String?) = PostFrag().apply { arguments = Bundle().apply { putString(PUT_BOARD_KEY, boardKey) } }
    }

    override fun onResume() {
        super.onResume()
        banner?.startSlide("PostFrag")    // 배너 슬라이드 시작
    }

    override fun onPause() {
        super.onPause()
        banner?.stopSlide()    // 배너 슬라이드 멈춤
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        mContext = context
        vmPost = getViewModel()
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
//        setHasOptionsMenu(true)    // 메뉴 만들기.

        setTabTitle()

        initView()
        initData()

    }

    /**     * MainBoardFrag.tab 타이틀 변경     */
    private fun setTabTitle() {
        /*** parent 있을때 && 2번째 보드일때만. */
        parentFragment?.let { parent ->
            val tabIndex = if (board.key == RECENT_BOARD_KEY) 0 else 1
            (parent as MainBoardFrag).b.tabLayout.getTabAt(tabIndex)?.let {
                it.text = board.title.cutName()
            }
        }
    }


    private fun initView() {

        b.fragment = this

//        fragmentManager.
        /** * 보드 공지사항 세팅 */
        OnTopNotice.setupPostDesc(mContext, board, b.inTopNotice)

        /** * noData 세팅 */
        b.inData.tvNoData.text = R.string.info_no_post.getStr()


        /** * adapter 세팅 */
        var init = true
        vmPost.livePostList.observe(this, Observer {
            if (init) {
                adapter = PostAdapter(it, RV_VERT, null)
                b.rv.adapter = adapter
                init = false
            } else {
                esLastIndex = OnRv.setRvInFrag(it, b.inData, b.refreshLayout, b.tvLast, esLastIndex)
                adapter.notifyDataSetChanged()
            }
        })


        b.refreshLayout.setOnRefreshListener { getEsDataList() }
        b.refreshLayout.setOnLoadMoreListener { vmPost.getEsPostList(esSize, esLastIndex, board, Status.DELETED_USER) }
        b.refreshLayout.setEnableLoadMore(false)  // 최초에는 loadMore 불능.


//        b.nScroll.setOnScrollChangeListener { _: NestedScrollView?, _: Int, y: Int, _: Int, oldY: Int ->
//            when {
//                y == 0 -> b.fabScroll.hideFadeOut(false)
//                y < oldY -> b.fabScroll.showFadeIn(false)
//                else -> b.fabScroll.hideFadeOut(false)
//            }
//        }
    }

//    fun onClickScrollUp() {
//        b.nScroll.smoothScrollTo(0, 0)
//        b.fabScroll.hideFadeOut(false)
//    }

    private fun initData() {

        b.inData.avLoading.visi()
        getEsDataList()

        /**   광고 top */
        val adType = if (board.key == RECENT_BOARD_KEY) AdType.TOP_TOTAL_BOARD else AdType.TOP_BOARD    // recentBoard일 때 분기.
        val boardKeyForAd = if (board.key == RECENT_BOARD_KEY) null else board.key   // recentBoard가 아니면 개별보드의 키.
        banner = CustomBannerSlide(mContext, adType, boardKeyForAd)
        b.frameAd.addView(banner)

//        MyCircle.cancel()
    }

    private fun getEsDataList() {
        esLastIndex = 0   // 꼭 해야 됨. esLastIndex로 loadMOre을 변경할 것이기 때문에.
        vmPost.getEsPostList(esSize, esLastIndex, board, Status.DELETED_USER)
        b.inData.tvRefresh.gone()
    }


}
