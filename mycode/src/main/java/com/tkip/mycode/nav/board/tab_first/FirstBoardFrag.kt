package com.tkip.mycode.nav.board.tab_first

import android.os.Bundle
import com.tkip.mycode.R
import com.tkip.mycode.databinding.FragmentFirstBoardBinding
import com.tkip.mycode.init.ACTION_NEW
import com.tkip.mycode.init.CH_LAST_POST_DATE
import com.tkip.mycode.init.base.getStr
import com.tkip.mycode.init.my_bind.BindingFragment
import com.tkip.mycode.model.board.BoardType
import com.tkip.mycode.model.board.CustomBoardList
import com.tkip.mycode.model.board.OnBoard
import com.tkip.mycode.model.my_enum.Status
import com.tkip.mycode.util.lib.retrofit2.ESquery


class FirstBoardFrag : BindingFragment<FragmentFirstBoardBinding>() {
    override fun getLayoutResId(): Int = R.layout.fragment_first_board
    //    private val vmBoard by lazy { getViewModel<BoardViewModel>() }
//    private lateinit var adapBoardPopular: BoardAdapter
    private lateinit var viewUpdated: CustomBoardList
    private lateinit var viewPopular: CustomBoardList
    private lateinit var viewNews: CustomBoardList


    companion object {
        @JvmStatic
        fun newInstance() = FirstBoardFrag().apply { arguments = Bundle().apply { } }
    }


    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)


        b.fragment = this

//        viewUpdated = ViewBoardList(mContext, R.string.title_board_recommend.getStr(),ESquery.boardList(6, CH_LAST_POST_DATE, Status.NORMAL))
//        b.lineCover.addView(viewUpdated)

        viewUpdated = CustomBoardList(mContext, R.string.title_board_recent_updated.getStr(), ESquery.boardList(6, BoardType.USER.no, CH_LAST_POST_DATE, Status.NORMAL))
        b.lineCover.addView(viewUpdated)

        viewPopular = CustomBoardList(mContext, R.string.title_board_popular.getStr(), ESquery.boardPopular(6))
        b.lineCover.addView(viewPopular)

        viewNews = CustomBoardList(mContext, R.string.title_board_recent_new.getStr(), ESquery.boardListNews(6,   true))
        b.lineCover.addView(viewNews)

//        getUpdatedList()
//        getPopularList()
//        getNewList()
    }

    fun onClickAddBoard() {
        OnBoard.gotoAddBoardActivity(mContext, ACTION_NEW, null, null)

        /*val dialog = AddBoardDialog.newInstance(ACTION_NEW,R.string.title_add_board.getStr(), R.string.description_add_board.getStr())
        dialog.setTargetFragment(this, Cons.REQ_ADD_BOARD_DIALOG)
//        dialog.show(childFragmentManager, "dialog")
        dialog.show(fragmentManager!!, "dialog")
//        dialog.show(fragmentManager?.beginTransaction(), "dialog")
//        dialog.show(childFragmentManager.beginTransaction(),"dialog")*/
    }


}
