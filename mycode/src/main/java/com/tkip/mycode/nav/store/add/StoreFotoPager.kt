package com.tkip.mycode.nav.store.add

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.viewpager.widget.PagerAdapter
import androidx.viewpager.widget.ViewPager
import com.tkip.mycode.R
import com.tkip.mycode.databinding.ItemFotoBinding
import com.tkip.mycode.util.lib.photo.Foto
import com.tkip.mycode.util.lib.photo.photoview.OnPhotoView
import com.tkip.mycode.funs.common.tranPair

/**
 * StoreActivity에서 상단에 보여질 foto pager
 */
class StoreFotoPager(  val items: ArrayList<Foto>) : PagerAdapter() {

//    lateinit var b: ItemFotoBinding
//    lateinit  var activity: AppCompatActivity
    override fun instantiateItem(container: ViewGroup, p: Int): Any {

        val  b =  ItemFotoBinding.inflate(LayoutInflater.from(container.context), container, false)
        b.adapter = this
        b.foto = items[p]

        b.iv.setOnClickListener{
            OnPhotoView.gotoPhotoViewActivity(container.context, p,items, arrayOf(b.iv.tranPair(R.string.tran_image)))
        }

        container.addView(b.root)
        return b.root
    }

    override fun isViewFromObject(view: View, obj: Any): Boolean {
        return view == obj
    }

    override fun destroyItem(container: ViewGroup, position: Int, `object`: Any) {
        (container as ViewPager).removeView(`object` as View)
    }

    override fun getCount(): Int {
        return items.size
    }



}