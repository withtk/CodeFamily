package com.tkip.mycode.nav.flea

import android.Manifest.permission.READ_EXTERNAL_STORAGE
import android.content.pm.PackageManager
import android.os.Bundle
import androidx.annotation.LayoutRes
import androidx.core.content.ContextCompat
import com.tkip.mycode.R
import com.tkip.mycode.admin.versionGteQ
import com.tkip.mycode.databinding.ActivityAddFleaBinding
import com.tkip.mycode.dialog.alert.MyAlert
import com.tkip.mycode.funs.common.*
import com.tkip.mycode.init.*
import com.tkip.mycode.init.base.clearAddAll
import com.tkip.mycode.init.base.getI
import com.tkip.mycode.init.base.getStr
import com.tkip.mycode.init.base.removeComma
import com.tkip.mycode.init.inter.interManualMore
import com.tkip.mycode.init.inter.interRequestPermissionsResult
import com.tkip.mycode.init.my_bind.BindingActivityPhoto
import com.tkip.mycode.model.flea.Flea
import com.tkip.mycode.model.flea.OnFlea
import com.tkip.mycode.model.flea.gotoFleaActivity
import com.tkip.mycode.model.flea.inValid
import com.tkip.mycode.model.media.LinearMediaSpinner
import com.tkip.mycode.model.media.ViewMediaEditText
import com.tkip.mycode.model.media.getFinalMediaMap
import com.tkip.mycode.model.media.setupMediaEditText
import com.tkip.mycode.model.my_enum.PickerFactory
import com.tkip.mycode.model.mytag.TagTYPE
import com.tkip.mycode.model.mytag.convertStringList
import com.tkip.mycode.model.peer.OnPeer
import com.tkip.mycode.util.lib.photo.Foto
import com.tkip.mycode.util.lib.photo.invalidFotoList
import com.tkip.mycode.util.lib.photo.view.LinearAddPhotoBox
import com.tkip.mycode.util.my_view.ViewUtil
import com.tkip.mycode.util.tools.anim.AnimUtil
import com.tkip.mycode.util.tools.etc.OnMyClick

/**
 */
class AddFleaActivity : BindingActivityPhoto<ActivityAddFleaBinding>() {
    @LayoutRes
    override fun getLayoutResId() = R.layout.activity_add_flea

    private val addType by lazy { intent.getIntExtra(PUT_ACTION, ACTION_NEW) }
    private val flea by lazy { intent.getParcelableExtra(PUT_FLEA) ?: Flea() }
    private lateinit var finalFlea: Flea

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        initView()
        finalFlea = Flea(flea)
        initData(finalFlea)
    }


    private fun initView() {
        setSupportActionBar(b.toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        b.activity = this
        b.addType = addType
        b.inMyCredit.peer = OnPeer.peer

        /*** 매뉴얼  */
        b.inManual.inter = interManualMore {
            b.inManual.tvManual1.maxLines = 100
            AnimUtil.goneNormal(b.inManual.btnMoreInfo)
        }
        b.inManual.manual = R.string.description_add_flea.getStr()

        /*** 사진박스  */
        viewAddPhotoBox = LinearAddPhotoBox(this, onOpenAlbum = { OnPermission.onOpenAlbum(true, this) })
        b.frameAddPhotoBox.addView(viewAddPhotoBox)

        /*** 검색태그 */
        linearAddTag = ViewUtil.addLinearAddTag(this, b.lineTag, TagTYPE.FLEA, R.string.name_tag_search.getStr(), R.string.btn_open_tag_dialog.getStr(), flea.tagList)


        /********************* superListener *****************************/
        listenerPermissionsResult = interRequestPermissionsResult(onReadExternalStorage = { OnPermission.onOpenAlbum(it, this) })

    }

    private fun initData(flea1: Flea) {
        b.flea = flea1

        /**         * 미디어 스피너 setup         */
        b.lineMedia.addView(LinearMediaSpinner(this,
                onClick = { b.lineMedia.addView(ViewMediaEditText(this, it)) }))

        /**         * 미디어 스피너 초기값 update        */
        flea1.mediaMap.setupMediaEditText(this, b.lineMedia)


        /**         *  사진 보이기         */
        viewAddPhotoBox?.setPhotos(flea1.fotoList)

    }

    /**     * 올릴 flea 준비하기.     */
    private fun getFinalFlea() =
            finalFlea.apply {
                price = b.tvPrice.getNullStr()?.removeComma()?.toInt() ?: 0
                title = b.etTitle.getNullStr()
                comment = b.etComment.getNullStr()
                contact = b.etContacts.getNullStr()
                local = b.tvLocal.getNullStr()

                /**  location : 위치태그  이미 삽입 onClickTagDialogOne  */

                /** 사진 삽입 */
                fotoList = fotoList.clearAddAll(viewAddPhotoBox?.selectedFotoList)

                /** 미디어 아이디 삽입 */
                mediaMap = getFinalMediaMap()

                /** 태그 삽입 */
                linearAddTag?.b?.tagView?.convertStringList()?.let { tagList = tagList.clearAddAll(it) }

            }


    override fun onRestoreInstanceState(save: Bundle) {
        super.onRestoreInstanceState(save)
        save.getParcelable<Flea>(PUT_RESULT_OBJECT)?.let { flea1 ->
            initData(flea1)
        } ?: finish()

        /**         * 퍼미션 있을때에만 앨범에서 사진 가져오기.         */
        val readPermission = ContextCompat.checkSelfPermission(this, READ_EXTERNAL_STORAGE)
        if (readPermission == PackageManager.PERMISSION_GRANTED) {
            save.getParcelableArrayList<Foto>(PUT_RESULT_LIST)?.let { list ->
                viewAddPhotoBox?.selectedFotoList = list
                viewAddPhotoBox?.selectedFotoAdapter?.notifyDataSetChanged()
            }
        }
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        lllogI("AddFleaActivity onSaveInstanceState  ")

        if (!versionGteQ) {
            outState.putParcelable(PUT_RESULT_OBJECT, getFinalFlea())
            outState.putParcelableArrayList(PUT_RESULT_LIST, viewAddPhotoBox?.selectedFotoList)
        }
    }

    override fun onBackPressed() {
//        if (OnValid.checkKeyboardHide(b.etComment, b.etContacts, b.etTitle)) return
        OnValid.removeFocusEt(b.etComment,b.etContacts,b.etTitle)

        if (b.etTitle.getStr().isNotBlank() || b.tvPrice.text.toString().isNotBlank())
            MyAlert.showConfirm(this, getString(R.string.info_wanna_leave_add_common), ok = { super.onBackPressed() }, cancel = {})
        else super.onBackPressed()
    }


    fun onClickPrice() {
        if (OnMyClick.isDoubleClick()) return

        MyAlert.selectPrice(this,finalFlea, b.tvPrice.getInt(), 10, onSuccess = { s, i -> b.tvPrice.text = s })

//        MyAlert.showNumberPicker(this, PickerFactory.PESO, b.tvPrice.getNullStr(), onOk = { amount, commaStr ->
//            lllogI("RoomAmount showNumberPicker $commaStr")
//            b.tvPrice.text = commaStr
//        })
    }

    fun onClickTagDialogOne() {
        if (OnMyClick.isDoubleClick()) return
        MyAlert.showAddTag(this, true, b.tvLocal.getNullStr(), TagTYPE.LOC, null,
                onSingleResult = {
                    lllogI("onClickTagDialogOne onSingleResult $it")
                    b.tvLocal.text = it
                    finalFlea.local = it
                })
    }

    fun onClickPreview() {
        if (OnMyClick.isDoubleClick()) return
        getFinalFlea().gotoFleaActivity(this, false, ACTION_PREVIEW, null)
    }

    fun onClickSubmit() {
        if (OnMyClick.isDoubleClick()) return
//        if (b.etStuffName.isNullBlank(R.string.alert_need_to_stuff_name)) return
//        if (b.tvPrice.isNullBlank(R.string.alert_need_to_price)) return
//        if (b.tvLocal.isNullBlank(R.string.alert_need_to_location_flea)) return

        lllogD("AddFleaActivity fotoList ${flea.fotoList.getI(0)}")
        if (viewAddPhotoBox?.selectedFotoList.invalidFotoList()) return
        if (getFinalFlea().inValid()) return

        MyAlert.showConfirm(this, R.string.info_wanna_upload_add_post.getStr(),
                ok = {
                    finalAdd()
                }, cancel = { })
    }


    private fun finalAdd() {
        OnFlea.addPhotoAndFlea(addType, this, null, viewAddPhotoBox?.selectedFotoList!!, flea, finalFlea, {}, {})
    }


}
