package com.tkip.mycode.nav.flea

import android.os.Bundle
import android.os.Handler
import android.view.View
import androidx.annotation.LayoutRes
import com.tkip.mycode.R
import com.tkip.mycode.admin.menu.AMenu
import com.tkip.mycode.admin.menu.OnMenu
import com.tkip.mycode.databinding.ActivityFleaBinding
import com.tkip.mycode.dialog.OnDDD
import com.tkip.mycode.dialog.alert.MyAlert
import com.tkip.mycode.dialog.toast.toastNormal
import com.tkip.mycode.funs.common.OnPermission
import com.tkip.mycode.funs.common.errorWhen
import com.tkip.mycode.funs.common.tranPair
import com.tkip.mycode.funs.custom.ViewAuthorTitle
import com.tkip.mycode.funs.custom.toolbar.ViewToolbar
import com.tkip.mycode.init.*
import com.tkip.mycode.init.base.getStr
import com.tkip.mycode.init.base.getViewModel
import com.tkip.mycode.init.inter.interActivityResult
import com.tkip.mycode.init.inter.interMenuBox
import com.tkip.mycode.init.inter.interRequestPermissionsResult
import com.tkip.mycode.init.my_bind.BindingActivityPhoto
import com.tkip.mycode.init.part.CodeApp
import com.tkip.mycode.model.cost.Cost
import com.tkip.mycode.model.count.CountType
import com.tkip.mycode.model.count.CountViewModel
import com.tkip.mycode.model.flea.Flea
import com.tkip.mycode.model.flea.OnFlea
import com.tkip.mycode.model.media.setupMediaButton
import com.tkip.mycode.model.my_enum.*
import com.tkip.mycode.model.my_enum.showStatusDialog
import com.tkip.mycode.model.mytag.CustomTagList
import com.tkip.mycode.model.reply.LinearReplyAddBox
import com.tkip.mycode.model.reply.OnReply
import com.tkip.mycode.model.toss.OnToss
import com.tkip.mycode.model.warn.OnWarn
import com.tkip.mycode.util.lib.retrofit2.ESget
import com.tkip.mycode.util.tools.fire.commonStatusUpdate
import com.tkip.mycode.util.tools.fire.real
import com.tkip.mycode.util.tools.recycler.OnRv


class FleaActivity : BindingActivityPhoto<ActivityFleaBinding>() {
    @LayoutRes
    override fun getLayoutResId() = R.layout.activity_flea

    private val addType by lazy { intent.getIntExtra(PUT_ACTION, ACTION_NORMAL) }
    //    private val vmFlea by lazy { getViewModel<FleaViewModel>() }
    private val vmCount by lazy { getViewModel<CountViewModel>() }

    private val putFlea by lazy { intent.getParcelableExtra<Flea>(PUT_FLEA) ?: Flea() }
    private val fleaKey by lazy { intent.getStringExtra(PUT_KEY) }

    private lateinit var flea: Flea
    private lateinit var viewToolbar: ViewToolbar

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        if (fleaKey == null) {

            flea = putFlea
            initView()
            initData()
//            MyCircle.cancel()

        } else {

//            WAIT_TYPE.FULL_CIRCLE.show(this@FleaActivity)
            ESget.getFlea(fleaKey,
                    success = {
                        it?.let { flea1 ->
                            if (flea1.isValidStatus(Status.NORMAL)) {
                                flea = flea1
                                initView()
                                initData()
//                                MyCircle.cancel()
                                CodeApp.curModelKey = flea.key

                            }
                        }
                    }, fail = {})
        }

    }

    override fun onResume() {
        super.onResume()
        CodeApp.curModelKey = if (::flea.isInitialized) flea.key else null
    }

    override fun onStop() {
        super.onStop()
        CodeApp.curModelKey = null
    }

    private fun initView() {

        Cost.new(null,ActType.OPEN_FLEA, flea).real()

        b.activity = this
        b.addType = addType

        /** Toolbar */
        viewToolbar = ViewToolbar(this@FleaActivity, R.string.title_flea.getStr(), flea.title, flea.addDate,
                onBack = { onBackPressed() },
                onBoardTitle = { },
                onMore = { onClickMore() }
        ).apply { this@FleaActivity.b.frameToolbar.addView(this) }

        /**  글쓴이 check anonymous  */
        b.lineWhite.addView(ViewAuthorTitle(this@FleaActivity, flea.anonymous, flea.authorUID, flea.addDate, onPeer = {}), 0)

        /**  top 사진 rv  */
        OnRv.setTopFotoAdapter(flea.fotoList, b.rvFoto)

        if (addType != ACTION_PREVIEW) initInterface()   // Action_PREVIEW는 하면 안 됨.
    }

    private fun initData() {
        b.flea = flea

        /*** 미디어 셋팅 */
        flea.mediaMap.setupMediaButton(this@FleaActivity, b.lineMedia)

        /*** 태그 : binding에서 기입 */

    }

    private fun initInterface() {

        /** 검색 태그리스트   */
        if (!flea.tagList.isNullOrEmpty()) b.frameTagList.addView(CustomTagList(this@FleaActivity, flea.tagList))

        /** 업데이트 data & 뷰 카운트   */
        b.inViewCount.updateDate = flea.updateDate
        b.inViewCount.viewCount = flea.viewCount
        //        b.inViewCount.bookCount = post.bookmarkCount
        b.inViewCount.replyCount = flea.replyCount
        //        b.inViewCount.pushCount = post.pushCount

        /********************************** inInterface ***********************************/

        /**   Count CheckBox 세팅    */
        vmCount.initCount(this@FleaActivity, flea, arrayOf(CountType.VIEW_UID, CountType.BOOK, CountType.PUSH), null, null, b.inMenuBox.cbBookmark, b.inMenuBox.cbPush)
//        vmCount.addViewCountRealDB(flea)  // VIEW_COUNT 추가
//        vmCount.getAllEsCount(flea, arrayOf(CountType.VIEW_UID, CountType.BOOK, CountType.PUSH))
//        vmCount.liveViewUid.observe(this, Observer { if (it == null) OnCount.addCount(flea, CountType.VIEW_UID) })
//        vmCount.liveBookmark.observe(this, Observer { b.inMenuBox.cbBookmark.setCountChange(it) })
//        vmCount.livePush.observe(this, Observer { b.inMenuBox.cbPush.setCountChange(it) })

        /**  메뉴박스   */
        b.inMenuBox.bookmark = flea.bookmarkCount
        b.inMenuBox.push = flea.pushCount
        b.inMenuBox.inter = interMenuBox(
                onClickThumbUp = { },
                onClickThumbDown = { },
                onClickBookmark = { vmCount.updateCount(flea, CountType.BOOK, it) },
                onClickPush = { vmCount.updateCount(flea, CountType.PUSH, it) },
                onClickToss = { OnToss.checkAdd(this@FleaActivity, flea) },
                onClickMore = { onClickMore() })

        /**   * 글 작성 직후일때 : 1초 후 자동으로 PUSH 추가.  */
        if (addType == ACTION_NEW) Handler().postDelayed({ toastNormal(R.string.info_set_push) }, 1000)

        /**  댓글 첨부박스   */
        linearReplyAddBox = LinearReplyAddBox(this@FleaActivity, flea.allowReply, flea.key,
                onClickAddReply = {
                    linearReplyAddBox.checkAddReply(this@FleaActivity, flea, replyAdapter,
                            success = { b.nestedScrollView.fullScroll(View.FOCUS_DOWN) }, fail = {})
                }).apply { this@FleaActivity.b.frameReplyBox.addView(this) }

        /** reply  */
        replyAdapter = OnReply.setAdapter(this@FleaActivity, EES_FLEA, flea.key, EES_REPLY, flea, b.inRvReply, linearReplyAddBox, onCompleteAdoption = { })

        /********************* superListener *****************************/
        listenerActivityResult = interActivityResult(
                onPick = {
                    linearReplyAddBox.addPhotoUri(it)
                }, onAlbum = {}, onMap = { _, _ -> }, onBanner = null, onData = null)

        listenerPermissionsResult = interRequestPermissionsResult(
                onReadExternalStorage = { OnPermission.onOpenGallery(this@FleaActivity, it, false) })

    }


    fun onClickMore() {
        if (addType == ACTION_PREVIEW) return
        MyAlert.showMenuList(this@FleaActivity, OnMenu.getMENULIST(flea),
                onSelect = { amenu ->
                    when (amenu) {
                        AMenu.UPDATE -> OnFlea.gotoAddFleaActivity(this@FleaActivity, ACTION_UPDATE, flea, arrayOf(b.lineWhite.tranPair(R.string.tran_linear)))
                        AMenu.DELETION -> OnDDD.delete(this@FleaActivity, ok = { commonStatusUpdate(EES_FLEA, flea.key, Status.DELETED_USER, success = { finish() }, fail = {}) }, cancel = {})
                        AMenu.TRANSACTION_COMPLETED -> OnDDD.confirm(this@FleaActivity, R.string.status_dialog_transaction_completed, ok = {commonStatusUpdate(EES_FLEA, flea.key, Status.TRANSACTION_COMPLETED, success = { finish() }, fail = {})}, cancel = {})
                        AMenu.WARN -> OnWarn.checkAndAdd(this@FleaActivity, flea, success = {}, fail = {})
                        AMenu.DEEP_DEL -> flea.deepDel(this@FleaActivity, onSuccess = {})
                        AMenu.STATUS -> flea.showStatusDialog(this@FleaActivity, null)
                        else -> errorWhen()
                    }
                })
    }

}