package com.tkip.mycode.nav.store.store_menu

import android.content.Context
import android.content.Intent
import android.view.View
import com.firebase.ui.firestore.FirestoreRecyclerOptions
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.firestore.Query
import com.tkip.mycode.R
import com.tkip.mycode.dialog.progress.MyCircle
import com.tkip.mycode.dialog.toast.toastNormal
import com.tkip.mycode.init.CH_INDEX
import com.tkip.mycode.init.EES_STORE
import com.tkip.mycode.init.EES_STORE_MENU
import com.tkip.mycode.init.PUT_STORE
import com.tkip.mycode.model.store.Store
import com.tkip.mycode.model.store.StoreMenu
import com.tkip.mycode.util.lib.TransitionHelper
import com.tkip.mycode.util.lib.photo.util.PhotoUtil
import com.tkip.mycode.util.tools.fire.Firee

/**
 * storeMenu
 */
class OnStoreMenu {

    companion object {

        fun gotoMenuManageActivity(context: Context, store: Store, arrPair: Array<androidx.core.util.Pair<View, String>>?) {
            val intent = Intent(context, StoreMenuManageActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
            intent.putExtra(PUT_STORE, store)
            TransitionHelper.startTran(context, intent, arrPair)
        }

        /**
         * upload
         */
        fun addPhotoAndStoreMenu(context: Context, storeMenu: StoreMenu, success: () -> Unit, fail: () -> Unit) {
            PhotoUtil.uploadUriSingle(context, storeMenu,
                    successUploading = { result ->

                        result?.let { foto ->
                            storeMenu.photoThumb = foto.thumbUrl
                            storeMenu.photoUrl = foto.url
                        }

                        Firee.addStoreMenu(storeMenu,
                                success = {
                                    success()
                                    toastNormal(R.string.complete_upload)
                                    MyCircle.cancel()
                                }, fail = { fail() })

                    }, fail = { })
        }


        /**
         *  스토어 메뉴 받아올 options
         */
        fun getOptions(storeKey: String) = FirestoreRecyclerOptions.Builder<StoreMenu>()
                .setQuery(FirebaseFirestore.getInstance().collection(EES_STORE).document(storeKey).collection(EES_STORE_MENU)
                        .orderBy(CH_INDEX, Query.Direction.ASCENDING), StoreMenu::class.java)
                .build()






    }

}