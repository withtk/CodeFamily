package com.tkip.mycode.nav.store.add

import android.Manifest.permission.READ_EXTERNAL_STORAGE
import android.content.pm.PackageManager
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.widget.TextView
import androidx.annotation.LayoutRes
import androidx.core.content.ContextCompat
import androidx.databinding.ViewDataBinding
import com.tkip.mycode.R
import com.tkip.mycode.admin.versionGteQ
import com.tkip.mycode.databinding.*
import com.tkip.mycode.dialog.alert.MyAlert
import com.tkip.mycode.funs.common.*
import com.tkip.mycode.funs.custom.LinearMapAddBox
import com.tkip.mycode.init.*
import com.tkip.mycode.init.base.clearAddAll
import com.tkip.mycode.init.base.getStr
import com.tkip.mycode.init.inter.interActivityResult
import com.tkip.mycode.init.inter.interBtnPreNext
import com.tkip.mycode.init.inter.interRequestPermissionsResult
import com.tkip.mycode.init.my_bind.BindingActivityPhoto
import com.tkip.mycode.model.media.addViewMediaEdittext
import com.tkip.mycode.model.media.addViewMediaSpinner
import com.tkip.mycode.model.media.getFinalMediaMap
import com.tkip.mycode.model.media.setupMediaEditText
import com.tkip.mycode.model.my_enum.STATUS_LIST_BASIC
import com.tkip.mycode.model.mytag.TagTYPE
import com.tkip.mycode.model.mytag.convertStringList
import com.tkip.mycode.model.peer.isAdmin
import com.tkip.mycode.model.store.Store
import com.tkip.mycode.model.store.gotoStoreActivity
import com.tkip.mycode.model.store.inValid
import com.tkip.mycode.nav.store.OnStore
import com.tkip.mycode.util.lib.map.MAP_ACT_ADD
import com.tkip.mycode.util.lib.map.OnMap
import com.tkip.mycode.util.lib.photo.Foto
import com.tkip.mycode.util.lib.photo.initSelectedFotoList
import com.tkip.mycode.util.lib.photo.invalidFotoList
import com.tkip.mycode.util.lib.photo.view.LinearAddPhotoBox
import com.tkip.mycode.util.my_view.ViewUtil
import com.tkip.mycode.util.tools.anim.gone
import com.tkip.mycode.util.tools.etc.OnMyClick
import com.tkip.mycode.util.tools.etc.OnSpannable
import com.tkip.mycode.util.tools.spinner.LinearSpinnerStatus
import com.tkip.mycode.util.tools.spinner.LinearSpinnerString
import com.tkip.mycode.util.tools.spinner.TypeList


/**
 * todo : 스토어는 하루에 한개만 등록 가능하게.
 */
class AddStoreActivity : BindingActivityPhoto<ActivityAddStoreBinding>() {
    @LayoutRes
    override fun getLayoutResId() = R.layout.activity_add_store

    private val addType by lazy { intent.getIntExtra(PUT_ACTION, ACTION_NEW) }
    private val store by lazy { intent.getParcelableExtra(PUT_STORE) ?: Store() }
    private lateinit var finalStore: Store

    private lateinit var bName: ItemAddStoreNameBinding
    private lateinit var bContact: ItemAddStoreContactBinding
    private lateinit var bTime: ItemAddStoreWorkingTimeBinding
    private lateinit var bAddress: ItemAddStoreAddressBinding
    private lateinit var bTag: ItemAddStoreTagBinding
    //    lateinit var bMenu: ItemAddStoreMenuBinding
    private lateinit var bPhotos: ItemAddStorePhotosBinding
    private lateinit var bComment: ItemAddStoreCommentBinding

//    private lateinit var menuAdapter: StoreMenuAdapter


    //    private val arr by lazy { arrayOf(bName, bContact, bTime, bAddress, bTag, bMenu, bPhotos, bComment) }
    lateinit var arr: Array<ViewDataBinding>

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        finalStore = Store(store)
        initView()
        initData(finalStore)

//        MyCircle.cancel()
    }


    private fun initView() {
        b.activity = this

        initPagerFrag()

        b.viewPager.adapter = AddStorePager(arr)
        b.viewPager.offscreenPageLimit = arr.size // tab이 리셋되지 않도록.
        b.viewPager.onPageChange({}, { _, _, _ -> }, onSelected = { _ -> })

        /********************* superListener *****************************/
        listenerActivityResult = interActivityResult(onPick = null, onAlbum = null,
                onMap = { address, latlng ->
                    finalStore.address = address
                    finalStore.lat = latlng?.latitude
                    finalStore.lng = latlng?.longitude
                }, onBanner = null, onData = null)
        listenerPermissionsResult = interRequestPermissionsResult(onReadExternalStorage = { OnPermission.onOpenAlbum(it, this@AddStoreActivity) })
    }

    override fun onBackPressed() {
        MyAlert.showConfirm(this@AddStoreActivity, R.string.info_canced_add_store.getStr(), { super.onBackPressed() }, {})
    }

    fun onClickMoveToEnd(v: View) {
        OnMyClick.setDisableAWhileBTN(v)
        b.viewPager.currentItem = arr.size - 1
    }


    private fun initPagerFrag() {

        /** itemName ******************************************************************************/
        bName = ItemAddStoreNameBinding.inflate(LayoutInflater.from(this@AddStoreActivity))
        OnSpannable.setRelativeSizeSpan(bName.tvTitle, 0.5f, 4, 8)

        /** itemContacts ******************************************************************************/
        bContact = ItemAddStoreContactBinding.inflate(LayoutInflater.from(this@AddStoreActivity))
        /**  * 미디어 스피너 setup   */
        bContact.lineMedia.addViewMediaSpinner(this@AddStoreActivity,
                onClick = { bContact.lineMedia.addViewMediaEdittext(this@AddStoreActivity, it) })


        /** itemWorkTime ******************************************************************************/
        bTime = ItemAddStoreWorkingTimeBinding.inflate(LayoutInflater.from(this@AddStoreActivity))
        OnSpannable.setRelativeSizeSpan(bTime.tvTitle, 0.5f, 4, 8)


        /** itemAddress ******************************************************************************/
        bAddress = ItemAddStoreAddressBinding.inflate(LayoutInflater.from(this@AddStoreActivity))
        bAddress.activity = this@AddStoreActivity
        OnSpannable.setRelativeSizeSpan(bAddress.tvTitle, 0.5f, 7, 11)
        /***  지도  */
        linearAddMap = LinearMapAddBox(this@AddStoreActivity, 2, finalStore.address, OnMap.createLatlng(finalStore.lat, finalStore.lng))
        bAddress.lineMap.addView(linearAddMap)

        /** itemTag ******************************************************************************/
        bTag = ItemAddStoreTagBinding.inflate(LayoutInflater.from(this@AddStoreActivity))
        bTag.store = finalStore
        bTag.activity = this@AddStoreActivity
        OnSpannable.setRelativeSizeSpan(bTag.tvDesc, 1.2f, 19, 24)
        OnSpannable.setRelativeSizeSpan(bTag.tvDesc, 1.2f, 25, 30)
        /**  태그리스트  */
        linearAddTag = ViewUtil.addLinearAddTag(this@AddStoreActivity, bTag.lineTag, TagTYPE.STORE, R.string.name_tag_search.getStr(), R.string.btn_open_tag_dialog.getStr(), finalStore.tagList)


        /** itemMenu ******************************************************************************/
//        bMenu = ItemAddStoreMenuBinding.inflate(LayoutInflater.from(this@AddStoreActivity))
//        bMenu.activity = this
//        menuAdapter = StoreMenuAdapter()
//
//        OnSpannable.setRelativeSizeSpan(bMenu.tvTitle, 0.5f, 5, 9)
//        OnSpannable.setRelativeSizeSpan(bMenu.tvDesc, 1.2f, 27, 32)
//        OnSpannable.setForegroundColorSpan(bMenu.tvDesc, R.color.skyblue_800.getClr(), 27, 32)


        /** itemPhoto ******************************************************************************/
        bPhotos = ItemAddStorePhotosBinding.inflate(LayoutInflater.from(this@AddStoreActivity))
        OnSpannable.setRelativeSizeSpan(bPhotos.tvTitle, 0.5f, 5, 12)
        /*** 사진박스  */
        viewAddPhotoBox = LinearAddPhotoBox(this@AddStoreActivity, onOpenAlbum = {
            OnPermission.onOpenAlbum(true, this@AddStoreActivity)
        }).apply { this@AddStoreActivity.bPhotos.frameAddPhotoBox.addView(this) }


        /** itemComment ******************************************************************************/
        bComment = ItemAddStoreCommentBinding.inflate(LayoutInflater.from(this@AddStoreActivity))
        bComment.activity = this
        if (isAdmin()) {
            /**         * Status 스피너     */
            bComment.lineSpin.addView(LinearSpinnerStatus(this@AddStoreActivity, true, STATUS_LIST_BASIC, finalStore.statusNo, onClick = { it.let { finalStore.statusNo = it.no } }))
            /**         * String 스피너 : StoreType    */
            bComment.lineSpin.addView(LinearSpinnerString(this@AddStoreActivity, true, null, TypeList.STORE, finalStore.storeTypeNo, onSelected = { storeTypeNo -> finalStore.storeTypeNo = storeTypeNo }))
        }

        arr = arrayOf(bName, bContact, bTime, bAddress, bTag, bPhotos, bComment)

        setBtnPreNext(bName.inBtn, bName.tvNo, 0)
        setBtnPreNext(bContact.inBtn, bContact.tvNo, 1)
        setBtnPreNext(bTime.inBtn, bTime.tvNo, 2)
        setBtnPreNext(bAddress.inBtn, bAddress.tvNo, 3)
        setBtnPreNext(bTag.inBtn, bTag.tvNo, 4)
//        setBtnPreNext(bMenu.inBtn, bMenu.tvNo, 5)
        setBtnPreNext(bPhotos.inBtn, bPhotos.tvNo, 5)
        setBtnPreNext(bComment.inBtn, bComment.tvNo, 6)

    }

    /**     * 앞뒤 버튼, tvNo 셋팅     */
    private fun setBtnPreNext(inBtn: LinearBtnPreNextBinding, tv: TextView, position: Int) {
        inBtn.currentNo = position
        inBtn.lastNo = arr.size - 1
        inBtn.inter = interBtnPreNext(
                onClickPre = { b.viewPager.currentItem = b.viewPager.currentItem - 1 },
                onClickNext = { b.viewPager.currentItem = b.viewPager.currentItem + 1 })

        val number = "${position + 1}/${arr.size}"
        tv.text = number
    }

    private fun initData(store1: Store) {
        bName.name = store1.title
        bName.isNew = if(isAdmin()) true else (addType == ACTION_NEW)


        bContact.contact = store1.contact
        bContact.homepage = store1.websiteUri

        /**         * 미디어 스피너 초기화        */
        finalStore.mediaMap.setupMediaEditText(this@AddStoreActivity, bContact.lineMedia)  // 초기화.

        bTime.workingTime = store1.weekdayText

        bAddress.address = store1.address
        bComment.comment = store1.comment

        /**         *  사진 보이기         */
        viewAddPhotoBox?.setPhotos(finalStore.fotoList)

    }

    /*************************************************************************************
     *************************************************************************************
     *************************************************************************************/
    override fun onRestoreInstanceState(save: Bundle) {
        super.onRestoreInstanceState(save)

        save.getParcelable<Store>(PUT_RESULT_OBJECT)?.let { store1 ->
            finalStore = store1
            initData(finalStore)
        } ?: finish()

        /**         * 퍼미션 있을때에만 앨범에서 사진 가져오기.         */
        //todo: 앨범의 사진이 삭제되면 문제. 그냥 모두 초기화 하는 것이 나아 보인다. 굳이 어렵게 짤 필요가...
        val readPermission = ContextCompat.checkSelfPermission(this@AddStoreActivity, READ_EXTERNAL_STORAGE)
        if (readPermission == PackageManager.PERMISSION_GRANTED) {
            save.getParcelableArrayList<Foto>(PUT_RESULT_LIST)?.let { list ->
                viewAddPhotoBox?.selectedFotoList?.initSelectedFotoList(list)
                viewAddPhotoBox?.selectedFotoAdapter?.notifyDataSetChanged()
            }
        }
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        outState.putParcelable(PUT_RESULT_OBJECT, getFinalStore())
        if (!versionGteQ) {
            outState.putParcelableArrayList(PUT_RESULT_LIST, viewAddPhotoBox?.selectedFotoList)
        }
    }

    /*************************************************************************************
     *************************************************************************************
     *************************************************************************************/
    private fun getFinalStore() =
            finalStore.apply {
                title = bName.etName.getStr()
                contact = bContact.etContact.getNullStr()
                /** 미디어 아이디 삽입 */
                mediaMap = getFinalMediaMap()

                weekdayText = bTime.etWorkingTime.getNullStr()

                /*** map 정보 삽입*/
//                address = linearAddMap?.address
//                lat = linearAddMap?.latLng?.latitude
//                lng = linearAddMap?.latLng?.longitude

                /** 태그 삽입 */
                linearAddTag?.b?.tagView?.convertStringList()?.let { tagList = tagList.clearAddAll(it) }

                /** 사진 삽입 */
                fotoList = fotoList.clearAddAll(viewAddPhotoBox?.selectedFotoList)

                comment = bComment.etComment.getNullStr()

                /** status & storeType은 이미 삽입 */

                lllogD("addStoreActivity getFinalStore $this")
            }


    fun onClickTagDialogOne(v: View) {
        OnMyClick.setDisableAWhileBTN(v)
        MyAlert.showAddTag(this@AddStoreActivity, true, bTag.tvLocal.getNullStr(), TagTYPE.LOC, null,
                onSingleResult = {
                    lllogI("onClickTagDialogOne onSingleResult $it")
                    bTag.tvLocal.text = it
                    finalStore.local = it
                })
    }

    fun onClickOpenMap(v: View) {
        OnMyClick.setDisableAWhileBTN(v)
        OnMap.gotoMainMap(this@AddStoreActivity, MAP_ACT_ADD, OnMap.createLatlng(finalStore.lat, finalStore.lng))
    }

    fun onClickDeleteLocation(v: View) {
        OnMyClick.setDisableAWhileBTN(v)
        finalStore.lat = null
        finalStore.lng = null
        finalStore.address = null
        linearAddMap?.deleteLocation()
    }

    fun onClickPreview(v: View) {
        OnMyClick.setDisableAWhileBTN(v)
        getFinalStore().gotoStoreActivity(this@AddStoreActivity, false, ACTION_PREVIEW, null)
    }

    fun onClickSubmit(v: View) {
        OnMyClick.setDisableAWhileBTN(v)

        if (getFinalStore().inValid()) return
        if (viewAddPhotoBox?.selectedFotoList.invalidFotoList()) return

        MyAlert.showConfirm(this@AddStoreActivity, R.string.info_wanna_upload_add_store.getStr(),
                ok = {
                    bComment.btnSubmit.isEnabled = false
                    finalAdd()
                }, cancel = {})

    }



    private fun finalAdd() {
        OnStore.addPhotoAndStore(addType, this@AddStoreActivity, null, viewAddPhotoBox?.selectedFotoList!!, store, finalStore, success = { }, fail = { bComment.btnSubmit.isEnabled = true })
    }


}
