package com.tkip.mycode.nav.store.add

import android.view.View
import android.view.ViewGroup
import androidx.databinding.ViewDataBinding
import androidx.viewpager.widget.PagerAdapter
import androidx.viewpager.widget.ViewPager

/**
 */
class AddStorePager(  val arr: Array<ViewDataBinding>) : PagerAdapter() {

    override fun instantiateItem(container: ViewGroup, p: Int): Any {
        container.addView(arr[p].root)
        return arr[p].root
    }

    override fun isViewFromObject(view: View, obj: Any): Boolean {
        return view == obj
    }

    override fun destroyItem(container: ViewGroup, position: Int, `object`: Any) {
        (container as ViewPager).removeView(`object` as View)
//        super.destroyItem(container, position, `object`)
    }

    override fun getCount(): Int {
        return arr.size
    }

}
