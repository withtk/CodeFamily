package com.tkip.mycode.nav.home

import android.content.Intent
import android.os.Bundle
import android.view.View
import com.tkip.mycode.R
import com.tkip.mycode.ad.AdType
import com.tkip.mycode.ad.ad_show.CustomBannerSlide
import com.tkip.mycode.databinding.FragmentHomeBinding
import com.tkip.mycode.funs.common.hhh
import com.tkip.mycode.funs.common.onClick
import com.tkip.mycode.funs.common.remoteStr
import com.tkip.mycode.funs.common.tranPair
import com.tkip.mycode.funs.search.OnSearch
import com.tkip.mycode.funs.search.model.CustomModelList
import com.tkip.mycode.funs.search.model.CustomModelListHori
import com.tkip.mycode.init.*
import com.tkip.mycode.init.base.getStr
import com.tkip.mycode.init.main.setDrawerLay
import com.tkip.mycode.init.my_bind.BindingFragment
import com.tkip.mycode.init.part.CodeApp
import com.tkip.mycode.model.board.BoardType
import com.tkip.mycode.model.board.OnBoard
import com.tkip.mycode.model.my_enum.DataType
import com.tkip.mycode.model.my_enum.Status
import com.tkip.mycode.my_test.TTTestActivity
import com.tkip.mycode.nav.store.OnStore
import com.tkip.mycode.util.lib.photo.glide.setFaceGlide
import com.tkip.mycode.util.lib.photo.glide.setUrlGlide
import com.tkip.mycode.util.lib.retrofit2.ESquery
import com.tkip.mycode.util.tools.etc.OnMyClick


class HomeFrag : BindingFragment<FragmentHomeBinding>() {
    override fun getLayoutResId(): Int = R.layout.fragment_home

    companion object {
        @JvmStatic
        fun newInstance() = HomeFrag().apply { arguments = Bundle().apply { } }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        b.fragment = this
        b.mainComment = R.string.remote_app_comment.remoteStr()
        b.isManageApp = CodeApp.isManageApp

        /**   광고 top */
        banner = CustomBannerSlide(mContext, AdType.MAIN, null)
        b.lineAd.addView(banner)
        hhh(3000) { banner?.startSlide("HomeFrag") }


//        b.iv1.setImageBitmap((CodeApp.icon as BitmapDrawable).bitmap)
        b.iv1.setImageDrawable(CodeApp.icon)
        b.ivLogo.setImageDrawable(CodeApp.icon)

        setTestCode()

        /*** 보드 News 세팅 */
//        b.lineBoard.addView(CustomBoardList(mContext, R.string.title_board_recommendation.getStr(), ESquery.boardPopular(6)))

        b.lineBoard.addView(CustomModelListHori(mContext, R.string.title_board_popular.getStr(), true, DataType.BOARD, RV_GRID_VERT,true, 2, ESquery.boardPopular(12), null))
//        b.lineBoard.addView(CustomModelListHori(mContext, R.string.title_board_recent_updated.getStr(), true, DataType.BOARD, RV_HORI_BIG, null, ESquery.boardList(6, BoardType.USER.no, CH_LAST_POST_DATE, Status.NORMAL), null))
//        b.lineBoard.addView(CustomModelListHori(mContext, R.string.title_board_recent_new.getStr(), true, DataType.BOARD, RV_HORI_BIG, null, ESquery.boardListNews(6, true), onItem = null))
        b.lineBoard.addView(CustomModelListHori(mContext, R.string.title_new_store.getStr(), true, DataType.STORE, RV_HORI_BIG, false, 1, ESquery.storeList(6, 0, Status.NORMAL), onItem = null))

    }

    fun onClickMenu(v: View) {
        OnMyClick.setDisableAWhileBTN(v)
        (activity as? MainActivity)?.toggleDrawer()   // 메뉴열기/닫기
    }

    fun onClickSearch(v: View) {
        OnMyClick.setDisableAWhileBTN(v)
        OnSearch.gotoSearchTotalActivity(mContext,null)
    }

    fun onClickAddStore(v: View) {
        OnMyClick.setDisableAWhileBTN(v)
        OnStore.gotoAddStoreActivity(mContext, ACTION_NEW, null, null)
    }

    fun onClickAddBoard(v: View) {
        OnMyClick.setDisableAWhileBTN(v)
        OnBoard.gotoAddBoardActivity(mContext, ACTION_NEW, null, null)
    }

    /**
     * 테스트 코드
     */
    private fun setTestCode() {

        b.btnTest.onClick {

            val intent = Intent(mContext, TTTestActivity::class.java)
            mContext.startActivity(intent)
        }

    }

}
