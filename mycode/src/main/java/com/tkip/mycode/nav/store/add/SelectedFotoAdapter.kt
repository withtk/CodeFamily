package com.tkip.mycode.nav.store.add

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.RecyclerView
import com.tkip.mycode.R
import com.tkip.mycode.databinding.HolderSelectedFotoBinding
import com.tkip.mycode.dialog.alert.MyAlert
import com.tkip.mycode.funs.common.lllogI
import com.tkip.mycode.init.base.getStr
import com.tkip.mycode.util.lib.photo.Foto
import com.tkip.mycode.util.tools.anim.AnimUtil

/**
 * 1. addStorePager 내의 사용자가 선택한 사진들, or admin이 검사할 때 보여지는 uploaded 사진들.
 * 1. addPostActivity의 업로드할 사진들.
 */
class SelectedFotoAdapter(val items: ArrayList<Foto>, val onLong: (Foto) -> Unit) : RecyclerView.Adapter<SelectedFotoAdapter.SelectedFotoViewHolder>() {
    private lateinit var activity: AppCompatActivity

    override fun getItemId(position: Int): Long {
        return items[position].hashCode().toLong()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): SelectedFotoViewHolder {
        this.activity = parent.context as AppCompatActivity
        val binding = HolderSelectedFotoBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return SelectedFotoViewHolder(binding)
    }

    override fun onBindViewHolder(h: SelectedFotoViewHolder, p: Int) {
        h.bind(items[p])
    }

    override fun getItemCount(): Int {
        return items.size
    }


    /**
     *
     */
    inner class SelectedFotoViewHolder(val b: HolderSelectedFotoBinding) : RecyclerView.ViewHolder(b.root) {

        lateinit var foto: Foto

        fun bind(foto: Foto) {
            lllogI("SelectedFotoViewHolder foto ${foto.thumbBitmap}")
            this.foto = foto
            b.holder = this
            b.foto = foto
            if (foto.main) AnimUtil.showSlideDown(b.ivCheck, false)
            else AnimUtil.hideSlideUp(b.ivCheck, false)

            b.executePendingBindings()

        }

        fun onClickCancel() {
            MyAlert.showConfirm(activity, R.string.info_delete_db_data.getStr(),
                    ok = {
                        items.remove(foto)
                        notifyDataSetChanged()
                    },
                    cancel = {})


            // url ? dbPhoto : myPhoto
//            if (foto.url == null) {
//            items.remove(foto)
//            notifyDataSetChanged()
//            } else {
//                MyAlert.showConfirm (activity, null, R.string.info_delete_db_data.getStr(), isCancel = true, isCancelTouch = true,
//                        ok = {
//                            PhotoUtil.deletePhoto(foto)
//                            items.remove(foto)
//                            notifyDataSetChanged()
//                        },
//                        cancel = {})
//            }

        }

        fun onLongClickPhoto(): Boolean {
            onLong(foto)
//            OnFoto.resetMainFoto(items,foto)
//            notifyDataSetChanged()
            return true
        }

    }

}