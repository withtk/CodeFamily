package com.tkip.mycode.nav.store.tab_info

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.activity.result.ActivityResultLauncher
import androidx.activity.result.contract.ActivityResultContracts
import androidx.appcompat.app.AppCompatActivity
import com.tkip.mycode.R
import com.tkip.mycode.admin.versionGteQ
import com.tkip.mycode.databinding.FragmentStoreInfoBinding
import com.tkip.mycode.funs.common.lllogD
import com.tkip.mycode.funs.common.lllogI
import com.tkip.mycode.funs.common.tranPair
import com.tkip.mycode.funs.custom.expandable.ViewExpandableCard
import com.tkip.mycode.funs.custom.text.ViewTvStatus
import com.tkip.mycode.init.*
import com.tkip.mycode.init.base.getStr
import com.tkip.mycode.init.base.getViewModel
import com.tkip.mycode.init.my_bind.BindingFragment
import com.tkip.mycode.model.board.Board
import com.tkip.mycode.model.board.OnBoard
import com.tkip.mycode.model.count.CountType
import com.tkip.mycode.model.count.CountViewModel
import com.tkip.mycode.model.media.setupMediaButton
import com.tkip.mycode.model.my_enum.Status
import com.tkip.mycode.model.my_enum.getStatus
import com.tkip.mycode.model.peer.isAuthor
import com.tkip.mycode.model.store.Store
import com.tkip.mycode.nav.board.tab_first.AddBoardActivity
import com.tkip.mycode.nav.store.StoreActivity
import com.tkip.mycode.util.lib.map.OnMap
import com.tkip.mycode.util.my_view.ViewTagView
import com.tkip.mycode.util.my_view.ViewUtil
import com.tkip.mycode.util.tools.anim.*
import com.tkip.mycode.util.tools.etc.OnMyClick
import com.tkip.mycode.util.tools.fire.Firee

class StoreInfoFrag : BindingFragment<FragmentStoreInfoBinding>() {
    override fun getLayoutResId(): Int = R.layout.fragment_store_info

    private lateinit var vmCount: CountViewModel

    //    private val vmCount by lazy { getViewModel<CountViewModel>() }
    private val store: Store by lazy { (activity as StoreActivity).store }
    //    private val store: Store by lazy { arguments?.getParcelable(PUT_STORE) ?: Store() }

    companion object {
        @JvmStatic
        fun newInstance(argStore: Store) = StoreInfoFrag().apply { arguments = Bundle().apply { putParcelable(PUT_STORE, argStore) } }
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        vmCount = getViewModel()
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        lllogD("mContextTest StoreInfoFrag onActivityCreated mContext:$mContext")

        initView()
        initData()

    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        lllogI("StoreInfoFrag onActivityResult onActivityResult requestCode :$requestCode  resultCode :$resultCode  data boardKey :${data?.getStringExtra(PUT_BOARD_KEY)} ")

        data?.getStringExtra(PUT_BOARD_KEY)?.let { boardKey ->
            store.boardKey = boardKey
            Firee.addStore(store, success = {
                resetCsButton()
            }, fail = {})   // store 수정하여 업로드
        }
    }

    private fun initView() {
        b.fragment = this
        b.store = store

        /*** 스토어 상태표시 : Normal 아닐때만  for Author */
        val status = store.statusNo.getStatus()
        if (status.no != Status.NORMAL.no) b.lineCover.addView(ViewTvStatus(mContext, status.msg), 0)

        /*** 스토어 호스트 닉네임 */
        Firee.getPeer(store.authorUID, { b.peer = it }, {})  // 마스터 정보넣기.

        lllogD("StoreInfoFrag boardKey ${store.boardKey}  ${b.btnCs.visibility}")

        /***  CS보드 만들기 버튼 셋팅  */
        if (store.boardKey == null) {
            b.btnCs.gone()  // cs보드 이동버튼
            if (store.authorUID.isAuthor(null, null)) b.lineCs.visi()
        }

    }

    private fun initData() {


        /**   Count CheckBox 세팅    */
        vmCount.initCount(activity as AppCompatActivity, store, arrayOf(CountType.BOOK), null, null, b.cbBookmark, null)

        store.comment?.let {
            val card = ViewExpandableCard(mContext, R.string.add_store_comment.getStr(), it, true, onClick = {})
            b.line.addView(card)
        }

        store.contact?.let {
            val card = ViewExpandableCard(mContext, R.string.add_store_detail.getStr(), it, true, onClick = {})
            b.line.addView(card)
            /*** 미디어 기입 */
            store.mediaMap.setupMediaButton(mContext, card.b.lineAttach)
        }

        store.weekdayText?.let { b.line.addView(ViewExpandableCard(mContext, R.string.store_working_time.getStr(), it, true, onClick = {})) }

        store.lat?.let {
            val card = ViewExpandableCard(mContext, R.string.name_address_map.getStr(), null, true, onClick = {})
            b.line.addView(card)

            /*** 지도 add & set */
            if (store.address != null || store.lat != null) {
                ViewUtil.addLinearMapShow(mContext, card.b.lineAttach, store.address, OnMap.createLatlng(store.lat, store.lng))
            }
        }

        /*** 검색 태그 */
        if (versionGteQ) store.tagList?.let { list ->
            val card = ViewExpandableCard(mContext, R.string.name_tag_list.getStr(), null, false, onClick = { cardB ->
                if (cardB.lineAttach.childCount < 1) cardB.lineAttach.addView(ViewTagView(mContext, list))
            })
            b.line.addView(card)
        }

    }

    fun onClick(btn: View, linear: View) {
        AnimUtil.rotateArrow(btn)
        AnimUtil.toggleUpDown(linear, true)
    }

    fun onClickBookmark(v: View) {
        OnMyClick.isDoubleClick()
        vmCount.updateCount(store, CountType.BOOK, v)
    }

//    fun onClickPush(v: View) {
//        OnMyClick.isDoubleClick()
//        vmCount.updateCount(store, CountType.BOOK, v)
//    }

    fun onClickCsBoard(v: View) {
        OnMyClick.setDisableAWhileBTN(v)
        store.boardKey?.let { OnBoard.gotoBoardActivity(mContext, false, null, it, arrayOf(b.btnCs.tranPair(R.string.tran_toolbar))) }
    }

    fun onClickCreateCsBoard(v: View) {
        OnMyClick.setDisableAWhileBTN(v)
        val intent = Intent(mContext, AddBoardActivity::class.java).apply {
            putExtra(PUT_BOARD, Board(store))
        }
        startActivityForResult(intent, REQ_DATA)
//        OnBoard.gotoAddBoardActivity(mContext, ACTION_NEW, Board(store), null)
    }


    fun resetCsButton() {
        b.btnCs.showSlideUp(false)
        b.lineCs.goneSlideDown(false)
    }

}
