package com.tkip.mycode.nav.store.tab_menu

import android.os.Bundle
import com.tkip.mycode.R
import com.tkip.mycode.databinding.FragmentStoreMenuBinding
import com.tkip.mycode.funs.common.lllogD
import com.tkip.mycode.init.ACTION_PREVIEW
import com.tkip.mycode.init.base.Cons
import com.tkip.mycode.init.my_bind.BindingFragment
import com.tkip.mycode.model.store.Store
import com.tkip.mycode.nav.store.store_menu.CustomStoreMenuList


class StoreMenuFrag : BindingFragment<FragmentStoreMenuBinding>() {
    override fun getLayoutResId(): Int = R.layout.fragment_store_menu

    private val store: Store by lazy { arguments?.getParcelable(Cons.PUT_OBJECT) ?: Store() }
    private val actionType: Int by lazy { arguments?.getInt(Cons.PUT_INT) ?: ACTION_PREVIEW }
    private lateinit var customStoreMenuList: CustomStoreMenuList

    companion object {
        @JvmStatic
        fun newInstance(argStore: Store, argActionType: Int) =
                StoreMenuFrag().apply {
                    arguments = Bundle().apply {
                        putParcelable(Cons.PUT_OBJECT, argStore)
                        putInt(Cons.PUT_INT, argActionType)
                    }
                }
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        lllogD("mContextTest StoreMenuFrag onActivityCreated mContext:$mContext")
        initView()
    }

    private fun initView() {
        b.fragment = this
        b.store = store

        /**         * StoreMenu list 붙이기         */
        customStoreMenuList = CustomStoreMenuList(mContext, store.key, StoreMenuAdapter.rvNormal, onItem = {})
        customStoreMenuList.getEsDataList(0)
        b.frame.addView(customStoreMenuList)

    }








}
