package com.tkip.mycode.admin.menu

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.LinearLayout
import androidx.recyclerview.widget.RecyclerView
import com.tkip.mycode.databinding.HolderAdminMenuBinding
import com.tkip.mycode.funs.custom.menu.ViewMenuBtnSquare

class AdminMenuAdapter(  val items: ArrayList<AMenu>) : RecyclerView.Adapter<AdminMenuAdapter.AdminMenuViewHolder>() {

    private lateinit var context: Context
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): AdminMenuViewHolder {
        context = parent.context
        val binding = HolderAdminMenuBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return AdminMenuViewHolder(binding)
    }

    override fun getItemId(position: Int): Long {
        return items[position].hashCode().toLong()
    }

    override fun onBindViewHolder(h: AdminMenuViewHolder, p: Int) {
        h.bind(items[p])
    }

    override fun getItemCount(): Int {
        return items.size
    }

    inner class AdminMenuViewHolder(val b: HolderAdminMenuBinding) : RecyclerView.ViewHolder(b.root) {

        lateinit var amenu: AMenu

        fun bind(amenu: AMenu) {
            this.amenu = amenu
//            b.holder = this
//            b.aMenu = aMenu

            val linearParams = LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT).apply { weight = 1f }
            b.line.addView(ViewMenuBtnSquare(context, amenu, null,onClick = { amenu.onClickMenu(context,null) }), linearParams)

//        ViewUtil.addViewMenuBtnSquare(context, b.line, amenu,
//                    onClick = {
//                        amenu.onClickMenu(context)
//                    })

            b.executePendingBindings()
        }

//        fun onClickCancel() {
//            notifyDataSetChanged()
//        }

//        fun onClickMenu() {
//            aMenu.onClick(activity)
//        }

    }

}