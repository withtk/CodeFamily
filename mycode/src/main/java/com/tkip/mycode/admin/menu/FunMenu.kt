package com.tkip.mycode.admin.menu

import android.content.Context
import android.content.Intent
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import com.tkip.mycode.R
import com.tkip.mycode.ad.OnBanner
import com.tkip.mycode.dialog.admin.UrgentNoticeDialog
import com.tkip.mycode.dialog.alert.MyAlert
import com.tkip.mycode.dialog.toast.TToast
import com.tkip.mycode.init.RV_MANAGE
import com.tkip.mycode.init.RV_MY_CONTENTS
import com.tkip.mycode.init.base.getDrawable
import com.tkip.mycode.init.base.getStr
import com.tkip.mycode.model.board.OnBoard
import com.tkip.mycode.model.cost.act.OnAct
import com.tkip.mycode.model.count.CountType
import com.tkip.mycode.model.count.OnCount
import com.tkip.mycode.model.grant.OnGrant
import com.tkip.mycode.model.mytag.MyTagListActivity
import com.tkip.mycode.model.peer.OnPeer
import com.tkip.mycode.model.peer.myuid
import com.tkip.mycode.model.ticket.OnBill
import com.tkip.mycode.model.warn.OnWarn
import com.tkip.mycode.nav.my_room.OnMyContents
import com.tkip.mycode.nav.settings.OnSettings


/******************************** admin menu in Navigation*****************************************/
enum class AMenu(val title: String, val desc: String?, private val iconId: Int) {

    /**     * for user   */
    SETTINGS(R.string.menu_title_settings.getStr(), null, R.drawable.ic_settings),                  // 설정
    SEARCH_TOTAL(R.string.menu_title_search_total.getStr(), null, R.drawable.ic_search),            // 검색
    MY_BOOKMARK(R.string.title_my_book_list.getStr(), null, R.drawable.ic_board),   // 북마크
    MY_PUSH(R.string.title_my_push_list.getStr(), null, R.drawable.ic_board),   // 북마크
    MY_CONTENTS(R.string.menu_title_contents.getStr(), null, R.drawable.ic_bookmark2),   // 내 컨텐츠
    MY_CREDIT_LIST(R.string.menu_title_my_credit_list.getStr(), null, R.drawable.ic_act),    // 내 포인트 내역 . 페이 내역. 캐럿 내역.
    MY_NOTIFICATION(R.string.menu_title_my_notification.getStr(), null, R.drawable.ic_notifications),

    CREDIT_MENU(R.string.menu_title_my_credit_menu.getStr(), null, R.drawable.ic_act),    // 크레딧 메뉴.
    CREDIT_PURCHASE(R.string.menu_purchase_credit.getStr(), R.string.menu_purchase_credit_desc.getStr(), R.drawable.ic_act),    // 크레딧 구매.
    BILL_LIST(R.string.menu_title_bills.getStr(), R.string.menu_desc_bills.getStr(), R.drawable.ic_act),    // 결제내역.
    PUSH_TIME(R.string.menu_title_push_time.getStr(), null, R.drawable.ic_notifications_off),  // 방해금지 설정.

    ADD_RECOMMENDER(R.string.menu_title_add_recommender.getStr(), null, R.drawable.ic_delete_forever),
    DELETE_PEER(R.string.menu_title_delete_peer.getStr(), null, R.drawable.ic_delete_forever),
    SIGN_OUT(R.string.menu_title_sign_out.getStr(), null, R.drawable.ic_sign_out),
    SIGN_OUT_CHANGE(R.string.btn_sign_out_change.getStr(), null, R.drawable.ic_sign_out),  // 구글ID로 전환.
    SUGGEST(R.string.menu_title_suggestion.getStr(), null, R.drawable.ic_act),     // 버그신고 및 어플 제안.
    QUERY(R.string.menu_title_query.getStr(), null, R.drawable.ic_act),     // 개별 문의.
    ALLIANCE(R.string.menu_title_investment.getStr(), null, R.drawable.ic_sign_out),   // 제휴문의.
    APP_INFO(R.string.menu_title_app_information.getStr(), null, R.drawable.ic_notifications),   // 어플 정보(version,
    TERMS_AND_CONDITIONS(R.string.menu_terms_of_service.getStr(), null, R.drawable.ic_act),   // 약관
    PRIVACY_POLICY(R.string.menu_privacy_policy.getStr(), null, R.drawable.ic_sign_out),   // 개인정보 처리방침
    INFO_LIBRARY(R.string.menu_library.getStr(), null, R.drawable.ic_sign_out),   // 개인정보 처리방침


    /**     * context menu     */
    INFO(R.string.title_info.getStr(), null, R.drawable.ic_person),          // 안내
    ADD(R.string.title_add.getStr(), null, R.drawable.ic_person),      // 새로 올리기
    UPDATE(R.string.title_update.getStr(), null, R.drawable.ic_person),      // 수정하기
    COPY(R.string.title_copy.getStr(), null, R.drawable.ic_person),          // 복사하기
    PUSH(R.string.title_push.getStr(), null, R.drawable.ic_person),          // 알림설정
    TOSS(R.string.title_toss.getStr(), null, R.drawable.ic_person),          // 전달하기
    REREPLY(R.string.title_rereply.getStr(), null, R.drawable.ic_person),    // 답변하기
    WARN(R.string.title_warn.getStr(), null, R.drawable.ic_person),          // 신고하기
    ADOPTION(R.string.title_adoption.getStr(), null, R.drawable.ic_person),  // 채택하기
    DELETION(R.string.title_delete.getStr(), null, R.drawable.ic_person),    // 삭제하기
    DEEP_DEL(R.string.title_deep_delete.getStr(), null, R.drawable.ic_person),    // 딥삭제
    TRANSACTION_COMPLETED(R.string.title_transaction_completed.getStr(), null, R.drawable.ic_person),    // 거래완료처리
    ADVERTISEMENT(R.string.menu_title_advertisement.getStr(), null, R.drawable.ic_person),    // 광고하기
    STATUS(R.string.menu_title_status.getStr(), null, R.drawable.ic_person),    // 게시물 상태변경 for admin
    STORE_MENU(R.string.menu_title_store_menu.getStr(), null, R.drawable.ic_person),    // 게시물 상태변경 for admin
    BLOCK(R.string.menu_title_block.getStr(), null, R.drawable.ic_person),    // 차단 : 채팅 차단 or 보드 업로드 차단.


    /**     * for admin   */
    SETTINGS_ADMIN(R.string.admin_title_settings_admin.getStr(), null, R.drawable.ic_settings),  // 어플관리
    PEER_LIST(R.string.admin_title_peer_list.getStr(), null, R.drawable.ic_person),
    MODEL_LIST(R.string.admin_title_model_list.getStr(), null, R.drawable.ic_store),
    STORE_LIST(R.string.admin_title_store_list.getStr(), null, R.drawable.ic_store),
    BOARD_LIST(R.string.admin_title_board_list.getStr(), null, R.drawable.ic_person),
    WARN_LIST(R.string.admin_title_warn_list.getStr(), null, R.drawable.ic_person),
    GRANT_LIST(R.string.admin_title_grant_list.getStr(), null, R.drawable.ic_person),
    BANNER_LIST(R.string.admin_title_banner_list.getStr(), null, R.drawable.ic_person),
    PENALTY_LIST(R.string.admin_title_penalty_list.getStr(), null, R.drawable.ic_person),
    SUGGEST_LIST(R.string.admin_title_suggest_list.getStr(), null, R.drawable.ic_person),
    TAG_LIST(R.string.admin_title_tag_list.getStr(), null, R.drawable.ic_person),
    HIT_ACT(R.string.admin_title_hit_act.getStr(), null, R.drawable.ic_person),   // act 올리기.
    NOTICE(R.string.admin_title_send_notice.getStr(), null, R.drawable.ic_person),    // 공지 올리기
    REFUND(R.string.admin_title_refund.getStr(), null, R.drawable.ic_person),


    /**     * for board master   */
    NOTHING(R.string.admin_title_nothing.getStr(), null, R.drawable.ic_person)
    ;  // 반드시 ;을 넣어줘야함.

    fun getIcon() = iconId.getDrawable()
    fun onClickMenu(context: Context, arrPair: Array<androidx.core.util.Pair<View, String>>?) {
        when (this) {
            SETTINGS -> OnSettings.gotoSettingsActivity(context, arrPair)
            SETTINGS_ADMIN -> OnSettings.gotoSettingsAdminActivity(context, arrPair)
            MY_BOOKMARK -> OnCount.gotoMyCountActivity(context, false, null, CountType.BOOK, arrPair)
            MY_PUSH -> OnCount.gotoMyCountActivity(context, false, null, CountType.PUSH, arrPair)
            MY_CONTENTS -> OnMyContents.gotoMyContentsActivity(context, myuid(), RV_MY_CONTENTS, arrPair)
            ADVERTISEMENT -> MyAlert.showBidMenu(context, null)

            PEER_LIST -> OnPeer.gotoAdminPeerActivity(context, arrPair)
//            STORE_LIST -> OnStore.gotoStoreListActivity(context, arrPair)
            MODEL_LIST -> OnMyContents.gotoMyContentsActivity(context, null, RV_MANAGE, arrPair)

            BOARD_LIST -> OnBoard.gotoBoardListActivity(context, arrPair)
            WARN_LIST -> OnWarn.gotoWarnListActivity(context, true, arrPair)
            GRANT_LIST -> OnGrant.gotoGrantListActivity(context, arrPair)
            BANNER_LIST -> OnBanner.gotoMyBannerActivity(context, null, arrPair)
            PENALTY_LIST -> OnWarn.gotoWarnListActivity(context, false, arrPair)
            TAG_LIST -> context.startActivity(Intent(context, MyTagListActivity::class.java))
            BILL_LIST -> OnBill.gotoBillListActivity(context, null)
            HIT_ACT -> OnAct.startActs(context)
            NOTICE -> UrgentNoticeDialog.newInstance().show((context as AppCompatActivity).supportFragmentManager, "dialog")
            else -> TToast.showNormal(R.string.info_not_working_amenu)
        }
    }
}


//
//enum class MENULIST(val arr: Array<AMenu>) {
//
//    NOTHING(arrayOf(AMenu.COPY)),                // 기본값.
//    /**     * context menu  */
//    REPLY_MY(arrayOf(AMenu.COPY, AMenu.REREPLY, AMenu.DELETION)),                // 내 댓글 롱클릭 메뉴
//    REPLY_USER(arrayOf(AMenu.COPY, AMenu.REREPLY, AMenu.WARN)),                    // 다른 댓글 롱클릭 메뉴
//    REPLY_USER_POST_ADOPTION(arrayOf(AMenu.COPY, AMenu.REREPLY, AMenu.WARN, AMenu.ADOPTION)),   // 다른 댓글 롱클릭 메뉴 : 답변 채택하기 전.
//    REPLY_ADMIN(arrayOf(AMenu.COPY, AMenu.REREPLY, AMenu.WARN, AMenu.DELETION, AMenu.STATUS)), // 어드민이 댓글 롱클릭.
//
//    LOBBY(arrayOf(AMenu.INFO, AMenu.DELETION, AMenu.BLOCK)),
//    LOBBY_ADMIN(arrayOf(AMenu.INFO, AMenu.DELETION, AMenu.STATUS)),
//    CHAT_MY(arrayOf(AMenu.COPY, AMenu.DELETION)),                // 내 채팅 롱클릭 메뉴
//    CHAT_USER(arrayOf(AMenu.COPY)),                    // 다른 채팅 롱클릭 메뉴
//
//
//    BOARD_AUTHOR(arrayOf(AMenu.INFO, AMenu.UPDATE)),         // 보드 주인
//    BOARD_USER(arrayOf(AMenu.INFO, AMenu.WARN)),
//    BOARD_ADMIN(arrayOf(AMenu.INFO, AMenu.UPDATE, AMenu.WARN, AMenu.STATUS)),
//    BOARD_CS_AUTHOR(arrayOf(AMenu.INFO, AMenu.UPDATE)),        // 보드 주인
//
//
//    POST_MASTER(arrayOf(AMenu.UPDATE, AMenu.WARN, AMenu.STATUS)),        // 보드 주인
//    POST_AUTHOR(arrayOf(AMenu.UPDATE, AMenu.DELETION)),        // 글쓴이
//    POST_USER(arrayOf(AMenu.WARN)),
//    POST_ADMIN(arrayOf(AMenu.UPDATE, AMenu.DELETION, AMenu.WARN, AMenu.STATUS)),
//    POST_CS_MASTER(arrayOf(AMenu.UPDATE, AMenu.WARN, AMenu.STATUS)),     // 보드 주인
//    POST_CS_AUTHOR(arrayOf(AMenu.UPDATE, AMenu.DELETION)),     // 글쓴이
//    POST_CS_USER(arrayOf(AMenu.WARN)),
//
//
//    FLEA_AUTHOR(arrayOf(AMenu.UPDATE, AMenu.DELETION)),
//    FLEA_USER(arrayOf(AMenu.WARN)),
//    FLEA_ADMIN(arrayOf(AMenu.UPDATE, AMenu.DELETION, AMenu.WARN, AMenu.STATUS)),
//
//
//    ROOM_AUTHOR(arrayOf(AMenu.UPDATE, AMenu.DELETION)),
//    ROOM_USER(arrayOf(AMenu.WARN)),
//    ROOM_ADMIN(arrayOf(AMenu.UPDATE, AMenu.DELETION, AMenu.WARN, AMenu.STATUS)),
//
//
//    // todo: 북마크와 복사하기.
//    STORE_AUTHOR(arrayOf(AMenu.STORE_MENU, AMenu.UPDATE, AMenu.DELETION)),
//    STORE_USER(arrayOf(AMenu.WARN)),
//    STORE_ADMIN(arrayOf(AMenu.STORE_MENU, AMenu.UPDATE, AMenu.DELETION, AMenu.WARN, AMenu.STATUS)),
//
//    /**     * menu  */
//    NAV_DEFAULT_USER(arrayOf(AMenu.MY_BOOKMARK, AMenu.MY_PUSH, AMenu.MY_CONTENTS, AMenu.SETTINGS, AMenu.ADVERTISEMENT))  // myroom 기본 메뉴.
//
//    ;  // 반드시 ;을 넣어줘야함.
//}


/*************************************************************************************/
/********************  ***********************************************************/
/*************************************************************************************/


val ARR_NOTHING by lazy { arrayOf(AMenu.COPY) }             // 기본값.

/**     * context menu  */
val ARR_REPLY_AUTHOR by lazy { arrayOf(AMenu.COPY, AMenu.REREPLY, AMenu.DELETION) }                // 내 댓글 롱클릭 메뉴
val ARR_REPLY_MY by lazy { arrayOf(AMenu.COPY, AMenu.REREPLY, AMenu.DELETION) }                // 내 댓글 롱클릭 메뉴
val ARR_REPLY_USER by lazy { arrayOf(AMenu.COPY, AMenu.REREPLY, AMenu.WARN) }                    // 다른 댓글 롱클릭 메뉴
val ARR_REPLY_USER_POST_ADOPTION by lazy { arrayOf(AMenu.COPY, AMenu.REREPLY, AMenu.WARN, AMenu.ADOPTION) }   // 다른 댓글 롱클릭 메뉴 : 답변 채택하기 전.
val ARR_REPLY_ADMIN by lazy { arrayOf(AMenu.COPY, AMenu.REREPLY, AMenu.WARN, AMenu.DELETION, AMenu.STATUS) } // 어드민이 댓글 롱클릭.

val ARR_LOBBY by lazy { arrayOf(AMenu.INFO, AMenu.DELETION, AMenu.BLOCK) }
val ARR_LOBBY_ADMIN by lazy { arrayOf(AMenu.INFO, AMenu.DELETION, AMenu.STATUS) }
val ARR_CHAT_MY by lazy { arrayOf(AMenu.COPY, AMenu.DELETION) }                // 내 채팅 롱클릭 메뉴
val ARR_CHAT_USER by lazy { arrayOf(AMenu.COPY) }                    // 다른 채팅 롱클릭 메뉴


val ARR_BOARD_AUTHOR by lazy { arrayOf( AMenu.UPDATE, AMenu.TOSS, AMenu.INFO) }         // 보드 주인
val ARR_BOARD_USER by lazy { arrayOf( AMenu.TOSS, AMenu.WARN, AMenu.INFO) }
val ARR_BOARD_ADMIN by lazy { arrayOf( AMenu.UPDATE, AMenu.TOSS, AMenu.WARN, AMenu.DEEP_DEL, AMenu.INFO, AMenu.STATUS) }
val ARR_BOARD_FLEA_ROOM_ADMIN by lazy { arrayOf(AMenu.PUSH, AMenu.UPDATE, AMenu.TOSS, AMenu.INFO, AMenu.STATUS) }
val ARR_BOARD_FLEA_ROOM_USER by lazy { arrayOf(AMenu.INFO) }
val ARR_BOARD_CS_AUTHOR by lazy { arrayOf(AMenu.UPDATE, AMenu.TOSS, AMenu.INFO) }        // 보드 주인


val ARR_POST_MASTER by lazy { arrayOf(AMenu.UPDATE, AMenu.WARN, AMenu.STATUS) }        // 보드 주인
val ARR_POST_AUTHOR by lazy { arrayOf(AMenu.UPDATE, AMenu.DELETION) }        // 글쓴이
val ARR_POST_USER by lazy { arrayOf(AMenu.WARN) }
val ARR_POST_ADMIN by lazy { arrayOf(AMenu.UPDATE, AMenu.DELETION, AMenu.WARN, AMenu.DEEP_DEL, AMenu.STATUS) }
val ARR_POST_CS_MASTER by lazy { arrayOf(AMenu.UPDATE, AMenu.WARN, AMenu.STATUS) }     // 보드 주인
val ARR_POST_CS_AUTHOR by lazy { arrayOf(AMenu.UPDATE, AMenu.DELETION) }     // 글쓴이
val ARR_POST_CS_USER by lazy { arrayOf(AMenu.WARN) }


val ARR_FLEA_AUTHOR by lazy { arrayOf(AMenu.UPDATE, AMenu.DELETION, AMenu.TRANSACTION_COMPLETED) }
val ARR_FLEA_USER by lazy { arrayOf(AMenu.WARN) }
val ARR_FLEA_ADMIN by lazy { arrayOf(AMenu.UPDATE, AMenu.WARN, AMenu.DEEP_DEL, AMenu.STATUS) }


val ARR_ROOM_AUTHOR by lazy { arrayOf(AMenu.UPDATE, AMenu.DELETION, AMenu.TRANSACTION_COMPLETED) }
val ARR_ROOM_USER by lazy { arrayOf(AMenu.WARN) }
val ARR_ROOM_ADMIN by lazy { arrayOf(AMenu.UPDATE, AMenu.WARN, AMenu.DEEP_DEL, AMenu.STATUS) }


val ARR_STORE_AUTHOR by lazy { arrayOf(AMenu.STORE_MENU, AMenu.UPDATE, AMenu.TOSS, AMenu.DELETION) }
val ARR_STORE_USER by lazy { arrayOf(AMenu.TOSS, AMenu.WARN) }
val ARR_STORE_ADMIN by lazy { arrayOf(AMenu.STORE_MENU, AMenu.UPDATE, AMenu.TOSS, AMenu.WARN, AMenu.DEEP_DEL, AMenu.STATUS) }

/**     * menu  */
val ARR_NAV_DEFAULT_USER by lazy { arrayOf(AMenu.MY_BOOKMARK, AMenu.MY_PUSH, AMenu.MY_CONTENTS, AMenu.SETTINGS, AMenu.ADVERTISEMENT) }  // myroom 기본 메뉴.


val ARR_BILL_ADMIN by lazy { arrayOf(AMenu.REFUND, AMenu.DEEP_DEL) }  // 결제내역 롱클릭
