package com.tkip.mycode.admin

import android.app.Activity
import android.os.Build
import com.tkip.mycode.R
import com.tkip.mycode.dialog.alert.MyAlert
import com.tkip.mycode.dialog.toast.TOAST_NORMAL
import com.tkip.mycode.dialog.toast.checkAndToast
import com.tkip.mycode.dialog.toast.toastWarning
import com.tkip.mycode.funs.common.OnBasic
import com.tkip.mycode.funs.common.lllogD
import com.tkip.mycode.init.base.Sred
import com.tkip.mycode.init.base.getStr
import com.tkip.mycode.init.part.CodeApp
import com.tkip.mycode.init.sredInt
import com.tkip.mycode.init.sredPutInt
import com.tkip.mycode.model.manage.SystemMan
import com.tkip.mycode.model.peer.isAdmin
import com.tkip.mycode.util.tools.fire.Firee

/******************************** penalty Date *****************************************/


var versionGteQ = Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q
var versionGteOreo = Build.VERSION.SDK_INT >= Build.VERSION_CODES.O


var systemMan: SystemMan? = null


/** * 실시간 받아온 거 셋팅 */
fun resetSystemMan(dbData: SystemMan?) {
    dbData?.let { sys ->
        systemMan = sys
        checkNotice()
    }
}

/** * 스위치 체크  */
fun notAllowAdAll() = (systemMan?.noAdAll == true).checkAndToast(TOAST_NORMAL, R.string.alert_fixing_all)
fun notAllowAdMain() = (systemMan?.noAdMain  == true).checkAndToast(TOAST_NORMAL, R.string.alert_fixing_now)
fun notAllowAdMenu() = (systemMan?.noAdMenu  == true).checkAndToast(TOAST_NORMAL, R.string.alert_fixing_now)
fun notAllowAdTop() = (systemMan?.noAdTop  == true).checkAndToast(TOAST_NORMAL, R.string.alert_fixing_now)



/** * notice 변경확인 : BaseActivity.onResume에서 항상 체크
 * 문제 : noti로 이동할 때 mainActi를 안 거치는 문제. */
fun checkNotice() {
    if (systemMan == null) {
        lllogD("FunSystem checkNotice systemMan:$systemMan")
        Firee.getSystemMan(success = { it?.let { sys -> showUrgentNotice(sys) } }, fail = {})
    } else {
        showUrgentNotice(systemMan!!)
    }
}

/** * 긴급공지 날리기 */
//fun showNotice(context: Context, title: String?, description: String?, okText: String?, isCancel: Boolean, isCancelTouch: Boolean, ok: () -> Unit, cancel: (() -> Unit)?) {
private fun showUrgentNotice(systemMan: SystemMan) {

    CodeApp.curActivity?.let { activity ->
        if (systemMan.noticeShow) {
            if (Sred.SYSTEM_NOTICE_NO.sredInt(0) < systemMan.noticeNo) {
                if (systemMan.systemOn)
                /***  단순히 공지만 보여줄 때  */
                    MyAlert.showNotice(activity, systemMan, R.string.btn_never_show_again_urgent_notice.getStr(), ok = { Sred.SYSTEM_NOTICE_NO.sredPutInt(systemMan.noticeNo) }, cancel = null)  // noticeNo 저장하기.
                else
                /**     *  강제 종료시킬 떄     */
                    if (isAdmin()) MyAlert.showNotice(activity, systemMan, R.string.btn_shot_out.getStr(), ok = { OnBasic.finishAllActivity(   )    }, cancel = {})  // 어플 종료 (어드민은 취소 가능)
                    else MyAlert.showNotice(activity, systemMan, R.string.btn_shot_out.getStr(), ok = { OnBasic.finishAllActivity(   ) }, cancel = null)  // 어플 종료
            }
        }

    } ?: toastWarning(R.string.info_notice_force_finish)


}
