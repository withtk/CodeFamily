package com.tkip.mycode.admin.menu

import com.tkip.mycode.init.FLEA_BOARD_KEY
import com.tkip.mycode.init.ROOM_BOARD_KEY
import com.tkip.mycode.model.board.Board
import com.tkip.mycode.model.flea.Flea
import com.tkip.mycode.model.peer.OnPeer
import com.tkip.mycode.model.peer.OnPeer.Companion.myUid
import com.tkip.mycode.model.peer.isCsBoard
import com.tkip.mycode.model.peer.isNotAdmin
import com.tkip.mycode.model.peer.myuid
import com.tkip.mycode.model.post.Post
import com.tkip.mycode.model.room.Room
import com.tkip.mycode.model.store.Store

class OnMenu {
    companion object {


        /**        * Admin Menu 설정     in drawer    */
//        fun setAdminMenuInDrawer(inMenu: NavMenuBinding) {
//            if (OnPeer.isAdmin(false)) {
//                val adapterAdminMenu = AdminMenuAdapter(
//                        arrayListOf<AMenu>().apply {
//                            when (OnPeer.peer.type) {
//                                PeerType.ADMIN_1.no, PeerType.ADMIN_2.no -> {
//                                    add(AMenu.STORE_LIST)
//                                    add(AMenu.WARN_LIST)
//                                    add(AMenu.NOTICE)
//                                }
//                                PeerType.ADMIN_SUPERVISOR.no -> {
//                                    add(AMenu.PEER_LIST)
//                                    add(AMenu.STORE_LIST)
//                                    add(AMenu.WARN_LIST)
//                                    add(AMenu.NOTICE)
//                                }
//                                else -> {
//                                }
//                            }
//                        })
//                adapterAdminMenu.hasStableIds()
//                inMenu.rvTop.adapter = adapterAdminMenu
//                inMenu.rvTop.setHasFixedSize(true)
//            }
//        }


        private fun isAuthor(authorUID: String) = myUid() == authorUID

        /**
         * context Menu list
         */
        fun getMENULIST(any: Any): Array<AMenu> {

            val isAdmin = OnPeer.isAdmin(false)

            return when (any) {
                is Post ->
                    when {
                        isAdmin -> ARR_POST_ADMIN
                        any.boardMasterUid == myuid() -> {
                            if (any.isCsBoard()) ARR_POST_CS_MASTER
                            else ARR_POST_MASTER
                        }
                        isAuthor(any.authorUID) -> {
                            if (any.isCsBoard()) ARR_POST_CS_AUTHOR
                            else ARR_POST_AUTHOR
                        }
                        else -> {
                            if (any.isCsBoard()) ARR_POST_CS_USER
                            else ARR_POST_USER
                        }
                    }
                is Flea ->
                    when {
                        isAdmin -> ARR_FLEA_ADMIN
                        isAuthor(any.authorUID) -> ARR_FLEA_AUTHOR
                        else -> ARR_FLEA_USER
                    }
                is Room ->
                    when {
                        isAdmin -> ARR_ROOM_ADMIN
                        isAuthor(any.authorUID) -> ARR_ROOM_AUTHOR
                        else -> ARR_ROOM_USER
                    }
                is Store ->
                    when {
                        isAdmin -> ARR_STORE_ADMIN
                        isAuthor(any.authorUID) -> ARR_STORE_AUTHOR
                        else -> ARR_STORE_USER
                    }
                is Board ->
                    /*** FLEA, ROOM은 어디민만 메뉴 생성. */
                    if (any.key == FLEA_BOARD_KEY || any.key == ROOM_BOARD_KEY)
                        when {
                            isAdmin -> ARR_BOARD_FLEA_ROOM_ADMIN
                            else -> ARR_BOARD_FLEA_ROOM_USER
                        }
                    else when {
                        isAdmin -> ARR_BOARD_ADMIN
                        isAuthor(any.authorUID) -> {
                            if (any.isCsBoard()) ARR_BOARD_CS_AUTHOR
                            else ARR_BOARD_AUTHOR
                        }
                        else -> ARR_BOARD_USER
                    }
                else -> ARR_NOTHING
            }
        }

        //todo : 보드에 대한 분기도 각각 모두 넣으려면 너무 무식한 짓인거 같은데....


        /**
         * context Menu list : post, flea, room
         */
//        fun getPostMenuList(any: Any): Array<AMenu> {
//            val isAdmin = OnPeer.isAdmin(false)
//            return when (any) {
//                is Post ->
//                    when {
//                        isAdmin -> ARR_POST_ADMIN
//                        any.boardMasterUid == myuid()->{
//                            if (any.boardTypeNo.isCsBoard()) ARR_POST_CS_MASTER
//                            else ARR_POST_MASTER
//                        }
//                        isAuthor(any.authorUID) -> {
//                            if (any.boardTypeNo.isCsBoard()) ARR_POST_CS_AUTHOR
//                            else ARR_POST_AUTHOR
//                        }
//                        else -> {
//                            if (any.boardTypeNo.isCsBoard()) ARR_POST_CS_USER
//                            else ARR_POST_USER
//                        }
//                    }
//
//                else -> ARR_NOTHING
//            }
//        }


    }
}