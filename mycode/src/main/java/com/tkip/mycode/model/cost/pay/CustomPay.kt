package com.tkip.mycode.model.cost.pay

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.widget.LinearLayout
import androidx.databinding.DataBindingUtil
import com.tkip.mycode.R
import com.tkip.mycode.databinding.CustomPayBinding
import com.tkip.mycode.init.base.getStr
import com.tkip.mycode.model.cost.credit.OnCredit
import com.tkip.mycode.model.my_enum.PayType
import com.tkip.mycode.model.ticket.OnTicket
import com.tkip.mycode.util.tools.anim.gone
import com.tkip.mycode.util.tools.anim.showFadeIn
import com.tkip.mycode.util.tools.anim.visi
import com.tkip.mycode.util.tools.etc.OnMyClick


/**
 * 어플내의 크레딧 결제 명세서.
 */
class CustomPay : LinearLayout {
    lateinit var b: CustomPayBinding
    lateinit var onOk: () -> Unit
    lateinit var onCancel: () -> Unit
    private lateinit var customMyBcrd: CustomMyPayType
    private lateinit var customMyCrd: CustomMyPayType
    var sum: Int = 0  // 결제할 크레딧

    constructor(context: Context) : super(context)
    constructor(context: Context, onOk: () -> Unit, onCancel: () -> Unit) : super(context) {
        b = DataBindingUtil.inflate(LayoutInflater.from(context), R.layout.custom_pay, this, true)
        b.custom = this
        this.onOk = onOk
        this.onCancel = onCancel


        /**         * 구매내역 셋팅         */
        PAY_ACT_TYPE_LIST.forEach {
            it.getCrdAmount()?.let { amount ->
                b.linePurchase.addView(CustomPayPurchaseList(context, it))
                sum += amount
            }
        }

        b.sum = sum

        /**         * 내 크레딧 셋팅         */
        customMyBcrd = CustomMyPayType(context, PayType.BONUS_CREDIT, null)
        customMyCrd = CustomMyPayType(context, PayType.CREDIT, null)
        b.lineMyPay.addView(customMyBcrd)
        b.lineMyPay.addView(customMyCrd)

        /**         * 결제 가능여부 셋팅         */
        OnCredit.checkAvailPayment(sum,
                onNotAvail = {
                    /**  모두 결제 불가능    */
                    b.tvMsg.text = R.string.query_not_enough_money.getStr()
                    b.btnOk.gone()
                    b.btnGotoCredit.visi()
                    b.lineFinal.showFadeIn(true)
                },
                onCreditOnly = { onClickCredit() },
                onBonusOnly = { onClickBonus() },
                onAvail = {
                    /**  모두 결제가능    */
                    b.lineSelect.showFadeIn(true)
                })

    }

    private fun afterSelected() {
        b.lineSelect.gone()
        b.lineFinal.showFadeIn(true)
    }

    /**     * 결제 타입 선택  :Bonus   */
    fun onClickBonus() {
        OnMyClick.setDisableAWhileBTN(b.btnBonus, b.btnCredit)
        afterSelected()
        customMyBcrd.setPrice(sum)
    }

    /**     * 결제 타입 선택  :Credit  */
    fun onClickCredit() {
        OnMyClick.setDisableAWhileBTN(b.btnBonus, b.btnCredit)
        afterSelected()
        customMyCrd.setPrice(sum)
    }


    fun onClickCancel(v: View) {
        OnMyClick.setDisableAWhileBTN(b.btnCancel, b.btnOk)
        onCancel()
    }

    /*** 결제하기 */
    fun onClickOk(v: View) {
        OnMyClick.setDisableAWhileBTN(b.btnCancel, b.btnOk)
        onOk()
    }

    fun onClickGotoCredit(v: View) {
        OnMyClick.setDisableAWhileBTN(v)
        onCancel()
        OnTicket.gotoPurchaseActivity(context)
    }


}