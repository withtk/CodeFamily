package com.tkip.mycode.model.mytag

import android.content.Context
import android.view.LayoutInflater
import android.widget.LinearLayout
import androidx.databinding.DataBindingUtil
import com.tkip.mycode.R
import com.tkip.mycode.databinding.CustomTagListBinding
import com.tkip.mycode.util.tools.anim.gone
import com.tkip.mycode.util.tools.anim.visi

/**
 * 검색 태그 리스트  in postActivity
 */
class CustomTagList : LinearLayout {
    lateinit var b: CustomTagListBinding

    constructor(context: Context) : super(context)
    constructor(context: Context, tagList: ArrayList<String>? ) : super(context) {
        b = DataBindingUtil.inflate(LayoutInflater.from(context), R.layout.custom_tag_list, this, true)

        if (tagList.isNullOrEmpty()) {
            b.line.gone()
        } else {
            b.line.visi()
            b.tagView.setAll(TAG_3, tagList)
        }

    }

}