package com.tkip.mycode.model.my_enum


/** * STATUS_LIST */
val STATUS_LIST_WARN by lazy { arrayListOf(Status.WARN_REQUESTED, Status.WARN_HANDLED) }
val STATUS_LIST_BASIC by lazy { arrayListOf(Status.BLIND_ADMIN, Status.DENIED, Status.DENIED_BAD_ERO, Status.DENIED_DUPLICATED,Status.DENIED_PROMOTION, Status.DENIED_NO_MATCH_LOCATION,  Status.SCREENING, Status.INSPECTING, Status.WRITING, Status.NORMAL) }
val STATUS_LIST_FLEA by lazy { arrayListOf(Status.BLIND_ADMIN, Status.DENIED, Status.TRANSACTION_COMPLETED, Status.DENIED_BAD_ERO, Status.DENIED_DUPLICATED,Status.DENIED_PROMOTION,  Status.DENIED_NO_MATCH_LOCATION, Status.SCREENING, Status.INSPECTING, Status.WRITING, Status.NORMAL) }
val STATUS_LIST_ROOM by lazy { arrayListOf(Status.BLIND_ADMIN, Status.DENIED, Status.TRANSACTION_COMPLETED, Status.DENIED_BAD_ERO, Status.DENIED_DUPLICATED,Status.DENIED_PROMOTION,Status.DENIED_NO_MATCH_LOCATION,   Status.SCREENING, Status.INSPECTING, Status.WRITING, Status.NORMAL) }
val STATUS_LIST_REPLY by lazy { arrayListOf(Status.BLIND_ADMIN, Status.DENIED,  Status.DENIED_BAD_ERO, Status.DENIED_DUPLICATED,Status.DENIED_PROMOTION,  Status.DENIED_NO_MATCH_LOCATION, Status.SCREENING, Status.INSPECTING, Status.WRITING, Status.NORMAL) }
val STATUS_LIST_BID by lazy { arrayListOf(Status.AD_DENIED, Status.BID_FAILED, Status.BID_BIDDING, Status.BID_SUCCESS) }
//val STATUS_LIST_BANNER by lazy { arrayListOf(Status.BLIND_ADMIN, Status.BANNER_DENIED, Status.BANNER_REQUESTED, Status.BANNER_GRANTED) }
val STATUS_LIST_TAG by lazy { arrayListOf(Status.INSPECTING, Status.NORMAL) }
val STATUS_LIST_TICKET by lazy { arrayListOf(Status.TICKET_DISABLED, Status.TICKET_ENABLED) }


val STATUS_LIST_BANNER by lazy { arrayListOf(Status.BLIND_ADMIN, Status.BANNER_DENIED, Status.BANNER_DENIED_NO_IMAGE, Status.BANNER_DENIED_SIZE_IMAGE, Status.BANNER_DENIED_BAD_WORD,  Status.BANNER_REQUESTED, Status.BANNER_GRANTED ) }

fun ArrayList<Status>.createStringlist(): ArrayList<String> =
        arrayListOf<String>().apply {
            this@createStringlist.forEach {
                 add(it.title)
            }
        }


/**
 * Status list 만들기
 */
//fun ArrayList<Status?>.setup(statusList: StatusLIST) =
//        this.apply {
//            clear()
//            add(null)    // 기본 안내멘트 추가.
//            statusList.list.forEach { add(it) }   // 모든 status 추가.
//
//        }

