package com.tkip.mycode.model.push

import com.tkip.mycode.R
import com.tkip.mycode.init.base.getStr
import com.tkip.mycode.util.system.notification.NotiChannel


/******************************** push 타입 *****************************************/
/**
 *
 */
enum class PushType(val no: Int, val notiCh: NotiChannel?, val title: String) {
    NOTICE(500,NotiChannel.DEFAULT, R.string.push_notice.getStr()),

    NEW_POST(1000,NotiChannel.POST, R.string.push_new_post.getStr()),
    NEW_REPLY(1100, NotiChannel.REPLY,R.string.push_new_reply.getStr()),
    NEW_LOBBY(1200,null, R.string.push_new_lobby.getStr()),
//    NEW_CHAT(1300,NotiChannel.CHAT, R.string.push_new_chat.getStr()),

    BID_FAIL(2000,NotiChannel.BID_FAIL, R.string.push_bid_fail.getStr()),
    BID_SUCCESS(2200,NotiChannel.BID_SUCCESS, R.string.push_bid_success.getStr()),

    PENALTY(3000,NotiChannel.PENALTY, R.string.push_penalty.getStr()),
    ;


}