package com.tkip.mycode.model.ticket

import android.content.Context
import android.content.Intent
import com.tkip.mycode.R
import com.tkip.mycode.dialog.progress.MyCircle
import com.tkip.mycode.dialog.toast.toastNormal
import com.tkip.mycode.model.my_enum.Status
import com.tkip.mycode.util.lib.TransitionHelper
import com.tkip.mycode.util.lib.photo.util.PhotoUtil
import com.tkip.mycode.util.tools.fire.Firee

/**
 * Hi~ Created by fabulous on 20-08-07
 * 구매 SKU
 */
class OnTicket {

    companion object {

        //        var skuIdMap:HashMap<String,Ticket>? = null
        val skuIdMap: HashMap<String, Ticket> by lazy { hashMapOf<String, Ticket>() }

        /**      * sku 맵에 모든 sku 채우기.     */
        fun resetSkuIDMap(list: ArrayList<Ticket>) {
            list.forEach { ticket ->
                if (ticket.statusNo == Status.TICKET_ENABLED.no) skuIdMap[ticket.skuId] = ticket
            }
        }

        fun addPhotoAndTicket(context: Context, ticket: Ticket, success: () -> Unit, fail: () -> Unit) {
            PhotoUtil.uploadUriSingle(context, ticket,
                    successUploading = { result ->
                        result?.let { foto ->
                            ticket.photoThumb = foto.thumbUrl
                            ticket.photoUrl = foto.url
                        }
                        Firee.addTicket(ticket,
                                success = {
                                    success()
                                    toastNormal(R.string.complete_upload)
                                    MyCircle.cancel()
                                }, fail = { fail() })
                    }, fail = { })
        }


        fun gotoPurchaseActivity(context: Context) {
            val intent = Intent(context, PurchaseActivity::class.java)
            TransitionHelper.startTran(context, intent, null)
        }
    }


}