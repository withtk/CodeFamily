package com.tkip.mycode.model.peer

import android.os.Parcel
import android.os.Parcelable
import com.google.firebase.auth.FirebaseUser
import com.tkip.mycode.init.base.Sred
import com.tkip.mycode.init.base.createGuestNick
import com.tkip.mycode.init.sredString
import com.tkip.mycode.model.my_enum.LoginFrom
import com.tkip.mycode.model.my_enum.PeerType
import com.tkip.mycode.util.tools.date.rightNow

/**
 *  <Peer 변경 잦은 정보 분기해야 하나?>  하지 말자. 보안규칙을 정교하게.
 */
data class Peer(

        var uid: String = "SixUIDthisIsDefault",
        var email: String? = null,

        var nick: String = "Anonymous",
        var dpName: String? = null,    // 이게 왜 필요한지 모르겠네. 필요할때 firebase user에서 받아와서 쓰면 되잖아..?
        var comment: String? = null,
        var face: String? = null,
        var faceThumb: String? = null,

        var fcmToken: String? = null,
        var type: Int = PeerType.BASIC_1.no,   // 유저 등급  todo: 이름변경 peerTypeNo.
        var loginFrom: Int = LoginFrom.ANONYMOUS.no,   // 가입 경로

        ///////////////////// fortune ///////////////////////////////////////////
        var cpay: Int = 0,  // codePay
        var pnt: Int = 0,
        var bcrd: Int = 0, // 광고결제시 보너스로 적립됨. 사용시 총 결제크레딧의 50%만 사용가능.
        var crd: Int = 0,
        var crt: Int = 0,      // 향후 이벤트용으로 사용 예정.

        ///////////////////// 유저 활동  ///////////////////////////////////////////
        var boardCount: Int = 0,
        var postCount: Int = 0,
        var roomCount: Int = 0,
        var fleaCount: Int = 0,
        var storeCount: Int = 0,

        ///////////////////// 유저 상태 ///////////////////////////////////////////
//        var activityPoint: Int = 0,     // 유저 활동에 따른 등급 포인트

        var penPoint: Int = 0,      // 전체 패널티.
        var penCount: Int = 0,      // 패널티 받은 회수.
        var penLastDate: Long = 0,  // 글올리기 금지해제되는  날짜.
        var penReason: String? = null,  // 패널티 이유.

        var totalPoint: Long = 0,       // Level 산정 기준포인트 = activityPoint - penaltyPoint.

        ////////////////////////////////////////////////////////////////
        var loginTime: Long = rightNow(),    //  최근 로그인 시간
        var addDate: Long = rightNow(),
        var delDate: Long? = null,    // 탈퇴한 날짜.
        var invalid: Boolean = false // 탈퇴한 날짜.

) : Parcelable {

    /**
     * 최초 가입시
     * 1. anonymous
     * 2. google
     */
    constructor(user: FirebaseUser, nick: String?, loginFrom: LoginFrom) : this(
            uid = user.uid,
            email = user.email,

            nick = nick ?: user.displayName ?: createGuestNick(),
            dpName = user.displayName,
            face = null,
            faceThumb = user.photoUrl?.toString(),

            fcmToken = Sred.DEVICE_FCM_TOKEN.sredString(),
            type = when (loginFrom) {
                LoginFrom.GOOGLE -> PeerType.BASIC_2.no
                LoginFrom.EMAIL -> PeerType.BASIC_2.no
                LoginFrom.ANONYMOUS -> PeerType.BASIC_1.no
                else -> PeerType.BASIC_1.no
            },
            loginFrom = loginFrom.no

    )

    /** clone 복사 */
    constructor(peer: Peer) : this(
            uid = peer.uid,
            email = peer.email,

            nick = peer.nick,
            dpName = peer.dpName,
            comment = peer.comment,
            face = peer.face,
            faceThumb = peer.faceThumb,

            fcmToken = peer.fcmToken,
            type = peer.type,
            loginFrom = peer.loginFrom,

            cpay = peer.cpay,
            crd = peer.crd,
            bcrd = peer.bcrd,
            pnt = peer.pnt,
            crt = peer.crt,

            boardCount = peer.boardCount,
            postCount = peer.postCount,
            fleaCount = peer.fleaCount,
            roomCount = peer.roomCount,
            storeCount = peer.storeCount,

            penPoint = peer.penPoint,
            penCount = peer.penCount,
            penLastDate = peer.penLastDate,
            penReason = peer.penReason,

            totalPoint = peer.totalPoint,

            loginTime = peer.loginTime,
            addDate = peer.addDate,
            delDate = peer.delDate,
            invalid = peer.invalid
    )

    constructor(source: Parcel) : this(
            source.readString()!!,
            source.readString(),
            source.readString()!!,
            source.readString(),
            source.readString(),
            source.readString(),
            source.readString(),
            source.readString(),
            source.readInt(),
            source.readInt(),
            source.readInt(),
            source.readInt(),
            source.readInt(),
            source.readInt(),
            source.readInt(),
            source.readInt(),
            source.readInt(),
            source.readInt(),
            source.readInt(),
            source.readInt(),
            source.readInt(),
            source.readInt(),
            source.readLong(),
            source.readString(),
            source.readLong(),
            source.readLong(),
            source.readLong(),
            source.readValue(Long::class.java.classLoader) as Long?,
            1 == source.readInt()
    )

    override fun describeContents() = 0

    override fun writeToParcel(dest: Parcel, flags: Int) = with(dest) {
        writeString(uid)
        writeString(email)
        writeString(nick)
        writeString(dpName)
        writeString(comment)
        writeString(face)
        writeString(faceThumb)
        writeString(fcmToken)
        writeInt(type)
        writeInt(loginFrom)
        writeInt(cpay)
        writeInt(pnt)
        writeInt(bcrd)
        writeInt(crd)
        writeInt(crt)
        writeInt(boardCount)
        writeInt(postCount)
        writeInt(roomCount)
        writeInt(fleaCount)
        writeInt(storeCount)
        writeInt(penPoint)
        writeInt(penCount)
        writeLong(penLastDate)
        writeString(penReason)
        writeLong(totalPoint)
        writeLong(loginTime)
        writeLong(addDate)
        writeValue(delDate)
        writeInt((if (invalid) 1 else 0))
    }

    companion object {
        @JvmField
        val CREATOR: Parcelable.Creator<Peer> = object : Parcelable.Creator<Peer> {
            override fun createFromParcel(source: Parcel): Peer = Peer(source)
            override fun newArray(size: Int): Array<Peer?> = arrayOfNulls(size)
        }
    }
}









