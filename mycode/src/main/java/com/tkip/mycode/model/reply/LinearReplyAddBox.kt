package com.tkip.mycode.model.reply

import android.content.Context
import android.net.Uri
import android.view.LayoutInflater
import android.view.View
import android.widget.LinearLayout
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import com.tkip.mycode.R
import com.tkip.mycode.databinding.LinearReplyAddBoxBinding
import com.tkip.mycode.funs.common.*
import com.tkip.mycode.init.ATTACH_TEXT
import com.tkip.mycode.init.REPLY_ATTACH_TYPE
import com.tkip.mycode.init.base.getStr
import com.tkip.mycode.init.inter.interReplyAddBox
import com.tkip.mycode.init.inter.interReplyBottom
import com.tkip.mycode.model.my_enum.ReplyType
import com.tkip.mycode.model.peer.checkAllAvailUpload
import com.tkip.mycode.model.toss.OnToss
import com.tkip.mycode.model.toss.ViewTossModel
import com.tkip.mycode.nav.lobby.Lobby
import com.tkip.mycode.nav.lobby.chat.Chat
import com.tkip.mycode.nav.lobby.chat.OnChat
import com.tkip.mycode.util.lib.photo.util.PhotoUtil
import com.tkip.mycode.util.my_view.ViewUtil
import com.tkip.mycode.util.tools.anim.AnimUtil
import com.tkip.mycode.util.tools.anim.hide
import com.tkip.mycode.util.tools.anim.visi
import com.tkip.mycode.util.tools.etc.MyKeyboard

/**
 * reply 올리는 박스
 */
class LinearReplyAddBox : LinearLayout {
    lateinit var b: LinearReplyAddBoxBinding
    lateinit var activity: AppCompatActivity

    constructor(context: Context) : super(context)
    constructor(context: Context, allowReply: Boolean, modelKey: String, onClickAddReply: () -> Unit) : super(context) {
        b = DataBindingUtil.inflate(LayoutInflater.from(context), R.layout.linear_reply_add_box, this, true)

        activity = context as AppCompatActivity

        /**  댓글박스   */
        handleTextChange()
        b.inter = interReplyAddBox(
                onClickOpenAttachMenu = { toggle() },
                onClickAddReply = { onClickAddReply() })

        /**  첨부 메뉴   */
        b.inBottom.inter = interReplyBottom(
                onClickCloseAttachMenu = { toggle() },
                onClickOpenGallery = { OnPermission.onOpenGallery(activity, true, false) },
                onClickOpenTossList = {
                    OnReply.resetAttachReply(b.lineAttachBox)

                    OnToss.showTossList(context, modelKey,
                            onToss = { toss ->
                                b.lineAttachBox.addView(ViewTossModel(context,null, toss,
                                        onGoto =null,
                                        onRemove = {
                                            OnReply.resetAttachReply(b.lineAttachBox)
                                        },
                                        onClose =null,
                                        onLong = null
                                ))
                                b.lineAttachBox.visi()
                                b.btnAdd.visi()  // add버튼 활성화
                            })
                }
        )


        /**
         * 댓글 허용 여부에 따라
         */
        if (!allowReply) {

            b.etReply.isEnabled = false
            b.etReply.hint = R.string.hint_not_add_reply.getStr()

            b.btnAttach.isEnabled = false
            b.btnAttach.hide()

            b.btnAdd.isEnabled = false
        }


    }


    /************************************************************************************************
     * onPick에서 받은 사진 추가하기.
     ***********************************************************************************************/
    fun addPhotoUri(uri: Uri?) {
        uri?.let {
            ViewUtil.addViewPhotoUri(context, b.lineAttachBox, it, onPhoto = {}, onClose = {})
            b.btnAdd?.visi()
        }
    }

    /************************************************************************************************
     * reply bottom메뉴 열고닫기
     ***********************************************************************************************/
    private fun toggle() {
        if (b.inBottom.coordiBottom.visibility == View.VISIBLE) {
            AnimUtil.hideSlideDown(true, b.inBottom.coordiBottom, true)
            PhotoUtil.resetAllUriList()
        } else AnimUtil.showNormal(b.inBottom.coordiBottom)
    }


    /************************************************************************************************
     * attach check & Add Reply  : 모든 model에서 사용가능.
     ***********************************************************************************************/
    fun checkAddReply(context: Context, any: Any, replyAdapter: ReplyAdapterFireS?, success: () -> Unit, fail: () -> Unit) {

        /*** 업로드 가능 여부 체크 */
        if (checkAllAvailUpload(context)) return

        /*** 도배방지 */
        if (OnReply.notAllowedAddReply(replyAdapter)) return

        /*** 유효성 체크 */
        if (REPLY_ATTACH_TYPE == ATTACH_TEXT && b.etReply.isNullBlank(R.string.hint_add_reply)) return

        OnVisible.fadeToggle(true, arrayOf(b.progress), arrayOf(b.btnAdd))

        val finalReply = Reply.create(any, ReplyType.TEXT, b.etReply.getNullStr())
        finalReply.setAttach()



        /**  댓글 올리기   */
        OnReply.addPhotoAndReply(context, finalReply,
                success = {
                    OnVisible.fadeToggle(false, arrayOf(b.progress), arrayOf(b.btnAdd))
                    b.etReply.setText("")
                    b.etReply.requestFocus()
                    MyKeyboard.hide(b.etReply)
                    bottomClear() // attachBox 닫기.
                    OnReply.resetAttachReply(null)
                    success()

                },
                fail = {
                    fail()
                    OnVisible.fadeToggle(false, arrayOf(b.progress), arrayOf(b.btnAdd))
                })
    }

    fun checkAddChat(context: Context, lobby: Lobby, success: () -> Unit, fail: () -> Unit) {
        /*** 유효성 체크 */
        if (REPLY_ATTACH_TYPE == ATTACH_TEXT && b.etReply.isNullBlank(R.string.hint_add_reply)) return

        OnVisible.fadeToggle(true, arrayOf(b.progress), arrayOf(b.btnAdd))  // Progress 돌리기

        val chat = Chat(lobby, ReplyType.TEXT, b.etReply.getNullStr())
        chat.setAttach()

        /**  댓글 올리기   */
        OnChat.addPhotoAndChat(context, chat,
                success = {
                    OnVisible.fadeToggle(false, arrayOf(b.progress), arrayOf(b.btnAdd))
                    b.etReply.setText("")
                    b.etReply.requestFocus()
                    MyKeyboard.hide(b.etReply)
                    bottomClear() // attachBox 닫기.
                    OnReply.resetAttachReply(null)
                    success()
                },
                fail = {
                    fail()
                    OnVisible.fadeToggle(false, arrayOf(b.progress), arrayOf(b.btnAdd))
                })

    }


    private fun bottomClear() {
        AnimUtil.hideSlideDown(true, b.inBottom.coordiBottom, true)
        PhotoUtil.resetAllUriList()
        b.lineAttachBox.removeAllViews()
//    b.lineAttachBox.gone()
    }


    /**
     * textChange 리스너
     */
    private fun handleTextChange() {
        /** 댓글 올리기 : 올리는 버튼  show/hide */
        b.etReply.textListener(after = {}, before = { _, _, _, _ -> }, on = { s, _, _, _ ->
            if (PhotoUtil.photoUris.isEmpty()) {
                if (s.toString().isNotBlank()) b.btnAdd.visi()
                else b.btnAdd.hide()
            } else {
                AnimUtil.showNormal(b.btnAdd)
            }
        })
    }


}