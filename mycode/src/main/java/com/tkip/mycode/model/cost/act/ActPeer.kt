package com.tkip.mycode.model.cost.act

import android.os.Parcel
import android.os.Parcelable
import com.tkip.mycode.init.base.createKey
import com.tkip.mycode.model.peer.myuid
import com.tkip.mycode.util.tools.date.rightNow

/**
 * all Act 처리 과정 list
 * map to List for adapter
 */
data class ActPeer(

        var uid: String? = null,
        var nick: String? = null,
        var pnt: Int? = null

) : Parcelable {
    constructor(source: Parcel) : this(
            source.readString(),
            source.readString(),
            source.readValue(Int::class.java.classLoader) as Int?
    )

    override fun describeContents() = 0

    override fun writeToParcel(dest: Parcel, flags: Int) = with(dest) {
        writeString(uid)
        writeString(nick)
        writeValue(pnt)
    }

    companion object {
        @JvmField
        val CREATOR: Parcelable.Creator<ActPeer> = object : Parcelable.Creator<ActPeer> {
            override fun createFromParcel(source: Parcel): ActPeer = ActPeer(source)
            override fun newArray(size: Int): Array<ActPeer?> = arrayOfNulls(size)
        }
    }
}