package com.tkip.mycode.model.toss

import android.os.Parcel
import android.os.Parcelable
import com.tkip.mycode.init.TOSS_COPY
import com.tkip.mycode.init.base.getI
import com.tkip.mycode.model.board.Board
import com.tkip.mycode.model.flea.Flea
import com.tkip.mycode.model.my_enum.*
import com.tkip.mycode.model.post.Post
import com.tkip.mycode.model.room.Room
import com.tkip.mycode.model.store.Store

/**
 * <전달하기>
 *  일단 내부적으로만 쓰임. db 안 올림.
 *  1. copy
 *  2. 복사후 표시
 *  3. reply에 표시
 */
data class Toss(

        var modelKey: String = "modelKey",
        var modelTitle: String? = null,
        var photoThumb: String? = null,

        var dataTypeNo: Int = DataType.POST.no,
        var type: Int = TOSS_COPY     // 내부적으로 분기할 때만 사용.

) : Parcelable {


    /**     * for model     */
    constructor(any:Any) : this(
            modelKey = any.anyKey(),
            modelTitle = any.anyTitle(),
            photoThumb = any.anyThumb(0),
            dataTypeNo = any.getMyDataType().no
    )

//    /**     * for post     */
//    constructor(post: Post) : this(
//            modelKey = post.key,
//            modelTitle = post.title,
//            photoThumb = post.fotoList.getI(0)?.thumbUrl,
//            dataTypeNo = DataType.POST.no
//    )
//
//    /**     * for flea     */
//    constructor(flea: Flea) : this(
//            modelKey = flea.key,
//            modelTitle = flea.title,
//            photoThumb = flea.fotoList.getI(0)?.thumbUrl,
//            dataTypeNo = DataType.FLEA.no
//    )
//
//    /**     * for room     */
//    constructor(room: Room) : this(
//            modelKey = room.key,
//            modelTitle = room.local,
//            photoThumb = room.fotoList.getI(0)?.thumbUrl,
//            dataTypeNo = DataType.ROOM.no
//    )
//
//    /**     * for Board     */
//    constructor(board: Board) : this(
//            modelKey = board.key,
//            modelTitle = board.title,
//            photoThumb = board.anyThumb(0),
//            dataTypeNo = DataType.BOARD.no
//    )

    constructor(source: Parcel) : this(
            source.readString()!!,
            source.readString(),
            source.readString(),
            source.readInt(),
            source.readInt()
    )

    override fun describeContents() = 0

    override fun writeToParcel(dest: Parcel, flags: Int) = with(dest) {
        writeString(modelKey)
        writeString(modelTitle)
        writeString(photoThumb)
        writeInt(dataTypeNo)
        writeInt(type)
    }

    companion object {

        /**
         * model에 맞게 toss 생성
         */
        fun create(any: Any, type1: Int) =
                when (any) {
                    is Post -> Toss(any)
                    is Flea -> Toss(any)
                    is Room -> Toss(any)
                    is Board -> Toss(any)
                    is Store -> Toss(any)
                    else -> Toss()
                }.apply { type = type1 }


        @JvmField
        val CREATOR: Parcelable.Creator<Toss> = object : Parcelable.Creator<Toss> {
            override fun createFromParcel(source: Parcel): Toss = Toss(source)
            override fun newArray(size: Int): Array<Toss?> = arrayOfNulls(size)
        }
    }
}
