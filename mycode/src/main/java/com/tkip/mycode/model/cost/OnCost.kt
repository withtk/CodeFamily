package com.tkip.mycode.model.cost

import android.content.Context
import android.content.Intent
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import com.tkip.mycode.dialog.OnDDD
import com.tkip.mycode.init.PUT_MANAGE
import com.tkip.mycode.init.PUT_UID
import com.tkip.mycode.model.cost.credit.CostListActivity
import com.tkip.mycode.model.peer.OnPeer
import com.tkip.mycode.model.peer.Peer
import com.tkip.mycode.util.lib.TransitionHelper
import com.tkip.mycode.util.tools.fire.Firee

class OnCost {
    companion object {


        fun gotoCostListActivity(context: Context, manage: Boolean, uid: String?, arrPair: Array<androidx.core.util.Pair<View, String>>?) {
            val intent = Intent(context, CostListActivity::class.java).apply {
                addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
                putExtra(PUT_MANAGE, manage)
                putExtra(PUT_UID, uid)
            }
            TransitionHelper.startTran(context, intent, arrPair)
        }

        fun openDialog(activity: AppCompatActivity, peer: Peer) {
            CostListDialog.newInstance(OnPeer.peer).show(activity.supportFragmentManager, "dialog")
        }

        /**
         * for admin
         * 딥삭제
         */
        fun deepDel(context:Context,costKey:String) {
            OnDDD.deepDel(context,{ Firee.deepDelCost(costKey, {}, {})},{})
        }


    }
}