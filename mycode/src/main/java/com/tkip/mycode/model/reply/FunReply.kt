package com.tkip.mycode.model.reply

import com.tkip.mycode.init.*
import com.tkip.mycode.model.my_enum.ReplyType
import com.tkip.mycode.model.my_enum.Status
import com.tkip.mycode.model.my_enum.dataType
import com.tkip.mycode.model.peer.isMe
import com.tkip.mycode.model.peer.myuid
import com.tkip.mycode.nav.lobby.Lobby
import com.tkip.mycode.nav.lobby.chat.Chat
import com.tkip.mycode.util.tools.fire.commonStatusUpdate


/************************************************************************************************
 * Chat
 ***********************************************************************************************/


/**
 * 상대방 uid 받아오기.
 **/
fun Lobby.getOtherUid() = if (this.authorUID.isMe()) this.targetUID else this.authorUID
fun Lobby.getOtherNick() = if (this.authorUID.isMe()) this.targetNick else this.authorNick


/**
 * attach 삽입하기.
 */
fun Chat.setAttach() {
    when (REPLY_ATTACH_TYPE) {
        ATTACH_TEXT -> {
            replyTypeNo = ReplyType.TEXT.no
        }
        ATTACH_PHOTO -> {
            replyTypeNo = ReplyType.PHOTO.no
        }
        ATTACH_REPLY -> {
            replyTypeNo = ReplyType.REPLY.no
            earlierKey = REPLY_ATTACH_REPLY_KEY
        }
        ATTACH_TOSS -> {
            replyTypeNo = ReplyType.TOSS.no
            tossModelKey = REPLY_ATTACH_TOSS?.modelKey
            tossDataTypeNo = REPLY_ATTACH_TOSS?.dataTypeNo
        }
    }
}


/************************************************************************************************
 * Reply
 ***********************************************************************************************/

fun Reply.isMy() = (this.authorUID == myuid())


/**
 * attach 삽입하기.
 */
fun Reply.setAttach() {
    when (REPLY_ATTACH_TYPE) {
        ATTACH_TEXT -> replyTypeNo = ReplyType.TEXT.no
        ATTACH_PHOTO -> replyTypeNo = ReplyType.PHOTO.no
        ATTACH_REPLY -> {
            replyTypeNo = ReplyType.REPLY.no
            earlierKey = REPLY_ATTACH_REPLY_KEY
        }
        ATTACH_TOSS -> {
            replyTypeNo = ReplyType.TOSS.no
            tossModelKey = REPLY_ATTACH_TOSS?.modelKey
            tossDataTypeNo = REPLY_ATTACH_TOSS?.dataTypeNo
        }
    }
}


/************************************************************************************************
 * 상태변경
 ***********************************************************************************************/
fun Reply.changeStatus(status: Status, success: () -> Unit, fail: () -> Unit) {
    val child = this.dataTypeNo.dataType().child
    commonStatusUpdate(child, this.modelKey, EES_REPLY, this.key, status, { success() }, { fail() })
//    batch().update(docRef(child, this.modelKey, CH_REPLY, this.key), CH_STATUS_NO, status.no)
//            .myCommit("changeStatus reply",
//                    success = {
//                        success()
//                        TToast.showNormal(R.string.status_complete_handled)
//                    }, fail = { fail() })
}

