package com.tkip.mycode.model.cost

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.RecyclerView
import com.tkip.mycode.R
import com.tkip.mycode.databinding.HolderCostActBinding
import com.tkip.mycode.databinding.HolderCostPaymentBinding
import com.tkip.mycode.dialog.OnDDD
import com.tkip.mycode.dialog.admin.CostDialog
import com.tkip.mycode.funs.common.tranPair
import com.tkip.mycode.init.inter.interBasicClick
import com.tkip.mycode.init.inter.interHolderBasic
import com.tkip.mycode.model.cost.balance.Balance
import com.tkip.mycode.model.my_enum.CostType
import com.tkip.mycode.model.my_enum.checkAndGo
import com.tkip.mycode.model.peer.isSupervisor
import com.tkip.mycode.util.tools.etc.OnMyClick

class CostAdapter(val items: ArrayList<Cost>, val onItemClick: ((Cost) -> Unit)?) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {
    private lateinit var context: Context

//    companion object {
//        val rvAct = 100    // 전체 Cost.
//        val rvCost = 150   // 전체 Cost. (act제외)
//    }

    override fun getItemViewType(position: Int): Int {
        val cost = items[position]
        return cost.costTypeNo ?: CostType.ACT.no
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        context = parent.context
        return when (viewType) {
            CostType.ACT.no -> CostActHolder(HolderCostActBinding.inflate(LayoutInflater.from(parent.context), parent, false))
            CostType.PAYMENT.no -> CostPaymentHolder(HolderCostPaymentBinding.inflate(LayoutInflater.from(parent.context), parent, false))
            CostType.TAKE.no -> CostPaymentHolder(HolderCostPaymentBinding.inflate(LayoutInflater.from(parent.context), parent, false))
            CostType.CONVERT.no -> CostPaymentHolder(HolderCostPaymentBinding.inflate(LayoutInflater.from(parent.context), parent, false))
            CostType.SEND.no -> CostPaymentHolder(HolderCostPaymentBinding.inflate(LayoutInflater.from(parent.context), parent, false))
            else -> CostPaymentHolder(HolderCostPaymentBinding.inflate(LayoutInflater.from(parent.context), parent, false))
        }
    }

    override fun onBindViewHolder(h: RecyclerView.ViewHolder, p: Int) {
        when (getItemViewType(p)) {
            CostType.ACT.no -> (h as CostActHolder).bind(items[p])
            CostType.PAYMENT.no -> (h as CostPaymentHolder).bind(items[p])
            CostType.TAKE.no -> (h as CostPaymentHolder).bind(items[p])
            CostType.CONVERT.no -> (h as CostPaymentHolder).bind(items[p])
            CostType.SEND.no -> (h as CostPaymentHolder).bind(items[p])
            else -> (h as CostPaymentHolder).bind(items[p])
        }
    }

    override fun getItemCount(): Int {
        return items.size
    }

    override fun getItemId(position: Int): Long {
        return items[position].key.hashCode().toLong()
    }


    /*** onClickCard */
    private fun commonClickCost(v: View, cost: Cost) {
        OnMyClick.setDisableAWhileBTN(v)

        onItemClick?.let {
            it(cost)
            return
        }


        if (isSupervisor()) {
            OnDDD.allowAdmin(context, ok = {
                /**     * Cost 상세 다이얼로그     */
                CostDialog.newInstance(cost, onOk1 = {}).show((context as AppCompatActivity).supportFragmentManager, "dialog")
            })
        }
    }

    /********************************************************************************************
     * * Act
     *******************************************************************************************/
    inner class CostActHolder(val b: HolderCostActBinding) : RecyclerView.ViewHolder(b.root) {
        private lateinit var cost: Cost

        fun bind(cost: Cost) {
            this.cost = cost
            b.cost = cost
            b.inter = interBasicClick(
                    on1 = {
                        commonClickCost(it, cost)
                    },
                    on2 = {
                        cost.checkAndGo(context,true, arrayOf(b.lineTitle.tranPair(R.string.tran_linear)), onSuccess = null)
                    }, on3 = null)

            b.executePendingBindings()
        }
    }

    /********************************************************************************************
     * * Payment
     *******************************************************************************************/
    inner class CostPaymentHolder(val b: HolderCostPaymentBinding) : RecyclerView.ViewHolder(b.root) {
        private lateinit var cost: Cost

        fun bind(cost: Cost) {
            this.cost = cost
            b.cost = cost

            val balance = Balance.new(cost)
            b.balance = balance
            b.inPay.balance = balance

            b.inter = interHolderBasic(
                    onClickCard = {
                        commonClickCost(it, cost)
                    },
                    onClickGo = {
                        OnMyClick.setDisableAWhileBTN(it)
                        /*** 이동할 수 있는 모델인지 확인 후 -> 모델 이동 */
                        cost.dataTypeNo?.let {
                            OnDDD.confirm(context, R.string.ask_move_to_model,
                                    ok = {
                                        cost.checkAndGo(context, false, arrayOf(b.cons.tranPair(R.string.tran_linear)), null)
                                    }, cancel = {})
                        }
                    },
                    onClickUpdate = {},
                    onClickDel = {},
                    onDeepDel = {
                        OnCost.deepDel(context, cost.key)
                    },
                    onClickStatus = {}
            )

            b.executePendingBindings()
        }
    }


//    /**     * Take     */
//    inner class CostTakeHolder(val b: HolderCostPaymentBinding) : RecyclerView.ViewHolder(b.root) {
//        private lateinit var cost: Cost
//
//        fun bind(cost: Cost) {
//            this.cost = cost
//            b.cost = cost
//            b.inter = interBasicClick(
//                    on1 = {
//                        commonClickCost(it, cost)
//                    }, on2 = null, on3 = null)
//
//            b.executePendingBindings()
//        }
//    }
//
//    /**     * Send     */
//    inner class CostSendHolder(val b: HolderCostPaymentBinding) : RecyclerView.ViewHolder(b.root) {
//        private lateinit var cost: Cost
//
//        fun bind(cost: Cost) {
//            this.cost = cost
//            b.cost = cost
//            b.inter = interBasicClick(
//                    on1 = {
//                        commonClickCost(it, cost)
//                    }, on2 = null, on3 = null)
//
//            b.executePendingBindings()
//        }
//    }
//
//    /**     * Convert     */
//    inner class CostConvertHolder(val b: HolderCostPaymentBinding) : RecyclerView.ViewHolder(b.root) {
//        private lateinit var cost: Cost
//
//        fun bind(cost: Cost) {
//            this.cost = cost
//            b.cost = cost
//            b.inter = interBasicClick(
//                    on1 = {
//                        commonClickCost(it, cost)
//                    }, on2 = null, on3 = null)
//
//            b.executePendingBindings()
//        }
//    }


}
