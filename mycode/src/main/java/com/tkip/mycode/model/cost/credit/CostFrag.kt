package com.tkip.mycode.model.cost.credit

import android.os.Bundle
import com.tkip.mycode.R
import com.tkip.mycode.databinding.FragmentCostBinding
import com.tkip.mycode.funs.search.model.CustomListCost
import com.tkip.mycode.init.PUT_COST_TYPE_NO
import com.tkip.mycode.init.PUT_UID
import com.tkip.mycode.init.my_bind.BindingFragment
import com.tkip.mycode.model.cost.Cost
import com.tkip.mycode.model.my_enum.CostType
import com.tkip.mycode.model.my_enum.costType


class CostFrag : BindingFragment<FragmentCostBinding>() {
    override fun getLayoutResId(): Int = R.layout.fragment_cost

    private lateinit var customSearchCost: CustomListCost

    private val uid by lazy { arguments?.getString(PUT_UID) }
    private val costType by lazy { arguments?.getInt(PUT_COST_TYPE_NO)?.costType() ?: CostType.PAYMENT }
    private var onItemClick: ((Cost) -> Unit)? = null

    companion object {
        @JvmStatic
        fun newInstance(uid: String?, costTypeNo: Int,   onItemClick: ((Cost) -> Unit)?) = CostFrag().apply {
            arguments = Bundle().apply {
                putString(PUT_UID, uid)
                putInt(PUT_COST_TYPE_NO, costTypeNo)
            }
            this.onItemClick = onItemClick
        }
    }


    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        customSearchCost = CustomListCost(mContext, uid, costType,  null, onItemClick)
        b.frame.addView(customSearchCost)

        customSearchCost.getEsDataList(0, null)

    }


}

