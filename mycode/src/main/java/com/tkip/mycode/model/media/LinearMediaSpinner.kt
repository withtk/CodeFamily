package com.tkip.mycode.model.media

import android.content.Context
import android.view.LayoutInflater
import android.widget.LinearLayout
import androidx.databinding.DataBindingUtil
import com.tkip.mycode.R
import com.tkip.mycode.databinding.ViewSpinnerCommonBinding
import com.tkip.mycode.funs.common.onItemSelected


class LinearMediaSpinner : LinearLayout {

    private lateinit var b: ViewSpinnerCommonBinding

    constructor(context: Context) : super(context)

    constructor(context: Context, onClick: (Media) -> Unit) : super(context) {
//        this.items = items
        b = DataBindingUtil.inflate(LayoutInflater.from(context), R.layout.view_spinner_common, this, true)

        val spinAdapter = SpinnerMediaAdapter(mediaItems.setup())
        b.spin.adapter = spinAdapter

        b.spin.onItemSelected(
                onNothin = {},
                onSelected = { _, _, p, id ->
                    mediaItems[p]?.let { media ->
                        onClick(media)
                        mediaItems.removeAt(p)
                        b.spin.setSelection(0)
                    }
                })


//        /**         * set Edittext : UPDATE         */
//        Media.values().forEach { media ->
//            media.userId?.let {
//                val p = items.getMediaIndex(media.name)
//                items.removeAt(p)
//                onClick(media)
//            }
//        }


//        initData(map)
    }

//    private fun setButton(onClick: (Media) -> Unit) {
//
//        Media.values().forEach { media ->
//            media.userId?.let {
//                val position = items.getMediaIndex(media.name)
//                onClick(media)
//            }
//        }
//    }

    /**
     * update인 경우에 값 셋팅하기.
     */
//    private fun initData(map: HashMap<String, String>?) {
//        map?.let {
//            for ((mediaName, userId) in it) {
//                Media.valueOf(mediaName).userId = userId   // Media에 userId값 셋팅
//                val p = items.getMediaIndex(mediaName)   // position 받아오기.
//                b.spin.setSelection(p)
//            }
//        }
//    }

    /**
     * 리스트내에서의 position값을 받아오기 위해.
     */
//    private fun ArrayList<Media?>.getMediaIndex(mediaName: String): Int {
//        this.forEachIndexed { i, m ->
//            m?.let {
//                if (m.name == mediaName) return i
//            }
//        }
//        return 0
//    }


}