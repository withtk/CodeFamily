package com.tkip.mycode.model

import android.os.Parcel
import android.os.Parcelable
import com.tkip.mycode.model.my_enum.PayType
import com.tkip.mycode.model.peer.OnPeer
import com.tkip.mycode.init.base.createKey
import com.tkip.mycode.init.base.getI
import com.tkip.mycode.model.store.Store
import com.tkip.mycode.util.tools.date.rightNow

/**
 * 스토어에 adCredit 올리기
 */
data class AdCredit(

        var key: String = createKey(),
        var uid: String = OnPeer.myUid(),
        var payTypeNo: Int = PayType.CREDIT.no,     // pay, credit, point
        var amount: Int = 10,                       // 최소10, 광고 입찰한 가격(필요할때만 쓰임)

        /////////////////// 광고되는 개체 ///////////////////////////////////
        var modelKey: String = "storeKey",
        var modelTitle: String = "storeName",
        var photoThumb: String? = null,
        ///////////////////////////////////////////////////////////////////

        var term: Int = 1,            // 광고게재 기간
        var startDate: Int= 190101,   // 시작 날짜
        var addDate: Long = rightNow()

) : Parcelable {

    constructor(store: Store) : this(
            modelKey = store.key,
            modelTitle = store.title,
            photoThumb = store.fotoList.getI(0)?.thumbUrl
    )

    constructor(source: Parcel) : this(
            source.readString()!!,
            source.readString()!!,
            source.readInt(),
            source.readInt(),
            source.readString()!!,
            source.readString()!!,
            source.readString(),
            source.readInt(),
            source.readInt(),
            source.readLong()
    )

    override fun describeContents() = 0

    override fun writeToParcel(dest: Parcel, flags: Int) = with(dest) {
        writeString(key)
        writeString(uid)
        writeInt(payTypeNo)
        writeInt(amount)
        writeString(modelKey)
        writeString(modelTitle)
        writeString(photoThumb)
        writeInt(term)
        writeInt(startDate)
        writeLong(addDate)
    }

    companion object {
        @JvmField
        val CREATOR: Parcelable.Creator<AdCredit> = object : Parcelable.Creator<AdCredit> {
            override fun createFromParcel(source: Parcel): AdCredit = AdCredit(source)
            override fun newArray(size: Int): Array<AdCredit?> = arrayOfNulls(size)
        }
    }
}