package com.tkip.mycode.model.post

import com.tkip.mycode.R
import com.tkip.mycode.init.base.*


/**
 * 유효성 체크
 * 통과 못하면 -> true
 */
fun Post.inValid(): Boolean {
    return (
            this.title.isAnyNull(R.string.alert_need_to_title)
            )
}
