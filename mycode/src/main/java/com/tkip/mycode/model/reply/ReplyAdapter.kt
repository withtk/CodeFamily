package com.tkip.mycode.model.reply

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.tkip.mycode.R
import com.tkip.mycode.databinding.HolderReplyMyConBinding
import com.tkip.mycode.funs.common.lllogD
import com.tkip.mycode.funs.common.tranPair
import com.tkip.mycode.init.RV_MANAGE
import com.tkip.mycode.init.inter.interHolderBasic
import com.tkip.mycode.model.my_enum.justGo
import com.tkip.mycode.model.my_enum.showStatusDialog
import com.tkip.mycode.model.peer.isAdmin
import com.tkip.mycode.util.tools.anim.gone
import com.tkip.mycode.util.tools.etc.OnMyClick

/**
 * Reply list
 */
class ReplyAdapter(private val items: ArrayList<Reply>, val rvType: Int, val onItemClick: ((Reply) -> Unit)?) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {
//    companion object {
//        const val rvVert = 100
//        const val rvHori = 200
//        const val rvGridHori = 300
//        const val rvMyContents = 500
//    }

    private lateinit var context: Context

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        this.context = parent.context
        return when (rvType) {
            RV_MANAGE -> ReplyMyConViewHolder(HolderReplyMyConBinding.inflate(LayoutInflater.from(parent.context), parent, false))
            else -> ReplyMyConViewHolder(HolderReplyMyConBinding.inflate(LayoutInflater.from(parent.context), parent, false))
        }
    }

    override fun onBindViewHolder(h: RecyclerView.ViewHolder, p: Int) {
        when (rvType) {
            RV_MANAGE -> (h as ReplyMyConViewHolder).bind(items[p])
            else -> (h as ReplyMyConViewHolder).bind(items[p])
        }
    }

    override fun getItemCount(): Int {
        return items.size
    }

    /******************************************************************************************
     * myContents, manage
     *******************************************************************************************/
    inner class ReplyMyConViewHolder(val b: HolderReplyMyConBinding) : RecyclerView.ViewHolder(b.root) {

        private lateinit var reply: Reply

        fun bind(reply: Reply) {
            lllogD("BoardMyConViewHolder reply $reply")
            this.reply = reply
            b.reply = reply
            b.inter = interHolderBasic(
                    onClickCard = { onClickCard(it) },
                    onClickGo = { },
                    onClickUpdate = null,
                    onClickDel = null, onDeepDel = {},
                    onClickStatus = { if (isAdmin()) reply.showStatusDialog(context,null) else onClickCard(it) })

            b.executePendingBindings()
            b.btnGrant.gone()   // 이걸 안 해주면 notify했을 때 이상하게 나와.
        }

        fun onClickCard(v: View) {
            OnMyClick.setDisableAWhileBTN(v)
            reply.justGo(context, true, arrayOf(b.tvTitle.tranPair(R.string.tran_linear)))
        }

    }


}