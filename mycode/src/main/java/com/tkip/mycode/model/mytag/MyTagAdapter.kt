package com.tkip.mycode.model.mytag

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.RecyclerView
import com.tkip.mycode.databinding.HolderMyTagBinding
import com.tkip.mycode.databinding.HolderMyTagManageBinding
import com.tkip.mycode.util.tools.etc.OnMyClick

/**
 * 검색한 myTag List.
 *
 */
class MyTagAdapter(private val items: ArrayList<MyTag>, private val rvType: Int, val onItemClick: ((adapter:MyTagAdapter, position:Int, myTag:MyTag) -> Unit)?) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    private lateinit var activity: AppCompatActivity

    companion object{
        const val rvNormal = 100
        const val rvManage = 200
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        this.activity = parent.context as AppCompatActivity
        return when (rvType) {
            rvNormal -> MyTagViewHolder(HolderMyTagBinding.inflate(LayoutInflater.from(parent.context), parent, false))
            rvManage -> MyTagManageViewHolder(HolderMyTagManageBinding.inflate(LayoutInflater.from(parent.context), parent, false))
            else -> MyTagViewHolder(HolderMyTagBinding.inflate(LayoutInflater.from(parent.context), parent, false))
        }
    }

    override fun onBindViewHolder(h: RecyclerView.ViewHolder, p: Int) {
        when (rvType) {
            rvNormal -> (h as MyTagViewHolder).bind(items[p])
            rvManage -> (h as MyTagManageViewHolder).bind(items[p])
            else -> (h as MyTagViewHolder).bind(items[p])
        }
    }

    override fun getItemCount(): Int {
        return items.size
    }

    /**
     * NORMAL
     */
    inner class MyTagViewHolder(val b: HolderMyTagBinding) : RecyclerView.ViewHolder(b.root) {

        private lateinit var myTag: MyTag

        fun bind(myTag: MyTag) {
            this.myTag = myTag
            b.holder = this
            b.myTag = myTag

            b.executePendingBindings()
        }

        fun onClickTag() {
            if (OnMyClick.isDoubleClick()) return
            onItemClick?.invoke(this@MyTagAdapter, adapterPosition, myTag)      // item  remove 는 alert에서.


//            items.removeAt(adapterPosition)
//            notifyDataSetChanged()

//            OnPost.checkAndGo(activity, room.key, arrayOf(b.line.tranPair(R.string.transition_linear)),
//                    onNotNormal = {
//                        items[adapterPosition] = it
//                        notifyItemChanged(adapterPosition)
//                    })


        }

    }


    /**
     * MANAGE
     */
    inner class MyTagManageViewHolder(val b: HolderMyTagManageBinding) : RecyclerView.ViewHolder(b.root) {

        private lateinit var myTag: MyTag

        fun bind(myTag: MyTag) {
            this.myTag = myTag
            b.holder = this
            b.myTag = myTag

            b.executePendingBindings()
        }

        fun onClickTag() {
            if (OnMyClick.isDoubleClick()) return
            onItemClick?.invoke(this@MyTagAdapter, adapterPosition, myTag)      // item  remove 는 alert에서.

            ADDmytagDialog.newInstance(myTag ).show(activity.supportFragmentManager,"dialog")

        }

    }

}