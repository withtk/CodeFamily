package com.tkip.mycode.model.media

import android.content.Context
import android.view.LayoutInflater
import android.widget.LinearLayout
import androidx.databinding.DataBindingUtil
import com.tkip.mycode.R
import com.tkip.mycode.databinding.ViewMediaEdittextBinding
import com.tkip.mycode.init.base.discard0
import com.tkip.mycode.funs.common.textListener


class ViewMediaEditText : LinearLayout {
      lateinit var b: ViewMediaEdittextBinding

    constructor(context: Context ,  media: Media) : super(context) {
        b = DataBindingUtil.inflate(LayoutInflater.from(context), R.layout.view_media_edittext, this, true)
        b.media = media


//        MediaMapSelected[media.name] = media.userId

        b.etUserId.textListener(
                after = {s->
                    media.userId = s.toString().discard0()
                    if(s.toString().isEmpty()) media.userId = null
                },
                before = { _, _, _, _ -> },
                on = { s, _, _, c ->
//                    media.userId = s.toString()
//                    MediaMapSelected[media.name] = s.toString()
                })

    }

    constructor(context: Context) : super(context)



}