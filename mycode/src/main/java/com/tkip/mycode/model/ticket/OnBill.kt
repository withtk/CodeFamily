package com.tkip.mycode.model.ticket

import android.app.Activity
import android.content.Context
import android.content.Intent
import com.android.billingclient.api.*
import com.tkip.mycode.R
import com.tkip.mycode.dialog.toast.toastAlert
import com.tkip.mycode.dialog.toast.toastNormal
import com.tkip.mycode.dialog.toast.toastWarning
import com.tkip.mycode.funs.common.lllogI
import com.tkip.mycode.funs.common.lllogW
import com.tkip.mycode.init.PUT_RV_TYPE
import com.tkip.mycode.init.PUT_UID
import com.tkip.mycode.util.lib.TransitionHelper


class OnBill {

    companion object {

        fun gotoBillListActivity(context: Context,uid:String?) {
            val intent = Intent(context, BillListActivity::class.java)
            intent.putExtra(PUT_UID,uid)
            intent.putExtra(PUT_RV_TYPE,if(uid==null)TicketAdapter.rvBillManage else TicketAdapter.rvBillNormal)
            TransitionHelper.startTran(context, intent, null)
        }



        /**
         * 구매완료 후 로직
         */
//        val purchasesUpdatedListener by lazy {
//            PurchasesUpdatedListener { billingResult, purchases ->
//
//                when (billingResult.responseCode) {
//                    /*** 구매완료 */
//                    BillingClient.BillingResponseCode.OK -> {
//                        if (!purchases.isNullOrEmpty()) {
//                            lllogI("OnBill onPurchasesUpdated   billing OK  purchases.size  ${purchases.size}")
//                            purchases.forEach {
//                                /*when(it.purchaseState){
//                                    Purchase.PurchaseState.PURCHASED->
//                                    Purchase.PurchaseState.UNSPECIFIED_STATE->
//                                    Purchase.PurchaseState.PENDING->
//                                }*/
//                                lllogI("OnBill onPurchasesUpdated   billing OK  orderId ${it.orderId}")
//                                lllogI("OnBill onPurchasesUpdated   billing OK  accountIdentifiers ${it.accountIdentifiers}")
//                                lllogI("OnBill onPurchasesUpdated   billing OK  developerPayload ${it.developerPayload}")
//                                lllogI("OnBill onPurchasesUpdated   billing OK  isAcknowledged ${it.isAcknowledged}")
//                                lllogI("OnBill onPurchasesUpdated   billing OK  isAutoRenewing ${it.isAutoRenewing}")
//                                lllogI("OnBill onPurchasesUpdated   billing OK  originalJson ${it.originalJson}")
//                                lllogI("OnBill onPurchasesUpdated   billing OK  packageName ${it.packageName}")
//                                lllogI("OnBill onPurchasesUpdated   billing OK  purchaseState ${it.purchaseState}")
//                                lllogI("OnBill onPurchasesUpdated   billing OK  purchaseTime ${it.purchaseTime}")
//                                lllogI("OnBill onPurchasesUpdated   billing OK  purchaseToken ${it.purchaseToken}")
//                                lllogI("OnBill onPurchasesUpdated   billing OK  signature ${it.signature}")
//                                lllogI("OnBill onPurchasesUpdated   billing OK  sku ${it.sku}")
//                                // 한개씩만 구매하는 것 아닌가? 한개만 받아오면 되는 것 같은데..?
////                                purchased?.invoke(it)
////                                onSucces()
//
//
//                            }
//                        }
//                    }
//                    else -> {
//                        /***  구매 안 됨  */
////                        onFail()
//                        when (billingResult.responseCode) {
//                            BillingClient.BillingResponseCode.USER_CANCELED -> toastNormal(R.string.error_bill_user_canceled)   // 로직 중간에 사용자의 구매취소
//                            BillingClient.BillingResponseCode.BILLING_UNAVAILABLE -> toastNormal("BILLING_UNAVAILABLE")
//                            BillingClient.BillingResponseCode.DEVELOPER_ERROR -> toastNormal("DEVELOPER_ERROR")
//                            BillingClient.BillingResponseCode.ERROR -> toastNormal("ERROR")
//                            BillingClient.BillingResponseCode.FEATURE_NOT_SUPPORTED -> toastNormal("FEATURE_NOT_SUPPORTED")
//                            BillingClient.BillingResponseCode.ITEM_ALREADY_OWNED -> toastNormal("ITEM_ALREADY_OWNED")
//                            BillingClient.BillingResponseCode.ITEM_NOT_OWNED -> toastNormal("ITEM_NOT_OWNED")
//                            BillingClient.BillingResponseCode.ITEM_UNAVAILABLE -> toastNormal("ITEM_UNAVAILABLE")
//                            BillingClient.BillingResponseCode.SERVICE_DISCONNECTED -> toastNormal("SERVICE_DISCONNECTED")
//                            BillingClient.BillingResponseCode.SERVICE_TIMEOUT -> toastNormal("SERVICE_TIMEOUT")
//                            BillingClient.BillingResponseCode.SERVICE_UNAVAILABLE -> toastNormal("SERVICE_UNAVAILABLE")
//
//                        }
//                    }
//                }
//
//            }
//        }


        /***  billingClient 만들기 */
//        fun billingClient(context: Context) = BillingClient.newBuilder(context)
//                .setListener(purchasesUpdatedListener)
//                .enablePendingPurchases()
//                .build()

        /**************************************************************************************
         * billingClient 연결 및 결제 시작.
         * onStartFlow : 결제 라이브러리 시작.
         * onFail : 그 와중에 실패.
         *************************************************************************************/
        fun connectBillingClient(activity: PurchaseActivity, billingClient: BillingClient, skuID: String,onStartFlow:()->Unit,onFail:()->Unit) {
            activity.working = true

            val clientState = object : BillingClientStateListener {
                /*** 연결 성공 */
                override fun onBillingSetupFinished(billingResult: BillingResult) {
                    lllogI("OnBill onBillingSetupFinished  initalized success responseCode:${billingResult.responseCode}")

                    when (billingResult.responseCode) {
                        BillingClient.BillingResponseCode.OK -> {
                            // The BillingClient is ready. You can query purchases here.
                            showPurchaseDialog(activity, skuID, billingClient,onStartFlow,onFail)
                        }
                        else -> {
                            /***  오류  */
                            when (billingResult.responseCode) {
                                BillingClient.BillingResponseCode.USER_CANCELED -> toastNormal(R.string.error_bill_user_canceled)
                                BillingClient.BillingResponseCode.BILLING_UNAVAILABLE -> toastWarning("BILLING_UNAVAILABLE")
                                BillingClient.BillingResponseCode.DEVELOPER_ERROR -> toastWarning("DEVELOPER_ERROR")
                                BillingClient.BillingResponseCode.ERROR -> toastWarning("ERROR")
                                BillingClient.BillingResponseCode.FEATURE_NOT_SUPPORTED -> toastWarning("FEATURE_NOT_SUPPORTED")
                                BillingClient.BillingResponseCode.ITEM_ALREADY_OWNED -> toastWarning("ITEM_ALREADY_OWNED")
                                BillingClient.BillingResponseCode.ITEM_NOT_OWNED -> toastWarning("ITEM_NOT_OWNED")
                                BillingClient.BillingResponseCode.ITEM_UNAVAILABLE -> toastWarning("ITEM_UNAVAILABLE")
                                BillingClient.BillingResponseCode.SERVICE_DISCONNECTED -> toastWarning("SERVICE_DISCONNECTED")
                                BillingClient.BillingResponseCode.SERVICE_TIMEOUT -> toastWarning("SERVICE_TIMEOUT")
                                BillingClient.BillingResponseCode.SERVICE_UNAVAILABLE -> toastWarning("SERVICE_UNAVAILABLE")
                            }
                            toastAlert(R.string.error_bill_error)
                            onFail()
                        }
                    }
                }

                /*** 연결 실패 */
                override fun onBillingServiceDisconnected() {
                    lllogI("OnBill onBillingServiceDisconnected  error")
                    onFail()
                    // Try to restart the connection on the next request to
                    // Google Play by calling the startConnection() method.
                }
            }

            /*** 연결 시도 */
            billingClient.startConnection(clientState)
        }


        /***************************************************************************************
         * Sku List
         *************************************************************************************/

//        const val SKU_CRD_300 = "crd_300"
//        const val SKU_CRD_1000 = "crd_1000"
//        const val SKU_CRD_3000 = "crd_3000"
//        const val SKU_CLUB_3 = "club_3"

//        const val SKU_TEST_OK = "android.test.purchased"
//        const val SKU_TEST_CANCEL = "android.test.canceled"
//        const val SKU_TEST_NO_ITEM = "item_unavailable"


        /***************************************************************************************
         * 다이얼로그 띄우기
         *************************************************************************************/
        private fun showPurchaseDialog(activity: Activity, skuID: String, billingClient: BillingClient, onStartFlow:()->Unit, onFail:()->Unit) {
            lllogI("OnBill showPurchaseDialog     start ")

            // Retrieve a value for "skuDetails" by calling querySkuDetailsAsync().

//            val skuDetailsResult =
//                    GlobalScope.launch {
//                        withContext(Dispatchers.IO) {
//                            billingClient.querySkuDetails(skuParamsBuilder().build())
//                        }
//                    }


            billingClient.querySkuDetailsAsync(skuParamsBuilder(skuID).build()) { billingResult, skuDetailsList ->
                if (billingResult.responseCode == BillingClient.BillingResponseCode.OK && !skuDetailsList.isNullOrEmpty()) {
                    skuDetailsList.forEach { skuDetails ->
                        val sku = skuDetails.sku
                        val price = skuDetails.price
                        lllogI("OnBill showPurchaseDialog sku($sku) price($price)")


                        if (OnTicket.skuIdMap.contains(skuID)) {

                            lllogI("OnBill showPurchaseDialog   skuID:$skuID")
                            val flowParams = BillingFlowParams.newBuilder()
                                    .setSkuDetails(skuDetails)
                                    .build()
                            billingClient.launchBillingFlow(activity, flowParams)
                            onStartFlow()

                        } else{
                            toastAlert(R.string.error_bill_no_sku)
                            onFail()
                        }


                    }
                } else {
                    /*** error code : 인앱 상품이 없습니다. */
                    lllogW("OnBill showPurchaseDialog   responseCode error")
                    toastAlert(R.string.error_bill_no_sku)
                    onFail()
                }
            }
        }

        private fun skuParamsBuilder(skuID: String): SkuDetailsParams.Builder {
            val skuList = ArrayList<String>()
            skuList.add(skuID)
            val params = SkuDetailsParams.newBuilder()
            return params.setSkusList(skuList).setType(BillingClient.SkuType.INAPP)
        }


        /**************************************************************************************
         *  결제 성공 후 : 소비 처리.
         *************************************************************************************/
        private fun BillingClient.handlePurchase(purchase: Purchase) {
            val consumeParams = ConsumeParams.newBuilder().setPurchaseToken(purchase.purchaseToken).build()
            this.consumeAsync(consumeParams) { billingResult, purchaseToken ->
                // todo : peer 크레딧 증가.
                // todo : amount                // purchase data 클래스 만들기.  구매데이타는 따로 관리.
//                val amount = 5500
//                batch().apply {
//                    Cost.take(null, ActType.TAKE_CREDIT, amount, null)
//                    Cost.new(null,ActType.BUY_CREDIT, amount, null)
//                    myCommit("handlePurchase", success = {}, fail = {})
//                }
            }

//            when (purchase.purchaseState) {
//                Purchase.PurchaseState.PURCHASED -> lllogI("OnBill handlePurchase purchaseState ${purchase.purchaseState}")
//                Purchase.PurchaseState.PENDING -> lllogI("OnBill handlePurchase purchaseState ${purchase.purchaseState}")
//                Purchase.PurchaseState.UNSPECIFIED_STATE -> lllogI("OnBill handlePurchase purchaseState ${purchase.purchaseState}")
//            }
        }

    }

}