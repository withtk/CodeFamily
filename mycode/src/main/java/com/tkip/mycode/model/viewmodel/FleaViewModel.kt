package com.tkip.mycode.model.viewmodel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.tkip.mycode.funs.common.lllogI
import com.tkip.mycode.init.EES_FLEA
import com.tkip.mycode.model.count.Count
import com.tkip.mycode.model.flea.Flea
import com.tkip.mycode.model.my_enum.Cntch
import com.tkip.mycode.model.my_enum.Status
import com.tkip.mycode.util.lib.retrofit2.ESget
import com.tkip.mycode.util.lib.retrofit2.ESquery
import com.tkip.mycode.util.tools.fire.Firee

class FleaViewModel : ViewModel() {

    val liveFleaList = ListLiveData<Flea>()

    val liveBookmark = MutableLiveData<Count>()
    val livePush = MutableLiveData<Count>()

    fun getEsFleaList(size: Int, from: Int,   status: Status): ListLiveData<Flea> {
        val query = ESquery.fleaList(size, from,  status)
        ESget.getFleaList(query,
                success = {
                    lllogI("myCallll success commonGetEs FleaViewModel ")

                    if (it == null) {
                        liveFleaList.clear(true)
                    } else {
                        if (from == 0) liveFleaList.clear(false)
                        liveFleaList.addAll(it)
                    }
                }, fail = {
            lllogI("myCallll fail commonGetEs FleaViewModel ")

        })
        return liveFleaList
    }

    /********************************************************************************************
     ********************************* Count ***************************************************/

//    fun getAllEsCount(flea: Flea) {
//        getEsCountList(flea, Cntch.BOOK)
//        getEsCountList(flea, Cntch.PUSH)
//    }
//
//    private fun getEsCountList(flea: Flea, cnt: Cntch): LiveData<Count> {
//        ESget.getCountList(cnt.getTopChild(DataType.FLEA), ESquery.countUidModelKey(flea.key), success = {
//            if (it == null || it.isEmpty()) {
//                setLiveCount(cnt, null)
//            } else {
//                setLiveCount(cnt, it[0])
//            }
//        }, fail = {})
//        return returnLiveCount(cnt)
//    }
//
//    fun getCount(flea: Flea, cnt: Cntch): LiveData<Count> {
//        Firee.getCount(cnt.getTopChild(DataType.FLEA), flea.key, success = { setLiveCount(cnt, it) }, fail = {})
//        return returnLiveCount(cnt)
//    }
//
//    private fun returnLiveCount(cnt: Cntch) = when (cnt) {
//        Cntch.BOOK -> liveBookmark
//        Cntch.PUSH -> livePush
//        else -> liveBookmark
//    }
//
//    private fun setLiveCount(cnt: Cntch, count: Count?) {
//        when (cnt) {
//            Cntch.BOOK -> liveBookmark.value = count
//            Cntch.PUSH -> livePush.value = count
//            else -> liveBookmark.value = count
//        }
//    }

//    private fun Cntch.count() =
//            when (this) {
//                Cntch.BOOK -> liveBookmark.value
//                Cntch.PUSH -> livePush.value
//                else -> null
//            }

//    fun updateCount(flea: Flea, cnt: Cntch, v: View) {
//        cnt.updateCount(flea, cnt.count(), v, success = { on, count1 ->
//            if (on) setLiveCount(cnt, count1)
//            else setLiveCount(cnt, null)
//        }, fail = {})
//    }

    fun addViewUID(flea: Flea, cnt: Cntch) {
        Firee.addViewUID(EES_FLEA, Count.create(flea, cnt), {}, {})
    }


}