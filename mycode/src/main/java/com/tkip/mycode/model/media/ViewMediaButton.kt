package com.tkip.mycode.model.media

import android.content.Context
import android.view.LayoutInflater
import android.widget.LinearLayout
import androidx.databinding.DataBindingUtil
import com.tkip.mycode.R
import com.tkip.mycode.databinding.ViewMediaButtonBinding
import com.tkip.mycode.funs.common.OnFunction


class ViewMediaButton : LinearLayout {
    private lateinit var b: ViewMediaButtonBinding

    constructor(context: Context) : super(context)
    constructor(context: Context , media: Media) : super(context) {

        b = DataBindingUtil.inflate(LayoutInflater.from(context), R.layout.view_media_button, this, true)
        b.media=media

        b.btnUserId.setOnClickListener{
            //todo: 미디어 어플로 이동 or 복사
            media.onClick(context)
            OnFunction.setClipBoardLink(b.btnUserId.text.toString())
//            TToast.showNormal(R.string.info_prepare_developing)
//            onClick(myMedia)
        }

    }

//
//    constructor(context: Context, attrs: AttributeSet?) : super(context, attrs) {
//    }
//
//    constructor(context: Context, attrs: AttributeSet?, defStyleAttr: Int) : super(context, attrs, defStyleAttr) {
//    }


}