package com.tkip.mycode.model.count.quick

import android.content.Context
import android.content.Intent
import android.view.View
import com.tkip.mycode.R
import com.tkip.mycode.dialog.toast.TOAST_NORMAL
import com.tkip.mycode.dialog.toast.checkAndToast
import com.tkip.mycode.dialog.toast.toastNormal
import com.tkip.mycode.init.PUT_ARRAY_COUNT
import com.tkip.mycode.init.QUICK_MENU_LIMIT
import com.tkip.mycode.init.base.getStr
import com.tkip.mycode.init.MY_QUICK_COUNT
import com.tkip.mycode.model.count.Count
import com.tkip.mycode.util.lib.TransitionHelper

class OnQuick {
    companion object {

//        private const val QUICK_MENU_LIMIT = 20
//        var quickCount = 0

        /**
         */
        fun gotoQuickManageActivity(context: Context, items: ArrayList<Count>, arrPair: Array<androidx.core.util.Pair<View, String>>?) {
            Intent(context, QuickManageActivity::class.java).apply {
                addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
                putExtra(PUT_ARRAY_COUNT, items)
                TransitionHelper.startTran(context, this, null)
            }
        }

        /**
         * 퀵메뉴는 20개까지만 가능.
         */
        fun overLimitQuickCount(): Boolean {
            if (MY_QUICK_COUNT < QUICK_MENU_LIMIT) {
                return false
            } else {
                val msg = String.format(R.string.alert_over_limit_quick.getStr(), QUICK_MENU_LIMIT)
                toastNormal(msg)
                return true
            }
        }


    }
}










