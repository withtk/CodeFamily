package com.tkip.mycode.model.board

import android.os.Parcel
import android.os.Parcelable
import com.tkip.mycode.init.base.createKey
import com.tkip.mycode.model.my_enum.Status
import com.tkip.mycode.model.peer.mynick
import com.tkip.mycode.model.peer.myuid
import com.tkip.mycode.model.store.Store
import com.tkip.mycode.util.tools.date.rightNow
import java.util.*

/**
 * 기본값은 변경금지 : 변경하려면 constructor도 모두 변경해야 함.
 */
data class Board(

        var key: String = createKey(),            // Store의 게시판은 store.key
        var authorUID: String = myuid(),
        var authorNick: String = mynick(),     // 매번 보드에 들어갈때마다 마스터이름과 비교하여 db 업데이트할 것.
        var boardTypeNo: Int = BoardType.USER.no,   // 보드  타입.

        //////////////// 수정 가능한 부분 ////////////////////////////////
        var title: String = "",         // 수정불가 : 북마크한 유저에게 혼란 야기 가능성 때문.
        var comment: String? = null,    // 안내 및 공지사항.
        var commentNo: Int = 1,         // 공지사항 번호에 따라 새로운 공지사항여부 결정됨.

        var photoUrl: String? = null,
        var photoThumb: String? = null,
        var tagList: ArrayList<String>? = null,    // 최대 10개까지만, 검색 키워드광고와 매치될 부분이기 때문에 남용 우려로 수량 제한.

        //////////////////  보드마스터가 선택하는 부분. //////////////////////////////
        var boardId: String? = null,       // ID를 이용하여 한번에 검색. (수정가능?)
        var allowPost: Boolean = true,     // 포스팅 허용.
        var allowSearch: Boolean = true,   // 보드 검색 허용.

        //////////////////  for admin. //////////////////////////////
        var adminAllowPost: Boolean = true,     //  allowPost 스위치 허용 여부(어드민의 설정)

        /**
         * 삭제는 불가. 왜냐면 나중에 검색결과에 나와야 하기 때문에.
         * 비공개 보드의 포스팅은 검색을 어떻게 해야 하나..?
         */

        //////////////////////// for author //////////////////////////
        var adCredit: Int = 0,       // 현재 검색 순위에 적용중인 credit
        var totalAdCredit: Int = 0,  // 그동안 결제한 totalAdCredit
        var decoSearchNo: Int = 0,     // search 결과에 나오는 holder 종류. 차후에 만들 예정.
        var decoBookNo: Int = 0,   // 유저가 bookmark 했을때 나오는 holder 종류. 차후에 만들 예정.

        ////////////////////// Count /////////////////////////////////
        var bookmarkCount: Int = 0,      // board bookmark
        var pushCount: Int = 0,       // push받는 유저수.
        var viewCount: Int = 0,       // 뷰 횟수. 전체 post의 view 카운트  : functions에서 자동.

        //////////////////////////////////////////////////////////////
        var postCount: Int = 0,      // post 수  : functions에서 자동.
        var warnCount: Int = 0,       // 신고 횟수.
        var lastPostDate: Long? = null,  // 마지막으로 post가 올라간 시간.  : functions에서 자동.  (최근업데이트 게시판 sort)

        var totalPoint: Long = 0,   //   postTotalCount + bookmarkCount + viewCount - warnCount + reply
        var statusNo: Int = Status.NORMAL.no,       // 최초 작성시 NORMAL
        var addDate: Long = rightNow(),
        var updateDate: Long? = null

) : Parcelable {
    /**     * Cs Board 생성    */
    constructor(store: Store) : this(
            title = store.title,
            boardTypeNo = BoardType.CS_CENTER.no,
            allowPost = true,
            allowSearch = true,
            adminAllowPost = true
    )

    /**     * clone    */
    constructor(board: Board) : this(
            key = board.key,
            authorUID = board.authorUID,
            authorNick = board.authorNick,
            boardTypeNo = board.boardTypeNo,

            title = board.title,
            comment = board.comment,
            commentNo = board.commentNo,

            photoUrl = board.photoUrl,
            photoThumb = board.photoThumb,
            tagList = board.tagList,

            boardId = board.boardId,
            allowPost = board.allowPost,
            allowSearch = board.allowSearch,

            adminAllowPost = board.adminAllowPost,

            adCredit = board.adCredit,
            totalAdCredit = board.totalAdCredit,
            decoSearchNo = board.decoSearchNo,
            decoBookNo = board.decoBookNo,

            bookmarkCount = board.bookmarkCount,
            pushCount = board.pushCount,
            viewCount = board.viewCount,

            postCount = board.postCount,
            warnCount = board.warnCount,
            lastPostDate = board.lastPostDate,

            totalPoint = board.totalPoint,
            statusNo = board.statusNo,
            addDate = board.addDate,
            updateDate = board.updateDate
    )

    constructor(source: Parcel) : this(
            source.readString()!!,
            source.readString()!!,
            source.readString()!!,
            source.readInt(),
            source.readString()!!,
            source.readString(),
            source.readInt(),
            source.readString(),
            source.readString(),
            source.createStringArrayList(),
            source.readString(),
            1 == source.readInt(),
            1 == source.readInt(),
            1 == source.readInt(),
            source.readInt(),
            source.readInt(),
            source.readInt(),
            source.readInt(),
            source.readInt(),
            source.readInt(),
            source.readInt(),
            source.readInt(),
            source.readInt(),
            source.readValue(Long::class.java.classLoader) as Long?,
            source.readLong(),
            source.readInt(),
            source.readLong(),
            source.readValue(Long::class.java.classLoader) as Long?
    )

    override fun describeContents() = 0

    override fun writeToParcel(dest: Parcel, flags: Int) = with(dest) {
        writeString(key)
        writeString(authorUID)
        writeString(authorNick)
        writeInt(boardTypeNo)
        writeString(title)
        writeString(comment)
        writeInt(commentNo)
        writeString(photoUrl)
        writeString(photoThumb)
        writeStringList(tagList)
        writeString(boardId)
        writeInt((if (allowPost) 1 else 0))
        writeInt((if (allowSearch) 1 else 0))
        writeInt((if (adminAllowPost) 1 else 0))
        writeInt(adCredit)
        writeInt(totalAdCredit)
        writeInt(decoSearchNo)
        writeInt(decoBookNo)
        writeInt(bookmarkCount)
        writeInt(pushCount)
        writeInt(viewCount)
        writeInt(postCount)
        writeInt(warnCount)
        writeValue(lastPostDate)
        writeLong(totalPoint)
        writeInt(statusNo)
        writeLong(addDate)
        writeValue(updateDate)
    }

    companion object {
        fun setDefaultBoard() {

//            OnBoard.getBoard(PH_CODE_BOARD_KEY, onBoard = {
//                phCodeBoard = it
//                Sred.putObjectBoard(PH_CODE_BOARD_KEY, it)
//            }, onFail = {})
//            OnBoard.getBoard(RECENT_BOARD_KEY, onBoard = {
//                recentBoard = it
//                Sred.putObjectBoard(RECENT_BOARD_KEY, it)
//            }, onFail = {})
//            OnBoard.getBoard(FLEA_BOARD_KEY, onBoard = {
//                fleaBoard = it
//                Sred.putObjectBoard(FLEA_BOARD_KEY, it)
//            }, onFail = {})
//            OnBoard.getBoard(ROOM_BOARD_KEY, onBoard = {
//                roomBoard = it
//                Sred.putObjectBoard(ROOM_BOARD_KEY, it)
//            }, onFail = {})


//            lllogI("setDefaultBoard PH_CODE_BOARD_KEY $PH_CODE_BOARD_KEY")
//            lllogI("setDefaultBoard FLEA_BOARD_KEY $FLEA_BOARD_KEY")
//            lllogI("setDefaultBoard ROOM_BOARD_KEY $ROOM_BOARD_KEY")


//            Sred.getObjectBoard(PH_CODE_BOARD_KEY)?.let {
//                lllogI("getObjectBoard phCodeBoard $it")
//                phCodeBoard = it
//                recentBoard = Sred.getObjectBoard(RECENT_BOARD_KEY)
//                fleaBoard = Sred.getObjectBoard(FLEA_BOARD_KEY)
//                roomBoard = Sred.getObjectBoard(ROOM_BOARD_KEY)
//            } ?: OnEs.createBasicBoard()

//            lllogI("setDefaultBoard phCodeBoard $phCodeBoard")
//            lllogI("setDefaultBoard fleaBoard $fleaBoard")
//            lllogI("setDefaultBoard roomBoard $roomBoard")


            /**         * 기본 보드 셋팅         */
//            board1 = Sred.getObjectBoard(BOARD_NO1)
//            if (board1 == null) board1 = phCodeBoard


//            /** getBoard : 오늘의 베스트 보드 */
//            ESget.getBoardList(ESquery.boardList(1, BoardType.NORMAL.no, CH_TOTAL_POINT, Status.NORMAL),
//                    success = {
//                        lllogI("setDefaultBoard  getBoardList  success")
//
//                        if (it.isNullOrEmpty()) {
//                            OnException.dbNoData("getBoardList in Board")
//                        } else {
//                            board1 = it[0]
//                            lllogI("setDefaultBoard success board1 $board1 ")
//                            Sred.putObjectBoard(BOARD_NO1, board1)
//                        }
//                    }, fail = { lllogI("setDefaultBoard  getBoardList  fail") })
//
//            /** getBoard : 중고거래 보드 */
//            OnBoard.getBoard(FLEA_BOARD_KEY,{ fleaBoard = it},{})
//            /** getBoard : 부동산 보드 */
//            OnBoard.getBoard(ROOM_BOARD_KEY,{ roomBoard = it},{})


        }

        @JvmField
        val CREATOR: Parcelable.Creator<Board> = object : Parcelable.Creator<Board> {
            override fun createFromParcel(source: Parcel): Board = Board(source)
            override fun newArray(size: Int): Array<Board?> = arrayOfNulls(size)
        }
    }
}
