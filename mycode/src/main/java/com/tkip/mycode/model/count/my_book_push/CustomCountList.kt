package com.tkip.mycode.model.count.my_book_push

import android.content.Context
import android.view.LayoutInflater
import android.widget.LinearLayout
import androidx.databinding.DataBindingUtil
import com.tkip.mycode.R
import com.tkip.mycode.databinding.CustomModelListBinding
import com.tkip.mycode.funs.common.lllogI
import com.tkip.mycode.init.base.getInt
import com.tkip.mycode.model.count.Count
import com.tkip.mycode.model.count.CountType
import com.tkip.mycode.model.my_enum.DataType
import com.tkip.mycode.model.peer.myuid
import com.tkip.mycode.util.lib.retrofit2.ESget
import com.tkip.mycode.util.lib.retrofit2.ESquery
import com.tkip.mycode.util.tools.recycler.OnRv

/**
 * my book, push, reply,
 */
class CustomCountList : LinearLayout {
    private lateinit var b: CustomModelListBinding
    private var manage: Boolean = false
    private lateinit var uid: String
    private lateinit var index: String
    private var dataTypeNo: Int = DataType.BOARD.no

    private val items = arrayListOf<Count>()
    lateinit var adapter: CountAdapter

    private val esSize by lazy { R.integer.es_size_count.getInt() }
    private var esLastIndex = 0

    constructor(context: Context) : super(context)
    constructor(context: Context, manage: Boolean, uid: String?, dataType: DataType, countType: CountType, onItem:((Count)->Unit)?) : super(context) {
        b = DataBindingUtil.inflate(LayoutInflater.from(context), R.layout.custom_model_list, this, true)
        this.manage = manage
        this.uid = uid?: myuid()
        this.dataTypeNo = dataType.no
        this.index = countType.getTopChild(dataType)   // index 세팅.

        /** set Adapter */
        val rvType = if(countType == CountType.BOOK) CountAdapter.rvMyBook else CountAdapter.rvMyPush
        adapter = CountAdapter(context,items, rvType)
        b.inRefresh.rv.adapter = adapter
        b.inRefresh.inData.tvNoData.text = countType.noDataMsg
                //        SnapHelper().attachToRecyclerView(b.rv)   // 스냅헬퍼


        b.inRefresh.refresh.setOnRefreshListener { getEsDataList(0) }
        b.inRefresh.refresh.setOnLoadMoreListener { getEsDataList(esLastIndex) }
//        b.refresh.setEnableRefresh(false)   //상단 refresh는 불능
//        b.refresh.setEnableLoadMore(false)  // 하단 loadMore 불능.
        b.inRefresh.inData.tvNoData.text = countType.noDataMsg


        getEsDataList(0)

    }


    /** * 가져오기  */
    private fun getEsDataList(from: Int) {

        val query = ESquery.myCount(esSize, from, uid, dataTypeNo)
        ESget.getCountList("CustomCountList getEsDataList ",index, query,
                success = {
                    lllogI("CustomCountList getEsDataList size $index : ${it?.size}")
//                    esLastIndex = OnRv.setRvList2(it,from, items, b.inData, R.string.info_no_my_banner.getStr(), b.refresh,b.tvLast)
                    esLastIndex = OnRv.setRvRefresh(it, from, items, b.inRefresh,true)
                    adapter.notifyDataSetChanged()
                },
                fail = {})

    }
}