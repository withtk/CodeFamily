package com.tkip.mycode.model.manage

import android.os.Parcel
import android.os.Parcelable
import com.tkip.mycode.init.NOTICE_KEY
import com.tkip.mycode.model.peer.myuid
import com.tkip.mycode.util.tools.date.rightNow

/**
 * 기본사항에 대한 정보. ( remoteControl 보다 더 실시간 처리 )
 * no Es
 * Notice 변경될 때 실시간으로 다이얼로그 발동.
 */

data class Notice(

        var key: String = NOTICE_KEY,    // 절대 변경 금지. 이 키는 모두의 키. 모든 유저가 이걸 실시간 리슨해야 하기때문에.
        var authorUID: String = myuid(),

        /////////////// 공지사항 /////////////////
        var no: Int = 0,
        var title: String = "title",
        var comment: String = "comment",
        var urgent: Boolean = false,

        var addDate: Long = rightNow()

) : Parcelable {
    constructor(notice1: Notice) : this(
            key = notice1.key,
            authorUID = notice1.authorUID,
            no = notice1.no,
            title = notice1.title,
            comment = notice1.comment,
            urgent = notice1.urgent,
            addDate = notice1.addDate
    )

    constructor(title: String, comment: String, noticeNo: Int, urgent: Boolean) : this(
            key = NOTICE_KEY,
            no = noticeNo,
            title = title,
            comment = comment,
            urgent = urgent
    )

    constructor(source: Parcel) : this(
            source.readString()!!,
            source.readString()!!,
            source.readInt(),
            source.readString()!!,
            source.readString()!!,
            1 == source.readInt(),
            source.readLong()
    )

    override fun describeContents() = 0

    override fun writeToParcel(dest: Parcel, flags: Int) = with(dest) {
        writeString(key)
        writeString(authorUID)
        writeInt(no)
        writeString(title)
        writeString(comment)
        writeInt((if (urgent) 1 else 0))
        writeLong(addDate)
    }

    companion object {
        @JvmField
        val CREATOR: Parcelable.Creator<Notice> = object : Parcelable.Creator<Notice> {
            override fun createFromParcel(source: Parcel): Notice = Notice(source)
            override fun newArray(size: Int): Array<Notice?> = arrayOfNulls(size)
        }
    }
}