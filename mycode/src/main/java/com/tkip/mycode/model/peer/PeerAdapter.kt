package com.tkip.mycode.model.peer

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.RecyclerView
import com.tkip.mycode.R
import com.tkip.mycode.ad.OnBanner
import com.tkip.mycode.databinding.HolderPeerSearchBinding
import com.tkip.mycode.databinding.HolderPeerVertBinding
import com.tkip.mycode.dialog.admin.PeerDialog
import com.tkip.mycode.funs.common.tranPair
import com.tkip.mycode.init.RV_MY_CONTENTS_MANAGE
import com.tkip.mycode.init.RV_SEARCH
import com.tkip.mycode.init.RV_VERT
import com.tkip.mycode.model.cost.OnCost
import com.tkip.mycode.nav.my_room.OnMyContents
import com.tkip.mycode.util.tools.etc.OnMyClick

/**
 * Peer list
 */
class PeerAdapter(private val items: ArrayList<Peer>, val rvType: Int, val onItemClick: ((Peer) -> Unit)?) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {
    private lateinit var activity: AppCompatActivity

    companion object {
//        const val rvVert = 100
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        this.activity = parent.context as AppCompatActivity
        return when (rvType) {
            RV_VERT -> PeerVertViewHolder(HolderPeerVertBinding.inflate(LayoutInflater.from(parent.context), parent, false))
            RV_SEARCH -> PeerSearchViewHolder(HolderPeerSearchBinding.inflate(LayoutInflater.from(parent.context), parent, false))
            else -> PeerSearchViewHolder(HolderPeerSearchBinding.inflate(LayoutInflater.from(parent.context), parent, false))
        }
    }

    override fun onBindViewHolder(h: RecyclerView.ViewHolder, p: Int) {
        when (rvType) {
            RV_VERT -> (h as PeerVertViewHolder).bind(items[p])
            RV_SEARCH -> (h as PeerSearchViewHolder).bind(items[p])
            else -> (h as PeerSearchViewHolder).bind(items[p])
        }
    }


    override fun getItemCount(): Int {
        return items.size
    }

    /**
     * vert, admin
     */
    inner class PeerVertViewHolder(val b: HolderPeerVertBinding) : RecyclerView.ViewHolder(b.root) {

        private lateinit var peer: Peer

        fun bind(peer: Peer) {
            this.peer = peer
            b.holder = this
            b.peer = peer
            b.inMyCredit.peer = peer
            b.executePendingBindings()
        }

        fun onClickCard(v: View) {
            OnMyClick.setDisableAWhileBTN(v)

            onItemClick?.let {
                it(peer)
                return
            }

            PeerDialog.newInstance(peer).show(activity.supportFragmentManager, "dialog")
        }

        fun onClickCost(v: View) {
            OnMyClick.setDisableAWhileBTN(v)
            OnCost.gotoCostListActivity(activity, true, peer.uid, arrayOf(b.btnCost.tranPair(R.string.tran_linear)))
        }

        fun onClickBanner(v: View) {
            OnMyClick.setDisableAWhileBTN(v)
            OnBanner.gotoMyBannerActivity(activity, peer.uid, arrayOf(b.btnBanner.tranPair(R.string.tran_toolbar)))
        }

        fun onClickModel(v: View) {
            OnMyClick.setDisableAWhileBTN(v)
            OnMyContents.gotoMyContentsActivity(activity, peer.uid, RV_MY_CONTENTS_MANAGE, arrayOf(b.btnModel.tranPair(R.string.tran_toolbar)))
        }
    }


    /**
     * search
     */
    inner class PeerSearchViewHolder(val b: HolderPeerSearchBinding) : RecyclerView.ViewHolder(b.root) {

        private lateinit var peer: Peer

        fun bind(peer: Peer) {
            this.peer = peer
            b.holder = this
            b.peer = peer
            b.executePendingBindings()
        }

        fun onClickCard(v: View) {
            OnMyClick.setDisableAWhileBTN(v)

            onItemClick?.let {
                it(peer)
                return
            }
        }
    }


}