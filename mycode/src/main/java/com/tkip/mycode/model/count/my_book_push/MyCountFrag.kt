package com.tkip.mycode.model.count.my_book_push

import android.os.Bundle
import com.tkip.mycode.R
import com.tkip.mycode.databinding.FragmentMyCountBinding
import com.tkip.mycode.init.PUT_COUNT_TYPE_NO
import com.tkip.mycode.init.PUT_DATA_TYPE_NO
import com.tkip.mycode.init.PUT_MANAGE
import com.tkip.mycode.init.PUT_UID
import com.tkip.mycode.init.my_bind.BindingFragment
import com.tkip.mycode.model.count.Count
import com.tkip.mycode.model.count.getCountType
import com.tkip.mycode.model.my_enum.dataType
import com.tkip.mycode.model.peer.myuid


class MyCountFrag : BindingFragment<FragmentMyCountBinding>() {
    override fun getLayoutResId(): Int = R.layout.fragment_my_count



    private val manage by lazy { arguments?.getBoolean(PUT_MANAGE, false) ?: false }
    private val uid by lazy { arguments?.getString(PUT_UID)?: myuid() }
    private val dataTypeNo by lazy { arguments?.getInt(PUT_DATA_TYPE_NO) }
    private val countTypeNo by lazy { arguments?.getInt(PUT_COUNT_TYPE_NO) }

    private lateinit var onItemClick: (Count) -> Unit

    companion object {
        @JvmStatic
        fun newInstance(manage: Boolean, uid: String, dataTypeNo: Int, countTypeNo: Int, onItemClick: (Count) -> Unit) = MyCountFrag().apply {
            arguments = Bundle().apply {
                putBoolean(PUT_MANAGE, manage)
                putString(PUT_UID, uid)
                putInt(PUT_DATA_TYPE_NO, dataTypeNo)
                putInt(PUT_COUNT_TYPE_NO, countTypeNo)
            }
            this.onItemClick = onItemClick
        }
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        countTypeNo?.let { cNo ->
            dataTypeNo?.let { dNo ->
                b.frame.addView(CustomCountList(mContext, manage, uid, dNo.dataType(), cNo.getCountType(), onItem = { onItemClick(it) }))
            }
        }
    }


}

