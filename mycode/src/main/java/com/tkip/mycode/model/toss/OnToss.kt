package com.tkip.mycode.model.toss

import android.content.Context
import android.widget.LinearLayout
import com.tkip.mycode.R
import com.tkip.mycode.dialog.OnDDD
import com.tkip.mycode.dialog.alert.MyAlert
import com.tkip.mycode.dialog.toast.TToast
import com.tkip.mycode.init.*
import com.tkip.mycode.model.my_enum.DataType
import com.tkip.mycode.model.my_enum.dataType
import com.tkip.mycode.model.my_enum.gteNormalStatus
import com.tkip.mycode.util.lib.retrofit2.ESget
import com.tkip.mycode.util.tools.anim.showSlideUp


class OnToss {
    companion object {

        /**
         * 첨부를 위한 toss List
         */
        fun showTossList(context: Context, currentModelKey: String, onToss: (Toss) -> Unit) {
            MyAlert.showTossList(context,
                    onClick = { toss ->
                        if (toss.modelKey == currentModelKey) TToast.showAlert(R.string.toss_error_current_item)
                        else {

                            toss.type = TOSS_PASTE

//                            inReplyBox.lineAttachBox.addView(ViewTossModel(context, toss, null, null,
//                                    onGoto = {},
//                                    onRemove = { OnReply.resetAttachReply(inReplyBox.lineAttachBox) },
//                                    onClose = {}
//                            ))

                            REPLY_ATTACH_TYPE = ATTACH_TOSS
                            REPLY_ATTACH_TOSS = toss
//                            AnimUtil.showNormal(inReplyBox.btnAdd)   // add버튼 활성화
                            onToss(toss)

//                                    b.inReplyBox.inToss.toss = toss
//                                    AnimUtil.showSlideUp(b.inReplyBox.lineToss, true)
//                                ViewUtil.addViewTossModel(this@FreePostActivity, b.lineToss, toss, onTossClick = {}, onClose = { onClickCancelToss() })
//                                AnimUtil.showSlideUp(b.lineToss, true)
                        }
                    })
        }


        /**
         * reply에 뿌려줄 toss Model Data 받아오기.
         */
//        fun getTossModelData(tossModelKey: String?, tossDataTypeNo: Int?, success: (Any?) -> Unit, fail: () -> Unit) {
//            tossModelKey?.let { key ->
//                tossDataTypeNo?.let { typeNo ->
//                    when (typeNo.dataType()) {
//                        DataType.POST -> ESget.getPost(key, success = { success(it) }, fail = { fail() })
//                        DataType.FLEA -> ESget.getFlea(key, success = { success(it) }, fail = { fail() })
//                        DataType.ROOM -> ESget.getRoom(key, success = { success(it) }, fail = { fail() })
//                        DataType.STORE -> ESget.getStore(key, success = { success(it) }, fail = { fail() })
//                        else -> {
//                        }
//                    }
//                }
//            }
//        }

        /**         복사하기 클릭시         */
//        fun copyToss(context: Context, board: Board?, any: Any) {
//            if (board == null) any.anyBoardKey()?.let { boardKey -> ESget.getBoard(boardKey, success = { checkAdd(context, it, any) }, fail = {}) }  // 보드 받아오기
//            else checkAdd(context, board, any)
//        }


        /**         * list에 TOSS 넣기.         */
        fun checkAdd(context: Context, any: Any) {
            if (any.gteNormalStatus())
                Toss.create(any, TOSS_COPY).apply {
                    myTossList.checkAdd(this)
                    MyAlert.showTossConfirm(context, this)
                }
            else OnDDD.info(context, R.string.toss_error_not_normal)

//            board?.let {
//                when (it.boardTypeNo.getBoardType()) {
//                    BoardType.FIXED, BoardType.USER -> {
//                        Toss.create(any, TOSS_COPY).apply {
//                            myTossList.checkAdd(this)
//                            MyAlert.showTossConfirm(context, this)
//                        }
//                    }
//                    else -> toastNormal(R.string.info_no_avil_toss)
//                }
//            }
        }


        /**********************************************************************************
         * addToss in Reply
         *         in Chat
         *********************************************************************************/
        fun attachToss(context: Context, parent: LinearLayout, tossModelKey: String?, tossDataTypeNo: Int?) {
          ViewTossModel(context, parent,tossModelKey, tossDataTypeNo, onGoto = null, onRemove = null, onClose = null, onLong = null)
//            parent.removeAllViews()
//            parent.addView(ViewTossModel(context, parent,tossModelKey, tossDataTypeNo, onGoto = null, onRemove = null, onClose = null, onLong = null))
//            parent.showSlideUp(true)
        }


    }

}