package com.tkip.mycode.model.viewmodel

import androidx.lifecycle.ViewModel
import com.cunoraz.tagview.TagView
import com.tkip.mycode.init.EES_STORE
import com.tkip.mycode.model.count.Count
import com.tkip.mycode.model.store.Store
import com.tkip.mycode.model.my_enum.Cntch
import com.tkip.mycode.model.my_enum.Status
import com.tkip.mycode.util.tools.fire.Firee
import com.tkip.mycode.funs.common.lllogI
import com.tkip.mycode.util.lib.retrofit2.ESget
import com.tkip.mycode.util.lib.retrofit2.ESquery

class StoreViewModel : ViewModel() {

//    val liveRoom = MutableLiveData<Store>()
    val liveStoreList = ListLiveData<Store>()
    val liveSearchList = ListLiveData<Store>()
//
//    val liveBookmark = MutableLiveData<Count>()
//    val livePush = MutableLiveData<Count>()
//    val liveWarn = MutableLiveData<Count>()


//    fun getRoom(roomKey: String): LiveData<Store> {
//        Firee.getRoom(roomKey, success = { it?.let { room1 -> liveRoom.value = room1 } }, fail = {})
//        return liveRoom
//    }


    fun getEsStoreList(size: Int, from: Int,  status: Status): ListLiveData<Store> {
        val query = ESquery.storeList(size, from, status)
        lllogI("getEsStoreList $query")
        ESget.getStoreList(query,
                success = {
                    lllogI("getEsStoreList list : ${it?.size}")

                    if (it == null  ) {
                        liveStoreList.clear(true)
                    } else {
                        if (from == 0) liveStoreList.clear(false)
                        liveStoreList.addAll(it)
                    }
                }, fail = {
            lllogI("getEsStoreList fail")
        })
        return liveStoreList
    }

    fun getEsSearch( size: Int, from: Int,  tagView:TagView, keyword:String? ): ListLiveData<Store> {
        lllogI("getEsSearch ")
        val query = ESquery.storeSearch( size,from,ESquery.getTermTaglist(tagView,keyword),Status.NORMAL)
        lllogI("getEsSearch $query")
        ESget.getStoreList(query,
                success = {
                    lllogI("getEsSearch list : ${it?.size}")
                    if (it == null  ) {
                        liveSearchList.clear(true)
                    } else {
                        if (from == 0) liveSearchList.clear(false)
                        liveSearchList.addAll(it)
                    }
                }, fail = {
            lllogI("getEsStoreList fail")
        })
        return liveSearchList
    }


/********************************************************************************************
     ********************************* Count ***************************************************//*

    fun getAllEsCount(store: Store) {
        getEsCountList(store, Cntch.BOOK)
        getEsCountList(store, Cntch.PUSH)
    }

    private fun getEsCountList(store: Store, cnt: Cntch): LiveData<Count> {
        ESget.getCountList(cnt.getTopChild(DataType.ROOM), ESquery.countUidModelKey(store.key), success = {
            if (it == null || it.isEmpty()) {
                setLiveCount(cnt, null)
            } else {
                setLiveCount(cnt, it[0])
            }
        }, fail = {})
        return returnLiveCount(cnt)
    }

    fun getCount(store: Store, cnt: Cntch): LiveData<Count> {
        Firee.getCount(cnt.getTopChild(DataType.ROOM), store.key, success = { setLiveCount(cnt, it) }, fail = {})
        return returnLiveCount(cnt)
    }

    private fun returnLiveCount(cnt: Cntch) = when (cnt) {
        Cntch.BOOK -> liveBookmark
        Cntch.PUSH -> livePush
        else -> liveBookmark
    }

    private fun setLiveCount(cnt: Cntch, count: Count?) {
        when (cnt) {
            Cntch.BOOK -> liveBookmark.value = count
            Cntch.PUSH -> livePush.value = count
            else -> liveBookmark.value = count
        }
    }

    private fun Cntch.count() =
            when (this) {
                Cntch.BOOK -> liveBookmark.value
                Cntch.PUSH -> livePush.value
                else -> null
            }

    fun updateCount(store: Store, cnt: Cntch, v: View) {
        cnt.updateCount(store, cnt.count(), v, success = { on, count1 ->
            if (on) setLiveCount(cnt, count1)
            else setLiveCount(cnt, null)
        }, fail = {})
    }

*/
    fun addViewUID(store: Store, cnt: Cntch) {
        Firee.addViewUID(EES_STORE, Count.create(store, cnt), {}, {})
    }


}