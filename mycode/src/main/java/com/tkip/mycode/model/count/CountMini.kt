package com.tkip.mycode.model.count

import android.os.Parcel
import android.os.Parcelable
import com.tkip.mycode.init.CH_COUNT
import com.tkip.mycode.init.base.createNanoKey
import com.tkip.mycode.model.my_enum.DataType
import com.tkip.mycode.model.my_enum.anyKey
import com.tkip.mycode.model.my_enum.getMyDataType
import com.tkip.mycode.model.peer.myuid
import com.tkip.mycode.util.tools.date.rightNow

/**
 * <RealDB>
 * 가벼운 Count
 * for ViewCount : post, flea, room, store
 */
data class CountMini(

        var key: String = createNanoKey(),
        var uid: String = myuid(),

        var topChildName: String = CH_COUNT,                // == topChildName  RealDB에 저장될 최상위 :삭제할 때 사용.
        var countType: Int = CountType.VIEW.no,

        ////////////////// 해당 모델 /////////////////
        var dataTypeNo: Int = DataType.POST.no,
        var modelKey: String = "modelKey",
        /////////////////////////////////////////////
        var addDate: Long = rightNow()

) : Parcelable {

    constructor(any: Any) : this(
            topChildName = CountType.VIEW.getTopChild(any),
            dataTypeNo = any.getMyDataType().no,
            modelKey = any.anyKey()
    )

    constructor(source: Parcel) : this(
            source.readString()!!,
            source.readString()!!,
            source.readString()!!,
            source.readInt(),
            source.readInt(),
            source.readString()!!,
            source.readLong()
    )

    override fun describeContents() = 0

    override fun writeToParcel(dest: Parcel, flags: Int) = with(dest) {
        writeString(key)
        writeString(uid)
        writeString(topChildName)
        writeInt(countType)
        writeInt(dataTypeNo)
        writeString(modelKey)
        writeLong(addDate)
    }

    companion object {

        @JvmField
        val CREATOR: Parcelable.Creator<CountMini> = object : Parcelable.Creator<CountMini> {
            override fun createFromParcel(source: Parcel): CountMini = CountMini(source)
            override fun newArray(size: Int): Array<CountMini?> = arrayOfNulls(size)
        }
    }
}
