package com.tkip.mycode.model.peer.recommend

import android.os.Parcel
import android.os.Parcelable
import com.tkip.mycode.init.base.createKey
import com.tkip.mycode.model.my_enum.Status
import com.tkip.mycode.model.peer.Peer
import com.tkip.mycode.model.peer.mynick
import com.tkip.mycode.model.peer.myuid
import com.tkip.mycode.util.tools.date.rightNow


/**
 * 단순 유저명 입력.
 * for recommend
 */
data class UidDate(

        var key: String = createKey(),   // when Recommend : 피추천인의 UID
        var comment: String? = null,

        var authorUID: String = myuid(),
        var authorNick: String = mynick(),

        var targetUID: String = "targetUID",
        var targetNick: String = "targetNick",

        var adminUID: String? = null,
        var adminNick: String? = null,

        var statusNo: Int = Status.NORMAL.no,
        var addDate: Long = rightNow()

) : Parcelable {

    /**     * 추천인 입력     */
    constructor(peer: Peer) : this(
            key = myuid(),
            targetUID = peer.uid,
            targetNick = peer.nick
    )


    constructor(source: Parcel) : this(
            source.readString()!!,
            source.readString(),
            source.readString()!!,
            source.readString()!!,
            source.readString()!!,
            source.readString()!!,
            source.readString(),
            source.readString(),
            source.readInt(),
            source.readLong()
    )

    override fun describeContents() = 0

    override fun writeToParcel(dest: Parcel, flags: Int) = with(dest) {
        writeString(key)
        writeString(comment)
        writeString(authorUID)
        writeString(authorNick)
        writeString(targetUID)
        writeString(targetNick)
        writeString(adminUID)
        writeString(adminNick)
        writeInt(statusNo)
        writeLong(addDate)
    }

    companion object {

        @JvmField
        val CREATOR: Parcelable.Creator<UidDate> = object : Parcelable.Creator<UidDate> {
            override fun createFromParcel(source: Parcel): UidDate = UidDate(source)
            override fun newArray(size: Int): Array<UidDate?> = arrayOfNulls(size)
        }

    }
}









