package com.tkip.mycode.model.store

import android.os.Parcel
import android.os.Parcelable
import com.tkip.mycode.init.base.createKey
import com.tkip.mycode.model.my_enum.PayType
import com.tkip.mycode.model.my_enum.Status
import com.tkip.mycode.util.tools.date.rightNow


/**
 * store > menu
 * 왜냐하면 이것도 thumb을 날리고 댓글을 날릴 수 있어야 하기 때문에.
 * 그리고 메뉴관리만 따로  할 수 있게 만들어야 한다.
 */

data class StoreMenu(

        var key: String = createKey(),
        var storeKey: String = "storeKey",   // 부모 store
        var order: Int = 0,      // 메뉴 나오는 순서

        var name: String = "",
        var payTypeNo: Int = PayType.PESO.no,
        var price: Int? = null,
        var photoUrl: String? = null,
        var photoThumb: String? = null,

        var thumbUpCount: Int? = null,     // 썸업을 준 횟수.
        var thumbDownCount: Int? = null,    // 썸다운을 횟수.
        var bookmarkCount: Int? = null,      // 유저가 즐겨찾기에 올릴시 카운팅
        var replyCount: Int? = null,      // 댓글 횟수.

        var statusNo: Int = Status.INSPECTING.no,
        var addDate: Long = rightNow()       // 최초 업로드 시간.

) : Parcelable {

    constructor(store: Store) : this(
            storeKey = store.key
    )

    constructor(storeKey: String, name: String, price: Int) : this(
            key = createKey(),
            storeKey = storeKey,
            name = name,
            price = price
    )

    constructor(source: Parcel) : this(
            source.readString()!!,
            source.readString()!!,
            source.readInt(),
            source.readString()!!,
            source.readInt(),
            source.readValue(Int::class.java.classLoader) as Int?,
            source.readString(),
            source.readString(),
            source.readValue(Int::class.java.classLoader) as Int?,
            source.readValue(Int::class.java.classLoader) as Int?,
            source.readValue(Int::class.java.classLoader) as Int?,
            source.readValue(Int::class.java.classLoader) as Int?,
            source.readInt(),
            source.readLong()
    )

    override fun describeContents() = 0

    override fun writeToParcel(dest: Parcel, flags: Int) = with(dest) {
        writeString(key)
        writeString(storeKey)
        writeInt(order)
        writeString(name)
        writeInt(payTypeNo)
        writeValue(price)
        writeString(photoUrl)
        writeString(photoThumb)
        writeValue(thumbUpCount)
        writeValue(thumbDownCount)
        writeValue(bookmarkCount)
        writeValue(replyCount)
        writeInt(statusNo)
        writeLong(addDate)
    }

    companion object {
        @JvmField
        val CREATOR: Parcelable.Creator<StoreMenu> = object : Parcelable.Creator<StoreMenu> {
            override fun createFromParcel(source: Parcel): StoreMenu = StoreMenu(source)
            override fun newArray(size: Int): Array<StoreMenu?> = arrayOfNulls(size)
        }
    }
}









