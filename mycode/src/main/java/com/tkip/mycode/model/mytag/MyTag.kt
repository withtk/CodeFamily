package com.tkip.mycode.model.mytag

import android.os.Parcel
import android.os.Parcelable
import com.tkip.mycode.init.base.createNanoKey
import com.tkip.mycode.model.my_enum.Status
import com.tkip.mycode.model.peer.mynick
import com.tkip.mycode.model.peer.myuid
import com.tkip.mycode.util.tools.date.rightNow

/**
 *
 * 중요 : Space는 원천 차단. 나중에 검색에서 모든 검색어에 걸릴 우려가 있다. 이러면 태그 10개 제한의 의미도 없고, adCredit의 의미도 없다.
 */
data class MyTag(

        var key: String = createNanoKey(),
        var authorUID: String = myuid(),
        var authorNick: String = mynick(),

        var tagTypeNo: Int = TagTYPE.LOC.no,
        var name: String = "",           // ex) 마닐라, manila, 식당,

        var hitCount: Int = 0,           // 검색 횟수.
        var bookmarkCount: Int = 0,      // functions에서 ++
        var totalPoint: Int = 1000,         // hitCount + bookmarkCount

        var statusNo: Int = Status.INSPECTING.no,       // 최초 작성시 INSPECTING  : admin 허가 후에 검색가능.
        var addDate: Long = rightNow()

) : Parcelable {

    constructor(str: String) : this(
            name = str
    )

    /**
     * add new : for user
     */
    constructor(str: String, tagType: TagTYPE) : this(
            name = str,
            tagTypeNo = tagType.no,
            statusNo = Status.NORMAL.no
    )

    /**
     * for admin
     */
    constructor(str: String, tagType: TagTYPE, totalPoint: Int?) : this(
            name = str,
            tagTypeNo = tagType.no,
//            tagTypeNo =   if (tagType == TagTYPE.ALL) TagTYPE.LOC.no else tagType.no,
            totalPoint = totalPoint ?: 0
    )


    constructor(source: Parcel) : this(
            source.readString()!!,
            source.readString()!!,
            source.readString()!!,
            source.readInt(),
            source.readString()!!,
            source.readInt(),
            source.readInt(),
            source.readInt(),
            source.readInt(),
            source.readLong()
    )

    override fun describeContents() = 0

    override fun writeToParcel(dest: Parcel, flags: Int) = with(dest) {
        writeString(key)
        writeString(authorUID)
        writeString(authorNick)
        writeInt(tagTypeNo)
        writeString(name)
        writeInt(hitCount)
        writeInt(bookmarkCount)
        writeInt(totalPoint)
        writeInt(statusNo)
        writeLong(addDate)
    }

    companion object {
        @JvmField
        val CREATOR: Parcelable.Creator<MyTag> = object : Parcelable.Creator<MyTag> {
            override fun createFromParcel(source: Parcel): MyTag = MyTag(source)
            override fun newArray(size: Int): Array<MyTag?> = arrayOfNulls(size)
        }
    }
}
