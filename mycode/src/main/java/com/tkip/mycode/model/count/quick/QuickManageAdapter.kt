package com.tkip.mycode.model.count.quick

import android.annotation.SuppressLint
import android.content.Context
import android.view.LayoutInflater
import android.view.MotionEvent
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.tkip.mycode.databinding.HolderQuickManageBinding
import com.tkip.mycode.dialog.OnDDD
import com.tkip.mycode.funs.common.lllogD
import com.tkip.mycode.funs.common.lllogI
import com.tkip.mycode.model.count.Count
import com.tkip.mycode.util.lib.MyTouchHelper
import com.tkip.mycode.util.tools.etc.OnMyClick
import com.tkip.mycode.util.tools.fire.Firee
import java.util.*

class QuickManageAdapter(  val items: ArrayList<Count>, val onDel: ((key: String) -> Unit)?, val listener: OnStartDragListener) : RecyclerView.Adapter<RecyclerView.ViewHolder>(), MyTouchHelper.OnMoveListener {
    private lateinit var context: Context

    /**   Quick 메뉴 관리 드래그용.    */
    interface OnStartDragListener {
        fun onStartDrag(holder: QuickManageViewHolder)
    }

    override fun onItemMove(fromP: Int, toP: Int) {
//        lllogD("isDiff onItemMove fromP1 ${items[fromP].modelTitle}")
        Collections.swap(items, fromP, toP)
//        lllogD("isDiff onItemMove fromP2 ${items[fromP].modelTitle}")
        notifyItemMoved(fromP, toP)
    }

    override fun onItemSwipe(p: Int, direction: Int) {
        lllogI("dragtest   onItemSwipe   ")
//        items.removeAt(p)
//        notifyItemRemoved(p)
    }

    /********************************/

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        context = parent.context
        return QuickManageViewHolder(HolderQuickManageBinding.inflate(LayoutInflater.from(parent.context), parent, false))
    }

    override fun onBindViewHolder(h: RecyclerView.ViewHolder, p: Int) {
        (h as QuickManageViewHolder).bind(items[p])
    }

    override fun getItemId(position: Int): Long {
        return items[position].hashCode().toLong()
    }

    override fun getItemCount(): Int {
        return items.size
    }

    /**************************************
     * quick 수정
     ************************************/
    inner class QuickManageViewHolder(val b: HolderQuickManageBinding) : RecyclerView.ViewHolder(b.root) {

        private lateinit var count: Count

        @SuppressLint("ClickableViewAccessibility")
        fun bind(count: Count) {
            this.count = count
            b.holder = this
            b.count = count

            b.executePendingBindings()

            b.ivDrag.setOnTouchListener { _, event ->
                if (event.action == MotionEvent.ACTION_DOWN) listener.onStartDrag(this)
                return@setOnTouchListener true
            }

            b.line.setOnLongClickListener {
                listener.onStartDrag(this)
                return@setOnLongClickListener true
            }
        }

        fun onClickDelete(v: View) {
            OnMyClick.setDisableAWhileBTN(v)

            OnDDD.delete(context,
                    ok = {
                        Firee.deleteQuick(count,
                                success = {
                                    items.removeAt(adapterPosition)
                                    notifyItemRemoved(adapterPosition)
                                    onDel?.invoke(count.key)
                                }, fail = {})
                    }, cancel = {})


        }

    }

}