package com.tkip.mycode.model.mytag

import android.content.Context
import android.view.LayoutInflater
import android.widget.LinearLayout
import androidx.databinding.DataBindingUtil
import com.tkip.mycode.R
import com.tkip.mycode.databinding.LinearAddTagBinding
import com.tkip.mycode.dialog.alert.MyAlert
import com.tkip.mycode.init.inter.interAddTag
import com.tkip.mycode.model.mytag.TagTYPE
import com.tkip.mycode.model.mytag.TAG_1
import com.tkip.mycode.model.mytag.setAll

/**
 * tag loc
 * tag builtIn
 *
 * 여러개 태그 만들때
 */
class LinearAddTag : LinearLayout {
    lateinit var b: LinearAddTagBinding

    constructor(context: Context) : super(context)
    constructor(context: Context, tagType: TagTYPE, title: String?, btnName: String, list: ArrayList<String>? ) : super(context) {
        b = DataBindingUtil.inflate(LayoutInflater.from(context), R.layout.linear_add_tag, this, true)

        b.inter = interAddTag(
                onClickOpenTagDialog = {
                    MyAlert.showAddTag(context, false, null, tagType, b.tagView, onSingleResult = { })
                }
        )

        b.title = title
        b.btnName = btnName

        b.tagView.setAll(TAG_1, list)

    }

}