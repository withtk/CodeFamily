package com.tkip.mycode.model.cost

import android.os.Parcel
import android.os.Parcelable
import com.tkip.mycode.funs.common.lllogD
import com.tkip.mycode.init.ATTACH_FAILED
import com.tkip.mycode.init.base.createKey
import com.tkip.mycode.init.base.createNanoKey
import com.tkip.mycode.model.bid.Bid
import com.tkip.mycode.model.cost.pay.CHOSEN_PAY_TYPE
import com.tkip.mycode.model.my_enum.*
import com.tkip.mycode.model.peer.mynick
import com.tkip.mycode.model.peer.myuid
import com.tkip.mycode.util.tools.date.rightNow

/**
 * (cost.key == bid.key는 동일하다) : 유찰일때(+failed) 배당일때(+allot)
 * (cost == act 사실상 동일하다)  다만, 저장 DB위치에 따라 credit과 point가 변경되는 여부가 갈림.
 *
 * 1.credit - 실시간 트랜잭션.
 * 2.act - 활동은 포인트가 있을시 어드민이 하루에 한번 일괄적으로 트랜잭션.(pnt만 해당됨.)
 *
 * 결제 : 각종 기능 사용, 광고 입찰.
 * 전환 : to myself
 * 전달 : to other
 */
data class Cost(

        var key: String = createKey(),
        var uid: String = myuid(),    // DB가 들어갈 UID : add 하는 유저와 다를 수 있음.
//        var sum: Int = 0,             // amount 계산한 결과.

        //////////////////// ACT + model, modelKey, modelTitle ////////////////
//        var payTypeNo: Int? = null,
        var costTypeNo: Int? = null,
        var actTypeNo: Int = ActType.NOTHING.no,    // income, outcome, change,

        var commission: Int? = null,    // 어플이 먹는 수수료:당시 적용된 커미션비율  (값은 %,)   == ActType.rate
//        var amount: Int = 0,    // 거래된 금액 총합 예) 1000, -1000   or ActPoint
        //////////////////////////////////////////////////////////////////////

        var comment: String? = null,   // 해당 cost의 구체적 이유 등..
        var pnt: Int? = null,
        var bcrd: Int? = null,
        var crd: Int? = null,
        var crt: Int? = null,

        /**         * for Tran(GIVE, TAKE, CHANGE) 일 때         */
        var otherUid: String? = null,
        var otherNick: String? = null,    // act 일때는 유저본인 nick

        /**         * for Act(활동일 때)  board, post, reply...       */
        //////////////// Target Model ///////////////////
        var dataTypeNo: Int? = null,
        var modelKey: String? = null,
        var modelTitle: String? = null,

        /**         * for Ad(광고일 때)  board, post, reply...       */
        var adTypeNo: Int? = null,   // 광고종류
        var term: Int? = null,       // 광고게재 기간
        var hitDate: Int? = null,    // 광고게재 날짜  예) 20190101   in functions
        var startDate: Int? = null,  // 한번에 입찰한 기간 : 시작 날짜    예) 20190101
        var endDate: Int? = null,    // 한번에 입찰한 기간 : 마지막 날짜  예) 20190101

        var addDate: Long = rightNow()

) : Parcelable {
    /*** for bid 광고입찰 : cost.key = bid.key 동일.
     * used :
     *     1.입찰
     *     2.유찰 환불cost 만들때 */
    constructor(bid: Bid, actType: ActType) : this(
            key = bid.key,
            costTypeNo = actType.getCostNo(),
            actTypeNo = actType.no,
            commission = actType.balance()?.rate,

            pnt = bid.amount,   // 활동포인트는 plus로 올려야.

            dataTypeNo = bid.dataTypeNo,
            modelKey = bid.modelKey,
            modelTitle = bid.modelTitle,

            adTypeNo = bid.adTypeNo,
            term = bid.term,
            hitDate = bid.hitDate,
            startDate = bid.startDate,
            endDate = bid.endDate
    )

    constructor(source: Parcel) : this(
            source.readString()!!,
            source.readString()!!,
            source.readValue(Int::class.java.classLoader) as Int?,
            source.readInt(),
            source.readValue(Int::class.java.classLoader) as Int?,
            source.readString(),
            source.readValue(Int::class.java.classLoader) as Int?,
            source.readValue(Int::class.java.classLoader) as Int?,
            source.readValue(Int::class.java.classLoader) as Int?,
            source.readValue(Int::class.java.classLoader) as Int?,
            source.readString(),
            source.readString(),
            source.readValue(Int::class.java.classLoader) as Int?,
            source.readString(),
            source.readString(),
            source.readValue(Int::class.java.classLoader) as Int?,
            source.readValue(Int::class.java.classLoader) as Int?,
            source.readValue(Int::class.java.classLoader) as Int?,
            source.readValue(Int::class.java.classLoader) as Int?,
            source.readValue(Int::class.java.classLoader) as Int?,
            source.readLong()
    )

    override fun describeContents() = 0

    override fun writeToParcel(dest: Parcel, flags: Int) = with(dest) {
        writeString(key)
        writeString(uid)
        writeValue(costTypeNo)
        writeInt(actTypeNo)
        writeValue(commission)
        writeString(comment)
        writeValue(pnt)
        writeValue(bcrd)
        writeValue(crd)
        writeValue(crt)
        writeString(otherUid)
        writeString(otherNick)
        writeValue(dataTypeNo)
        writeString(modelKey)
        writeString(modelTitle)
        writeValue(adTypeNo)
        writeValue(term)
        writeValue(hitDate)
        writeValue(startDate)
        writeValue(endDate)
        writeLong(addDate)
    }

    companion object {


        fun new(uid1: String?, actType: ActType, any: Any?): Cost {
            return when (actType.getCostNo()) {
                CostType.ACT.no -> act(actType, any)  // add Cost to realDB
                else -> new(uid1, actType, any, null, null, null, null)   // add Cost to FireS
            }
        }

        /**
         * 사용자 활동 업로드 : uid=myuid()  중요 : CostType.Act만 이 메서드를 통과시킬 것.
         *  .real() 붙이면  ->  add to RealDB
         */
        fun act(actType: ActType, any: Any?): Cost = new(null, actType, any, null, null, null, null).apply { otherNick = mynick() }

        /**
         * 1. null : actType의 값 적용.
         * 2. 커스텀 기입 : (중요) 커스텀하려면 전체(4가지) 모두 기입해야 함.( 마이너스는 필요없음. CostType에 따라 자동으로 마이너스 붙음)
         */
        fun new(uid1: String?, actType: ActType, any: Any?, pnt1: Int?, bcrd1: Int?, crd1: Int?, crt1: Int?): Cost {
            val balance = actType.balance()
            lllogD("commonCost  costType ${actType.getCostType()} actType $actType actType.balance $balance pnt1 $pnt1 bcrd1 $bcrd1 crd1 $crd1 crt1 $crt1   any: ${anyTitle()}")
            return Cost()
                    .apply {
                        uid = uid1 ?: myuid()
                        costTypeNo = actType.getCostNo()
                        actTypeNo = actType.no
                        commission = actType.balance()?.rate


                        /*** 1. 커스텀 금액 결제시 */
                        if (pnt1 != null || bcrd1 != null || crd1 != null || crt1 != null) {
                            pnt = pnt1
                            bcrd = bcrd1
                            crd = crd1
                            crt = crt1
                        } else {
                            /*** 2. 정해진 actType 금액 결제시 */
                            balance?.let { bal ->
                                pnt = bal.pnt
                                bcrd = bal.bcrd
                                crd = bal.crd
                                crt = bal.crt
                            }
                        }

                        /*** 3.모델 정보 삽입. */
                        any?.let {
                            dataTypeNo = it.getMyDataType().no
                            modelKey = it.anyKey()
                            modelTitle = it.anyTitle()
                        }

                        /*** 4.수입/지출 여부에 따라 분기. */
                        when (actType.getCostNo().costType()) {
                            CostType.PAYMENT -> {

                                bcrd = null
                                crd = null

                                /*** 선택한 결제종류로 결제cost 업로드 */
                                actType.getCrdAmount()?.let { amount ->
                                    when (CHOSEN_PAY_TYPE) {
                                        PayType.BONUS_CREDIT -> bcrd = amount
                                        PayType.CREDIT -> crd = amount
                                        else -> {
                                        }
                                    }
                                }

                                /***  마이너스 붙이기  */
                                bcrd?.let { bcrd = -it }
                                crd?.let { crd = -it }
                                crt?.let { crt = -it }
                            }
                            CostType.TAKE, CostType.SEND -> {
                                /***  수수료 계산  */
                                bcrd?.let { bcrd = actType.calRate(it) }
                                crd?.let { crd = actType.calRate(it) }
                                crt?.let { crt = actType.calRate(it) }
                            }
                            CostType.CONVERT -> {
                                /***  마이너스 붙이기  */
                                pnt?.let { pnt = -it }
                            }
                            else -> {
                            }
                        }
                        lllogD("commonCost  actType $actType Cost $this")
                    }

        }

        /**     * 광고  */
        fun bid(bid: Bid) =
                Cost(bid, ActType.BID_ADD).apply {
                    // 입찰은 언제나 마이너스.
                    when {
                        (bid.payTypeNo == PayType.CREDIT.no) -> crd = -(bid.amount ?: 0)
                        (bid.payTypeNo == PayType.BONUS_CREDIT.no) -> bcrd = -(bid.amount ?: 0)
                    }
                }

        /**
         * todo: 다시 체크할 것. 아직 확실히 안 봤다.
         */
        fun bidRefund(bid: Bid) =
                Cost(bid, ActType.BID_FAILED).apply {
                    key = bid.key + ATTACH_FAILED
//                    amount = bid.amount    // 개별 입찰 가격.  ( 환불은 무조건 플러스로 들어가야 함.!!!!!!)
                }

        @JvmField
        val CREATOR: Parcelable.Creator<Cost> = object : Parcelable.Creator<Cost> {
            override fun createFromParcel(source: Parcel): Cost = Cost(source)
            override fun newArray(size: Int): Array<Cost?> = arrayOfNulls(size)
        }
    }
}