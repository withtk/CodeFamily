package com.tkip.mycode.model.cost.credit

import android.widget.Button
import com.tkip.mycode.R
import com.tkip.mycode.databinding.AlertChangeCreditBinding
import com.tkip.mycode.funs.common.getInt
import com.tkip.mycode.funs.custom.number.ViewNumberBtn
import com.tkip.mycode.init.base.getStr
import com.tkip.mycode.init.base.getStringComma
import com.tkip.mycode.init.base.removeComma
import com.tkip.mycode.model.peer.myPnt
import com.tkip.mycode.model.peer.mypeer
import com.tkip.mycode.util.tools.anim.hideSlideDown
import com.tkip.mycode.util.tools.anim.showFadeIn
import com.tkip.mycode.util.tools.anim.showSlideUp
import kotlin.math.ceil


class OnPnt {

    companion object {

        private val RATE_CHANGE by lazy { 5.3 }   // 비율 pnt -> bcrd

        /********************************************************************************************
         * Change Pnt -> Bcrd
         * 숫자클릭시 pnt, bcrd 실시간 변경.
         */
        fun calAndReset(b: AlertChangeCreditBinding, digit: Int?) {

            /*** 초기화 */
            if (digit == null) {
                b.btnChange.isEnabled = false
                b.tvInfo.hideSlideDown(false)
                b.tvPnt.text = "0"
                b.tvBcrd.text = "0"
                return
            }

            val inPnt = (b.tvPnt.text.toString().removeComma().toInt() + digit)   // 기입한 pnt

            /*** pnt 부족할 때 */
            if (inPnt > myPnt()) {
                b.tvInfo.text = R.string.info_chagne_pnt_not_enough.getStr()
                b.tvInfo.showSlideUp(true)
                b.btnChange.isEnabled = (b.tvPnt.getInt()>0 )
                return
            }

            /*** 유효성 검사 : 버튼 표시여부 */
            if (inPnt < 10000) {

                b.btnChange.isEnabled = false // 버튼 enable
                b.tvInfo.text = R.string.info_chagne_pnt_more_than_number.getStr()
                b.tvInfo.showSlideUp(true)

            } else {
                b.tvInfo.hideSlideDown(false)
                b.btnChange.isEnabled = true // 버튼 enable

                /*** 계산 & 표시 */
                val outBcrd = ceil(inPnt / RATE_CHANGE).toInt()           // 계산된 bcrd (올림. ceil)
                b.tvPnt.text = inPnt.getStringComma()
                b.tvBcrd.text = outBcrd.getStringComma()
                b.tvPnt.showFadeIn(true)
                b.tvBcrd.showFadeIn(true)
            }

        }



    }

}