package com.tkip.mycode.model.toss

import android.content.Context
import android.view.LayoutInflater
import android.widget.LinearLayout
import androidx.databinding.DataBindingUtil
import com.tkip.mycode.R
import com.tkip.mycode.databinding.ViewTossModelBinding
import com.tkip.mycode.funs.common.lllogD
import com.tkip.mycode.funs.common.tranPair
import com.tkip.mycode.init.TOSS_VIEW
import com.tkip.mycode.init.base.getStr
import com.tkip.mycode.init.inter.interTossModel
import com.tkip.mycode.model.my_enum.checkAndGo
import com.tkip.mycode.model.my_enum.dataType
import com.tkip.mycode.model.my_enum.getMyDataType
import com.tkip.mycode.util.tools.anim.gone
import com.tkip.mycode.util.tools.anim.hideSlideDown
import com.tkip.mycode.util.tools.anim.showSlideUp
import com.tkip.mycode.util.tools.etc.OnMyClick

/**
 * 1. toss 내부적으로 보여줌. copy,  list, add,
 * 2. inReply rv에 첨부될 때.
 */
class ViewTossModel : LinearLayout {
    private lateinit var b: ViewTossModelBinding
    private lateinit var toss1: Toss

    constructor(context: Context) : super(context)
    constructor(context: Context, parent: LinearLayout?,toss: Toss?, onGoto: ((Toss?) -> Unit)?, onRemove: (() -> Unit)?, onClose: (() -> Unit)?, onLong: (() -> Unit)?) : super(context) {
        init(context, parent,toss, onGoto, onRemove, onClose,onLong, null, null)
    }

    constructor(context: Context,parent: LinearLayout?, tossModelKey: String?, tossDataTypeNo: Int?, onGoto: ((Toss?) -> Unit)?, onRemove: (() -> Unit)?, onClose: (() -> Unit)?, onLong: (() -> Unit)?) : super(context) {
        init(context, parent,null, onGoto, onRemove, onClose,onLong, tossDataTypeNo, tossModelKey)
    }

    private fun init(context: Context, parent: LinearLayout?,toss: Toss?, onGoto: ((Toss?) -> Unit)?, onRemove: (() -> Unit)?, onClose: (() -> Unit)?, onLong: (() -> Unit)?, tossDataTypeNo: Int?, tossModelKey: String?) {
        lllogD("ViewTossModel ViewTossModel toss $toss tossDataTypeNo $tossDataTypeNo tossModelKey $tossModelKey")


        b = DataBindingUtil.inflate(LayoutInflater.from(context), R.layout.view_toss_model, this, true)

        b.inter = interTossModel(
                onClickGoto = {
                    OnMyClick.setDisableAWhileBTN(it)
                    lllogD("ViewTossModel ViewTossModel toss1 $toss1")
                    toss1.checkAndGo(context, false, arrayOf(b.card.tranPair(R.string.tran_linear)), onSuccess = {})
                    onGoto?.invoke(toss1)
                },
                onClickRemove = { onRemove?.invoke() },
                onClickClose = {
                    b.card.hideSlideDown(true)
                    onClose?.invoke()
                })

        b.line.setOnLongClickListener {
            onLong?.invoke()
            return@setOnLongClickListener true
        }

        /***  setToss  */
        toss?.let {
            b.toss = toss
            toss1 = it
        }

        /**
         * <toss == 널 && reply != null>
         * 처음에 무조건 progress 돌아감.
         */
        /** data 받아와서 뿌려주기 */
        tossDataTypeNo?.dataType()?.get(tossModelKey!!,
                success = { any ->
                    lllogD("ViewTossModel ViewTossModel get $any")

                    b.progressSmall.gone()
                    if (any == null) b.tvTitle.text = R.string.error_data_deleted.getStr()
                    else {
                        val toss2 = Toss.create(any, TOSS_VIEW)
                        b.toss = toss2
                        toss1 = toss2
                    }
                }, fail = {  lllogD("ViewTossModel ViewTossModel get  fail")})





        /*** view 붙이기 */
        parent?.removeAllViews()
        parent?.addView(this@ViewTossModel)
        parent?.showSlideUp(true)
    }


}