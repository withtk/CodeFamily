package com.tkip.mycode.model.my_enum

import com.tkip.mycode.R
import com.tkip.mycode.funs.common.lllogD
import com.tkip.mycode.init.base.getClr
import com.tkip.mycode.init.base.getDrawable
import com.tkip.mycode.init.base.getStr
import com.tkip.mycode.model.cost.balance.*
import com.tkip.mycode.model.peer.mybonus
import com.tkip.mycode.model.peer.mycredit
import com.tkip.mycode.model.peer.mypoint
import kotlin.math.ceil


/******************************** cost 타입 *****************************************/
/**
 * Cost 모델을 사용하는 상황. (cost구분에 활용)
 */
enum class CostType(val no: Int, val title: String, private val colorId: Int) {
    NOTHING(10, R.string.costtype_title_act.getStr(), R.color.text_light),        // 미정
    ACT(20, R.string.costtype_title_act.getStr(), R.color.text_light),        // 활동
    PAYMENT(40, R.string.costtype_title_payment.getStr(), R.color.colorAccent2),    // 결제
    TAKE(60, R.string.costtype_title_take.getStr(), R.color.colorAccent),     // 받기
    SEND(70, R.string.costtype_title_send.getStr(), R.color.colorAccent2),  // 보내기  중요:받는 유저에 반드시 cost넣어줘야함.
    CONVERT(80, R.string.costtype_title_convert.getStr(), R.color.text_gray),    // 전환
    ;

    fun clr() = colorId.getClr()
}

fun Int.costType(): CostType {
    CostType.values().forEach { if (it.no == this) return it }
    return CostType.ACT
}

fun String.costType(): CostType {
    CostType.values().forEach { if (it.title == this) return it }
    return CostType.ACT
}


/******************************** 크레딧 종류 타입 *****************************************/
enum class PayType(val no: Int, val title: String, val colorId: Int, val iconId: Int) {
    CPAY(10, R.string.paytype_title_philpay.getStr(), R.color.colorAccent, R.drawable.ic_person),
    CARAT(20, R.string.paytype_title_carat.getStr(), R.color.colorAccent, R.drawable.ic_person),
    CREDIT(30, R.string.paytype_title_credit.getStr(), R.color.credit_background, R.drawable.ic_crd),
    BONUS_CREDIT(40, R.string.paytype_title_bonus_credit.getStr(), R.color.credit_background, R.drawable.ic_bcrd),
    POINT(60, R.string.paytype_title_point.getStr(), R.color.skyblue_800, R.drawable.ic_pnt),

    WON(80, R.string.paytype_title_won.getStr(), R.color.skyblue_800, R.drawable.ic_pay_point),
    PESO(90, R.string.paytype_title_peso.getStr(), R.color.skyblue_800, R.drawable.ic_peso_small),

///******************************** 크레딧 종류 타입 *****************************************/
//enum class PayType(val no: Int, val title: String, val colorId: Int, val iconId: Int) {
//    CPAY(300, R.string.paytype_title_philpay.getStr(), R.color.colorAccent, R.drawable.ic_person),
//    CARAT(500, R.string.paytype_title_carat.getStr(), R.color.colorAccent, R.drawable.ic_person),
//    CREDIT(600, R.string.paytype_title_credit.getStr(), R.color.credit_background, R.drawable.ic_crd),
//    BONUS_CREDIT(700, R.string.paytype_title_bonus_credit.getStr(), R.color.credit_background, R.drawable.ic_bcrd),
//    POINT(900, R.string.paytype_title_point.getStr(), R.color.skyblue_800, R.drawable.ic_pnt),
//
//    WON(1000, R.string.paytype_title_won.getStr(), R.color.skyblue_800, R.drawable.ic_pay_point),
//    PESO(1100, R.string.paytype_title_peso.getStr(), R.color.skyblue_800, R.drawable.ic_peso_small),

//    CARROT(1000, R.string.paytype_title_carrot.getStr(), R.color.colorAccent, R.drawable.ic_person),
    ;

    fun getIcon() = iconId.getDrawable()
    fun getColor() = colorId.getClr()
    fun getMyAmount() =
            when (this) {
                CREDIT -> mycredit()
                BONUS_CREDIT -> mybonus()
                POINT -> mypoint()
                else -> 0
            }

    fun isNotEnough(price: Int?): Boolean = price?.let { (this.getMyAmount() < it) } ?: false
    fun isEnough(price: Int?): Boolean = price?.let { (this.getMyAmount() >= it) } ?: false

}

fun Int.payType(): PayType {
    PayType.values().forEach { if (it.no == this) return it }
    return PayType.POINT
}

fun String.payType(): PayType {
    PayType.values().forEach { if (it.title == this) return it }
    return PayType.POINT
}

/**
 * actiPoint : 활동으로 인해 획득하는 포인트. 크레딧과는 별개.
 * sign : 지불 or 획득
 * price : PAYMENT - 해당 서비스의 가격(크레딧)
 * rate : 수수료율 : (단위 %)
 *      1) 입찰후 머니백
 *      1) 광고게재로 인한 보드마스터의 수당
 *      1) 어플이 먹는 수수료율
 */
/******************************** act 타입 *****************************************/

/**
 * no : 가장 중요. (변경불가)
 * costType : 중요. 이것에 따라서 계산이 달라짐.
 * PAYMENT : 무조건 마이너스 (cost 생성시 minus 붙임)
 */
enum class ActType(val no: Int, val title: String, val msg: String, val colorId: Int?) {
    NOTHING(200, R.string.act_title_nothing.getStr(), R.string.act_msg_mothing.getStr(),null),    // 항목 미정

    /** CONVERT : 본인이 전환 */
    CRD_TO_PAY(500, R.string.act_title_to_pay.getStr(), R.string.act_msg_to_pay.getStr(),null),
    PAY_TO_CRD(600, R.string.act_title_pay_to_credit.getStr(), R.string.act_msg_pay_to_credit.getStr(),null),
    PNT_TO_BCRD(700, R.string.act_title_point_to_credit.getStr(), R.string.act_msg_point_to_credit.getStr(),null),
    CRD_TO_PNT(800, R.string.act_title_to_point.getStr(), R.string.act_msg_to_point.getStr(),null),
    CHANGE(900, R.string.act_title_change.getStr(), R.string.act_msg_change.getStr(),null),
    MONEY(1000, R.string.act_title_money.getStr(), R.string.act_msg_money.getStr(),null),


    /** GIVE : 유저간 주고받기 */
    GIVE_CREDIT(2000, R.string.act_title_give.getStr(), R.string.act_msg_take.getStr(),null),  // 다른유저에게 전달.


    /** TAKE : 획득 */
    SIGN_UP(3000, R.string.act_title_signup.getStr(), R.string.act_msg_signup.getStr(),null),
    RECOMMEND(3020, R.string.act_title_recommend.getStr(), R.string.act_title_recommend.getStr(),null),
    RECOMMENDED(3040, R.string.act_title_recommended.getStr(), R.string.act_title_recommended.getStr(),null),
    BUY_CREDIT(3500, R.string.act_title_buy_credit.getStr(), R.string.act_msg_buy_credit.getStr(), R.color.text_plus),  // 크레딧 구매.
    TAKE_CREDIT(3600, R.string.act_title_take.getStr(), R.string.act_msg_give.getStr(), R.color.text_plus), // 다른유저에게 받을때.

    BETA_FREE_BONUS_CREDIT(3700, R.string.act_title_beta_free_bonus_credit.getStr(), R.string.act_msg_free_bonus_credit.getStr(), R.color.text_plus), // 베타테스터 보너스.
//    FIRST_ADD_STORE(3701,  R.string.act_title_first_store.getStr(), R.string.act_msg_free_bonus_credit.getStr(), R.color.text_plus),
//    FIRST_ADD_BOARD(3702,  R.string.act_title_first_board.getStr(), R.string.act_msg_free_bonus_credit.getStr(), R.color.text_plus),
//    FIRST_ADD_POST(3703,  R.string.act_title_first_post.getStr(), R.string.act_msg_free_bonus_credit.getStr(), R.color.text_plus),

    FREE_BONUS_CREDIT(3720, R.string.act_title_free_bonus_credit.getStr(), R.string.act_msg_free_bonus_credit.getStr(), R.color.text_plus), // 무료 보너스 크레딧.
    THANKS_BONUS_CREDIT(3721, R.string.act_title_thanks_free_bonus_credit.getStr(), R.string.act_msg_thanks_free_bonus_credit.getStr(), R.color.text_plus), // 감사 보너스 크레딧.
    LUCKY_BONUS_CREDIT(3722, R.string.act_title_lucky_bonus_credit.getStr(), R.string.act_msg_lucky_bonus_credit.getStr(), R.color.text_plus), // 럭키 보너스 크레딧.
    FREE_CREDIT(3723, R.string.act_title_free_credit.getStr(), R.string.act_msg_free_credit.getStr(), R.color.text_plus),  // 무료 크레딧.
    THANKS_CREDIT(3724, R.string.act_title_thanks_free_credit.getStr(), R.string.act_msg_thanks_free_credit.getStr(), R.color.text_plus),// 감사 크레딧.
    FREE_POINT(3725, R.string.act_title_free_point.getStr(), R.string.act_msg_free_point.getStr(), R.color.text_plus),   // 무료 포인트.
    THANKS_POINT(3726, R.string.act_title_thanks_free_point.getStr(), R.string.act_msg_thanks_free_point.getStr(), R.color.text_plus), // 감사 포인트.

    WATCH_AD(3800, R.string.act_title_watch_ad.getStr(), R.string.act_msg_watch_ad.getStr(), R.color.text_plus), // 광고 시청.

    GET_REPLY_ADOPTED(4000, R.string.act_title_reply_adopted.getStr(), R.string.act_msg_reply_adopted.getStr(), R.color.text_plus),     // 채택 되었을 때(rewawrd)
    GET_REPLY_ADOPTED_COMPLETED(4020, R.string.act_title_reply_adoption_completed.getStr(), R.string.act_msg_reply_adoption_completed.getStr(), R.color.text_plus),   // 채택 완료. : 질문자에게 머니백.
    GET_WARN_COMPLETED(4050, R.string.act_title_warn_completed.getStr(), R.string.act_msg_warn_completed.getStr(), R.color.text_plus),   // 올바른 신고 후 :관리자가 처리 완료.


    BID_ALLOT(4300, R.string.act_title_auction_allot.getStr(), R.string.act_msg_auction_allot.getStr(), R.color.text_minus),   // 개별보드 낙찰액 배당.


    /** PAYMENT : 지불 (중요) Cost에 기입시 반드시 마이너스 붙일 것.(표기할때는 양수가 가독력(유저입장)이 좋아서 모두 양수로 기재함.)*/
    ADD_REWARD_POST(5000, R.string.act_title_add_reward_post.getStr(), R.string.act_msg_add_reward_post.getStr(), R.color.text_minus),     // post에 reward 올렸을 때.
    ADD_ANONYMOUS_POST(5100, R.string.act_title_anonymous_post.getStr(), R.string.act_msg_anonymous.getStr(), R.color.text_minus),     // post에 익명 업로드.
    ADD_ANONYMOUS_FLEA(5120, R.string.act_title_anonymous.getStr(), R.string.act_msg_anonymous.getStr(), R.color.text_minus),     // flea 익명 업로드.

    BID_ADD(6400, R.string.act_title_auction_bidding.getStr(), R.string.act_msg_auction_bidding.getStr(), R.color.text_minus),   // 입찰
    BID_ADCREDIT_ADD(6420, R.string.act_title_adcredit.getStr(), R.string.act_msg_adcredit.getStr(), R.color.text_minus),   // adCredit 추가.

    //    BID_ADD_BACK(6500, 100, null, null,null,  20, R.string.act_title_auction_bid_back.getStr(), R.string.act_msg_auction_bid_back.getStr(),R.color.text_minus),   // 크레딧백
    //    AD_BIDDING_CANCELED(1120, 100, null, null,null,  , null, 10, R.string.act_title_auction_bidding_canceled.getStr(), R.string.act_msg_auction_bidding_canceled.getStr(),R.color.text_minus),   // 입찰 취소
    BID_FAILED(6600, R.string.act_title_auction_failed.getStr(), R.string.act_msg_auction_failed.getStr(), R.color.text_minus),   // 유찰
    ERROR_MINUS(6665, R.string.act_title_error_minus.getStr(), R.string.act_msg_error_minus.getStr(), R.color.text_minus),
    PENALTY(6666, R.string.act_title_penalty.getStr(), R.string.act_msg_penalty.getStr(), R.color.text_minus),

    PREMIUM_BOARD(7140, R.string.act_title_premium_board.getStr(), R.string.act_msg_premium_board.getStr(), R.color.text_minus),   // 프리미엄 보드 등록.


    /************************************************************************************************************************************************
     * ACT
     ************************************************************************************************************************************************/
    ADD_REPLY(4100, R.string.act_title_add_reply.getStr(), R.string.act_msg_add_reply.getStr(),null),
    ADD_WARN(4120, R.string.act_title_add_warn.getStr(), R.string.act_msg_add_warn.getStr(),null),

    ADD_BOARD(7120, R.string.act_title_add_board.getStr(), R.string.act_msg_add_board.getStr(), R.color.text_minus),   // 보드 생성.
    ADD_POST(7230, R.string.act_title_add_post.getStr(), R.string.act_msg_add_post.getStr(), R.color.text_minus),  // 포스트 올리기.
    ADD_FLEA(7240, R.string.act_title_add_flea.getStr(), R.string.act_msg_add_flea.getStr(), R.color.text_minus),   // 중고 올리기.
    ADD_ROOM(7360, R.string.act_title_add_room.getStr(), R.string.act_msg_add_room.getStr(), R.color.text_minus),   // 방 올리기.
    ADD_STORE(7380, R.string.act_title_add_store.getStr(), R.string.act_msg_add_store.getStr(), R.color.text_minus),   // 방 올리기.


    LOGIN(10200, R.string.act_title_login.getStr(), R.string.act_msg_login.getStr(), R.color.text_gray),             // 오픈 어플 : when opened MainActivity
    ATTENDANCE(10210, R.string.act_title_attendance.getStr(), R.string.act_msg_attendance.getStr(), R.color.text_gray),    //출석체크
    RE_LOGIN(10220, R.string.act_title_login_re.getStr(), R.string.act_msg_login_re.getStr(),null),   // 재로그인: 이미 가입자가 어플 새로 로그인시 from SignActivity
    SIGN_OUT(10230, R.string.act_title_signout.getStr(), R.string.act_msg_signout.getStr(),null),
    SIGN_OUT_DUPLICATE(10235, R.string.act_title_signout_dupl.getStr(), R.string.act_msg_signout_dupl.getStr(),null),
    LEAVE(10250, R.string.act_title_delete.getStr(), R.string.act_msg_delete.getStr(),null),       // 탈퇴

    OPEN_MODEL(10300, String.format(R.string.act_title_open_model.getStr(), ""), String.format(R.string.act_msg_open_model.getStr(), ""),null),
    OPEN_BOARD(10400, String.format(R.string.act_title_open_model.getStr(), R.string.title_board.getStr()), String.format(R.string.act_msg_open_model.getStr(), R.string.title_board.getStr()),null),
    OPEN_POST(10500, String.format(R.string.act_title_open_model.getStr(), R.string.title_post.getStr()), String.format(R.string.act_msg_open_model.getStr(), R.string.title_post.getStr()),null),
    OPEN_FLEA(10600, String.format(R.string.act_title_open_model.getStr(), R.string.title_flea.getStr()), String.format(R.string.act_msg_open_model.getStr(), R.string.title_flea.getStr()),null),
    OPEN_ROOM(10700, String.format(R.string.act_title_open_model.getStr(), R.string.title_room.getStr()), String.format(R.string.act_msg_open_model.getStr(), R.string.title_room.getStr()),null),
    OPEN_STORE(10800, String.format(R.string.act_title_open_model.getStr(), R.string.title_store.getStr()), String.format(R.string.act_msg_open_model.getStr(), R.string.title_store.getStr()),null),


    ;

    /** CostTypeNo */
    fun getCostNo(): Int = balance()?.costTypeNo ?: CostType.ACT.no
    fun getCostType(): CostType = balance()?.costTypeNo?.costType() ?: CostType.ACT


//    /**     * amount에 부등호 붙이기  : amount가 없는 경우는 price로 계산해야 하기때문에.   */
//    fun calAmount() = this.sign * (price ?: 0)

    /**
     * fot PAYMENT : 금액 받아오기
     * payType이 뭐가될지(bonus or credit) 모르기 때문에 필요하다.
     */
    fun getCrdAmount(): Int? {
        balance()?.bcrd?.let { return it }
        balance()?.crd?.let { return it }
        return null
    }

    /**
     * 1. CREDIT
     * 2. CARAT
     */
    fun payType(): PayType {
        balance()?.bcrd?.let { return PayType.CREDIT }
        balance()?.crd?.let { return PayType.CREDIT }
        balance()?.crt?.let { return PayType.CARAT }
        return PayType.CREDIT
    }

    /**     * 수수료 뺀 결과     */
    fun calRate(number: Int?): Int? {
        number?.let {
            balance()?.rate?.let { per ->
                val resultPercent = (100 - per.toDouble())   // 수수료 제외한 %
                val cal = (number * (resultPercent / 100)) // 계산 : 수수료%를 제외한 결과.
                return ceil(cal).toInt() // 소수점 모두 올림한 Int
            }
        } ?: let { return number }
    }

    fun balance(): Balance? {
        return if (BALANCE_MAP.isEmpty()) {
            setupBalanceMap()
            BALANCE_MAP[this.no]
        } else BALANCE_MAP[this.no]
    }
}

fun Int.actType(): ActType {
    ActType.values().forEach { if (it.no == this) return it }
    return ActType.NOTHING
}

fun String.actType(): ActType {
    ActType.values().forEach { if (it.title == this) return it }
    return ActType.NOTHING
}



//
//
//
///**
// * 금액 정리.
// * 순서 중요 : BalanceMap 만들때 기준이 됨.
// */
//enum class CostPrice( val no: Int,val costNo: Int, val rate: Int?, val pnt: Int?, val bcrd: Int?, val crd: Int?, val crt: Int?) {
//
//    /**
//     * Hi~ Created by fabulous on 20-08-16
//     */
//    NOTHING(200, CostType.ACT.no,2, null, null, null, null),       // 항목 미정
//
//    /** CONVERT : 본인이 전환 */
//    CREDIT_TO_PAY(500, CostType.CONVERT.no,2, null, null, null, null),    // receive
//    PAY_TO_CREDIT(600, CostType.CONVERT.no,2, null, null, null, null),    // receive
//    POINT_TO_CREDIT(700, CostType.CONVERT.no,2, null, null, null, null),  // receive
//    CREDIT_TO_POINT(800, CostType.CONVERT.no,2, null, null, null, null),  // receive
//    CHANGE(900, CostType.CONVERT.no,null, null, null, null, null),        // give
//    MONEY(1000, CostType.CONVERT.no,null, null, null, null, null),        //  결제 했을 때
//
//
//    /** SEND : 유저간 주고받기 */
//    GIVE_CREDIT(2000, CostType.SEND.no,null, null, null, null, null), // 다른유저에게 전달
//
//
//    /** TAKE : 획득 */
//    SIGN_UP( 3000, CostType.TAKE.no,null, 5000, null, null, null),
//    BUY_CREDIT(3500, CostType.TAKE.no,null, null, null, null, null), // 크레딧 구매
//    TAKE_CREDIT(3600, CostType.TAKE.no,null, null, null, null, null),// 다른유저에게 받을 때
//
//    BETA_FREE_BONUS_CREDIT(3700, CostType.TAKE.no,null, null, 20000, null, null), // 베타테스터 보너스
////    FIRST_ADD_STORE(3701, null, null, 1500, null, null), // 처음 보너스
////    FIRST_ADD_BOARD(3702, null, null, 1200, null, null), // 처음 보너스
////    FIRST_ADD_POST(3703, null, null, 1000, null, null),  // 처음 보너스
//
//    FREE_BONUS_CREDIT(3720, CostType.TAKE.no,null, null, null, null, null), // 무료 보너스 크레딧
//    THANKS_BONUS_CREDIT(3721, CostType.TAKE.no,null, null, null, null, null), // 감사 보너스 크레딧
//    LUCKY_BONUS_CREDIT(3722, CostType.TAKE.no,null, null, null, null, null),
//    FREE_CREDIT(3723, CostType.TAKE.no,null, null, null, null, null), // 무료 크레딧
//    THANKS_CREDIT(3724, CostType.TAKE.no,null, null, null, null, null), // 감사 크레딧
//    FREE_POINT(3725, CostType.TAKE.no,null, null, null, null, null), // 무료 포인트
//    THANKS_POINT(3726, CostType.TAKE.no,null, null, null, null, null), // 감사 포인트
//
//    WATCH_AD(3800, CostType.TAKE.no,null, 50, null, null, null),// 광고시청
//
//    GET_REPLY_ADOPTED(4000, CostType.TAKE.no,30, null, null, null, null),    // 채택 되었을 때 (rewawrd)
//    GET_REPLY_ADOPTED_COMPLETED(CostType.TAKE.no,4020, 20, null, null, null, null),  // 채택 완료. : 질문자에게 머니백.
//    GET_WARN_COMPLETED(4050, CostType.TAKE.no,20, 5000, null, null, null),  // 올바른 신고 후 :관리자가 처리 완료.
//
//
//    BID_ALLOT(4300, CostType.TAKE.no,50, null, null, null, null),  // 개별보드 낙찰액 배당.
//
//    /** PAYMENT : 지불  (중요) Cost에 기입시 반드시 마이너스 붙일 것. (표기할때는 양수가 가독력이 좋아서(유저입장) 모두 양수로 기재함.)*/
//    ADD_REWARD_POST(5000, CostType.PAYMENT.no,null, 200, null, null, null),    // post에 reward 올렸을 때.
//    ADD_ANONYMOUS_POST(5100, CostType.PAYMENT.no,null, 20, null, 500, null),    // post에 익명 업로드.
//    ADD_ANONYMOUS_FLEA(5120, CostType.PAYMENT.no,null, 20, null, 1000, null),    // flea 익명 업로드.
//
//    BID_ADD(6400, CostType.PAYMENT.no,null, null, null, null, null),  // 입찰
//    BID_ADCREDIT_ADD(6420, CostType.PAYMENT.no,null, null, null, null, null),  // adCredit 추가.
//    //    BID_ADD_BACK (6500, 100, null, null,null,null),  // 크레딧백
////    AD_BIDDING_CANCELED (1120, 100, null, null,null,null),  // 입찰 취소
//    BID_FAILED(6600, CostType.PAYMENT.no,null, null, null, null, null),  // 유찰
//    ERROR_MINUS(6665, CostType.PAYMENT.no,null, null, null, null, null),
//    PENALTY(6666, CostType.PAYMENT.no,null, null, null, null, null),
//
//    PREMIUM_BOARD(7140, CostType.PAYMENT.no,null, 2500, null, 2500, null),  // 프리미엄 보드 등록.
//
//
//    /************************************************************************************************************************************************
//     ************************************************************************************************************************************************
//     * ACT : 활동 (포인트 및 크레딧 변동 트리거 없음: 차후에 어드민에 의해서 pnt만 올라감.)
//     * 중요 : CostType.ACT는 반드시 pnt만 기재할 것. 다른 것들을 쓰려면 CostType 변경 필수.
//     * 여기 정렬된 no의 순서는 중요하지 않다.
//     ************************************************************************************************************************************************
//     ************************************************************************************************************************************************/
//
//
//    ADD_REPLY(4100, CostType.ACT.no,null, 40, null, null, null),
//    ADD_WARN(4120, CostType.ACT.no,null, 200, null, null, null),
//
//    ADD_BOARD(7120, CostType.ACT.no,null, 120, null, null, null),  // 보드 생성.
//    ADD_POST(7230, CostType.ACT.no,null, 120, null, null, null),    // 포스트 올리기.
//    ADD_FLEA(7240, CostType.ACT.no,null, 120, null, null, null),    // 중고 올리기.
//    ADD_ROOM(7360, CostType.ACT.no,null, 120, null, null, null),    // 방 올리기.
//    ADD_STORE(7380, CostType.ACT.no,null, 1200, null, null, null),  // 방 올리기.
//
//
//
//    LOGIN(10200, CostType.ACT.no,null, null, null, null, null),            // 오픈 어플 : when opened MainActivity
//    RE_LOGIN(10220, CostType.ACT.no,null, null, null, null, null),  // 재로그인: 이미 가입자가 어플 새로 로그인시 from SignActivity
//    SIGN_OUT(10230, CostType.ACT.no,null, null, null, null, null),
//    SIGN_OUT_DUPLICATE(10235, CostType.ACT.no,null, null, null, null, null),
//    LEAVE(10250, CostType.ACT.no,null, null, null, null, null),      // 탈퇴
//
//    OPEN_MODEL(10300, CostType.ACT.no,null, null, null, null, null),
//    OPEN_BOARD(10400, CostType.ACT.no,null, 1, null, null, null),
//    OPEN_POST(10500, CostType.ACT.no,null, 1, null, null, null),
//    OPEN_FLEA(10600, CostType.ACT.no,null, 1, null, null, null),
//    OPEN_ROOM(10700, CostType.ACT.no,null, 1, null, null, null),
//    OPEN_STORE(10800, CostType.ACT.no,null, 1, null, null, null),
//
//}
//
///**
// * String 하나로 만들기.
// */
//fun getCostPriceString() {
//    lllogD(
//            StringBuffer().apply {
//                CostPrice.values().forEach {
//                    if (this.isEmpty()) append("${it.no}") else append("$balanceDiv1111${it.no}")
//                    if (this.isEmpty()) append("${it.costNo}") else append("$balanceDiv2${CostType.ACT.no}")
//                    if (it.rate == null) append(balanceDiv2 + "n") else append("$balanceDiv2${it.rate}")
//                    if (it.pnt == null) append(balanceDiv2 + "n") else append("$balanceDiv2${it.pnt}")
//                    if (it.bcrd == null) append(balanceDiv2 + "n") else append("$balanceDiv2${it.bcrd}")
//                    if (it.crd == null) append(balanceDiv2 + "n") else append("$balanceDiv2${it.crd}")
//                    if (it.crt == null) append(balanceDiv2 + "n") else append("$balanceDiv2${it.crt}")
//                }
//            }.toString()
//    )
//}
//
//
//
//
