package com.tkip.mycode.model.warn

import android.content.Context
import android.content.Intent
import android.view.View
import com.tkip.mycode.R
import com.tkip.mycode.dialog.OnDDD
import com.tkip.mycode.dialog.alert.MyAlert
import com.tkip.mycode.dialog.progress.MyCircle
import com.tkip.mycode.dialog.progress.WAIT_TYPE
import com.tkip.mycode.dialog.toast.TToast
import com.tkip.mycode.dialog.toast.toastNormal
import com.tkip.mycode.funs.common.errorWhen
import com.tkip.mycode.init.*
import com.tkip.mycode.init.base.getStr
import com.tkip.mycode.model.board.OnBoard
import com.tkip.mycode.model.cost.Cost
import com.tkip.mycode.model.flea.gotoFleaActivity
import com.tkip.mycode.model.my_enum.*
import com.tkip.mycode.model.peer.checkAllAvailUpload
import com.tkip.mycode.model.peer.myuid
import com.tkip.mycode.model.post.OnPost
import com.tkip.mycode.model.reply.Reply
import com.tkip.mycode.model.room.OnRoom
import com.tkip.mycode.util.lib.TransitionHelper
import com.tkip.mycode.util.lib.retrofit2.ESget
import com.tkip.mycode.util.lib.retrofit2.ESquery
import com.tkip.mycode.util.tools.date.FORMAT_YMD_STRING
import com.tkip.mycode.util.tools.date.getLocalTimeString
import com.tkip.mycode.util.tools.date.rightNow
import com.tkip.mycode.util.tools.fire.*
import java.util.concurrent.TimeUnit

class OnWarn {

    companion object {

        fun gotoWarnListActivity(context: Context, isWarn: Boolean, arrPair: Array<androidx.core.util.Pair<View, String>>?) {
            TransitionHelper.startTran(context, getWarnListIntent(context, isWarn), arrPair)
        }

        fun getWarnListIntent(context: Context, isWarn: Boolean) =
                Intent(context, WarnListActivity::class.java).apply {
                    addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP or Intent.FLAG_ACTIVITY_SINGLE_TOP)
                    putExtra(PUT_BOOLEAN, isWarn)
                }

        /**
         * date8 만들기
         */
        fun getDateLastDate(period: Int): String {
            val lastLong = rightNow() + TimeUnit.DAYS.toMillis(period.toLong())
            return lastLong.getLocalTimeString(FORMAT_YMD_STRING)

//            val today = getDate8TodayLocal(LOCALE_PH).toInt()   // "20200305"
//            val ymd = today.getYMD()
//            val cal = GregorianCalendar(ymd.first, ymd.second - 1, ymd.third)
//            cal.add(Calendar.DATE, period)
//            return cal.timeInMillis.getDate8()
        }


        /**
         * 기신고 체크 and Add
         */
        fun checkAndAdd(context: Context, any: Any, success: () -> Unit, fail: () -> Unit) {

            /*** 업로드 가능 여부 체크 */
            if (checkAllAvailUpload(context)) return

            /** * 자기 게시물은 신고불가 메세지 */
            if (myuid() == any.anyAuthorUid()) {
                toastNormal(R.string.info_not_avail_warn_myself)
                return
            }

            MyAlert.showAddWarn(context, getWarnArr(any),
                    onOk = { warnType, comment ->

                        WAIT_TYPE.BASIC_1.show(context)

                        val warn = getWarnData(any, comment, warnType)

                        // 기신고 여부 확인.
                        ESget.getWarnList(EES_WARN,ESquery.warn(warn.modelKey),
                                success = {
                                    if (it == null || it.isEmpty()) {
                                        /** 새로 신고 */
                                        Firee.addWarn(warn,
                                                success = {
                                                    OnDDD.confirm(context, R.string.warn_completed, {}, null)
//                                                    TToast.showNormal(R.string.warn_completed)
                                                    Cost.new(null, ActType.ADD_WARN, warn).real()

                                                }, fail = { fail() })
                                    } else OnDDD.confirm(context, R.string.warn_already_completed, {}, null)

//                                    TToast.showNormal(R.string.warn_already_completed)
                                    MyCircle.cancel()
                                    success()

                                }, fail = { fail() })
                    }, onCancel = {})

        }

        private fun getWarnData(any: Any, comment: String?, warnType: WarnType): Warn {

            return Warn(comment, warnType).apply {
                this.targetUID = any.anyAuthorUid()
                this.targetNick = any.anyAuthorNick()
                this.dataTypeNo = any.getMyDataType().no
                this.modelKey = any.anyKey()
                this.modelTitle = any.anyTitle()
                this.modelStatus = any.anyStatusNo()

                if (any is Reply) {
                    this.inModel = true
                    this.inDataTypeNo = any.dataTypeNo
                    this.inModelKey = any.modelKey
                }
            }
        }


        /**
         * for master
         * warn 처리하기
         * functions 처리 : peer. penaltyPoint  penaltyCount  penalty  penaltyLastDate
         */
        fun handle(context: Context, warn: Warn, success: () -> Unit, fail: () -> Unit) {

            // 다시 한번 물어볼 것.
            MyAlert.showConfirm(context, R.string.status_dialog_warn_handled.getStr(),
                    ok = {

                        WAIT_TYPE.FULL_AV.show(context)

                        batch().apply {

                            /*** update 신고warn : 처리완료. */
                            warn.statusNo = Status.WARN_HANDLED.no   // 처리완료 status
                            set(docRef(EES_WARN, warn.key), warn)


                            /*** update model.Status : 블라인드 여부 */
                            if (warn.inModel) update(docRef(warn.inDataTypeNo!!.dataType().child, warn.inModelKey!!, warn.dataTypeNo.dataType().child, warn.modelKey), CH_STATUS_NO, warn.modelStatus)  // 2중 child
                            else update(docRef(warn.dataTypeNo.dataType().child, warn.modelKey), CH_STATUS_NO, warn.modelStatus)  // 단일 child


                            /*** add 패널티 : 패널티 날짜 기입했을 때만 */
                            if (warn.period == null) warn.deletePenalty()
                            else set(docRef(EES_PENALTY, warn.key), warn)   // functions에서 peer 수정.


                            myCommit("OnWarnHandle", success = {
                                success()
                                toastNormal(R.string.info_warn_handle_add_success)
                            }, fail = { fail() })
                        }
                    }, cancel = { fail() })
        }


        /**
         * for Admin
         * warnActivity 리스트에서 항목 클릭시 이동.
         */
        fun gotoModel(context: Context, warn: Warn, arrPair: Array<androidx.core.util.Pair<View, String>>?) {
            when (warn.dataTypeNo.dataType()) {

                DataType.BOARD -> ESget.getBoard(warn.modelKey,
                        success = {
                            it?.let { board1 -> OnBoard.gotoBoardActivity(context, true, null, board1, arrPair) }
                                    ?: TToast.showNormal(R.string.error_data_deleted)
                        }, fail = {})

                DataType.POST, DataType.REPLY -> ESget.getPost(warn.modelKey,
                        success = {
                            it?.let { post1 -> OnPost.gotoPostActivity(context, true, null, post1, arrPair) }
                                    ?: TToast.showNormal(R.string.error_data_deleted)
                        }, fail = {})

                DataType.FLEA -> ESget.getFlea(warn.modelKey,
                        success = {
                            it?.gotoFleaActivity(context, true, ACTION_NORMAL, arrPair)
                                    ?: TToast.showNormal(R.string.error_data_deleted)
                        }, fail = {})
                DataType.ROOM -> ESget.getRoom(warn.modelKey,
                        success = {
                            it?.let { room -> OnRoom.gotoRoomActivity(context, true, ACTION_NORMAL, room, arrPair) }
                                    ?: TToast.showNormal(R.string.error_data_deleted)
                        }, fail = {})
                else -> errorWhen()
            }

        }


    }

}