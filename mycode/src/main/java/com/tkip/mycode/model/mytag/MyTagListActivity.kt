package com.tkip.mycode.model.mytag

import android.os.Bundle
import android.view.View
import androidx.annotation.LayoutRes
import androidx.fragment.app.Fragment
import com.tkip.mycode.R
import com.tkip.mycode.databinding.ActivityMytagListBinding
import com.tkip.mycode.dialog.alert.MyAlert
import com.tkip.mycode.funs.common.keyword
import com.tkip.mycode.funs.custom.toolbar.ViewToolbarIcon
import com.tkip.mycode.funs.search.model.CustomSearchTag
import com.tkip.mycode.funs.search.view.CustomSearchEditText
import com.tkip.mycode.init.base.getStr
import com.tkip.mycode.init.my_bind.BindingActivity
import com.tkip.mycode.util.tools.etc.OnMyClick

/**
 * for admin
 * 모든 mytag 리스트
 */
class MyTagListActivity : BindingActivity<ActivityMytagListBinding>() {
    @LayoutRes
    override fun getLayoutResId() = R.layout.activity_mytag_list

    //    private val manage by lazy { intent?.getBooleanExtra(PUT_MANAGE, false) ?: false }
//    private val uid by lazy {  intent?.getStringExtra(PUT_UID) ?: myuid()}
    private val list = arrayListOf<Pair<Fragment, String>>()
    //    lateinit var customModelList: CustomSearchList
    private lateinit var viewToolbarIcon: ViewToolbarIcon
    private lateinit var customSearchTag: CustomSearchTag
    private lateinit var customSearchEditText: CustomSearchEditText

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        initView()

        /** 시작하자마자 검색 */
        customSearchTag.getEsDataList(0, null)
//        Handler().postDelayed({ customSearchTag.getEsDataList(0, null) }, 2000)
    }

    private fun initView() {
        b.activity = this

        /** 툴바 */
        viewToolbarIcon = ViewToolbarIcon(this@MyTagListActivity, onBack = { onBackPressed() }, topTitle = null, onTopTitle = {}, title = R.string.title_tag.getStr(), menuText = R.string.add_mytag.getStr(), onTextMenu = { onClickAdd(it) }, onMore = null)
        b.frameToolbar.addView(viewToolbarIcon)

        /** 검색창 */
        customSearchEditText = CustomSearchEditText(this@MyTagListActivity,
                onClear = { customSearchTag.getEsDataList(0, null) },
                onSearch = { wordList -> wordList?.let { customSearchTag.getEsDataList(0, wordList.getStr()) } })
        b.lineCover.addView(customSearchEditText)

        /** 태그 리스트 */
        customSearchTag = CustomSearchTag(this@MyTagListActivity,true, TagTYPE.ALL)
        b.lineCover.addView(customSearchTag)

    }

    /**     * TagType 선택    */
    fun onClickTagType(v: View) {
        OnMyClick.setDisableAWhileBTN(v)
        val tagTypeList = arrayListOf<String>().setTagType()
        MyAlert.showSelectString(this@MyTagListActivity, null, tagTypeList,
                onSelected = { position ->
                    customSearchTag.tagTYPE = tagTypeList[position].getTagType()
                    viewToolbarIcon.setTopTitle(customSearchTag.tagTYPE.title)
                    customSearchTag.getEsDataList(0, customSearchEditText.b.etSearch.keyword())
                })
    }

    /**     * 새 태그 만들기     */
    fun onClickAdd(v: View) {
        OnMyClick.setDisableAWhileBTN(v)
        val newTag = customSearchEditText.b.etSearch.keyword()?.let { MyTag(it, if (customSearchTag.tagTYPE == TagTYPE.ALL) TagTYPE.LOC else customSearchTag.tagTYPE, null) }   // 현재 et의 키워드 그대로.
        ADDmytagDialog.newInstance(newTag).show(this@MyTagListActivity.supportFragmentManager, "dialog")
    }


}