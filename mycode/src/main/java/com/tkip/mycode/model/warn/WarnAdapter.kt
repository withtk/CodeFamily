package com.tkip.mycode.model.warn

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.RecyclerView
import com.tkip.mycode.R
import com.tkip.mycode.databinding.HolderPenaltyBinding
import com.tkip.mycode.databinding.HolderWarnBinding
import com.tkip.mycode.dialog.OnDDD
import com.tkip.mycode.dialog.warn.PenaltyDialog
import com.tkip.mycode.dialog.warn.WarnHandleDialog
import com.tkip.mycode.funs.common.tranPair
import com.tkip.mycode.util.tools.etc.OnMyClick
import com.tkip.mycode.util.tools.fire.Firee

class WarnAdapter(private val items: ArrayList<Warn>, val rvType: Int, private val onItemClick: ((Int, Warn) -> Unit)?) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    private lateinit var context: Context

    companion object {
        val rvWarn = 100
        //        val rvWarnHandled = 200
        val rvPenalty = 500

        //todo : 각각의 홀더 만들 것.
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        context = parent.context
        return when (rvType) {
            rvWarn -> WarnViewHolder(HolderWarnBinding.inflate(LayoutInflater.from(parent.context), parent, false))
            rvPenalty -> PenaltyHolder(HolderPenaltyBinding.inflate(LayoutInflater.from(parent.context), parent, false))
            else -> WarnViewHolder(HolderWarnBinding.inflate(LayoutInflater.from(parent.context), parent, false))
        }
    }

    override fun getItemId(position: Int): Long {
        return items[position].hashCode().toLong()
    }

    override fun getItemCount(): Int {
        return items.size
    }

    override fun onBindViewHolder(h: RecyclerView.ViewHolder, p: Int) {

        when (rvType) {
            rvWarn -> (h as WarnViewHolder).bind(items[p])
            rvPenalty -> (h as PenaltyHolder).bind(items[p])
            else -> (h as WarnViewHolder).bind(items[p])
        }
    }


    /******************************************************************************************
     * 기본 Warn 신고내역    ********************************************************************
     *******************************************************************************************/
    inner class WarnViewHolder(val b: HolderWarnBinding) : RecyclerView.ViewHolder(b.root) {

        private lateinit var warn: Warn

        fun bind(warn: Warn) {
            this.warn = warn
            b.holder = this
            b.warn = warn

            b.executePendingBindings()
        }


        fun onClickMove() {
            OnWarn.gotoModel(context, warn, arrayOf(b.line.tranPair(R.string.tran_linear)))
        }

        fun onClickDeepDel(v: View) {
            OnMyClick.setDisableAWhileBTN(v)
            OnDDD.deepDel(context,
                    ok = {
                        Firee.deepDelWarn(warn.key,
                                success = {
                                    items.removeAt(adapterPosition)
                                    notifyItemRemoved(adapterPosition)
                                }, fail = {})
                    }, cancel = {})
        }

        fun onClickCard(v: View) {
            OnMyClick.setDisableAWhileBTN(v)

//            onItemClick?.invoke(adapterPosition,warn)

            WarnHandleDialog.newInstance(warn,
                    onOk = {
                        warn = it
                        notifyItemChanged(adapterPosition)
                    }).show((context as AppCompatActivity).supportFragmentManager, "dialog")

//            MyAlert.showWarnHandle(activity, warn,
//                    onFinished = {
//                        warn.statusNo = Status.WARN_HANDLED.no
//                        b.warn = warn
//                        items[adapterPosition] = warn
//                    })

        }
    }

    /******************************************************************************************
     * 패널티 내역    ********************************************************************
     *******************************************************************************************/
    inner class PenaltyHolder(val b: HolderPenaltyBinding) : RecyclerView.ViewHolder(b.root) {

        private lateinit var warn: Warn

        fun bind(warn: Warn) {
            this.warn = warn
            b.holder = this
            b.warn = warn

            b.executePendingBindings()
        }


        fun onClickMove() {
            OnWarn.gotoModel(context, warn, arrayOf(b.line.tranPair(R.string.tran_linear)))
        }

        fun onClickCard(v: View) {
            OnMyClick.setDisableAWhileBTN(v)

//            onItemClick?.invoke(adapterPosition,warn)

            /**         *  패널티 Dialog 오픈         */
            PenaltyDialog.newInstance(warn, null, onOk = {}).show((context as AppCompatActivity).supportFragmentManager, "dialog")

//            WarnHandleDialog.newInstance(warn,
//                    onOk = {
//                        warn = it
//                        notifyItemChanged(adapterPosition)
//                    }).show((context as AppCompatActivity).supportFragmentManager, "dialog")

//            MyAlert.showWarnHandle(activity, warn,
//                    onFinished = {
//                        warn.statusNo = Status.WARN_HANDLED.no
//                        b.warn = warn
//                        items[adapterPosition] = warn
//                    })

        }
    }

}