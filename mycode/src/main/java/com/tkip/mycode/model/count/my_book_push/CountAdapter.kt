package com.tkip.mycode.model.count.my_book_push

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.CheckBox
import androidx.recyclerview.widget.RecyclerView
import com.tkip.mycode.R
import com.tkip.mycode.databinding.HolderBookCountBinding
import com.tkip.mycode.databinding.HolderPushCountBinding
import com.tkip.mycode.databinding.HolderQuickCountBinding
import com.tkip.mycode.dialog.OnDDD
import com.tkip.mycode.funs.common.*
import com.tkip.mycode.init.inter.interHolderBasic
import com.tkip.mycode.model.count.Count
import com.tkip.mycode.model.count.delete
import com.tkip.mycode.model.count.quick.OnQuick
import com.tkip.mycode.model.count.syncModelInHolder
import com.tkip.mycode.model.my_enum.checkAndGo
import com.tkip.mycode.util.tools.anim.AnimUtil
import com.tkip.mycode.util.tools.etc.OnMyClick
import com.tkip.mycode.util.tools.fire.Firee
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

class CountAdapter(private val context: Context,private val items: ArrayList<Count>, private val rvType: Int) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

//    private lateinit var context: Context

    companion object {
        const val rvMyBook = 200
        const val rvMyPush = 300
        const val rvMyQuick = 400
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
//        context = parent.context
        return when (rvType) {
            rvMyBook -> BookCountViewHolder(HolderBookCountBinding.inflate(LayoutInflater.from(parent.context), parent, false))
            rvMyPush -> PushCountViewHolder(HolderPushCountBinding.inflate(LayoutInflater.from(parent.context), parent, false))
            rvMyQuick -> QuickCountViewHolder(HolderQuickCountBinding.inflate(LayoutInflater.from(parent.context), parent, false))
            else -> BookCountViewHolder(HolderBookCountBinding.inflate(LayoutInflater.from(parent.context), parent, false))
        }
    }

    override fun onBindViewHolder(h: RecyclerView.ViewHolder, p: Int) {
        when (rvType) {
            rvMyBook -> (h as BookCountViewHolder).bind(items[p])
            rvMyPush -> (h as PushCountViewHolder).bind(items[p])
            rvMyQuick -> (h as QuickCountViewHolder).bind(items[p])
            else -> (h as BookCountViewHolder).bind(items[p])
        }
    }


    override fun getItemId(position: Int): Long {
        return items[position].hashCode().toLong()
    }

    override fun getItemCount(): Int {
        return items.size
    }


    /*******************************************************************************************
     * 모든 즐겨찾기에 적용.
     *******************************************************************************************/
    inner class BookCountViewHolder(val b: HolderBookCountBinding) : RecyclerView.ViewHolder(b.root) {

        private lateinit var count: Count

        fun bind(count: Count) {
            this.count = count
            b.holder = this
            b.count = count

//            if (b.iv.drawable != null) {
//                GlobalScope.launch {
//                    withContext(Dispatchers.Main) {
//                        count.getModel { b.photoThumb = it?.anyThumb(0) }
//                    }
//                }
//            }

            b.executePendingBindings()
        }

        fun onClickQuick(v: View) {
            lllogI("MyBookViewHolder onClickAddQuick View")
            OnMyClick.setDisableAWhileBTN(v)

            /**             * quick 수량 체크             */
            if (OnQuick.overLimitQuickCount()) {
                if (v is CheckBox) v.isChecked = false
                return
            }
            if (b.cbQuick.isChecked) Firee.addQuick(count, success = {}, fail = {})
            else Firee.deleteQuick(count, success = {}, fail = {})
        }

        fun onClickDelete(v: View) {
            OnMyClick.setDisableAWhileBTN(v)

            OnVisible.toggleDB(true, b.avLoading, b.btnDelete)
            OnDDD.delete(context, ok = {
                count.delete(
                        success = {
                            OnVisible.toggleDB(false, b.avLoading, b.btnDelete)
                            AnimUtil.hideOverRight(b.line)
                            items.remove(count)
                            notifyItemRemoved(adapterPosition)
                        }, fail = { OnVisible.toggleDB(false, b.avLoading, b.btnDelete) })
            }, cancel = { OnVisible.toggleDB(false, b.avLoading, b.btnDelete) })

        }

        fun onClickCard(v: View) {
            OnMyClick.setDisableAWhileBTN(v)
            count.checkAndGo(context,false, arrayOf(b.line.tranPair(R.string.tran_linear)),
                    onSuccess = {
                        /*** count 동기화하기 : db Count가 변경됐을 수도 있음. */
                        GlobalScope.launch { withContext(Dispatchers.Main) { count.syncModelInHolder(false, it, success = { notifyItemChanged(adapterPosition) }) } }
                    })
        }
    }

    /********************************************************************************************
     * 모든 푸쉬
     ********************************************************************************************/
    inner class PushCountViewHolder(val b: HolderPushCountBinding) : RecyclerView.ViewHolder(b.root) {

        private lateinit var count: Count

        fun bind(count: Count) {
            this.count = count
            b.holder = this
            b.count = count

            b.executePendingBindings()
        }

        fun onClickCard(v: View) {
            OnMyClick.setDisableAWhileBTN(v)
            count.checkAndGo(context, false,arrayOf(b.line.tranPair(R.string.tran_linear)),
                    onSuccess = {
                        /*** count 동기화하기 : db Count가 변경됐을 수도 있음. */
                        GlobalScope.launch { withContext(Dispatchers.Main) { count.syncModelInHolder(false, it, success = { notifyItemChanged(adapterPosition) }) } }
                    })
        }

        fun onClickDelete(v: View) {
            OnMyClick.setDisableAWhileBTN(v)

            OnVisible.toggleDB(true, b.avLoading, b.btnDelete)
            OnDDD.delete(context, ok = {
                count.delete(
                        success = {
                            OnVisible.toggleDB(false, b.avLoading, b.btnDelete)
                            AnimUtil.hideOverRight(b.line)
                            items.remove(count)
                            notifyItemRemoved(adapterPosition)
                        }, fail = { OnVisible.toggleDB(false, b.avLoading, b.btnDelete) })
            }, cancel = { OnVisible.toggleDB(false, b.avLoading, b.btnDelete) })
        }
    }


    /********************************************************************************************
     * Quick In 내비게이션 메뉴
     ********************************************************************************************/
    inner class QuickCountViewHolder(private val b: HolderQuickCountBinding) : RecyclerView.ViewHolder(b.root) {

        private lateinit var count: Count

        fun bind(count: Count) {
            this.count = count
            b.count = count
            b.inter = interHolderBasic(onClickCard = { onClickCard(it) }, onClickGo = null, onClickUpdate = null, onClickDel = null, onDeepDel = null, onClickStatus = null)


            b.executePendingBindings()
        }

        fun onClickCard(v: View) {
            OnMyClick.setDisableAWhileBTN(v)

            count.checkAndGo(context, false,b.line.arrTranLinear(),
                    onSuccess = {
                        /*** count 동기화하기 : db Count가 변경됐을 수도 있음. */
                        GlobalScope.launch { withContext(Dispatchers.Main) { count.syncModelInHolder(true, it, success = { notifyItemChanged(adapterPosition) }) } }
                    })
        }

    }

}