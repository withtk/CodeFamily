package com.tkip.mycode.model.manage

import android.os.Parcel
import android.os.Parcelable
import com.tkip.mycode.util.tools.date.rightNow

/**
 * 백엔드 상태변경 스위치 for functions.
 * EES_NOTICE > hit
 */
data class Hit(

        var key: String = "hit",
        var addDate: Long = rightNow()

) : Parcelable {

    constructor(source: Parcel) : this(
            source.readString()!!,
            source.readLong()
    )

    override fun describeContents() = 0

    override fun writeToParcel(dest: Parcel, flags: Int) = with(dest) {
        writeString(key)
        writeLong(addDate)
    }

    companion object {
        @JvmField
        val CREATOR: Parcelable.Creator<Hit> = object : Parcelable.Creator<Hit> {
            override fun createFromParcel(source: Parcel): Hit = Hit(source)
            override fun newArray(size: Int): Array<Hit?> = arrayOfNulls(size)
        }
    }
}
