package com.tkip.mycode.model.reply

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.RecyclerView
import com.firebase.ui.firestore.FirestoreRecyclerAdapter
import com.firebase.ui.firestore.FirestoreRecyclerOptions
import com.google.firebase.firestore.FirebaseFirestoreException
import com.tkip.mycode.databinding.HolderReplyMyBinding
import com.tkip.mycode.databinding.HolderReplyUserBinding
import com.tkip.mycode.dialog.alert.MyAlert
import com.tkip.mycode.funs.common.OnException
import com.tkip.mycode.model.peer.OnPeer
import com.tkip.mycode.model.peer.Peer
import com.tkip.mycode.model.peer.isAdmin
import com.tkip.mycode.model.toss.OnToss
import com.tkip.mycode.model.toss.ViewTossModel
import com.tkip.mycode.util.lib.photo.Foto
import com.tkip.mycode.util.lib.photo.photoview.OnPhotoView
import com.tkip.mycode.util.my_view.ViewUtil
import com.tkip.mycode.util.tools.etc.OnMyClick
import com.tkip.mycode.util.tools.fire.Firee

class ReplyAdapterFireS(options: FirestoreRecyclerOptions<Reply>, val topAny: Any, val onCreateReReply: (Reply, Int) -> Unit, val onClickEarlierReply: (Int) -> Unit, val onCompleteAdoption: () -> Unit) : FirestoreRecyclerAdapter<Reply, RecyclerView.ViewHolder>(options) {
    private lateinit var activity: AppCompatActivity

    private val viewReplyMy = 10
    private val viewReplyUser = 20

    override fun onDataChanged() {
        super.onDataChanged()
    }

    override fun onError(e: FirebaseFirestoreException) {
        super.onError(e)
    }

    override fun getItemViewType(position: Int): Int {
        return if (OnPeer.isMyPeer(getItem(position).authorUID)) viewReplyMy
        else viewReplyUser
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        this.activity = parent.context as AppCompatActivity
        return when (viewType) {
            viewReplyMy -> ReplyMyViewHolder(HolderReplyMyBinding.inflate(LayoutInflater.from(parent.context), parent, false))
            viewReplyUser -> ReplyUserViewHolder(HolderReplyUserBinding.inflate(LayoutInflater.from(parent.context), parent, false))
            else -> ReplyUserViewHolder(HolderReplyUserBinding.inflate(LayoutInflater.from(parent.context)))
        }
    }

    override fun onBindViewHolder(h: RecyclerView.ViewHolder, p: Int, m: Reply) {

        val preReply = if (p == 0) null else getItem(p - 1)  //이전 Reply 세팅.

        if (preReply == null || preReply.authorUID != m.authorUID) {  //1.첫번째 2.상위와 다른 peer
            Firee.getPeer(m.authorUID,
                    success = {
                        bind(h, m, preReply, isShow = true, currentPeer = it)
                    }, fail = {})
//            m.authorUID.getPeerFromMap { bind(h, m, preReply, isShow = true, currentPeer = it) }
        } else {   // 상위와 같은 peer
            bind(h, m, preReply, isShow = false, currentPeer = null)
        }

    }

    private fun bind(h: RecyclerView.ViewHolder, m: Reply, preReply: Reply?, isShow: Boolean, currentPeer: Peer?) {
        when (h) {
            is ReplyUserViewHolder -> h.bind(m, preReply, isShow, currentPeer)
            is ReplyMyViewHolder -> h.bind(m, preReply)
            else -> OnException.forceException("onBindViewHolder in ReplyAdapterFireS (reply key:${m.key})")
        }
    }

    /**     * rereply의 reply 받아오기     */
    private fun getEarlier(earlierKey: String): Pair<Reply, Int>? {
        snapshots.forEachIndexed { i, reply -> if (reply.key == earlierKey) return Pair(reply, i) }
        return null
    }

    private fun commonOnClickIv(reply: Reply) {
        reply.url?.let { OnPhotoView.gotoPhotoViewActivity(activity, Foto(reply.thumbUrl, reply.url), null) }
    }


    /**************************************************************
     ********************** User Viewholder *********************
     **************************************************************/
    inner class ReplyUserViewHolder(val b: HolderReplyUserBinding) : RecyclerView.ViewHolder(b.root) {

        private lateinit var reply: Reply

        fun bind(reply: Reply, preReply: Reply?, isShowPeer: Boolean, currentPeer: Peer?) {
            this.reply = reply
            b.holder = this
            b.reply = reply
            b.preReply = preReply
            b.isShowPeer = isShowPeer
            b.peer = currentPeer

            //  앞에것과 체크해서 날짜 삽입
//            b.lineAttach.addView(ViewDateSmallTop(activity,reply.addDate))


//            reply.thumbUrl?.let {
//                b.lineAttach.addView(ViewPhotoAttach(activity,it))
//            }

            /**  toss 첨부 */
            reply.tossModelKey?.let {
                ViewTossModel(activity, b.lineAttach, reply.tossModelKey, reply.tossDataTypeNo, onGoto = null, onRemove = null, onClose = null, onLong = { onLongClickChat() })
//                OnToss.attachToss(activity, b.lineAttach, reply.tossModelKey, reply.tossDataTypeNo)
            }


            /**  replyEarlier 첨부 */
            reply.earlierKey?.let { key ->
                getEarlier(key)?.let { pair ->
                    ViewUtil.addViewReplyEarlier(activity, b.lineAttach, pair.first, onClick = { onClickEarlierReply(pair.second) })
                }
            }

            /**  상태 LongClick 처리 */
            if (isAdmin()) b.tvInfoStatus.setOnLongClickListener {
                onLongClickChat()
                return@setOnLongClickListener true
            }

            b.executePendingBindings()
        }

        fun onClickAuthor(v: View) {
            OnMyClick.setDisableAWhileBTN(v)
            MyAlert.showPeerInfo(activity, reply.authorUID, onLobby = {})
        }

        fun onClickIv() {
            commonOnClickIv(reply)
        }

        fun onLongClickChat(): Boolean {
            OnReply.showMenuDialog(activity, topAny, reply,
                    onCreateReReply = { onCreateReReply(reply, adapterPosition) },
                    onCompleteAdoption = { onCompleteAdoption() },
                    onChanged = { status ->
                        reply.statusNo = status.no
                        notifyItemChanged(adapterPosition)
                    }
            )
            return true
        }

    }

    /***************************************************************
     *************************** My Viewholder *********************
     **************************************************************/
    inner class ReplyMyViewHolder(val b: HolderReplyMyBinding) : RecyclerView.ViewHolder(b.root) {

        private lateinit var reply: Reply

        fun bind(reply: Reply, preReply: Reply?) {
            this.reply = reply
            b.holder = this
            b.reply = reply
            b.preReply = preReply


            /**  toss 첨부 */
            reply.tossModelKey?.let {
                ViewTossModel(activity, b.lineAttach, reply.tossModelKey, reply.tossDataTypeNo, onGoto = null, onRemove = null, onClose = null, onLong = { onLongClickChat() })
//                OnToss.attachToss(activity, b.lineAttach, reply.tossModelKey, reply.tossDataTypeNo)
            }

            /**  replyEarlier 첨부 */
            reply.earlierKey?.let { key ->
                getEarlier(key)?.let { pair ->
                    ViewUtil.addViewReplyEarlier(activity, b.lineAttach, pair.first, onClick = { onClickEarlierReply(pair.second) })
                }
            }

            /**  상태 LongClick 처리 */
            if (isAdmin()) b.tvInfoStatus.setOnLongClickListener {
                onLongClickChat()
                return@setOnLongClickListener true
            }

//            reply.earlierKey?.let {
//                getEarlier(it)?.let { pair ->
//                    ViewUtil.addViewReplyEarlier(activity, b.lineEarlier, pair.first, { onClickReply(pair.second) })
//                }
//            }

            b.executePendingBindings()
        }

        fun onClickCard() {

        }

        fun onClickIv() {
            commonOnClickIv(reply)
        }

        fun onLongClickChat(): Boolean {
            OnReply.showMenuDialog(activity, topAny, reply,
                    onCreateReReply = { onCreateReReply(reply, adapterPosition) },
                    onCompleteAdoption = {},
                    onChanged = { status ->
                        reply.statusNo = status.no
                        notifyItemChanged(adapterPosition)
                    }
            )
            return true
        }

    }


}
