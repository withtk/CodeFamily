package com.tkip.mycode.model.board

import com.tkip.mycode.R
import com.tkip.mycode.init.base.getStr


/***********************************************************************************
 * 포스트  r w e
 * 댓글쓰기
 *
 * 신고처리 Id
 *
 * 유저 종류
 *     - 슈퍼어드민
 *     - 일반어드민
 *     - 마스터유저
 *     - 게시유저
 *     - 일반유저
 *     - 익명유저
 *
 *************************************************************************************/




/**************************** sttBoard  ************************/
val BOARD_TYPE_STR_LIST_ADMIN by lazy { arrayListOf<String>().apply { BoardType.values().forEach { add(it.title) } } }
//val BOARD_TYPE_STR_LIST by lazy { arrayOf(BoardType.USER.title, BoardType.CS_CENTER.title).toList() as ArrayList }

enum class BoardType(val no: Int, val title: String, val searchBoard: Boolean, val searchPost: Boolean, val test: Int, val up: Boolean, val down: Boolean, val book: Boolean, val push: Boolean, val toss: Boolean, val warn: Boolean) {
    FIXED(100, R.string.board_type_fixed.getStr(), false, false, 1, true, true, true, true, true, true),         // 초기 기본 필수 고정게시판. recentPost, flea, room
    ADMIN(200, R.string.board_type_admin.getStr(), false, false, 1, true, true, true, true, true, true),         // 어드민이 만든 보드 : 차후에 디자인 조금 변경할 때 사용할 듯.
    USER(500, R.string.board_type_user.getStr(), true, true, 1, true, true, true, true, true, true),            // 유저가 만든 게시판.
    CS_CENTER(700, R.string.board_type_cs.getStr(), true, false, 1, false, false, true, true, false, false),   // 고객센터 : 자신의 글만 읽음. 중요:모든 포스트 및 댓글 검색에서 제거.
//    STORE(150,false, true),   // 스토어의 게시판 :스토어관련 얘기 나누는 보드 (일반보드로 만들면 되잖아..?) 차후에 사용 가능성 있나.>? 욕쓰는 애들 때문에 안 하는게 나을 듯.
//    CUSTOMIZE(800,false, Pair(UNIQUE, UNIQUE)),      // 개별 수정.  이건 꼭 해야하나.. premium Boolean으로 해서 알아서 선택하는 쪽으로 하는 것이 낫다.
    ;
}

fun Int.getBoardType(): BoardType {
    BoardType.values().forEach { if (this == it.no) return it }
    return BoardType.CS_CENTER
}

fun String.getBoardType(): BoardType {
    BoardType.values().forEach { if (this == it.title) return it }
    return BoardType.CS_CENTER
}


//private const val ALL = 7      // ADMIN + MASTER + AUTHOR + USER
//private const val AUTHOR = 5   // ADMIN + MASTER + AUTHOR
//private const val UNIQUE = 3   // ADMIN + MASTER + 특정유저
//private const val MASTER = 1   // ADMIN + MASTER
//private const val ADMIN = 0    // ADMIN

//ACT : READ+4, WRITE+2, EXECUTE+1
//private const val ALL_ACT = 7
//private const val READ = 4   // READ ONLY
//private const val WRITE = 2  // READ & WRITE


/**
 * postPair : Pair<읽기,쓰기>
 */
//enum class AllowType(val no: Int,val postNotAvailSearchFixed:Boolean, val post: Pair<Int, Int>) {
//    BASIC(1000,false, Pair(ALL, ALL)),                // 기본 보드
//    READ_ONLY(3000,false, Pair(ALL, MASTER)),         // 읽기전용.
//    CS_CENTER(4000,true, Pair(AUTHOR, AUTHOR)),       // 고객센터 : 자신의 글만 읽음. 중요:모든 포스트 및 댓글 검색에서 제거.
//    CUSTOMIZE(5000,false, Pair(UNIQUE, UNIQUE)),      // 개별 수정.  이건 꼭 해야하나.. 타입을 선택하는 쪽으로 하는 것이 낫다.
//    ;
//}


/**
 *   프리미엄 보드 타입 설정. : 변경 불가..?
 *   Pair : READ, WRITE
 *   postPair : 권한설정.
 *   postNotAvailSearchFixed : post 검색에서 무조건 제거 = true
 *   todo : 댓글 가능 여부와 댓글 read write 모두 강제로 차단할 수 있게.
 *   todo : 문제는 이걸 변경할 수 있게 하느냐의 문제.
 */
//enum class PremiumType(val no: Int,val postNotAvailSearchFixed:Boolean, val postPair: Pair<Int, Int>) {
//    BASIC(1000,false, Pair(ALL, ALL)),                // 기본 보드
//    READ_ONLY(3000,false, Pair(ALL, MASTER)),         // 읽기전용.
//    CS_CENTER(4000,true, Pair(AUTHOR, AUTHOR)),       // 고객센터 : 자신의 글만 읽음. 중요:모든 포스트 및 댓글 검색에서 제거.
//    CUSTOMIZE(5000,false, Pair(UNIQUE, UNIQUE)),      // 개별 수정.  이건 꼭 해야하나.. 타입을 선택하는 쪽으로 하는 것이 낫다.
//    ;
//}
