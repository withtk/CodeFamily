package com.tkip.mycode.model.peer.peer_allow

import android.os.Parcel
import android.os.Parcelable
import com.tkip.mycode.model.board.Board
import com.tkip.mycode.model.peer.Peer
import com.tkip.mycode.util.tools.date.rightNow

/**
 * for Board
 *     작성 가능 리스트
 *     작성 불가능 리스트
 */
data class PeerAllow(

        var key: String = "boardkeyPlusUid",    // board.key + uid
        var allowPost: Boolean = true,      // 포스팅 작성.
        var allowReply: Boolean = true,    // 댓글 작성.

        ////////////// 해당 peer ///////////////////////
        var uid: String = "SixUIDthisIsDefault",
        var email: String? = null,
        var nick: String = "NickDefault",
        var faceThumb: String? = null,

        ////////////// 해당 board ///////////////////////
        var modelKey: String = "modelKey",
        var modelName: String = "modelName",
        var modelAuthorUid: String = "modelAuthorUid",   // 보드마스터 uid

        var addDate: Long = rightNow()

) : Parcelable {
    constructor(board: Board, peer: Peer) : this(
            key = board.key + peer.uid,
            uid = peer.uid,
            email = peer.email,
            nick = peer.nick,
            faceThumb = peer.faceThumb,

            modelKey = board.key,
            modelName = board.title,
            modelAuthorUid = board.authorUID
    )

    constructor(source: Parcel) : this(
            source.readString()!!,
            1 == source.readInt(),
            1 == source.readInt(),
            source.readString()!!,
            source.readString(),
            source.readString()!!,
            source.readString(),
            source.readString()!!,
            source.readString()!!,
            source.readString()!!,
            source.readLong()
    )

    override fun describeContents() = 0

    override fun writeToParcel(dest: Parcel, flags: Int) = with(dest) {
        writeString(key)
        writeInt((if (allowPost) 1 else 0))
        writeInt((if (allowReply) 1 else 0))
        writeString(uid)
        writeString(email)
        writeString(nick)
        writeString(faceThumb)
        writeString(modelKey)
        writeString(modelName)
        writeString(modelAuthorUid)
        writeLong(addDate)
    }

    companion object {
        /*** 고정. DB와 연동. 변경 불가 */
//        const val ALLOWED = 100
//        const val DENIED = 200



        @JvmField
        val CREATOR: Parcelable.Creator<PeerAllow> = object : Parcelable.Creator<PeerAllow> {
            override fun createFromParcel(source: Parcel): PeerAllow = PeerAllow(source)
            override fun newArray(size: Int): Array<PeerAllow?> = arrayOfNulls(size)
        }
    }
}









