package com.tkip.mycode.model.count.quick

import android.os.Bundle
import android.view.View
import androidx.annotation.LayoutRes
import androidx.recyclerview.widget.ItemTouchHelper
import com.tkip.mycode.R
import com.tkip.mycode.databinding.ActivityQuickBinding
import com.tkip.mycode.dialog.OnDDD
import com.tkip.mycode.dialog.alert.MyAlert
import com.tkip.mycode.dialog.progress.WAIT_TYPE
import com.tkip.mycode.dialog.toast.toastNormal
import com.tkip.mycode.funs.common.lllogD
import com.tkip.mycode.funs.common.lllogI
import com.tkip.mycode.funs.custom.toolbar.ViewToolbarIcon
import com.tkip.mycode.init.EES_QUICK
import com.tkip.mycode.init.QUICK_MENU_LIMIT

import com.tkip.mycode.init.base.getStr
import com.tkip.mycode.init.my_bind.BindingActivity
import com.tkip.mycode.model.count.Count
import com.tkip.mycode.model.peer.myuid
import com.tkip.mycode.util.lib.MyTouchHelper
import com.tkip.mycode.util.lib.retrofit2.ESget
import com.tkip.mycode.util.lib.retrofit2.ESquery
import com.tkip.mycode.util.tools.anim.hideFadeOut
import com.tkip.mycode.util.tools.anim.showSlideUp
import com.tkip.mycode.util.tools.anim.visi
import com.tkip.mycode.util.tools.etc.OnMyClick
import com.tkip.mycode.util.tools.fire.Firee

/**
 * quick 메뉴 수정하는 activity
 */
class QuickManageActivity : BindingActivity<ActivityQuickBinding>() {
    @LayoutRes
    override fun getLayoutResId() = R.layout.activity_quick

    //    private val oldItems by lazy { intent?.getParcelableArrayListExtra<Count>(PUT_ARRAY_COUNT) }
//    private val oldMap = hashMapOf<String, Count>()

    //    private val newMap = hashMapOf<String, Count>()
    private val oldItems = arrayListOf<Count>()
    private val newItems = arrayListOf<Count>()
    private lateinit var viewToolbarIcon: ViewToolbarIcon
    private lateinit var adapter: QuickManageAdapter
    private lateinit var itemTouchHelper: ItemTouchHelper


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        b.progress.visi()
        initView()
    }

    private fun initView() {
        b.activity = this

        /** 툴바 */
        b.frameToolbar.addView(ViewToolbarIcon(this@QuickManageActivity,
                onBack = { onBackPressed() }, title = R.string.title_quick_menu.getStr(), menuText = R.string.btn_save.getStr(),
                onTextMenu = { onClickSave(it) }, onMore = null))
        setAdapter()
        getEs()
    }

    private fun getEs() {
        ESget.getCountList("QuickManageActivity", EES_QUICK, ESquery.quickUID(QUICK_MENU_LIMIT, myuid()),
                success = { list ->
                    b.progress.hideFadeOut(false)

                    if (!list.isNullOrEmpty()) {
                        b.tvInfo.showSlideUp(true)
                        oldItems.addAll(list)
                        newItems.addAll(list)
                        adapter.notifyDataSetChanged()
//                        setAdapter()
                    }

                }, fail = { b.progress.hideFadeOut(false) })
    }

    private fun setAdapter() {
        adapter = QuickManageAdapter(newItems,
                onDel = { countKey ->
                    /** oldItems 에서도 삭제 */
//                    oldMap.remove(countKey)
                    var i = 0
                    oldItems.forEachIndexed { index, count ->
                        if (count.key == countKey) i = index
                    }
                    oldItems.removeAt(i)
                },
                listener = object : QuickManageAdapter.OnStartDragListener {
                    override fun onStartDrag(holder: QuickManageAdapter.QuickManageViewHolder) {
                        itemTouchHelper.startDrag(holder)
                        lllogD("isDiff --- newItems 0  ${newItems[0].modelTitle}")
                        lllogD("isDiff --- adapter 0  ${adapter.items[0].modelTitle}")
                    }
                })

        itemTouchHelper = ItemTouchHelper(MyTouchHelper(adapter))
        itemTouchHelper.attachToRecyclerView(b.rv)

        b.rv.adapter = adapter
        adapter.notifyDataSetChanged()
    }

// private fun setAdapter() {
//        if (oldItems.isNullOrEmpty())
//            b.tvInfo.showSlideUp(true)
//        else {
//            newItems.addAll(oldItems!!)
//            adapter = QuickManageAdapter(newItems,
//                    onDel = { countKey ->
//                        /** oldItems 에서도 삭제 */
//                        var ii: Int? = null
//                        oldItems?.forEachIndexed { i, count ->
//                            if (count.key == countKey) {
//                                ii = i
//                                return@forEachIndexed
//                            }
//                        }
//                        ii?.let { oldItems?.removeAt(it) }
//                    },
//                    listener = object : QuickManageAdapter.OnStartDragListener {
//                        override fun onStartDrag(holder: QuickManageAdapter.QuickManageViewHolder) {
//                            itemTouchHelper.startDrag(holder)
//                        }
//                    })
//
//            itemTouchHelper = ItemTouchHelper(MyTouchHelper(adapter))
//            itemTouchHelper.attachToRecyclerView(b.rv)
//
//            b.rv.adapter = adapter
//            adapter.notifyDataSetChanged()
//        }
//    }

    override fun onBackPressed() {
        if (isDiff()) OnDDD.confirm(this@QuickManageActivity, R.string.info_quick_change, ok = { super.onBackPressed() }, cancel = {})
        else super.onBackPressed()
    }

    fun onClickSave(v: View) {
        OnMyClick.setDisableAWhileBTN(v)
        if (isDiff())
            MyAlert.showConfirm(this@QuickManageActivity, R.string.info_wanna_upload_quick.getStr(),
                    ok = {
                        WAIT_TYPE.FULL_CIRCLE.show(this@QuickManageActivity)
                        lllogI("QuickManageActivity  ok onClickSave")
                        Firee.modifyQuick(newItems, success = { finish() }, fail = {})
                    },
                    cancel = { lllogI("QuickManageActivity  cancel onClickSave") })
        else toastNormal(R.string.info_quick_nochange)
    }

    /**
     * 변경여부 체크 : true = 변경됨.
     */
    private fun isDiff(): Boolean {
        newItems.forEachIndexed { i, newC ->
            lllogD("isDiff   newC ${newC.modelTitle}")
            if (newC.key != oldItems[i].key) return true
        }

//        var i = 0
//        oldMap.forEach { (key, _) ->
//            if (key != newItems[i].key) return true
//            i++
//        }
        return false
    }


}