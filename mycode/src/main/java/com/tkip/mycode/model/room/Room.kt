package com.tkip.mycode.model.room

import android.os.Parcel
import android.os.Parcelable
import com.tkip.mycode.init.base.createKey
import com.tkip.mycode.model.my_enum.ContractType
import com.tkip.mycode.model.my_enum.RoomStatus
import com.tkip.mycode.model.my_enum.RoomType
import com.tkip.mycode.model.my_enum.Status
import com.tkip.mycode.model.peer.mynick
import com.tkip.mycode.model.peer.myuid
import com.tkip.mycode.util.lib.photo.Foto
import com.tkip.mycode.util.tools.date.rightNow
import java.util.*

/**
 * todo : 일단 room 올리고, 사진은 업데이트로. 중간에 인터넷이 끊길수도 있기 때문에... //  아니야.. 그냥 기존대로..하자.
 * 안내 : 동일한 게시물은 자동으로 삭제될 수 있음을 경고할 것. 왜냐면 유저들의 혼란가중 때문에. 크레딧 반환은 없음.
 */
data class Room(

        var key: String = createKey(),
        var authorUID: String = myuid(),
        var authorNick: String = mynick(),

        //////////////// 수정 가능한 부분 ////////////////////////////
        var contractTypeNo: Int = ContractType.RENT.no,
        var roomTypeNo: Int = RoomType.CONDO.no,
        var roomStatusNo: Int = RoomStatus.EMPTY.no,

        var constructionDate: Int? = null,       // 시공완료날짜
        var areaTotal: Int? = null,    // 전체면적(전용+공용)  sqm
        var areaEx: Int? = null,       // 전용면적  sqm

        var countRoom: Int? = null,
        var countToilet: Int? = null,
        var countKitchen: Int? = null,
        var countLiving: Int? = null,
        var countBath: Int? = null,
        var countFloor: Int? = null,  // 층수
        var countParking: Int? = null,  // 주차가능대수


        var minTerm: Int? = null,   // 최소 계약 기간 (개월)
        var price: Int? = null,   //  물건 금액 peso  (매매)
        var inAdvanced: Int? = null,    //  선불 납입(개월)
        var deposit: Int? = null,       //  보증금(개월)

        var local: String? = null,    // 대략위치   예) 건물명, 지역이름
        var lat: Double? = null,
        var lng: Double? = null,
        var address: String? = null,   // 주소

        var title: String? = null,    //       예) 보니파시오 콘도
        var comment: String? = null,  // 상세설명
        var contact: String? = null,  // 연락처
        var mediaMap: HashMap<String, String>? = null,  // 미디어 아이디

        var builtInList: ArrayList<String>? = null,  // 가구/가전 빌트인.  예) 냉장고, 침대, 세탁기, 에어컨
        var tagList: ArrayList<String>? = null,    // 최대 10개까지만, 검색 키워드광고와 매치될 부분이기때문에 남용 우려로 수량 제한.
        var fotoList: ArrayList<Foto>? = null,        // DB에 저장된 foto,


        ////////////////////// for Author /////////////////////////////////
        var allowReply: Boolean = true,  // 댓글 입력 가능 여부.

        var paidCredit: Int = 0,       // 결제한 credit (나중에 절반 돌려주는 기준이 되는 금액)
        var reloadCount: Int = 0,    // 재업로드 횟수.  (차후에 횟수에 따라 보상 예정.)  재업로드시 새로 올리는 것보다 credit이 쌈. 절반가격.

        var rank: Int = 0,       // 상위 랭크 여부 (예 20200102) 오늘날짜가 가장 크다.
        var adCredit: Int = 0,   // 현재 검색 순위에 적용중인 credit
        var totalAdCredit: Int = 0,  // 그동안 결제한 totalAdCredit
        var decoSearchNo: Int = 0,     // search 결과에 나오는 holder 종류. 차후에 만들 예정.
        var decoBookNo: Int = 0,   // 유저가 bookmark 했을때 나오는 holder 종류. 차후에 만들 예정.

        ////////////////////// Count /////////////////////////////////
        var bookmarkCount: Int = 0,   // 유저가 즐겨찾기에 올릴시 카운팅
        var viewCount: Int = 0,       // 뷰 횟수.
        var viewUidCount: Int = 0,    // 유저당 한번 뷰 횟수.
        var pushCount: Int = 0,       // push받는 유저수.

        //////////////////////////////////////////////////////////////
        var replyCount: Int = 0,      // 댓글 횟수.
        var warnCount: Int = 0,       // 신고 횟수.

        var totalPoint: Long = 0,       //  bookmarkCount + viewcount + replyCount + pushCount +(reward*100) - (warnCount*100)
        var statusNo: Int = Status.NORMAL.no,       // 최초 작성시 NORMAL
        var addDate: Long = rightNow(),       // 최초 업로드 시간.   재업로드시 재업로드 시간으로 변경.
        var updateDate: Long? = null

) : Parcelable {
    /**     * clone    */
    @Suppress("UNCHECKED_CAST")
    constructor(room: Room) : this(

            key = room.key,
            authorUID = room.authorUID,
            authorNick = room.authorNick,

            contractTypeNo = room.contractTypeNo,
            roomTypeNo = room.roomTypeNo,
            roomStatusNo = room.roomStatusNo,

            constructionDate = room.constructionDate,
            areaTotal = room.areaTotal,
            areaEx = room.areaEx,

            countRoom = room.countRoom,
            countToilet = room.countToilet,
            countKitchen = room.countKitchen,
            countLiving = room.countLiving,
            countBath = room.countBath,
            countFloor = room.countFloor,
            countParking = room.countParking,

            minTerm = room.minTerm,
            price = room.price,
            inAdvanced = room.inAdvanced,
            deposit = room.deposit,

            local = room.local,
            lat = room.lat,
            lng = room.lng,
            address = room.address,

            title = room.title,
            comment = room.comment,
            contact = room.contact,
            mediaMap = room.mediaMap?.clone() as? HashMap<String, String>,
            builtInList = room.builtInList,
            tagList = room.tagList,
            fotoList = room.fotoList?.clone() as? ArrayList<Foto>,

            allowReply = room.allowReply,
            //////////////////////////////////////////////////////////////////////

            paidCredit = room.paidCredit,
            reloadCount = room.reloadCount,

            rank = room.rank,
            adCredit = room.adCredit,
            totalAdCredit = room.totalAdCredit,
            decoSearchNo = room.decoSearchNo,
            decoBookNo = room.decoBookNo,

            bookmarkCount = room.bookmarkCount,
            viewCount = room.viewCount,
            viewUidCount = room.viewUidCount,
            pushCount = room.pushCount,

            replyCount = room.replyCount,
            warnCount = room.warnCount,

            totalPoint = room.totalPoint,
            statusNo = room.statusNo,
            addDate = room.addDate,
            updateDate = room.updateDate
    )

    constructor(source: Parcel) : this(
            source.readString()!!,
            source.readString()!!,
            source.readString()!!,
            source.readInt(),
            source.readInt(),
            source.readInt(),
            source.readValue(Int::class.java.classLoader) as Int?,
            source.readValue(Int::class.java.classLoader) as Int?,
            source.readValue(Int::class.java.classLoader) as Int?,
            source.readValue(Int::class.java.classLoader) as Int?,
            source.readValue(Int::class.java.classLoader) as Int?,
            source.readValue(Int::class.java.classLoader) as Int?,
            source.readValue(Int::class.java.classLoader) as Int?,
            source.readValue(Int::class.java.classLoader) as Int?,
            source.readValue(Int::class.java.classLoader) as Int?,
            source.readValue(Int::class.java.classLoader) as Int?,
            source.readValue(Int::class.java.classLoader) as Int?,
            source.readValue(Int::class.java.classLoader) as Int?,
            source.readValue(Int::class.java.classLoader) as Int?,
            source.readValue(Int::class.java.classLoader) as Int?,
            source.readString(),
            source.readValue(Double::class.java.classLoader) as Double?,
            source.readValue(Double::class.java.classLoader) as Double?,
            source.readString(),
            source.readString(),
            source.readString(),
            source.readString(),
            source.readSerializable() as HashMap<String, String>?,
            source.createStringArrayList(),
            source.createStringArrayList(),
            source.createTypedArrayList(Foto.CREATOR),
            1 == source.readInt(),
            source.readInt(),
            source.readInt(),
            source.readInt(),
            source.readInt(),
            source.readInt(),
            source.readInt(),
            source.readInt(),
            source.readInt(),
            source.readInt(),
            source.readInt(),
            source.readInt(),
            source.readInt(),
            source.readInt(),
            source.readLong(),
            source.readInt(),
            source.readLong(),
            source.readValue(Long::class.java.classLoader) as Long?
    )

    override fun describeContents() = 0

    override fun writeToParcel(dest: Parcel, flags: Int) = with(dest) {
        writeString(key)
        writeString(authorUID)
        writeString(authorNick)
        writeInt(contractTypeNo)
        writeInt(roomTypeNo)
        writeInt(roomStatusNo)
        writeValue(constructionDate)
        writeValue(areaTotal)
        writeValue(areaEx)
        writeValue(countRoom)
        writeValue(countToilet)
        writeValue(countKitchen)
        writeValue(countLiving)
        writeValue(countBath)
        writeValue(countFloor)
        writeValue(countParking)
        writeValue(minTerm)
        writeValue(price)
        writeValue(inAdvanced)
        writeValue(deposit)
        writeString(local)
        writeValue(lat)
        writeValue(lng)
        writeString(address)
        writeString(title)
        writeString(comment)
        writeString(contact)
        writeSerializable(mediaMap)
        writeStringList(builtInList)
        writeStringList(tagList)
        writeTypedList(fotoList)
        writeInt((if (allowReply) 1 else 0))
        writeInt(paidCredit)
        writeInt(reloadCount)
        writeInt(rank)
        writeInt(adCredit)
        writeInt(totalAdCredit)
        writeInt(decoSearchNo)
        writeInt(decoBookNo)
        writeInt(bookmarkCount)
        writeInt(viewCount)
        writeInt(viewUidCount)
        writeInt(pushCount)
        writeInt(replyCount)
        writeInt(warnCount)
        writeLong(totalPoint)
        writeInt(statusNo)
        writeLong(addDate)
        writeValue(updateDate)
    }

    companion object {
        @JvmField
        val CREATOR: Parcelable.Creator<Room> = object : Parcelable.Creator<Room> {
            override fun createFromParcel(source: Parcel): Room = Room(source)
            override fun newArray(size: Int): Array<Room?> = arrayOfNulls(size)
        }
    }
}