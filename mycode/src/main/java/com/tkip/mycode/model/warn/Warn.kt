package com.tkip.mycode.model.warn

import android.os.Parcel
import android.os.Parcelable
import com.tkip.mycode.init.base.createKey
import com.tkip.mycode.model.my_enum.DataType
import com.tkip.mycode.model.my_enum.Status
import com.tkip.mycode.model.peer.mynick
import com.tkip.mycode.model.peer.myuid
import com.tkip.mycode.util.tools.date.rightNow

/**
 * peer의 penalty 데이타 변경 : 버튼을 따로 만들어서 처리할 것.
 * warn - warn.key
 * warn처리 - warn.key
 * penalty처리 - warn.key
 */
data class Warn(

        var key: String = createKey(),
        var authorUID: String = myuid(),  // 신고자
        var authorNick: String = mynick(),
        var adminUID: String? = null,   // 처리한 어드민.
        var adminNick: String? = null,

        /**         * 유저         */
        var targetUID: String? = null,  // 모델의 작성자
        var targetNick: String? = null,

        /**         * 포스트나 내용 신고할 때         */
        var dataTypeNo: Int = DataType.POST.no,   // 신고당한 모델 dataTypeNo.
        var modelKey: String = "modelKey",        // 신고당한 모델 Key         ,해당되는 포스트의 키. ex) post, reply, store, board, storeRate,
        var modelTitle: String? = null,           // 신고당한 모델 Title.
        var modelStatus: Int = Status.NORMAL.no,  // 신고당한 모델 Status.

        /**         * 2중Child(댓글) 신고할 때         */
        var inModel: Boolean = false,             // 2중Child일때.
        var inDataTypeNo: Int? = null,            // 2중Child일때 : 그 모델의 dataTypeNo.
        var inModelKey: String? = null,           // 2중Child일때 : 그 모델의 Key.

        /////////////// for 신고자가 기입한 신고 사항 /////////////////////////////////////////////////
        var warnTypeNo: Int = WarnType.BAD_WORD.no,   // 신고유형 예) 욕설, 비방, 음란, 도박, 불법, 규정위반.
        var comment: String? = null,  // 페널티의 이유.

        /////////////// for admin : 해당 모델 작성자에게 패널티 부여할 때. /////////////////////////////
        var amount: Int? = null,      // 패널티 점수.
        var period: Int? = null,          // 정지 기간 Day.
        var lastDate: Long? = null,       // 글올리기 금지해제되는 날짜.
        var reason: String? = null,   // 패널티 이유.

        ////////////////////////////////////////////////////////////////////////////////////////////
        var statusNo: Int = Status.WARN_REQUESTED.no,
        var addDate: Long = rightNow()

) : Parcelable {
    fun deletePenalty() {
        this.amount = null
        this.period = null
        this.lastDate = null
        this.reason = null
    }

    /**         * 포스트나 내용 신고할 때         */
    constructor(comment: String?, warnType: WarnType) : this(
            comment = comment,
            warnTypeNo = warnType.no
    )

    constructor(source: Parcel) : this(
            source.readString()!!,
            source.readString()!!,
            source.readString()!!,
            source.readString(),
            source.readString(),
            source.readString(),
            source.readString(),
            source.readInt(),
            source.readString()!!,
            source.readString(),
            source.readInt(),
            1 == source.readInt(),
            source.readValue(Int::class.java.classLoader) as Int?,
            source.readString(),
            source.readInt(),
            source.readString(),
            source.readValue(Int::class.java.classLoader) as Int?,
            source.readValue(Int::class.java.classLoader) as Int?,
            source.readValue(Long::class.java.classLoader) as Long?,
            source.readString(),
            source.readInt(),
            source.readLong()
    )

    override fun describeContents() = 0

    override fun writeToParcel(dest: Parcel, flags: Int) = with(dest) {
        writeString(key)
        writeString(authorUID)
        writeString(authorNick)
        writeString(adminUID)
        writeString(adminNick)
        writeString(targetUID)
        writeString(targetNick)
        writeInt(dataTypeNo)
        writeString(modelKey)
        writeString(modelTitle)
        writeInt(modelStatus)
        writeInt((if (inModel) 1 else 0))
        writeValue(inDataTypeNo)
        writeString(inModelKey)
        writeInt(warnTypeNo)
        writeString(comment)
        writeValue(amount)
        writeValue(period)
        writeValue(lastDate)
        writeString(reason)
        writeInt(statusNo)
        writeLong(addDate)
    }

    companion object {
        @JvmField
        val CREATOR: Parcelable.Creator<Warn> = object : Parcelable.Creator<Warn> {
            override fun createFromParcel(source: Parcel): Warn = Warn(source)
            override fun newArray(size: Int): Array<Warn?> = arrayOfNulls(size)
        }
    }
}
