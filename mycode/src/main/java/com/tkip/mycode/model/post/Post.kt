package com.tkip.mycode.model.post

import android.os.Parcel
import android.os.Parcelable
import com.tkip.mycode.init.base.createKey
import com.tkip.mycode.model.board.Board
import com.tkip.mycode.model.board.BoardType
import com.tkip.mycode.model.my_enum.PostType
import com.tkip.mycode.model.my_enum.Status
import com.tkip.mycode.model.peer.mynick
import com.tkip.mycode.model.peer.myuid
import com.tkip.mycode.util.lib.photo.Foto
import com.tkip.mycode.util.tools.date.rightNow

/**
 *  reward 를 걸면 색상 바꿀 것.
 *
 *              ArrayList<Long>().apply { source.readList(this as List<Long>, Long::class.java.classLoader) }
 *              todo : 사진 삭제시 광고중일때는 사진 표시 안 됨을 알려.
 */
data class Post(

        var key: String = createKey(),
        var authorUID: String = myuid(),
        var authorNick: String = mynick(),
        var anonymous: Boolean = false,       // 익명 업로드- 포인트 필요.

        var postType: Int = PostType.NORMAL.no,    // 나중에 쓸지도 몰라서.
        var boardKey: String = "BoardKey",
        var boardName: String = "BoardName",
        var boardTypeNo: Int = BoardType.CS_CENTER.no,     // 보드 타입.
        var boardMasterUid: String = "boardMasterUid",     // 보드마스터 uid.
//        var boardMasterKey: String = "boardMasterKey",   // 보드마스터 uid.

        //////////////// 수정 가능한 부분 ////////////////////////////
        var title: String? = null,
        var comment: String? = null,
        var fotoList: ArrayList<Foto>? = null,        // DB에 저장된 foto,
        var tagList: ArrayList<String>? = null,    // 최대 10개까지만, 검색 키워드광고와 매치될 부분이기때문에 남용 우려로 수량 제한.

        //////////////////////////////////////////////////////////////
        var chosen: Boolean = false,  // 답변 채택했는지 여부.
        var reward: Int? = null,      // 포인트 걸기.
        var allowReply: Boolean = true,  // 댓글 입력 가능 여부.
        var allowSearch: Boolean = true, // 포스트 검색 허용.
//        var allowReplyAdmin: Boolean = true,  // 댓글 입력 가능 여부. 어드민. 가장 High

        //////////////// for admin ////////////////////////////
        var adminAllowReply: Boolean = true,  // 댓글 입력 스위치의 스위치    todo: addpost 에서 처리할 것.

        ////////////////////// Count /////////////////////////////////
        var thumbUpCount: Int = 0,    // 썸업을 준 횟수.
        var thumbDownCount: Int = 0,  // 썸다운을 횟수.
        var bookmarkCount: Int = 0,   // 유저가 즐겨찾기에 올릴시 카운팅.
        var viewCount: Int = 0,       // 뷰 횟수.
        var viewUidCount: Int = 0,    // 유저당 한번 뷰 횟수.
        var pushCount: Int = 0,       // push받는 유저수.

        var replyCount: Int = 0,      // 댓글 횟수.
        var warnCount: Int = 0,       // 신고 횟수.

        var totalPoint: Long = 0,   // 차후에 peer.type의 no로 적용하자. author의 레벨 + thumbUp - thumbDown + bookmark + view + reply
        var statusNo: Int = Status.NORMAL.no,       // 최초 작성시 NORMAL
        var addDate: Long = rightNow(),       // 최초 업로드 시간.
        var updateDate: Long? = null

) : Parcelable {
    /**     * 빈 post 생성     */
    constructor(postType: Int?, board: Board) : this(
            postType = postType ?: PostType.NORMAL.no,
            boardKey = board.key,
            boardName = board.title,
            boardTypeNo = board.boardTypeNo,
            boardMasterUid = board.authorUID
    )

    /**     * clone    */
    @Suppress("UNCHECKED_CAST")
    constructor(post: Post) : this(
            key = post.key,
            authorUID = post.authorUID,
            authorNick = post.authorNick,
            anonymous = post.anonymous,

            postType = post.postType,
            boardKey = post.boardKey,
            boardName = post.boardName,
            boardTypeNo = post.boardTypeNo,
            boardMasterUid = post.boardMasterUid,

            title = post.title,
            comment = post.comment,
            fotoList = post.fotoList?.clone() as? ArrayList<Foto>,
            tagList = post.tagList,

            chosen = post.chosen,
            reward = post.reward,
            allowReply = post.allowReply,
            allowSearch = post.allowSearch,

            adminAllowReply = post.adminAllowReply,

            thumbUpCount = post.thumbUpCount,
            thumbDownCount = post.thumbDownCount,
            bookmarkCount = post.bookmarkCount,
            viewCount = post.viewCount,
            viewUidCount = post.viewUidCount,
            pushCount = post.pushCount,

            replyCount = post.replyCount,
            warnCount = post.warnCount,

            totalPoint = post.totalPoint,
            statusNo = post.statusNo,
            addDate = post.addDate,
            updateDate = post.updateDate
    )

    constructor(source: Parcel) : this(
            source.readString()!!,
            source.readString()!!,
            source.readString()!!,
            1 == source.readInt(),
            source.readInt(),
            source.readString()!!,
            source.readString()!!,
            source.readInt(),
            source.readString()!!,
            source.readString(),
            source.readString(),
            source.createTypedArrayList(Foto.CREATOR),
            source.createStringArrayList(),
            1 == source.readInt(),
            source.readValue(Int::class.java.classLoader) as Int?,
            1 == source.readInt(),
            1 == source.readInt(),
            1 == source.readInt(),
            source.readInt(),
            source.readInt(),
            source.readInt(),
            source.readInt(),
            source.readInt(),
            source.readInt(),
            source.readInt(),
            source.readInt(),
            source.readLong(),
            source.readInt(),
            source.readLong(),
            source.readValue(Long::class.java.classLoader) as Long?
    )

    override fun describeContents() = 0

    override fun writeToParcel(dest: Parcel, flags: Int) = with(dest) {
        writeString(key)
        writeString(authorUID)
        writeString(authorNick)
        writeInt((if (anonymous) 1 else 0))
        writeInt(postType)
        writeString(boardKey)
        writeString(boardName)
        writeInt(boardTypeNo)
        writeString(boardMasterUid)
        writeString(title)
        writeString(comment)
        writeTypedList(fotoList)
        writeStringList(tagList)
        writeInt((if (chosen) 1 else 0))
        writeValue(reward)
        writeInt((if (allowReply) 1 else 0))
        writeInt((if (allowSearch) 1 else 0))
        writeInt((if (adminAllowReply) 1 else 0))
        writeInt(thumbUpCount)
        writeInt(thumbDownCount)
        writeInt(bookmarkCount)
        writeInt(viewCount)
        writeInt(viewUidCount)
        writeInt(pushCount)
        writeInt(replyCount)
        writeInt(warnCount)
        writeLong(totalPoint)
        writeInt(statusNo)
        writeLong(addDate)
        writeValue(updateDate)
    }

    companion object {
        @JvmField
        val CREATOR: Parcelable.Creator<Post> = object : Parcelable.Creator<Post> {
            override fun createFromParcel(source: Parcel): Post = Post(source)
            override fun newArray(size: Int): Array<Post?> = arrayOfNulls(size)
        }
    }
}