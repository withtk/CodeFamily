package com.tkip.mycode.model.peer

import android.content.Context
import com.tkip.mycode.R
import com.tkip.mycode.auth.OnSign
import com.tkip.mycode.dialog.alert.checkAndAlertInfo
import com.tkip.mycode.dialog.toast.checkAndToast
import com.tkip.mycode.funs.common.remoteStr
import com.tkip.mycode.init.base.getStr
import com.tkip.mycode.init.base.gte
import com.tkip.mycode.init.base.lt
import com.tkip.mycode.model.board.Board
import com.tkip.mycode.model.board.BoardType
import com.tkip.mycode.model.my_enum.PeerType
import com.tkip.mycode.model.peer.peer_allow.PeerAllow
import com.tkip.mycode.model.post.Post
import com.tkip.mycode.model.warn.OnPenalty
import com.tkip.mycode.util.tools.date.rightNow


fun String.isMe() = (myuid() == this)
fun mypeer() = OnPeer.peer
fun mynick() = OnPeer.peer.nick
fun myuid() = mypeer().uid
fun myloginFrom() = mypeer().loginFrom
fun mytype() = mypeer().type
fun mycredit() = mypeer().crd
fun mybonus() = mypeer().bcrd
fun myPnt() = mypeer().pnt
fun mypenaltyDate() = mypeer().penLastDate
fun mypenaltyReason() = mypeer().penReason
fun myphilpay() = mypeer().cpay
fun mypoint() = mypeer().pnt
fun mycarrot() = mypeer().crt


fun isSupervisor() = (R.string.remote_super_uid.remoteStr() == myuid()) && mytype().gte(PeerType.ADMIN_SUPERVISOR.no)
fun isAdmin() = OnPeer.isAdmin(false)
fun isNotAdmin(toastType: Int?,strId: Int?) = (!mytype().gte(PeerType.ADMIN_1.no)).checkAndToast(toastType,strId?:PeerType.ADMIN_1.msgId)


fun String.isAuthorAndAdmin(toastType: Int?,strId: Int?) = (isAdmin() || this == myuid()).checkAndToast(toastType,strId)
fun String.isAuthor(toastType: Int?,strId: Int?) = (this == myuid()).checkAndToast(toastType,strId)
fun String.isNotAuthor(toastType: Int?,strId: Int?) = (this != myuid()).checkAndToast(toastType,strId?:R.string.alert_not_author)

fun String.isMaster() = (this == myuid())
fun Int.isCsBoard() = this == BoardType.CS_CENTER.no
fun Post.isCsBoard() = this.boardTypeNo.isCsBoard()
fun Board.isCsBoard() = this.boardTypeNo.isCsBoard()


/************ db 작업 가능 여부 체크 *****************************************************************/
/** * true : 권한 없음. */
fun checkAllAvailUpload(context: Context?) = (isAnonymous(context) || isNotValidDate(context) || isPenalty(context))

fun isPenaltyNoDialog() = (rightNow() < mypeer().penLastDate)
fun isPenalty(context: Context?) = isPenaltyNoDialog().checkAndAlertInfo(context, OnPenalty.dateMessage(mypenaltyDate()))
fun isAnonymous() = (mypeer().type.lt(PeerType.BASIC_2.no))
fun isAnonymous(context: Context?) = isAnonymous().checkAndAlertInfo(context, R.string.alert_not_avail_anonymous)

/** * 탈퇴 가입제한 기간 체크 */
fun isNotValidDateNoDialog() = (mypeer().delDate != null && mypeer().delDate!! > rightNow())
fun isNotValidDate(context: Context?) = isNotValidDateNoDialog().checkAndAlertInfo(context, String.format(R.string.info_still_period_no_signup.getStr(), OnSign.postponeMonth))
fun Context.notAvailAllowPost(board: Board) = (!board.allowPost).checkAndAlertInfo(this, R.string.no_permit_writing)
fun Context.notAvailBoardAllowPost(miniPeer: PeerAllow) = (!miniPeer.allowPost).checkAndAlertInfo(this, R.string.no_permit_writing)
fun Context.notAvailBoardAllowReply(miniPeer: PeerAllow) = (!miniPeer.allowReply).checkAndAlertInfo(this, R.string.no_permit_writing)




