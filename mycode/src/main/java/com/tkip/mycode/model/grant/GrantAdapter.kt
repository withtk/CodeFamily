package com.tkip.mycode.model.grant

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.RecyclerView
import com.tkip.mycode.R
import com.tkip.mycode.ad.OnBanner
import com.tkip.mycode.databinding.HolderGrantVertBinding
import com.tkip.mycode.dialog.OnDDD
import com.tkip.mycode.dialog.alert.MyAlert
import com.tkip.mycode.funs.common.lllogD
import com.tkip.mycode.funs.common.tranPair
import com.tkip.mycode.init.ACTION_ADMIN
import com.tkip.mycode.init.inter.interHolderBasic
import com.tkip.mycode.model.my_enum.*
import com.tkip.mycode.model.peer.isAdmin
import com.tkip.mycode.util.lib.retrofit2.ESget
import com.tkip.mycode.util.tools.etc.OnMyClick
import com.tkip.mycode.util.tools.fire.Firee

/**
 * Grant list
 */
class GrantAdapter(private val items: ArrayList<Grant>, val rvType: Int, val onItemClick: ((Grant) -> Unit)?) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    private lateinit var context: Context

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        this.context = parent.context
        return GrantVertHolder(HolderGrantVertBinding.inflate(LayoutInflater.from(parent.context), parent, false))
//        return when (rvType) {
//            RV_VERT -> BoardVertViewHolder(HolderBoardVertBinding.inflate(LayoutInflater.from(parent.context), parent, false))
//            else -> BoardVertViewHolder(HolderBoardVertBinding.inflate(LayoutInflater.from(parent.context), parent, false))
    }


    override fun onBindViewHolder(h: RecyclerView.ViewHolder, p: Int) {
        (h as GrantVertHolder).bind(items[p])
//        when (rvType) {
//            RV_VERT -> (h as BoardVertViewHolder).bind(items[p])
//            else -> (h as BoardVertViewHolder).bind(items[p])
//        }
    }


    override fun getItemCount(): Int {
        return items.size
    }


    /******************************************************************************************
     * vert
     *******************************************************************************************/
    inner class GrantVertHolder(val b: HolderGrantVertBinding) : RecyclerView.ViewHolder(b.root) {

        private lateinit var grant: Grant

        fun bind(grant: Grant) {
            lllogD("GrantVertHolder bind grant $grant")
            this.grant = grant
            b.grant = grant
            b.inter = interHolderBasic(
                    onClickCard = {
                        /*** 오픈 Status  */
                        openStatus(it)
                    },
                    onClickGo = {
                        /*** 모델로 이동.  */
                        gotoModel(it)
                    },
                    onClickUpdate = {
                        /*** 오픈 grant  */
                        openGrant(it)
                    },
                    onClickDel = null,
                    onDeepDel = {
                        OnMyClick.setDisableAWhileBTN(it)
                        if (isAdmin()) {
                            OnDDD.deepDel(context, ok = {
                                Firee.deepDelGrant(grant.key, success = {
                                    items.removeAt(adapterPosition)
                                    notifyItemRemoved(adapterPosition)
                                }, fail = {})
                            }, cancel = {})
                        }
                    },
                    onClickStatus = null)
            b.executePendingBindings()
        }

        /**오픈 Status*/
        private fun openStatus(v: View) {
            OnMyClick.setDisableAWhileBTN(v)
            when (val dataType = grant.dataTypeNo.dataType()) {
                DataType.BOARD, DataType.POST, DataType.FLEA, DataType.ROOM, DataType.STORE ->
                    dataType.get(grant.modelKey,
                            success = {
                                it.showStatusDialog(context,
                                        onSuccess = { status, g ->
                                            when (status) {
                                                Status.NORMAL -> OnDDD.deepDel(context, ok = {
                                                    Firee.deepDelGrant(grant.key, success = {
                                                        items.removeAt(adapterPosition)
                                                        notifyItemRemoved(adapterPosition)
                                                    }, fail = {})
                                                }, cancel = {})   // 정상은 grant 삭제할 것.
                                                else -> g?.let { grant1 ->
                                                    items[adapterPosition] = grant1
                                                    notifyItemChanged(adapterPosition)
                                                }
                                            }
                                        })
                            }, fail = {})
                DataType.BANNER -> ESget.getBanner(grant.modelKey, success = { banner -> OnBanner.openADDBannerDialog(context as AppCompatActivity, ACTION_ADMIN, banner, onOk = { _, g -> notifyItemChanged(adapterPosition, g) }) }, fail = {})
                else -> {
                }
            }
        }

        /**모델로 이동.*/
        private fun gotoModel(v: View) {
            OnMyClick.setDisableAWhileBTN(v)
            lllogD("GrantVertHolder onClickCard grant $grant")
            when (grant.dataTypeNo.dataType()) {
                DataType.BANNER -> OnBanner.gotoMyBannerActivity(context, grant.authorUID, null)   // 유저의 banner List open
                else -> grant.goto(context, arrayOf(b.cons.tranPair(R.string.tran_linear)))   // 모델로 이동
            }
        }

        /**오픈 grant*/
        private fun openGrant(v: View) {
            OnMyClick.setDisableAWhileBTN(v)
            MyAlert.showGrant(context, grant, onSuccess = { updateAdapterList(it) }, onFail = {}) // 승인 handle.
        }


        private fun updateAdapterList(it: Grant) {
            items[adapterPosition] = it
            notifyItemChanged(adapterPosition)
        }

    }

}