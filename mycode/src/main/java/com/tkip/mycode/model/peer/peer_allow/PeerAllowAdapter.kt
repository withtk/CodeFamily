package com.tkip.mycode.model.peer.peer_allow

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.tkip.mycode.R
import com.tkip.mycode.databinding.HolderPeerAllowBinding
import com.tkip.mycode.dialog.OnDDD
import com.tkip.mycode.init.RV_VERT
import com.tkip.mycode.init.base.getStr
import com.tkip.mycode.util.tools.etc.OnMyClick
import com.tkip.mycode.util.tools.fire.Firee

class PeerAllowAdapter(val items: ArrayList<PeerAllow>, val rvType: Int) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {
    private lateinit var context: Context

//    companion object {
//        const val rvVert = 100
//    }


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        this.context = parent.context
        return when (rvType) {
            RV_VERT -> PeerAllowViewHolder(HolderPeerAllowBinding.inflate(LayoutInflater.from(parent.context), parent, false))
            else -> PeerAllowViewHolder(HolderPeerAllowBinding.inflate(LayoutInflater.from(parent.context), parent, false))

        }
    }

    override fun onBindViewHolder(h: RecyclerView.ViewHolder, p: Int) {
        when (rvType) {
            RV_VERT -> (h as PeerAllowViewHolder).bind(items[p])
            else -> (h as PeerAllowViewHolder).bind(items[p])
        }
    }

    override fun getItemCount(): Int {
        return items.size
    }

    /******************************************************************************************
     *  기본 rvVert
     *****************************************************************************************/
    inner class PeerAllowViewHolder(val b: HolderPeerAllowBinding) : RecyclerView.ViewHolder(b.root) {

        private lateinit var peerAllow: PeerAllow

        fun bind(peerAllow: PeerAllow) {
            this.peerAllow = peerAllow
            b.holder = this
            b.peerAllow = peerAllow

            b.executePendingBindings()
        }


        fun onClickCbPost(v: View) {
            OnMyClick.setDisableAWhileBTN(v)
            peerAllow.allowPost = b.cbAllowPost.isChecked
            Firee.addBoardAllow(peerAllow,{},{})
        }
        fun onClickCbReply(v: View) {
            OnMyClick.setDisableAWhileBTN(v)
            peerAllow.allowReply = b.cbAllowReply.isChecked
            Firee.addBoardAllow(peerAllow,{},{})
        }

        fun onClickDelete(v: View) {
            OnMyClick.setDisableAWhileBTN(v)
            OnDDD.delete(context, R.string.alert_delete_in_list.getStr(),
                    ok = {
                        Firee.deleteBoardAllow(peerAllow, success = { notifyItemRemoved(adapterPosition) }, fail = {})
                    }, cancel = {})

        }


    }


}