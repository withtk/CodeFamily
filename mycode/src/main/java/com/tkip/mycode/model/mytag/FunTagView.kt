package com.tkip.mycode.model.mytag

import android.widget.TextView
import com.cunoraz.tagview.Tag
import com.cunoraz.tagview.TagView
import com.tkip.mycode.R
import com.tkip.mycode.dialog.toast.TToast
import com.tkip.mycode.funs.common.lllogI
import com.tkip.mycode.init.MSG_LIMIT_TAG
import com.tkip.mycode.init.base.getClr
import com.tkip.mycode.model.my_enum.Status
import com.tkip.mycode.util.lib.retrofit2.ESget
import com.tkip.mycode.util.lib.retrofit2.ESquery
import com.tkip.mycode.util.tools.anim.AnimUtil
import com.tkip.mycode.util.tools.anim.gone
import com.wang.avi.AVLoadingIndicatorView
import java.util.*


///** * 올린 것 중에서 안 쓰는 거는 삭제. */
//fun ArrayList<MyTag>.arrange(tagView:TagView){
//}
const val TAG_1 = 10
const val TAG_2 = 20
const val TAG_3 = 30
const val TAG_4 = 40


/**         *  중복체크 tagview         */
fun TagView.hasString(str: String): Boolean {
    for (tag in tags) {
        if (tag.text == str) return true
    }
    return false
}

fun TagView.addTag1(str: String) {
    addTag(Tag(str).apply {
        tagTextColor = R.color.text_light.getClr()
        layoutBorderColor = R.color.md_pink_500.getClr()
        layoutColor = R.color.skyblue_800_alpha.getClr()
        layoutColorPress = R.color.blue_900.getClr()
        deleteIndicatorColor = R.color.md_cyan_300.getClr()
    })
}

fun TagView.addTag2(str: String) {
    addTag(Tag(str).apply {
        tagTextColor = R.color.text_light.getClr()
//        layoutBorderColor = R.color.md_pink_500.getClr()
        layoutColor = R.color.colorPrimaryDark.getClr()
        layoutColorPress = R.color.colorAccent.getClr()
        deleteIndicatorColor = R.color.md_cyan_300.getClr()
//        tagTextSize = 20f
    })
}

fun TagView.addTag3(str: String) {
    addTag(Tag(str).apply {
        tagTextColor = R.color.grey_40.getClr()
//        layoutBorderColor = R.color.md_pink_500.getClr()
        layoutColor = R.color.text_light.getClr()
        layoutColorPress = R.color.text_dark.getClr()
        deleteIndicatorColor = R.color.md_cyan_300.getClr()
    })
}

fun TagView.addTag4(str: String) {
    addTag(Tag(str).apply {
        tagTextColor = R.color.text_light.getClr()
        layoutColor = android.R.color.transparent
        layoutColorPress = R.color.text_dark.getClr()
        deleteIndicatorColor = R.color.md_cyan_300.getClr()
    })
}


/**         *  존재여부 체크 후  tag 올리기         */
fun TagView.checkAdd(type: Int, str: String) {
    if (hasString(str)) return
    when (type) {
        TAG_1 -> addTag1(str)
        TAG_2 -> addTag2(str)
        TAG_3 -> addTag3(str)
        TAG_4 -> addTag4(str)
    }
}

/** * 전체 붙여넣기 */
fun TagView.setAll(type: Int, list: ArrayList<String>?) {
    list?.let {
        it.forEach { str ->
            when (type) {
                TAG_1 -> this.addTag1(str)
                TAG_2 -> this.addTag2(str)
                TAG_3 -> this.addTag3(str)
                TAG_4 -> this.addTag4(str)
            }
        }
    }
}


/**         *  10개 이하로 제한         */
fun TagView.isLimit10(isToast: Boolean): Boolean {
    if (tags.size >= 10 && isToast) {
        TToast.showNormal(MSG_LIMIT_TAG)
        return true
    }
    return false
}

/** * 단순루프. 있으면 true */
fun TagView.has(keyword: String): Boolean {
    this.tags.forEach {
        if (it.text == keyword) return true
    }
    return false
}

/**
 * 금지어 체크
 */
fun TagView.checkBanWord(hasBan: (Boolean) -> Unit) {

    ESget.cntMyTagList(ESquery.mytagCheckBan(10, this.getKeywords(), Status.DELETED_USER),
            success = { value ->
                lllogI("checkBanWord getKeywords ${this.getKeywords()}    value $value")

                if (value != null && value > 0) hasBan(true)
                else hasBan(false)

//                    if (ing) {
//                        if (!result.isNullOrEmpty()) {
//                            hasBan(true)
//                            ing = false
//                        } else if (i == tags.size - 1) hasBan(false)   // 마지막 체크가 null일때는 return 해 줄 것.
//                    }
            }, fail = { })


////    var ing = true
//    this.tags.forEachIndexed { i, tag ->
//        lllogI("checkBanWord tag ${tag.text}")
//        ESget.isMyTagList(ESquery.mytagCheckBan(10, this.getKeywords(), Status.DELETED),
//                success = { value ->
//                    lllogI("checkBanWord tag ${tag.text}    value $value")
//
//                    if (value != null && value > 0) hasBan(true)
//                    else hasBan(false)
//
////                    if (ing) {
////                        if (!result.isNullOrEmpty()) {
////                            hasBan(true)
////                            ing = false
////                        } else if (i == tags.size - 1) hasBan(false)   // 마지막 체크가 null일때는 return 해 줄 것.
////                    }
//                }, fail = { })
//    }
}

fun TagView.getKeywords() = StringBuffer().apply { this@getKeywords.tags.forEach { this.append("${it.text} ") } }.toString()


/**
 * 검색
 */
private fun getTagList(tagType: TagTYPE, keyword: String, avLoading: AVLoadingIndicatorView, noData: TextView, success: (ArrayList<MyTag>?) -> Unit, fail: () -> Unit) {
    AnimUtil.showFadeIn(avLoading, true)
    noData.gone()
    OnTag.getEsTagList(15, 0, tagType, keyword,
            success = {
                AnimUtil.hideFadeOut(avLoading, true)
                lllogI("getEsTagList size ${it?.size}")
                success(it)
//                if(it==null || it.isEmpty()) noData.visi()
            },
            fail = {
                fail()
                AnimUtil.hideFadeOut(avLoading, true)
            })
}

/**
 * tag 셋팅 -> tagView
 */
fun MyTagAdapter.setTagList(tagType: TagTYPE, keyword: String, items: ArrayList<MyTag>, tagView: TagView, avLoading: AVLoadingIndicatorView, noData: TextView) {
    getTagList(tagType, keyword, avLoading, noData,
            success = {
                items.clear()
//                if (it == null || it.isEmpty()) AnimUtil.showSlideDown(noData, true)
//                else it.forEach { tag -> if (!tagView.hasString(tag.name)) items.add(tag) }
                it?.forEach { tag -> if (!tagView.hasString(tag.name)) items.add(tag) }
                notifyDataSetChanged()
            }, fail = {})

}

/**
 *  tagView에 없는 것만 리스트에 추가.
 */
fun ArrayList<MyTag>?.addCheckTagView(items: ArrayList<MyTag>, tagView: TagView) {
    items.clear()
    this?.let {
        it.forEach { tag -> if (!tagView.hasString(tag.name)) items.add(tag) }
    }
}

///**
// * tag 리턴 1개
// */
//fun MyTagAdapter.setTagListOne(tagType: TagTYPE, keyword: String, items: ArrayList<MyTag>, avLoading: AVLoadingIndicatorView, noData: TextView, tvSelected: TextView) {
//    getTagList(tagType, keyword, avLoading, noData,
//            success = {
//                it?.let {
//                    items.clear()
//                    it.forEach { tag -> if (tvSelected.getNullStr() != tag.name) items.add(tag) }
//                    notifyDataSetChanged()
//                } ?: let {
//                    AnimUtil.showSlideDown(noData, true)
//                }
//            }, fail = {})
//
//}

/**
 *  사용자가 tagView에서 삭제하면 uploadMyTag에서도 삭제.
 */
fun ArrayList<MyTag>.removeOne(str: String) {
    this.forEachIndexed { i, myTag ->
        if (myTag.name == str) {
            this.removeAt(i)
            return
        }
    }
}


/**
 * arraylist로 변환
 */
fun TagView.convertStringList(): ArrayList<String>? {
    return if (tags.isNotEmpty()) {
        arrayListOf<String>().apply {
            tags.forEach { add(it.text) }
        }
    } else null
}




