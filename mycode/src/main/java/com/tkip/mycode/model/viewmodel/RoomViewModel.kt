package com.tkip.mycode.model.viewmodel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.tkip.mycode.init.EES_ROOM
import com.tkip.mycode.model.count.Count
import com.tkip.mycode.model.my_enum.Cntch
import com.tkip.mycode.model.my_enum.Status
import com.tkip.mycode.model.room.Room
import com.tkip.mycode.util.lib.retrofit2.ESget
import com.tkip.mycode.util.lib.retrofit2.ESquery
import com.tkip.mycode.util.tools.fire.Firee

class RoomViewModel : ViewModel() {

    val liveRoom = MutableLiveData<Room>()
    val liveRoomList = ListLiveData<Room>()

    val liveBookmark = MutableLiveData<Count>()
    val livePush = MutableLiveData<Count>()
    val liveWarn = MutableLiveData<Count>()


//    fun getRoom(roomKey: String): LiveData<Room> {
//        Firee.getRoom(roomKey, success = { it?.let { room1 -> liveRoom.value = room1 } }, fail = {})
//        return liveRoom
//    }


    fun getEsRoomList(size: Int, from: Int,  status: Status): ListLiveData<Room> {
        val query = ESquery.roomList(size, from, status)
        ESget.getRoomList(query,
                success = {
                    if (it == null  ) {
                        liveRoomList.clear(true)
                    } else {
                        if (from == 0) liveRoomList.clear(false)
                        liveRoomList.addAll(it)
                    }
                }, fail = {})
        return liveRoomList
    }


    /********************************************************************************************
     ********************************* Count ***************************************************/
//    fun getAllEsCount(room: Room) {
//        getEsCountList(room, Cntch.BOOK)
//        getEsCountList(room, Cntch.PUSH)
//    }
//
//    private fun getEsCountList(room: Room, cnt: Cntch): LiveData<Count> {
//        ESget.getCountList(cnt.getTopChild(DataType.ROOM), ESquery.countUidModelKey(room.key), success = {
//            if (it == null || it.isEmpty()) {
//                setLiveCount(cnt, null)
//            } else {
//                setLiveCount(cnt, it[0])
//            }
//        }, fail = {})
//        return returnLiveCount(cnt)
//    }
//
//    fun getCount(room: Room, cnt: Cntch): LiveData<Count> {
//        Firee.getCount(cnt.getTopChild(DataType.ROOM), room.key, success = { setLiveCount(cnt, it) }, fail = {})
//        return returnLiveCount(cnt)
//    }

//    private fun returnLiveCount(cnt: Cntch) = when (cnt) {
//        Cntch.BOOK -> liveBookmark
//        Cntch.PUSH -> livePush
//        else -> liveBookmark
//    }
//
//    private fun setLiveCount(cnt: Cntch, count: Count?) {
//        when (cnt) {
//            Cntch.BOOK -> liveBookmark.value = count
//            Cntch.PUSH -> livePush.value = count
//            else -> liveBookmark.value = count
//        }
//    }

//    private fun Cntch.count() =
//            when (this) {
//                Cntch.BOOK -> liveBookmark.value
//                Cntch.PUSH -> livePush.value
//                else -> null
//            }

//    fun updateCount(room: Room, cnt: Cntch, v: View) {
//        cnt.updateCount(room, cnt.count(), v, success = { on, count1 ->
//            if (on) setLiveCount(cnt, count1)
//            else setLiveCount(cnt, null)
//        }, fail = {})
//    }

    fun addViewUID(room: Room, cnt: Cntch) {
        Firee.addViewUID(EES_ROOM, Count.create(room, cnt), {}, {})
    }


}