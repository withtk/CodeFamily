package com.tkip.mycode.model.peer

import com.tkip.mycode.init.base.gte


//const val BONUS_PERCENT  = 0.5   // 결제가능한 BonusCredit 퍼센티지
const val BONUS_CREDIT_MIN = 10000   // 결제가능한 BonusCredit 최소 보너스 (입찰)

/** * 보너스로 결제가능한지 여부 */
fun isGte10000Bonus() = mybonus().gte(BONUS_CREDIT_MIN)



/**
 * 내 크레딧이 결제가능한지 체크.
 * this : 결제할 금액
 * payWithBonus.true : Bonus로 결제.
 */
//fun Int.notAvailPayTotal(payWithBonus: Boolean): Boolean {
//    return if (payWithBonus) {                              // Bonus결제 선택시
//        if (isGte10000Bonus()) notAvailMyBonus()            // Bonus 10000이상인지  & myBonus 충분한지
//        else notAvailMyCredit()                             // myCredit 충분한지
//    } else notAvailMyCredit()                               // myCredit 충분한지
//}
//
//private fun Int.notAvailMyCredit(): Boolean {
//    return if (mycredit().lt(this)) {
//        lllogI("vBills credit not available")
//        toastAlert(R.string.error_not_enough_credit)
//        true
//    } else false
//}
//
//private fun Int.notAvailMyBonus(): Boolean {
//    return if (mybonus().lt(this)) {
//        lllogI("vBills sum not available")
//        toastAlert(R.string.error_not_enough_credit)
//        true
//    } else false
//}
