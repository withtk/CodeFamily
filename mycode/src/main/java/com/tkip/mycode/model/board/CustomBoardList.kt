package com.tkip.mycode.model.board

import android.content.Context
import android.view.LayoutInflater
import android.widget.LinearLayout
import androidx.databinding.DataBindingUtil
import com.tkip.mycode.R
import com.tkip.mycode.databinding.CustomBoardListBinding
import com.tkip.mycode.init.RV_GRID_HORI
import com.tkip.mycode.nav.board.tab_first.BoardAdapter
import com.tkip.mycode.util.lib.retrofit2.ESget
import com.tkip.mycode.util.tools.anim.AnimUtil
import com.tkip.mycode.util.tools.anim.hideFadeOut
import com.tkip.mycode.util.tools.anim.showFadeIn

/**
 * for  각종 보드 리스트
 * horizontal & grid
 */
class CustomBoardList : LinearLayout {
    lateinit var b: CustomBoardListBinding
    private var adapter: BoardAdapter? = null
    private lateinit var list: ArrayList<Board>
    private var hasData = false

    constructor(context: Context) : super(context)
    constructor(context: Context, title: String, query: String) : super(context) {
        b = DataBindingUtil.inflate(LayoutInflater.from(context), R.layout.custom_board_list, this, true)

        b.inTopTitle.title = title
        b.inTopTitle.lineTitle.setOnClickListener {
            if (!hasData) getEs(query)   // data 받아오기
            AnimUtil.rotateArrow(b.inTopTitle.ivArrow)
            AnimUtil.toggleUpDown(b.rv, true)
        }

//        b.lineTitle.performClick()
    }

    private fun getEs(query: String) {
        b.inTopTitle.avLoading.showFadeIn(true)

        ESget.getBoardList(query,
                success = {
                    b.inTopTitle.avLoading.hideFadeOut(false)
                    if (!it.isNullOrEmpty()) {
                        list = it
                        if (adapter == null) {
                            adapter = BoardAdapter(list, RV_GRID_HORI, null)
                            b.rv.adapter = adapter
                        }
                        adapter?.notifyDataSetChanged()
                    }
                }, fail = { b.inTopTitle.avLoading.hideFadeOut(false) })
    }


//    fun setUpRv(list: ArrayList<Board>) {
//
//        adapter = BoardAdapter(list, BoardAdapter.rvVert, null)
//        b.rv.adapter = adapter
//        adapter?.notifyDataSetChanged()
//    }

}