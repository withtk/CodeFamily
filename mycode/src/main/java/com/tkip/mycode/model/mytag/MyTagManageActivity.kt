package com.tkip.mycode.model.mytag

import android.os.Bundle
import androidx.annotation.LayoutRes
import com.tkip.mycode.R
import com.tkip.mycode.databinding.ActivityMytagManageBinding
import com.tkip.mycode.funs.common.getStr
import com.tkip.mycode.funs.common.keyword
import com.tkip.mycode.funs.common.textListener
import com.tkip.mycode.init.base.getInt
import com.tkip.mycode.init.base.getStr
import com.tkip.mycode.init.inter.interToolbarIcon
import com.tkip.mycode.init.my_bind.BindingActivity
import com.tkip.mycode.util.lib.retrofit2.ESget
import com.tkip.mycode.util.lib.retrofit2.ESquery
import com.tkip.mycode.util.tools.recycler.OnRv
import com.tkip.mycode.util.tools.spinner.LinearSpinnerString
import com.tkip.mycode.util.tools.spinner.TypeList

/**
 * 안 쓰는 거..
 */
class MyTagManageActivity : BindingActivity<ActivityMytagManageBinding>() {
    @LayoutRes
    override fun getLayoutResId() = R.layout.activity_mytag_manage

    private lateinit var adapter: MyTagAdapter
    private val items = arrayListOf<MyTag>()

    private val esSize by lazy { R.integer.es_size_tag.getInt() }
    private var esLastIndex = 0
    private var tagTYPE = TagTYPE.LOC

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        initView()
    }

    private fun initView() {
        b.activity = this

        b.inToolbar.title = R.string.title_mytag_manage.getStr()
        b.inToolbar.backIcon = true
        b.inToolbar.menuText = R.string.add_mytag_manage.getStr()
        b.inToolbar.inter = interToolbarIcon(
                onFirst = { onBackPressed() }, onTopTitle = {},
                onTextMenu = {
                    ADDmytagDialog.newInstance(null).show(supportFragmentManager, "dialog")
                },onTextMenu2 = null, onIcon = {}, onMore = {})


        /**         * String 스피너 : TAG type    */
        b.frame.addView(LinearSpinnerString(this@MyTagManageActivity,true, null, TypeList.MYTAG, tagTYPE.no,
                onSelected = { typeNo ->
                    tagTYPE = typeNo.getTagType()
                    getEsDataList(0)
                }))


        b.etSearch.textListener(after = {}, before = { _, _, _, _ -> }, on = { _, _, _, _ ->
            b.etSearch.keyword()?.let { keyword ->
                if (keyword.length > 1) getEsDataList(0)   // 두 글자부터 순간검색
            } ?: let {
                // keyword 널이면 list 지워.
                items.clear()
                adapter.notifyDataSetChanged()
            }


        })


        /** set Adapter */
        adapter = MyTagAdapter(items, MyTagAdapter.rvManage, null)
        b.rv.adapter = adapter


        b.refresh.setOnRefreshListener { getEsDataList(0) }
        b.refresh.setOnLoadMoreListener { getEsDataList(esLastIndex) }

        getEsDataList(0)
    }


    /** * 가져오기  */
    private fun getEsDataList(from: Int) {
//        from?.let { esLastIndex = from }

        ESget.getMyTagList(ESquery.mytagManage(esSize, esLastIndex, tagTYPE, b.etSearch.getStr()),
                success = {
                    esLastIndex = OnRv.setRvTagList(it, from, items, b.inData, R.string.info_no_my_banner.getStr(), b.refresh, b.tvLast)
                    adapter.notifyDataSetChanged()
                }, fail = { })


    }


}
