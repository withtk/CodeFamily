package com.tkip.mycode.model.media

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import com.tkip.mycode.databinding.HolderSpinnerMediaBinding


class SpinnerMediaAdapter(val items: ArrayList<Media?>) : BaseAdapter() {

    override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View {

        val b = HolderSpinnerMediaBinding.inflate(LayoutInflater.from(parent?.context), parent, false)
        items[position]?.let {
            b.media = items[position]
        }

        return b.root
    }

    override fun getItem(position: Int): Any? {
        return null
    }

    override fun getItemId(position: Int): Long {
        return 0
    }

    override fun getCount(): Int {
        return items.size
    }




//    inner class SpinnerMediaViewHolder(val b: HolderSpinnerMediaBinding) : RecyclerView.ViewHolder(b.root) {
//
////        private lateinit var storeMenu: StoreMenu
//
//        fun bind() {
//        }
//
//    }


}