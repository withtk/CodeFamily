package com.tkip.mycode.model.bid

import android.os.Parcel
import android.os.Parcelable
import com.tkip.mycode.ad.AdType
import com.tkip.mycode.ad.BannerType
import com.tkip.mycode.init.base.createKey
import com.tkip.mycode.model.board.Board
import com.tkip.mycode.model.my_enum.*
import com.tkip.mycode.model.peer.myuid
import com.tkip.mycode.util.tools.date.rightNow

/**
 * 1. 입찰  : (bid.key == cost.key는 동일하다)
 * 2. 배너  : 유저삭제(사진은 남아야 함)
 */
data class Bid(

        var key: String = createKey(),
        var uid: String = myuid(),
        var adTypeNo: Int? = null,
        var bannerTypeNo: Int = BannerType.NOTHING.no,   // NOTHING으로 해야 함. 아직 선택 안 됐을 때를 체크할때 필요하다. in AddBannerDialog
        var payTypeNo: Int? = null,     // BONUS or CREDIT   나중에 보드마스터에게 배당줄 때 보너스는 보너스로. 크레딧은 크레딧으로. 그럼 캐럿은 왜 만들었지..?

        /////////////////// adType.Board일때만 ///////////////////////////////////////////////////
        var boardKey: String? = null,   // 게시될 보드키
        var boardName: String? = null,  // 게시될 보드타이틀

        /////////////////// 광고되는 개체 ///////////////////////////////////////////////////////////
        var dataTypeNo: Int? = null,
        var modelKey: String? = null,
        var modelTitle: String? = null,
        var modelThumb: String? = null,     // 모델 fotoList 첫번째 사진.

        /////////////////// 배너모델을 따로 만들면 안 되는 이유: cons.banner.xml을 두개 만들어야 한다. 너무 복잡해지니까..//////////
        var title: String? = null,
        var comment: String? = null,
        var comment2: String? = null,
        var photoUrl: String? = null,
        var photoThumb: String? = null,
        var delDate: Long? = null,

        /////////////////// 기간/결제 ///////////////////////////////////////////////////////////////////
        var term: Int? = null,      // 광고게재 기간   주의) 기본광고는 1000이상.
        var hitDate: Int? = null,  // 광고게재 날짜  예) 20190101   in functions   중요:Bid & Banner 구분하는 키.
        var startDate: Int? = null,  // 한번에 입찰한 기간 : 시작 날짜    예) 20190101
        var endDate: Int? = null,    // 한번에 입찰한 기간 : 마지막 날짜  예) 20190101

        var amount: Int? = null,    // 광고 입찰한 가격(필요할때만 쓰임)

        //////////////////////////////////////////////////////////////////////////////////////////
        var statusNo: Int = Status.NOTHING.no,   // 유찰됐는데 환불 안 되는 경우를 대비하여 12시에 status == 입찰중인 내역만 불러와 stack 개수에 초과되는 bid 환불처리할 것.
        var addDate: Long = rightNow()   // 같은 입찰 그룹은 모두 동일한 addDate.

) : Parcelable {
    /**     * clone    */
    @Suppress("UNCHECKED_CAST")
    constructor(bid: Bid) : this(
            key = bid.key,
            uid = bid.uid,
            adTypeNo = bid.adTypeNo,
            bannerTypeNo = bid.bannerTypeNo,
            payTypeNo = bid.payTypeNo,

            boardKey = bid.boardKey,
            boardName = bid.boardName,

            dataTypeNo = bid.dataTypeNo,
            modelKey = bid.modelKey,
            modelTitle = bid.modelTitle,
            modelThumb = bid.modelThumb,

            title = bid.title,
            comment = bid.comment,
            comment2 = bid.comment2,
            photoUrl = bid.photoUrl,
            photoThumb = bid.photoThumb,
            delDate = bid.delDate,

            term = bid.term,
            hitDate = bid.hitDate,
            startDate = bid.startDate,
            endDate = bid.endDate,

            amount = bid.amount,
//            amountBack = bid.amountBack,
//            paidCredit = bid.paidCredit,
//            paidBonusCredit = bid.paidBonusCredit,

            statusNo = bid.statusNo,
            addDate = bid.addDate
    )

    constructor(source: Parcel) : this(
            source.readString()!!,
            source.readString()!!,
            source.readValue(Int::class.java.classLoader) as Int?,
            source.readInt(),
            source.readValue(Int::class.java.classLoader) as Int?,
            source.readString(),
            source.readString(),
            source.readValue(Int::class.java.classLoader) as Int?,
            source.readString(),
            source.readString(),
            source.readString(),
            source.readString(),
            source.readString(),
            source.readString(),
            source.readString(),
            source.readString(),
            source.readValue(Long::class.java.classLoader) as Long?,
            source.readValue(Int::class.java.classLoader) as Int?,
            source.readValue(Int::class.java.classLoader) as Int?,
            source.readValue(Int::class.java.classLoader) as Int?,
            source.readValue(Int::class.java.classLoader) as Int?,
            source.readValue(Int::class.java.classLoader) as Int?,
            source.readInt(),
            source.readLong()
    )

    override fun describeContents() = 0

    override fun writeToParcel(dest: Parcel, flags: Int) = with(dest) {
        writeString(key)
        writeString(uid)
        writeValue(adTypeNo)
        writeInt(bannerTypeNo)
        writeValue(payTypeNo)
        writeString(boardKey)
        writeString(boardName)
        writeValue(dataTypeNo)
        writeString(modelKey)
        writeString(modelTitle)
        writeString(modelThumb)
        writeString(title)
        writeString(comment)
        writeString(comment2)
        writeString(photoUrl)
        writeString(photoThumb)
        writeValue(delDate)
        writeValue(term)
        writeValue(hitDate)
        writeValue(startDate)
        writeValue(endDate)
        writeValue(amount)
        writeInt(statusNo)
        writeLong(addDate)
    }

    companion object {

        fun create(adType: AdType, any: Any, board: Board?) =
                Bid().apply {
                    adTypeNo = adType.no
                    bannerTypeNo = adType.bannerType.no

                    board?.let {
                        boardKey = it.key
                        boardName = it.title
                    }

                    dataTypeNo = any.getMyDataType().no
                    modelKey = any.anyKey()
                    modelTitle = any.anyTitle()
                    modelThumb = any.anyThumb(0)

                    title = any.anyTitle()
                    comment = any.anyComment()
                    photoUrl = any.anyUrl(0)
                    photoThumb = any.anyThumb(0)
                }

        /*** 배너 만들기 */
        fun banner(bannerType: BannerType?) =
                Bid().apply {
                    bannerType?.let { bannerTypeNo = it.no }
                    statusNo = Status.BANNER_REQUESTED.no   // 처음 배너 만들때는 항상 requested.
                }

        @JvmField
        val CREATOR: Parcelable.Creator<Bid> = object : Parcelable.Creator<Bid> {
            override fun createFromParcel(source: Parcel): Bid = Bid(source)
            override fun newArray(size: Int): Array<Bid?> = arrayOfNulls(size)
        }
    }
}