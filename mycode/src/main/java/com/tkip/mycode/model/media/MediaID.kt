package com.tkip.mycode.model.media

import android.os.Parcel
import android.os.Parcelable
import com.tkip.mycode.init.base.createKey

/**
 * store의 미디어 아이디 기입시 사용.
 */
data class MediaID(

        var key: String,
        var name: String,
        var idString: String,
        var addDate: Long

) : Parcelable{
    constructor() : this(key = createKey(), name = "idString", idString = "idString", addDate = System.currentTimeMillis())
    constructor(myMedia: String) : this(key = createKey(), name = myMedia, idString = "idString", addDate = System.currentTimeMillis())
//    constructor(myMedia: String, idString: String) : this(key = createKey(), name = myMedia, idString = idString, addDate = System.currentTimeMillis())

    constructor(source: Parcel) : this(
    source.readString()!!,
    source.readString()!!,
    source.readString()!!,
    source.readLong()
    )

    override fun describeContents() = 0

    override fun writeToParcel(dest: Parcel, flags: Int) = with(dest) {
        writeString(key)
        writeString(name)
        writeString(idString)
        writeLong(addDate)
    }

    companion object {
        @JvmField
        val CREATOR: Parcelable.Creator<MediaID> = object : Parcelable.Creator<MediaID> {
            override fun createFromParcel(source: Parcel): MediaID = MediaID(source)
            override fun newArray(size: Int): Array<MediaID?> = arrayOfNulls(size)
        }
    }
}









