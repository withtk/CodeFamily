package com.tkip.mycode.model.viewmodel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.tkip.mycode.model.board.Board
import com.tkip.mycode.model.count.Count
import com.tkip.mycode.util.lib.retrofit2.ESget

class BoardViewModel : ViewModel() {

    var liveBoard = MutableLiveData<Board>()
    var liveBoardList = ListLiveData<Board>()

    val liveBookmark = MutableLiveData<Count>()
    val livePush = MutableLiveData<Count>()


//    fun getBoard(boardKey: String): LiveData<Board> {
//        Firee.getBoard(boardKey, success = { it?.let { board1 ->liveBoard.value = board1 }}, fail = {})
//        return liveBoard
//    }
//
//    fun getEsBoard(key:String){
//        ESget.getBoard(key,{},{})
//    }

    fun getEsBoardList(query: String): ListLiveData<Board> {
        ESget.getBoardList(query,
                success = {
                    if (it == null) {
                        liveBoardList.clear(true)
                    } else {
                        liveBoardList.clear(false)
                        liveBoardList.addAll(it)
                    }
                }, fail = {})
        return liveBoardList
    }

    /********************************************************************************************
     ********************************* Count ***************************************************/


    /**     * for FIRESTORE     */
//    fun getCount(board: Board): LiveData<Count> {
//        Firee.getCount(Cntch.BOOK.getTopChild(DataType.BOARD), board.key, success = { liveBookmark.value = it }, fail = {})
//        return liveBookmark
//    }
//
//    fun getAllEsCount(board: Board) {
//        getEsCountList(board, Cntch.BOOK)
//        getEsCountList(board, Cntch.PUSH)
//    }
//
//    /**     * for ElasticSearch     */
//    fun getEsCountList(board: Board, cnt: Cntch): LiveData<Count> {
//
//        ESget.getCountList(cnt.getTopChild(DataType.BOARD), ESquery.countUidModelKey(board.key),
//                success = {
//                    if (it == null || it.isEmpty()) {
//                        setLiveCount(cnt, null)
//                    } else {
//                        setLiveCount(cnt, it[0])
//                    }
//
//                }, fail = {})
//        return returnLiveCount(cnt)
//    }
//
//    private fun setLiveCount(cnt: Cntch, count: Count?) {
//        when (cnt) {
//            Cntch.BOOK -> liveBookmark.value = count
//            Cntch.PUSH -> livePush.value = count
//            else -> liveBookmark.value = count
//        }
//    }
//
//    private fun returnLiveCount(cnt: Cntch) = when (cnt) {
//        Cntch.BOOK -> liveBookmark
//        Cntch.PUSH -> livePush
//        else -> liveBookmark
//    }
//
//    private fun Cntch.count() =
//            when (this) {
//                Cntch.BOOK -> liveBookmark.value
//                Cntch.PUSH -> livePush.value
//                else -> null
//            }

//    fun updateCount(board: Board, cnt: Cntch, v: View) {
//        cnt.updateCount(board, cnt.count(), v, success = { on, count1 ->
//            if (on) setLiveCount(cnt, count1)
//            else setLiveCount(cnt, null)
//        }, fail = {})
//    }


//
//
//    fun updateCount(board: Board, v: View) {
//        val on = (v as CheckBox).isChecked
//        if (on) {// 증가
//            Firee.addCount(Cntch.BOOK.getTopChild(DataType.BOARD), Count.create(board, Cntch.BOOK),
//                    success = {
//                        liveBookmark.value = it
//                        TToast.showNormal(R.string.complete_add_bookmark)
//                    }, fail = {})
//        } else {// 감소
//            liveBookmark.value?.let {
//                Firee.deleteCount(it,
//                        success = {
//                            liveBookmark.value = null
//                            TToast.showNormal(R.string.complete_del_bookmark)
//                        },
//                        fail = {})
//            }
//        }
//    }


}