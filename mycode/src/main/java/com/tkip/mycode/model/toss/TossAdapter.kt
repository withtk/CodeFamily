package com.tkip.mycode.model.toss

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.RecyclerView
import com.tkip.mycode.databinding.HolderTossModelBinding
import com.tkip.mycode.funs.common.lllogI
import com.tkip.mycode.init.TOSS_LIST
import com.tkip.mycode.init.inter.interTossModel

/**
 * board list
 */
class TossAdapter(private val items: ArrayList<Toss>, val onClick: ((Toss) -> Unit)?) : RecyclerView.Adapter<TossAdapter.TossModelViewHolder>() {
    private lateinit var activity: AppCompatActivity

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): TossModelViewHolder {
        this.activity = parent.context as AppCompatActivity
        val binding = HolderTossModelBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return TossModelViewHolder(binding)
    }

    override fun getItemCount(): Int {
        return items.size
    }

    override fun onBindViewHolder(h: TossModelViewHolder, p: Int) {
        val t = items[p]
        t.type = TOSS_LIST
        h.bind(items[p])
    }

    /**
     *
     */
    inner class TossModelViewHolder(val b: HolderTossModelBinding) : RecyclerView.ViewHolder(b.root) {

        private lateinit var toss: Toss

        fun bind(toss: Toss) {
            this.toss = toss
//            b.inToss.holder = this
            b.inToss.toss = toss

            b.inToss.inter = interTossModel(
                    onClickGoto = {
                        if (onClick == null) {

                        } else onClick.invoke(toss)
                    },
                    onClickRemove = {
                        lllogI("TossModelViewHolder onClickRemove ")
                        items.remove(toss)
                        notifyDataSetChanged()
                    },
                    onClickClose = {}
            )

//            b.inToss.line.setOnClickListener {
//                onClick(toss)
//            }
//            b.inToss.btnDelete.setOnClickListener {
//                items.remove(toss)
//                notifyDataSetChanged()
//            }

            b.executePendingBindings()
        }


    }
}