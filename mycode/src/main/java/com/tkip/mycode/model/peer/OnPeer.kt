package com.tkip.mycode.model.peer

import android.content.Context
import android.content.Intent
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import com.tkip.mycode.dialog.admin.PeerNewDialog
import com.tkip.mycode.dialog.peer.PeerUpdateDialog
import com.tkip.mycode.dialog.toast.TToast
import com.tkip.mycode.init.MainActivity
import com.tkip.mycode.init.base.Sred
import com.tkip.mycode.init.base.gte
import com.tkip.mycode.init.base.lt
import com.tkip.mycode.model.my_enum.PeerType
import com.tkip.mycode.util.lib.TransitionHelper
import com.tkip.mycode.util.lib.retrofit2.ESget
import com.tkip.mycode.util.lib.retrofit2.ESquery


class OnPeer {

    companion object {

        lateinit var peer: Peer   //todo: ?로 바꿀 것. 널이면 바로 flag Top으로 signActivity로 이동.
        var peerMap = mutableMapOf<String, Peer>()

//        fun peer() :Peer{
//            if(peer==null) // todo : goto sign Activity.
//            return peer
//        }


        fun myUid(): String = peer.uid
        fun peer() = peer

        fun setNull() {
            peer = Peer()
        }


        /************************************************************************
         ************************************************************************
         ************************************************************************/

        /**
         * set Peer in device sred
         *
         * REALPEER는 항상 로그인된 PEER로 유지.
         * PEER는 변경 될 수 있음.(예, 테스트아이디)
         * 강제종료된 후에는 REALPEER로 받아와서 뿌려주면 됨.
         */
        fun setUpPeer(peer1: Peer?) {
            if (Sred.getObjectPeer(Sred.PEER_REAL).uid == Sred.getObjectPeer(Sred.PEER).uid) {
                Sred.putObjectPeer(Sred.PEER_REAL, peer1)
                Sred.putObjectPeer(Sred.PEER, peer1)
                peer1?.let {
                    peer = peer1
                }
            }
        }

        fun setFrontPeer(peer1: Peer) {
            Sred.putObjectPeer(Sred.PEER, peer1)
            peer = peer1
        }

        fun restoreFrontPeer() {
            val realPeer = Sred.getObjectPeer(Sred.PEER_REAL)
            Sred.putObjectPeer(Sred.PEER, realPeer)
            peer = realPeer
        }

        fun startMainActiClear(activity: AppCompatActivity) {
            val intent = Intent(activity, MainActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
            activity.startActivity(intent)
            activity.finish()
        }


        /**
         * 단순 peer uid string 비교.
         * ex) store, storeRating, master, post authur
         */
        @JvmStatic
        fun isMyPeer(uid: String?) = (peer.uid == uid)
//                if (uid == null) false
//                else peer.uid == uid


        /************************************************************************
         ************************************************************************
         ************************************************************************/

        fun notAllowedDB(): Boolean {
            /*if (OnInternet.isOffLine()) {
                TToast.showNormal(OnApp.getAppContext().getString(com.tkip.mycode.R.string.info_offline))
                MyProgress.cancel()
                return true
            } else {

            }*/
            return false
        }

//        /**     //todo:    * 게스트 check    모든 add에 앞서 체크할 것.      */
//        @JvmStatic
//        fun isGuest(authorUid: String) = myUid() == authorUid

        /**         * author check         */
        @JvmStatic
        fun isAuthor(authorUID: String) = myUid() == authorUID

        /**         * admin check         */
        // todo : ES db값으로 비교를 해야..하나..? 아니면 그냥 실시간peer를 믿어야 하나..?
        @JvmStatic
        fun isAdmin(isToast: Boolean): Boolean {
            return if (peer.type.gte(PeerType.ADMIN_1.no)) true
            else {
                if (isToast) TToast.showWarning(PeerType.ADMIN_1.msgId)
                false
            }
        }

        @JvmStatic
        fun isAdmin(adminPeerType: PeerType, isToast: Boolean): Boolean {
            return if (peer.type.gte(adminPeerType.no)) true
            else {
                if (isToast) TToast.showWarning(adminPeerType.msgId)
                false
            }
        }

        @JvmStatic
        fun isAdmin(adminPeerType: PeerType) = (peer.type.gte(adminPeerType.no))

        @JvmStatic
        fun isAdminSupervisor(isToast: Boolean): Boolean {
            return if (peer.type.gte(PeerType.ADMIN_SUPERVISOR.no)) true
            else {
                if (isToast) TToast.showWarning(PeerType.ADMIN_SUPERVISOR.msgId)
                false
            }
        }


        @JvmStatic
        fun isNotAdmin(isToast: Boolean): Boolean {
            return if (peer.type.lt(PeerType.ADMIN_1.no)) {
                if (isToast) TToast.showWarning(PeerType.ADMIN_1.msgId)
                true
            } else false
        }


        /**************************************************
         * nick email 존재여부 체크
         *************************************************/
        fun existPeerBody(child: String, value: String, success: (Boolean) -> Unit, fail: () -> Unit) {
            val query = ESquery.peerExist(child, value)
            ESget.getPeerBody(query,
                    success = {
                        it?.hits?.total?.value?.let { size ->
                            success(size > 0)
                        } ?: success(false)
                    }, fail = { fail() })
        }


        /**************************************************
         * my peer 정보 변경
         *************************************************/
        fun openUpdatePeerDialog(activity: AppCompatActivity, ok: ((String) -> Unit)?) {

            /*** 업로드 가능 여부 체크 */
            if (checkAllAvailUpload(activity)) return

            PeerUpdateDialog.newInstance(ok = { ok?.invoke(it) }).show(activity.supportFragmentManager, "dialog")
//        UpdateMyPeerDialog.newInstance(mypeer(), ok = { b.peer = it }).show((activity as AppCompatActivity).supportFragmentManager, "dialog")
        }

        fun openNewPeerDialog(activity: AppCompatActivity, ok: ((String) -> Unit)?) {
            PeerNewDialog.newInstance().show(activity.supportFragmentManager, "dialog")
        }


        fun gotoAdminPeerActivity(context: Context, arrPair: Array<androidx.core.util.Pair<View, String>>?) {
            if (isNotAdmin(null, null)) return
            Intent(context, PeerListActivity::class.java).apply {
                addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
                TransitionHelper.startTran(context, this, arrPair)
            }
        }


    }
}