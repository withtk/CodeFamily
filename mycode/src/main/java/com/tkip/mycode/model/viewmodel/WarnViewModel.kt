package com.tkip.mycode.model.viewmodel

import androidx.lifecycle.ViewModel
import com.tkip.mycode.init.EES_WARN
import com.tkip.mycode.model.warn.Warn
import com.tkip.mycode.util.lib.retrofit2.ESget
import com.tkip.mycode.util.lib.retrofit2.ESquery

class WarnViewModel : ViewModel() {

    val liveWarnList = ListLiveData<Warn>()

    fun getEsWarnList(  size: Int, from: Int ): ListLiveData<Warn> {
        val query = ESquery.warnList(size, from)
        ESget.getWarnList( EES_WARN,query,
                success = {
                    if (it == null) {
                        liveWarnList.clear(true)
                    } else {
                        if (from == 0) liveWarnList.clear(false)
                        liveWarnList.addAll(it)
                    }
                }, fail = {})
        return liveWarnList
    }


}