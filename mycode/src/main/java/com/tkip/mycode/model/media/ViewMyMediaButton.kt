package com.tkip.mycode.model.media

import android.content.Context
import android.util.AttributeSet
import android.view.LayoutInflater
import android.widget.LinearLayout
import androidx.databinding.DataBindingUtil
import com.tkip.mycode.R
import com.tkip.mycode.databinding.ViewMyMediaButtonBinding
import com.tkip.mycode.nav.store.add.MyMedia


class ViewMyMediaButton : LinearLayout {
    private lateinit var b: ViewMyMediaButtonBinding

    constructor(context: Context?, myMedia: MyMedia, mediaId:String, onClick:(MyMedia)->Unit ) : super(context) {

        b = DataBindingUtil.inflate(LayoutInflater.from(context), R.layout.view_my_media_button, this, true)
        b.myMedia=myMedia
        b.mediaId=mediaId

        b.btnMedia.setOnClickListener{
            onClick(myMedia)
        }

    }

    constructor(context: Context) : super(context) {
        init( )
    }

    constructor(context: Context, attrs: AttributeSet?) : super(context, attrs) {
        init( )
    }

    constructor(context: Context, attrs: AttributeSet?, defStyleAttr: Int) : super(context, attrs, defStyleAttr) {
        init( )
    }

    private fun init( ) {

    }


}