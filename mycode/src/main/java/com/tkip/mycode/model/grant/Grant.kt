package com.tkip.mycode.model.grant

import android.os.Parcel
import android.os.Parcelable
import com.tkip.mycode.model.my_enum.*

/**
 * 사유 + 항변 저장 모델 : 지우면 안 됨. 처리한 뒤에 언제든 볼 수 있어야 하기때문에.
 * (board, post, flea, room, store, banner)
 * case 1. 유저가 업로드 승인 요청 -> 어드민이 처리.
 * case 2. 어드민이 모델 상태변경하면서 코멘트를 남겨야할때 : grant로 코멘트 작성됨. (유저가 코멘트 확인할 때 여기서 불러옴.)
 */
data class Grant(

        var key: String = "1_Grant_Key",   // key == modelKey 하나의 data는 하나의 grant.

        ////////////////// admin /////////////////////////////////////////////////////////
        var adminUID: String? = null,                  // admin 작성자.
        var push: Int = 0,                             // push 날릴지 여부.
//        var grantTypeNo: Int = GrantType.NOTHING.no, // 거절 이유에 대한 간략한 tag.
        var reason: String? = null,                    // 해당 model의 승인여부에 대한 사유 간단히 설명.
        var handleDate: Long? = null,                  // 어드민이 처리한 시간.

        ////////////////// user /////////////////////////////////////////////////////////
        var comment: String? = null,     // 유저의 항변. (미사용중)

        ////////////////// 해당 모델 ////////////////////////////////////////////////////////
        var dataTypeNo: Int = DataType.NOTHING.no,    //  ex) post, reply, store, flea, room
        var authorUID: String? = null,   // model 작성자.
        var modelKey: String = "modelKey",
        var modelTitle: String? = null,
        var photoThumb: String? = null,
        var modelStatus: Int = Status.NOTHING.no,
        /////////////////////////////////////////////////////////////////////////////////////

        var statusNo: Int = Status.WRITING.no,    //  승인 접수.
        var addDate: Long = 0L           // 유저의 요청 시간.

) : Parcelable {
    /*** 모델에 맞게 생성   */
    constructor(any: Any) : this(
            key = any.anyKey(),

            dataTypeNo = any.getMyDataType().no,
            authorUID = any.anyAuthorUid(),
            modelKey = any.anyKey(),
            modelTitle = any.anyTitle(),
            photoThumb = any.anyThumb(0),
            modelStatus = any.anyStatusNo()
    )

    fun setAnyToGrant(any: Any?) {
        any?.let {
            dataTypeNo = any.getMyDataType().no
            authorUID = any.anyAuthorUid()
            modelKey = any.anyKey()
            modelTitle = any.anyTitle()
            photoThumb = any.anyThumb(0)
            modelStatus = any.anyStatusNo()
        }
    }

    constructor(source: Parcel) : this(
            source.readString()!!,
            source.readString(),
            source.readInt(),
            source.readString(),
            source.readValue(Long::class.java.classLoader) as Long?,
            source.readString(),
            source.readInt(),
            source.readString(),
            source.readString()!!,
            source.readString(),
            source.readString(),
            source.readInt(),
            source.readInt(),
            source.readLong()
    )

    override fun describeContents() = 0

    override fun writeToParcel(dest: Parcel, flags: Int) = with(dest) {
        writeString(key)
        writeString(adminUID)
        writeInt(push)
        writeString(reason)
        writeValue(handleDate)
        writeString(comment)
        writeInt(dataTypeNo)
        writeString(authorUID)
        writeString(modelKey)
        writeString(modelTitle)
        writeString(photoThumb)
        writeInt(modelStatus)
        writeInt(statusNo)
        writeLong(addDate)
    }

    companion object {
        @JvmField
        val CREATOR: Parcelable.Creator<Grant> = object : Parcelable.Creator<Grant> {
            override fun createFromParcel(source: Parcel): Grant = Grant(source)
            override fun newArray(size: Int): Array<Grant?> = arrayOfNulls(size)
        }
    }
}