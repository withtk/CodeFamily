package com.tkip.mycode.model.cost.act

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.RecyclerView
import com.tkip.mycode.R
import com.tkip.mycode.ad.OnBanner
import com.tkip.mycode.databinding.HolderActpeerBinding
import com.tkip.mycode.databinding.HolderPeerVertBinding
import com.tkip.mycode.dialog.admin.PeerDialog
import com.tkip.mycode.funs.common.tranPair
import com.tkip.mycode.init.RV_MY_CONTENTS_MANAGE
import com.tkip.mycode.model.cost.OnCost
import com.tkip.mycode.nav.my_room.OnMyContents
import com.tkip.mycode.util.tools.etc.OnMyClick

/**
 * Peer list
 */
class ActPeerAdapter(private val items: ArrayList<ActPeer>, val onItemClick: ((ActPeer) -> Unit)?) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {
    private lateinit var context: Context

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        this.context = parent.context
        return ActPeerHolder(HolderActpeerBinding.inflate(LayoutInflater.from(parent.context), parent, false))
    }

    override fun onBindViewHolder(h: RecyclerView.ViewHolder, p: Int) {
        (h as ActPeerHolder).bind(items[p])
    }

    override fun getItemCount(): Int {
        return items.size
    }

    /**
     * 기본
     */
    inner class ActPeerHolder(val b: HolderActpeerBinding) : RecyclerView.ViewHolder(b.root) {

        private lateinit var actPeer: ActPeer

        fun bind(actPeer: ActPeer) {
            this.actPeer = actPeer
            b.holder = this
            b.actPeer = actPeer
            b.executePendingBindings()
        }

        fun onClickCard(v: View) {
            OnMyClick.setDisableAWhileBTN(v)

            onItemClick?.let {
                it(actPeer)
                return
            }

        }

    }


}