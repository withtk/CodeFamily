package com.tkip.mycode.model.mytag

import com.tkip.mycode.R
import com.tkip.mycode.init.base.getStr
import java.util.*


/**************************** TagTYPE  ************************/
/**
 * 수정할 때 queryTypes 반드시 체크할 것.
 * queryTypes는 이 값 그대로 es에 적용. (여러개를 넣을수도 있어서 이렇게 그지같이 짰다.)
 */
enum class TagTYPE(val no: Int, val title: String, val queryTypes: String) {
    ALL(500, R.string.tagtype_all.getStr(), "500"),    // 금기어 제외한 모든 태그. for admin.  DB에는 안 들어감. query를 위해.
    BAN(1000, R.string.tagtype_ban.getStr(), "1000"),    // 금기어 욕설, 음란
    LOC(2000, R.string.tagtype_loc.getStr(), "2000"),    // 전체 통합태그  예) 지역명
    BOARD(3000, R.string.tagtype_board.getStr(), "2000,3000"),     // from 유저  BOARD 검색할 때만 나옴.
    POST(4000, R.string.tagtype_post.getStr(), "2000,4000"),     // from 유저  POST 검색할 때만 나옴.
    FLEA(5000, R.string.tagtype_flea.getStr(), "2000,5000"),       // from 유저  FLEA 검색할 때만 나옴.  물건종류
    ROOM(6000, R.string.tagtype_room.getStr(), "2000,6000"),      // from 유저  ROOM 검색할 때 나옴.
    BUILT(7000, R.string.tagtype_built.getStr(), "7000"),     // from 유저  ROOM 검색할 때 나옴.
    STORE(8000, R.string.tagtype_store.getStr(), "2000,8000"),     // from 유저  STORE 검색할 때 나옴.
    ;
}

/** * ArrayList 만들기. */
fun ArrayList<String>.setTagType(): ArrayList<String> {
    TagTYPE.values().forEach { this.add(it.title) }
    return this
}

fun Int.getTagType(): TagTYPE {
    TagTYPE.values().forEach { if (it.no == this) return it }
    return TagTYPE.LOC
}

fun String.getTagType(): TagTYPE {
    TagTYPE.values().forEach { if (it.title == this) return it }
    return TagTYPE.LOC
}