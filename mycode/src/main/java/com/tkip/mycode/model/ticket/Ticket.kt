package com.tkip.mycode.model.ticket

import android.os.Parcel
import android.os.Parcelable
import com.android.billingclient.api.Purchase
import com.tkip.mycode.init.base.createKey
import com.tkip.mycode.model.my_enum.Status
import com.tkip.mycode.model.peer.mynick
import com.tkip.mycode.model.peer.myuid
import com.tkip.mycode.util.tools.date.rightNow

/**
 * 1. sku 종류
 * 2. 영수증 Bill
 */
data class Ticket(


        /***************** Ticket 안내  *********************/
        var key: String = createKey(),
        var skuId: String = "",
        var photoUrl: String? = null,
        var photoThumb: String? = null,
        var order: Int? = null,  // 순서

        /***************** 공통사항 *********************/
        var won: Int? = null,
        var bonusPercent: Int? = null, // bonusRate  보너스로 주는 %  : 100% 단위로 int값만 넣어줘.

        var extra: Int? = null,   // 추가로 증정하는 extra 크레딧.
        var pnt: Int? = null,
        var bcrd: Int? = null,
        var crd: Int? = null,

        /***************** 구매자 영수증  *********************/
        var orderId: String? = null,
        var purchaseState: Int? = null,
        var purchaseToken: String? = null,
        var signature: String? = null,
        var purchaseTime: Long? = null,


        var uid: String? = null,
        var nick: String? = null,
        var eventNo: Int? = null,   // 이벤트 적용 여부.

        /*** <상태표시>
         *  Ticket => TICKET_DISABLED/TICKET_ENABLED
         *  Bill => BILL_REFUNDED/BILL_PURCHASED         */
        var statusNo: Int = Status.TICKET_DISABLED.no,
        var addDate: Long = rightNow()


) : Parcelable {

    /**     * for BILL     */
    constructor(ticket: Ticket, purchase: Purchase) : this(
//    constructor(ticket: Ticket ) : this(
            key = createKey(),   // 새로운 키 생성.
            skuId = ticket.skuId,

            won = ticket.won,
            crd = ticket.crd,
            bonusPercent = ticket.bonusPercent,
            extra = ticket.extra,
            pnt = ticket.pnt,    // ticket에 이미 기재되어 있음.
            bcrd = ticket.bcrd ?: ticket.extra,    // extra는 bcrd

            orderId = purchase.orderId,
            purchaseState = purchase.purchaseState,
            purchaseToken = purchase.purchaseToken,
            signature = purchase.signature,
            purchaseTime = purchase.purchaseTime,

            uid = myuid(),
            nick = mynick(),
            eventNo = ticket.eventNo,

            statusNo = Status.BILL_PURCHASED.no

    )

    constructor(source: Parcel) : this(
            source.readString()!!,
            source.readString()!!,
            source.readString(),
            source.readString(),
            source.readValue(Int::class.java.classLoader) as Int?,
            source.readValue(Int::class.java.classLoader) as Int?,
            source.readValue(Int::class.java.classLoader) as Int?,
            source.readValue(Int::class.java.classLoader) as Int?,
            source.readValue(Int::class.java.classLoader) as Int?,
            source.readValue(Int::class.java.classLoader) as Int?,
            source.readValue(Int::class.java.classLoader) as Int?,
            source.readString(),
            source.readValue(Int::class.java.classLoader) as Int?,
            source.readString(),
            source.readString(),
            source.readValue(Long::class.java.classLoader) as Long?,
            source.readString(),
            source.readString(),
            source.readValue(Int::class.java.classLoader) as Int?,
            source.readInt(),
            source.readLong()
    )

    override fun describeContents() = 0

    override fun writeToParcel(dest: Parcel, flags: Int) = with(dest) {
        writeString(key)
        writeString(skuId)
        writeString(photoUrl)
        writeString(photoThumb)
        writeValue(order)
        writeValue(won)
        writeValue(bonusPercent)
        writeValue(extra)
        writeValue(pnt)
        writeValue(bcrd)
        writeValue(crd)
        writeString(orderId)
        writeValue(purchaseState)
        writeString(purchaseToken)
        writeString(signature)
        writeValue(purchaseTime)
        writeString(uid)
        writeString(nick)
        writeValue(eventNo)
        writeInt(statusNo)
        writeLong(addDate)
    }

    companion object {
        @JvmField
        val CREATOR: Parcelable.Creator<Ticket> = object : Parcelable.Creator<Ticket> {
            override fun createFromParcel(source: Parcel): Ticket = Ticket(source)
            override fun newArray(size: Int): Array<Ticket?> = arrayOfNulls(size)
        }
    }
}