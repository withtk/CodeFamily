package com.tkip.mycode.model.peer.peer_allow

import android.os.Bundle
import androidx.annotation.LayoutRes
import com.tkip.mycode.R
import com.tkip.mycode.databinding.ActivityBoardAllowBinding
import com.tkip.mycode.init.RV_VERT
import com.tkip.mycode.init.base.getInt
import com.tkip.mycode.init.base.getStr
import com.tkip.mycode.init.inter.interToolbarIcon
import com.tkip.mycode.init.my_bind.BindingActivity
import com.tkip.mycode.util.lib.retrofit2.ESget
import com.tkip.mycode.util.lib.retrofit2.ESquery
import com.tkip.mycode.util.tools.recycler.OnRv

/**
 * board의 miniPeer
 */
class BoardAllowActivity : BindingActivity<ActivityBoardAllowBinding>() {
    @LayoutRes
    override fun getLayoutResId() = R.layout.activity_board_allow

    private lateinit var miniPeerAdapter: PeerAllowAdapter
    private val items = arrayListOf<PeerAllow>()

    private val esSize by lazy { R.integer.es_size.getInt() }
    private var esLastIndex = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        initView()
    }

    private fun initView() {
        b.activity = this

        /** 툴바 */
        b.inToolbar.title = R.string.title_board_allow.getStr()
        b.inToolbar.backIcon = true
//        b.inToolbar.menuText = R.string.add_ad_unit_manage.getStr()
        b.inToolbar.inter = interToolbarIcon(onFirst = { onBackPressed() }, onTopTitle = {}, onTextMenu = {},onTextMenu2 = null, onIcon = {}, onMore = {})


        /** set Adapter */
        setupRV()

        getEsDataList(0)

    }

    fun setupRV() {
        miniPeerAdapter = PeerAllowAdapter(items, RV_VERT)
        b.inRefresh.rv.adapter = miniPeerAdapter

        b.inRefresh. refresh.setOnRefreshListener { getEsDataList(0) }
        b.inRefresh.refresh.setOnLoadMoreListener { getEsDataList(esLastIndex) }
//        b.refresh.setEnableRefresh(false)   //상단 refresh는 불능
//        b.refresh.setEnableLoadMore(false)  // 하단 loadMore 불능.

    }


    fun getEsDataList(from: Int) {
        val query = ESquery.matchAll(esSize,from)
        ESget.getBoardAllowList(query,
                success = {
                    esLastIndex = OnRv.setRvRefresh(it,from,items,b.inRefresh,true)
                    miniPeerAdapter.notifyDataSetChanged()
                }, fail = {})

    }


}