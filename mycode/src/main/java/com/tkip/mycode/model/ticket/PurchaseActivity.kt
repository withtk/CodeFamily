package com.tkip.mycode.model.ticket

import android.os.Bundle
import androidx.annotation.LayoutRes
import com.android.billingclient.api.*
import com.tkip.mycode.R
import com.tkip.mycode.databinding.ActivityPurchaseBinding
import com.tkip.mycode.dialog.alert.MyAlert
import com.tkip.mycode.dialog.progress.MyCircle
import com.tkip.mycode.dialog.progress.WAIT_TYPE
import com.tkip.mycode.dialog.toast.toastAlert
import com.tkip.mycode.dialog.toast.toastWarning
import com.tkip.mycode.funs.common.hhh
import com.tkip.mycode.funs.common.lllogD
import com.tkip.mycode.funs.custom.toolbar.ViewToolbarIcon
import com.tkip.mycode.init.BOARD_KEY_BILL_CS
import com.tkip.mycode.init.EES_BILL
import com.tkip.mycode.init.EES_TICKET
import com.tkip.mycode.init.base.getStr
import com.tkip.mycode.init.my_bind.BindingActivity
import com.tkip.mycode.model.board.OnBoard
import com.tkip.mycode.model.cost.Cost
import com.tkip.mycode.model.my_enum.ActType
import com.tkip.mycode.model.my_enum.Status
import com.tkip.mycode.model.peer.isAdmin
import com.tkip.mycode.model.peer.myuid
import com.tkip.mycode.util.lib.retrofit2.ESget
import com.tkip.mycode.util.lib.retrofit2.ESquery
import com.tkip.mycode.util.tools.anim.gone
import com.tkip.mycode.util.tools.anim.visi
import com.tkip.mycode.util.tools.fire.addBill
import com.tkip.mycode.util.tools.fire.addCost
import com.tkip.mycode.util.tools.fire.batch
import com.tkip.mycode.util.tools.fire.myCommit
import com.tkip.mycode.util.tools.recycler.OnRv

/**
 * 구매하기
 */
class PurchaseActivity : BindingActivity<ActivityPurchaseBinding>(), PurchasesUpdatedListener {
    @LayoutRes
    override fun getLayoutResId() = R.layout.activity_purchase

    private lateinit var ticketAdapter: TicketAdapter
    private var rvType = TicketAdapter.rvTicketNormal
    private lateinit var query: String
    private val items = arrayListOf<Ticket>()
    var working = false // 결제처리 진행중 : 진행중에는 뒤로가기 금지를 위해.

    //    private var esSize = 20
//    private var esLastIndex = 0
    private val billingClient: BillingClient by lazy { BillingClient.newBuilder(this@PurchaseActivity).setListener(this).enablePendingPurchases().build() }
    private var billEvent: BillEvent? = null   // 이벤트 번호.

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        initView()
    }

    override fun onBackPressed() {
        if (working) toastAlert(R.string.alert_bill_ing)
        else super.onBackPressed()
    }

    private fun initView() {
        b.activity = this

        /*** 첫 결제여부 체크  */
        ESget.sizeTicketList(EES_BILL, ESquery.billList(10, 0, myuid()), success = {
            if (it == null || it < 1) {
                billEvent = BillEvent.FIRST
                b.tvFirst.visi()
            } else {
                billEvent = null
                b.tvFirst.gone()
            }
            lllogD("BillingActivity sizeTicketList event:$billEvent size:$it")
        }, fail = {})


        /** 툴바 */
        if (isAdmin()) {
            query = ESquery.ticketListAdmin()
            rvType = TicketAdapter.rvTicketManage
            b.frameToolbar.addView(ViewToolbarIcon(this@PurchaseActivity,
                    onBack = { onBackPressed() }, title = R.string.bill_title_admin.getStr(), menuText = R.string.add_ticket.getStr(), menuText2 = R.string.goto_bill_cs_board.getStr(),
                    onTextMenu = { ADDticketDialog.newInstance(null).show(supportFragmentManager, "dialog") },
                    onTextMenu2 = { OnBoard.checkAndGo(this@PurchaseActivity, true, BOARD_KEY_BILL_CS, arrPair = null, onSuccess = null, onNotNormal = null) },
                    onMore = null))
        } else {
            query = ESquery.ticketList(Status.TICKET_ENABLED)
            b.frameToolbar.addView(ViewToolbarIcon(this@PurchaseActivity,
                    onBack = { onBackPressed() }, title = R.string.bill_title.getStr(), menuText = R.string.goto_bill_cs_board.getStr(),
                    onTextMenu = { OnBoard.checkAndGo(this@PurchaseActivity, true, BOARD_KEY_BILL_CS, arrPair = null, onSuccess = null, onNotNormal = null) }, onMore = null))
//                    ViewToolbarEmpty(this@PurchaseActivity, R.string.bill_title.getStr(),
//                    onBack = { onBackPressed() }, onMore = null))
        }


        /** Adaper 셋팅 */
        ticketAdapter = TicketAdapter(items, rvType, onPurchase = { skuId -> OnBill.connectBillingClient(this@PurchaseActivity, billingClient, skuId, onStartFlow = {}, onFail = { working = false }) })
        b.inRefresh.rv.adapter = ticketAdapter
        b.inRefresh.refresh.setEnableRefresh(false)   //상단 refresh는 불능.
        b.inRefresh.refresh.setEnableLoadMore(false)  // 하단 loadMore 불능.
//        b.inRefresh.refresh.setOnRefreshListener { getEsDataList(0) }
//        b.inRefresh.refresh.setOnLoadMoreListener { getEsDataList(esLastIndex) }

        getEsDataList()

    }

    private fun getEsDataList() {
        ESget.getTicketList(EES_TICKET, query,
                success = {
                    if (!it.isNullOrEmpty()) OnTicket.resetSkuIDMap(it)   // map에 추가.
                    OnRv.setRvRefresh(it, 0, items, b.inRefresh, false)
                    b.inRefresh.refresh.setEnableLoadMore(false)  // 하단 loadMore 불능.
                    ticketAdapter.notifyDataSetChanged()
                }, fail = {})
    }


    /**************************************************************************************
     *  결제 리스너
     *************************************************************************************/
    override fun onPurchasesUpdated(billingResult: BillingResult, purchases: MutableList<Purchase>?) {

        when (billingResult.responseCode) {
            /*** 구매완료 */
            BillingClient.BillingResponseCode.OK -> {
                if (!purchases.isNullOrEmpty()) {
                    lllogD("OnBill onPurchasesUpdated   billing OK  purchases.size  ${purchases.size}")
                    purchases.forEach {
                        when (it.purchaseState) {
                            Purchase.PurchaseState.PURCHASED -> {
                                lllogD("OnBill onPurchasesUpdated   billing OK  sku ${it.sku}")
                                lllogD("OnBill onPurchasesUpdated   billing OK  purchaseState ${it.purchaseState}")
                                lllogD("OnBill onPurchasesUpdated   billing OK  orderId ${it.orderId}")
                                lllogD("OnBill onPurchasesUpdated   billing OK  purchaseToken ${it.purchaseToken}")
                                lllogD("OnBill onPurchasesUpdated   billing OK  isAcknowledged ${it.isAcknowledged}")
                                lllogD("OnBill onPurchasesUpdated   billing OK  isAutoRenewing ${it.isAutoRenewing}")
                                lllogD("OnBill onPurchasesUpdated   billing OK  signature ${it.signature}")
                                lllogD("OnBill onPurchasesUpdated   billing OK  accountIdentifiers ${it.accountIdentifiers}")
                                lllogD("OnBill onPurchasesUpdated   billing OK  developerPayload ${it.developerPayload}")
                                lllogD("OnBill onPurchasesUpdated   billing OK  originalJson ${it.originalJson}")
                                lllogD("OnBill onPurchasesUpdated   billing OK  packageName ${it.packageName}")
                                lllogD("OnBill onPurchasesUpdated   billing OK  purchaseTime ${it.purchaseTime}")
                                billingClient.handlePurchase(it)  // 결제 완료
                            }
                            Purchase.PurchaseState.UNSPECIFIED_STATE -> toastWarning(String.format(R.string.error_bill_error_consume.getStr(), "UNSPECIFIED_STATE "))  // 결제 오류
                            Purchase.PurchaseState.PENDING -> toastWarning(String.format(R.string.error_bill_error_consume.getStr(), "PENDING "))  // 결제 오류
                        }
                    }
                }
            }
            else -> {
                /***  구매 실패  */
                when (billingResult.responseCode) {
                    BillingClient.BillingResponseCode.USER_CANCELED -> toastWarning(String.format(R.string.error_bill_error_consume.getStr(), R.string.error_bill_user_canceled.getStr()))  // 로직 중간에 사용자의 구매취소
                    BillingClient.BillingResponseCode.BILLING_UNAVAILABLE -> toastAlert(String.format(R.string.error_bill_error_consume.getStr(), "BILLING_UNAVAILABLE"))
                    BillingClient.BillingResponseCode.DEVELOPER_ERROR -> toastAlert(String.format(R.string.error_bill_error_consume.getStr(), "DEVELOPER_ERROR"))
                    BillingClient.BillingResponseCode.ERROR -> toastAlert(String.format(R.string.error_bill_error_consume.getStr(), "ERROR"))
                    BillingClient.BillingResponseCode.FEATURE_NOT_SUPPORTED -> toastAlert(String.format(R.string.error_bill_error_consume.getStr(), "FEATURE_NOT_SUPPORTED"))
                    BillingClient.BillingResponseCode.ITEM_ALREADY_OWNED -> toastAlert(String.format(R.string.error_bill_error_consume.getStr(), "ITEM_ALREADY_OWNED"))
                    BillingClient.BillingResponseCode.ITEM_NOT_OWNED -> toastAlert(String.format(R.string.error_bill_error_consume.getStr(), "ITEM_NOT_OWNED"))
                    BillingClient.BillingResponseCode.ITEM_UNAVAILABLE -> toastAlert(String.format(R.string.error_bill_error_consume.getStr(), "ITEM_UNAVAILABLE"))
                    BillingClient.BillingResponseCode.SERVICE_DISCONNECTED -> toastAlert(String.format(R.string.error_bill_error_consume.getStr(), "SERVICE_DISCONNECTED"))
                    BillingClient.BillingResponseCode.SERVICE_TIMEOUT -> toastAlert(String.format(R.string.error_bill_error_consume.getStr(), "SERVICE_TIMEOUT"))
                    BillingClient.BillingResponseCode.SERVICE_UNAVAILABLE -> toastAlert(String.format(R.string.error_bill_error_consume.getStr(), "SERVICE_UNAVAILABLE"))
                }
                working = false
            }
        }
    }

    /**************************************************************************************
     *  결제 성공 후 : 소비 처리.
     *************************************************************************************/
    private fun BillingClient.handlePurchase(purchase: Purchase) {
        val consumeParams = ConsumeParams.newBuilder().setPurchaseToken(purchase.purchaseToken).build()
        this.consumeAsync(consumeParams) { billingResult, purchaseToken ->

            lllogD("OnBill handlePurchase   debugMessage:  ${billingResult.debugMessage}")
            lllogD("OnBill handlePurchase   purchaseToken:  $purchaseToken")

            batch().apply {

                OnTicket.skuIdMap[purchase.sku]?.let { ticket ->
                    lllogD("OnBill handlePurchase skuIdMap  ticket:  $ticket")
                    WAIT_TYPE.FULL_CIRCLE.show(this@PurchaseActivity)

                    /*** 영수증 생성. */
                    val bill = Ticket(ticket, purchase)

                    /*** 결제 이벤트 처리 */
                    billEvent?.handleEvent(bill)
                    lllogD("OnBill handlePurchase skuIdMap  bill:  $bill")

                    addBill(bill)    //  영수증 올리기.
                    addCost(Cost.new(null, ActType.BUY_CREDIT, null, bill.pnt, bill.bcrd, bill.crd, null))    // cost.credit 추가 : amount(bill.cnt + bill.add).

                    myCommit("handlePurchase", success = {
                        WAIT_TYPE.FULL_CIRCLE.show(this@PurchaseActivity)
                        hhh(2000) {
                            MyAlert.showBillSuccess(this@PurchaseActivity, bill)
                            MyCircle.cancel()
                            working = false
                        }
                    }, fail = { working = false })

                } ?: let {
                    toastAlert(R.string.error_bill_error)
                    working = false
                    return@consumeAsync
                }
            }


        }
    }

}