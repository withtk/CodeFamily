package com.tkip.mycode.model.store

import android.content.Context
import android.view.View
import com.tkip.mycode.R
import com.tkip.mycode.init.base.getI
import com.tkip.mycode.init.base.isAnyNull
import com.tkip.mycode.init.base.isNullBlank
import com.tkip.mycode.model.store.Store
import com.tkip.mycode.nav.store.OnStore


/*************************************************************************************/
/********************  ***********************************************************/
/*************************************************************************************/


fun Store.gotoAddStoreActivity(context: Context, action: Int, arrPair: Array<androidx.core.util.Pair<View, String>>?) {
    OnStore.gotoAddStoreActivity(context, action, this, arrPair)
}

fun Store.gotoStoreActivity(context: Context, isTop: Boolean, action: Int?, arrPair: Array<androidx.core.util.Pair<View, String>>?) {
    OnStore.gotoStoreActivity(context, isTop, action, this, arrPair)
}


/**
 * 유효성 체크
 * 통과 못하면 -> true
 */
fun Store.inValid(): Boolean =
        (
                this.title.isNullBlank(R.string.alert_need_to_store_name)
                        || this.local.isNullBlank(R.string.alert_need_to_store_local)
//            || this.fotoList.getI(0).isAnyNull(R.string.alert_need_to_photo)
                )