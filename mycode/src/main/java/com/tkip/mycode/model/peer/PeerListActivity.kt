package com.tkip.mycode.model.peer

import android.os.Bundle
import androidx.annotation.LayoutRes
import com.tkip.mycode.R
import com.tkip.mycode.databinding.ActivityPeerListBinding
import com.tkip.mycode.dialog.admin.PeerNewDialog
import com.tkip.mycode.funs.common.onCheckedChange
import com.tkip.mycode.funs.custom.toolbar.ViewToolbarIcon
import com.tkip.mycode.funs.search.model.CustomSearchPeer
import com.tkip.mycode.init.CH_ADD_DATE
import com.tkip.mycode.init.CH_LOGIN_TIME
import com.tkip.mycode.init.CH_TOTAL_POINT
import com.tkip.mycode.init.RV_VERT
import com.tkip.mycode.init.base.getStr
import com.tkip.mycode.init.my_bind.BindingActivity
import com.tkip.mycode.util.tools.radio.setQueryList


class PeerListActivity : BindingActivity<ActivityPeerListBinding>() {
    @LayoutRes
    override fun getLayoutResId() = R.layout.activity_peer_list

    private lateinit var viewToolbarIcon: ViewToolbarIcon
    private lateinit var customSearchPeer: CustomSearchPeer
    private val arr = arrayOf(CH_LOGIN_TIME, CH_ADD_DATE, CH_TOTAL_POINT)
    private var rbID = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        initView()
    }

    private fun initView() {
        b.activity = this

        /** 툴바 */
        viewToolbarIcon = ViewToolbarIcon(this@PeerListActivity, onBack = { onBackPressed() }, topTitle = null, onTopTitle = {}, title = R.string.admin_title_peer_list.getStr(), menuText = null, onTextMenu = null, onMore = null)
        b.frameToolbar.addView(viewToolbarIcon)

        /** custom Peer 셋팅 */
        customSearchPeer = CustomSearchPeer(this@PeerListActivity, RV_VERT)
        b.frame.addView(customSearchPeer)


        b.cb.onCheckedChange { _, _ -> search() }

        /** 정렬기준 세팅 */
        b.rg.setQueryList(this@PeerListActivity, arr)
        b.rg.setOnCheckedChangeListener { _, id ->
            rbID = id
            search()
        }

        /** 초기 검색 : 첫번째 라디오버튼 클릭 */
        b.rg.getChildAt(0).performClick()
    }

    fun search() {
        customSearchPeer.getEsDataList(0, arr[rbID], b.cb.isChecked, null)
    }

    fun onClickAddPeer() {
        PeerNewDialog.newInstance().show(supportFragmentManager, "dialog")
    }

}
