package com.tkip.mycode.model.reply

import android.content.Context
import android.view.LayoutInflater
import android.widget.LinearLayout
import androidx.databinding.DataBindingUtil
import com.tkip.mycode.R
import com.tkip.mycode.init.inter.interReplyEarlier
import com.tkip.mycode.databinding.ViewReplyEarlierBinding
import com.tkip.mycode.model.reply.Reply


class ViewReplyEarlier : LinearLayout {
    private lateinit var b: ViewReplyEarlierBinding

    constructor(context: Context) : super(context)
    constructor(context: Context, reply: Reply, onClick: (Reply) -> Unit) : super(context) {

        b = DataBindingUtil.inflate(LayoutInflater.from(context), R.layout.view_reply_earlier, this, true)
        b.reply = reply
        b.inter = interReplyEarlier(
                onClickLine = {
                    onClick(reply)
                })

    }


}