package com.tkip.mycode.model.store

import android.os.Parcel
import android.os.Parcelable
import com.google.firebase.firestore.Exclude

/**
 * 유저가 준 평가star의 총 개수.
 * store에 넣을지 말지 고민.
 *
 *
 * store > rating
 *
 */
data class Rating(

        var storeKey: String,

        var average: String = "0.0",
        var averageFloat: Float = 0F,
        var ratingCount: Int = 0,
        var star5: Int = 0,               //  5별 개수  그때그때 다시 계산해서 넣어야 함.
        var star4: Int = 0,
        var star3: Int = 0,
        var star2: Int = 0,
        var star1: Int = 0


) : Parcelable{

    constructor() : this("storeKey")

    @Exclude
    fun toMap(): Map<String, Any> {

        val result = HashMap<String, Any>()
        result["storeKey"] = storeKey

        result["average"] = average
        result["averageFloat"] = averageFloat
        result["ratingCount"] = ratingCount
        result["star5"] = star5
        result["star4"] = star4
        result["star3"] = star3
        result["star2"] = star2
        result["star1"] = star1

        return result
    }

    constructor(source: Parcel) : this(
    source.readString()!!,
    source.readString()!!,
    source.readFloat(),
    source.readInt(),
    source.readInt(),
    source.readInt(),
    source.readInt(),
    source.readInt(),
    source.readInt()
    )

    override fun describeContents() = 0

    override fun writeToParcel(dest: Parcel, flags: Int) = with(dest) {
        writeString(storeKey)
        writeString(average)
        writeFloat(averageFloat)
        writeInt(ratingCount)
        writeInt(star5)
        writeInt(star4)
        writeInt(star3)
        writeInt(star2)
        writeInt(star1)
    }

    companion object {
        @JvmField
        val CREATOR: Parcelable.Creator<Rating> = object : Parcelable.Creator<Rating> {
            override fun createFromParcel(source: Parcel): Rating = Rating(source)
            override fun newArray(size: Int): Array<Rating?> = arrayOfNulls(size)
        }
    }
}









