package com.tkip.mycode.model.media

import android.content.Context
import android.util.AttributeSet
import android.view.LayoutInflater
import android.widget.LinearLayout
import androidx.databinding.DataBindingUtil
import com.tkip.mycode.R
import com.tkip.mycode.databinding.ViewMyMediaEdittextBinding
import com.tkip.mycode.nav.store.add.MyMedia


class ViewMyMediaEditText : LinearLayout {
    public lateinit var b: ViewMyMediaEdittextBinding

    constructor(context: Context?,myMedia: MyMedia) : super(context) {

        b = DataBindingUtil.inflate(LayoutInflater.from(context), R.layout.view_my_media_edittext, this, true)
        b.myMedia=myMedia

    }

    constructor(context: Context) : super(context) {
        init( )
    }

    constructor(context: Context, attrs: AttributeSet?) : super(context, attrs) {
        init( )
    }

    constructor(context: Context, attrs: AttributeSet?, defStyleAttr: Int) : super(context, attrs, defStyleAttr) {
        init( )
    }

    private fun init( ) {
        /* // inflate(getContext(), R.layout.beacon_setup_view, this);
         val inflater = context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
         inflater.inflate(R.layout.view_info, this)

         tvTitle = findViewById(R.id.tv_title)
         tvDescription = findViewById(R.id.tv_description)
         ivIcon = findViewById(R.id.iv_icon)
         btn = findViewById<Button>(R.id.btn)*/
    }


}