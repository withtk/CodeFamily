package com.tkip.mycode.model.viewmodel

import androidx.lifecycle.MutableLiveData

class ListLiveData<T> : MutableLiveData<ArrayList<T>>() {
    init {
        value = ArrayList()
    }

    fun add(item: T) {
        val items = value
        items!!.add(item)
        value = items   // 데이터가 변경되는 시점. 이로써 observer에서 콜백.
    }

    fun addAll(list: List<T>) {
        val items = value
        items!!.addAll(list)
        value = items
    }

    fun clear(notify: Boolean) {
        val items = value
        items!!.clear()
        if (notify) {
            value = items    // notify == true 이면 clear된 후에 observer 콜백.
        }
    }

    fun remove(item: T) {
        val items = value
        items!!.remove(item)
        value = items
    }

    fun notifyChange() {
        val items = value
        value = items
    }

}
