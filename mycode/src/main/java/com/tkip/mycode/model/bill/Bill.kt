package com.tkip.mycode.model.bill

import android.os.Parcel
import android.os.Parcelable
import com.tkip.mycode.init.base.createKey
import com.tkip.mycode.model.my_enum.Status
import com.tkip.mycode.model.peer.mynick
import com.tkip.mycode.model.peer.myuid
import com.tkip.mycode.util.tools.date.rightNow

/**
 * 계산서 : 안 쓰는 거..
 */
data class Bill(

        var key: String = createKey(),
        var uid: String = myuid(),
        var nick: String = mynick(),

        var skuId: String = "skuId",
        var bCrd: Int? = null,
        var crd: Int = 0,
        var pnt: Int? = null,
        var extra: Int = 0,   // 추가로 증정하는 extra credit

        var statusNo: Int = Status.BILL_PURCHASED.no,
        var purchaseTime: Long? = null,
        var addDate: Long = rightNow()

) : Parcelable {

//    constructor(msku: Msku, extra: Int) : this(
//            skuId = msku.id,
//            bCrd = msku.bCrd,
//            crd = msku.crd,
//            pnt = msku.crd + extra,    // 최종 크레딧과 언제나 일치 : 충전 크레딧 + extra 크레딧
//            extra = extra
//    )

    constructor(source: Parcel) : this(
            source.readString()!!,
            source.readString()!!,
            source.readString()!!,
            source.readString()!!,
            source.readValue(Int::class.java.classLoader) as Int?,
            source.readInt(),
            source.readValue(Int::class.java.classLoader) as Int?,
            source.readInt(),
            source.readInt(),
            source.readValue(Long::class.java.classLoader) as Long?,
            source.readLong()
    )

    override fun describeContents() = 0

    override fun writeToParcel(dest: Parcel, flags: Int) = with(dest) {
        writeString(key)
        writeString(uid)
        writeString(nick)
        writeString(skuId)
        writeValue(bCrd)
        writeInt(crd)
        writeValue(pnt)
        writeInt(extra)
        writeInt(statusNo)
        writeValue(purchaseTime)
        writeLong(addDate)
    }

    companion object {
        @JvmField
        val CREATOR: Parcelable.Creator<Bill> = object : Parcelable.Creator<Bill> {
            override fun createFromParcel(source: Parcel): Bill = Bill(source)
            override fun newArray(size: Int): Array<Bill?> = arrayOfNulls(size)
        }
    }
}