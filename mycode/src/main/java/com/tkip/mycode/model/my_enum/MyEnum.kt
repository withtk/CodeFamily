package com.tkip.mycode.model.my_enum

import android.app.Activity
import android.widget.LinearLayout
import android.widget.RadioButton
import com.tkip.mycode.R
import com.tkip.mycode.init.*
import com.tkip.mycode.init.base.getClr
import com.tkip.mycode.init.base.getDrawable
import com.tkip.mycode.init.base.getStr
import com.tkip.mycode.init.base.getStringComma
import com.tkip.mycode.util.my_view.ViewUtil
import com.tkip.mycode.util.tools.radio.ViewRGint
import java.util.*

/**
 * msg  : 상태알림(스크린 중입니다.)
 * dialogMsg : 처리질문(스크린으로 처리할까요?)
 */
/******************************** 상태 : tag, store, rating, reply, post *****************************************/

enum class Status(val no: Int, val title: String, val msg: String, val dialogMsg: String, val colorId: Int, val iconId: Int) {

    NOTHING(100,  R.string.status_nothing_title.getStr(),  R.string.status_nothing.getStr(),  R.string.status_nothing_dialog.getStr(), R.color.text_gray, R.drawable.ic_whatshot),   // 찾는 status no가 없을때 기본값.
    
    //todo : 중고, 부동산 중복 게시물로 인한 삭제.
    //todo : 스토어 삭제.
    //todo : 중고, 부동산 거래완료.

    /********* INVALID 변경불가. 승인요청 불가능.**********************/
    INVALID(200, R.string.status_invalid_title.getStr(), R.string.status_invalid.getStr(), R.string.status_invalid_dialog.getStr(), R.color.text_gray, R.drawable.ic_whatshot),      // 실효 : DB에서 완전 삭제할 것들.
    DELETED_USER(300, R.string.status_deleted_title.getStr(), R.string.status_deleted.getStr(), R.string.status_deleted_dialog.getStr(), R.color.text_gray, R.drawable.ic_whatshot),     // 유저의 삭제.
    TRANSACTION_COMPLETED(400, R.string.status_title_transaction_completed.getStr(), R.string.status_transaction_completed.getStr(), R.string.status_dialog_transaction_completed.getStr(), R.color.text_gray, R.drawable.ic_whatshot),    // 거래완료.

    /********* BLINDED  승인요청 불가능.**********************/
    BLIND_ADMIN(800, R.string.status_blind_title.getStr(), R.string.status_blind.getStr(), R.string.status_blind_dialog.getStr(), R.color.md_red_500, R.drawable.ic_whatshot),     //  관리자의 블라인드

    /********* DENIED 게시 거부. 승인요청 가능.**********************/
    DENIED(1000, R.string.status_denied_title.getStr(), R.string.status_denied.getStr(), R.string.status_denied_dialog.getStr(), R.color.text_gray, R.drawable.ic_whatshot),      //  승인 거절됨
    AD_DENIED(1050, R.string.status_ad_denied_title.getStr(), R.string.status_ad_denied.getStr(), R.string.status_ad_denied_dialog.getStr(), R.color.text_gray, R.drawable.ic_whatshot),      // 광고 게시후에 승인거절

    BANNER_DENIED(1140, R.string.status_banner_denied_title.getStr(), R.string.status_banner_denied.getStr(), R.string.status_banner_denied_dialog.getStr(), R.color.text_dark, R.drawable.ic_whatshot),      // 배너 승인거절
    BANNER_DENIED_NO_IMAGE(1146, R.string.status_banner_denied_title.getStr(), R.string.status_title_denied_no_image.getStr(), R.string.status_dialog_denied_no_image.getStr(), R.color.text_dark, R.drawable.ic_whatshot),
    BANNER_DENIED_SIZE_IMAGE(1150, R.string.status_banner_denied_title.getStr(), R.string.status_title_denied_wrong_size_image.getStr(), R.string.status_dialog_denied_wrong_size_image.getStr(), R.color.text_dark, R.drawable.ic_whatshot),
    BANNER_DENIED_BAD_WORD(1154, R.string.status_banner_denied_title.getStr(), R.string.status_title_denied_bad_word.getStr(), R.string.status_dialog_denied_bad_word.getStr(), R.color.text_dark, R.drawable.ic_whatshot),

    DENIED_DUPLICATED(1200, R.string.status_banner_denied_title.getStr(), R.string.status_title_denied_duplicated.getStr(), R.string.status_dialog_denied_duplicated.getStr(), R.color.text_dark, R.drawable.ic_whatshot),
    DENIED_PROMOTION(1220, R.string.status_banner_denied_title.getStr(), R.string.status_title_denied_promotion.getStr(), R.string.status_dialog_denied_promotion.getStr(), R.color.text_dark, R.drawable.ic_whatshot),
    DENIED_BAD_ERO(1240, R.string.status_denied_bad_title.getStr(), R.string.status_denied_bad.getStr(), R.string.status_denied_bad_dialog.getStr(), R.color.text_dark, R.drawable.ic_whatshot),
    DENIED_NO_MATCH_LOCATION(1250, R.string.status_inspecting_no_match_title.getStr(), R.string.status_inspecting_no_match.getStr(), R.string.status_inspecting_no_match_dialog.getStr(), R.color.text_dark, R.drawable.ic_whatshot),  // 위치부적절



    /********* SCREENING 신고누적 검열중. 승인요청 가능.**********************/
    SCREENING(1400, R.string.status_screening_title.getStr(), R.string.status_screening.getStr(), R.string.status_screening_dialog.getStr(), R.color.text_gray, R.drawable.ic_whatshot),    // 검열중(자동처리 for functions)
    WAIT_MASTER(1800, R.string.status_waiting_title.getStr(), R.string.status_waiting.getStr(), R.string.status_waiting_dialog.getStr(), R.color.text_gray, R.drawable.ic_whatshot),     //  대기  for Master

    /********* WRITING 작성중. 승인요청전.**********************/
    WRITING(2200, R.string.status_writing_title.getStr(), R.string.status_writing.getStr(), R.string.status_writing_dialog.getStr(), R.color.text_gray, R.drawable.ic_whatshot),            //    작성중

    /********* REQUESTED 게시 대기. 승인요청중.**********************/
    INSPECTING(2400, R.string.status_inspecting_title.getStr(), R.string.status_inspecting.getStr(), R.string.status_inspecting_dialog.getStr(), R.color.text_gray, R.drawable.ic_whatshot),  // 승인 전 검사중
    BANNER_REQUESTED(2520, R.string.status_banner_requested_title.getStr(), R.string.status_banner_requested.getStr(), R.string.status_banner_requested_dialog.getStr(), R.color.text_gray, R.drawable.ic_whatshot),  // 배너 승인요청
    WARN_REQUESTED(2540, R.string.status_warn_normal.getStr(), R.string.status_msg_warn_normal.getStr(), R.string.status_dialog_warn_normal.getStr(), R.color.colorAccent2, R.drawable.ic_whatshot),   //  신고 접수.
    GRANT_REQUESTED(2560, R.string.status_grant_normal.getStr(), R.string.status_grant_normal_msg.getStr(), R.string.status_dialog_basic.getStr(), R.color.colorAccent, R.drawable.ic_whatshot),    // 승인 접수.

    /********* NORMAL : COMPLETED **********************/
    NORMAL(3500, R.string.status_normal_1000_title.getStr(), R.string.status_normal_1000.getStr(), R.string.status_normal_1000_dialog.getStr(), R.color.colorAccent2, R.drawable.ic_whatshot),            //  정상 게시

    BID_FAILED(5550, R.string.status_ad_failed_title.getStr(), R.string.status_ad_failed.getStr(), R.string.status_ad_failed_dialog.getStr(), R.color.text_gray, R.drawable.ic_whatshot),      // 유찰
    BID_BIDDING(5600, R.string.status_ad_bidding_title.getStr(), R.string.status_ad_bidding.getStr(), R.string.status_ad_bidding_dialog.getStr(), R.color.colorPrimary, R.drawable.ic_whatshot),      // 입찰중
    BID_SUCCESS(5700, R.string.status_ad_success_title.getStr(), R.string.status_ad_success.getStr(), R.string.status_ad_success_dialog.getStr(), R.color.colorAccent, R.drawable.ic_whatshot),      // 광고중

    WARN_HANDLED(6000, R.string.status_complete_handled.getStr(), R.string.status_msg_warn_handled.getStr(), R.string.status_dialog_warn_handled.getStr(), R.color.text_gray, R.drawable.ic_whatshot),   //  신고 처리 완료.
    BANNER_GRANTED(6020, R.string.status_banner_granted_title.getStr(), R.string.status_banner_granted.getStr(), R.string.status_banner_granted_dialog.getStr(), R.color.colorAccent, R.drawable.ic_whatshot),      // 배너 승인완료.
    GRANT_HANDLED(6040, R.string.status_grant_handled.getStr(), R.string.status_grant_handled_msg.getStr(), R.string.status_dialog_basic.getStr(), R.color.text_gray, R.drawable.ic_whatshot),   // 처리 완료.

    /********* COMPLETED 결과표시. 등급 불필요 (단순 구분을 위한 status) **********************/
    TICKET_DISABLED(8000, R.string.status_ticket_disable_title.getStr(), R.string.status_ticket_disable.getStr(), R.string.status_ticket_disable_dialog.getStr(), R.color.text_gray, R.drawable.ic_whatshot), // 사용불가
    TICKET_ENABLED(8020, R.string.status_ticket_enable_title.getStr(), R.string.status_ticket_enable.getStr(), R.string.status_ticket_enable_dialog.getStr(), R.color.colorAccent, R.drawable.ic_whatshot), // 사용중
    BILL_REFUNDED(8100, R.string.status_bill_canceled_title.getStr(), R.string.status_bill_canceled.getStr(), R.string.status_bill_canceled_dialog.getStr(), R.color.text_gray, R.drawable.ic_whatshot), // 구매 환불
    BILL_PURCHASED(8120, R.string.status_bill_normal_title.getStr(), R.string.status_bill_normal.getStr(), R.string.status_bill_normal_dialog.getStr(), R.color.text_gray, R.drawable.ic_whatshot),         // 구매 완료


    ;  // 반드시 ;을 넣어줘야함.

    fun getIcon() = iconId.getDrawable()
    fun clr() = colorId.getClr()
    fun gte(no: Int) = no >= this.no
    fun lt(no: Int) = no < this.no
    fun contains(vararg statusArray: Status): Boolean {
        for (s in statusArray) {
            if (s.no == no) return true
        }
        return false
    }
}

fun Int.getStatus(): Status {
    Status.values().forEach { if (it.no == this) return it }
    return Status.NOTHING
}

/*************************************************************************************************
 * spinner에 들어갈 list
 *************************************************************************************************/

//enum class StatusLIST(val list: ArrayList<Status>) {
//    WARN(arrayListOf(Status.WARN_NORMAL, Status.WARN_HANDLED)),
//    BASIC(arrayListOf(Status.INVALID, Status.DELETED, Status.BLIND, Status.DENIED, Status.WAIT, Status.SCREENING, Status.INSPECTING, Status.WRITING, Status.NORMAL)),
//    BID(arrayListOf(Status.AD_DELETED, Status.AD_DENIED, Status.AD_CANCELED, Status.AD_EXPIRED, Status.AD_SCREENING, Status.AD_FAILED, Status.AD_BIDDING, Status.AD_SUCCESS)),
//    TAG(arrayListOf(Status.INVALID, Status.NORMAL)),
//    LOBBY(arrayListOf(Status.CHAT_INVALID, Status.CHAT_VALID)),
//    ;  // 반드시 ;을 넣어줘야함.


//    fun createArraylist(): ArrayList<String> =
//            arrayListOf<String>().apply {
//                this@StatusLIST.list.forEach {
//                    add(it.title)
//                }
//            }
//}


/******************************** 유저 등급 *****************************************/
// todo : 357구조로 만들 것.
enum class PeerType(val no: Int, val title: String, val msgId: Int, val colorId: Int, val iconId: Int) {

//    BLOCK_100(100, R.string.warn_peer_type_blocked, R.drawable.ic_person),
//    BLOCK_200(300, R.string.warn_peer_type_blocked, R.drawable.ic_person),
//    BLOCK_300(500, R.string.warn_peer_type_blocked, R.drawable.ic_person),

    BASIC_1(1000, R.string.peertype_basic1.getStr(), R.string.warn_peer_type_blocked, R.color.colorAccent2, R.drawable.ic_person),               // 익명 계정
    BASIC_2(1200, R.string.peertype_basic2.getStr(), R.string.warn_peer_type_anonymous, R.color.text_dark, R.drawable.ic_person),                // 일반 계정
    BASIC_BETA(1250, R.string.peertype_basic_beta.getStr(), R.string.warn_peer_type_anonymous, R.color.green_monster, R.drawable.ic_person),     // 베타 유저
    BASIC_3(1400, R.string.peertype_basic3.getStr(), R.string.warn_peer_type_under_grade, R.color.text_dark, R.drawable.ic_person),

    VIP_1(2000, R.string.peertype_vip.getStr(), R.string.warn_peer_type_under_grade, R.color.colorAccent, R.drawable.ic_person),

    ADMIN_1(8000, R.string.peertype_admin.getStr(), R.string.warn_peer_type, R.color.text_gray, R.drawable.ic_person),
    ADMIN_2(8200, R.string.peertype_admin.getStr(), R.string.warn_peer_type_lack_authority, R.color.text_gray, R.drawable.ic_person),
    ADMIN_SUPERVISOR(9000, R.string.peertype_admin.getStr(), R.string.warn_peer_type_supervisor, R.color.text_gray, R.drawable.ic_person),
    ;

    fun getMsg() = msgId.getStr()
    fun getIcon() = iconId.getDrawable()
    fun greaterThanOrEqualTo(peerType: PeerType) = (no >= peerType.no)
    fun lessThan(peerType: PeerType) = (no < peerType.no)

}

fun Int.getPeerType(): PeerType {
    PeerType.values().forEach {
        if (it.no == this) return it
    }
    return PeerType.BASIC_1
}


/******************************** 유저 가입 경로 *****************************************/
enum class LoginFrom(val no: Int, val title: String, private val colorId: Int, private val iconId: Int) {

    //    TEST_ID(100, R.string.unit_test.getStr(), R.color.text_gray, R.drawable.ic_person),
    ANONYMOUS(200, R.string.unit_anonymous.getStr(), R.color.text_gray, R.drawable.ic_person),
    GOOGLE(300, R.string.unit_google.getStr(), R.color.blue_500, R.drawable.ic_person),
    EMAIL(400, R.string.unit_email.getStr(), R.color.md_amber_600, R.drawable.ic_person),
    ;

    fun clr() = colorId.getClr()
    fun icon() = iconId.getDrawable()
}


fun Int.getLoginFrom(): LoginFrom {
    LoginFrom.values().forEach {
        if (it.no == this) return it
    }
    return LoginFrom.ANONYMOUS
}


/******************************** post 타입 *****************************************/
enum class PostType(val no: Int) {
    MASTER(1000),
    PEER(3000),
    NORMAL(5000),
    ;
}

/******************************** room   *****************************************
 *  index: RGstring에서 list의 index를 바로 받아올 수 있게.
 */
enum class ContractType(val no: Int, val index: Int, val title: String, val priceTitle: String, val colorId: Int) {
    RENT(1000, 0, R.string.contract_type_title_rent.getStr(), R.string.contract_type_title_rent_price.getStr(), R.color.colorPrimary),
    SELL(2000, 1, R.string.contract_type_title_sell.getStr(), R.string.title_selling_price.getStr(), R.color.colorAccent),
    DISTRIBUTION(5000, 2, R.string.contract_type_title_distribution.getStr(), R.string.contract_type_title_distribution_price.getStr(), R.color.colorAccent2),
    ;
}

fun Int.getContractType(): ContractType {
    ContractType.values().forEach { if (it.no == this) return it }
    return ContractType.RENT
}

fun String.getContractType(): ContractType {
    ContractType.values().forEach { if (it.title == this) return it }
    return ContractType.RENT
}

/** * 현재 room의 계약 상태 */
enum class RoomStatus(val no: Int, val index: Int, val title: String) {
    EMPTY(2000, 0, R.string.room_status_empty.getStr()),
    RENTING(4000, 1, R.string.room_status_renting.getStr()),
    NEW(8000, 2, R.string.room_status_new.getStr()),
    ;
}

fun Int.getRoomStatus(): RoomStatus {
    RoomStatus.values().forEach { if (it.no == this) return it }
    return RoomStatus.EMPTY
}

fun String.getRoomStatus(): RoomStatus {
    RoomStatus.values().forEach { if (it.title == this) return it }
    return RoomStatus.EMPTY
}

/** * 방 종류 */
enum class RoomType(val no: Int, val index: Int, val title: String, val colorId: Int) {
    //    STUDIO(20, R.string.room_type_studio.getStr()),
    CONDO(2000, 0, R.string.room_type_condo.getStr(), R.color.colorPrimary),
    HOUSE(4000, 1, R.string.room_type_house.getStr(), R.color.md_red_500),
    SHOP(6000, 2, R.string.room_type_shop.getStr(), R.color.overlay_dark_40),
    ETC(8000, 3, R.string.room_type_etc.getStr(), R.color.overlay_dark_40),
    ;
}

fun Int.getRoomType(): RoomType {
    RoomType.values().forEach { if (it.no == this) return it }
    return RoomType.CONDO
}

fun String.getRoomType(): RoomType {
    RoomType.values().forEach { if (it.title == this) return it }
    return RoomType.CONDO
}

/** * room 수 :  radio에 들어감.
 * TYPE_CONTRACT,TYPE_ROOM,TYPE_ROOM_STATUS 의 amount는 각 type 의 no!! */
enum class RGint(val no: Int, val title: String, val list: ArrayList<Int>, var amount: Int?) {

//    TYPE_CONTRACT(20, R.string.name_type_contract.getStr(), arrayListOf(ContractType.RENT.title, ContractType.SELL.title, ContractType.DISTRIBUTION.title), null),
//    TYPE_ROOM(30, R.string.name_type_room.getStr(), arrayListOf(RoomType.CONDO.title, RoomType.HOUSE.title, RoomType.SHOP.title, RoomType.ETC.title), null),
//    TYPE_ROOM_STATUS(40, R.string.name_type_room_status.getStr(), arrayListOf(RoomStatus.EMPTY.title, RoomStatus.RENTING.title, RoomStatus.NEW.title), null),

    ROOM(500, R.string.name_room.getStr(), arrayListOf(0, 1, 2, 3, 4, 5), null),
    TOILET(600, R.string.name_toilet.getStr(), arrayListOf(0, 1, 2, 3, 4, 5), null),
    KITCHEN(700, R.string.name_kitchen.getStr(), arrayListOf(0, 1, 2, 3, 4, 5), null),
    LIVING(800, R.string.name_living.getStr(), arrayListOf(0, 1, 2, 3, 4, 5), null),
    BATH(900, R.string.name_bath.getStr(), arrayListOf(0, 1, 2, 3, 4, 5), null),

    PARKING(1000, R.string.name_parking.getStr(), arrayListOf(0, 1, 2, 3, 4, 5), null),
    FLOOR(1100, R.string.name_floor.getStr(), arrayListOf(0, 1, 2, 3, 4, 5), null),
    ADVANCED(1200, R.string.name_in_advanced_unit.getStr(), arrayListOf(0, 1, 2, 3, 4, 5), null),    // 선불 개월
    DEPOSIT(1240, R.string.name_in_deposit_unit.getStr(), arrayListOf(0, 1, 2, 3, 4, 5), null),    // 디파짓 개월
    MIN_TERM(1300, R.string.name_min_term_unit.getStr(), arrayListOf(0, 1, 2, 3, 4, 5), null),    //최소계약기간
    ;

    /**
     *  전체 해제하면 -1로 들어옴. 0은 통과해야 함. 첫번째이기 때문에.
     */
    fun setAmount(digit: Int) {
        amount = if (digit <= 0) null else digit
    }

    fun getResultAmount() = if (amount != null && amount!! > 0) amount else null

    fun addRGint(activity: Activity, parent: LinearLayout, onMore: (RadioButton) -> Unit) {
        parent.addView(ViewRGint(activity, this, onMore))
    }
}


enum class RGstring(val no: Int, val title: String, val list: ArrayList<String>, var selectedIndex: Int) {
    TYPE_CONTRACT(2000, R.string.name_type_contract.getStr(), arrayListOf(ContractType.RENT.title, ContractType.SELL.title, ContractType.DISTRIBUTION.title), 0),
    TYPE_ROOM(3000, R.string.name_type_room.getStr(), arrayListOf(RoomType.CONDO.title, RoomType.HOUSE.title, RoomType.SHOP.title, RoomType.ETC.title), 0),
    TYPE_ROOM_STATUS(4000, R.string.name_type_room_status.getStr(), arrayListOf(RoomStatus.EMPTY.title, RoomStatus.RENTING.title, RoomStatus.NEW.title), 0),
    ;

    fun addRGstring(activity: Activity, parent: LinearLayout) {
        ViewUtil.addViewRGstring(activity, parent, this)
    }

    /**
     * Room.typeNo -> type -> title -> setSelectedIndex
     */
    fun setIndexFromlistIndex(title: String) {
        selectedIndex = getlistIndex(title)
    }

    /**
     * selectedIndex -> list.title -> type -> return typeNo
     */
//    fun getLisdfsdfstIndex(typeNo: Int): Int {
//        list[selectedIndex]
//
//
//        val typeTitle = list[selectedIndex]
//        return getlistIndex(typeTitle)
//    }

    fun getlistIndex(title: String): Int {
        list.forEachIndexed { i, str ->
            if (str == title) return i
        }
        return 0
    }
}


/**
 * 1. Int Picker : min,max 최소값,최대값
 */
/******************************** pickerFactory *****************************************/
enum class PickerFactory(var omitNumber: Int, val titleUnit: Int?, val initPicker: Int, val maxPicker: Int, val min: Int, val max: Int) {
    PESO(100, R.string.unit_peso, 1, 5, 0, 9),
    BIDDING(10, R.string.unit_adcredit, 1, 4, 0, 9),   // 광고올릴때만 : omitNumber가 수시로 변경됨.
    ONE_TWO(1, null, 1, 2, 0, 9),  // 1개짜리
    FLOOR(1, null, 2, 2, 0, 9),    // 층수표시할 때 : 2개짜리
    ;
}


/** * 최소 단위 표시 String */
fun getFormatMinCredit(strID: Int, omitNumber: Int) = String.format(strID.getStr(), omitNumber.getStringComma())

/******************************** reply 타입 *****************************************/
enum class ReplyType(val no: Int) {
    TEXT(1000),
    REPLY(1500),   // 댓글에 답변하기
    TOSS(2000),    // 전달하기
    TOSS_POST(2200),    // post 링크
    TOSS_FLEA(2300),    // flea 링크
    TOSS_ROOM(2400),    // room 링크
    TOSS_STORE(2500),   // store 링크
    LINK(2600),
    PHOTO(3000),
    YOUTUBE(4000),
    ;
}


/******************************** store 타입 *****************************************/
enum class StoreType(val no: Int, val title: String) {

    SHOP(2000, R.string.store_type_shop.getStr()),
    OFFICE_GOVERN(4000, R.string.store_type_government_office.getStr()),

    ;  // 반드시 ;을 getIcon() = iconId.getDrawable()
}

/** * ArrayList 만들기. */
fun ArrayList<String>.setStoreType(): ArrayList<String> {
    StoreType.values().forEach { this.add(it.title) }
    return this
}

fun Int.getStoreType(): StoreType {
    StoreType.values().forEach { if (it.no == this) return it }
    return StoreType.SHOP
}

fun String.getStoreType(): StoreType {
    StoreType.values().forEach { if (it.title == this) return it }
    return StoreType.SHOP
}


///**************************** TagTYPE  ************************/
///**
// * 수정할 때 queryTypes 반드시 체크할 것.
// * queryTypes는 이 값 그대로 es에 적용.
// */
//enum class TagTYPE(val no: Int, val title: String, val queryTypes: String) {
////    ALL(500, R.string.tagtype_all.getStr(), "500"),    // 모든 태그..?
//    BAN(1000, R.string.tagtype_ban.getStr(), "1000"),    // 금기어 욕설, 음란
//    LOC(2000, R.string.tagtype_loc.getStr(), "2000"),    // 전체 통합태그  예) 지역명
//    BOARD(3000, R.string.tagtype_board.getStr(), "2000,3000"),     // from 유저  BOARD 검색할 때만 나옴.
//    POST(4000, R.string.tagtype_post.getStr(), "2000,4000"),     // from 유저  POST 검색할 때만 나옴.
//    FLEA(5000, R.string.tagtype_flea.getStr(), "2000,5000"),       // from 유저  FLEA 검색할 때만 나옴.  물건종류
//    ROOM(6000, R.string.tagtype_room.getStr(), "2000,6000"),      // from 유저  ROOM 검색할 때 나옴.
//    BUILT(7000, R.string.tagtype_built.getStr(), "7000"),     // from 유저  ROOM 검색할 때 나옴.
//    STORE(8000, R.string.tagtype_store.getStr(), "2000,8000"),     // from 유저  STORE 검색할 때 나옴.
//    ;
//}
//
///** * ArrayList 만들기. */
//fun ArrayList<String>.setTagType(): ArrayList<String> {
//    TagTYPE.values().forEach { this.add(it.title) }
//    return this
//}
//
//fun Int.getTagType(): TagTYPE {
//    TagTYPE.values().forEach { if (it.no == this) return it }
//    return TagTYPE.LOC
//}
//
//fun String.getTagType(): TagTYPE {
//    TagTYPE.values().forEach { if (it.title == this) return it }
//    return TagTYPE.LOC
//}

enum class Cntch(val no: Int, val child: String) {

//    WARN(5, CH_WARN_COUNT),

    UP(100, CH_THUMB_UP_COUNT),
    DOWN(200, CH_THUMB_DOWN_COUNT),
    BOOK(300, CH_BOOKMARK_COUNT),
    //    VIEW(400, CH_VIEW_COUNT),
    VIEW_UID(500, CH_VIEW_UID_COUNT),   // 해당 post에 내uid 올리기.

    //    REPLY(600, CH_REPLY_COUNT),
    PUSH(700, CH_PUSH_COUNT),
    ONLINE(800, CH_ONLINE_COUNT),
    ;

    /**     * DB의 최상위 child name     */
    fun getTopChild(any: Any) = getTopChild(any.getMyDataType())

    fun getTopChild(dataType: DataType) =
//            if (this == PUSH) CH_TOTAL_PUSH_COUNT
//            else
            when (dataType) {
                DataType.BOARD -> "board_${child.toLowerCase(Locale.ENGLISH)}"
                DataType.POST -> "post_${child.toLowerCase(Locale.ENGLISH)}"
                DataType.FLEA -> "flea_${child.toLowerCase(Locale.ENGLISH)}"
                DataType.ROOM -> "room_${child.toLowerCase(Locale.ENGLISH)}"
                DataType.STORE -> "store_${child.toLowerCase(Locale.ENGLISH)}"
                else -> child
            }
}

fun Int.getCntch(): Cntch {
    Cntch.values().forEach { if (it.no == this) return it }
    return Cntch.BOOK
}


/********************************  *****************************************/
//@Retention(AnnotationRetention.RUNTIME)
//@IntDef({ NAVI123, NNNN234})
//
//@interface NAVIMODE {
//
//
//}

//
//const val TIP_A = 1
//const val TIP_B = 2
//const val TIP_C = 3
//
//@IntDef(TIP_A, TIP_B, TIP_C)
//@Retention(AnnotationRetention.SOURCE)
//annotation class TipId

//
//const val TEST_ID = 1000
//const val ANONYMOUS = 2000
//const val GOOGLE = 3000
//
//@IntDef(TEST_ID, ANONYMOUS, GOOGLE)
//@Retention(AnnotationRetention.SOURCE)
//annotation class SignFrom
