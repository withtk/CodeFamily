package com.tkip.mycode.model.peer.recommend

import androidx.appcompat.app.AppCompatActivity
import com.tkip.mycode.R
import com.tkip.mycode.dialog.OnDDD
import com.tkip.mycode.dialog.admin.SearchPeerDialog
import com.tkip.mycode.dialog.progress.MyCircle
import com.tkip.mycode.dialog.progress.WAIT_TYPE
import com.tkip.mycode.funs.common.lllogD
import com.tkip.mycode.init.EES_RECOMMEND
import com.tkip.mycode.init.base.getStr
import com.tkip.mycode.model.cost.Cost
import com.tkip.mycode.model.my_enum.ActType
import com.tkip.mycode.model.peer.isAnonymous
import com.tkip.mycode.model.peer.isPenalty
import com.tkip.mycode.model.peer.myuid
import com.tkip.mycode.util.lib.retrofit2.ESget
import com.tkip.mycode.util.tools.fire.*


class OnRecommend {

    companion object {

        /**
         * 1. 추천인 등록 여부 체크
         * 2. 추천인 검색 다이얼로그
         */
        fun checkRecommend(activity: AppCompatActivity) {
            if (isAnonymous(activity)) return
            if (isPenalty(activity)) return

            ESget.getRecommend(myuid(), success = { uidDate1 ->
                if (uidDate1 == null) {

                    /*** 유저 선택 다이얼로그 */
                    SearchPeerDialog.newInstance { peer1 ->
                        lllogD("checkRecommend SearchPeerDialog   peer1 $peer1")

                        WAIT_TYPE.FULL_CIRCLE.show(activity)

                        batch().apply {
                            val uidDate = UidDate(peer1)
                            set(docRef(EES_RECOMMEND, uidDate.key), uidDate)
                            addCost(Cost.new(null, ActType.RECOMMENDED, null))
                            addCost(Cost.new(uidDate.targetUID, ActType.RECOMMEND, null))

                        }.myCommit("addRecommend",
                                success = {
                                    MyCircle.cancel()
                                    OnDDD.info(activity, String.format(R.string.info_completed_recommender.getStr(), peer1.nick))
                                }, fail = {})

                    }.show(activity.supportFragmentManager, "dialog")

                } else OnDDD.info(activity, String.format(R.string.info_recommend_already_done.getStr(), uidDate1.targetNick))   // 이미 추천인 등록됨.

            }, fail = {})


        }

    }
}