package com.tkip.mycode.model.cost.credit

import android.os.Bundle
import androidx.annotation.LayoutRes
import com.tkip.mycode.R
import com.tkip.mycode.admin.menu.AMenu
import com.tkip.mycode.databinding.ActivityCreditMenuBinding
import com.tkip.mycode.dialog.alert.MyAlert
import com.tkip.mycode.dialog.toast.toastNormal
import com.tkip.mycode.funs.common.remoteBool
import com.tkip.mycode.funs.custom.menu.ViewMenuLine
import com.tkip.mycode.funs.custom.toolbar.ViewToolbarEmpty
import com.tkip.mycode.init.V_TYPE1
import com.tkip.mycode.init.base.getStr
import com.tkip.mycode.init.my_bind.BindingActivity
import com.tkip.mycode.model.cost.OnCost
import com.tkip.mycode.model.peer.isAdmin
import com.tkip.mycode.model.peer.isNotValidDateNoDialog
import com.tkip.mycode.model.peer.isPenaltyNoDialog
import com.tkip.mycode.model.peer.mypeer
import com.tkip.mycode.model.ticket.OnBill
import com.tkip.mycode.model.ticket.OnTicket

/**
 */
class CreditMenuActivity : BindingActivity<ActivityCreditMenuBinding>() {
    @LayoutRes
    override fun getLayoutResId() = R.layout.activity_credit_menu

    private lateinit var viewToolbar: ViewToolbarEmpty

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        initView()
    }

    private fun initView() {
        b.activity = this

        /** Toolbar */
        viewToolbar = ViewToolbarEmpty(this@CreditMenuActivity,
                R.string.title_credit_menu.getStr(),
                onBack = { onBackPressed() },
                onMore = null
        ).apply { this@CreditMenuActivity.b.frameToolbar.addView(this) }


        /** 메뉴 리스트 */
        b.lineCover.addView(ViewMenuLine(this@CreditMenuActivity, null, R.string.menu_credit_my_list.getStr(), R.string.menu_credit_my_list_desc.getStr(), onClick = {
            OnCost.openDialog(this@CreditMenuActivity, mypeer())
//            OnCost.gotoCostListActivity(this@CreditMenuActivity, false, myuid(), null)
        }))
        b.lineCover.addView(ViewMenuLine(this@CreditMenuActivity, null, R.string.menu_credit_convert.getStr(), R.string.menu_credit_convert_desc.getStr(), onClick = { MyAlert.convertCredit(this@CreditMenuActivity, ok = {}) }))
        b.lineCover.addView(ViewMenuLine(this@CreditMenuActivity, null, R.string.menu_credit_send.getStr(), R.string.menu_credit_send_desc.getStr(), onClick = { developing() }))
        b.lineCover.addView(ViewMenuLine(this@CreditMenuActivity, null, R.string.menu_free_credit_ad.getStr(), R.string.menu_free_credit_ad_desc.getStr(), onClick = { developing() }))

        /** 메뉴 : 결제내역 */
        if (isAdmin()) b.lineCover.addView(ViewMenuLine(this@CreditMenuActivity, null, AMenu.BILL_LIST, onClick = { OnBill.gotoBillListActivity(this@CreditMenuActivity, null) }))

        /** 메뉴 : 크레딧 구매 */
        if (!isPenaltyNoDialog() && !isNotValidDateNoDialog()   ) b.lineCover.addView(ViewMenuLine(this@CreditMenuActivity, V_TYPE1, AMenu.CREDIT_PURCHASE, onClick = { OnTicket.gotoPurchaseActivity(this@CreditMenuActivity) }))

    }


    private fun developing() {
        toastNormal(R.string.info_prepare_developing)
    }


}