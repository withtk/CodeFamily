package com.tkip.mycode.model.manage

import android.os.Parcel
import android.os.Parcelable
import com.tkip.mycode.R
import com.tkip.mycode.admin.systemMan
import com.tkip.mycode.dialog.toast.TOAST_NORMAL
import com.tkip.mycode.dialog.toast.checkAndToast
import com.tkip.mycode.init.SYSTEM_MAN_KEY

/**
 * SystemManage 시스템 실시간 스위치
 *       :  MainActivity 이후 로직에만 해당됨.
 *
 */
data class SystemMan(

        var key: String = SYSTEM_MAN_KEY,    // 변경하면 안 됨.

        /*****************  시스템 체크  ***********************/
//        var mainTitle: String? = R.string.app_name.getStr(),
//        var mainComment: String? = R.string.app_comment.getStr(),

        var appUrl: String = "https://play.google.com/store/apps/details?id=com.tkip.phcode2",
        var verLatest: Int = 0,
        var verLatestName: String = "verLatestName",
        var verForce: Int = 0,
        var verForceName: String = "verForceName",
        var esUrl: String = "http://34.64.216.36:9200/",
        var esId: String = "SDFwefvc234#@#!@%fd",
        var esPw: String = "OPJL$&%^ewv12v56h",

        /*****************  실시간 공지사항  : 어플 점검시간 ***********************/
        var noticeShow: Boolean = true,
        var noticeNo: Int = 0,
        var noticeTitle: String? = null,
        var noticeComment: String? = null,
        var systemOn: Boolean = true,    // 중요! 어플 강제종료 : 다이얼로그 취소 금지

        /*****************  입찰 스위치  true = 입찰불가  ***********************/
        var noAdAll: Boolean = false,
        var noAdMain: Boolean = false,
        var noAdMenu: Boolean = false,
        var noAdTop: Boolean = false,

        /*****************  등록허가제 true = on   ***********************/
        var permitStore: Boolean = false,
        var permitBoard: Boolean = false,
        var permitPost: Boolean = false,
        var permitFlea: Boolean = false,
        var permitRoom: Boolean = false


) : Parcelable {

    /**     * clone    */
    @Suppress("UNCHECKED_CAST")
    constructor(systemMan: SystemMan) : this(
            key = systemMan.key,

            appUrl = systemMan.appUrl,
            verLatest = systemMan.verLatest,
            verLatestName = systemMan.verLatestName,
            verForce = systemMan.verForce,
            verForceName = systemMan.verForceName,
            esUrl = systemMan.esUrl,
            esId = systemMan.esId,
            esPw = systemMan.esPw,

            noticeNo = systemMan.noticeNo,
            noticeTitle = systemMan.noticeTitle,
            noticeComment = systemMan.noticeComment,
            systemOn = systemMan.systemOn,

            permitStore = systemMan.permitStore,
            permitBoard = systemMan.permitBoard,
            permitPost = systemMan.permitPost,
            permitFlea = systemMan.permitFlea,
            permitRoom = systemMan.permitRoom


    )

    /**     * notice 변경.     */
    fun notice(show: Boolean, no: Int, title: String?, comment: String?, onOff: Boolean) {
        noticeShow = show
        noticeNo = no
        noticeTitle = title
        noticeComment = comment
        systemOn = onOff
    }

    constructor(source: Parcel) : this(
            source.readString()!!,
            source.readString()!!,
            source.readInt(),
            source.readString()!!,
            source.readInt(),
            source.readString()!!,
            source.readString()!!,
            source.readString()!!,
            source.readString()!!,
            1 == source.readInt(),
            source.readInt(),
            source.readString(),
            source.readString(),
            1 == source.readInt(),
            1 == source.readInt(),
            1 == source.readInt(),
            1 == source.readInt(),
            1 == source.readInt(),
            1 == source.readInt(),
            1 == source.readInt(),
            1 == source.readInt(),
            1 == source.readInt(),
            1 == source.readInt()
    )

    override fun describeContents() = 0

    override fun writeToParcel(dest: Parcel, flags: Int) = with(dest) {
        writeString(key)
        writeString(appUrl)
        writeInt(verLatest)
        writeString(verLatestName)
        writeInt(verForce)
        writeString(verForceName)
        writeString(esUrl)
        writeString(esId)
        writeString(esPw)
        writeInt((if (noticeShow) 1 else 0))
        writeInt(noticeNo)
        writeString(noticeTitle)
        writeString(noticeComment)
        writeInt((if (systemOn) 1 else 0))
        writeInt((if (noAdAll) 1 else 0))
        writeInt((if (noAdMain) 1 else 0))
        writeInt((if (noAdMenu) 1 else 0))
        writeInt((if (noAdTop) 1 else 0))
        writeInt((if (permitStore) 1 else 0))
        writeInt((if (permitBoard) 1 else 0))
        writeInt((if (permitPost) 1 else 0))
        writeInt((if (permitFlea) 1 else 0))
        writeInt((if (permitRoom) 1 else 0))
    }

    companion object {


        @JvmField
        val CREATOR: Parcelable.Creator<SystemMan> = object : Parcelable.Creator<SystemMan> {
            override fun createFromParcel(source: Parcel): SystemMan = SystemMan(source)
            override fun newArray(size: Int): Array<SystemMan?> = arrayOfNulls(size)
        }
    }
}