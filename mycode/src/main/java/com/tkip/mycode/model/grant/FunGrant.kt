package com.tkip.mycode.model.grant

import com.tkip.mycode.funs.common.lllogD
import com.tkip.mycode.model.my_enum.Status
import com.tkip.mycode.model.peer.myuid
import com.tkip.mycode.util.tools.date.rightNow


/*** 처리 **********************************************************************/
fun Grant.convert(isHandle: Boolean, any: Any?, message: String?): Grant {
    lllogD("funGrant convert1 Grant $this")
    return if (isHandle) this.handle(any, message)
    else this.request(any, message)
//    lllogD("funGrant convert  Grant $this")
}


/** * 승인 request 작성 */
private fun Grant.request(any: Any?, comment1: String?) =
        this.apply {
            setAnyToGrant(any)
            push += 1
            comment = comment1
            addDate = rightNow()
            statusNo = Status.GRANT_REQUESTED.no
            lllogD("funGrant request  Grant $this")
        }

/** * 승인 handle 작성 */
private fun Grant.handle(any: Any?, reason1: String?) =
        this.apply {
            setAnyToGrant(any)
            adminUID = myuid()
            reason = reason1
            handleDate = rightNow()
            lllogD("funGrant handle  Grant $this")
        }


/** * 승인 handle 작성 */
fun Grant.handle(isStatusOk: Boolean, isPush: Boolean, reason1: String?) =
        this.apply {
            adminUID = myuid()
            reason = reason1
            handleDate = rightNow()
            lllogD("funGrant handle isPush $isPush Grant push ${this.push}")
            if (isPush) this.push += 1
            statusNo = if (isStatusOk) Status.GRANT_HANDLED.no else Status.GRANT_REQUESTED.no
//            isStatusOk?.let { if (it) Status.GRANT_HANDLED.no else Status.GRANT_REQUESTED.no }
            lllogD("funGrant handle  Grant $this")
        }
