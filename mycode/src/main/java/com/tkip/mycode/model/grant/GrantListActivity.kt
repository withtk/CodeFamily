package com.tkip.mycode.model.grant

import android.os.Bundle
import android.view.MenuItem
import androidx.annotation.LayoutRes
import com.tkip.mycode.R
import com.tkip.mycode.databinding.ActivityGrantListBinding
import com.tkip.mycode.funs.custom.toolbar.ViewToolbarEmpty
import com.tkip.mycode.funs.search.model.CustomListGrant
import com.tkip.mycode.init.base.getStr
import com.tkip.mycode.init.my_bind.BindingActivity

/**
 */
class GrantListActivity : BindingActivity<ActivityGrantListBinding>() {
    @LayoutRes
    override fun getLayoutResId() = R.layout.activity_grant_list

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        initView()
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            android.R.id.home -> onBackPressed()
        }
        return super.onOptionsItemSelected(item)
    }

    private fun initView() {
        b.activity = this

        /** Toolbar */
        val title =   R.string.admin_title_grant_list.getStr()
        ViewToolbarEmpty(this@GrantListActivity, title,
                onBack = { onBackPressed() },
                onMore = null
        ).apply { this@GrantListActivity.b.frameToolbar.addView(this) }


        /** list */
        val customSearchWarn = CustomListGrant(this@GrantListActivity,  null)
        b.frame.addView(customSearchWarn)

        customSearchWarn.getEsDataList(0)

    }


}