package com.tkip.mycode.model.push

import android.os.Parcel
import android.os.Parcelable
import com.tkip.mycode.init.base.createNanoKey
import com.tkip.mycode.util.tools.date.rightNow

/**
 * peer/uid/pushKey
 * 유저가 받은 푸쉬알림
 *     업로드 in functions ->  새 포스트, 새 댓글, 광고유찰, 신고당함
 *     업로드 in own Phone->  패널티 적용, 공지사항
 */
data class Push(

        var key: String = createNanoKey(),
        var pushTypeNo: Int = PushType.NOTICE.no,
        var push: Boolean = false,   // push 날릴 지 여부.

        ////////////////// about target peer/////////////////
        var uid: String = "targetUid",
        var fcmToken: String = "fcmToken",

        ////////////////// about target Model/////////////////
        var dataTypeNo: Int? = null,   // reply일때는 POST
        var modelKey: String? = null,  // reply일때는 POST
        var modelTitle: String? = null,
        var photoThumb: String? = null,

        var addDate: Long = rightNow()

) : Parcelable {

    constructor(source: Parcel) : this(
            source.readString()!!,
            source.readInt(),
            1 == source.readInt(),
            source.readString()!!,
            source.readString()!!,
            source.readValue(Int::class.java.classLoader) as Int?,
            source.readString(),
            source.readString(),
            source.readString(),
            source.readLong()
    )

    override fun describeContents() = 0

    override fun writeToParcel(dest: Parcel, flags: Int) = with(dest) {
        writeString(key)
        writeInt(pushTypeNo)
        writeInt((if (push) 1 else 0))
        writeString(uid)
        writeString(fcmToken)
        writeValue(dataTypeNo)
        writeString(modelKey)
        writeString(modelTitle)
        writeString(photoThumb)
        writeLong(addDate)
    }

    companion object {
        @JvmField
        val CREATOR: Parcelable.Creator<Push> = object : Parcelable.Creator<Push> {
            override fun createFromParcel(source: Parcel): Push = Push(source)
            override fun newArray(size: Int): Array<Push?> = arrayOfNulls(size)
        }
    }
}
