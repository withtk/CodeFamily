package com.tkip.mycode.model.reply

import android.os.Parcel
import android.os.Parcelable
import com.tkip.mycode.init.FLEA_BOARD_KEY
import com.tkip.mycode.init.ROOM_BOARD_KEY
import com.tkip.mycode.init.base.createKey
import com.tkip.mycode.model.flea.Flea
import com.tkip.mycode.model.my_enum.DataType
import com.tkip.mycode.model.my_enum.ReplyType
import com.tkip.mycode.model.my_enum.Status
import com.tkip.mycode.model.peer.mynick
import com.tkip.mycode.model.peer.myuid
import com.tkip.mycode.model.post.Post
import com.tkip.mycode.model.room.Room
import com.tkip.mycode.util.tools.date.rightNow

data class Reply(

        var key: String = createKey(),
        var authorUID: String = myuid(),
        var authorNick: String = mynick(),
        var anonymous: Boolean = false,       // 익명 업로드 - 포인트 필요.

        var modelKey: String = "modelKey",    // 댓글이 달린 post, flea, room
        var modelTitle: String? = "modelTitle",
        var boardKey: String = "boardKey",    // 댓글이 달린 post, flea, room 의 board
        var boardName: String = "boardName",
        var replyTypeNo: Int = ReplyType.TEXT.no,
        var dataTypeNo: Int = DataType.POST.no,    //  ex) post, flea, room, store  : functions에서 사용.

        var comment: String? = null,      // 수정불가. 원래 한번 뱉은 말은 수정이 안 된다. 삭제는 가능.
        var thumbUrl: String? = null,     // 사진 올리기.
        var url: String? = null,          // 사진 올리기.
        var chosen: Boolean = false,      // Reward받은 reply.

        var notRead: Int? = null,         // 봤는지여부 체크하는 int.


        /////////////////TOSS/////////////////
        var tossModelKey: String? = null,
        var tossDataTypeNo: Int? = null,
        //////////////////////////////////////

        var thumbUpCount: Int? = null,     // 썸업을 준 횟수.
        var thumbDownCount: Int? = null,    // 썸다운을 횟수.
        var warnCount: Int? = null,       // 신고 횟수.
        var totalPoint: Long? = null,   // OnPeer.peer.activityPoint : author의 레벨 + thumbUp - thumbDown

        /////////////////rereply//////////////////
        var earlierKey: String? = null,  // 재리플의 경우만 해당. reply.key

        var statusNo: Int = Status.NORMAL.no,
        var addDate: Long = rightNow()

) : Parcelable {
    constructor() : this(createKey())   // adapter에서 사용.

    constructor(post: Post, replyType: ReplyType, comment: String?) : this(
            modelKey = post.key,
            modelTitle = post.title,
            boardKey = post.boardKey,
            boardName = post.boardName,
            dataTypeNo = DataType.POST.no,
            replyTypeNo = replyType.no,
            comment = comment
    )

    constructor(flea: Flea, replyType: ReplyType, comment: String?) : this(
            modelKey = flea.key,
            modelTitle = flea.title,
            boardKey = FLEA_BOARD_KEY,
            boardName = FLEA_BOARD_KEY,
            dataTypeNo = DataType.FLEA.no,
            replyTypeNo = replyType.no,
            comment = comment
    )

    constructor(room: Room, replyType: ReplyType, comment: String?) : this(
            modelKey = room.key,
            modelTitle = room.local,
            boardKey = ROOM_BOARD_KEY,
            boardName = ROOM_BOARD_KEY,
            dataTypeNo = DataType.ROOM.no,
            replyTypeNo = replyType.no,
            comment = comment
    )

    constructor(source: Parcel) : this(
            source.readString()!!,
            source.readString()!!,
            source.readString()!!,
            1 == source.readInt(),
            source.readString()!!,
            source.readString(),
            source.readString()!!,
            source.readString()!!,
            source.readInt(),
            source.readInt(),
            source.readString(),
            source.readString(),
            source.readString(),
            1 == source.readInt(),
            source.readValue(Int::class.java.classLoader) as Int?,
            source.readString(),
            source.readValue(Int::class.java.classLoader) as Int?,
            source.readValue(Int::class.java.classLoader) as Int?,
            source.readValue(Int::class.java.classLoader) as Int?,
            source.readValue(Int::class.java.classLoader) as Int?,
            source.readValue(Long::class.java.classLoader) as Long?,
            source.readString(),
            source.readInt(),
            source.readLong()
    )

    override fun describeContents() = 0

    override fun writeToParcel(dest: Parcel, flags: Int) = with(dest) {
        writeString(key)
        writeString(authorUID)
        writeString(authorNick)
        writeInt((if (anonymous) 1 else 0))
        writeString(modelKey)
        writeString(modelTitle)
        writeString(boardKey)
        writeString(boardName)
        writeInt(replyTypeNo)
        writeInt(dataTypeNo)
        writeString(comment)
        writeString(thumbUrl)
        writeString(url)
        writeInt((if (chosen) 1 else 0))
        writeValue(notRead)
        writeString(tossModelKey)
        writeValue(tossDataTypeNo)
        writeValue(thumbUpCount)
        writeValue(thumbDownCount)
        writeValue(warnCount)
        writeValue(totalPoint)
        writeString(earlierKey)
        writeInt(statusNo)
        writeLong(addDate)
    }

    companion object {
        /**
         * model에 맞게 생성
         */
        fun create(any: Any, replyType: ReplyType, comment: String?): Reply =
                when (any) {
                    is Post -> Reply(any, replyType, comment)
                    is Flea -> Reply(any, replyType, comment)
                    is Room -> Reply(any, replyType, comment)
                    else -> Reply()
                }

        @JvmField
        val CREATOR: Parcelable.Creator<Reply> = object : Parcelable.Creator<Reply> {
            override fun createFromParcel(source: Parcel): Reply = Reply(source)
            override fun newArray(size: Int): Array<Reply?> = arrayOfNulls(size)
        }
    }
}