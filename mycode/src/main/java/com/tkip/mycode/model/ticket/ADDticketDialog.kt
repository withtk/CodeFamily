package com.tkip.mycode.model.ticket

import android.app.Dialog
import android.content.Context
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AlertDialog
import com.tkip.mycode.R
import com.tkip.mycode.databinding.DialogAddTicketBinding
import com.tkip.mycode.dialog.OnDDD
import com.tkip.mycode.dialog.progress.WAIT_TYPE
import com.tkip.mycode.dialog.toast.toastNormal
import com.tkip.mycode.funs.common.getNullStr
import com.tkip.mycode.funs.common.getStr
import com.tkip.mycode.funs.common.textListener
import com.tkip.mycode.init.PUT_TICKET
import com.tkip.mycode.init.base.isNullBlankAndSetError
import com.tkip.mycode.init.inter.interDialogBasicClick
import com.tkip.mycode.init.my_bind.BindingDialog
import com.tkip.mycode.model.my_enum.STATUS_LIST_TICKET
import com.tkip.mycode.model.peer.isNotAdmin
import com.tkip.mycode.util.lib.photo.view.LinearAddPhotoOne
import com.tkip.mycode.util.tools.date.rightNow
import com.tkip.mycode.util.tools.etc.OnMyClick
import com.tkip.mycode.util.tools.fire.Firee
import com.tkip.mycode.util.tools.spinner.LinearSpinnerStatus
import kotlin.math.ceil


class ADDticketDialog : BindingDialog<DialogAddTicketBinding>() {
    override fun getLayoutResId(): Int = R.layout.dialog_add_ticket
    override fun getSize(): Pair<Float?, Float?> = Pair(0.95f, null)

    private val ticket by lazy { arguments?.getParcelable<Ticket>(PUT_TICKET) ?: Ticket() }

    companion object {
        @JvmStatic
        fun newInstance(ticket: Ticket?) = ADDticketDialog().apply { arguments = Bundle().apply { putParcelable(PUT_TICKET, ticket) } }
    }

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        if(isNotAdmin(null,null)) dismiss()

        b.ticket = ticket
        b.inter = interDialogBasicClick(
                onDel = { onClickDelete(it) },
                onCancel = { onClickCancel(it) },
                onOk = { onClickOk(it) })

        /**         * Status 스피너     */
        b.lineSpin.addView(LinearSpinnerStatus(mContext, true, STATUS_LIST_TICKET, ticket.statusNo, onClick = { it.let { ticket.statusNo = it.no } }))


        b.etSku.setOnFocusChangeListener { _, isOn -> if (!isOn) b.etSku.isNullBlankAndSetError(b.laySku, R.string.error_add_ticket_sku) }

        b.etCrd.textListener(after = { s -> if (!s.isNullOrEmpty()) resetCalculate() }, before = { _, _, _, _ -> }, on = { _, _, _, _ -> })   // crd 바뀔때마다 전체값 수정.
        b.etRate.textListener(after = { s -> if (!s.isNullOrEmpty()) resetCalculate() }, before = { _, _, _, _ -> }, on = { _, _, _, _ -> })   // rate 바뀔때마다 전체값 수정.


        /**         * 사진 1장 올리기 박스         */
        viewAddPhotoOne = LinearAddPhotoOne(mContext, ticket.photoThumb, ticket.photoUrl,
                onDelete = { },
                onGallery = { onClickOpenGallery() })
        b.frameAddPhotoOne.addView(viewAddPhotoOne)


        val builder = AlertDialog.Builder(activity as Context).apply {
            setView(b.root)
            setCancelable(true)
        }

        return builder.create().apply {
            setCanceledOnTouchOutside(true)
            window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        }

    }


    fun onClickDelete(v: View) {
        OnMyClick.setDisableAWhileBTN(v)
        OnDDD.delete(mContext, ok = {
            Firee.delTicket(ticket, success = {
                toastNormal(R.string.complete_delete)
                dismiss()
            }, fail = {})
        }, cancel = {})
    }

    fun onClickCancel(v: View) {
        OnMyClick.setDisableAWhileBTN(v)
        this.dialog?.dismiss()
    }

    /**     * 실시간 계산     */
    private fun resetCalculate() {
        val crd = b.etCrd.getNullStr()?.toInt() ?: 0
        b.etRate.getNullStr()?.let {
            val bonusPercent = b.etRate.getNullStr()?.toDouble() ?: 0.0
            val rate = (bonusPercent / 100)

            val cal = crd * rate
            val extra = ceil(cal).toInt()
            b.etExtra.setText(extra.toString())
            b.etPnt.setText((crd + extra).toString())
        }
    }


    fun onClickOk(v: View) {
        OnMyClick.setDisableAWhileBTN(v)

        if (b.etSku.isNullBlankAndSetError(b.laySku, R.string.error_add_ticket_sku)) return
        if (b.etWon.isNullBlankAndSetError(b.laySku, R.string.error_add_ticket_won)) return

        ticket.apply {

            skuId = b.etSku.getStr()
            won = b.etWon.getNullStr()?.toInt()
            order = b.etOrder.getNullStr()?.toInt()

            crd = b.etCrd.getNullStr()?.toInt()
            bonusPercent = b.etRate.getNullStr()?.toInt()
            pnt = b.etPnt.getNullStr()?.toInt()
            extra = b.etExtra.getNullStr()?.toInt()
            bcrd = b.etBcrd.getNullStr()?.toInt()

            /*** status는 이미 기입.    */

            addDate = rightNow()
        }

        OnDDD.confirm(mContext, R.string.status_dialog_confirm_ticket, ok = {
            WAIT_TYPE.FULL_AV.show(mContext)
            OnTicket.addPhotoAndTicket(mContext, ticket, success = { dismiss() }, fail = {})
        }, cancel = {})

    }


}