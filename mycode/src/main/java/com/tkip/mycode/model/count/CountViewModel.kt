package com.tkip.mycode.model.count

import android.util.Log
import android.view.View
import android.widget.CheckBox
import androidx.lifecycle.*
import com.tkip.mycode.funs.common.lllogD
import com.tkip.mycode.funs.common.lllogI
import com.tkip.mycode.init.base.Cons
import com.tkip.mycode.model.my_enum.DataType
import com.tkip.mycode.model.my_enum.anyKey
import com.tkip.mycode.model.my_enum.getMyDataType
import com.tkip.mycode.model.peer.myuid
import com.tkip.mycode.model.viewmodel.ListLiveData
import com.tkip.mycode.util.lib.retrofit2.ESget
import com.tkip.mycode.util.lib.retrofit2.ESquery
import com.tkip.mycode.util.tools.fire.real

class CountViewModel : ViewModel() {

    val liveCountList = ListLiveData<Count>()

    val liveViewUid = MutableLiveData<Count>()
    val liveThumbUp = MutableLiveData<Count>()
    val liveThumbDown = MutableLiveData<Count>()
    val liveBookmark = MutableLiveData<Count>()
    val livePush = MutableLiveData<Count>()


    /**
     *   bookmark, push
     */
    fun getEsCountList(index: String, size: Int, from: Int, uid: String): ListLiveData<Count> {

        val query = ESquery.countUID(size, from, uid)
        Log.d(Cons.TAG, "liveCountList $query")

        ESget.getCountList("CountViewModel getEsCountList ", index, query,
                success = {
                    if (it == null) {
                        liveCountList.clear(true)
                    } else {
                        if (from == 0) liveCountList.clear(false)
                        liveCountList.addAll(it)
                    }
                }, fail = {})
        return liveCountList
    }


    /********************************************************************************************
     ********************************* Count ***************************************************/
    fun initCount(owner: LifecycleOwner, any: Any, arr: Array<CountType>, cbUP: CheckBox?, cbDOWN: CheckBox?, cbBOOK: CheckBox?, cbPUSH: CheckBox?) {
        addViewCountRealDB(any)  // VIEW_COUNT 추가
        getAllEsCount(any, arr)
//        liveViewUid.observe(owner, Observer { if (it == null) OnCount.addCount(any, CountType.VIEW_UID) })   // viewUid 없으면 새로 넣어주기.
        cbUP?.let { liveThumbUp.observe(owner, Observer { cbUP.setCountChange(it) }) }
        cbDOWN?.let { liveThumbDown.observe(owner, Observer { cbDOWN.setCountChange(it) }) }
        cbBOOK?.let { liveBookmark.observe(owner, Observer { cbBOOK.setCountChange(it) }) }
        cbPUSH?.let { livePush.observe(owner, Observer { cbPUSH.setCountChange(it) }) }
    }

    /** * Count로 인한 CHECKBOX 변경 */
    private fun CheckBox.setCountChange(count: Count?) {
        this.isChecked = count != null
        this.isEnabled = true
    }

    /**
     * ViewCount올리기 : post, flea, room
     */
    private fun addViewCountRealDB(any: Any) {
        when (any.getMyDataType()) {
            DataType.BOARD -> return   // board 는 viewCount 안 올려.
            else -> CountMini(any).real()
        }
//        if(any.getDataType() == DataType.BOARD) return   // board는 viewCount 안 올려.
//        CountMini(any).real()
//        Count.createForVIEW(any, CountType.VIEW).real()
    }

    fun getAllEsCount(any: Any, arr: Array<CountType>) {
        arr.forEach { getEsCount(any, it) }
    }

    private fun getEsCount(any: Any, countType: CountType): LiveData<Count> {
        val index = countType.getTopChild(any)
        lllogI("countViewModel getEsCount index $index")
        val key = countType.name + myuid() + any.anyKey()
        ESget.getCount(index, key, success = { count -> countType.setLiveCount(count) }, fail = { lllogI("getcount getcount #$#$@@$#!@#@#$@#!$#@$##@$#@@#$") })
        return countType.returnLiveCount()
    }

    fun updateCount(any: Any, countType: CountType, v: View) {
        v as CheckBox
        countType.updateCount(any, countType.getCount(), v, success = { count1 -> countType.setLiveCount(count1) }, fail = {})
    }

    /*****************************************************************************************
     * count check
     *****************************************************************************************/
    private fun CountType.returnLiveCount() =
            when (this) {
                CountType.VIEW_UID -> liveViewUid
                CountType.UP -> liveThumbUp
                CountType.DOWN -> liveThumbDown
                CountType.BOOK -> liveBookmark
                CountType.PUSH -> livePush
                else -> liveThumbUp
            }

    private fun CountType.setLiveCount(count: Count?) {
        when (this) {
            CountType.VIEW_UID -> liveViewUid.value = count
            CountType.UP -> liveThumbUp.value = count
            CountType.DOWN -> liveThumbDown.value = count
            CountType.BOOK -> liveBookmark.value = count
            CountType.PUSH -> livePush.value = count
            else -> liveThumbDown.value = count
        }
    }


    private fun CountType.getCount() =
            when (this) {
                CountType.VIEW_UID -> liveViewUid.value
                CountType.UP -> liveThumbUp.value
                CountType.DOWN -> liveThumbDown.value
                CountType.BOOK -> liveBookmark.value
                CountType.PUSH -> livePush.value
                else -> null
            }


}