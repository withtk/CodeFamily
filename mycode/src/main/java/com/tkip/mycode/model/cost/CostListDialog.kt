package com.tkip.mycode.model.cost

import android.app.Dialog
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import com.tkip.mycode.R
import com.tkip.mycode.databinding.DialogCostListBinding
import com.tkip.mycode.funs.search.model.CustomListCost
import com.tkip.mycode.init.PUT_PEER
import com.tkip.mycode.init.my_bind.BindingDialog
import com.tkip.mycode.model.my_enum.CostType
import com.tkip.mycode.model.peer.Peer

/**
 * 나의 크레딧 사용 리스트
 */
class CostListDialog : BindingDialog<DialogCostListBinding>() {
    override fun getLayoutResId(): Int = R.layout.dialog_cost_list
    override fun getSize(): Pair<Float?, Float?> = Pair(0.95f, null)

    private val peer by lazy { arguments?.getParcelable<Peer>(PUT_PEER) }
    private lateinit var customSearchCost: CustomListCost

    companion object {
        @JvmStatic
        fun newInstance(peer: Peer) = CostListDialog().apply { arguments = Bundle().apply { putParcelable(PUT_PEER, peer) } }
    }

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {

        initView()
        val builder = AlertDialog.Builder(activity as AppCompatActivity).apply {
            setView(b.root)
            setCancelable(true)
        }

        return builder.create().apply {
            setCanceledOnTouchOutside(true)
            window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        }
    }

    private fun initView() {
        b.dialog = this
        peer ?: dismiss()
        b.inMyCredit.peer = peer


        /*** add List */
        customSearchCost = CustomListCost(mContext, null, CostType.PAYMENT,  CostType.CONVERT,  null)
        b.frame.addView(customSearchCost)


        /*** 초기 리스트 */
        customSearchCost.getEsDataList(0, null)
    }


}
