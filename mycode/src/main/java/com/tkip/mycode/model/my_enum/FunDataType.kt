package com.tkip.mycode.model.my_enum

import android.app.PendingIntent
import android.content.Context
import android.view.View
import com.tkip.mycode.R
import com.tkip.mycode.dialog.toast.toastNormal
import com.tkip.mycode.funs.common.lllogD
import com.tkip.mycode.funs.common.lllogM
import com.tkip.mycode.init.*
import com.tkip.mycode.init.base.getClr
import com.tkip.mycode.init.base.getStr
import com.tkip.mycode.model.board.Board
import com.tkip.mycode.model.board.OnBoard
import com.tkip.mycode.model.flea.Flea
import com.tkip.mycode.model.flea.OnFlea
import com.tkip.mycode.model.post.OnPost
import com.tkip.mycode.model.post.Post
import com.tkip.mycode.model.room.OnRoom
import com.tkip.mycode.model.room.Room
import com.tkip.mycode.model.store.Store
import com.tkip.mycode.nav.store.OnStore
import com.tkip.mycode.util.lib.retrofit2.ESget
import com.tkip.mycode.util.tools.fire.docRef


/******************************** Model   *****************************************/
/** * functions연동 */
enum class DataType(val no: Int, val child: String, val title: String, val noDataMsg: String?, val colorId: Int) {

    NOTHING(20, EES_NOTHING, R.string.tab_title_nothing.getStr(), null, R.color.gray),
    PEER(50, EES_PEER, R.string.tab_title_peer.getStr(), null, R.color.gray),

    BOARD(100, EES_BOARD, R.string.tab_title_board.getStr(), R.string.info_no_board.getStr(), R.color.md_brown_400),
    STORE(200, EES_STORE, R.string.tab_title_store.getStr(), R.string.info_no_store.getStr(), R.color.md_pink_400),
    POST(300, EES_POST, R.string.tab_title_post.getStr(), R.string.info_no_post.getStr(), R.color.md_green_400),
    FLEA(400, EES_FLEA, R.string.tab_title_flea.getStr(), R.string.info_no_flea.getStr(), R.color.md_deep_orange_300),
    ROOM(500, EES_ROOM, R.string.tab_title_room.getStr(), R.string.info_no_room.getStr(), R.color.md_orange_A100),

    //    LOBBY(700,  EES_LOBBY, R.string.tab_title_lobby.getStr(),null),
    REPLY(800, EES_REPLY, R.string.tab_title_reply.getStr(), null, R.color.colorAccent),
    COUNT(900, CH_COUNT, R.string.tab_title_count.getStr(), null, R.color.colorAccent),
    WARN(950, EES_WARN, R.string.tab_title_warn.getStr(), null, R.color.colorAccent),
    GRANT(980, EES_GRANT, R.string.tab_title_grant.getStr(), null, R.color.colorAccent),
    MYTAG(1000, EES_TAG, R.string.status_mytag.getStr(), null, R.color.colorAccent),
    BID(1050, EES_BID, R.string.status_bid.getStr(), null, R.color.colorAccent),
    BANNER(1060, EES_BANNER, R.string.status_banner.getStr(), null, R.color.colorAccent),
//    COST(1100,   EES_COST, R.string.status_cost.getStr(), null, R.color.colorAccent),

    ALL(2000, "all", "all", null, R.color.gray),
    ;

//    fun getIcon() = iconId.getDrawable()

    fun getModel(key: String, onSuccess: ((Any?) -> Unit)?) {
        when (this) {
            BOARD -> ESget.getBoard(key, { onSuccess?.invoke(it) }, {})
            POST -> ESget.getPost(key, { onSuccess?.invoke(it) }, {})
            FLEA -> ESget.getFlea(key, { onSuccess?.invoke(it) }, {})
            ROOM -> ESget.getRoom(key, { onSuccess?.invoke(it) }, {})
            STORE -> ESget.getStore(key, { onSuccess?.invoke(it) }, {})
            else -> onSuccess?.invoke(null)
        }
    }



    fun checkAndGo(context: Context, isTop: Boolean, modelKey: String, arrPair: Array<androidx.core.util.Pair<View, String>>?, onSuccess: ((Any) -> Unit)?) {
        lllogD("ViewTossModel checkAndGo modelKey $modelKey")
        when (this) {
            BOARD -> OnBoard.checkAndGo(context, isTop, modelKey, arrPair, onSuccess, null)
            POST -> OnPost.checkAndGo(context, isTop, modelKey, Status.NORMAL, arrPair, onSuccess, onNotNormal = { })
            FLEA -> OnFlea.checkAndGo(context, isTop, modelKey, null, Status.NORMAL, arrPair, onSuccess, onNotNormal = { })
            ROOM -> OnRoom.checkAndGo(context, isTop, modelKey, null, Status.NORMAL, arrPair, onSuccess, onNotNormal = { })
            STORE -> OnStore.checkAndGo(context, isTop, modelKey, null, Status.NORMAL, arrPair, onSuccess, onNotNormal = { })
            else -> {
            }
        }
    }

    /*** 상태체크 없이 그냥 고 */
    fun justGo(context: Context, isTop: Boolean, key: String, arrPair: Array<androidx.core.util.Pair<View, String>>?) {
        lllogM("DataType justGo this $this")
        when (this) {
            BOARD -> ESget.getBoard(key, success = {   it?.let { data -> OnBoard.gotoBoardActivity(context, isTop, null, data, arrPair) }?: toastNormal(R.string.error_data_deleted) }, fail = {})
            POST -> ESget.getPost(key, success = { it?.let { data -> OnPost.gotoPostActivity(context, isTop, null, data, arrPair) } ?: toastNormal(R.string.error_data_deleted)}, fail = {})
            FLEA -> ESget.getFlea(key, success = { it?.let { data -> OnFlea.gotoFleaActivity(context, isTop, null, data, arrPair) }?: toastNormal(R.string.error_data_deleted) }, fail = {})
            ROOM -> ESget.getRoom(key, success = { it?.let { data -> OnRoom.gotoRoomActivity(context, isTop, null, data, arrPair) }?: toastNormal(R.string.error_data_deleted) }, fail = {})
            STORE -> ESget.getStore(key, success = { it?.let { data -> OnStore.gotoStoreActivity(context, isTop, null, data, arrPair) }?: toastNormal(R.string.error_data_deleted) }, fail = {})
//            BID -> ESget.getBanner(key, success = { it?.let { data -> OnBanner.openADDBannerDialog((context as AppCompatActivity), ACTION_ADMIN, data, onOk = {}) } }, fail = {})
            else -> {
            }
        }
    }

//    fun showStatusDialog(context: Context, key: String) {
//        when (this) {
//            BOARD, POST, FLEA, ROOM, STORE ->
//                get(key, success = { it.showStatusDialog(context,null) }, fail = {})
//            BANNER -> {
//                get(key, success = {
//                    OnBanner.openADDBannerDialog(context as AppCompatActivity, ACTION_ADMIN, it as? Bid, onOk = { _, g -> notifyItemChanged(adapterPosition, g) })
//                }, fail = {})
//            }
//            else -> {
//            }
//        }
//    }


    /*** 모델 받아오기 */
    fun get(key: String, success: (Any?) -> Unit, fail: () -> Unit) {
        lllogD("ViewTossModel ViewTossModel get this $this key $key")

        when (this) {
            BOARD -> ESget.getBoard(key, success, fail)
            POST -> ESget.getPost(key, success, fail)
            FLEA -> ESget.getFlea(key, success, fail)
            ROOM -> ESget.getRoom(key, success, fail)
            STORE -> ESget.getStore(key, success, fail)
//            BID -> ESget.getBanner(key, success = { it?.let { data -> OnBanner.openADDBannerDialog((context as AppCompatActivity), ACTION_ADMIN, data, onOk = {}) } }, fail = {})
            BANNER -> ESget.getBanner(key, success, fail)
            else -> {
            }
        }
    }


    fun getDocRef(key: String) =
            when (this) {
                BOARD -> docRef(EES_BOARD, key)
                POST -> docRef(EES_POST, key)
                FLEA -> docRef(EES_FLEA, key)
                ROOM -> docRef(EES_ROOM, key)
                STORE -> docRef(EES_STORE, key)
                else -> docRef(EES_BOARD, "0board_null")
            }

    fun emptyList() =
            when (this) {
                BOARD -> arrayListOf<Board>()
                POST -> arrayListOf<Post>()
                FLEA -> arrayListOf<Flea>()
                ROOM -> arrayListOf<Room>()
                STORE -> arrayListOf<Store>()
                else -> arrayListOf<Board>()
            }

    fun statusList() =
            when (this) {
//                BOARD -> STATUS_LIST_BASIC
//                POST -> STATUS_LIST_BASIC
                FLEA -> STATUS_LIST_FLEA
                ROOM -> STATUS_LIST_ROOM
                REPLY -> STATUS_LIST_REPLY
//                STORE -> STATUS_LIST_BASIC
                else -> STATUS_LIST_BASIC
            }

    fun clr() = colorId.getClr()

//
//    fun <T> newAdapter(list:ArrayList<T> ,rvType:Int, onItem: ((Any) -> Unit)?) =
//        when (this) {
//            BOARD ->StoreAdapter(list, StoreAdapter.rvHori,  onItem   )
//            POST ->StoreAdapter(list, StoreAdapter.rvHori,  onItem   )
//            FLEA ->StoreAdapter(list, StoreAdapter.rvHori,  onItem   )
//            ROOM ->StoreAdapter(list, StoreAdapter.rvHori,  onItem   )
//            STORE -> StoreAdapter(list, StoreAdapter.rvHori,  onItem   )
//            else -> StoreAdapter(list, StoreAdapter.rvHori,  onItem   )
//    }


}


fun Int.dataType(): DataType {
    DataType.values().forEach { if (it.no == this) return it }
    return DataType.NOTHING
}


fun String.dataType(): DataType {
    DataType.values().forEach { if (it.title == this) return it }
    return DataType.NOTHING
}

fun DataType.getPendingIntent(context: Context, id: Int, modelKey: String): PendingIntent? =
        when (this) {
            DataType.POST -> PendingIntent.getActivity(context, id, OnPost.getIntent(context, false, null, modelKey), PendingIntent.FLAG_CANCEL_CURRENT)
            DataType.FLEA -> PendingIntent.getActivity(context, id, OnFlea.getIntent(context, false, null, modelKey), PendingIntent.FLAG_CANCEL_CURRENT)
            DataType.ROOM -> PendingIntent.getActivity(context, id, OnRoom.getIntent(context, false, null, modelKey), PendingIntent.FLAG_CANCEL_CURRENT)
            DataType.BOARD -> PendingIntent.getActivity(context, id, OnBoard.getIntent(context, false, null, modelKey), PendingIntent.FLAG_CANCEL_CURRENT)
            else -> null
        }









