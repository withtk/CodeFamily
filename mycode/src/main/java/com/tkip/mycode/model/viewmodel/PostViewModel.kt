package com.tkip.mycode.model.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.google.firebase.firestore.ListenerRegistration
import com.tkip.mycode.funs.common.lllogI
import com.tkip.mycode.init.recentBoard
import com.tkip.mycode.model.board.Board
import com.tkip.mycode.model.board.BoardType
import com.tkip.mycode.model.board.getBoardType
import com.tkip.mycode.model.count.Count
import com.tkip.mycode.model.my_enum.Cntch
import com.tkip.mycode.model.my_enum.Status
import com.tkip.mycode.model.peer.isMe
import com.tkip.mycode.model.post.Post
import com.tkip.mycode.util.lib.retrofit2.ESget
import com.tkip.mycode.util.lib.retrofit2.ESquery
import com.tkip.mycode.util.tools.fire.Firee

class PostViewModel : ViewModel() {

    private var listenerPost: ListenerRegistration? = null

    val livePost = MutableLiveData<Post>()
    val livePostList = ListLiveData<Post>()

    val liveThumbUp = MutableLiveData<Count>()
    val liveThumbDown = MutableLiveData<Count>()
    val liveBookmark = MutableLiveData<Count>()
    val livePush = MutableLiveData<Count>()

    override fun onCleared() {
        super.onCleared()
        listenerPost?.remove()
    }

    fun getPost(postKey: String): LiveData<Post> {
        Firee.getPost(postKey, success = { it?.let { post1 -> livePost.value = post1 } }, fail = {})
        return livePost
    }

    private fun commonGetEs(from: Int, query: String): ListLiveData<Post> {
        ESget.getPostList(query,
                success = {
                    lllogI("myCallll success commonGetEs PostViewModel ")

                    if (it == null) {
                        livePostList.clear(true)
                    } else {
                        if (from == 0) livePostList.clear(false)
                        livePostList.addAll(it)
                    }
                }, fail = {
            lllogI("myCallll fail commonGetEs PostViewModel ")
        })
        return livePostList
    }

    fun getEsPostList(size: Int, from: Int, board: Board, status: Status) {
        val query = when {
            (board  == recentBoard) -> ESquery.postRecentList(size, from)
            (board.boardTypeNo.getBoardType() == BoardType.CS_CENTER) -> {              // CsCENTER
                if (board.authorUID.isMe()) ESquery.postList(size, from, board.key, status)
                else ESquery.postListCsCenter(size, from, board.key, board.authorUID, status)
            }
            else -> ESquery.postList(size, from, board.key, status)   // 그 외의 모드 개별보드.
        }
        commonGetEs(from, query)  // 받아오기.
    }

    fun getEsPostRecentList(size: Int, from: Int) {
        commonGetEs(from, ESquery.postRecentList(size, from))
    }

    /********************************************************************************************
     ********************************* Count ***************************************************/

//    fun addViewUID(post: Post, cnt: Cntch) {
//        Firee.addViewUID(CH_POST, Count.create(post, cnt), {}, {})
//    }

//    fun getAllEsCount(post: Post) {
//        getEsCountList(post, Cntch.UP)
//        getEsCountList(post, Cntch.DOWN)
//        getEsCountList(post, Cntch.BOOK)
//        getEsCountList(post, Cntch.PUSH)
//    }
//
//    private fun getEsCountList(post: Post, cnt: Cntch): LiveData<Count> {
//        ESget.getCountList(
//            cnt.getTopChild(DataType.POST),
//            ESquery.countUidModelKey(post.key),
//            success = {
//                if (it == null || it.isEmpty()) {
//                    setLiveCount(cnt, null)
//                } else {
//                    setLiveCount(cnt, it[0])
//                }
//            },
//            fail = {})
//        return returnLiveCount(cnt)
//    }

//    fun getCount(post: Post, cnt: Cntch): LiveData<Count> {
//        Firee.getCount(
//            cnt.getTopChild(DataType.POST),
//            post.key,
//            success = { setLiveCount(cnt, it) },
//            fail = {})
//        return returnLiveCount(cnt)
//    }

//    fun updateCount(post: Post, cnt: Cntch, v: View) {
//        cnt.updateCount(post, cnt.count(), v, success = { on, count1 ->
//            if (on) setLiveCount(cnt, count1)
//            else setLiveCount(cnt, null)
//        }, fail = {})
//    }


    /**
     * private
     */
    private fun returnLiveCount(cnt: Cntch) =
            when (cnt) {
                Cntch.UP -> liveThumbUp
                Cntch.DOWN -> liveThumbDown
                Cntch.BOOK -> liveBookmark
                Cntch.PUSH -> livePush
                else -> liveThumbUp
            }

    private fun setLiveCount(cnt: Cntch, count: Count?) {
        when (cnt) {
            Cntch.UP -> liveThumbUp.value = count
            Cntch.DOWN -> liveThumbDown.value = count
            Cntch.BOOK -> liveBookmark.value = count
            Cntch.PUSH -> livePush.value = count
            else -> liveThumbDown.value = count
        }
    }

    private fun Cntch.count() =
            when (this) {
                Cntch.UP -> liveThumbUp.value
                Cntch.DOWN -> liveThumbDown.value
                Cntch.BOOK -> liveBookmark.value
                Cntch.PUSH -> livePush.value
                else -> null
            }


//
//    fun hasThumbDown(post: Post): LiveData<Count> {
//        Firee.getCount(Count.getCnt(post, Cntch.DOWN), success = { liveThumbDown.value = it }, fail = {})
////        Firee.commonGET(docRef(CH_POST, postKey, CH_THUMB_DOWN_COUNT, OnPeer.myUid()), success = { liveThumbDown.value = it != null && it.exists() }, fail = { })
//        return liveThumbDown
//    }
//
//    fun hasBookmark(post: Post): LiveData<Count> {
//
//        Firee.getCount(Count.getCnt(post, Cntch.BOOK), success = { liveBookmark.value = it }, fail = {})
////        Firee.commonGET(docRef(CH_POST, postKey, CH_BOOKMARK_COUNT, OnPeer.myUid()), success = { liveBookmark.value = it != null && it.exists() }, fail = { })
//        return liveBookmark
//    }
//
//    fun hasPush(post: Post): LiveData<Count> {
//        Firee.getCount(Count.getCnt(post, Cntch.PUSH), success = { livePush.value = it }, fail = {})
////        Firee.commonGET(docRef(CH_POST, postKey, CH_PUSH_COUNT, OnPeer.myUid()), success = { livePush.value = it != null && it.exists() }, fail = { })
//        return livePush
//    }

    /**   Count 추가     */
//    fun addCount(post: Post, cntCH: Cntch) {
//        lllogD("PostViewModel : addNickPeer  countChild")
//        Firee.commonADD(docRef(CH_POST, post.key, cntCH.child, OnPeer.myUid()), Count.getCnt(post,cntCH), success = { }, fail = { })
//    }


//    fun listenPost(postKey: String) {
//        listenerPost = Firee.commonRealTimeDoc("listenerPost", arrayOf(CH_POST, postKey),
//                success = { doc ->
//                    lllogD("listenPost : success")
//                    doc.toObject(Post::class.java)?.let { post ->
//                        livePost.value = post
//                    }
//                }, fail = { livePost.value = null })
//    }


//    private fun firstQuery(board: Board, limit: Long) {

//        Firee.firstQueryPost(board .key, limit,
//                success = { querySnapShot ->
//                    AnimUtil.hideNormal(b.progressAv)
//                    b.refreshLayout.finishRefresh(0)
//
//                    OnVisible.toggleIsEmpty(querySnapShot.isEmpty, arrayOf(b.tvNoData, b.tvRefresh, b.btnGet), arrayOf(b.rv))
//
//
//                    OnPost.setItemsList(querySnapShot, items)
//                    adapter.notifyDataSetChanged()
//
//                    lastDoc = querySnapShot.setLastSnap()
//                    isEndData = false

//                },
//                fail = { b.refreshLayout.finishRefresh(0) })

//    }


//    private fun secondQuery() {

//        Firee.secondQueryPost(board!!.key, lastDoc!!, LIMIT,
//                success = { querySnapShot ->
//                    AnimUtil.hideNormal(b.progressAv)
//                    b.refreshLayout.finishLoadMore(0)
//
//                    OnPost.setItemsList(querySnapShot, items)
//                    adapter.notifyDataSetChanged()
//
//                    lastDoc = querySnapShot.setLastSnap()
//
//                    Log.d(Cons.TAG, "secondQuery lastDoc title : ${lastDoc?.get("title").toString()}")
//                    isEndData = querySnapShot.setEndData(LIMIT)
//
//                },
//                fail = { b.refreshLayout.finishRefresh(0) })
//    }


}