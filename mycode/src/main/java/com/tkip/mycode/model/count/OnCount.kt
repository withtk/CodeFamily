package com.tkip.mycode.model.count

import android.content.Context
import android.content.Intent
import android.view.View
import com.tkip.mycode.R
import com.tkip.mycode.dialog.toast.toastNormal
import com.tkip.mycode.funs.common.lllogM
import com.tkip.mycode.init.PUT_COUNT_TYPE_NO
import com.tkip.mycode.init.PUT_MANAGE
import com.tkip.mycode.init.PUT_UID
import com.tkip.mycode.model.board.Board
import com.tkip.mycode.model.count.my_book_push.CountListActivity
import com.tkip.mycode.model.my_enum.anyKey
import com.tkip.mycode.model.peer.myuid
import com.tkip.mycode.util.lib.TransitionHelper
import com.tkip.mycode.util.lib.retrofit2.ESget
import com.tkip.mycode.util.tools.fire.Firee

class OnCount {
    companion object {

        /**
         * 나의 countKey
         * CountType.name + uid + modelKey
         */
        fun myCountKey(any: Any, type: CountType) = type.name + myuid() + any.anyKey()

        fun addCount(any: Any, countType: CountType) {
            Firee.addCount(Count(any, countType), {}, {})
        }

        /**
         * for book, push
         */
        fun gotoMyCountActivity(context: Context, manage: Boolean, uid: String?, countType: CountType, arrPair: Array<androidx.core.util.Pair<View, String>>?) {
            Intent(context, CountListActivity::class.java).apply {
                addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
                putExtra(PUT_MANAGE, manage)
                putExtra(PUT_UID, uid)
                putExtra(PUT_COUNT_TYPE_NO, countType.no)
                TransitionHelper.startTran(context, this, arrPair)
            }
        }


        /*****************************************************************************************
         * for FLEA, ROOM
         * 북마크, 푸쉬
         * 이미 했는지 여부 체크
         */
        fun checkAddCountForFleaRoom(board1: Board?, cntType: CountType) {
            lllogM("checkAddCountForFleaRoom board1 $board1")
            board1?.let { board -> hasCount(board, cntType, onAdd = { Firee.addCount(Count(board, cntType), success = { toastNormal(cntType.msgOn) }, fail = { }) }) }
        }

        /**
         * 북마크, 푸쉬
         * 이미 했는지 여부 체크
         * onAdd() : 추가해야 할 때
         * count != null : 이미 추가됨.
         ****************************************************************************************/
        private fun hasCount(board: Board, cntType: CountType, onAdd: () -> Unit) {
            val index = cntType.getTopChild(board)
            val key = myCountKey(board, cntType)
            ESget.getCount(index, key,
                    success = {
                        if (it == null) onAdd()
                        else toastNormal(R.string.alert_has_count)
                    }, fail = {})
        }

    }
}










