package com.tkip.mycode.model.board

import android.content.Context
import android.content.Intent
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import com.tkip.mycode.R
import com.tkip.mycode.admin.menu.AMenu
import com.tkip.mycode.admin.menu.OnMenu
import com.tkip.mycode.dialog.alert.MyAlert
import com.tkip.mycode.dialog.progress.MyCircle
import com.tkip.mycode.dialog.progress.WAIT_TYPE
import com.tkip.mycode.dialog.toast.TToast
import com.tkip.mycode.funs.common.lllogD
import com.tkip.mycode.funs.common.lllogI
import com.tkip.mycode.init.*
import com.tkip.mycode.init.base.Sred
import com.tkip.mycode.init.base.checkUpdate
import com.tkip.mycode.init.base.isChanged
import com.tkip.mycode.model.cost.Cost
import com.tkip.mycode.model.my_enum.ActType
import com.tkip.mycode.model.my_enum.Status
import com.tkip.mycode.model.my_enum.getStatus
import com.tkip.mycode.model.mytag.OnTag
import com.tkip.mycode.model.peer.checkAllAvailUpload
import com.tkip.mycode.model.peer.isNotAdmin
import com.tkip.mycode.model.peer.peer_allow.BoardAllowActivity
import com.tkip.mycode.nav.board.BoardActivity
import com.tkip.mycode.nav.board.BoardListActivity
import com.tkip.mycode.nav.board.tab_first.AddBoardActivity
import com.tkip.mycode.nav.post.PostActivity
import com.tkip.mycode.util.lib.TransitionHelper
import com.tkip.mycode.util.lib.photo.util.PhotoUtil
import com.tkip.mycode.util.lib.retrofit2.ESget
import com.tkip.mycode.util.lib.retrofit2.ESquery
import com.tkip.mycode.util.tools.etc.OnMyClick
import com.tkip.mycode.util.tools.fire.*


class OnBoard {

    companion object {

        fun gotoBoardAllowActivity(context: Context, arrPair: Array<androidx.core.util.Pair<View, String>>?) {
            val intent = Intent(context, BoardAllowActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
            TransitionHelper.startTran(context, intent, arrPair)
        }


        /**
         * board가 널이면 Addboard에서 알아서 새로 만든다.
         */
        fun gotoAddBoardActivity(context: Context, action: Int, board: Board?, arrPair: Array<androidx.core.util.Pair<View, String>>?) {

            /*** 업로드 가능 여부 체크 */
            if (checkAllAvailUpload(context)) return

            val intent = Intent(context, AddBoardActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
            intent.putExtra(PUT_ACTION, action)
            intent.putExtra(PUT_BOARD, board)

            TransitionHelper.startTran(context, intent, arrPair)
        }

        /**
         * isTop : FreePostActivity 액션바의 보드네임 클릭시.
         */
        fun gotoBoardActivity(context: Context, isTop: Boolean, action: Int?, board: Board?, arrPair: Array<androidx.core.util.Pair<View, String>>?) {
            lllogD("gotoBoardActivity board $board")

            if (board == null) return   // todo : 토스트를 날려줘야 하나..?
//            if (board.statusNo.lt(Status.NORMAL.no)) {
//                TToast.showAlert(board.statusNo.getStatus().msg)
//                return
//            }

            val intent = Intent(context, BoardActivity::class.java)
            if (isTop) intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
            intent.putExtra(PUT_ACTION, action)
            intent.putExtra(PUT_BOARD, board)
            TransitionHelper.startTran(context, intent, null)
        }

        fun gotoBoardActivity(context: Context, isTop: Boolean, action: Int?, boardKey: String, arrPair: Array<androidx.core.util.Pair<View, String>>?) {
            val intent = Intent(context, BoardActivity::class.java)
            if (isTop) intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
            intent.putExtra(PUT_ACTION, action)
            intent.putExtra(PUT_KEY, boardKey)
            TransitionHelper.startTran(context, intent, null)
        }


        fun getIntent(context: Context, isTop: Boolean, addType: Int?, modelKey: String?) =
                Intent(context, BoardActivity::class.java).apply {
                    if (isTop) addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
                    addType?.let { putExtra(PUT_ACTION, it) }
                    putExtra(PUT_KEY, modelKey)
                }


        /**
         * for admin
         */
        fun gotoBoardListActivity(context: Context, arrPair: Array<androidx.core.util.Pair<View, String>>?) {
            val intent = Intent(context, BoardListActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
            TransitionHelper.startTran(context, intent, null)
        }


        /******************************************************************************
         ************ upload **********************************************************
         ******************************************************************************/
        fun addPhotoAndBoard(addType: Int, activity: AppCompatActivity, preBoard: Board, board: Board, success: () -> Unit, fail: () -> Unit) {
            WAIT_TYPE.BASIC_1.show(activity)

            lllogI("addPhotoAndBoard addPhotoAndBoard start")
            PhotoUtil.uploadUriSingle(activity, board,
                    successUploading = { foto ->

                        foto?.let {
                            PhotoUtil.deletePhoto(board.photoUrl, board.photoThumb)
                            board.photoUrl = foto.url
                            board.photoThumb = foto.thumbUrl
                        }


                        /** gotoFire */
                        batch().apply {
                            when (addType) {
                                ACTION_NEW -> {
                                    this.set(docRef(EES_BOARD, board.key), board)
                                }
                                ACTION_UPDATE -> this.update(docRef(EES_BOARD, board.key), makeMap(preBoard, board))
                            }

                            /** 계산서 올리기 */
                            if (isNotAdmin(null, null)) this.addCostWithList(board)
                            /** 신규 MyTag 올리기 */
                            OnTag.addToBatchAllMytag(this)
                        }.myCommit("addPhotoAndBoard",
                                success = {
                                    when (addType) {
                                        ACTION_NEW -> {
                                            success()
                                            Cost.new(null, ActType.ADD_BOARD, board).real()
                                        }
                                        ACTION_UPDATE -> {
                                            success()
                                        }
                                    }
                                }, fail = {})

                    }, fail = { fail() })
        }

//        private fun getBatchBoard(addType: Int, preBoard: Board, board: Board): WriteBatch =
//                batch().apply {
//                    lllogI("OnBoard getBatchBoard  \n preBoard:$preBoard \n board:$board")
//                    when (addType) {
//                        ACTION_NEW -> {
//                            this.set(docRef(EES_BOARD, board.key), board)
//                            /** 계산서 올리기 */
//                            if (isNotAdmin(null, null))   this.addCostWithList(board)
//                        }
//                        ACTION_UPDATE -> this.update(docRef(EES_BOARD, board.key), makeMap(preBoard, board))
//                    }
//                    OnTag.addToBatchAllMytag(this)
//
//                }

        fun completedDB(activity: AppCompatActivity, board: Board) {
            OnTag.clearMyTagList()
            TToast.showNormal(R.string.complete_upload)
            lllogI("OnBoard completedDB board $board")
            gotoBoardActivity(activity, true, ACTION_NEW, board, null)
            activity.finish()
        }

        /**
         * batch에 넣을 map
         */
        private fun makeMap(preBoard: Board, board: Board) =
                mutableMapOf<String, Any?>().apply {
                    checkUpdate(preBoard.title, board.title, CH_TITLE)
                    checkUpdate(preBoard.comment, board.comment, CH_COMMENT)

                    if (preBoard.comment.isChanged(board.comment) && !board.comment.isNullOrBlank()) {   // 바뀌고 comment 널이 아니어야 카운트 증가.
                        put(CH_COMMENT_NO, board.commentNo + 1)
                    }

                    checkUpdate(preBoard.boardId, board.boardId, CH_BOARD_ID)
//                    checkUpdate(preBoard.password, board.password, CH_PASSWORD)
                    checkUpdate(preBoard.photoUrl, board.photoUrl, CH_PHOTO_URL)
                    checkUpdate(preBoard.photoThumb, board.photoThumb, CH_PHOTO_THUMB)

//                    if (board.password == null) put(CH_BLIND, false)
//                    else put(CH_BLIND, true)

                    board.tagList?.let { put(CH_TAG_LIST, it) }

                    put(CH_ALLOW_POST, board.allowPost)
                    put(CH_ALLOW_SEARCH, board.allowSearch)
                    if (isNotAdmin(null, null)) put(CH_UPDATE_DATE, System.currentTimeMillis())
                }

        fun changeStatus(board: Board, status: Status, success: () -> Unit, fail: () -> Unit) {
            Firee.updateBoard(batch().update(docRef(EES_BOARD, board.key), CH_STATUS_NO, status.no), success = { success() }, fail = { fail() })
        }


        /**
         *  context menu Dialog 띄우기.
         */
        fun showMenuDialog(context: Context, board: Board, onSelect: (AMenu) -> Unit) {
            MyAlert.showMenuList(context, OnMenu.getMENULIST(board), onSelect = { amenu -> onSelect(amenu) })
        }


        /**
         * board1 받고  ->  status 체크하고  ->  정상이면 이동
         */
        fun checkAndGo(context: Context, isTop: Boolean, key: String, arrPair: Array<androidx.core.util.Pair<View, String>>?, onSuccess: ((Board) -> Unit)?, onNotNormal: ((Board) -> Unit)?) {
            lllogD("onBoard checkAndGo start")
//            if (OnMyClick.isDoubleClick()) return

            ESget.getBoard(key,
                    success = { board1 ->

                        if (board1 == null) {
                            lllogD("onBoard checkAndGo null")
                            TToast.showNormal(R.string.error_data_deleted)
                            MyCircle.cancel()
                        } else {
                            lllogD("onBoard checkAndGo true")
                            if (board1.statusNo >= Status.NORMAL.no) {
                                gotoBoardActivity(context, isTop, null, board1, arrPair)
                                onSuccess?.invoke(board1)
                            } else {
                                onNotNormal?.invoke(board1)
                                TToast.showAlert(board1.statusNo.getStatus().msg)
                                MyCircle.cancel()
                            }
                        }

                    }, fail = {})

        }

        /**
         * 상태체크 없이 그냥 고
         */
//        fun justGo(context: Context,  isTop: Boolean, key: String,arrPair: Array<androidx.core.util.Pair<View, String>>? ) {
//            ESget.getBoard(key, success = { it?.let { data -> gotoBoardActivity(context, isTop, null, data, arrPair) } }, fail = {})
//        }


        /**
         * 기본 보드는 toast 금지.
         */
        fun getBoard(boardKey: String, toast: Boolean, onBoard: ((Board) -> Unit)?, onFail: (() -> Unit)?) {
            ESget.getBoard(boardKey,
                    success = { board1 ->
//                        lllogD("onBoard getBoard board1 : $board1")
                        if (board1 == null) {
                            if (toast) TToast.showNormal(R.string.error_data_deleted)
                            MyCircle.cancel()
                        } else onBoard?.invoke(board1)
                    }, fail = { onFail?.invoke() })
        }

        /*************  기본 보드 리셋하기  *********************************************/
        fun resetBasicBoard() {
            /** 베스트 보드1 */
            getBestBoardList(1, onSuccess = {
                if (!it.isNullOrEmpty()) {
                    BOARD_NO1 = it[0]
                    Sred.putObjectBoard(Sred.BOARD_NO1, BOARD_NO1)
                }
            }, onFail = {})
        }

        /************* Flea ROom 보드 refresh 하기.   *************/
        fun resetFleaRoomBoard() {
            /** getBoard : 중고거래 보드 */
            getBoard(FLEA_BOARD_KEY, false, onBoard = { fleaBoard = it }, onFail = {})
            /** getBoard : 부동산 보드 */
            getBoard(ROOM_BOARD_KEY, false, onBoard = { roomBoard = it }, onFail = {})
            /** getBoard : 최근 베스트 보드 */
            getBoard(RECENT_BOARD_KEY, false, onBoard = {
                recentBoard = it
                Sred.putObjectBoard(Sred.BOARD_RECENT, recentBoard)
            }, onFail = {})
        }

        /*********************************************************************************/


        fun getBestBoardList(size: Int, onSuccess: ((ArrayList<Board>?) -> Unit)?, onFail: () -> Unit) {
            /*** 베스트 보드 받아오기 & refresh */
            ESget.getBoardList(ESquery.boardList(size, BoardType.USER.no, CH_TOTAL_POINT, Status.NORMAL), success = { onSuccess?.invoke(it) }, fail = { onFail() })
        }


    }

}