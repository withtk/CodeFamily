package com.tkip.mycode.model.mytag

import android.app.Dialog
import android.content.Context
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AlertDialog
import com.tkip.mycode.R
import com.tkip.mycode.databinding.DialogAddMytagBinding
import com.tkip.mycode.dialog.OnDDD
import com.tkip.mycode.dialog.progress.MyCircle
import com.tkip.mycode.dialog.progress.WAIT_TYPE
import com.tkip.mycode.dialog.toast.toastAlert
import com.tkip.mycode.funs.common.getNullStr
import com.tkip.mycode.funs.common.getStr
import com.tkip.mycode.funs.common.isNullBlank
import com.tkip.mycode.funs.common.lllogI
import com.tkip.mycode.init.PUT_MY_TAG
import com.tkip.mycode.init.base.isNullBlankAndSetError
import com.tkip.mycode.init.inter.interDialogBasicClick
import com.tkip.mycode.init.my_bind.BindingDialog
import com.tkip.mycode.model.my_enum.STATUS_LIST_TAG
import com.tkip.mycode.util.tools.etc.OnMyClick
import com.tkip.mycode.util.tools.fire.Firee
import com.tkip.mycode.util.tools.spinner.LinearSpinnerStatus
import com.tkip.mycode.util.tools.spinner.LinearSpinnerString
import com.tkip.mycode.util.tools.spinner.TypeList


class ADDmytagDialog : BindingDialog<DialogAddMytagBinding>() {
    override fun getLayoutResId(): Int = R.layout.dialog_add_mytag
    override fun getSize(): Pair<Float?, Float?> = Pair(0.75f, null)


    lateinit var spinStatus: LinearSpinnerStatus
    lateinit var spinString: LinearSpinnerString

    private val mytag by lazy { arguments?.getParcelable<MyTag>(PUT_MY_TAG) ?: MyTag() }

    companion object {
        @JvmStatic
        fun newInstance(mytag: MyTag?) = ADDmytagDialog().apply { arguments = Bundle().apply { putParcelable(PUT_MY_TAG, mytag) } }
    }

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {

        b.mytag = mytag
        b.inter = interDialogBasicClick(onDel = { onClickDelete(it) },
                onCancel = { onClickCancel(it) },
                onOk = { onClickOk(it) })

        b.etTitle.setOnFocusChangeListener { _, isOn -> if (!isOn) b.etTitle.isNullBlankAndSetError(b.layTitle, R.string.error_add_ad_unit_title) }


        /**         * Status 스피너     */
        spinStatus = LinearSpinnerStatus(mContext, true,STATUS_LIST_TAG, mytag.statusNo, onClick = { it.let { mytag.statusNo = it.no } })
        b.lineSpin.addView(spinStatus)

        /**         * String 스피너 : TAG type    */
        spinString = LinearSpinnerString(mContext, true, null, TypeList.MYTAG, mytag.tagTypeNo, onSelected = { typeNo -> mytag.tagTypeNo = typeNo })
        b.lineSpin.addView(spinString)

        lllogI("ADDmytagDialog spinStatus $spinStatus")
        lllogI("ADDmytagDialog spinString $spinString")

        val builder = AlertDialog.Builder(activity as Context).apply {
            setView(b.root)
            setCancelable(true)
        }

        return builder.create().apply {
            setCanceledOnTouchOutside(true)
            window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        }

    }

    fun onClickDelete(v: View) {
        OnMyClick.setDisableAWhileBTN(v)
        OnDDD.delete(mContext, ok = {
            Firee.deleteMyTag(mytag, success = { this.dialog?.dismiss() }, fail = {})
        }, cancel = {})
    }

    fun onClickCancel(v: View) {
        OnMyClick.setDisableAWhileBTN(v)
        this.dialog?.dismiss()
    }


    fun onClickOk(v: View) {
        OnMyClick.setDisableAWhileBTN(v)

        if (b.etTitle.isNullBlank(R.string.error_add_ad_unit_title)) return
        if (spinStatus.selectedP == 0 || spinString.selected == 0) {
            toastAlert(R.string.error_spin_selected)
            return
        }

//        OnDialog.confirm(mContext, R.string.info_wanna_upload,
//                ok = {

        WAIT_TYPE.FULL_AV.show(mContext)
        mytag.name = b.etTitle.getStr()
        mytag.totalPoint = b.etPoint.getNullStr()?.toInt() ?: 0
        /*** status, tayType은 이미 대입. */

        Firee.addMyTag(mytag,
                success = {
                    MyCircle.cancel()
                    this.dialog?.dismiss()
                }, fail = { MyCircle.cancel() })

//                },
//                cancel = {})

    }


}