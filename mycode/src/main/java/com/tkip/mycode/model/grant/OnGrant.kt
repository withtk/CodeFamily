package com.tkip.mycode.model.grant

import android.content.Context
import android.content.Intent
import android.view.View
import android.widget.Button
import com.tkip.mycode.R
import com.tkip.mycode.ad.OnBanner
import com.tkip.mycode.dialog.OnDDD
import com.tkip.mycode.dialog.alert.MyAlert
import com.tkip.mycode.dialog.toast.TOAST_ALERT
import com.tkip.mycode.dialog.toast.toastNormal
import com.tkip.mycode.funs.common.lllogD
import com.tkip.mycode.funs.common.lllogM
import com.tkip.mycode.init.ACTION_UPDATE
import com.tkip.mycode.model.bid.Bid
import com.tkip.mycode.model.my_enum.*
import com.tkip.mycode.model.peer.isNotAuthor
import com.tkip.mycode.util.lib.TransitionHelper
import com.tkip.mycode.util.lib.retrofit2.ESget
import com.tkip.mycode.util.tools.anim.visi
import com.tkip.mycode.util.tools.fire.Firee
import com.tkip.mycode.util.tools.fire.commonStatusUpdate

/**
 * 승인 처리
 *    1. 모델의 status 변경.
 *    2. 승인의 status, reason 변경.
 */
class OnGrant {

    companion object {

        fun gotoGrantListActivity(context: Context, arrPair: Array<androidx.core.util.Pair<View, String>>?) {
            TransitionHelper.startTran(context, getGrantIntent(context), arrPair)
        }


        fun getGrantIntent(context: Context) = Intent(context, GrantListActivity::class.java).apply {
            addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP or Intent.FLAG_ACTIVITY_SINGLE_TOP)
        }

        /***********************************************************************************************
         * myContents 클릭시.
         *      onAvailRequest : 승인요청 가능 여부.
         *      onNormal : 정상 모델 -> 해당 모델로 goto ~
         *      onDeleted : 승인 요청 불가능한 게시물 -> 삭제 물음 -> Status.DELETED 처리.
         */
//        val INTERVAL_BTN_SHOW = 3000
        fun checkStatus(context: Context, any: Any,  btnGrant: Button, onNormal: () -> Unit, onDeleted: () -> Unit) {
            lllogM("OnGrant checkStatus any $any")
            val statusNo = any.anyStatusNo()
            when {
                /** 정상 onNormal */
                any.gteNormalStatus() -> onNormal()

                /** 비정상(model.statusNo : DENIED 이상일 때)   */
                availRequestGrant(null, any) ->
                    getGrant(any) { grant1 ->
                        MyAlert.showInfoGrant(context, grant1, any, onNormal, onDeleted)  // 다이얼로그
                        btnGrant.visi() // show btn
//                        /*** 이미 요청했으면 버튼 no show. */
//                        hhh(1000) { if(grant1.statusNo != Status.GRANT_REQUESTED.no)  btnGrant.visi() else  btnGrant.gone()}
//                        onAvailRequest(grant1.statusNo != Status.GRANT_REQUESTED.no)  // true(show) -> (grant1 == null) || else ,  false(hide) ->  grant1.statusNo == Status.GRANT_NORMAL.no
                    }

                /**  삭제할까요? -> 삭제처리 후 콜백.  onDeleted */
                else -> OnDDD.confirm(context, R.string.ask_grant_delete_model, ok = { commonStatusUpdate(any.anyChild(), any.anyKey(), Status.DELETED_USER, success = { onDeleted() }, fail = { }) }, cancel = {})
            }
        }

        /*** 승인 요청 가능여부 : 모델.StatusNo > BLIND_ADMIN.no */
        fun availRequestGrant(context: Context?, any: Any): Boolean {
            return if (any.anyStatusNo() == Status.BLIND_ADMIN.no) {
                /*** 알림 show */
                context?.let { OnDDD.confirm(it, R.string.ask_grant_delete_model, ok = { commonStatusUpdate(any.anyChild(), any.anyKey(), Status.DELETED_USER, success = { }, fail = { }) }, cancel = {}) }
                false
            } else true
        }


        /*** grant 받아오기 & convert (요청 or 처리) */
        fun getGrant(any: Any, onGet: (Grant?) -> Unit) {
            lllogM("OnGrant getGrant key $any")
            ESget.getGrant(any.anyKey(), success = { onGet(it) }, fail = {})
        }

        /*** 업로드를 위한 grant 만들기 */
        fun finalGrant(isHandle: Boolean, g: Grant?, any: Any?, message: String?) =
                (g?.convert(isHandle, any, message) ?: Grant(any!!).convert(isHandle, any, message))


        /**
         * 승인 요청 : for user
         */
        fun request(context: Context, any: Any, onSuccess: (Grant) -> Unit) {
            lllogD("onGrant request any $any")

            if (any.anyAuthorUid().isNotAuthor(TOAST_ALERT, null)) return

            /** model 승인 요청 가능한 상태인지 체크. */
            if (!availRequestGrant(context, any)) return

            /** grant 받아오기 & 승인 (요청or처리) */
            getGrant(any) { grant1 ->
                lllogD("onGrant request grant1 $grant1")

                /*** 기 신청 여부 체크 */
                if (grant1?.statusNo != Status.GRANT_REQUESTED.no) {

                    lllogD("sdfsdf sdfsdfsd")
                    /** grant request 다이얼로그 띄우기 */
                    MyAlert.showRequestGrant(context, any,
                            onSuccess = { message ->
                                /** grant 받아오기 & 승인 (요청or처리) */
                                getGrant(any) { g ->
                                    val ggg = finalGrant(false, g, any, message)
//                                            grant2?.convert(false, message)
//                                            ?: Grant(any).convert(false, message)
                                    Firee.addGrant(ggg,
                                            success = {
                                                toastNormal(R.string.info_grant_request)
                                                onSuccess(it)
                                            }, fail = {})
                                }
                            })
                } else {
                    /*** 이미 요청중. */
                    OnDDD.info(context, R.string.info_already_requested_grant)
                    return@getGrant
                }

            }
        }

        /**
         * 승인 요청 : for user
         */
        fun requestBanner(context: Context, banner: Bid, onSuccess: () -> Unit) {
            lllogD("onGrant request any $banner")

            /** model 승인 요청 가능한 상태인지 체크. */
            if (!availRequestGrant(context, banner)) return

            /** grant 받아오기 & 승인 (요청or처리) */
            getGrant(banner) { grant1 ->
                lllogD("onGrant request grant1 $grant1")

                /*** 기 신청 여부 체크 */
                if (grant1?.statusNo != Status.GRANT_REQUESTED.no) {

                    lllogD("sdfsdf sdfsdfsd")
                    /** grant request 다이얼로그 띄우기 */
                    MyAlert.showRequestGrant(context, banner,
                            onSuccess = { message ->

                                getGrant(banner) {
                                    banner.statusNo = Status.BANNER_REQUESTED.no   // status 요청으로 변경.
                                    val ggg = finalGrant(false, it, banner, message)
                                    OnBanner.addPhotoAndBanner(context, ACTION_UPDATE, banner, ggg,
                                            success = {
                                                toastNormal(R.string.info_grant_request)
                                                onSuccess()
                                            },
                                            fail = { })
                                }


//                                /** grant 받아오기 & 승인 (요청or처리) */
//                                getGrant(banner) { g ->
//                                    val ggg = finalGrant(false, g, banner, message)
////                                            grant2?.convert(false, message)
////                                            ?: Grant(any).convert(false, message)
//                                    Firee.addGrant(ggg,
//                                            success = {
//                                                toastNormal(R.string.info_grant_request)
//                                                onSuccess(it)
//                                            }, fail = {})
//                                }
                            })
                } else {
                    /*** 이미 요청중. */
                    OnDDD.info(context, R.string.info_already_requested_grant)
                    return@getGrant
                }

            }
        }


        /******************************************************************************************
         * 일반 모델 (Board,,,,,) 상태 변경 사유 올릴때 for admin
         */
        fun addGrant1111(isHandle: Boolean, grant: Grant, onSuccess: (Grant) -> Unit, onFail: () -> Unit) {
            lllogD("onGrant handle  grant $grant")
            /**  add grant 승인 처리  */
            Firee.addGrant(
//                this.convert(true, reason1)
//                    finalGrant(true, grant, null, reason1)
                    grant,
                    success = {
                        lllogD("onGrant handle addGrant success grant $grant")
                        toastNormal(if (isHandle) R.string.info_grant_handle else R.string.info_grant_request)
                        onSuccess(it)
                    }, fail = { onFail() })
        }


        /**
         * for admin
         * 딥삭제
         */
        fun deepDel(status:Status,any: Any) {
            lllogD("onGrant deepDel status $status")
            if(status == Status.NORMAL) Firee.deepDelGrant(any.anyKey(), {}, {})   // any.Status 정상시 grant 삭제.
        }




    }
}










