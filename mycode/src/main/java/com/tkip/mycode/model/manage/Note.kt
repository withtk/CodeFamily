package com.tkip.mycode.model.manage

import android.os.Parcel
import android.os.Parcelable
import com.tkip.mycode.init.base.createKey
import com.tkip.mycode.model.my_enum.Status
import com.tkip.mycode.model.peer.OnPeer
import com.tkip.mycode.model.peer.mynick
import com.tkip.mycode.model.peer.myuid
import com.tkip.mycode.util.tools.date.rightNow

/**
 * 단순 노트 업로드.
 * 1. 버그 신고.
 * 2. 개별 문의.
 * 3. 제휴문의.
 */
data class Note(

        var key: String = createKey(),
        var authorUID: String = myuid(),
        var authorNick: String = mynick(),
        var authorThumb: String? = OnPeer.peer.faceThumb,

        var comment: String? = null,

        var statusNo: Int = Status.NORMAL.no,
        var addDate: Long = rightNow()

) : Parcelable {
    constructor(source: Parcel) : this(
            source.readString()!!,
            source.readString()!!,
            source.readString()!!,
            source.readString(),
            source.readString(),
            source.readInt(),
            source.readLong()
    )

    override fun describeContents() = 0

    override fun writeToParcel(dest: Parcel, flags: Int) = with(dest) {
        writeString(key)
        writeString(authorUID)
        writeString(authorNick)
        writeString(authorThumb)
        writeString(comment)
        writeInt(statusNo)
        writeLong(addDate)
    }

    companion object {

        @JvmField
        val CREATOR: Parcelable.Creator<Note> = object : Parcelable.Creator<Note> {
            override fun createFromParcel(source: Parcel): Note = Note(source)
            override fun newArray(size: Int): Array<Note?> = arrayOfNulls(size)
        }
    }
}
