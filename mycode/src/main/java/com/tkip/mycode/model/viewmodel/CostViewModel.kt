package com.tkip.mycode.model.viewmodel

import androidx.lifecycle.ViewModel
import com.tkip.mycode.model.cost.Cost
import com.tkip.mycode.util.lib.retrofit2.ESget
import com.tkip.mycode.util.lib.retrofit2.ESquery

class CostViewModel : ViewModel() {

    val liveCostList = ListLiveData<Cost>()

    fun getEsCostList(size: Int, from: Int, uid: String): ListLiveData<Cost> {
        ESget.getCostList(ESquery.costList(size, from, uid),
                success = {
                    if (it == null) {
                        liveCostList.clear(true)
                    } else {
                        if (from == 0) liveCostList.clear(false)
                        liveCostList.addAll(it)
                    }
                }, fail = {})
        return liveCostList
    }

}