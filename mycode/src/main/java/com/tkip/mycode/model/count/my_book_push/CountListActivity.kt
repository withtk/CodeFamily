package com.tkip.mycode.model.count.my_book_push

import android.os.Bundle
import androidx.annotation.LayoutRes
import com.tkip.mycode.R
import com.tkip.mycode.databinding.ActivityCountListBinding
import com.tkip.mycode.init.PUT_COUNT_TYPE_NO
import com.tkip.mycode.init.PUT_MANAGE
import com.tkip.mycode.init.PUT_UID
import com.tkip.mycode.init.inter.interToolbarIcon
import com.tkip.mycode.init.my_bind.BindingActivity
import com.tkip.mycode.model.count.CountType
import com.tkip.mycode.model.count.getCountType
import com.tkip.mycode.model.peer.myuid

/**
 * 내 COUNT : book, push, reply
 */
class CountListActivity : BindingActivity<ActivityCountListBinding>() {
    @LayoutRes
    override fun getLayoutResId() = R.layout.activity_count_list

    private val manage by lazy { intent?.getBooleanExtra(PUT_MANAGE, false) ?: false }
    private val uid by lazy {  intent?.getStringExtra(PUT_UID) ?: myuid()}
    private val countType by lazy {  intent?.getIntExtra(PUT_COUNT_TYPE_NO,CountType.BOOK.no)?.getCountType()!! }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        initView()
    }

    private fun initView() {
        b.activity = this

        /** 툴바 */
        b.inToolbar.title = countType.listTitle
        b.inToolbar.backIcon = true
//        b.inToolbar.menuText = R.string.add_ad_unit_manage.getStr()
        b.inToolbar.inter = interToolbarIcon(onFirst = { onBackPressed() }, onTopTitle = {}, onTextMenu = {},onTextMenu2 = null, onIcon = {}, onMore = {})


        b.frame.addView(CustomMyCountPager(this@CountListActivity,supportFragmentManager,manage,uid,countType, onItemClick = {}))


    }

}