package com.tkip.mycode.model.room

import android.widget.LinearLayout
import com.cunoraz.tagview.TagView
import com.tkip.mycode.R
import com.tkip.mycode.dialog.alert.MyAlert
import com.tkip.mycode.funs.common.lllogI
import com.tkip.mycode.funs.common.pickerValue
import com.tkip.mycode.init.base.*
import com.tkip.mycode.model.my_enum.*
import com.tkip.mycode.model.mytag.TAG_2
import com.tkip.mycode.model.mytag.checkAdd
import com.tkip.mycode.nav.room.AddRoomActivity
import com.tkip.mycode.nav.room.RoomActivity


/**
 * 유효성 체크
 * 통과 못하면 -> true
 */
fun Room.inValid(): Boolean =
        (this.price.isNull0(100, R.string.alert_need_to_price)
                || this.title.isAnyNull(R.string.alert_need_to_title)
                || this.local.isAnyNull(R.string.alert_need_to_local_name)
                || this.areaEx.isAnyNull(R.string.alert_need_to_area)
                || this.areaTotal.isAnyNull(R.string.alert_need_to_area)
//                || this.fotoList.getI(0).isAnyNull(R.string.alert_need_to_photo)
                )

/**
 * 작성중이었는지 여부.
 * onBackpressed()
 */
fun Room.isWriting(): Boolean {
    return !comment.isNullOrBlank() || !local.isNullOrBlank()
}


/*************************************************************************************/
/********************  ***********************************************************/
/*************************************************************************************/


/**
 * for Add
 * 라디오 그룹 셋팅 & 초기화
 */
fun AddRoomActivity.setupAllRadioGroup(room: Room, lineCompulsory: LinearLayout, line1: LinearLayout, line2: LinearLayout) {


//    room.matchAmountToRG()

    /**     * 모든 RG 초기화     */
    RGstring.values().forEach { it.selectedIndex = 0 }
    RGint.values().forEach { it.amount = null }

    RGstring.TYPE_CONTRACT.setIndexFromlistIndex(room.contractTypeNo.getContractType().title)
    RGstring.TYPE_ROOM.setIndexFromlistIndex(room.roomTypeNo.getRoomType().title)
    RGstring.TYPE_ROOM_STATUS.setIndexFromlistIndex(room.roomStatusNo.getRoomStatus().title)


    /*** room -> RG 대입 */
    room.countRoom?.let { RGint.ROOM.amount = it }
    room.countToilet?.let { RGint.TOILET.amount = it }
    room.countKitchen?.let { RGint.KITCHEN.amount = it }
    room.countLiving?.let { RGint.LIVING.amount = it }
    room.countBath?.let { RGint.BATH.amount = it }

    room.countParking?.let { RGint.PARKING.amount = it }
    room.countFloor?.let { RGint.FLOOR.amount = it }
    room.inAdvanced?.let { RGint.ADVANCED.amount = it }
    room.deposit?.let { RGint.DEPOSIT.amount = it }
    room.minTerm?.let { RGint.MIN_TERM.amount = it }


    /**     * 모든 Linear 초기화     */
    lineCompulsory.removeAllViews()
    line1.removeAllViews()
    line2.removeAllViews()


    /**         * String 라디오 버튼 삽입          */
    arrayListOf(RGstring.TYPE_CONTRACT, RGstring.TYPE_ROOM, RGstring.TYPE_ROOM_STATUS).forEach { roomAmount ->
        roomAmount.addRGstring(this, lineCompulsory)
    }

    /**         * 라디오 버튼 삽입          */
    arrayListOf(RGint.ROOM, RGint.TOILET, RGint.PARKING, RGint.ADVANCED, RGint.DEPOSIT).forEach { roomAmount ->
        roomAmount.addRGint(this, line1, onMore = { radio ->
            MyAlert.showNumberPicker(this, PickerFactory.ONE_TWO, radio.pickerValue(), onOk = { _, commaStr ->
                radio.text = commaStr
                roomAmount.setAmount(commaStr.toInt())
            })
        })
    }

    /**         * 추가 입력사항          */
    arrayListOf(RGint.KITCHEN, RGint.LIVING, RGint.BATH, RGint.MIN_TERM).forEach { roomAmount ->
        roomAmount.addRGint(this, line2, onMore = { radio ->
            MyAlert.showNumberPicker(this, PickerFactory.ONE_TWO, radio.pickerValue(), onOk = { _, commaStr ->
                radio.text = commaStr
                roomAmount.setAmount(commaStr.toInt())
            })
        })
    }

    /**         * 층수          */
    RGint.FLOOR.addRGint(this, line2, onMore = { radio ->
        MyAlert.showNumberPicker(this, PickerFactory.FLOOR, radio.pickerValue(), onOk = { _, commaStr ->
            RGint.FLOOR.setAmount(commaStr.removeComma().toInt())
            radio.text = commaStr
            RGint.FLOOR.setAmount(commaStr.toInt())
        })
    })
}


fun Room.hasAdditionalData() =
        (countKitchen != null || countLiving != null || countBath != null || countParking != null || constructionDate != null || countFloor != null || inAdvanced != null || deposit != null || minTerm != null || builtInList != null)


/**
 * for Add
 * RG -> room에 대입.
 */
fun AddRoomActivity.matchAmountFromRG(room: Room) {
    RGstring.values().forEach {
        when (it) {
            RGstring.TYPE_CONTRACT -> {
                val typeTitle = it.list[it.selectedIndex]
                val typeNo = typeTitle.getContractType().no
                room.contractTypeNo = typeNo
            }
            RGstring.TYPE_ROOM -> room.roomTypeNo = it.list[it.selectedIndex].getRoomType().no
            RGstring.TYPE_ROOM_STATUS -> room.roomStatusNo = it.list[it.selectedIndex].getRoomStatus().no
        }
    }

    RGint.values().forEach {
        when (it) {
            RGint.ROOM -> room.countRoom = it.getResultAmount()
            RGint.TOILET -> room.countToilet = it.getResultAmount()
            RGint.KITCHEN -> room.countKitchen = it.getResultAmount()
            RGint.LIVING -> room.countLiving = it.getResultAmount()
            RGint.BATH -> room.countBath = it.getResultAmount()
            RGint.PARKING -> room.countParking = it.getResultAmount()
            RGint.FLOOR -> room.countFloor = it.getResultAmount()
            RGint.ADVANCED -> room.inAdvanced = it.getResultAmount()
            RGint.DEPOSIT -> room.deposit = it.getResultAmount()
            RGint.MIN_TERM -> room.minTerm = it.getResultAmount()
        }
    }
}


///**
// * for Add
// * room -> RG에 대입.
// */
//fun Room.matchAmountToRG() {
////    lllogI("matchAmountToRG Room $this")
//
//    /**     * 모든 RG 초기화     */
//    RGstring.values().forEach { it.selectedIndex = 0 }
//    RGint.values().forEach { it.amount = null }
//
//    RGstring.TYPE_CONTRACT.setIndexFromlistIndex(contractTypeNo.getContractType().title)
//    RGstring.TYPE_ROOM.setIndexFromlistIndex(roomTypeNo.getRoomType().title)
//    RGstring.TYPE_ROOM_STATUS.setIndexFromlistIndex(roomStatusNo.getRoomStatus().title)
//
//
//    /*** room 대입 */
//    countRoom?.let { RGint.ROOM.amount = it }
//    countToilet?.let { RGint.TOILET.amount = it }
//    countKitchen?.let { RGint.KITCHEN.amount = it }
//    countLiving?.let { RGint.LIVING.amount = it }
//    countBath?.let { RGint.BATH.amount = it }
//
//    countParking?.let { RGint.PARKING.amount = it }
//    countFloor?.let { RGint.FLOOR.amount = it }
//    inAdvanced?.let { RGint.ADVANCED.amount = it }
//    minTerm?.let { RGint.MIN_TERM.amount = it }
//
//}


/**
 * for Load
 * roomActivity에 기입.
 */
fun RoomActivity.showAmountInRoomActi(room: Room, tagViewAmount: TagView) {
    lllogI("showAmountInRoomActi Room $room")

    room.countRoom?.let { tagViewAmount.checkAdd(TAG_2, R.string.name_room.getStr() + it.toString()) }
    room.countToilet?.let { tagViewAmount.checkAdd(TAG_2, R.string.name_toilet.getStr() + it.toString()) }
    room.countKitchen?.let { tagViewAmount.checkAdd(TAG_2, R.string.name_kitchen.getStr() + it.toString()) }
    room.countLiving?.let { tagViewAmount.checkAdd(TAG_2, R.string.name_living.getStr() + it.toString()) }
    room.countBath?.let { tagViewAmount.checkAdd(TAG_2, R.string.name_bath.getStr() + it.toString()) }

    room.countParking?.let { tagViewAmount.checkAdd(TAG_2, String.format(R.string.name_parking_show.getStr(), it)) }
    room.countFloor?.let { tagViewAmount.checkAdd(TAG_2, "$it ${R.string.unit_floor.getStr()}") }
    room.constructionDate?.let { tagViewAmount.checkAdd(TAG_2, getStringConstructionDate(room.constructionDate.toString())) }

    room.inAdvanced?.let { tagViewAmount.checkAdd(TAG_2, String.format(R.string.name_in_advanced_item.getStr(), it)) }
    room.deposit?.let { tagViewAmount.checkAdd(TAG_2, String.format(R.string.name_in_deposit_item.getStr(), it)) }
    room.minTerm?.let { tagViewAmount.checkAdd(TAG_2, "${R.string.name_min_term.getStr()} $it${R.string.unit_month.getStr()}") }


//    builtInList?.let { it.forEach { str -> tagViewBuilt.addTag2( str ) } }
//    tagList?.let { it.forEach { str -> tagViewTag.addTag2(str) } }

}


/**
 * 준공년월.
 */
fun getStringConstructionDate(yearMonth6: String): String {
    val y4 = yearMonth6.substring(0, 4)
    val m2 = yearMonth6.substring(4, yearMonth6.length)
    return String.format(R.string.construction_date.getStr(), y4, m2)
}

