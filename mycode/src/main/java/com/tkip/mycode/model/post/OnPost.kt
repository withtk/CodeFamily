package com.tkip.mycode.model.post

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.view.View
import com.google.firebase.firestore.WriteBatch
import com.tkip.mycode.R
import com.tkip.mycode.dialog.progress.MyCircle
import com.tkip.mycode.dialog.toast.TToast
import com.tkip.mycode.funs.common.lllogI
import com.tkip.mycode.init.*
import com.tkip.mycode.init.base.checkUpdate
import com.tkip.mycode.init.base.isChanged
import com.tkip.mycode.model.board.BoardType
import com.tkip.mycode.model.board.OnBoard
import com.tkip.mycode.model.board.getBoardType
import com.tkip.mycode.model.cost.Cost
import com.tkip.mycode.model.cost.pay.PAY_ACT_TYPE_LIST
import com.tkip.mycode.model.cost.pay.checkAndShowPay
import com.tkip.mycode.model.my_enum.ActType
import com.tkip.mycode.model.my_enum.Status
import com.tkip.mycode.model.my_enum.getStatus
import com.tkip.mycode.model.mytag.OnTag
import com.tkip.mycode.model.peer.checkAllAvailUpload
import com.tkip.mycode.model.peer.isNotAdmin
import com.tkip.mycode.nav.post.AddPostActivity
import com.tkip.mycode.nav.post.PostActivity
import com.tkip.mycode.util.lib.TransitionHelper
import com.tkip.mycode.util.lib.photo.Foto
import com.tkip.mycode.util.lib.photo.util.PhotoUtil
import com.tkip.mycode.util.lib.retrofit2.ESget
import com.tkip.mycode.util.tools.etc.OnMyClick
import com.tkip.mycode.util.tools.fire.*

class OnPost {

    companion object {

        fun gotoAddPostActivity(context: Context, action: Int, post: Post, arrPair: Array<androidx.core.util.Pair<View, String>>?) {

            /*** 어드민 체크 */
//            if (isAdmin()) openAddPostActivity(context, action, post, arrPair)
//            else
            when (action) {
                ACTION_NEW -> {
                    /*** 업로드 가능 여부 체크 */
                    if (checkAllAvailUpload(context)) return

                    openAddPostActivity(context, action, post, arrPair)

                    /*** board.allowPost  체크 */
//                    ESget.getBoard(post.boardKey,
//                            success = {
//                                it?.let { board ->
//                                    if (board.allowPost) openAddPostActivity(context, action, post, arrPair)
//                                    else {
//                                        lllogI("gotoAddPostActivity allowPost false ")
//                                        MyAlert.showConfirm(context, R.string.info_not_allow_post.getStr())
//
//                                        /*** allow peer 체크 */
//                                        /* ESget.getBoardAllow(post.boardKey + myuid(),
//                                                 success = { miniPeer ->
//                                                     miniPeer?.let { miniPeer1 ->
//                                                         if (context.notAvailBoardAllowPost(miniPeer1)) return@getBoardAllow
//                                                         openAddPostActivity(context, action, post, arrPair)
//                                                     }
//                                                             ?: openAddPostActivity(context, action, post, arrPair)
//                                                 }, fail = {})*/
//                                    }
//                                }
//                            }, fail = {})
                }

                /*** UPDATE는 allow 체크 없이 그냥 허용*/
                ACTION_UPDATE -> openAddPostActivity(context, action, post, arrPair)
                else -> openAddPostActivity(context, action, post, arrPair)
            }

        }

        private fun openAddPostActivity(context: Context, action: Int, post: Post, arrPair: Array<androidx.core.util.Pair<View, String>>?) {
            val intent = Intent(context, AddPostActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
            intent.putExtra(PUT_ACTION, action)
            intent.putExtra(PUT_POST, post)

            TransitionHelper.startTran(context, intent, arrPair)
        }

        fun gotoPostActivity(context: Context, isTop: Boolean, addType: Int?, post: Post, arrPair: Array<androidx.core.util.Pair<View, String>>?) {
            val intent = getIntent(context, isTop, addType, post)
            TransitionHelper.startTran(context, intent, null)
        }

        private fun getIntent(context: Context, isTop: Boolean, addType: Int?, post: Post) =
                Intent(context, PostActivity::class.java).apply {
                    if (isTop) addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
                    addType?.let { putExtra(PUT_ACTION, it) }
                    putExtra(PUT_POST, post)
                }

        fun gotoPostActivity(context: Context, isTop: Boolean, addType: Int?, postKey: String, arrPair: Array<androidx.core.util.Pair<View, String>>?) {
            val intent = getIntent(context, isTop, addType, postKey)
            TransitionHelper.startTran(context, intent, arrPair)
        }

        fun getIntent(context: Context, isTop: Boolean, addType: Int?, modelKey: String?) =
                Intent(context, PostActivity::class.java).apply {
                    if (isTop) addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
                    addType?.let { putExtra(PUT_ACTION, it) }
                    putExtra(PUT_KEY, modelKey)
                }


        /**************************************************************************
         * add 일때는 prePost == null
         *************************************************************************/
        fun addCheckUpload(addType: Int, activity: Activity, widthOrigin: Int?, selectedFotoList: ArrayList<Foto>, prePost: Post, post: Post,
                           success: () -> Unit, fail: () -> Unit) {

            OnBoard.getBoard(post.boardKey, true,
                    onBoard = { board1 ->
                        if (board1.boardTypeNo.getBoardType() == BoardType.CS_CENTER) post.allowSearch = false
                        addPhotoAndPost(addType, activity, widthOrigin, selectedFotoList, prePost, post, success, fail)
                    }, onFail = {})

        }

        private fun addPhotoAndPost(addType: Int, activity: Activity, widthOrigin: Int?, selectedFotoList: ArrayList<Foto>, prePost: Post, post: Post,
                                    success: () -> Unit, fail: () -> Unit) {

            PhotoUtil.uploadUriList(activity, widthOrigin, post, prePost.fotoList, selectedFotoList,
                    successUploading = { finalList ->
                        lllogI("addPhotoAndPost post $post")

                        post.fotoList = finalList

                        /** gotoFire */
                        batch().apply {

                            lllogI("onPost getBatchPost start")
                            when (addType) {
                                ACTION_NEW -> this.set(docRef(EES_POST, post.key), post)
                                ACTION_UPDATE -> this.update(docRef(EES_POST, post.key), makeUpdateMap(prePost, post, this))
                            }

                            /** 계산서 올리기 */
                            if (isNotAdmin(null, null)) this.addCostWithList(post)
                            /** 신규 MyTag 올리기 */
                            OnTag.addToBatchAllMytag(this)

                        }.myCommit("addPhotoAndPost",
                                success = {
                                    when (addType) {
                                        ACTION_NEW -> {
                                            completedDB(activity, true, addType, post)
                                            Cost.new(null,ActType.ADD_POST,post).real()
                                        }
                                        ACTION_UPDATE -> completedDB(activity, true, addType, post)
                                    }
                                    success()
                                }, fail = { fail() })
                    }, fail = { fail() })
        }

        private fun completedDB(activity: Activity, isTop: Boolean, addType: Int, post: Post) {
            OnTag.clearMyTagList()
            TToast.showNormal(R.string.complete_upload_refresh_list)
            gotoPostActivity(activity, isTop, addType, post, null)
            activity.finish()
        }

        private fun makeUpdateMap(prePost: Post, post: Post, batch: WriteBatch) =
                mutableMapOf<String, Any?>().apply {
                    checkUpdate(prePost.title, post.title, CH_TITLE)
                    checkUpdate(prePost.comment, post.comment, CH_COMMENT)

                    post.fotoList?.let { put(CH_FOTO_LIST, it) }  // fotolist 는 비교하기가 너무 까다롭다. 그냥 무조건 업데이트할 것.

                    if (prePost.reward.isChanged(post.reward)) {   // reward가 최초로 추가됐을때
                        post.reward?.let { reward ->
                            put(CH_REWARD, reward)   // map에 추가.
//                            Cost.addBatchAct(batch, ActType.ADD_REWARD_POST, -reward, post)
//                            batch.addCost(Cost.payment(null, ActType.ADD_REWARD_POST, post, reward))
                            PAY_ACT_TYPE_LIST.add(ActType.ADD_REWARD_POST)
//                            batch.addCost(Cost.payment(null, ActType.ADD_REWARD_POST, post, null, reward, null))
                        }
                    }
                    if (prePost.anonymous.isChanged(post.anonymous) && post.anonymous) {   // anonymous 최초로 추가됐을때
                        put(CH_ANONYMOUS, post.anonymous)
//                        Cost.addBatchAct(batch, ActType.ADD_ANONYMOUS_POST, null, post)
//                        batch.addCost(Cost.payment(null, ActType.ADD_ANONYMOUS_POST, post, null))
                        PAY_ACT_TYPE_LIST.add(ActType.ADD_ANONYMOUS_POST)
//                        batch.addCost(Cost.payment(null, ActType.ADD_ANONYMOUS_POST, post, null, null, null))
                    }

                    post.tagList?.let { put(CH_TAG_LIST, it) }

                    put(CH_ALLOW_REPLY, post.allowReply)
                    put(CH_ALLOW_SEARCH, post.allowSearch)
                    if (isNotAdmin(null, null)) put(CH_UPDATE_DATE, System.currentTimeMillis())
                }


        fun updateStatus(key: String, status: Status, success: () -> Unit, fail: (Exception) -> Unit) {
            commonStatusUpdate(EES_POST, key, status, success = { success() }, fail = { fail(it) })
        }


        /********************************************************
         * 익명 포스팅으로 변경.
         * cost 추가. credit 사용.
         *******************************************************/
        fun setAnonymous(context: Context, post: Post, success: () -> Unit, fail: () -> Unit) {
            /**         * show payDialog         */
            checkAndShowPay(context, ok = {
                post.anonymous = true
                batch().apply {
                    update(docRef(EES_POST, post.key), CH_ANONYMOUS, true)
//                            Cost.addBatchAct(this, ActType.ADD_ANONYMOUS_POST, null, post)
//                            this.addCost(Cost.payment(null, ActType.ADD_ANONYMOUS_POST, post, null))
                    this.addCost(Cost.new(null, ActType.ADD_ANONYMOUS_POST, post))
                }.myCommit("setAnonymous post",
                        success = {
                            //                                    MyCircle.cancel()
                            success()
                        }, fail = { fail() })
            }, cancel = {})


//            WAIT_TYPE.BASIC_2.show(context)
//            OnCost.checkPayment(ActType.ADD_ANONYMOUS_POST,
//                    allow = {
//                        post.anonymous = true
//                        batch().apply {
//                            update(docRef(EES_POST, post.key), CH_ANONYMOUS, true)
////                            Cost.addBatchAct(this, ActType.ADD_ANONYMOUS_POST, null, post)
////                            this.addCost(Cost.payment(null, ActType.ADD_ANONYMOUS_POST, post, null))
//                            this.addCost(Cost.new(null, ActType.ADD_ANONYMOUS_POST, post ))
//                        }.myCommit("setAnonymous post",
//                                success = {
//                                    //                                    MyCircle.cancel()
//                                    success()
//                                }, fail = { fail() })
//                    }, deny = { fail() })
        }


        /**************************************************************************
         * post 받고   * status 체크하고         * 정상이면 이동
         *************************************************************************/
        fun checkAndGo(context: Context, isTop: Boolean, key: String, status: Status, arrPair: Array<androidx.core.util.Pair<View, String>>?, onSuccess: ((Post) -> Unit)?, onNotNormal: ((Post) -> Unit)?) {
            if(OnMyClick.isDoubleClick()) return  // rv에서 동시 클릭 방지를 위해.

            ESget.getPost(key,
                    success = { post1 ->

                        if (post1 == null) {
                            TToast.showNormal(R.string.error_data_deleted)
                            MyCircle.cancel()
                        } else {
                            if (post1.statusNo >= status.no) {
                                gotoPostActivity(context, isTop, null, post1, arrPair)
                                onSuccess?.invoke(post1)
//                                MyCircle.cancel()   // postActivity에서 cancel()
                            } else {
                                onNotNormal?.invoke(post1)
                                TToast.showAlert(post1.statusNo.getStatus().msg)
                                MyCircle.cancel()
                            }
                        }


                    }, fail = {})

            /*  return

              Firee.getPost(key,
                      success = {
                          it?.let { post1 ->
                              if (post1.statusNo >= Status.NORMAL_1000.no) {
                                  gotoPostActivity(context, isTop, post1, arrPair)
                              } else {
                                  onErrorStatus(post1)
                                  TToast.showAlert(post1.statusNo.getStatus().message)
                              }
                          } ?: TToast.showNormal(R.string.error_data_deleted)
                      }, fail = {})*/
        }


    }

}