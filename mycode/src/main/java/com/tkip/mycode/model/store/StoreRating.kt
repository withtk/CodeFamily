package com.tkip.mycode.model.store

import android.os.Parcel
import android.os.Parcelable
import com.google.firebase.firestore.Exclude
import com.tkip.mycode.model.my_enum.Status
import com.tkip.mycode.model.peer.OnPeer
import com.tkip.mycode.util.tools.date.rightNow

/**
 * 각 유저가 준 평가
 *
 */
data class StoreRating(

        var uid: String,
        var storeKey: String = "storeKey",
        var comment: String? = null,
        var rating: Int = 0,            // star 개수
        var addDate: Long = rightNow(),

        var thumbUpCount: Int = 0,     // 썸업을 준 횟수.
        var thumbDownCount: Int = 0,    // 썸다운을 횟수.
        var warnCount: Int = 0,       // 신고 횟수.

        var hostComment: String? = null,        // 업주의 답변.
        var hostAddDate: Long = System.currentTimeMillis(),

        val status: Status = Status.NORMAL,
        var invalid: Boolean = false

) : Parcelable {
    constructor() : this(OnPeer.myUid())

    constructor(store: Store, comment: String?, rating: Int) : this(
            uid = OnPeer.myUid(),
            storeKey = store.key,
            comment = comment,
            rating = rating
    )

    @Exclude
    fun toMap(): Map<String, Any> {

        val result = HashMap<String, Any>()
        result["uid"] = uid
        result["storeKey"] = storeKey
        result["comment"] = comment!!
        result["rating"] = rating
        result["addDate"] = addDate

        result["thumbUpCount"] = thumbUpCount
        result["thumbDownCount"] = thumbDownCount
        result["warnCount"] = warnCount

        result["hostComment"] = hostComment!!
        result["hostAddDate"] = hostAddDate
        result["statusNo"] = status
        result["invalid"] = invalid


        return result
    }

    constructor(source: Parcel) : this(
            source.readString()!!,
            source.readString()!!,
            source.readString(),
            source.readInt(),
            source.readLong(),
            source.readInt(),
            source.readInt(),
            source.readInt(),
            source.readString(),
            source.readLong(),
            Status.values()[source.readInt()],
            1 == source.readInt()
    )

    override fun describeContents() = 0

    override fun writeToParcel(dest: Parcel, flags: Int) = with(dest) {
        writeString(uid)
        writeString(storeKey)
        writeString(comment)
        writeInt(rating)
        writeLong(addDate)
        writeInt(thumbUpCount)
        writeInt(thumbDownCount)
        writeInt(warnCount)
        writeString(hostComment)
        writeLong(hostAddDate)
        writeInt(status.ordinal)
        writeInt((if (invalid) 1 else 0))
    }

    companion object {
        @JvmField
        val CREATOR: Parcelable.Creator<StoreRating> = object : Parcelable.Creator<StoreRating> {
            override fun createFromParcel(source: Parcel): StoreRating = StoreRating(source)
            override fun newArray(size: Int): Array<StoreRating?> = arrayOfNulls(size)
        }
    }
}









