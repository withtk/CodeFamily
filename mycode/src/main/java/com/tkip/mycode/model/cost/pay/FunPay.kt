package com.tkip.mycode.model.cost.pay

import android.content.Context
import com.tkip.mycode.dialog.alert.MyAlert
import com.tkip.mycode.model.my_enum.ActType
import com.tkip.mycode.model.my_enum.PayType

/**
 * Hi~ Created by fabulous on 20-08-12
 */


val PAY_ACT_TYPE_LIST by lazy { arrayListOf<ActType>() }
var CHOSEN_PAY_TYPE:PayType? = null

fun ArrayList<ActType>.reset(vararg actTypes: ActType) {
    this.clear()
    actTypes.forEach { this.add(it) }
}

/** * 전체 결제할 크레딧 액수 받아오기 */
fun ArrayList<ActType>.getSum(): Int {
    var sum = 0
    this.forEach { it.getCrdAmount()?.let { amount -> sum += amount } }
    return sum
}


/*** show 계산서 */
fun checkAndShowPay(context: Context, ok: () -> Unit, cancel: () -> Unit) {
    if (PAY_ACT_TYPE_LIST.getSum() > 0) {
        MyAlert.showPay(context, ok, cancel)
    } else {
        resetActPay()
        ok()
    }
}

/*** 초기화 */
fun resetActPay() {
    PAY_ACT_TYPE_LIST.clear()
    CHOSEN_PAY_TYPE = PayType.CREDIT
}

