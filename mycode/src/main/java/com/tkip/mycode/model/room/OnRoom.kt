package com.tkip.mycode.model.room

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.view.View
import com.google.firebase.firestore.WriteBatch
import com.tkip.mycode.R
import com.tkip.mycode.dialog.progress.MyCircle
import com.tkip.mycode.dialog.toast.TToast
import com.tkip.mycode.init.*
import com.tkip.mycode.init.base.checkUpdate
import com.tkip.mycode.model.cost.Cost
import com.tkip.mycode.model.my_enum.ActType
import com.tkip.mycode.model.my_enum.Status
import com.tkip.mycode.model.my_enum.getStatus
import com.tkip.mycode.model.mytag.OnTag
import com.tkip.mycode.model.peer.checkAllAvailUpload
import com.tkip.mycode.model.peer.isNotAdmin
import com.tkip.mycode.nav.room.AddRoomActivity
import com.tkip.mycode.nav.room.RoomActivity
import com.tkip.mycode.util.lib.TransitionHelper
import com.tkip.mycode.util.lib.photo.Foto
import com.tkip.mycode.util.lib.photo.util.PhotoUtil
import com.tkip.mycode.util.lib.retrofit2.ESget
import com.tkip.mycode.util.tools.etc.OnMyClick
import com.tkip.mycode.util.tools.fire.*

class OnRoom {

    companion object {

        fun gotoAddRoomActivity(context: Context, action: Int, room: Room?, arrPair: Array<androidx.core.util.Pair<View, String>>?) {

            /*** 업로드 가능 여부 체크 */
            if (checkAllAvailUpload(context)) return

            val intent = Intent(context, AddRoomActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
            intent.putExtra(PUT_ACTION, action)
            room?.let { intent.putExtra(PUT_ROOM, it) }
            TransitionHelper.startTran(context, intent, arrPair)
        }
//
//        fun gotoRoomActivity(context: Context, isTop: Boolean, action: Int?, room: Room, arrPair: Array<androidx.core.util.Pair<View, String>>?) {
//            val intent = Intent(context, RoomActivity::class.java)
//            if (isTop) intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
//            action?.let { intent.putExtra(PUT_ACTION, it) }
//            intent.putExtra(PUT_ROOM, room)
//            TransitionHelper.startTran(context, intent, arrPair)
//        }


        fun gotoRoomActivity(context: Context, isTop: Boolean, addType: Int?, room: Room, arrPair: Array<androidx.core.util.Pair<View, String>>?) {
            TransitionHelper.startTran(context, getIntent(context, isTop, addType, room), null)
        }

        private fun getIntent(context: Context, isTop: Boolean, addType: Int?, room: Room) =
                Intent(context, RoomActivity::class.java).apply {
                    if (isTop) addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
                    addType?.let { putExtra(PUT_ACTION, it) }
                    putExtra(PUT_ROOM, room)
                }

        fun gotoRoomActivity(context: Context, isTop: Boolean, addType: Int?, modelKey: String, arrPair: Array<androidx.core.util.Pair<View, String>>?) {
            TransitionHelper.startTran(context, getIntent(context, isTop, addType, modelKey), arrPair)
        }

        fun getIntent(context: Context, isTop: Boolean, addType: Int?, modelKey: String?) =
                Intent(context, RoomActivity::class.java).apply {
                    if (isTop) addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
                    addType?.let { putExtra(PUT_ACTION, it) }
                    putExtra(PUT_KEY, modelKey)
                }


        /******************************************************************************
         ****************************** upload ****************************************
         *****************************************************************************/
        fun addPhotoAndRoom(addType: Int, activity: Activity, widthOrigin: Int?, selectedFotoList: ArrayList<Foto>, preRoom: Room, room: Room,
                            success: (() -> Unit)?, fail: () -> Unit) {

            PhotoUtil.uploadUriList(activity, widthOrigin, room, preRoom.fotoList, selectedFotoList,
                    successUploading = { finalList ->

                        room.fotoList = finalList

                        /** gotoFire */


                        batch().apply {
                            when (addType) {
                                ACTION_NEW -> this.set(docRef(EES_ROOM, room.key), room)
                                ACTION_UPDATE -> this.update(docRef(EES_ROOM, room.key), makeMap(preRoom, room))
                            }

                            /** 계산서 올리기 */
                            if (isNotAdmin(null, null)) this.addCostWithList(room)
                            /** 신규 MyTag 올리기 */
                            OnTag.addToBatchAllMytag(this)

                        }.myCommit("addPhotoAndRoom",
                                success = {
                                    when (addType) {
                                        ACTION_NEW -> {
                                            completedDB(activity, room)
                                            Cost.new(null, ActType.ADD_ROOM, room).real()
                                        }
                                        ACTION_UPDATE -> completedDB(activity, room)
                                    }
                                }, fail = {})
                    },
                    fail = { fail() })
        }

        private fun completedDB(activity: Activity, room: Room) {
            OnTag.clearMyTagList()
            TToast.showNormal(R.string.complete_upload)
            gotoRoomActivity(activity, true, ACTION_NORMAL, room, null)
            activity.finish()
        }

        private fun getBatchRoom(addType: Int, preRoom: Room, room: Room): WriteBatch =
                batch().apply {
                    when (addType) {
                        ACTION_NEW -> {
                            this.set(docRef(EES_ROOM, room.key), room)
                            this.addCostWithList(room)
//                            this.addAct(Cost.new(null,ActType.ADD_ROOM,  room))
                        }
                        ACTION_UPDATE -> this.update(docRef(EES_ROOM, room.key), makeMap(preRoom, room))
                    }
                    OnTag.addToBatchAllMytag(this)
                }


        private fun makeMap(preData: Room, data: Room) = mutableMapOf<String, Any?>().apply {

            checkUpdate(preData.contractTypeNo, data.contractTypeNo, CH_CONTRACT_TYPE_NO)
            checkUpdate(preData.roomStatusNo, data.roomStatusNo, CH_ROOM_STATUS_NO)
            checkUpdate(preData.roomTypeNo, data.roomTypeNo, CH_ROOM_TYPE_NO)

            checkUpdate(preData.constructionDate, data.constructionDate, CH_CONSTRUCTION_DATE)
            checkUpdate(preData.areaTotal, data.areaTotal, CH_AREA_TOTAL)
            checkUpdate(preData.areaEx, data.areaEx, CH_AREA_EX)

            checkUpdate(preData.countRoom, data.countRoom, CH_COUNT_ROOM)
            checkUpdate(preData.countToilet, data.countToilet, CH_COUNT_TOILET)
            checkUpdate(preData.countKitchen, data.countKitchen, CH_COUNT_KITCHEN)
            checkUpdate(preData.countLiving, data.countLiving, CH_COUNT_LIVING)
            checkUpdate(preData.countBath, data.countBath, CH_COUNT_BATH)
            checkUpdate(preData.countFloor, data.countFloor, CH_COUNT_FLOOR)
            checkUpdate(preData.countParking, data.countParking, CH_COUNT_PARKING)

            data.fotoList?.let { put(CH_FOTO_LIST, it) }  // fotolist 는 비교하기가 너무 까다롭다. 그냥 무조건 업데이트할 것.

            checkUpdate(preData.minTerm, data.minTerm, CH_MIN_TERN)
            checkUpdate(preData.price, data.price, CH_PRICE)
            checkUpdate(preData.inAdvanced, data.inAdvanced, CH_INADVANCED)
            checkUpdate(preData.deposit, data.deposit, CH_DEPOSIT)

            checkUpdate(preData.title, data.title, CH_TITLE)
            checkUpdate(preData.local, data.local, CH_LOCAL)
            checkUpdate(preData.lat, data.lat, CH_LAT)
            checkUpdate(preData.lng, data.lng, CH_LNG)
            checkUpdate(preData.address, data.address, CH_ADDRESS)

            checkUpdate(preData.contact, data.contact, CH_CONTACT)
            data.mediaMap?.let { put(CH_MEDIA_MAP, it) }
            checkUpdate(preData.comment, data.comment, CH_COMMENT)
            data.builtInList?.let { put(CH_BUILT_IN_LIST, it) }
            data.tagList?.let { put(CH_TAG_LIST, it) }


            put(CH_ALLOW_REPLY, data.allowReply)
            if (isNotAdmin(null, null)) put(CH_UPDATE_DATE, System.currentTimeMillis())

        }


        /**********************************************************************
         * Room 받고   * status 체크하고         * 정상이면 activity 이동
         ***********************************************************************/
        fun checkAndGo(context: Context, isTop: Boolean, key: String, action: Int?, status: Status, arrPair: Array<androidx.core.util.Pair<View, String>>?, onSuccess: ((Room) -> Unit)?, onNotNormal: ((Room) -> Unit)?) {
            if(OnMyClick.isDoubleClick()) return

            ESget.getRoom(key,
                    success = { room1 ->
                        if (room1 == null) {
                            TToast.showNormal(R.string.error_data_deleted)
                            MyCircle.cancel()
                        } else {
                            if (room1.statusNo >= status.no) {
                                onSuccess?.invoke(room1)
                                gotoRoomActivity(context, isTop, action, room1, arrPair)
//                        MyCircle.cancel()
                            } else {
                                onNotNormal?.invoke(room1)
                                TToast.showAlert(room1.statusNo.getStatus().msg)
                                MyCircle.cancel()
                            }
                        }

                    }, fail = {})
        }

        /*** 상태체크 없이 그냥 고 */
//        fun justGo(context: Context, isTop: Boolean, room: Room, arrPair: Array<androidx.core.util.Pair<View, String>>? ) {
//            gotoRoomActivity(context, isTop, null, room, arrPair)
//        }


        fun updateStatus(key: String, status: Status, success: () -> Unit, fail: (Exception) -> Unit) {
            commonStatusUpdate(EES_ROOM, key, status, success = { success() }, fail = { fail(it) })
        }


    }

}