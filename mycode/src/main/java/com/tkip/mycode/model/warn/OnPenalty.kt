package com.tkip.mycode.model.warn

import android.content.Context
import com.tkip.mycode.R
import com.tkip.mycode.dialog.alert.MyAlert
import com.tkip.mycode.dialog.progress.WAIT_TYPE
import com.tkip.mycode.dialog.toast.toastNormal
import com.tkip.mycode.init.EES_PENALTY
import com.tkip.mycode.init.base.getStr
import com.tkip.mycode.model.peer.mypenaltyReason
import com.tkip.mycode.util.tools.date.getDayCount
import com.tkip.mycode.util.tools.fire.batch
import com.tkip.mycode.util.tools.fire.docRef
import com.tkip.mycode.util.tools.fire.myCommit

/**
 * Hi~ Created by fabulous on 20-07-16
 */
class OnPenalty {
    companion object {

        /**
         * for admin
         * 유저 패널티 처리하기
         */
        fun handle(context: Context, warn: Warn, success: () -> Unit, fail: () -> Unit) {

            // 다시 한번 물어볼 것.
            MyAlert.showConfirm(context, R.string.status_dialog_penalty_handled.getStr(),
                    ok = {
                        WAIT_TYPE.FULL_AV.show(context)

                        batch().apply {

                            /*** add 패널티 : 패널티 날짜 기입했을 때만 */
                            if (warn.period == null) warn.deletePenalty()
                            else set(docRef(EES_PENALTY, warn.key), warn)   // functions에서 peer 수정.

                            myCommit("OnWarnHandle",
                                    success = {
                                        success()
                                        toastNormal(R.string.info_penalty_add_success)
                                    }, fail = { fail() })
                        }
                    }, cancel = { fail() })
        }


        fun dateMessage(date: Long): String {
            val tri = date.getDayCount()
            val days = tri.first
            val hours = tri.second
            val mins = tri.third


            /*** 남은 시간 : null이 아니면 항목 넣기 */
            val remainingTime = StringBuffer().apply {
                when {
                    /*** 날 + 시간 */
                    (days != null) -> {
                        append(String.format(R.string.day.getStr(), days))
                        hours?.let { append(String.format(R.string.hour.getStr(), hours)) }
                    }
                    /*** 시간 + 분 */
                    (hours != null) -> {
                        append(String.format(R.string.hour.getStr(), hours))
                        mins?.let { append(String.format(R.string.min.getStr(), mins)) }
                    }
                    /*** 분 */
                    (mins != null) -> append(String.format(R.string.min.getStr(), mins))
                }
            }

            val strRemainingTime = String.format(R.string.status_msg_peer_penalty.getStr(), remainingTime)

            return "$strRemainingTime \n\n ${mypenaltyReason()}"
        }

    }
}