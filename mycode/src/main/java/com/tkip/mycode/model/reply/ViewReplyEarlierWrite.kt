package com.tkip.mycode.model.reply

import android.content.Context
import android.view.LayoutInflater
import android.widget.LinearLayout
import androidx.databinding.DataBindingUtil
import com.tkip.mycode.R
import com.tkip.mycode.databinding.ViewReplyEarlierWriteBinding
import com.tkip.mycode.init.inter.interReplyEarlierWrite
import com.tkip.mycode.nav.lobby.chat.Chat
import com.tkip.mycode.util.tools.anim.hideSlideDown
import com.tkip.mycode.funs.common.lllogI
import com.tkip.mycode.util.tools.anim.showSlideUp


class ViewReplyEarlierWrite : LinearLayout {
    companion object {
        fun add(context: Context, parent: LinearLayout, reply: Reply?, chat: Chat?, onClick: ((key: String) -> Unit)?, onClose: (() -> Unit)?) {
            val view = ViewReplyEarlierWrite(context, reply, chat, onClick, onClose)
            parent.removeAllViews()
            parent.addView(view)
            parent.showSlideUp(true)
        }
    }

    private lateinit var b: ViewReplyEarlierWriteBinding

    constructor(context: Context) : super(context)
    constructor(context: Context, reply: Reply?, chat: Chat?, onClick: ((key: String) -> Unit)?, onClose: (() -> Unit)?) : super(context) {

        b = DataBindingUtil.inflate(LayoutInflater.from(context), R.layout.view_reply_earlier_write, this, true)
        reply?.let {
            b.authorNick = it.authorNick
            b.comment = it.comment
            b.statusNo = it.statusNo
            b.thumbUrl = it.thumbUrl
        }
        chat?.let {
            b.authorNick = it.authorNick
            b.comment = it.comment
            b.statusNo = it.statusNo
            b.thumbUrl = it.thumbUrl
        }

        b.inter = interReplyEarlierWrite(
                onClickLine = {
                    //                    onClick(reply)
                },
                onClickClose = {
                    lllogI("ViewReplyEarlierWrite onClick")
                    b.lineContents.hideSlideDown(true)
                    onClose?.invoke()
                })


    }

}