package com.tkip.mycode.model.my_enum

import android.content.Context
import android.view.View
import androidx.core.util.Pair
import com.tkip.mycode.R
import com.tkip.mycode.dialog.OnDDD
import com.tkip.mycode.dialog.alert.MyAlert
import com.tkip.mycode.dialog.toast.TToast
import com.tkip.mycode.dialog.toast.toastAlert
import com.tkip.mycode.dialog.toast.toastNormal
import com.tkip.mycode.funs.common.lllogD
import com.tkip.mycode.init.*
import com.tkip.mycode.init.base.getI
import com.tkip.mycode.init.base.getStr
import com.tkip.mycode.init.base.gte
import com.tkip.mycode.init.base.lt
import com.tkip.mycode.model.bid.Bid
import com.tkip.mycode.model.board.Board
import com.tkip.mycode.model.board.OnBoard
import com.tkip.mycode.model.cost.Cost
import com.tkip.mycode.model.cost.pay.PAY_ACT_TYPE_LIST
import com.tkip.mycode.model.count.*
import com.tkip.mycode.model.flea.Flea
import com.tkip.mycode.model.flea.OnFlea
import com.tkip.mycode.model.grant.Grant
import com.tkip.mycode.model.manage.Device
import com.tkip.mycode.model.mytag.MyTag
import com.tkip.mycode.model.peer.OnPeer
import com.tkip.mycode.model.peer.Peer
import com.tkip.mycode.model.peer.isNotAdmin
import com.tkip.mycode.model.post.OnPost
import com.tkip.mycode.model.post.Post
import com.tkip.mycode.model.reply.OnReply
import com.tkip.mycode.model.reply.Reply
import com.tkip.mycode.model.room.OnRoom
import com.tkip.mycode.model.room.Room
import com.tkip.mycode.model.store.Store
import com.tkip.mycode.model.store.StoreMenu
import com.tkip.mycode.model.ticket.Ticket
import com.tkip.mycode.model.toss.Toss
import com.tkip.mycode.model.warn.Warn
import com.tkip.mycode.nav.store.OnStore
import com.tkip.mycode.util.lib.elastic.OnDBtoES
import com.tkip.mycode.util.lib.photo.util.PhotoUtil
import com.tkip.mycode.util.lib.retrofit2.ESget
import com.tkip.mycode.util.lib.retrofit2.ESquery
import com.tkip.mycode.util.tools.fire.docRef


/********************************   *****************************************/

//            val index =    when (any) {
//
//                is Board -> EES_BOARD
//                is Post -> EES_BOARD
//                is Flea -> EES_BOARD
//                is Room -> EES_BOARD
//                is Store -> EES_BOARD
//                is StoreMenu-> EES_BOARD
//
//                is MyTag -> EES_BOARD
//                is Warn -> EES_BOARD
//                is Bid -> EES_BOARD
//                is Cost -> EES_BOARD
//                is  Reply-> EES_BOARD
//                is  -> EES_BOARD
//                is  -> EES_BOARD
//                is  -> EES_BOARD
//                is  -> EES_BOARD
//                else -> EES_SYSTEM
//            }


/**
 * 단순 status 체크
 */
fun Any.gteStatus(status: Status, hasToast: Boolean) =
        when (this) {
            is Board -> this.statusNo.checkGteStatus(status, hasToast)
            is Post -> this.statusNo.checkGteStatus(status, hasToast)
            is Room -> this.statusNo.checkGteStatus(status, hasToast)
            is Flea -> this.statusNo.checkGteStatus(status, hasToast)
            is Store -> this.statusNo.checkGteStatus(status, hasToast)
            is Reply -> this.statusNo.checkGteStatus(status, hasToast)
            else -> false
        }

private fun Int.checkGteStatus(status: Status, hasToast: Boolean): Boolean {
    val result = this >= status.no
    if (!result && hasToast) toastAlert(this.getStatus().msg)
    return result
}


/*************************************************************************************************/
/********************  Model 받아오기 : onSuccess(Any) Board,Post,Flea,Room,Store ********************/
/*************************************************************************************************/
fun Count.getModel(onSuccess: ((Any?) -> Unit)?) {
    this.dataTypeNo.dataType().getModel(this.modelKey, onSuccess)
}

/*************************************************************************************************/
/********************  Model 이동 : onSuccess(Any) Board,Post,Flea,Room,Store ********************/
/*************************************************************************************************/
private fun commonCheckAndGo(context: Context, any: Any, isTop: Boolean, arrPair: Array<Pair<View, String>>?, onSuccess: ((Any) -> Unit)?) {
    lllogD("ViewTossModel commonCheckAndGo any $any")
    any.anyDataTypeNo()?.let {
        any.anyModelKey()?.let { modelKey ->
            lllogD("ViewTossModel commonCheckAndGo dataType ${it.dataType()}")
            it.dataType().checkAndGo(context, isTop, modelKey, arrPair, onSuccess)
        }
    }
}

fun Count.checkAndGo(context: Context, isTop: Boolean, arrPair: Array<androidx.core.util.Pair<View, String>>?, onSuccess: ((Any) -> Unit)?) {
    commonCheckAndGo(context, this, isTop, arrPair, onSuccess)
}

fun Bid.checkAndGo(context: Context, isTop: Boolean, arrPair: Array<androidx.core.util.Pair<View, String>>?, onSuccess: ((Any) -> Unit)?) {
    commonCheckAndGo(context, this, isTop, arrPair, onSuccess)
}

fun Toss.checkAndGo(context: Context, isTop: Boolean, arrPair: Array<androidx.core.util.Pair<View, String>>?, onSuccess: ((Any) -> Unit)?) {
    commonCheckAndGo(context, this, isTop, arrPair, onSuccess)
}

fun Cost.checkAndGo(context: Context, isTop: Boolean, arrPair: Array<androidx.core.util.Pair<View, String>>?, onSuccess: ((Any) -> Unit)?) {
    commonCheckAndGo(context, this, isTop, arrPair, onSuccess)
}


fun Grant.goto(context: Context, arrPair: Array<androidx.core.util.Pair<View, String>>?) {
    this.dataTypeNo.dataType().justGo(context, true, this.modelKey, arrPair)
}


/*************************************************************************************/
/********************************  단순 대입  *****************************************/
/*************************************************************************************/

/** * key 받아오기 */
fun Any.anyKey() =
        when (this) {
            is Post -> this.key
            is Board -> this.key
            is Room -> this.key
            is Flea -> this.key
            is Store -> this.key
            is StoreMenu -> this.key

            is Reply -> this.key

            is Warn -> this.key
            is MyTag -> this.key
            is Bid -> this.key
            is Count -> this.key

            is Peer -> this.uid
            is Device -> this.key
            else -> ""
        }

/** * title 받아오기 */
fun Any.anyTitle() =
        when (this) {
            is Post -> this.title
            is Board -> this.title
            is Room -> this.title
            is Flea -> this.title
            is Store -> this.title
            is Reply -> this.comment

            is Warn -> this.targetNick
            is MyTag -> this.name
            is Bid -> this.title
            is Count -> this.key
            else -> ""
        }

/** * title 받아오기 */
fun Any.anyComment() =
        when (this) {
            is Post -> this.comment
            is Board -> this.comment
            is Room -> this.comment
            is Flea -> this.comment
            is Store -> this.comment
            is Reply -> this.comment

            is Warn -> this.comment
            is MyTag -> this.name
            is Bid -> this.comment
            is Count -> this.key
            else -> ""
        }

/** * statusNo 받아오기 */
fun Any.anyStatusNo() =
        when (this) {
            is Post -> this.statusNo
            is Board -> this.statusNo
            is Room -> this.statusNo
            is Flea -> this.statusNo
            is Store -> this.statusNo
            is Reply -> this.statusNo

            is Warn -> this.statusNo
            is MyTag -> this.statusNo
            is Bid -> this.statusNo
            else -> Status.WRITING.no
        }

/** * authorUID 받아오기 */
fun Any.anyAuthorUid(): String =
        when (this) {
            is Post -> this.authorUID
            is Board -> this.authorUID
            is Room -> this.authorUID
            is Flea -> this.authorUID
            is Store -> this.authorUID
            is Reply -> this.authorUID

            is Warn -> this.authorUID
            is MyTag -> this.authorUID
            is Bid -> this.uid
            is Count -> this.uid
            else -> ""
        }

/** * authorUID 받아오기 */
fun Any.anyAuthorNick(): String =
        when (this) {
            is Post -> this.authorNick
            is Board -> this.authorNick
            is Room -> this.authorNick
            is Flea -> this.authorNick
            is Store -> this.authorNick
            is Reply -> this.authorNick

            is Warn -> this.authorNick
            is MyTag -> this.authorNick
            else -> ""
        }


/** * thumbFoto[0] 받아오기 */
fun Any.anyThumb(i: Int) =
        when (this) {
            is Post -> this.fotoList.getI(i)?.thumbUrl
            is Board -> this.photoThumb
            is Room -> this.fotoList.getI(i)?.thumbUrl
            is Flea -> this.fotoList.getI(i)?.thumbUrl
            is Store -> this.fotoList.getI(i)?.thumbUrl
            is Reply -> this.thumbUrl
            else -> null
        }

/** * fotoList 전체 딥삭제 */
fun Any.deepDelAllFoto() {
    when (this) {
        is Post -> fotoList?.let { PhotoUtil.deletePhoto(it) }
        is Room -> fotoList?.let { PhotoUtil.deletePhoto(it) }
        is Flea -> fotoList?.let { PhotoUtil.deletePhoto(it) }
        is Store -> fotoList?.let { PhotoUtil.deletePhoto(it) }
        is Board -> PhotoUtil.deletePhoto(photoThumb, photoUrl)
        is Reply -> PhotoUtil.deletePhoto(thumbUrl, url)
        else -> {
        }
    }
}

/** * urlFoto[0] 받아오기 */
fun Any.anyUrl(i: Int) =
        when (this) {
            is Post -> this.fotoList.getI(i)?.url
            is Board -> this.photoUrl
            is Room -> this.fotoList.getI(i)?.url
            is Flea -> this.fotoList.getI(i)?.url
            is Store -> this.fotoList.getI(i)?.url
            is Reply -> this.url
            else -> null
        }

fun Any.anyChild() = this.getMyDataType().child


/** * boardKey 받아오기 */
fun Any.anyBoardKey() =
        when (this) {
            is Post -> this.boardKey
            is Room -> ROOM_BOARD_KEY
            is Flea -> FLEA_BOARD_KEY
            else -> null
        }


/** * AddDate 받아오기 */
fun Any.anyAddDate() =
        when (this) {
            is Post -> this.addDate
            is Board -> this.addDate
            is Flea -> this.addDate
            is Room -> this.addDate
            is Store -> this.addDate

            is Reply -> this.addDate
            is Warn -> this.addDate
            is MyTag -> this.addDate
            is Bid -> this.addDate
            is Count -> this.addDate
            else -> 0L
        }

/** * dataTypeNo 받아오기 */
fun Any.anyDataTypeNo() =
        when (this) {
            is Cost -> dataTypeNo
            is Warn -> dataTypeNo
            is Reply -> dataTypeNo

            is Bid -> dataTypeNo
            is Count -> dataTypeNo
            is Toss -> dataTypeNo
            else -> null
        }

/** * dataTypeNo 받아오기 */
fun Any.anyModelKey() =
        when (this) {
            is Cost -> modelKey
            is Warn -> modelKey
            is Reply -> modelKey

            is Bid -> modelKey
            is Count -> modelKey
            is Toss -> modelKey
            else -> null
        }

/** * adCredit 받아오기 */
fun Any.anyAdCredit() =
        when (this) {
            is Store -> this.adCredit
            is Room -> this.adCredit
            is Flea -> this.adCredit
            is Board -> this.adCredit
            else -> 0
        }

/** * adCredit 받아오기 */
fun Any.anyTotalAdCredit() =
        when (this) {
            is Store -> this.totalAdCredit
            is Room -> this.totalAdCredit
            is Flea -> this.totalAdCredit
            is Board -> this.totalAdCredit
            else -> 0
        }

/**
 * 모델 status 검사.
 */
fun Any.isValidStatus(gteStatus: Status): Boolean {
    return if (this.anyStatusNo().gte(gteStatus.no)) true
    else {
        TToast.showAlert(gteStatus.msg)
        false
    }
}

/*************************************************************************************/

/*** 상태체크 없이 그냥 고 */
fun Any.justGo(context: Context, isTop: Boolean, arrPair: Array<androidx.core.util.Pair<View, String>>?) =
        when (this) {
            is Board -> OnBoard.gotoBoardActivity(context, isTop, null, this, arrPair)
            is Post -> OnPost.gotoPostActivity(context, isTop, null, this, arrPair)
            is Flea -> OnFlea.gotoFleaActivity(context, isTop, null, this, arrPair)
            is Room -> OnRoom.gotoRoomActivity(context, isTop, null, this, arrPair)
            is Store -> OnStore.gotoStoreActivity(context, isTop, null, this, arrPair)
            is Reply -> OnReply.gotoModelActivity(context, isTop, null, this, arrPair)
            else -> {
            }
        }

/*************************************************************************************/
/********************************  Storage *****************************************/
/*************************************************************************************/

fun Any.getStorageChild(): String {
    return when (this) {
        is Post -> ST_POST
        is Reply -> ST_REPLY
        is Flea -> ST_FLEA
        is Board -> ST_BOARD
        is Room -> ST_ROOM
        is Store -> ST_STORE
        is StoreMenu -> ST_STORE_MENU
        is Peer -> PhotoUtil.child(ST_PEER, OnPeer.myUid())
        is Bid -> ST_BID
        is Ticket -> ST_TICKET
        else -> ST_NO_CHILD
    }
}


/*************************************************************************************/
/********************************  DataType *****************************************/
/*************************************************************************************/

fun Any.getMyDataType(): DataType {
    return when (this) {
        is Count -> DataType.COUNT
        is Post -> DataType.POST
        is Reply -> DataType.REPLY
        is Board -> DataType.BOARD
        is Flea -> DataType.FLEA
        is Room -> DataType.ROOM
        is Store -> DataType.STORE

        is Peer -> DataType.PEER
        is Bid -> if (hitDate == null) DataType.BANNER else DataType.BID
        is Warn -> DataType.WARN
        is MyTag -> DataType.MYTAG
        is Grant -> DataType.GRANT

        else -> DataType.NOTHING
    }
}


/**
 * Board, Post, Flea, Room, Store, Reply
 */
fun Any?.showStatusDialog(context: Context, onSuccess: ((status: Status?, grant: Grant?) -> Unit)?) {
    if (isNotAdmin(null, null)) return
    if (this == null) toastNormal(R.string.db_no_data)
    else MyAlert.showUpdateStatus(context, this, onSuccess)
}


/** * NORMAL 미만*/
fun Any.ltNormalStatus() = this.anyStatusNo().lt(Status.NORMAL.no)

/** * NORMAL 이상 */
fun Any.gteNormalStatus() = this.anyStatusNo().gte(Status.NORMAL.no)


/*************************************************************************************/
/********************************  sttDeepDel *****************************************/
/*************************************************************************************/

/**
 * model 딥삭제
 */
fun Any.deepDel(context: Context, onSuccess: (() -> Unit)?) {
    OnDDD.deepDel(context, ok = {
        when (this) {
            is Post, is Flea, is Room -> deepDelModelAndCount(context, onSuccess)
            //todo : 원래는 cost랑 act도 지워야 하는데..  그걸 지우면 나중에 크레딧의 SUM을 확인 해야할 때, 계산이 틀려져서...
            is Reply -> {
            }
            is Peer -> {

            }
            is Count -> {
            }
            is Bid -> {
            }
            is Warn -> {
            }
            is MyTag -> {
            }
            is Grant -> {
            }

            else -> {
            }
        }
    }, cancel = {})

}


/**
 *     1. 모델 삭제
 *     2. Count 삭제
 *     3. Quick 삭제
 */
private fun Any.deepDelModelAndCount(context: Context, onSuccess: (() -> Unit)?) {

    OnDBtoES.clear()
    var index = 0
    var countListSize = 0
    val arr = when (this) {
        is Post -> COUNT_TYPE_POST
        is Flea, is Room -> COUNT_TYPE_FLEA_BOOK
        else -> COUNT_TYPE_BOARD_STORE  // board, store
    }

    arr.forEach { countType ->

        /*** 1. Count 받아오기 */
        val topChild = countType.getTopChild(this)
        ESget.getCountList("deepDel $topChild", topChild, ESquery.countModel(null, null, anyKey()),
                success = { countList ->
                    countListSize += countList?.size ?: 0     // countList는 여러개라서 여기서 취합해야..

                    /*** esList에 삽입 */
                    countList?.forEach { count ->
                        lllogD("deepDelModelAndCount countType $countType topChild $topChild key ${count.key}")
                        OnDBtoES.add(Triple(docRef(topChild, count.key), null, null))
                    }    // delete Count

                    index++
                    if (index == arr.size) {

                        /*** 2. Quick 받아오기 */
                        ESget.getCountList("deepDel Quick", EES_QUICK, ESquery.quickModel(null, anyKey()),
                                success = { quickList ->

                                    /*** esList에 삽입 */
                                    quickList?.forEach { count -> OnDBtoES.add(Triple(docRef(EES_QUICK, count.key), null, null)) }    // delete Quick

                                    /*** 3. Reply 받아오기 */
                                    ESget.getReplyList(ESquery.replyModel(null, anyKey()),
                                            success = { replyList ->
                                                replyList?.forEach { reply -> OnDBtoES.add(Triple(docRef(EES_POST, reply.modelKey, EES_REPLY, reply.key), null, null)) }    // delete Reply

                                                /*** 해당 모델 삭제 */
                                                OnDBtoES.add(Triple(docRef(this.anyChild(), anyKey()), null, null))             // delete Model

                                                /*** 토탈 사이즈 보고 */
                                                OnDDD.confirm(context, String.format(R.string.ask_deep_del_model.getStr(), OnDBtoES.esList.size, countListSize, quickList?.size, replyList?.size),
                                                        ok = {
                                                            OnDDD.pw(context) {
                                                                OnDBtoES.addToFireS("deepDelCount ${this.anyTitle()}",
                                                                        onSuccess = {
                                                                            /*** 사진 삭제 */
                                                                            this.deepDelAllFoto()
                                                                            replyList?.forEach { reply -> reply.deepDelAllFoto() }
                                                                            OnDDD.info(context, R.string.complete_delete)
                                                                            onSuccess?.invoke()
                                                                        })
                                                            }
                                                        }, cancel = {})


                                            }, fail = {})
                                }, fail = {})


                    }

                }, fail = {})

    }
}


/**
 * todo: 탈퇴처리까지 할 것.
 * 1. peer
 * 2. count
 * 3. cost
 * 4. act
 * 5. model - board, post, flea, room, store, reply
 */
private fun Peer.deepDel(context: Context, onSuccess: (() -> Unit)?) {

}


