package com.tkip.mycode.model.cost.credit

import android.os.Bundle
import androidx.annotation.LayoutRes
import androidx.fragment.app.Fragment
import com.tkip.mycode.R
import com.tkip.mycode.databinding.ActivityCostListBinding
import com.tkip.mycode.init.PUT_MANAGE
import com.tkip.mycode.init.PUT_UID
import com.tkip.mycode.init.base.getStr
import com.tkip.mycode.init.inter.interToolbarIcon
import com.tkip.mycode.init.my_bind.BindingActivity
import com.tkip.mycode.model.my_enum.CostType
import com.tkip.mycode.util.tools.fire.Firee
import com.tkip.mycode.util.tools.view_pager.OnPager


/**
 *  1. 나의 크레딧 내역 :  credit, point
 *  2. peer 전체 활동내역 : credit, point, act
 *  탭 < ACT, PAYMENT, TAKE, SEND, CONVERT >
 */
class CostListActivity : BindingActivity<ActivityCostListBinding>() {
    @LayoutRes
    override fun getLayoutResId() = R.layout.activity_cost_list

    private val fragList = arrayListOf<Pair<Fragment, String>>()
    private val manage by lazy { intent.getBooleanExtra(PUT_MANAGE,false) }
    private val uid by lazy { intent.getStringExtra(PUT_UID) }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        initView()
    }

    private fun initView() {
        b.activity = this

        /*** Peer 가져오기 : 상단 닉네임 셋팅 */
        if (manage) Firee.getPeer(uid, success = { it?.let { peer -> b.inToolbar.topTitle = peer.nick } }, fail = {})


        /** 툴바 */
        b.inToolbar.title = R.string.menu_title_credit_point_list.getStr()
        b.inToolbar.backIcon = true
//        b.inToolbar.menuText = R.string.add_ad_unit_manage.getStr()
        b.inToolbar.inter = interToolbarIcon(onFirst = { onBackPressed() }, onTopTitle = {}, onTextMenu = {},onTextMenu2 = null, onIcon = {}, onMore = {})
//        b.frame.addView(CustomSearchTotalPager(this@SearchTotalActivity,supportFragmentManager,manage,uid,countType, onItemClick = {}))


        /*** tab & viewPager 세팅 */
        if (manage) fragList.add(Pair(ActFrag.newInstance(uid, null), CostType.ACT.title))   //   ActFrag  추가  for admin Only

        arrayOf(CostType.PAYMENT, CostType.TAKE, CostType.SEND, CostType.CONVERT)
                .forEach { fragList.add(Pair(CostFrag.newInstance(uid, it.no, null), it.title)) }  // add the Rest CostFrag

        OnPager.commonSetPager(supportFragmentManager, fragList, b.viewPager, b.tabLayout)
//        b.viewPager.onPageChange({}, { _, _, _ -> }, onSelected = { })


    }


}