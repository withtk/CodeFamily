package com.tkip.mycode.model.ticket

import com.tkip.mycode.R


/**
 *  결제 이벤트
 */


/******* BILL EVENT *****************/
//val BILL_EVENT_FIRST by lazy {1000}
/*********************************/

enum class BillEvent(val no: Int, val title: Int) {
    NOTHING(10, R.string.tab_title_nothing),
    FIRST(1000, R.string.title_bill_event_first),   // 첫 결제 이벤트
    FREE_MONTH(1200, R.string.title_bill_free_month),  // 매달 무료 이벤트
    ;

//    fun getTitle() = this.title.getStr()

    /*** 이벤트 처리  */
    fun handleEvent(ticket: Ticket) {
        when (this) {
            FIRST -> {
                /***  첫 결제 + 100% */
                ticket.bonusPercent = 100
                ticket.extra = ticket.crd
                ticket.bcrd = ticket.crd
                ticket.pnt = ticket.crd?.let { it * 2 }
                ticket.eventNo = this.no
            }
            else -> {
            }
        }
    }

}


fun Int.billEvent(): BillEvent {
    BillEvent.values().forEach {
        if (it.no == this) return it
    }
    return BillEvent.NOTHING
}

//
//
//fun String.isMskuContains(): Boolean {
//    Msku.values().forEach { if (it.id == this) return true }
//    return false
//}
