package com.tkip.mycode.model.peer.peer_uid

import android.os.Parcel
import android.os.Parcelable
import com.tkip.mycode.init.base.createNanoKey
import com.tkip.mycode.model.peer.Peer
import com.tkip.mycode.model.peer.myuid
import com.tkip.mycode.util.tools.date.rightNow

/**
 * 간편 peer
 * for login, leave, online
 *
 */
data class PeerUID(
        var key: String = createNanoKey(),   // == UID
        var addDate: Long = rightNow()

) : Parcelable {

    constructor(peer: Peer) : this(
            key = peer.uid
    )

    constructor(source: Parcel) : this(
            source.readString()!!,
            source.readLong()
    )

    override fun describeContents() = 0

    override fun writeToParcel(dest: Parcel, flags: Int) = with(dest) {
        writeString(key)
        writeLong(addDate)
    }

    companion object {
        val peerMap = hashMapOf<String, PeerUID>()

        @JvmField
        val CREATOR: Parcelable.Creator<PeerUID> = object : Parcelable.Creator<PeerUID> {
            override fun createFromParcel(source: Parcel): PeerUID = PeerUID(source)
            override fun newArray(size: Int): Array<PeerUID?> = arrayOfNulls(size)
        }
    }
}









