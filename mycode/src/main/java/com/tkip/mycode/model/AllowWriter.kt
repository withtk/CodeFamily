package com.tkip.mycode.model

import android.os.Parcel
import android.os.Parcelable
import com.tkip.mycode.model.peer.OnPeer
import com.tkip.mycode.util.tools.date.rightNow

/**
 *  적용 : Board, Peer
 *
 *  포스팅 허용 & 불허용
 *  댓글 허용 & 불허용
 *  나에게 챗팅 허용 & 불허용   -> 나의 peer 밑에.
 *  key는 uid로.
 *
 */
data class AllowWriter(

//        var key: String = createKey(),
        var uid: String = OnPeer.myUid(),
//        var payTypeNo: Int = PayType.CREDIT.no,     // pay, credit, point
//        var amount: Int = 10,                       // 최소10, 광고 입찰한 가격(필요할때만 쓰임)
//
//        /////////////////// 광고되는 개체 ///////////////////////////////////
//        var modelKey: String = "storeKey",
//        var modelTitle: String = "storeName",
//        var photoThumb: String? = null,
//        ///////////////////////////////////////////////////////////////////
//
//        var term: Int = 1,            // 광고게재 기간
//        var startDate: Int= 190101,   // 시작 날짜
        var addDate: Long = rightNow()

) : Parcelable {

    constructor(source: Parcel) : this(
            source.readString()!!,
            source.readLong()
    )

    override fun describeContents() = 0

    override fun writeToParcel(dest: Parcel, flags: Int) = with(dest) {
        writeString(uid)
        writeLong(addDate)
    }

    companion object {
        @JvmField
        val CREATOR: Parcelable.Creator<AllowWriter> = object : Parcelable.Creator<AllowWriter> {
            override fun createFromParcel(source: Parcel): AllowWriter = AllowWriter(source)
            override fun newArray(size: Int): Array<AllowWriter?> = arrayOfNulls(size)
        }
    }
}