package com.tkip.mycode.model.cost.credit

import android.os.Bundle
import com.tkip.mycode.R
import com.tkip.mycode.databinding.FragmentActBinding
import com.tkip.mycode.funs.search.model.CustomListCost
import com.tkip.mycode.init.CH_ADD_DATE
import com.tkip.mycode.init.CH_LOGIN_TIME
import com.tkip.mycode.init.CH_TOTAL_POINT
import com.tkip.mycode.init.PUT_UID
import com.tkip.mycode.init.my_bind.BindingFragment
import com.tkip.mycode.model.cost.Cost
import com.tkip.mycode.model.my_enum.CostType
import com.tkip.mycode.util.tools.radio.setQueryList

/**
 * 활동내역은 for admin only.
 */
class ActFrag : BindingFragment<FragmentActBinding>() {
    override fun getLayoutResId(): Int = R.layout.fragment_act

    private lateinit var customSearchCost: CustomListCost
    private val uid by lazy { arguments?.getString(PUT_UID) }

    //    private val costType by lazy { arguments?.getInt(PUT_COST_TYPE_NO)?.costType() ?: CostType.ACT }
    private var onItemClick: ((Cost) -> Unit)? = null
    private val arr = arrayOf(CH_LOGIN_TIME, CH_ADD_DATE, CH_TOTAL_POINT)
    private var rbID = 0

    companion object {
        @JvmStatic
        fun newInstance(uid: String?, onItemClick: ((Cost) -> Unit)?) =
                ActFrag().apply {
                    arguments = Bundle().apply { putString(PUT_UID, uid) }
                    this.onItemClick = onItemClick
                }
    }


    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        initView()
    }

    private fun initView() {

        /***  쌓인 act 올리기  */
//        uid?.let { OnAct.addMyActs(it, { OnAct.removeMyActs(it) }, {}) }

        /***  cost list 붙이기  */
        customSearchCost = CustomListCost(mContext, uid, CostType.ACT, null, onItemClick)
        b.frame.addView(customSearchCost)

        /**  데이터 가져오기 */
        customSearchCost.getEsDataList(0, null)


        /** RadioGroup 정렬기준 세팅 : for 상단 리스트 정렬 */
        b.rg.setQueryList(mContext, arr)
        b.rg.setOnCheckedChangeListener { _, id ->
            rbID = id
            search()
        }

        /** 초기 검색 : 첫번째 라디오버튼 클릭 */
        b.rg.getChildAt(0).performClick()
    }

    fun search() {
        customSearchCost.getEsDataList(0, null)
    }


}

