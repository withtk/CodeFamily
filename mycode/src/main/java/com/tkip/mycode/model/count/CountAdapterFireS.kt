package com.tkip.mycode.model.count

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.firebase.ui.firestore.FirestoreRecyclerAdapter
import com.firebase.ui.firestore.FirestoreRecyclerOptions
import com.tkip.mycode.databinding.HolderQuickCountBinding
import com.tkip.mycode.funs.common.arrTranLinear
import com.tkip.mycode.funs.common.lllogM
import com.tkip.mycode.init.MY_QUICK_COUNT
import com.tkip.mycode.init.inter.interHolderBasic
import com.tkip.mycode.model.my_enum.checkAndGo
import com.tkip.mycode.util.tools.etc.OnMyClick
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

/**
 * for Quick Menu in DrawerLayout
 */
class CountAdapterFireS(private val context: Context, options: FirestoreRecyclerOptions<Count>) : FirestoreRecyclerAdapter<Count, CountAdapterFireS.QuickCountViewHolder>(options) {
//    private lateinit var activity: AppCompatActivity

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): QuickCountViewHolder {
//        this.activity = parent.context as AppCompatActivity
        return QuickCountViewHolder(HolderQuickCountBinding.inflate(LayoutInflater.from(parent.context)))
    }

    override fun onBindViewHolder(h: QuickCountViewHolder, p: Int, m: Count) {
        h.bind(m)
    }

    override fun onDataChanged() {
        super.onDataChanged()
        lllogM("CountAdapterFireS onDataChanged size ${snapshots.size}")
        MY_QUICK_COUNT = snapshots.size
    }
//
//    override fun onError(e: FirebaseFirestoreException) {
//        super.onError(e)
//    }


    /**
     * Quick In 내비게이션 메뉴
     */
    inner class QuickCountViewHolder(private val b: HolderQuickCountBinding) : RecyclerView.ViewHolder(b.root) {

        private lateinit var count: Count

        fun bind(count: Count) {
            this.count = count
            b.count = count
            b.inter = interHolderBasic(onClickCard = {onClickCard(it)}, onClickGo = null, onClickUpdate = null, onClickDel = null, onDeepDel = null, onClickStatus = null)

            b.executePendingBindings()
        }

        fun onClickCard(v: View) {
            OnMyClick.setDisableAWhileBTN(v)

            count.checkAndGo(context,false, b.line.arrTranLinear(),
                    onSuccess = {
                        /*** count 동기화하기 : db Count가 변경됐을 수도 있음. */
                        GlobalScope.launch { withContext(Dispatchers.Main) { count.syncModelInHolder(true, it, success = { notifyItemChanged(adapterPosition) }) } }
                    })
        }

    }

}
