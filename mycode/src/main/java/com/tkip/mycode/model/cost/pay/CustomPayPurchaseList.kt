package com.tkip.mycode.model.cost.pay

import android.content.Context
import android.view.LayoutInflater
import android.widget.LinearLayout
import androidx.databinding.DataBindingUtil
import com.tkip.mycode.R
import com.tkip.mycode.databinding.CustomPayPurchaseListBinding
import com.tkip.mycode.model.my_enum.ActType


/**
 * 구매내역
 */
class CustomPayPurchaseList : LinearLayout {
    lateinit var b: CustomPayPurchaseListBinding

    constructor(context: Context) : super(context)
    constructor(context: Context, actType: ActType) : super(context) {
        b = DataBindingUtil.inflate(LayoutInflater.from(context), R.layout.custom_pay_purchase_list, this, true)
        b.title = actType.title
        b.amount = actType.getCrdAmount()

    }

}