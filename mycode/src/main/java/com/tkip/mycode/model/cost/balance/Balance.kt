package com.tkip.mycode.model.cost.balance

import android.os.Parcel
import android.os.Parcelable
import com.tkip.mycode.model.cost.Cost
import com.tkip.mycode.model.my_enum.CostType
import com.tkip.mycode.model.ticket.Ticket


/**
 * <NoDB>
 * actType 밸런스
 * --no^rate^actPnt^bcrd^crd^crt
 */
data class Balance(

        var no: Int = 0,
        var costTypeNo: Int = CostType.PAYMENT.no,
        var rate: Int? = null,

        var pnt: Int? = null,
        var bcrd: Int? = null,
        var crd: Int? = null,
        var crt: Int? = null

) : Parcelable {
    /**
     * balance 금액만 필요할 때. using : custom_pay_type_list.xml
     */
    constructor(ticket: Ticket) : this(
            no = 123,
            pnt = ticket.pnt,
            bcrd = ticket.extra ?: ticket.bcrd,
            crd = ticket.crd
    )

    /**
     * ActType.balance()와 엄연히 다름.
     * 그건 고정. 이값은 관리자에 의해 유닉한 값이 들어갔을 수도 있음.
     */
    constructor(cost: Cost) : this(
            no = 123,
            pnt = cost.pnt,
            bcrd = cost.bcrd,
            crd = cost.crd,
            crt = cost.crt
    )

    constructor(source: Parcel) : this(
            source.readInt(),
            source.readInt(),
            source.readValue(Int::class.java.classLoader) as Int?,
            source.readValue(Int::class.java.classLoader) as Int?,
            source.readValue(Int::class.java.classLoader) as Int?,
            source.readValue(Int::class.java.classLoader) as Int?,
            source.readValue(Int::class.java.classLoader) as Int?
    )

    override fun describeContents() = 0

    override fun writeToParcel(dest: Parcel, flags: Int) = with(dest) {
        writeInt(no)
        writeInt(costTypeNo)
        writeValue(rate)
        writeValue(pnt)
        writeValue(bcrd)
        writeValue(crd)
        writeValue(crt)
    }

    companion object {
        fun new(cost: Cost): Balance? {
            return if (cost.bcrd == null && cost.crd == null && cost.pnt == null && cost.crt == null) null
            else Balance(cost)
        }

        fun new(ticket: Ticket): Balance = Balance(ticket)

        @JvmField
        val CREATOR: Parcelable.Creator<Balance> = object : Parcelable.Creator<Balance> {
            override fun createFromParcel(source: Parcel): Balance = Balance(source)
            override fun newArray(size: Int): Array<Balance?> = arrayOfNulls(size)
        }
    }
}