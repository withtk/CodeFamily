package com.tkip.mycode.model.media

import android.app.Activity
import android.content.Context
import android.util.Log
import android.view.View
import android.widget.LinearLayout
import com.tkip.mycode.R
import com.tkip.mycode.init.base.Cons
import com.tkip.mycode.init.base.getDrawable
import com.tkip.mycode.init.base.getStr
import com.tkip.mycode.util.tools.anim.AnimUtil


/**
 * CREATE : spinner -> 클릭 -> addEdittext -> 사용자 수정 -> set Media.UserId
 * UPDATE : Room.map -> set Media.UserId, set spinnerList -> spinner -> addEditText -> ... 상동
 * VIEW :
 */

/********************************  Media *****************************************/

//val MediaMapSelected:HashMap<String,String> = hashMapOf()
//val MediaMapSelected by lazy { HashMap<String, String?>() }
//val mediaItems by lazy { makeMediaItems() }
val mediaItems = arrayListOf<Media?>()

enum class Media(val no: Int, val title: String, val hint: String, var userId: String?, private val iconId: Int) {
    KAKAO(10, R.string.media_kakao.getStr(), R.string.hint_media_kakao.getStr(), null, R.drawable.em_kakaotalk),
    LINE(20, R.string.media_line.getStr(), R.string.hint_media_line.getStr(), null, R.drawable.em_line),
    SKYPE(30, R.string.media_skype.getStr(), R.string.hint_media_skype.getStr(), null, R.drawable.em_skype),
    WHATSAPP(40, R.string.media_whatsapp.getStr(), R.string.hint_media_whatsapp.getStr(), null, R.drawable.em_whatsapp),
    WECHAT(50, R.string.media_wechat.getStr(), R.string.hint_media_wechat.getStr(), null, R.drawable.em_wechat),

    TELEGRAM(60, R.string.media_telegram.getStr(), R.string.hint_media_telegram.getStr(), null, R.drawable.em_telegram),
    FACEBOOK(70, R.string.media_facebook_messenger.getStr(), R.string.hint_media_facebook_messenger.getStr(), null, R.drawable.em_facebook_messenger),
    TWITTER(80, R.string.media_twitter.getStr(), R.string.hint_media_twitter.getStr(), null, R.drawable.em_twitter),
    VIBER(90, R.string.media_viber.getStr(), R.string.hint_media_viber.getStr(), null, R.drawable.em_viber),
    SLACK(100, R.string.media_slack.getStr(), R.string.hint_media_slack.getStr(), null, R.drawable.em_slack),
    ;


    fun getIcon() = iconId.getDrawable()

    fun onClick(context: Context) {
        when (this) {
            KAKAO -> Log.d(Cons.TAG, this.title)
            LINE -> Log.d(Cons.TAG, this.title)
            SKYPE -> Log.d(Cons.TAG, this.title)
            WHATSAPP -> Log.d(Cons.TAG, this.title)
            WECHAT -> Log.d(Cons.TAG, this.title)
            TELEGRAM -> Log.d(Cons.TAG, this.title)
            FACEBOOK -> Log.d(Cons.TAG, this.title)
            TWITTER -> Log.d(Cons.TAG, this.title)
            VIBER -> Log.d(Cons.TAG, this.title)
            SLACK -> Log.d(Cons.TAG, this.title)
        }
    }

}

//fun mediaClearAll(){
//    Media.values().forEach { it.userId = null }
//}

/**
 * spinner list 셋팅.
 */
fun ArrayList<Media?>.setup() =
        this.apply {
            clear()
            add(null)   // 기본 안내멘트 추가.
            Media.values().forEach {
                //전체초기화 후 전체삽입
                it.userId = null
                add(it)
            }
        }


fun ArrayList<Media?>.has(media: Media): Boolean {
    this.forEach { m -> m?.let { if (it.no == media.no) return true } }
    return false
}


/**
 * for Add
 * setup
 * db의 mediaMap -> add EditText
 */
fun HashMap<String, String>?.setupMediaEditText(activity: Activity, parent: LinearLayout) {
    this?.forEach { (k, v) ->
        val m = Media.valueOf(k)
        parent.addViewMediaEdittext(activity, m)  //et 추가.
        mediaItems.remove(m)   // spinner list에서 제거.
        m.userId = v   // userId값 넣기.
    }
}

/**
 * setup view
 * db의 mediaMap -> add Button
 */
fun HashMap<String, String>?.setupMediaButton(context: Context, parent: LinearLayout) {
    this?.forEach { (k, v) ->
        val m = Media.valueOf(k)
        parent.addViewMediaButton(context, m)  //et 추가.
        mediaItems.remove(m)   // spinner list에서 제거.
        m.userId = v   // userId값 넣기.
    }
}

/**
 * 최종적으로 가져올 map
 */
fun getFinalMediaMap(): HashMap<String, String>? {

    var map2: HashMap<String, String>? = null
    Media.values().forEach { media ->
        media.userId?.let {
            if (map2 == null) map2 = hashMapOf<String, String>().apply { put(media.name, it) }
            else map2?.put(media.name, it)
        }
    }

//    val map = hashMapOf<String, String>().apply {
//        Media.values().forEach { media ->
//            media.userId?.let { this[media.name] = it }
//        }
//    }
//    return if (map.isEmpty()) null else map
    return map2
}




/**
 * parent에 붙이기
 */

fun LinearLayout.addViewMediaSpinner(context: Context,  onClick: (Media) -> Unit): View {
    val view = LinearMediaSpinner(context, onClick)
    this.removeAllViews()
    this.addView(view)
    AnimUtil.showSlideDown(view, true)
    return view
}

fun LinearLayout.addViewMediaEdittext(context: Context, media: Media): View {
    val view = ViewMediaEditText(context, media)
    this.addView(view)
    AnimUtil.showSlideDown(view, true)
    return view
}
fun LinearLayout.addViewMediaButton(context: Context, media: Media): View {
    val view = ViewMediaButton(context, media)
    this.addView(view)
    AnimUtil.showSlideDown(view, true)
    return view
}



