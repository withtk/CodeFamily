package com.tkip.mycode.model.count

import android.os.Parcel
import android.os.Parcelable
import com.tkip.mycode.init.CH_COUNT
import com.tkip.mycode.init.base.createKey
import com.tkip.mycode.init.base.createNanoKey
import com.tkip.mycode.init.base.getI
import com.tkip.mycode.model.board.Board
import com.tkip.mycode.model.flea.Flea
import com.tkip.mycode.model.my_enum.*
import com.tkip.mycode.model.peer.OnPeer
import com.tkip.mycode.model.peer.mynick
import com.tkip.mycode.model.peer.myuid
import com.tkip.mycode.model.post.Post
import com.tkip.mycode.model.room.Room
import com.tkip.mycode.model.store.Store
import com.tkip.mycode.util.tools.date.rightNow

/**
 * 종류 : up, down, book, push
 * 사용중 : post, store , flea, room, quickLink
 *
 * ph_OOOOO_count
 * ph_quick
 */
data class Count(

        var key: String = createKey(),    // Unique 조합형 countType + uid + modelKey
        var uid: String = myuid(),
        var nick: String = mynick(),     // no Async

        var topChildName: String = CH_COUNT,                // == topChildName  firestore에 저장될 최상위 : 삭제할 떄 사용.(중요)
        var countType: Int = CountType.VIEW.no,

        ////////////////// 해당 모델 /////////////////
        var dataTypeNo: Int = DataType.POST.no,               //  ex) post, reply, store, flea, room
        var modelKey: String = "modelKey",                     // 해당되는 포스트의 키. ex) post, reply
        var modelTitle: String? = null,
        var photoThumb: String? = null,   // no Async
        /////////////////////////////////////////////

        var quick: Boolean = false,                       // 퀵메뉴에 추가여부.
        var order: Long = System.currentTimeMillis(),     // 퀵메뉴에서 쓰임. 순서정렬.
        var addDate: Long = rightNow()

) : Parcelable {
    /**
     * for VIEW_UID, UP, DOWN, BOOK, PUSH    :  key = uid + modelKey
     * not VIEW    :  key = createKey()
     */
    constructor(any: Any, type: CountType) : this(
            key = OnCount.myCountKey(any, type),
            topChildName = type.getTopChild(any.getMyDataType()),
            countType = type.no,

            dataTypeNo = any.getMyDataType().no,
            modelKey = any.anyKey(),
            modelTitle = any.anyTitle(),
            photoThumb = any.anyThumb(0),

            quick = true  // 퀵메뉴에 자동으로 추가할 것.
    )

    constructor(source: Parcel) : this(
            source.readString()!!,
            source.readString()!!,
            source.readString()!!,
            source.readString()!!,
            source.readInt(),
            source.readInt(),
            source.readString()!!,
            source.readString(),
            source.readString(),
            1 == source.readInt(),
            source.readLong(),
            source.readLong()
    )

    override fun describeContents() = 0

    override fun writeToParcel(dest: Parcel, flags: Int) = with(dest) {
        writeString(key)
        writeString(uid)
        writeString(nick)
        writeString(topChildName)
        writeInt(countType)
        writeInt(dataTypeNo)
        writeString(modelKey)
        writeString(modelTitle)
        writeString(photoThumb)
        writeInt((if (quick) 1 else 0))
        writeLong(order)
        writeLong(addDate)
    }

    companion object {
        private fun Count.setModel(any: Any) {
            when (any) {
                is Board -> {
                    modelKey = any.key
                    modelTitle = any.title
                    photoThumb = any.photoThumb
                }
                is Post -> {
                    modelKey = any.key
                    modelTitle = any.title
                    photoThumb = any.fotoList.getI(0)?.thumbUrl
                }
                is Flea -> {
                    modelKey = any.key
                    modelTitle = any.title
                    photoThumb = any.fotoList.getI(0)?.thumbUrl
                }
                is Room -> {
                    modelKey = any.key
                    modelTitle = any.local
                    photoThumb = any.fotoList.getI(0)?.thumbUrl
                }
                is Store -> {
                    modelKey = any.key
                    modelTitle = any.title
                    photoThumb = any.fotoList.getI(0)?.thumbUrl
                }
            }
            dataTypeNo = any.getMyDataType().no

        }

        fun create(any: Any, cnt: Cntch): Count {
            return Count().apply {
                setModel(any)
                topChildName = cnt.getTopChild(any)
            }
        }

        /**     * For VIEW  : key 새로 생성. 중복 없음     */
        fun createForVIEW(any: Any, type: CountType) = Count(any, type).apply { key = createNanoKey() }

        @JvmField
        val CREATOR: Parcelable.Creator<Count> = object : Parcelable.Creator<Count> {
            override fun createFromParcel(source: Parcel): Count = Count(source)
            override fun newArray(size: Int): Array<Count?> = arrayOfNulls(size)
        }
    }
}
