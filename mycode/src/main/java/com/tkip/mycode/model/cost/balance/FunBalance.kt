package com.tkip.mycode.model.cost.balance

import com.tkip.mycode.R
import com.tkip.mycode.funs.common.lllogD
import com.tkip.mycode.funs.common.remoteStr
import com.tkip.mycode.init.base.isNumeric
import com.tkip.mycode.model.my_enum.CostType

/**
 * cost 수치
 */


val BALANCE_MAP by lazy { hashMapOf<Int, Balance>() }
const val balanceDiv1111 = "--"
const val balanceDiv2 = "^"
fun setupBalanceMap() {
//    val test = "3360^2^200^1500^n^n--12370^44^12330^100^n^10"
    R.string.remote_balance_cost.remoteStr()
            .split(balanceDiv1111)
            .forEach { act ->
                val arr = act.split(balanceDiv2)    // 한 개의 actType String
                Balance().apply {
                    arr.forEachIndexed { i, s ->
                        when (i) {
                            0 -> no = checkAddInt(s) ?: 0
                            1 -> costTypeNo = checkAddInt(s) ?: CostType.ACT.no
                            2 -> rate = checkAddInt(s)
                            3 -> pnt = checkAddInt(s)
                            4 -> bcrd = checkAddInt(s)
                            5 -> crd = checkAddInt(s)
                            6 -> crt = checkAddInt(s)
                        }
                    }
                    BALANCE_MAP[no] = this
                }
            }

//    BALANCE_MAP.forEach { (k, v) ->
//        println("$k $v")
//    }

    lllogD("TTTestActivity BALANCE_MAP size ${BALANCE_MAP.size}")

}

fun checkAddInt(s: String) = if (s.isNumeric()) s.toInt() else null


/**
 * actType에서 string 만들기. for admin
 */
//fun actTypeList() {
//    val bu = StringBuffer().apply {
//        ActType.values().forEach {
//            if (this.isEmpty()) this.append("${it.no}") else this.append("--${it.no}")
//            this.append("^${it.rate}")
//            this.append("^${it.actPnt}")
//            this.append("^${it.bcrd}")
//            this.append("^${it.crd}")
//            this.append("^${it.crt}")
//        }
//        lllogD("StringBuffer  : $this")
//    }
//
//    val result = bu.toString().replace("null", "n")
//    lllogD("StringBuffer result : $result")
//}
