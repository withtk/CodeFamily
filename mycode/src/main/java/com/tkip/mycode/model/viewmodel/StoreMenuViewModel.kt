package com.tkip.mycode.model.viewmodel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.tkip.mycode.init.EES_STORE_MENU
import com.tkip.mycode.model.count.Count
import com.tkip.mycode.model.store.StoreMenu
import com.tkip.mycode.model.my_enum.Cntch
import com.tkip.mycode.model.my_enum.Status
import com.tkip.mycode.util.tools.fire.Firee
import com.tkip.mycode.funs.common.lllogI
import com.tkip.mycode.util.lib.retrofit2.ESget
import com.tkip.mycode.util.lib.retrofit2.ESquery

class StoreMenuViewModel : ViewModel() {

    val liveStoreMenu = MutableLiveData<StoreMenu>()
    val liveStoreMenuList = ListLiveData<StoreMenu>()

    val liveBookmark = MutableLiveData<Count>()
    val livePush = MutableLiveData<Count>()
    val liveWarn = MutableLiveData<Count>()


//    fun getStoreMenu(roomKey: String): LiveData<StoreMenu> {
//        Firee.getRoom(roomKey, success = { it?.let { room1 -> liveRoom.value = room1 } }, fail = {})
//        return liveRoom
//    }


    fun getEsRoomList(size: Int, from: Int, storeKey:String, status: Status): ListLiveData<StoreMenu> {
        val query = ESquery.storeMenuList(size, from, storeKey,status)

        ESget.getStoreMenuList(query,
                success = {
                    lllogI("getStoreMenuList list : $it.size")
                    if (it == null) {
                        liveStoreMenuList.clear(true)
                    } else {
                        if (from == 0) liveStoreMenuList.clear(false)
                        liveStoreMenuList.addAll(it)
                    }
                }, fail = {})
        return liveStoreMenuList
    }


//    /********************************************************************************************
//     ********************************* Count ***************************************************/
//    fun getAllEsCount(storeMenu: StoreMenu) {
//        getEsCountList(storeMenu, Cntch.BOOK)
//        getEsCountList(storeMenu, Cntch.PUSH)
//    }
//
//    private fun getEsCountList(storeMenu: StoreMenu, cnt: Cntch): LiveData<Count> {
//        ESget.getCountList(cnt.getTopChild(DataType.ROOM), ESquery.countUidModelKey(storeMenu.key), success = {
//            if (it == null || it.isEmpty()) {
//                setLiveCount(cnt, null)
//            } else {
//                setLiveCount(cnt, it[0])
//            }
//        }, fail = {})
//        return returnLiveCount(cnt)
//    }
//
//    fun getCount(storeMenu: StoreMenu, cnt: Cntch): LiveData<Count> {
//        Firee.getCount(cnt.getTopChild(DataType.ROOM), storeMenu.key, success = { setLiveCount(cnt, it) }, fail = {})
//        return returnLiveCount(cnt)
//    }
//
//    private fun returnLiveCount(cnt: Cntch) = when (cnt) {
//        Cntch.BOOK -> liveBookmark
//        Cntch.PUSH -> livePush
//        else -> liveBookmark
//    }
//
//    private fun setLiveCount(cnt: Cntch, count: Count?) {
//        when (cnt) {
//            Cntch.BOOK -> liveBookmark.value = count
//            Cntch.PUSH -> livePush.value = count
//            else -> liveBookmark.value = count
//        }
//    }
//
//    private fun Cntch.count() =
//            when (this) {
//                Cntch.BOOK -> liveBookmark.value
//                Cntch.PUSH -> livePush.value
//                else -> null
//            }
//
//    fun updateCount(storeMenu: StoreMenu, cnt: Cntch, v: View) {
//        cnt.updateCount(storeMenu, cnt.count(), v, success = { on, count1 ->
//            if (on) setLiveCount(cnt, count1)
//            else setLiveCount(cnt, null)
//        }, fail = {})
//    }

    fun addViewUID(storeMenu: StoreMenu, cnt: Cntch) {
        Firee.addViewUID(EES_STORE_MENU, Count.create(storeMenu, cnt), {}, {})
    }


}