package com.tkip.mycode.model.mytag

import android.content.Context
import android.content.Intent
import android.view.View
import com.cunoraz.tagview.Tag
import com.cunoraz.tagview.TagView
import com.google.firebase.firestore.WriteBatch
import com.tkip.mycode.R
import com.tkip.mycode.funs.common.lllogD
import com.tkip.mycode.funs.common.lllogI
import com.tkip.mycode.init.base.Sred
import com.tkip.mycode.init.base.getClr
import com.tkip.mycode.init.base.getInt
import com.tkip.mycode.model.my_enum.Status
import com.tkip.mycode.util.lib.TransitionHelper
import com.tkip.mycode.util.lib.retrofit2.ESget
import com.tkip.mycode.util.lib.retrofit2.ESquery
import com.tkip.mycode.util.tools.fire.addMytag


class OnTag {

    companion object {

        val saveKeywordsLimit = R.integer.save_keyword_limit.getInt()


        fun gotoMytagManageActivity(context: Context, arrPair: Array<androidx.core.util.Pair<View, String>>?) {
            val intent = Intent(context, MyTagManageActivity::class.java)
            TransitionHelper.startTran(context, intent, arrPair)
        }

        /** 맵으로 하는 이유 : 존재유무 검색을 키값을 이용하여 한번에 바로 하기 위해.*/
        val myUploadList = arrayListOf<MyTag>()

        /**
         *  내가 만든 tag batch에 추가하기.
         */
        fun addToBatchAllMytag(batch: WriteBatch) {
            lllogI("addToBatchAllMytag   myUploadList.size :  ${myUploadList.size}")
            myUploadList.forEach {
                batch.addMytag(it)
                lllogI("addToBatchAllMytag   myUploadList.MyTag :  $it")
            }
        }

        fun clearMyTagList() {
            lllogI("clearMyTagList   myUploadList.size :  ${myUploadList.size}")
            if (myUploadList.isNotEmpty()) myUploadList.clear()
        }
        /**********************************************************************************
         ***  tagview  basic ************************************************************
         *********************************************************************************/

        /**         *  중복체크 tagview         */
        fun isContainTagView(tagName: String, tagView: TagView): Boolean {
            tagView.tags.forEach { if (it.text == tagName) return true }
            return false
        }

        /**         *  tagView에 tag 올리기         */
        fun addTagInTagView(tagName: String, tagView: TagView) {
            if (isContainTagView(tagName, tagView)) return
            tagView.addTag(getTag(tagName))
        }

        /**         *  tagView에 tag 올리기         */
        fun addTagUniqueInTagView(tagName: String, tagView: TagView) {
            if (isContainTagView(tagName, tagView)) return
            tagView.addTag(getTag2(tagName))
        }

        private fun getTag(text: String): Tag {
            return Tag(text).apply {
                tagTextColor = R.color.text_light.getClr()
                layoutBorderColor = R.color.md_pink_500.getClr()
                layoutColor = R.color.skyblue_800_alpha.getClr()
                layoutColorPress = R.color.blue_900.getClr()
                deleteIndicatorColor = R.color.md_cyan_300.getClr()
            }
        }

        private fun getTag2(text: String): Tag {
            return Tag(text).apply {
                tagTextColor = R.color.text_light.getClr()
                layoutBorderColor = R.color.md_pink_500.getClr()
                layoutColor = R.color.skyblue_800.getClr()
                layoutColorPress = R.color.blue_900.getClr()
                deleteIndicatorColor = R.color.md_cyan_300.getClr()
            }
        }


        fun getEsTagList(size: Int, from: Int, tagType: TagTYPE, keyword: String, success: (ArrayList<MyTag>?) -> Unit, fail: (() -> Unit)? = null) {
            if (tagType == TagTYPE.ALL) ESget.getMyTagList(ESquery.tagSearchAll(size, 0, keyword, Status.NORMAL), success = { success(it) }, fail = { fail?.invoke() })
            else ESget.getMyTagList(ESquery.mytagList(size, from, tagType.queryTypes, keyword, Status.NORMAL), success = { success(it) }, fail = { fail?.invoke() })
        }


        /**
         * 검색 기록 저장.
         */
        fun saveMyKeywordList(key: String, tag: String?) {
            tag?.let {
                val list = Sred.loadArray(key) ?: arrayListOf()
                if (!list.contains(it)) {
                    list.add(it)
                    if (list.size > saveKeywordsLimit) list.removeAt(0)   // 저장 개수 limit
                    Sred.saveArray(key, list)
                }
            }
        }

        fun loadMyKeywordList(key: String): ArrayList<String>? {
            return Sred.loadArray(key)?.apply {
                this.reverse()
            }
        }

        /*** 기본 즐겨찾는 태그 등록하기. */
        fun setBasicBookTags(): ArrayList<String>? {
            arrayOf( "마닐라","마카티", "앙헬레스", "식당", "음식점", "세부").forEach {
                saveMyKeywordList(Sred.ARR_STORE_KEYWORD, it)
            }
            val list = loadMyKeywordList(Sred.ARR_STORE_KEYWORD)
            lllogD("setBasicBookTags size ${list?.size}")
           return list
        }

        /**********************************************************************************
         ***  upload  *********************************************************************
         *********************************************************************************/
        fun addNewTag(tagType: TagTYPE, keyword: String) {
            // 이미 있는지 여부.
            getEsTagList(1, 0, tagType, keyword,
                    success = {
                        if (it.isNullOrEmpty()) {
                            myUploadList.add(MyTag(keyword, tagType))  // 추가
                            lllogI("myUploadList add keyword $keyword")
                        }
                    }, fail = { })
        }


    }

}