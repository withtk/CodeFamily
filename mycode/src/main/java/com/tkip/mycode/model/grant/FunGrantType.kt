package com.tkip.mycode.model.grant

import com.tkip.mycode.R
import com.tkip.mycode.init.base.getStr

//
//enum class GrantType(val no: Int, val title: String) {
//
//    NOTHING(100, R.string.status_nothing.getStr()),
//
//    NO_IMAGE(2000, R.string.grant_type_no_image.getStr()),
//    SIZE_IMAGE(2300, R.string.grant_type_wrong_size_image.getStr()),
//
//    BAD_WORD(3000, R.string.grant_type_bad_word.getStr()),
//
//    DUPLICATED(4000, R.string.grant_type_duplicated.getStr()),
//
//    ;
//}
//
//fun Int.grantType(): GrantType {
//    GrantType.values().forEach { if (it.no == this) return it }
//    return GrantType.NOTHING
//}
//
//fun String.grantType(): GrantType {
//    GrantType.values().forEach { if (this == it.title) return it }
//    return GrantType.NOTHING
//}
