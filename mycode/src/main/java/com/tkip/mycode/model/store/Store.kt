package com.tkip.mycode.model.store

import android.os.Parcel
import android.os.Parcelable
import com.tkip.mycode.init.base.createKey
import com.tkip.mycode.model.my_enum.Status
import com.tkip.mycode.model.my_enum.StoreType
import com.tkip.mycode.model.peer.mynick
import com.tkip.mycode.model.peer.myuid
import com.tkip.mycode.util.lib.photo.Foto
import com.tkip.mycode.util.tools.date.rightNow
import java.util.*

/**
 *
 */
data class Store(

        var key: String = createKey(),    // placeId를 넣거나 임의로 만든 key. 불변. 차후에 poi가 생겨도 place업데이트만 하면 됨. key는 바꾸지 않아도 됨.
        var authorUID: String = myuid(),    // 작성자 : admin or user
        var authorNick: String = mynick(),
        var hostUID: String? = null,    // 차후에 업소주인 등록시 host에 따라 메뉴가 바뀔 예정.
        var boardKey: String? = null,    // 스토어와 연결된 보드 , boardName = storeName
        var storeTypeNo: Int = StoreType.SHOP.no,

        /////////////////////// 스토어 안내 /////////////////////////////////////
        var title: String = "",      // 변경불가.  처음 올릴때 이 값을 뿌려주기때문에 blank로 초기화함.
        var comment: String? = null,
        var contact: String? = null,  // 연락처
        var mediaMap: HashMap<String, String>? = null,  // 미디어 아이디
        var tagList: ArrayList<String>? = null,    // 최대 10개까지만, 검색 키워드광고와 매치될 부분이기때문에 남용 우려로 수량 제한.
        var tagLimit: Int? = null,    // null은 기본값 10개, 그 이상은 구매요. todo : 다른 모델들도?

        var fotoList: ArrayList<Foto>? = null,        // DB에 저장된 foto,

        var local: String? = null,    // 지역이름
        var lat: Double? = null,
        var lng: Double? = null,
        var address: String? = null,   // 주소

        ////////////////////// Place /////////////////////////////////
        var placeId: String? = null,
//        var placeTypeList: ArrayList<String>? = null,       // for 구글맵 place
        var websiteUri: String? = null,
        var phoneNumber: String? = null,
        var weekdayText: String? = null,

        ////////////////////// for Host /////////////////////////////////

        var adCredit: Int = 0,   // 현재 검색 순위에 적용중인 credit
        var totalAdCredit: Int = 0,  // 그동안 결제한 totalAdCredit
        var decoSearchNo: Int? = null,     // search 결과에 나오는 holder 종류. 차후에 만들 예정.
        var decoBookNo: Int? = null,   // 유저가 bookmark 했을때 나오는 holder 종류. 차후에 만들 예정.
        var eventKey: String? = null,   // 이벤트 없으면 null, 있으면 이벤트모델 받아와서 holder에 뿌려줘.

        ////////////////////// Rating  /////////////////////////////////
//        var rating: Double? = null,     // 평균값
//        var ratingCount: Int = 0,           // no DB : storeRatingFrag에 쓰임.
//        var rating5: Int = 0,           //  5별 부여한 유저수
//        var rating4: Int = 0,
//        var rating3: Int = 0,
//        var rating2: Int = 0,
//        var rating1: Int = 0,

        ////////////////////// Count /////////////////////////////////
        var bookmarkCount: Int = 0,   // 유저가 즐겨찾기에 올릴시 카운팅
        var viewCount: Int = 0,       // 뷰 횟수.
        var viewUidCount: Int = 0,    // 유저당 한번 뷰 횟수.
        var pushCount: Int = 0,       // push받는 유저수.


        var totalPoint: Long = 0,       //  bookmarkCount + viewcount + replyCount + pushCount +(reward*100) - (warnCount*100)
        var statusNo: Int = Status.NORMAL.no,       // 최초 작성시 NORMAL
        var addDate: Long = rightNow(),       // 최초 업로드 시간.   재업로드시 재업로드 시간으로 변경.
        var updateDate: Long? = null


) : Parcelable {
    /**     * clone    */
    @Suppress("UNCHECKED_CAST")
    constructor(store: Store) : this(

            key = store.key,
            authorUID = store.authorUID,
            authorNick = store.authorNick,
            hostUID = store.hostUID,
            boardKey = store.boardKey,
            storeTypeNo = store.storeTypeNo,

            title = store.title,
            comment = store.comment,
            contact = store.contact,
            mediaMap = store.mediaMap?.clone() as? HashMap<String, String>,
            tagList = store.tagList,
            tagLimit = store.tagLimit,

            fotoList = store.fotoList?.clone() as? ArrayList<Foto>,

            local = store.local,
            lat = store.lat,
            lng = store.lng,
            address = store.address,

            placeId = store.placeId,
//            placeTypeList = store.placeTypeList,
            websiteUri = store.websiteUri,
            phoneNumber = store.phoneNumber,
            weekdayText = store.weekdayText,

            adCredit = store.adCredit,
            totalAdCredit = store.totalAdCredit,
            decoSearchNo = store.decoSearchNo,
            decoBookNo = store.decoBookNo,
            eventKey = store.eventKey,

//            rating = store.rating,
//            ratingCount = store.ratingCount,
//            rating5 = store.rating5,
//            rating4 = store.rating4,
//            rating3 = store.rating3,
//            rating2 = store.rating2,
//            rating1 = store.rating1,


            bookmarkCount = store.bookmarkCount,
            viewCount = store.viewCount,
            viewUidCount = store.viewUidCount,
            pushCount = store.pushCount,

            totalPoint = store.totalPoint,
            statusNo = store.statusNo,
            addDate = store.addDate,
            updateDate = store.updateDate

    )

    constructor(source: Parcel) : this(
            source.readString()!!,
            source.readString()!!,
            source.readString()!!,
            source.readString(),
            source.readString(),
            source.readInt(),
            source.readString()!!,
            source.readString(),
            source.readString(),
            source.readSerializable() as HashMap<String, String>?,
            source.createStringArrayList(),
            source.readValue(Int::class.java.classLoader) as Int?,
            source.createTypedArrayList(Foto.CREATOR),
            source.readString(),
            source.readValue(Double::class.java.classLoader) as Double?,
            source.readValue(Double::class.java.classLoader) as Double?,
            source.readString(),
            source.readString(),
            source.readString(),
            source.readString(),
            source.readString(),
            source.readInt(),
            source.readInt(),
            source.readValue(Int::class.java.classLoader) as Int?,
            source.readValue(Int::class.java.classLoader) as Int?,
            source.readString(),
            source.readInt(),
            source.readInt(),
            source.readInt(),
            source.readInt(),
            source.readLong(),
            source.readInt(),
            source.readLong(),
            source.readValue(Long::class.java.classLoader) as Long?
    )

    override fun describeContents() = 0

    override fun writeToParcel(dest: Parcel, flags: Int) = with(dest) {
        writeString(key)
        writeString(authorUID)
        writeString(authorNick)
        writeString(hostUID)
        writeString(boardKey)
        writeInt(storeTypeNo)
        writeString(title)
        writeString(comment)
        writeString(contact)
        writeSerializable(mediaMap)
        writeStringList(tagList)
        writeValue(tagLimit)
        writeTypedList(fotoList)
        writeString(local)
        writeValue(lat)
        writeValue(lng)
        writeString(address)
        writeString(placeId)
        writeString(websiteUri)
        writeString(phoneNumber)
        writeString(weekdayText)
        writeInt(adCredit)
        writeInt(totalAdCredit)
        writeValue(decoSearchNo)
        writeValue(decoBookNo)
        writeString(eventKey)
        writeInt(bookmarkCount)
        writeInt(viewCount)
        writeInt(viewUidCount)
        writeInt(pushCount)
        writeLong(totalPoint)
        writeInt(statusNo)
        writeLong(addDate)
        writeValue(updateDate)
    }

    companion object {
        @JvmField
        val CREATOR: Parcelable.Creator<Store> = object : Parcelable.Creator<Store> {
            override fun createFromParcel(source: Parcel): Store = Store(source)
            override fun newArray(size: Int): Array<Store?> = arrayOfNulls(size)
        }
    }
}