package com.tkip.mycode.model.flea

import android.os.Parcel
import android.os.Parcelable
import com.tkip.mycode.init.base.createKey
import com.tkip.mycode.model.my_enum.Status
import com.tkip.mycode.model.peer.mynick
import com.tkip.mycode.model.peer.myuid
import com.tkip.mycode.util.lib.photo.Foto
import com.tkip.mycode.util.tools.date.rightNow
import java.util.*

data class Flea(
        var key: String = createKey(),
        var authorUID: String = myuid(),
        var authorNick: String = mynick(),
        var anonymous: Boolean = false,       // 익명 업로드- 포인트 필요.  //todo : add에서만들것 post와 똑같이.

        //////////////// 수정 가능한 부분 ////////////////////////////
        var title: String? = null,    // 품명
        var comment: String? = null,
        var price: Int? = null,   //  물건 금액 peso
        var fotoList: ArrayList<Foto>? = null,        // DB에 저장된 foto,
        var tagList: ArrayList<String>? = null,    // 최대 10개까지만, 검색 키워드광고와 매치될 부분이기때문에 남용 우려로 수량 제한.

        var local: String? = null,    // 지역명 1개.
        var contact: String? = null,  // 연락처
        var mediaMap: HashMap<String, String>? = null,  // 미디어 아이디


        //////////////////////// for author //////////////////////////////////////
        var allowReply: Boolean = true,  // 댓글 입력 가능 여부.

        var paidCredit: Int = 0,       // 결제한 credit (나중에 절반 돌려주는 기준이 되는 금액)
        var reloadCount: Int? = null,    // 재업로드 횟수.  (차후에 횟수에 따라 보상 예정.)  재업로드시 새로 올리는 것보다 credit이 쌈. 절반가격.

        var rank: Int = 0,       // 상위 랭크 여부 (예 20200102) 오늘 날짜가 가장 크다.
        var adCredit: Int = 0,   // 현재 검색 순위에 적용중인 credit
        var totalAdCredit: Int = 0,  // 그동안 결제한 totalAdCredit
        var decoSearchNo: Int = 0,     // search 결과에 나오는 holder 종류. 차후에 만들 예정.
        var decoBookNo: Int = 0,   // 유저가 bookmark 했을때 나오는 holder 종류. 차후에 만들 예정.


        ////////////////////// Count /////////////////////////////////
        var bookmarkCount: Int = 0,   // 유저가 즐겨찾기에 올릴시 카운팅
        var viewCount: Int = 0,       // 뷰 횟수.
        var viewUidCount: Int = 0,    // 유저당 한번 뷰 횟수.
        var pushCount: Int = 0,       // push받는 유저수.

        //////////////////////////////////////////////////////////////
        var replyCount: Int = 0,      // 댓글 횟수.
        var warnCount: Int = 0,       // 신고 횟수.

        var totalPoint: Long = 0,       // bookmarkCount + viewcount + replyCount + pushCount + (adPoint*100)
        var statusNo: Int = Status.NORMAL.no,       // 최초 작성시 NORMAL
        var addDate: Long = rightNow(),       // 최초 업로드 시간.   재업로드시 재업로드 시간으로 변경.
        var updateDate: Long? = null

) : Parcelable {
    /**     * clone    */
    @Suppress("UNCHECKED_CAST")
    constructor(flea: Flea) : this(
            key = flea.key,
            authorUID = flea.authorUID,
            authorNick = flea.authorNick,
            anonymous = flea.anonymous,

            title = flea.title,
            comment = flea.comment,
            price = flea.price,
            fotoList = flea.fotoList?.clone() as? ArrayList<Foto>,
            tagList = flea.tagList,

            local = flea.local,
            contact = flea.contact,
            mediaMap = flea.mediaMap,

            allowReply = flea.allowReply,
            //////////////////////////////////////////////////////////////////////
            paidCredit = flea.paidCredit,
            reloadCount = flea.reloadCount,

            rank = flea.rank,
            adCredit = flea.adCredit,
            totalAdCredit = flea.totalAdCredit,
            decoSearchNo = flea.decoSearchNo,
            decoBookNo = flea.decoBookNo,

            bookmarkCount = flea.bookmarkCount,
            viewCount = flea.viewCount,
            viewUidCount = flea.viewUidCount,
            pushCount = flea.pushCount,

            replyCount = flea.replyCount,
            warnCount = flea.warnCount,

            totalPoint = flea.totalPoint,
            statusNo = flea.statusNo,
            addDate = flea.addDate,
            updateDate = flea.updateDate
    )

    constructor(source: Parcel) : this(
            source.readString()!!,
            source.readString()!!,
            source.readString()!!,
            1 == source.readInt(),
            source.readString(),
            source.readString(),
            source.readValue(Int::class.java.classLoader) as Int?,
            source.createTypedArrayList(Foto.CREATOR),
            source.createStringArrayList(),
            source.readString(),
            source.readString(),
            source.readSerializable() as HashMap<String, String>?,
            1 == source.readInt(),
            source.readInt(),
            source.readValue(Int::class.java.classLoader) as Int?,
            source.readInt(),
            source.readInt(),
            source.readInt(),
            source.readInt(),
            source.readInt(),
            source.readInt(),
            source.readInt(),
            source.readInt(),
            source.readInt(),
            source.readInt(),
            source.readInt(),
            source.readLong(),
            source.readInt(),
            source.readLong(),
            source.readValue(Long::class.java.classLoader) as Long?
    )

    override fun describeContents() = 0

    override fun writeToParcel(dest: Parcel, flags: Int) = with(dest) {
        writeString(key)
        writeString(authorUID)
        writeString(authorNick)
        writeInt((if (anonymous) 1 else 0))
        writeString(title)
        writeString(comment)
        writeValue(price)
        writeTypedList(fotoList)
        writeStringList(tagList)
        writeString(local)
        writeString(contact)
        writeSerializable(mediaMap)
        writeInt((if (allowReply) 1 else 0))
        writeInt(paidCredit)
        writeValue(reloadCount)
        writeInt(rank)
        writeInt(adCredit)
        writeInt(totalAdCredit)
        writeInt(decoSearchNo)
        writeInt(decoBookNo)
        writeInt(bookmarkCount)
        writeInt(viewCount)
        writeInt(viewUidCount)
        writeInt(pushCount)
        writeInt(replyCount)
        writeInt(warnCount)
        writeLong(totalPoint)
        writeInt(statusNo)
        writeLong(addDate)
        writeValue(updateDate)
    }

    companion object {
        @JvmField
        val CREATOR: Parcelable.Creator<Flea> = object : Parcelable.Creator<Flea> {
            override fun createFromParcel(source: Parcel): Flea = Flea(source)
            override fun newArray(size: Int): Array<Flea?> = arrayOfNulls(size)
        }
    }
}