package com.tkip.mycode.model.manage

import android.content.Context
import com.tkip.mycode.databinding.LinearTopNoticeBinding
import com.tkip.mycode.dialog.alert.MyAlert
import com.tkip.mycode.funs.common.lllogM
import com.tkip.mycode.init.base.Sred
import com.tkip.mycode.init.base.lt
import com.tkip.mycode.init.inter.interTopNotice
import com.tkip.mycode.model.board.Board
import com.tkip.mycode.util.lib.retrofit2.ESget
import com.tkip.mycode.util.tools.anim.AnimUtil
import com.tkip.mycode.util.tools.anim.gone
import com.tkip.mycode.util.tools.anim.visi


/**
 * 보드 공지사항
 */
class OnTopNotice {
    companion object {


        /*******************************************************************************
         * topNotice : 보드 공지사항
         * postRecent, flea, room  이것들은 보드가 없기때문에 수동으로 적어줘야.
         ******************************************************************************/
        private fun commonTopNotice(boardKey: String, comment: String?, descNo: Int, inTopNotice: LinearTopNoticeBinding) {
            if (comment.isNullOrBlank()) {            // 공란이면 공지 안 띄워.
                inTopNotice.line.gone()
                Sred.putInt(boardKey, descNo)
            } else {
                if (Sred.getInt(boardKey, 0).lt(descNo)) { // true : 더이상안보기 버튼 아직 일때
                    inTopNotice.desc = comment
                    inTopNotice.line.visi()
                }
            }
        }

        private fun commonNoMoreShow(boardKey: String, descNo: Int, inTopNotice: LinearTopNoticeBinding) {
            AnimUtil.hideSlideUp(true, inTopNotice.line, false)
            Sred.putInt(boardKey, descNo)
        }

        fun setupPostDesc(context: Context?, board: Board?, inTopNotice: LinearTopNoticeBinding) {
            lllogM("setupPostDesc board.key : ${board?.key}")
            board?.let {
                commonTopNotice(board.key, board.comment, board.commentNo, inTopNotice)
                inTopNotice.inter = interTopNotice(
                        onNotice = {   context?.let { MyAlert.showBoardInfo(context, board)  }},
                        onNoMoreShow = { commonNoMoreShow(board.key, board.commentNo, inTopNotice) }
                )

//                ESget.getBoard(boardKey,
//                        success = {
//                            it?.let { board ->
//                                lllogI("setupPostDesc getBoard board $board")
//                                commonTopNotice(board.key, board.comment, board.commentNo, inTopNotice)
//                                inTopNotice.inter = interTopNotice(
//                                        onNotice = { MyAlert.showBoardInfo(context, board) },
//                                        onNoMoreShow = { commonNoMoreShow(board.key, board.commentNo, inTopNotice) }
//                                )
//                            }
//                        }, fail = {})
            }
//                    ?: setupRecentPostDesc(inTopNotice)
        }

//        fun setupRecentPostDesc(inTopNotice: LinearTopNoticeBinding) {
//            val boardKey = R.string.key_recent_board.getStr()
//            val desc = R.string.rmt_board_recent_post_desc.remoteStr()
//            val descNo = R.string.rmt_board_recent_post_desc_no.remoteLong().toInt()
//            commonTopNotice(boardKey, desc, descNo, inTopNotice)
//
//            inTopNotice.inter = interTopNotice(
//                    onNotice = {},
//                    onNoMoreShow = { commonNoMoreShow(boardKey, descNo, inTopNotice) }
//            )
//        }

        fun setupBasicBoardDesc(boardKey: String, inTopNotice: LinearTopNoticeBinding) {
            ESget.getBoard(boardKey,
                    success = {
                        it?.let { board ->
                            commonTopNotice(board.key, board.comment, board.commentNo, inTopNotice)
                            inTopNotice.inter = interTopNotice(
                                    onNotice = {},
                                    onNoMoreShow = { commonNoMoreShow(board.key, board.commentNo, inTopNotice) }
                            )
                        }
                    }, fail = {})
        }

//        fun setupFleaDesc(inTopNotice: LinearTopNoticeBinding) {
//            val boardKey = R.string.key_flea_board.getStr()
//            val desc = R.string.rmt_board_flea_desc.remoteStr()
//            val descNo = R.string.rmt_board_flea_desc_no.remoteLong().toInt()
//            commonTopNotice(boardKey, desc, descNo, inTopNotice)
//
//            inTopNotice.inter = interTopNotice(
//                    onNotice = {},
//                    onNoMoreShow = { commonNoMoreShow(boardKey, descNo, inTopNotice) }
//            )
//        }

//        fun setupRoomDesc(inTopNotice: LinearTopNoticeBinding) {
//            val boardKey = R.string.key_room_board.getStr()
//            val desc = R.string.rmt_board_room_desc.remoteStr()
//            val descNo = R.string.rmt_board_room_desc_no.remoteLong().toInt()
//            commonTopNotice(boardKey, desc, descNo, inTopNotice)
//
//            inTopNotice.inter = interTopNotice(
//                    onNotice = {},
//                    onNoMoreShow = { commonNoMoreShow(boardKey, descNo, inTopNotice) }
//            )
//        }

        /*****************************************************************************/

    }

}