package com.tkip.mycode.model.cost.credit

import android.content.Context
import android.content.Intent
import android.view.View
import com.tkip.mycode.databinding.AlertChangeCreditBinding
import com.tkip.mycode.init.base.getStringComma
import com.tkip.mycode.init.base.removeComma
import com.tkip.mycode.model.my_enum.PayType
import com.tkip.mycode.model.peer.isAnonymous
import com.tkip.mycode.util.lib.TransitionHelper
import kotlin.math.ceil


class OnCredit {

    companion object {

        fun gotoCreditMenuActivity(context: Context, arrPair: Array<androidx.core.util.Pair<View, String>>?) {

            /*** 업로드 가능 여부 체크 */
            if (isAnonymous(context)) return

            val intent = Intent(context, CreditMenuActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
            TransitionHelper.startTran(context, intent, arrPair)
        }


        /*** 결제 가능여부 체크 */
        fun checkAvailPayment(sum: Int, onNotAvail: () -> Unit, onCreditOnly: () -> Unit, onBonusOnly: () -> Unit, onAvail: () -> Unit) {
            val notEnoughBonus = (PayType.BONUS_CREDIT.isNotEnough(sum))
            val notEnoughCredit = (PayType.CREDIT.isNotEnough(sum))
            when {
                notEnoughBonus && notEnoughCredit -> onNotAvail()
                notEnoughBonus -> onCreditOnly()
                notEnoughCredit -> onBonusOnly()
                else -> onAvail()
            }
        }


    }

}