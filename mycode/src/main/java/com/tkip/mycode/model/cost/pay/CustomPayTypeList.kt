package com.tkip.mycode.model.cost.pay

import android.content.Context
import android.view.LayoutInflater
import android.widget.LinearLayout
import androidx.databinding.DataBindingUtil
import com.tkip.mycode.R
import com.tkip.mycode.databinding.CustomPayTypeListBinding
import com.tkip.mycode.model.cost.Cost
import com.tkip.mycode.model.ticket.Ticket


/**
 * horizontal 모든 payType list
 * for holder bill
 *     holder cost
 */
class CustomPayTypeList : LinearLayout {
    lateinit var b: CustomPayTypeListBinding

    constructor(context: Context) : super(context)

    constructor(context: Context, cost: Cost) : super(context) {
        b = DataBindingUtil.inflate(LayoutInflater.from(context), R.layout.custom_pay_type_list, this, true)
//        b.cost = cost
    }

    constructor(context: Context, ticket: Ticket) : super(context) {
        b = DataBindingUtil.inflate(LayoutInflater.from(context), R.layout.custom_pay_type_list, this, true)
//        b.ticket = ticket
    }

//    private fun init(context: Context, bCrd: Int?, crd: Int?, pnt: Int?, extra: Int?) {
//        b = DataBindingUtil.inflate(LayoutInflater.from(context), R.layout.custom_pay_type_list, this, true)
//        b.bCrd = bCrd
//        b.crd = crd
//        b.pnt = pnt
//        b.extra = extra
//    }

}