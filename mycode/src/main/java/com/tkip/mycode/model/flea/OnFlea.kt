package com.tkip.mycode.model.flea

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.view.View
import com.tkip.mycode.R
import com.tkip.mycode.dialog.progress.MyCircle
import com.tkip.mycode.dialog.toast.TToast
import com.tkip.mycode.funs.common.lllogI
import com.tkip.mycode.init.*
import com.tkip.mycode.init.base.checkUpdate
import com.tkip.mycode.model.cost.Cost
import com.tkip.mycode.model.my_enum.ActType
import com.tkip.mycode.model.my_enum.Status
import com.tkip.mycode.model.my_enum.getStatus
import com.tkip.mycode.model.mytag.OnTag
import com.tkip.mycode.model.peer.OnPeer
import com.tkip.mycode.model.peer.checkAllAvailUpload
import com.tkip.mycode.model.peer.isNotAdmin
import com.tkip.mycode.nav.flea.AddFleaActivity
import com.tkip.mycode.nav.flea.FleaActivity
import com.tkip.mycode.util.lib.TransitionHelper
import com.tkip.mycode.util.lib.photo.Foto
import com.tkip.mycode.util.lib.photo.util.PhotoUtil
import com.tkip.mycode.util.lib.retrofit2.ESget
import com.tkip.mycode.util.lib.retrofit2.ESquery
import com.tkip.mycode.util.tools.date.rightNow
import com.tkip.mycode.util.tools.etc.OnMyClick
import com.tkip.mycode.util.tools.fire.*

class OnFlea {

    companion object {


        fun gotoAddFleaActivity(context: Context, action: Int, flea: Flea?, arrPair: Array<androidx.core.util.Pair<View, String>>?) {

            /*** 업로드 가능 여부 체크 */
            if (checkAllAvailUpload(context)) return

            val intent = Intent(context, AddFleaActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
            intent.putExtra(PUT_ACTION, action)
            flea?.let { intent.putExtra(PUT_FLEA, it) }
            TransitionHelper.startTran(context, intent, arrPair)
        }


        fun gotoFleaActivity(context: Context, isTop: Boolean, addType: Int?, flea: Flea, arrPair: Array<androidx.core.util.Pair<View, String>>?) {
            TransitionHelper.startTran(context, getIntent(context, isTop, addType, flea), null)
        }

        private fun getIntent(context: Context, isTop: Boolean, addType: Int?, flea: Flea) =
                Intent(context, FleaActivity::class.java).apply {
                    if (isTop) addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
                    addType?.let { putExtra(PUT_ACTION, it) }
                    putExtra(PUT_FLEA, flea)
                }

        fun gotoPostActivity(context: Context, isTop: Boolean, addType: Int?, modelKey: String, arrPair: Array<androidx.core.util.Pair<View, String>>?) {
            TransitionHelper.startTran(context, getIntent(context, isTop, addType, modelKey), arrPair)
        }

        fun getIntent(context: Context, isTop: Boolean, addType: Int?, modelKey: String?) =
                Intent(context, FleaActivity::class.java).apply {
                    if (isTop) addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
                    addType?.let { putExtra(PUT_ACTION, it) }
                    putExtra(PUT_KEY, modelKey)
                }


        /**************************************************************************
         * add 일때는 prePost == null
         *************************************************************************/
        fun addPhotoAndFlea(addType: Int, activity: Activity, widthOrigin: Int?, selectedFotoList: ArrayList<Foto>, preflea: Flea, flea: Flea,
                            success: (() -> Unit)?, fail: () -> Unit) {

            PhotoUtil.uploadUriList(activity, widthOrigin, flea, preflea.fotoList, selectedFotoList,
                    successUploading = { finalList ->

                        flea.fotoList = finalList

                        /** gotoFire */
                        batch().apply {
                            when (addType) {
                                ACTION_NEW -> this.set(docRef(EES_FLEA, flea.key), flea)
                                ACTION_UPDATE -> this.update(docRef(EES_FLEA, flea.key), makeUpdateMap(preflea, flea))
                            }
                            /** 계산서 올리기 */
                            if (isNotAdmin(null, null)) this.addCostWithList(flea)
                            /** 신규 MyTag 올리기 */
                            OnTag.addToBatchAllMytag(this)

                        }.myCommit("addPhotoAndFlea",
                                success = {
                                    when (addType) {
                                        ACTION_NEW -> {
                                            completedDB(activity, true, addType, flea)
                                            Cost.new(null, ActType.ADD_FLEA,flea).real()
                                        }
                                        ACTION_UPDATE -> completedDB(activity, true, addType, flea)
                                    }
                                }, fail = {})

                    },
                    fail = { fail() })
        }

        private fun completedDB(activity: Activity, isTop: Boolean, addType: Int, flea: Flea) {
            OnTag.clearMyTagList()
            TToast.showNormal(R.string.complete_upload_refresh_list)
            gotoFleaActivity(activity, isTop, addType, flea, null)
            activity.finish()
        }

//        private fun getBatchFlea(addType: Int, preflea: Flea, flea: Flea): WriteBatch =
//                batch().apply {
//                    when (addType) {
//                        ACTION_NEW -> {
//                            this.set(docRef(EES_FLEA, flea.key), flea)
//                            this.addCostWithList(flea)
////                            this.addAct(Cost.new(null,ActType.ADD_FLEA,  flea))
//                        }
//                        ACTION_UPDATE -> this.update(docRef(EES_FLEA, flea.key), makeUpdateMap(preflea, flea))
//                    }
//                    OnTag.addToBatchAllMytag(this)
//                }

        private fun makeUpdateMap(preData: Flea, data: Flea) =
                mutableMapOf<String, Any?>().apply {
                    checkUpdate(preData.title, data.title, CH_TITLE)
                    checkUpdate(preData.comment, data.comment, CH_COMMENT)
                    checkUpdate(preData.price, data.price, CH_PRICE)
                    data.fotoList?.let { put(CH_FOTO_LIST, it) }  // fotolist 는 비교하기가 너무 까다롭다. 그냥 무조건 업데이트할 것.
                    data.mediaMap?.let { put(CH_MEDIA_MAP, it) }
                    data.tagList?.let { put(CH_TAG_LIST, it) }

                    checkUpdate(preData.local, data.local, CH_LOCAL)
                    checkUpdate(preData.contact, data.contact, CH_CONTACT)

                    put(CH_ALLOW_REPLY, data.allowReply)
                    if (isNotAdmin(null, null)) put(CH_UPDATE_DATE, rightNow())
                }

        fun updateStatus(key: String, status: Status, success: () -> Unit, fail: (Exception) -> Unit) {
            commonStatusUpdate(EES_FLEA, key, status, success = { success() }, fail = { fail(it) })
        }


        /********************************************************
         * 익명 포스팅으로 변경.
         * cost 추가. credit 사용.
         *******************************************************/
//        fun setAnonymous(context: Context, flea: Flea, success: () -> Unit, fail: () -> Unit) {
//            WAIT_TYPE.BASIC_2.show(context)
//            OnCost.checkPayment(ActType.ADD_ANONYMOUS_POST,
//                    allow = {
//                        flea.anonymous = true
//                        batch().apply {
//                            update(docRef(EES_FLEA, flea.key), CH_ANONYMOUS, true)
////                            addCost(Cost.payment(null, ActType.ADD_ANONYMOUS_FLEA, flea, null))
//                            addCost(Cost.payment(null, ActType.ADD_ANONYMOUS_FLEA, flea, null,null,null))
//                        }.myCommit("setAnonymous flea",
//                                success = {
//                                    //                                    MyCircle.cancel()
//                                    success()
//                                }, fail = { fail() })
//                    }, deny = { fail() })
//        }


        /**************************************************************************
         * flea 받고  status 체크하고  정상이면 이동
         *************************************************************************/
        fun checkAndGo(context: Context, isTop: Boolean, key: String, action: Int?, status: Status, arrPair: Array<androidx.core.util.Pair<View, String>>?, onSuccess: ((Flea) -> Unit)?, onNotNormal: ((Flea) -> Unit)?) {
            if(OnMyClick.isDoubleClick()) return

            ESget.getFlea(key,
                    success = { flea1 ->

                        if (flea1 == null) {
                            TToast.showNormal(R.string.error_data_deleted)
                            MyCircle.cancel()
                        } else {
                            if (flea1.statusNo >= status.no) {
                                onSuccess?.invoke(flea1)
                                gotoFleaActivity(context, isTop, action, flea1, arrPair)
                            } else {
                                onNotNormal?.invoke(flea1)
                                TToast.showAlert(flea1.statusNo.getStatus().msg)
                                MyCircle.cancel()
                            }
                        }

                    }, fail = {})
        }

        /*** 상태체크 없이 그냥 고 */
//        fun justGo(context: Context, isTop: Boolean, flea: Flea, arrPair: Array<androidx.core.util.Pair<View, String>>? ) {
//            gotoFleaActivity(context, isTop, null, flea, arrPair)
//        }


        /*****************************************************************
         *************************** 검색 **********************************
         ****************************************************************/

        /*** es 검색하기 */
        fun getMyFlea(size: Int, from: Int, success: (ArrayList<Flea>?) -> Unit, fail: () -> Unit) {
            val query = ESquery.matchUID(size, from, OnPeer.myUid(), Status.NORMAL)
            ESget.getFleaList(query,
                    success = {
                        lllogI("getMyFlea list : ${it?.size}")
                        success(it)
                    }, fail = {
                fail()
                lllogI("getMyFlea fail")
            })
        }


    }

}