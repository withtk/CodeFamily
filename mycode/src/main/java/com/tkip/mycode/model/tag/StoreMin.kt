package com.tkip.mycode.model.tag

import android.os.Parcel
import android.os.Parcelable
import com.google.firebase.firestore.Exclude

data class StoreMin(

        var storeKey: String = "storeKey",    // key = storeKey와 동일
        var mainKey: String = "mainKey",
        var photo: String = "photo",
        var mainLoca: String? = null,
        var name: String = "name"

) : Parcelable {
    @Exclude
    fun toMap(): Map<String, Any> {

        val result = HashMap<String, Any>()
        result["storeKey"] = storeKey
        result["mainKey"] = mainKey
        result["name"] = name

        return result
    }

    constructor() : this("storeKey")
//    constructor(store: Store) : this(storeKey = store.key, mainKey = "mainKey", photo = store.fotoList[0].thumbUrl!!, mainLoca = store.mainLoc, name = store.name)

    constructor(source: Parcel) : this(
            source.readString()!!,
            source.readString()!!,
            source.readString()!!,
            source.readString(),
            source.readString()!!
    )

    override fun describeContents() = 0

    override fun writeToParcel(dest: Parcel, flags: Int) = with(dest) {
        writeString(storeKey)
        writeString(mainKey)
        writeString(photo)
        writeString(mainLoca)
        writeString(name)
    }

    companion object {
        @JvmField
        val CREATOR: Parcelable.Creator<StoreMin> = object : Parcelable.Creator<StoreMin> {
            override fun createFromParcel(source: Parcel): StoreMin = StoreMin(source)
            override fun newArray(size: Int): Array<StoreMin?> = arrayOfNulls(size)
        }
    }
}