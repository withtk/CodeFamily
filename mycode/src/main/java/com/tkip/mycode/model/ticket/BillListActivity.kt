package com.tkip.mycode.model.ticket

import android.os.Bundle
import androidx.annotation.LayoutRes
import com.tkip.mycode.R
import com.tkip.mycode.databinding.ActivityBillListBinding
import com.tkip.mycode.funs.common.lllogD
import com.tkip.mycode.funs.custom.toolbar.ViewToolbarEmpty
import com.tkip.mycode.init.EES_BILL
import com.tkip.mycode.init.PUT_COUNT_TYPE_NO
import com.tkip.mycode.init.PUT_RV_TYPE
import com.tkip.mycode.init.PUT_UID
import com.tkip.mycode.init.base.getStr
import com.tkip.mycode.init.my_bind.BindingActivity
import com.tkip.mycode.model.count.CountType
import com.tkip.mycode.model.count.getCountType
import com.tkip.mycode.model.peer.isAdmin
import com.tkip.mycode.model.peer.myuid
import com.tkip.mycode.util.lib.retrofit2.ESget
import com.tkip.mycode.util.lib.retrofit2.ESquery
import com.tkip.mycode.util.tools.recycler.OnRv

/**
 * 구매내역
 */
class BillListActivity : BindingActivity<ActivityBillListBinding>() {
    @LayoutRes
    override fun getLayoutResId() = R.layout.activity_bill_list

    private val uid by lazy { intent?.getStringExtra(PUT_UID) }
    private val rvType by lazy { intent?.getIntExtra(PUT_RV_TYPE, TicketAdapter.rvBillNormal) ?: TicketAdapter.rvBillNormal }

    private lateinit var ticketAdapter: TicketAdapter
    private val items = arrayListOf<Ticket>()
    private var esSize = 20
    private var esLastIndex = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        initView()
    }

    private fun initView() {
        b.activity = this

        /** 툴바 */
        when (rvType) {
            TicketAdapter.rvBillManage -> b.frameToolbar.addView(ViewToolbarEmpty(this@BillListActivity, R.string.title_bill_list_admin.getStr(), onBack = { onBackPressed() }, onMore = null))
            TicketAdapter.rvBillNormal -> b.frameToolbar.addView(ViewToolbarEmpty(this@BillListActivity, R.string.title_bill_list.getStr(), onBack = { onBackPressed() }, onMore = null))
        }

        lllogD("BillListActivity rvType $rvType")

        /** Adaper 셋팅 */
        ticketAdapter = TicketAdapter(items, rvType, onPurchase = { })
        b.inRefresh.rv.adapter = ticketAdapter
//        b.inRefresh.refresh.setEnableRefresh(false)   //상단 refresh는 불능.
//        b.inRefresh.refresh.setEnableLoadMore(false)  // 하단 loadMore 불능.
        b.inRefresh.refresh.setOnRefreshListener { getEsDataList(0) }
        b.inRefresh.refresh.setOnLoadMoreListener { getEsDataList(esLastIndex) }

        getEsDataList(0)

    }

    private fun getEsDataList(from: Int) {
        val query =
                if (uid == null) ESquery.matchAll(esSize, from)
                else ESquery.billList(esSize, from, uid!!)

        ESget.getTicketList(EES_BILL, query,
                success = {
                    esLastIndex = OnRv.setRvRefresh(it, from, items, b.inRefresh, true)
                    ticketAdapter.notifyDataSetChanged()
                }, fail = {})
    }


}