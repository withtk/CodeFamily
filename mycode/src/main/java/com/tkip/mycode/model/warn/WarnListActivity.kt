package com.tkip.mycode.model.warn

import android.os.Bundle
import android.view.MenuItem
import androidx.annotation.LayoutRes
import com.tkip.mycode.R
import com.tkip.mycode.databinding.ActivityWarnListBinding
import com.tkip.mycode.funs.custom.toolbar.ViewToolbarEmpty
import com.tkip.mycode.funs.search.model.CustomListWarn
import com.tkip.mycode.init.PUT_BOOLEAN
import com.tkip.mycode.init.base.getStr
import com.tkip.mycode.init.my_bind.BindingActivity

/**
 */
class WarnListActivity : BindingActivity<ActivityWarnListBinding>() {
    @LayoutRes
    override fun getLayoutResId() = R.layout.activity_warn_list

    private val isWarn by lazy { intent.getBooleanExtra(PUT_BOOLEAN, true) }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        initView()
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            android.R.id.home -> onBackPressed()
        }
        return super.onOptionsItemSelected(item)
    }

    private fun initView() {
        b.activity = this

        /** Toolbar */
        val title = if (isWarn) R.string.admin_title_warn_list.getStr() else R.string.admin_title_penalty_list.getStr()
        ViewToolbarEmpty(this@WarnListActivity, title,
                onBack = { onBackPressed() },
                onMore = null
        ).apply { this@WarnListActivity.b.frameToolbar.addView(this) }


        /** list */
        val customSearchWarn = CustomListWarn(this@WarnListActivity, isWarn, null).apply {
            this@WarnListActivity.b.frame.addView(this)
        }

        customSearchWarn.getEsDataList(0)

    }


}