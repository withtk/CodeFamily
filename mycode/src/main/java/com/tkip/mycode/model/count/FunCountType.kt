package com.tkip.mycode.model.count

import android.os.Handler
import android.view.View
import android.widget.CheckBox
import com.tkip.mycode.R
import com.tkip.mycode.dialog.toast.toastNormal
import com.tkip.mycode.funs.common.lllogI
import com.tkip.mycode.init.*
import com.tkip.mycode.init.base.getStr
import com.tkip.mycode.model.board.Board
import com.tkip.mycode.model.board.BoardType
import com.tkip.mycode.model.count.quick.OnQuick
import com.tkip.mycode.model.my_enum.DataType
import com.tkip.mycode.model.my_enum.anyThumb
import com.tkip.mycode.model.my_enum.anyTitle
import com.tkip.mycode.model.my_enum.getMyDataType
import com.tkip.mycode.util.tools.fire.Firee
import com.tkip.mycode.util.tools.fire.batch
import com.tkip.mycode.util.tools.fire.docRef
import com.tkip.mycode.util.tools.fire.myCommit


/**************************** stCountChild  ************************/
val COUNT_TYPE_POST by lazy { arrayOf(CountType.UP, CountType.DOWN, CountType.BOOK, CountType.PUSH, CountType.VIEW_UID) }
val COUNT_TYPE_FLEA_BOOK by lazy { arrayOf(CountType.BOOK, CountType.PUSH, CountType.VIEW_UID) }
val COUNT_TYPE_BOARD_STORE by lazy { arrayOf(CountType.BOOK, CountType.PUSH) }

enum class CountType(val no: Int, val child: String, val msgOn: String?, val msgOff: String?, val listTitle: String?, val noDataMsg: String?) {

//    WARN(5, CH_WARN_COUNT),

    UP(100, CH_THUMB_UP_COUNT_LOWER, R.string.complete_add_up_count.getStr(), R.string.complete_del_up_count.getStr(), null, null),
    DOWN(200, CH_THUMB_DOWN_COUNT_LOWER, R.string.complete_add_down_count.getStr(), R.string.complete_del_down_count.getStr(), null, null),
    BOOK(300, CH_BOOKMARK_COUNT_LOWER, R.string.complete_add_bookmark.getStr(), R.string.complete_del_bookmark.getStr(), R.string.title_my_book_list.getStr(), R.string.info_no_my_book.getStr()),
    VIEW(400, CH_VIEW_COUNT_LOWER, null, null, null, null),

    VIEW_UID(500, CH_VIEW_UID_COUNT_LOWER, null, null, null, null),   // 해당 post에 내uid 올리기.

    //    REPLY(600, CH_REPLY_COUNT_LOWER, null, null, null, null),
    PUSH(700, CH_PUSH_COUNT_LOWER, R.string.complete_add_push.getStr(), R.string.complete_del_push.getStr(), R.string.title_my_push_list.getStr(), R.string.info_no_my_push.getStr()),
//    ONLINE(800, CH_ONLINE_COUNT_LOWER, null, null, null, null),
    ;

    /**     * DB의 최상위 child name     */
    fun getTopChild(any: Any) = getTopChild(any.getMyDataType())

    fun getTopChild(dataType: DataType) = "${dataType.child}_${child}"


    /**     * 체크박스 로직   */
    fun updateCount(any: Any, count: Count?, v: CheckBox, success: (Count?) -> Unit, fail: () -> Unit) {

        /***********   HANDLE CHECKBOX   *****************/
//        v as CheckBox
        v.isEnabled = false   // 클릭 불가

        lllogI("CountType.updateCount :: count($count) v.isChecked(${v.isChecked}) ")

        /** count 변경 : 기본 보드가 아닐때만 카운트 보이기.*/
        if (!(any is Board && any.boardTypeNo == BoardType.FIXED.no))
            if (!v.text.isNullOrBlank()) {
                val count1 = v.text.toString().toInt() + if (v.isChecked) 1 else -1
                if (this != DOWN) v.text = count1.toString()   // thumbDown은 카운트 표시 안 함.
            }

        Handler().postDelayed({ v.isEnabled = true }, 2000)   // 2초후에 버튼 정상화.

        /***********   UPDATE COUNT FIRE   *****************/
        v.isChecked.let { on ->
            /** 증가 */
            if (on) {
                if (this == BOOK) {
                    val newCount = Count(any, this)
                    batch().apply {
                        set(docRef(newCount.topChildName, newCount.key), newCount) // add count
                        set(docRef(EES_QUICK, newCount.key), newCount)    // add quick
                        myCommit("updateCount set $this", {}, {})
                    }
                } else {
                    Firee.addCount(Count(any, this), success = { success(it) }, fail = { fail() })
                }
            }
            /** 감소 */
            else {
                if (this == BOOK) {
                    count?.let {
                        batch().apply {
                            delete(docRef(count.topChildName, count.key)) // delete count
                            delete(docRef(EES_QUICK, count.key)) // delete quick
                            myCommit("updateCount delete $this", {}, {})
                        }
                    }
                } else count?.let { it.delete({}, {}) }
            }
//        delete(success = { success(on, it) }, fail = { fail() })
            toastNormal(if (on) msgOn else msgOff)  // 처리완료 토스트
        }
    }

}

fun Int.getCountType(): CountType {
    CountType.values().forEach { if (it.no == this) return it }
    return CountType.BOOK
}


/*************************************************************************************/
/********************  ***********************************************************/
/*************************************************************************************/

/** * post, board */
fun Count.delete(success: (Count) -> Unit, fail: () -> Unit) {
    lllogI("Count.delete ::")
    Firee.deleteCount(this, { success(it) }, { fail() })
}


/**
 * model data 동기화하기 in 내 알림, 내 북마크, 퀵메뉴
 */
fun Count.syncModelInHolder(quick: Boolean, new: Any, success: () -> Unit) {
    /**     체크 사항 : thumb, title   */
    if (this.photoThumb != new.anyThumb(0) || this.modelTitle != new.anyTitle()) {
        this.photoThumb = new.anyThumb(0)
        this.modelTitle = new.anyTitle()
        if (quick) Firee.updateQuick(this, success = { success() }, fail = {})
        else Firee.updateCount(this, success = { success() }, fail = {})
    }
}
