package com.tkip.mycode.model.manage

import android.content.res.Resources
import android.os.Build
import android.os.Parcel
import android.os.Parcelable
import androidx.core.os.ConfigurationCompat
import com.tkip.mycode.BuildConfig
import com.tkip.mycode.init.base.createNanoKey
import com.tkip.mycode.util.tools.date.rightNow
import java.util.*

/**
 * 모두 하나의 collection에
 * 현재의 device는 addDate의 가장 큰 값으로 찾기.
 */
data class Device(

        var key: String = createNanoKey(),
        var uid: String = "uid",
        var nick: String = "nick",

        var brand: String? = null,
        var manufacturer: String? = null,
        var model: String? = null,
        var country: String? = null,

        var display_country: String? = null,
        var language: String? = null,
        var time_id: String? = null,
        var time_name: String? = null,

        var sdkNo: Int? = null,
        var appNo: Int? = null,
        var addDate: Long = rightNow()

) : Parcelable {
    constructor(uid: String, nick: String, locale: Locale, timeZone: TimeZone) : this(
            uid = uid,
            nick = nick,

            brand = Build.BRAND,
            manufacturer = Build.MANUFACTURER,
            model = Build.MODEL,
            country = locale.country,

            display_country = locale.displayCountry,
            language = locale.displayLanguage,
            time_id = timeZone.id,
            time_name = timeZone.displayName,

            sdkNo = Build.VERSION.SDK_INT,
            appNo = BuildConfig.VERSION_CODE
    )

    constructor(source: Parcel) : this(
            source.readString()!!,
            source.readString()!!,
            source.readString()!!,
            source.readString(),
            source.readString(),
            source.readString(),
            source.readString(),
            source.readString(),
            source.readString(),
            source.readString(),
            source.readString(),
            source.readValue(Int::class.java.classLoader) as Int?,
            source.readValue(Int::class.java.classLoader) as Int?,
            source.readLong()
    )

    override fun describeContents() = 0

    override fun writeToParcel(dest: Parcel, flags: Int) = with(dest) {
        writeString(key)
        writeString(uid)
        writeString(nick)
        writeString(brand)
        writeString(manufacturer)
        writeString(model)
        writeString(country)
        writeString(display_country)
        writeString(language)
        writeString(time_id)
        writeString(time_name)
        writeValue(sdkNo)
        writeValue(appNo)
        writeLong(addDate)
    }

    companion object {

        fun create( uid: String, nick: String): Device {
            val locale = ConfigurationCompat.getLocales(Resources.getSystem().configuration).get(0)
            val timeZone = TimeZone.getDefault()
            return Device(uid, nick, locale, timeZone)
        }

        @JvmField
        val CREATOR: Parcelable.Creator<Device> = object : Parcelable.Creator<Device> {
            override fun createFromParcel(source: Parcel): Device = Device(source)
            override fun newArray(size: Int): Array<Device?> = arrayOfNulls(size)
        }
    }
}
