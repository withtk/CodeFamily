package com.tkip.mycode.model.warn

import com.tkip.mycode.R
import com.tkip.mycode.init.base.getStr
import com.tkip.mycode.model.board.Board
import com.tkip.mycode.model.flea.Flea
import com.tkip.mycode.model.post.Post
import com.tkip.mycode.model.reply.Reply
import com.tkip.mycode.model.room.Room
import java.util.*


/**
 * amount : penalty point
 * period : 업로드 정지 기간
 */
/******************************** 신고 타입 *****************************************/
enum class WarnType(val no: Int, val title: String, val message: String, val amount: Int, val period: Int) {

    DUPLICATED(100, R.string.warn_type_duplicated.getStr(), R.string.warn_type_msg_duplicated.getStr(), 1000, 1),   // 중복된 게시물.
    BAD_WORD(200, R.string.warn_type_badword.getStr(), R.string.warn_type_msg_badword.getStr(), 1000, 1),   // 욕설, 음란물, 비방, 음해.
    BAD_UPLOAD(400, R.string.warn_type_badupload.getStr(), R.string.warn_type_msg_badupload.getStr(), 1000, 1),   // 해당 위치와 성격이 맞지 않을떄, 정치적 선전문구 등.
    VIOLATION(600, R.string.warn_type_regulation_violation.getStr(), R.string.warn_type_msg_regulation_violation.getStr(), 1000, 1), // 어플 규정 위반.

    FAKE(700, R.string.warn_type_fake.getStr(), R.string.warn_type_msg_fake.getStr(), 1000, 1),    // 허위매물 : 중고, 부동산
    ILLEGAL(800, R.string.warn_type_illegal.getStr(), R.string.warn_type_msg_illegal.getStr(), 1000, 1),   // 불법
    ;

}


/** * ArrayList 만들기. */
fun ArrayList<String>.setWarnType(): ArrayList<String> {
    WarnType.values().forEach { this.add(it.title) }
    return this
}

fun Int.getWarnType(): WarnType {
    WarnType.values().forEach { if (it.no == this) return it }
    return WarnType.BAD_UPLOAD
}

fun String.getWarnType(): WarnType {
    WarnType.values().forEach { if (it.title == this) return it }
    return WarnType.BAD_UPLOAD
}

/** * warn List */
fun getWarnArr(any: Any): Array<WarnType> {
    return when (any) {
        is Board -> arrayOf(WarnType.BAD_WORD, WarnType.BAD_UPLOAD, WarnType.VIOLATION, WarnType.ILLEGAL)
        is Post -> arrayOf(WarnType.BAD_WORD, WarnType.BAD_UPLOAD, WarnType.VIOLATION, WarnType.ILLEGAL)
        is Flea -> arrayOf(WarnType.FAKE, WarnType.BAD_WORD, WarnType.BAD_UPLOAD, WarnType.VIOLATION, WarnType.ILLEGAL)
        is Room -> arrayOf(WarnType.FAKE, WarnType.BAD_WORD, WarnType.BAD_UPLOAD, WarnType.VIOLATION, WarnType.ILLEGAL)
        is Reply -> arrayOf(WarnType.BAD_WORD, WarnType.BAD_UPLOAD, WarnType.VIOLATION, WarnType.ILLEGAL)
        else -> arrayOf(WarnType.BAD_WORD, WarnType.BAD_UPLOAD, WarnType.VIOLATION, WarnType.ILLEGAL)
    }
}

