package com.tkip.mycode.model.ticket

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.RecyclerView
import com.tkip.mycode.R
import com.tkip.mycode.admin.menu.AMenu
import com.tkip.mycode.admin.menu.ARR_BILL_ADMIN
import com.tkip.mycode.admin.menu.OnMenu
import com.tkip.mycode.databinding.HolderBillBinding
import com.tkip.mycode.databinding.HolderTicketBinding
import com.tkip.mycode.dialog.OnDDD
import com.tkip.mycode.dialog.alert.MyAlert
import com.tkip.mycode.funs.common.onClick
import com.tkip.mycode.init.CH_STATUS_NO
import com.tkip.mycode.init.EES_BILL
import com.tkip.mycode.model.cost.balance.Balance
import com.tkip.mycode.model.my_enum.Status
import com.tkip.mycode.util.tools.anim.goneSlideDown
import com.tkip.mycode.util.tools.anim.hideSlideUp
import com.tkip.mycode.util.tools.anim.showSlideDown
import com.tkip.mycode.util.tools.etc.OnMyClick
import com.tkip.mycode.util.tools.fire.batch
import com.tkip.mycode.util.tools.fire.docRef
import com.tkip.mycode.util.tools.fire.myCommit

/**
 * 구매할 티켓 리스트
 */
class TicketAdapter(private val items: ArrayList<Ticket>, private val rvType: Int, val onPurchase: (skuId: String) -> Unit) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    private lateinit var activity: AppCompatActivity

    companion object {
        const val rvTicketNormal = 100
        const val rvTicketManage = 200
        const val rvBillNormal = 300
        const val rvBillManage = 400
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        this.activity = parent.context as AppCompatActivity
        return when (rvType) {
            rvTicketNormal -> TicketHolder(HolderTicketBinding.inflate(LayoutInflater.from(parent.context), parent, false))
            rvTicketManage -> TicketHolder(HolderTicketBinding.inflate(LayoutInflater.from(parent.context), parent, false))
            rvBillNormal, rvBillManage -> BillHolder(HolderBillBinding.inflate(LayoutInflater.from(parent.context), parent, false))
            else -> TicketHolder(HolderTicketBinding.inflate(LayoutInflater.from(parent.context), parent, false))
        }
    }

    override fun onBindViewHolder(h: RecyclerView.ViewHolder, p: Int) {

        when (rvType) {
            rvTicketNormal -> (h as TicketHolder).bind(items[p])
            rvTicketManage -> (h as TicketHolder).bind(items[p])
            rvBillNormal, rvBillManage -> (h as BillHolder).bind(items[p])
            else -> (h as TicketHolder).bind(items[p])
        }
    }

    override fun getItemCount(): Int {
        return items.size
    }


    /**
     * NORMAL
     */
    inner class TicketHolder(val b: HolderTicketBinding) : RecyclerView.ViewHolder(b.root) {

        private lateinit var ticket: Ticket

        fun bind(ticket: Ticket) {
            this.ticket = ticket
            b.holder = this
            b.ticket = ticket
            val isManage = (rvType == rvTicketManage)
            b.manage = isManage

            if (isManage) if (ticket.statusNo == Status.TICKET_DISABLED.no) b.line.alpha = 0.5f

            b.executePendingBindings()
        }

        fun onClickUpdate(v: View) {
            OnMyClick.setDisableAWhileBTN(v)
            ADDticketDialog.newInstance(ticket).show(activity.supportFragmentManager, "dialog")
        }

        /**         * 구매 버튼         */
        fun onClickPurchase(v: View) {
            OnMyClick.setDisableAWhileBTN(v)
            OnDDD.confirm(activity, R.string.info_purchase_no_refund, ok = { onPurchase(ticket.skuId) }, cancel = {})
        }

    }


    /**
     * Bill
     */
    inner class BillHolder(val b: HolderBillBinding) : RecyclerView.ViewHolder(b.root) {

        private lateinit var ticket: Ticket

        fun bind(ticket: Ticket) {
            this.ticket = ticket
            b.holder = this
            b.ticket = ticket
            b.inPay.balance = Balance.new(ticket)
            b.manage = (rvType == rvBillManage)

            b.cons.setOnClickListener {
                /*** lineBelow show/hide */
                if (b.lineBelow.visibility == View.VISIBLE) b.lineBelow.goneSlideDown(false)
                else b.lineBelow.showSlideDown(false)
            }

            b.cons.setOnLongClickListener {
                MyAlert.showMenuList(activity, ARR_BILL_ADMIN, onSelect = {
                    when (it) {
                        AMenu.REFUND -> {
                            /*** 환불처리로 Ticket 변경.*/
                            OnDDD.confirm(activity, Status.BILL_REFUNDED.dialogMsg, ok = {
                                OnDDD.pw(activity, ok = {
                                    batch().apply {
                                        update(docRef(EES_BILL, ticket.key), CH_STATUS_NO, Status.BILL_REFUNDED.no)
                                        myCommit("BillHolder BILL_REFUNDED",
                                                success = {
                                                    ticket.statusNo = Status.BILL_REFUNDED.no
                                                    notifyItemChanged(adapterPosition)
                                                }, fail = {})
                                    }
                                })
                            }, cancel = {})
                        }
                        else -> {
                        }
                    }
                })
                return@setOnLongClickListener true
            }
            b.executePendingBindings()
        }

    }


}