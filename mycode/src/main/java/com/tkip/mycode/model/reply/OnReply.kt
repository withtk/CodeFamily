package com.tkip.mycode.model.reply

import android.content.Context
import android.view.View
import android.widget.LinearLayout
import com.firebase.ui.firestore.FirestoreRecyclerOptions
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.firestore.Query
import com.google.firebase.firestore.QuerySnapshot
import com.tkip.mycode.R
import com.tkip.mycode.admin.menu.*
import com.tkip.mycode.databinding.LayoutRvReplyBinding
import com.tkip.mycode.dialog.OnDDD
import com.tkip.mycode.dialog.alert.MyAlert
import com.tkip.mycode.dialog.progress.MyCircle
import com.tkip.mycode.dialog.toast.TToast
import com.tkip.mycode.funs.common.OnFunction
import com.tkip.mycode.funs.common.lllogD
import com.tkip.mycode.funs.common.lllogI
import com.tkip.mycode.init.*
import com.tkip.mycode.init.base.getStr
import com.tkip.mycode.init.base.gte
import com.tkip.mycode.init.part.CodeApp
import com.tkip.mycode.model.cost.Cost
import com.tkip.mycode.model.flea.Flea
import com.tkip.mycode.model.my_enum.*
import com.tkip.mycode.model.peer.OnPeer
import com.tkip.mycode.model.peer.isAdmin
import com.tkip.mycode.model.peer.isAuthor
import com.tkip.mycode.model.peer.isMe
import com.tkip.mycode.model.post.Post
import com.tkip.mycode.model.room.Room
import com.tkip.mycode.model.warn.OnWarn
import com.tkip.mycode.util.lib.photo.util.PhotoUtil
import com.tkip.mycode.util.tools.fire.*

class OnReply {

    companion object {

        /**         * reply가 담긴 모델로 고고         */
        fun gotoModelActivity(context: Context, isTop: Boolean, action: Int?, reply: Reply, arrPair: Array<androidx.core.util.Pair<View, String>>?) {
            reply.dataTypeNo.dataType().justGo(context, isTop, reply.modelKey, arrPair)
        }

        /**
         *
         */
        fun setAdapter(context: Context, child: String, key: String, child2: String, any: Any, inRvReply: LayoutRvReplyBinding, linearReplyAddBox: LinearReplyAddBox, onCompleteAdoption: () -> Unit): ReplyAdapterFireS {
            val options = getOptions(child, key, child2, null, null)

            val adapter = ReplyAdapterFireS(options, any,

                    onCreateReReply = { reply1, _ ->
                        ViewReplyEarlierWrite.add(context, linearReplyAddBox.b.lineAttachBox, reply1, null, null, null)
                        REPLY_ATTACH_TYPE = ATTACH_REPLY
                        REPLY_ATTACH_REPLY_KEY = reply1.key
                    },

                    onClickEarlierReply = {
                        //todo : 스크롤 이동할 것.
                        /* b.rv.smoothScrollToPosition(it)*/
                    },

                    /*** POST Only */
                    onCompleteAdoption = { onCompleteAdoption() })

            inRvReply.rv.adapter = adapter    //set adapter in RV
            inRvReply.inNoData.tvNoData.text = R.string.info_no_data_reply.getStr()     // "댓글 없습니다"

            adapter.startListening()   // stopListening : in SuperClass

            /**         * 리프레쉬 : 가끔 안 보일때가 있다.         */
            android.os.Handler().postDelayed({
                adapter.notifyDataSetChanged()
            }, 1000)

            return adapter
        }

        /**  *  attach한 것들 모두 초기화   */
        fun resetAttachReply(lineAttachBox: LinearLayout?) {
            lineAttachBox?.removeAllViews()
            REPLY_ATTACH_REPLY_KEY = null
            REPLY_ATTACH_TOSS = null
            PhotoUtil.resetAllUriList()  // 사진 첨부한 것 해제.
        }


        /****************************************************************
         * 모델 삭제전 reply 체크
         * onAnonymous : 익명으로 전환.
         * onAnonymous : 삭제하러 가기.
         *****************************************************************/
        fun deleteModel(context: Context, replyCount: Int, onAnonymous: () -> Unit, onGotoDelete: () -> Unit) {
            /*** reply 개수 체크 */
            if (replyCount.gte(1)) {
                val str = String.format(R.string.info_not_delete_by_reply_count.getStr(), ActType.ADD_ANONYMOUS_POST.getCrdAmount(), PayType.CREDIT.title)
                MyAlert.showConfirm(context, str, ok = { onAnonymous() }, cancel = {})
            } else MyAlert.showDelete(context, ok = { onGotoDelete() }, cancel = {})
        }

        /****************************************************************
         * add   *******************************************************
         *****************************************************************/
        fun addPhotoAndReply(context: Context, reply: Reply, success: () -> Unit, fail: () -> Unit) {
            //todo : 하루 최대 업로드 제한 걸기. 500
            PhotoUtil.uploadUriSingle(context, reply,
                    successUploading = { foto ->
                        foto?.let {
                            reply.thumbUrl = it.thumbUrl
                            reply.url = it.url
                            reply.replyTypeNo = ReplyType.PHOTO.no
                        }
//                        MyProgress.cancel()
                        MyCircle.cancel()
                        batch().apply {
                            this.set(docRef(reply.dataTypeNo.dataType().child, reply.modelKey, EES_REPLY, reply.key), reply)
//                            this.addAct(Cost.new(null, ActType.ADD_REPLY, reply))
                            myCommit("addPhotoAndReply",
                                    success = {
                                success()
                                        Cost.new(null, ActType.ADD_REPLY, reply).real()
                                    }, fail = { fail() })
                        }
                    },
                    fail = { fail() })
        }

        /**
         * 연속적인 댓글 쓰기 방지
         */
        fun notAllowedAddReply(adapter: ReplyAdapterFireS?): Boolean {

            if (adapter != null && adapter.itemCount > 5) {
                var count = 0  // 도배 카운트
                for (j in -1 downTo -5) {
                    if (adapter.getItem(adapter.itemCount + j).authorUID == OnPeer.peer.uid) {
                        count++
                    }

                    if (count >= 5) {
                        TToast.showWarning(R.string.info_prohibition_wall_paper)
                        return true
                    }
                }
            }
            return false
        }


        /****************************************************************
         * LongClick Menu
         *****************************************************************/
        fun showMenuDialog(context: Context, any: Any, reply: Reply, onCreateReReply: () -> Unit, onCompleteAdoption: () -> Unit, onChanged: (Status) -> Unit) {

            MyAlert.showMenuList(context, getMENULIST(any, reply),
                    onSelect = { aMenu ->

                        when (aMenu) {
                            AMenu.COPY -> OnFunction.setClipBoardLink(reply.comment.toString())
                            AMenu.WARN -> {
                                OnWarn.checkAndAdd(context, reply, success = { }, fail = {})

//                                MyAlert.showWarn(context, WarnTypeLIST.REPLY.arr,
//                                        onOk = { warnType, comment ->
//                                            WAIT_TYPE.BASIC_2.show(context)
//                                            val warn = Warn.getWarnData(reply, comment, warnType)
//                                            OnWarn.checkAndAdd(warn, {}, {})
//                                        }, onCancel = {})
                            }
                            AMenu.DELETION -> OnDDD.delete(context,
                                    ok = {
                                        reply.changeStatus(Status.DELETED_USER, success = { onChanged(Status.DELETED_USER) }, fail = {})
                                    }, cancel = {})
                            AMenu.REREPLY -> onCreateReReply()
                            AMenu.STATUS -> MyAlert.showSelectStatus(context, reply, onSelect = { reply.changeStatus(it, success = {onChanged(it)}, fail = {}) }, onCancel = {})

                            /*** POST Only */
                            AMenu.ADOPTION -> {
                                val post = any as Post
                                MyAlert.showConfirm(context, R.string.ask_request_adoption.getStr(),
                                        ok = {
                                            Firee.updateReplyFreePost(
                                                    batch().apply {
                                                        update(docRef(EES_POST, reply.modelKey, EES_REPLY, reply.key), CH_CHOSEN, true)
                                                        update(docRef(EES_POST, reply.modelKey), CH_CHOSEN, true)
//                                                        Cost.addBatchGet(this, reply.authorUID, ActType.GET_REPLY_ADOPTED, post.reward, post)
//                                                        addCost(Cost.take(reply.authorUID, ActType.GET_REPLY_ADOPTED, post.reward, post))
                                                        addCost(Cost.new(reply.authorUID, ActType.GET_REPLY_ADOPTED, post, null, null, post.reward, null))
                                                    },
                                                    success = {
                                                        /**
                                                         * todo : functions에서
                                                         * post.isChose변경,
                                                         * 채택받은 유저 credit 변동.
                                                         * credit 변동 기록.
                                                         *
                                                         */
                                                        onCompleteAdoption()
                                                        TToast.showNormal(CodeApp.getAppContext().getString(R.string.complete_adoption))
                                                    }, fail = {})
                                        }, cancel = {})
                            }
                            else -> {
                            }
                        }
                    })
        }


        /**         * 채택 여부에 따라 Dialog 메뉴 list 변경.         */
        private fun getMENULIST(any: Any, reply: Reply): Array<AMenu> {
            return when (any.getMyDataType()) {
                DataType.POST, DataType.FLEA, DataType.ROOM ->
                    when {
                        isAdmin() -> ARR_REPLY_ADMIN    // 어드민
                        reply.authorUID.isAuthor(null, null) -> ARR_REPLY_MY   // 내 리플
                        else -> ARR_REPLY_USER  // 타인 리플
                    }
                else -> ARR_NOTHING
            }
        }


        /****************************************************************
         * Reply Listen : 실시간을 위해 post>reply 안에 넣음.
         *****************************************************************/
        fun getOptions(child: String, key: String, child2: String, onSuccess: ((QuerySnapshot) -> Unit)?, onFail: (() -> Unit)?): FirestoreRecyclerOptions<Reply> {
            val query = FirebaseFirestore.getInstance().collection(child).document(key).collection(child2)
                    .orderBy(CH_ADD_DATE, Query.Direction.ASCENDING)

            onSuccess?.let {
                query.get()
                        .addOnSuccessListener {
                            lllogI("addOnSuccessListener size : ${it.size()}")
                            onSuccess.invoke(it)
                        }
                        .addOnFailureListener {
                            lllogD("addOnFailureListener $it")
                            onFail?.invoke()
                        }
            }

            val options = FirestoreRecyclerOptions.Builder<Reply>()
                    .setQuery(query, Reply::class.java)
                    .build()
            return options
        }


    }

}