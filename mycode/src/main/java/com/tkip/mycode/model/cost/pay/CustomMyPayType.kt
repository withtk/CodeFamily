package com.tkip.mycode.model.cost.pay

import android.content.Context
import android.view.LayoutInflater
import android.widget.LinearLayout
import androidx.databinding.DataBindingUtil
import com.tkip.mycode.R
import com.tkip.mycode.databinding.CustomMyPayTypeBinding
import com.tkip.mycode.funs.common.lllogM
import com.tkip.mycode.model.my_enum.PayType
import com.tkip.mycode.util.tools.anim.gone
import com.tkip.mycode.util.tools.anim.visi


/**
 * 내 크레딧 표기 : for 결제할 때
 * price : 결제될 금액.
 */
class CustomMyPayType : LinearLayout {
    lateinit var b: CustomMyPayTypeBinding
    lateinit var payType: PayType

    constructor(context: Context) : super(context)
    constructor(context: Context, payType: PayType, price: Int?) : super(context) {
        init(context, payType, price)
    }

    private fun init(context: Context, payType: PayType, price: Int?) {
        this.payType = payType
        b = DataBindingUtil.inflate(LayoutInflater.from(context), R.layout.custom_my_pay_type, this, true)
        b.payType = payType
        b.myAmount = payType.getMyAmount()

        /*** set 메세지 */
        when {
            /*** 크레딧 10000 이하 : 일반 기능결제에서는 10000이하 제한 걸지 말자. */
//            (payType == PayType.BONUS_CREDIT) && !isGte10000Bonus() -> {
//                b.tvMsg.text = R.string.info_my_credit_bonus.getStr()
//                b.tvMsg.visi()
//            }
            /*** 크레딧 부족*/
            (payType.isNotEnough(price)) -> b.tvMsg.visi()
            else -> b.tvMsg.gone()
        }

//        b.price = price?.let { -it }  //  마이너스 반드시 붙일 것.
//        b.btnPrice.onClick { onPrice() }
    }

    /**
     * 결제종류 선택.
     */
    fun setPrice(sum: Int) {
        lllogM("CustomMyPayType payType $payType setPrice $sum")
//        b.tvPrice.text = (-sum).getStringComma()   // 마이너스 붙여서 comma넣기.
//        b.tvPrice.visi()
        b.price = -sum
        CHOSEN_PAY_TYPE = payType

    }




}