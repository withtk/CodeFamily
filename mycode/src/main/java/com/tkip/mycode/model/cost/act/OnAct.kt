package com.tkip.mycode.model.cost.act

import android.content.Context
import android.os.Parcelable
import com.google.firebase.firestore.FirebaseFirestore
import com.tkip.mycode.R
import com.tkip.mycode.dialog.OnDDD
import com.tkip.mycode.dialog.alert.MyAlert
import com.tkip.mycode.dialog.toast.toastWarning
import com.tkip.mycode.funs.common.OnException
import com.tkip.mycode.funs.common.lllogD
import com.tkip.mycode.funs.common.lllogW
import com.tkip.mycode.init.*
import com.tkip.mycode.init.base.createKey
import com.tkip.mycode.init.base.getStr
import com.tkip.mycode.model.cost.Cost
import com.tkip.mycode.model.peer.Peer
import com.tkip.mycode.model.peer.myuid
import com.tkip.mycode.util.lib.retrofit2.ESget
import com.tkip.mycode.util.lib.retrofit2.Retro
import com.tkip.mycode.util.tools.fire.FireReal

/**
 * 유저 활동 업로드.
 */
class OnAct {

    companion object {

        /****************************************************************************************
         * acts
         ****************************************************************************************/
        private val actsList by lazy { arrayListOf<Cost>() }
        private val actsMap by lazy { hashMapOf<String, Pair<String, Int>>() }
        private val actPeerList by lazy { arrayListOf<ActPeer>() }

        fun startActs(context: Context) {
            getAllActs(onSuccess = { msg ->
                MyAlert.showActPeerList(context, msg, actPeerList, onOk = {
                    addActsToES(onSuccess = {
                        transactionToPeer(onSuccess = {
//                                OnDDD.confirm(context, R.string.actpeer_delete_all, ok = {
                            removeMyActs(onSuccess = {
                                OnDDD.info(context, R.string.actpeer_completed_all)
                            }, onFail = {})
//                                }, cancel = {})
                        }, onFail = {})
                    }, onFail = {})
                })

//                OnDDD.confirm(context, msg, ok = {
//
//                }, cancel = {})
            }, onFail = {})
        }


        /**
         * 전체 받아오기
         */
        private fun getAllActs(onSuccess: (msg: String) -> Unit, onFail: () -> Unit) {
            actsList.clear()
            actsMap.clear()
            actPeerList.clear()
            FireReal.getAll(EES_ACT,
                    onSuccess = { snap ->
                        snap?.children?.forEach { dataSnapshot ->
                            dataSnapshot.getValue(Cost::class.java)?.let { cost ->
                                lllogD("OnAct dealAllActs cost :$cost")
                                actsList.add(cost)

                                /** pnt 값이 있을때만 add to MAP */
                                cost.pnt?.let {
                                    if (actsMap[cost.uid] == null) actsMap[cost.uid] = Pair(cost.otherNick
                                            ?: "nick", it)
                                    else actsMap[cost.uid] = Pair(cost.otherNick
                                            ?: "nick", actsMap[cost.uid]!!.second + it)
                                }

                                /** add to ES */
//                                addActsToES(cost, i, snap, onSuccess, onFail)

                                /** add transactionPeer */
//                                transactionPeer(cost.uid)
                            }
                        }

                        actsMap.forEach { (uid, pair) ->
                            actPeerList.add(ActPeer(uid, pair.first, pair.second))
                        }

                        lllogD("OnAct dealAllActs actsList size:${actsList.size}")
                        lllogD("OnAct dealAllActs actsMap size:${actsMap.size}")
                        val msg = String.format(R.string.actpeer_add_to_es.getStr(), actsList.size, actsMap.size)
                        onSuccess(msg)

                    }, onFail = {
                onFail()
                toastWarning(R.string.db_no_data)
            })
        }

        /**
         * acts ES에 넣기
         */
        private fun addActsToES(onSuccess: () -> Unit, onFail: () -> Unit) {
            actsList.forEachIndexed { i, cost ->
                ESget.addModel(Retro.esApi, EES_ACT, cost.key, cost,
                        success = {
                            lllogD("OnAct addMyActs success  ")
                            if (i >= actsList.size - 1) onSuccess()
                        },
                        fail = {
                            lllogW("OnAct addMyActs onFail  ")
                            onFail()
                            OnException.dbNoData(R.string.db_failure2.getStr())
                        })
            }
        }

        /**
         * 트랜잭션 시작
         */
        private fun transactionToPeer(onSuccess: () -> Unit, onFail: () -> Unit) {
            var i = 0
//            actsMap.forEachIndexed
            actsMap.forEach { (uid, pair) ->

                lllogD("OnAct Transaction start ############ uid $uid")
                val sfDocRef = FirebaseFirestore.getInstance().collection(EES_PEER).document(uid)
                FirebaseFirestore.getInstance()
                        .runTransaction { transaction ->
                            transaction.get(sfDocRef).toObject(Peer::class.java)?.let { peer ->
//                                lllogD("OnAct Transaction peer $peer")
                                transaction.update(sfDocRef, CH_POINT, peer.pnt + pair.second)
                                transaction.update(sfDocRef, CH_TOTAL_POINT, peer.totalPoint + pair.second)
                            }
                        }
                        .addOnSuccessListener {
                            lllogD("OnAct Transaction Success i:$i size:${actsMap.size}")
                            i++
                            if (i >= actsMap.size) {
                                onSuccess()
                                lllogD("OnAct Transaction Success ")
                            }
                        }
                        .addOnFailureListener { e ->
                            lllogW("OnAct Transaction failure.", e)
                            onFail()
                            toastWarning(R.string.error_android)
                        }
            }

        }

        /** 해당 유저의 act 올린뒤 삭제하기. */
        private fun removeMyActs(onSuccess: () -> Unit, onFail: () -> Unit) {
            FireReal.delete(EES_ACT, onSuccess = { onSuccess() }, onFail = { onFail() })
        }


    }
}