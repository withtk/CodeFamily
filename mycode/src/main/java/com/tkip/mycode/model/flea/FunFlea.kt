package com.tkip.mycode.model.flea

import android.content.Context
import android.view.View
import com.tkip.mycode.R
import com.tkip.mycode.init.base.getI
import com.tkip.mycode.init.base.isAnyNull
import com.tkip.mycode.init.base.isNull0


/*************************************************************************************/
/********************  ***********************************************************/
/*************************************************************************************/


fun Flea.gotoAddFleaActivity(context: Context, action: Int, arrPair: Array<androidx.core.util.Pair<View, String>>?) {
    OnFlea.gotoAddFleaActivity(context, action, this, arrPair)
}


fun Flea.gotoFleaActivity(context: Context, isTop: Boolean, action: Int?, arrPair: Array<androidx.core.util.Pair<View, String>>?) {
    OnFlea.gotoFleaActivity(context, isTop, action, this, arrPair)
}

/**
 * 유효성 체크
 * 통과 못하면 -> true
 */
fun Flea.inValid( ): Boolean {
    return (this.price.isNull0(100,R.string.alert_need_to_price)
            ||  this.local.isAnyNull(R.string.alert_need_to_local_name)
            || this.title.isAnyNull(R.string.alert_need_to_stuff_name)
//            || this.fotoList.getI(0).isAnyNull(R.string.alert_need_to_photo)
            )
}
