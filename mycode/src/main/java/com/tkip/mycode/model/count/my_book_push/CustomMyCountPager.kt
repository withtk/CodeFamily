package com.tkip.mycode.model.count.my_book_push

import android.content.Context
import android.view.LayoutInflater
import android.widget.LinearLayout
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import com.tkip.mycode.R
import com.tkip.mycode.databinding.CustomMyCountPagerBinding
import com.tkip.mycode.funs.common.lllogI
import com.tkip.mycode.model.count.Count
import com.tkip.mycode.model.count.CountType
import com.tkip.mycode.model.my_enum.DataType
import com.tkip.mycode.util.tools.view_pager.OnPager

/**
 * 내 Count viewPager
 */
class CustomMyCountPager : LinearLayout {
    private lateinit var b: CustomMyCountPagerBinding

    constructor(context: Context) : super(context)
    constructor(context: Context, fragmentManager: FragmentManager, manage: Boolean, uid: String, countType: CountType, onItemClick: (Count) -> Unit) : super(context) {
        lllogI("CustomMyCountPager start")
        b = DataBindingUtil.inflate(LayoutInflater.from(context), R.layout.custom_my_count_pager, this, true)



        val list = arrayListOf<Pair<Fragment, String>>().apply {
            if (countType == CountType.BOOK)
                arrayOf(DataType.BOARD, DataType.POST, DataType.FLEA, DataType.ROOM, DataType.STORE).forEach { add(createPairFrag(manage, uid, it, countType, onItemClick = { count -> onItemClick(count) })) }
            else if (countType == CountType.PUSH)
                arrayOf(DataType.BOARD, DataType.POST, DataType.FLEA, DataType.ROOM).forEach { add(createPairFrag(manage, uid, it, countType, onItemClick = { count -> onItemClick(count) })) }
        }
        lllogI("CustomMyCountPager list : $list")



        OnPager.commonSetPager(fragmentManager, list, b.viewPager, b.tabLayout)
//        b.viewPager.onPageChange({}, { _, _, _ -> }, onSelected = { })

    }

    private fun createPairFrag(manage: Boolean, uid: String, dataType: DataType, countType: CountType, onItemClick: (Count) -> Unit) =
            Pair(MyCountFrag.newInstance(manage, uid, dataType.no, countType.no, onItemClick = { onItemClick(it) }), dataType.title)


}