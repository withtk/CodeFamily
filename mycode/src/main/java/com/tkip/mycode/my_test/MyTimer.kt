package com.tkip.mycode.my_test

import android.os.Handler
import android.os.Message
import com.tkip.mycode.funs.common.lllogI

class MyTimer  : Handler() {

    companion object {
        const val START = 10
        const val ING = 20
        const val STOP = 30
    }

    override fun handleMessage(msg: Message) {
//        super.handleMessage(msg)
        when (msg.what) {
            START -> {
                lllogI("MyTimer handleMessage START")
            }
            ING -> {
                lllogI("MyTimer handleMessage ING")
            }
            STOP -> {
                lllogI("MyTimer handleMessage STOP")
            }
        }


    }
}