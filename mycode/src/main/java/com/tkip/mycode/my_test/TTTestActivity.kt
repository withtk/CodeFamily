package com.tkip.mycode.my_test

import android.content.Intent
import android.os.Bundle
import android.widget.LinearLayout
import androidx.databinding.DataBindingUtil
import com.mancj.slideup.SlideUp
import com.tkip.mycode.R
import com.tkip.mycode.databinding.ActivityTttttestBinding
import com.tkip.mycode.dialog.toast.toastAlert
import com.tkip.mycode.dialog.toast.toastNormal
import com.tkip.mycode.dialog.toast.toastWarning
import com.tkip.mycode.funs.common.OnBasic
import com.tkip.mycode.funs.common.lllogD
import com.tkip.mycode.funs.common.lllogI
import com.tkip.mycode.funs.common.onClick
import com.tkip.mycode.init.my_bind.BaseActivity
import com.tkip.mycode.model.board.Board
import com.tkip.mycode.my_test.sample.assignment.TTTest2222Activity
import com.tkip.mycode.my_test.sample.assignment.ViewTvBox
import com.tkip.mycode.my_test.sample.assignment.closeLIST
import com.tkip.mycode.my_test.sample.assignment.closeMap
import com.tkip.mycode.util.lib.calendar.isNotBidTime
import com.tkip.mycode.util.tools.anim.animStopTurning
import com.tkip.mycode.util.tools.anim.animTurning
import com.tkip.mycode.util.tools.date.rightNowHour
import com.tkip.mycode.util.tools.date.rightNowPHhour
import io.github.ponnamkarthik.richlinkpreview.ViewListener

class TTTestActivity : BaseActivity() {

    private lateinit var b: ActivityTttttestBinding
    private lateinit var slideUp: SlideUp
    private val listBoard: ArrayList<Board> = arrayListOf()


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        b = DataBindingUtil.setContentView(this, R.layout.activity_tttttest)
        b.activity = this

//        showRichPreview()
//        setSlideUp()

        b.btnTest4.onClick {
            b.btnTest4.animTurning()

            val sdfsdf =  Intent(this@TTTestActivity, TTTest2222Activity::class.java)
            startActivity(sdfsdf)

        }

        b.btnTest5.onClick {
            b.btnTest4.animStopTurning()
            makeView()
            //            slideUp.hide()
        }
        b.btnTest6.onClick {
            lllogD("assignment closeMap Size ${closeMap.size}")
            //            list.forEach {peer->
//                Firee.commonADD("TTTestActivity commonADD", docRef(EES_PEER, peer.uid),peer, success = { lllogD("commonADD success   ${peer.uid} ")}, fail = { lllogD("commonADD fail   ${peer.uid} ") })
//            }
//            toastNormal(R.string.info_current_login_test_id)
        }

        b.btnTest7.onClick {

            toastWarning("이것은 btnTest7")
        }
        b.btnTest8.onClick {
            OnBasic.finishAllActivity()
            toastAlert("이것은   토스트")
        }

    }

    fun makeView(){
//        val arr = closeLIST.toTypedArray()
        closeLIST.sortBy { b -> b.g }  //  순으로 정렬
//        arr.sortByDescending { b -> b.g }  // 역순으로 정렬
        b.lineAaa.addView(makeLinear())
        var curG = 0
        closeLIST.forEach { box ->
            b.lineAaa.addView(ViewTvBox(this, box))
//            if(box.g > curG) b.lineAaa.addView(makeLinear())
//            curG = box.g
//            (b.lineAaa.getChildAt(box.g) as LinearLayout).addView(ViewTvBox(this, box))
        }
    }

    private fun makeLinear(): LinearLayout {
        val layout2 = LinearLayout(this)
        layout2.layoutParams = LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT)
        layout2.orientation = LinearLayout.HORIZONTAL
        return layout2
    }

//    private fun setSlideUp() {
//        slideUp = SlideUpBuilder(b.line)
//                //.withGesturesEnabled()
//                //.withHideSoftInputWhenDisplayed()
//                //.withInterpolator()
//                //.withAutoSlideDuration()
//                //.withTouchableAreaPx()
//                //.withTouchableAreaDp()
//                //.withListeners()
//                //.withSavedState()
//                .withListeners(object : SlideUp.Listener.Events {
//                    override fun onVisibilityChanged(visibility: Int) {
//                        lllogI("SlideUp onVisibilityChanged visibility : $visibility")
//                    }
//
//                    override fun onSlide(percent: Float) {
//                        lllogI("SlideUp onSlide percent : $percent")
//                    }
//                })
//                .withStartState(SlideUp.State.HIDDEN)
//                .withStartGravity(Gravity.BOTTOM)
//                .withLoggingEnabled(true)
//                .withSlideFromOtherView(b.coor)
//                .build()
//    }

    private fun timeTest() {
        if (isNotBidTime(this)) {
            lllogI("timeCheck is not bidtime $rightNowPHhour")
            toastNormal("not bidtime $rightNowPHhour")
        } else {
            lllogI("timeCheck is bidtime $rightNowPHhour")
            toastNormal("bidtime $rightNowPHhour")
        }
        lllogI("timeCheck is rightNowHour $rightNowHour")
    }

    private fun showRichPreview() {
        val url = "https://news.naver.com/main/read.nhn?mode=LSD&mid=shm&sid1=105&oid=029&aid=0002605033"
        b.rich1.setLink(url, object : ViewListener {
            override fun onSuccess(status: Boolean) {
                lllogI("setUrlPreview onSuccess status $status")
            }

            override fun onError(e: Exception?) {
                lllogI("setUrlPreview onError e $e")
            }
        })
        b.rich2.setLink(url, object : ViewListener {
            override fun onSuccess(status: Boolean) {
                lllogI("setUrlPreview onSuccess status $status")
            }

            override fun onError(e: Exception?) {
                lllogI("setUrlPreview onError e $e")
            }
        })
        b.rich3.setLink(url, object : ViewListener {
            override fun onSuccess(status: Boolean) {
                lllogI("setUrlPreview onSuccess status $status")
            }

            override fun onError(e: Exception?) {
                lllogI("setUrlPreview onError e $e")
            }
        })
        b.rich4.setLink(url, object : ViewListener {
            override fun onSuccess(status: Boolean) {
                lllogI("setUrlPreview onSuccess status $status")
            }

            override fun onError(e: Exception?) {
                lllogI("setUrlPreview onError e $e")
            }
        })
    }


}


/**
 *
 *
 * 1. 전체 포토 리스트 model 만들어서 realdb에 모두 넣을 것.
 * 2. 주말에 admin이 수동으로 각 보드의 배당금 지불할 것.
 * 3. 신고받으면 처리하는 페이지 마스터도 볼 수 잇게. 컨텍스트 메뉴에 넣을 것. 푸쉬도 날려줄 것.
 * 4. 크레딧 다른유저에게 전달은 functions 에서 처리할 것.
 * 5. addBoard 에서 고객센터 보드 분기할 것.
 * 6. 보드 info도 고객센터는 분기할 것.
 * 7. 스토어 올릴떄 마지막부분에 고객센터 여부 선택할 것. info stroe 에서는 고개센터 보드 만들지 선택할 것.
 *
 *
 *
 *
 * ===== 나중에 ========
 *
 *
 * ################ sttReply  ######################################################################
 *       reply adapter 눌렀다 돌아오면 왜 사라지는 거냐... 이럴수가 있나...
 *       swipe reply 구현할 것.
 *        * 댓글 :  익명 업로드 기능 추가.
 *
 *
 *
 *
 * ################ sttAdmin  ######################################################################
 * 어드민 행동 퍼미션 단계 :
 *                  1. peerType으로 체크.
 *                  1. 어드민 uid list에서 체크할 것. 매번 로그인 할 때 한번.
 *                  1. 비밀번호
 *
 *
 *
 * ################ sttSecure
 * 도배방지 및 ddos디도스공격방지를 위해. 최대 업로드 개수제한 할 것
 *      배너, 댓글, 포스트(일일), 보드,
 *      - Issue 리포트 모델 만들어서 보고서 자동작성하게 만들기.
 *      - 테스트 : 100개이상 write시 hit. 차단은 1000개
 *              : 100번이상 read시 hit. 차단은 1000번.
 *
 *
 *
 *
 * ################ sttModel  ######################################################################
 *
 * 유저삭제 다이얼로그
 *     체크박스 모든 게시물 삭제
 *     push book 삭제
 *     모든 board 업로드 차단으로 변경.
 *     모든 store 유저탈퇴 여부 표시.
 *
 *
 *
 * 스토어 :
 *     메인페이지 디자인 변경.
 *
 *
 * 보드 :
 *       보드업로드 권한 설정 리스트.
 *       삭제한 포스팅, 댓글 : 보드 마스터에게는 보이기.
 *
 *       보드 폐쇄 기능? : 신규 업로드만 불가하게..?
 *       CS보드 : 전달금지, 검색금지.
 *
 *       보드타입에 맞게 선택기능.
 *       프리미엄 보드의 기능 추가.
 *
 * board master.
 *      - 글쓰기 가능 유저 리스트.
 *      - 불가능 유저 리스트.
 *
 *
 *
 *
 *
 * status에서 본인이 가리기랑 master가 가리기 다르게 설정할 것.
 *
 *
 * noti에 맞게 post, board, flea, room 모두 다 들어가서 데이터 받는 걸로 바꿔야.. 그럼 check도 안에 들어가서 해야하나?  그 안에서 팝업을 하나 띄우자. initView하지 말고.
 *
 *
 *
 *
 *
 * 신고 ; 신고 모델만 처리하고, 피어는 따로 처리하는 걸로.
 *     : 개별보드 신고에서는 개별 보드에서만 패널티가 적용되어야 하는데... (아니다. 완전 차단시켜야 된다. 그런 유저는 계속 사고친다.)
 *     일단 신고처리는 관리자만. 차후에 각 보드의 allowPeer 만들기 전까지만.
 *
 *
 *
 * 공지사항 올리면
 *     현재 activity를 가져와서 dialog 날리고
 *     5분뒤에는 다이얼로그 다시 날려서 어플을 종료시키게 만들어야한다.
 *     그 뒤로 어플 켜면 Main에서 바로 어플 종료시키는 로직으로 가야 한다.
 *
 *     그러면 다 변경되고 난 뒤에
 *            remote를  새로운 걸로 받아와야 하고
 *            새로운 es 와 연결되는지 확인해야 한다.
 *
 *
 *
 * ################ sttPeer  ######################################################################
 *       * 가입 동의서 : 로그인페이지와  가입페이지는 별도로. 만들 것.
 *
 *
 *
 *
 *
 *
 * ################ sttbBid  ######################################################################
 *     크레딧 번 거 주기 : 크레딧인지 보너스인지 구별해야 하는데... 골치아프네.. 그럼 캐럿은 왜 만든거..?ㅋㅋ
 *
 *
 *     불법 광고시 그 광고 시작된 날짜를 받아서 모든 bid의 status를 광고거부로 변경할 것.
 *      입찰중인 광고는 입찰 취소로 변경 : functiosn에서 해당 날짜에 환불.
 *
 *
 *     입찰시 결제부분.
 *           보너스 1만 이하면 선택 불가능하게...
 *           선택완료시 완료된 결제종류만 남겨줘.
 *
 *
 *     adCredit : AdCredit만 검색할 수 있는 페이지 만들기.
 *
 *
 *
 * 유찰 푸시
 *     : 유찰시 모두 받아서 마지막 유찰푸시에 유찰 수량 적어서 한번만 보여줄 것.
 *     푸쉬 보고 있을때는 오지 말 것.
 *     푸쉬 시간 설정하여 받을지 결정하는 기능 만들 것.
 *
 *
 *
 *
 * ################ sttvCost  ######################################################################
 * 전환 구현하기.
 *
 *
 * 크레딧 : 전환, 전달 기능 만들기. 전달은 lv.5 이상부터.
 *
 *
 *
 *
 * ################ sttSearch  ######################################################################
 *      검색어 실시간 검색 차단하고 마지막에만 검색하게 할 것.
 *
 * 검색창 :
 *      각 페이지 검색
 *      스토어 검색
 *      보드검색
 *      포스팅 검색
 *      전체 통합 검색
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 * ################ sttBasic  ######################################################################
 *      맵핑 금지방법..  act는 모두 맵핑 금지시킬 것.
 *      bid의 안 쓰는 이미지는 나중에 일괄적으로 삭제할 것.  20200101도 삭제 안 되게 짤 것.
 *
 * 라이브러리 업데이트
 * kotlin 업데이트
 * es 업그레이드 하기.
 *
 * 폰트 다시 정리할 것.
 *
 *
 * 다이내믹 링크 - 가입 추천인
 * 출석체크 - 100 : 5일연속 X2 10일연속 X4 20일연속 X8   : 정책 변경가능성 고지.
 *
 *
 *
 *
 *
 * ################ sttTest  ######################################################################
 *       es id pw 처음 론칭시에도 잘 작동되는지 테스트할 것.
 *
 *
 *
 *
 * ################ sttError  ######################################################################
 *
 * 사진 올릴때
 * checkUriExistance (파일 존재유무 확인)
 * 여기서 제거 index오류 해결할 것.
 *
 *
 * 오토 맵핑 금지처리 할 것.
 * 오토 인덱싱 금지 - act
 *
 *
 * 메인광고 이미지 세로사진일때 배경 자동을 채워지게 하는 라이브러리 달기.
 *
 *
 *
 * 금액 기입 모두 다 바꾸기..
 * 버튼형으로.
 *
 *
 * 구글로 가입 후에 바로 추천인 등록여부 물어볼 것.
 *
 *
 * 하루 맥스로 올리는 양은... 10개..? post, board, flea, room, store 모두 합쳐서..
 * 댓글은 300개..?  "오늘 너무 많이 올리 신거 아닌가요? 도배방지를 위해 오늘은 이만..."
 *
 *
 *
 * 광고는 하루단위로 받으면 된다. 매번 받아올 필요가 없다. 하루에 한번만 받아서 sred에 넣어두고 쓰면 된다.
 * 혹시 모르니 실시간 refresh를 remote에 넣자..
 *
 *
 *
 * 실제 결제 및 환불 테스트 할 것.
 * 환불했을 때 수동으로 Bill 환불로 처리 할 것.
 *
 *
 *
 *
 * myCalll 토스트 안 날릴 수 있게..
 * myCommit 딜레이 시간 설정할 수 있게..
 *
 *
 */