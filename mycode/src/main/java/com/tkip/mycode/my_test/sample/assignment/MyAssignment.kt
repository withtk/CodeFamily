package com.tkip.mycode.my_test.sample.assignment

import java.util.*
import kotlin.collections.ArrayDeque
import kotlin.collections.ArrayList

data class BOX(
        var box: String = "box",
        var f: Int = 100,
        var g: Int = 100,
        var h: Int = 100,
        var parent: String? = null
)
    : Comparable<BOX> {
    /*** f 낮은 순으로 정렬*/
    override fun compareTo(other: BOX): Int {
        return if (f >= other.f) 1 else -1
    }
}

val GOAL2 = BOX("123456780", 0, 0, 0, null)
val START2 = BOX("243185706", 100, 0, 100, null)

val openQueue = PriorityQueue<BOX>()
val closeLIST = arrayListOf<BOX>()


fun startLoop() {
    startMyBox(START2)
}

fun startMyBox(box: BOX) {
    println("assignment- start $box  size ${openQueue.size}  ")
    closeLIST.add(box)

    // h==0 이면 성공
    if (box.h == 0) {
        closeLIST.add(box) // 성공BOX 저장하기.
        println("assignment- finish success $box  #################")
        resultReport()  // 결과 보고
        return
    }

    /** * 이동가능한 위치 목록 만들기 */
    getMoveBlankList(box)
    openQueue.poll()?.let { startMyBox(it) } // 다시 시작.
}

private fun getMoveBlankList(box: BOX) {
    val listToMove = box.box.indexOf("0").aroundNo()  // 현재위치에서 가능한 이동위치 목록
    listToMove?.first?.forEach { loc -> checkAndAddToOpen(createNextBOX(box, loc, null).setHeuristic()) }  // 한칸 이동
    listToMove?.second?.forEach { loc -> checkAndAddToOpen(createNextBOX(box, loc.first, loc.second).setHeuristic()) }  // 두칸 이동
}

/** * 중복 체크하고 삽입. */
private fun checkAndAddToOpen(box: BOX) {
    closeLIST.forEach { if (it.box == box.box) return }
    closeLIST.add(box)
    openQueue.offer(box)
}


/** * 다음 박스 만들기 */
fun createNextBOX(box: BOX, loc: Int, loc2: Int?): BOX {
    /** * 해당위치의 번호변경 */
    val arr = box.box.toCharArray()
    arr[box.box.indexOf(z)] = arr[loc]  // 빈 공간 위치.
    arr[loc] = z   // -> 0
    val result1 = StringBuffer().apply { arr.forEach { append(it) } }.toString()
    val newBOX1 = BOX(result1, 0, box.g + 1, 0, box.box)

    loc2?.let { second ->
        val arr2 = result1.toCharArray()
        arr2[result1.indexOf(z)] = arr[second]  // 빈 공간 위치.
        arr2[second] = z   // -> 0
        val result2 = StringBuffer().apply { arr2.forEach { append(it) } }.toString()
        return BOX(result2, 0, box.g + 1, 0, box.box)
    }
    return newBOX1
}

/*** h 받아오기 : 틀린 위치 개수 */
private fun BOX.setHeuristic(): BOX {
    var i = 0
    (this.box.indices).forEachIndexed { index, _ -> if (GOAL[index] != this.box[index]) i++ }
    this.h = i   // BOX.h에 대입.
    this.f = this.g + i
    return this
}


/*** 결과 보고 */
fun resultReport() {
    closeLIST.sortBy { it.g }
    closeLIST.forEach {
        println("assignment CLOSE_LIST box $it ")
    }
}


//val openLIST = arrayListOf<BOX>()
//val closeLIST = arrayListOf<BOX>()

//
//fun BOX.startMyBox() {
//    if (this.g > 20) return   // 일단 20번까지만 제한
////    println("assignment- startMyBox $this ")
//
//    // OPEN, CLOSE에 삭제/삽입
////    openLIST.remove(this)
//    closeLIST.add(this)
//
//    // 성공이면 끝.
//    if (this.h == 0) {
////        closeLIST.add(this) // 성공BOX 저장하기.
//        println("assignment- finish success $box  #################")
//        resultReport()  // 결과 보고
//        return
//    }
//
//    // 노드 확장하여 OPEN에 넣기.
//    makeNode(this)
//
//    /**
//     * <openLIST 탐색>
//     * 1. 이번 box를 parent로 갖는 OPEN중에서
//     * 2. h가 가장 낮은 BOX 선정하기.
//     * 3. 중복 제거.
//     */
//    var chosenBOX: BOX? = null
//    val arr = openLIST.toTypedArray()
//    arr.sortBy { b -> b.h }  // h 순으로 정렬
//
//    run loop@{
//        arr.forEach { b ->
//            if (b.parent == this.box) {
//                chosenBOX = b
//                println("assignment chosenBOX  $chosenBOX SSSSSSSSSSSSSSSSSSS")
//                return@loop
//            }
//        }
//    }
//
//
//    // 선정하여 다시 시작하기 or 실패 보고.
//    if (chosenBOX == null) {
//        println("assignment failed ############################ ")
//        resultReport()  // 결과 보고
//    } else chosenBOX!!.startMyBox()
//}
//
///*** 결과 보고 */
//fun resultReport() {
//    closeLIST.forEach {
//        println("assignment closeList box $it ")
//        println("assignment closeList box \n${it.box.substring(0, 3)}\n${it.box.substring(3, 6)}\n${it.box.substring(6, 9)}\n ")
//    }
//}
//
//
//// 노드 확장
//fun makeNode(box: BOX) {
//    val listToMove = box.box.indexOf("0").aroundNo()  // 현재위치에서 가능한 이동위치 목록
//    /*** 한칸이동 */
//    listToMove?.first?.forEach { loc -> nextBox(box, loc, null) }
//    /*** 두칸이동 */
//    listToMove?.second?.forEach { loc -> nextBox(box, loc.first, loc.second) }
//
//    println("assignment openLIST size ${openLIST.size} OOOOOOOOOOOOOOOOOOOOOO ")
//    openLIST.forEach {
//        println("assignment openLIST box $it ")
//    }
//}
//
//
///** * 다음 박스 만들기 */
//fun nextBox(box: BOX, loc: Int, loc2: Int?) {
//
////    println("assignment nextBox $this   ")
//
//    /** * 해당위치의 번호변경 */
//    val arr = box.box.toCharArray()
//    arr[box.box.indexOf(z)] = arr[loc]  // 빈 공간 위치.
//    arr[loc] = z   // -> 0
//    val result1 = StringBuffer().apply { arr.forEach { append(it) } }.toString()
//    val newBOX1 = BOX(result1, 0, box.g + 1, 0, box.box)
//
//    loc2?.let { second ->
//        val arr2 = result1.toCharArray()
//        arr2[result1.indexOf(z)] = arr[second]  // 빈 공간 위치.
//        arr2[second] = z   // -> 0
//        val result2 = StringBuffer().apply { arr2.forEach { append(it) } }.toString()
//        val newBOX2 = BOX(result2, 0, box.g + 1, 0, box.box)
//        return addToOpen(newBOX2)
//    }
//
//    /** * OPEN에 넣기 */
//    addToOpen(newBOX1)
//}
//
///** * 새로운 박스 OPEN 리스트에 넣기 */
//private fun addToOpen(newBox: BOX) {
//    openLIST.forEach {
//        if (newBox.box == it.box) return // 중복은 저장 안 하고 패스.
//    }
//
//    newBox.h = newBox.box.getHeuristic()
//    openLIST.add(newBox)
////    println("assignment addToOpen $this   ")
//}


//////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////

val GOAL = "123456780"
val START = "243185706"
private const val z: Char = '0'  // 빈 공간

//private val START by lazy {    arrayOf(2, 4, 3, 1, 8, 5, 7, 9, 6).asList() as ArrayList<Int>}

private val closeList = arrayListOf<String>()

//val openSet = hashSetOf<String>()
//val closeSet = hashSetOf<String>()
val openMap = hashMapOf<String, Pair<Int, Int>>()
val closeMap = hashMapOf<String, Pair<Int, Int>>()
fun newMap() = hashMapOf<String, Pair<Int, Int>>()
val openStack = ArrayDeque<Pair<String, Int>>()

//private fun llist(arr: Array<Int>): ArrayList<Int> = arrayListOf<Int>().apply { arr.forEach { add(it) } }
fun createList(noStr: String): ArrayList<Int> = arrayListOf<Int>().apply { (noStr.indices).forEach { add(noStr[it].toString().toInt()) } }

/*********************************************************************************
 * 인접번호 리스트 <1칸, 2칸>
 * 0 1 2
 * 3 4 5
 * 6 7 8
 **********************************************************************************/
private val no0 = Pair(arrayOf(1, 3), arrayOf(Pair(1, 2), Pair(3, 6)))
private val no1 = Pair(arrayOf(0, 2), arrayOf(Pair(4, 7)))
private val no2 = Pair(arrayOf(1, 5), arrayOf(Pair(1, 0), Pair(5, 8)))

private val no3 = Pair(arrayOf(0, 4, 6), arrayOf(Pair(4, 5)))
private val no4 = Pair(arrayOf(1, 3, 5, 7), null)
private val no5 = Pair(arrayOf(2, 4, 8), arrayOf(Pair(4, 3)))

private val no6 = Pair(arrayOf(3, 7), arrayOf(Pair(3, 6), Pair(7, 8)))
private val no7 = Pair(arrayOf(4, 6, 8), arrayOf(Pair(4, 1)))
private val no8 = Pair(arrayOf(5, 7), arrayOf(Pair(5, 2), Pair(7, 6)))


//
//fun startMyBox(box: String, pair: Pair<Int, Int>) {
//    println("assignment- startMyBox $box ${pair.first} ${pair.second}")
//
////    openMap.remove(box)
//    if (pair.second == 0) {
//        closeMap[box] = pair
//        println("assignment- finish success $box ${pair.first} ${pair.second}")
//        return
//    }
//
//    val newMap = box.makeMap(pair.first + 1, pair.second)
//    var beforeBox: String? = null
//    newMap.forEach { (box, p) ->
//        if (beforeBox == null) beforeBox = box
//        else {
//            val beforeP = newMap[beforeBox]
//            if ((p.first + p.second) <= (beforeP!!.first + beforeP.second)) beforeBox = box
//        }
//    }
//
//    startMyBox(beforeBox!!,newMap[beforeBox]!!)
//
//    closeMap[box] = pair
////    checkOPEN()
//
////    openMap[box] = Pair(i, 0)
//
////    openMap.forEach { (box, p) ->
////        box.makeNext(i+1, p.second)
////    }
//
//}
//
//
//
//fun checkOPEN() {
//
//    var beforeBox: String? = null
//    openMap.forEach { (box, p) ->
//        if (beforeBox == null) beforeBox = box
//        else {
//            val beforeP = openMap[beforeBox]
//            if ((p.first + p.second) <= (beforeP!!.first + beforeP.second)) beforeBox = box
//        }
//
//    }
//    println("openMap count:${openMap[beforeBox]!!.first} beforeBox:$beforeBox  ")
//    startMyBox(beforeBox!!, openMap[beforeBox]!!)
//}
//
//
//fun String.makeMap(g: Int, f: Int): HashMap<String, Pair<Int, Int>> {
//
//    val map = newMap()
//    val listToMove = this.indexOf("0").aroundNo()  // 현재위치에서 가능한 이동위치 목록
//
//    /*** 한칸이동 */
//    listToMove?.first?.forEach { loc ->
////        this.getNextBox(g,loc, null).addOPEN(g, f)
//        this.nextBox(map, g, loc, null)
//    }
//
//    /*** 두칸이동 */
//    listToMove?.second?.forEach { loc ->
//        this.nextBox(map, g, loc.first, loc.second)
//    }
//
//    return map
//}
////map:HashMap<String,Pair<Int,Int>>,
//
//
///** * 다음 박스 만들기 */
//fun String.nextBox(map: HashMap<String, Pair<Int, Int>>, g: Int, loc: Int, loc2: Int?) {
////    println("assignment- getNextBox $this $loc $loc2")
//
//    /** * 해당위치의 번호변경 */
//    val arr = this.toCharArray()
//    arr[this.indexOf(z)] = arr[loc]  // 빈 공간 위치.
//    arr[loc] = z   // -> 0
//
//    val result1 = StringBuffer().apply { arr.forEach { append(it) } }.toString()
////    println("assignment- getNextBox11 $result1")
//
//    loc2?.let { second ->
//        val arr2 = result1.toCharArray()
//        arr2[result1.indexOf(z)] = arr[second]  // 빈 공간 위치.
//        arr2[second] = z   // -> 0
//        val result2 = StringBuffer().apply { arr2.forEach { append(it) } }.toString()
//        return result2.addToOpen(map, g)
//    }
//
//    /** * OPEN에 넣기 */
//    result1.addToOpen(map, g)
//}
//
//private fun String.addToOpen(map: HashMap<String, Pair<Int, Int>>, g: Int) {
//    val h = this.getHeuristic()
////    println("assignment- getthis  getHeuristic $this $h")
//    map[this] = Pair(g, h)
//}

//////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////

///** * 다음 박스 만들기 */
//fun String.createAndAdd(g: Int, loc: Int, loc2: Int?) {
//    println("assignment- getNextBox $this $loc $loc2")
//
//    /** * 해당위치의 번호변경 */
//    val arr = this.toCharArray()
//    arr[this.indexOf(z)] = arr[loc]  // 빈 공간 위치.
//    arr[loc] = z   // -> 0
//
//    val result1 = StringBuffer().apply { arr.forEach { append(it) } }.toString()
//    println("assignment- getNextBox11 $result1")
//
//    loc2?.let { second ->
//        val arr2 = result1.toCharArray()
//        arr2[result1.indexOf(z)] = arr[second]  // 빈 공간 위치.
//        arr2[second] = z   // -> 0
//        val result2 = StringBuffer().apply { arr2.forEach { append(it) } }.toString()
//        result2.addToOpen(g)
//        println("assignment- getNextBox22 $result2")
//        return
//    }
//
//
//    /** * OPEN에 넣기 */
//    result1.addToOpen(g)
//}
//
//private fun String.addToOpen(g: Int) {
//    val h = this.getHeuristic()
//    println("assignment- getthis  getHeuristic $this $h")
//    if (h == 0) println("assignment- success ~!!!") // 박스 완성.
//    else {
////        val f = g + h + beforeF
//        if (openMap[this] == null) openMap[this] = Pair(g, h)
//        else if (h < openMap[this]!!.second) openMap[this] = Pair(g, h)
//    }
//}


/*** 틀린 위치 개수 : 0이면 박스 완성. */
private fun String.getHeuristic(): Int {
    var i = 0
    (this.indices).forEachIndexed { index, _ ->
//        println("assignment- countWrongNo ($this) GOAL:${GOAL[index]}  this:${this[index]}  ${(GOAL[index] != this[index])}")
        if (GOAL[index] != this[index]) i++
    }
    return i
}

/*** 해당위치의 이동 가능한 위치목록 */
private fun Int.aroundNo() =
        when (this) {
            0 -> no0
            1 -> no1
            2 -> no2
            3 -> no3
            4 -> no4
            5 -> no5
            6 -> no6
            7 -> no7
            8 -> no8
            else -> null
        }


//fun makeNext(g: Int, f: Int, list:ArrayList<Int>, root: StringBuffer?) {
//
//    val listToMove = list.indexOf("0").aroundNo()  // 현재위치에서 가능한 이동위치 목록
//
//    /*** 한칸이동 */
//    listToMove?.first?.forEach { loc ->
//        val newList = list.new()
//        changeNo(newList, loc)
//        addBoxToOpen(g + 1, f, newList, root?.append("$loc") ?: StringBuffer("$loc"))
//    }
//
//    /*** 두칸이동 */
//    listToMove?.second?.forEach { loc ->
//        val newList = list.new()
//        changeNo(newList, loc.first)
//        changeNo(newList, loc.second)
//        addBoxToOpen(g + 1, f, newList, root?.append("$loc") ?: StringBuffer("$loc"))
//    }
//
//}
//
///** * 해당위치의 번호변경 */
//private fun changeNo(newList: ArrayList<Int>, loc: Int) {
//    newList[newList.indexOf(0)] = newList[loc]  // 변경할 번호
//    newList[loc] = 0
//}

private fun ArrayList<Int>.new(): ArrayList<Int> {
    val newList = arrayListOf<Int>()
    newList.addAll(this)
    return newList
}

//private fun addBoxToOpen(g: Int, beforeF: Int, list: ArrayList<Int>, root: StringBuffer) {
//    val result = StringBuffer().apply { list.forEach { append("$it") } }.toString()   // 일렬로 만들기
//    println(result)
//
//    val h = getWrongNo(result)
//    if (h == 0)    // 박스 완성.
//    else {
//        val f = g + h + beforeF
//        if (openMap[result] == null) openMap[result] = f
//        else if (f < openMap[result]!!) openMap[result] = f
//        reStart()
//    }

//    root.append()
//    if (buffer.toString() == GOAL) {
//        println("assignment- success $$$$$$$$$$$$$$$$$$$ $root")
//    } else {
//        boxInit(list,root)
//    }

//    println("assignment- boxInit ########\n${list[0]}${list[1]}${list[2]}\n${list[3]}${list[4]}${list[5]}\n${list[6]}${list[7]}${list[8]}")

//}

//fun getGreatRoot():String?{
//    val before = ""
//    resultMap.forEach { (box, f) ->
//        if(before)
//    }
//    return null
//}


/** * 0번(빈칸) 위치 가져오기. */
private fun Array<Int>.getLoc9(): Int? {
    this.forEachIndexed { index, i -> if (i == 9) return index }
    return null
}


/*********************************************************************************
 **********************************************************************************/
