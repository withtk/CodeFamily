package com.tkip.mycode.my_test.sample.assignment

import android.os.Bundle
import android.widget.LinearLayout
import androidx.databinding.DataBindingUtil
import com.mancj.slideup.SlideUp
import com.tkip.mycode.R
import com.tkip.mycode.databinding.ActivityTttttest2Binding
import com.tkip.mycode.funs.common.onClick
import com.tkip.mycode.init.my_bind.BaseActivity
import com.tkip.mycode.model.board.Board
import com.tkip.mycode.util.tools.anim.animStopTurning
import com.tkip.mycode.util.tools.anim.animTurning

class TTTest2222Activity : BaseActivity() {

    private lateinit var b: ActivityTttttest2Binding
    private lateinit var slideUp: SlideUp
    private val listBoard: ArrayList<Board> = arrayListOf()


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        b = DataBindingUtil.setContentView(this, R.layout.activity_tttttest2)
        b.activity = this

//        showRichPreview()
//        setSlideUp()

        b.btnTest4.onClick {
            b.btnTest4.animTurning()

//            START2.startLoop()

        }

        b.btnTest5.onClick {
            b.btnTest4.animStopTurning()
            makeView()
            //            slideUp.hide()
        }

    }

    fun makeView() {
//        val arr = closeLIST.toTypedArray()
        closeLIST.sortBy { b -> b.g }  //  순으로 정렬
//        arr.sortByDescending { b -> b.g }  // 역순으로 정렬
        b.lineAaa.addView(makeLinear())
        var curG = 0
        closeLIST.forEach { box ->
            b.lineAaa.addView(ViewTvBox(this, box))
//            if(box.g > curG) b.lineAaa.addView(makeLinear())
//            curG = box.g
//            (b.lineAaa.getChildAt(box.g) as LinearLayout).addView(ViewTvBox(this, box))
        }
    }

    private fun makeLinear(): LinearLayout {
        val layout2 = LinearLayout(this)
        layout2.layoutParams = LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT)
        layout2.orientation = LinearLayout.HORIZONTAL
        return layout2
    }

//    private fun setSlideUp() {
//        slideUp = SlideUpBuilder(b.line)
//                //.withGesturesEnabled()
//                //.withHideSoftInputWhenDisplayed()
//                //.withInterpolator()
//                //.withAutoSlideDuration()
//                //.withTouchableAreaPx()
//                //.withTouchableAreaDp()
//                //.withListeners()
//                //.withSavedState()
//                .withListeners(object : SlideUp.Listener.Events {
//                    override fun onVisibilityChanged(visibility: Int) {
//                        lllogI("SlideUp onVisibilityChanged visibility : $visibility")
//                    }
//
//                    override fun onSlide(percent: Float) {
//                        lllogI("SlideUp onSlide percent : $percent")
//                    }
//                })
//                .withStartState(SlideUp.State.HIDDEN)
//                .withStartGravity(Gravity.BOTTOM)
//                .withLoggingEnabled(true)
//                .withSlideFromOtherView(b.coor)
//                .build()
//    }
}