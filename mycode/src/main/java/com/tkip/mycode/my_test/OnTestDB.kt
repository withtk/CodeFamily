package com.tkip.mycode.my_test

import android.util.Log
import com.google.firebase.firestore.DocumentReference
import com.google.firebase.firestore.WriteBatch
import com.tkip.mycode.R
import com.tkip.mycode.dialog.toast.toastAlert
import com.tkip.mycode.dialog.toast.toastNormal
import com.tkip.mycode.funs.common.lllogD
import com.tkip.mycode.funs.common.lllogI
import com.tkip.mycode.init.*
import com.tkip.mycode.init.base.Cons
import com.tkip.mycode.model.bid.Bid
import com.tkip.mycode.model.board.Board
import com.tkip.mycode.model.flea.Flea
import com.tkip.mycode.model.my_enum.dataType
import com.tkip.mycode.model.peer.myuid
import com.tkip.mycode.model.post.Post
import com.tkip.mycode.model.reply.Reply
import com.tkip.mycode.model.room.Room
import com.tkip.mycode.model.store.Store
import com.tkip.mycode.util.lib.retrofit2.ESget
import com.tkip.mycode.util.lib.retrofit2.ESquery
import com.tkip.mycode.util.tools.fire.*


class OnTestDB {

    companion object {


        /**************************************************************************************
         *************************** 일회용  **************************************************
         *************************************************************************************/


        private val uploadList by lazy {  arrayListOf<Triple<DocumentReference, String?, Any>>()}

        fun changeAllData() {
            val method = "changeAllData"
            val query = ESquery.matchAll(2000, 0)
//            val child = CH_REPLY
            val index = EES_POST
//            val myClass = Peer::class.java

            uploadList.clear()

            ESget.getPostList(query,
                    success = { resultList ->
                        lllogD("$method  resultList  size:${resultList?.size}")
                        resultList?.forEach { post ->
                        }
                        lllogD("$method uploadList  ${uploadList.size}")
                    }, fail = { lllogI("OnTotalDB ESget fail!") })


//                batch().apply {
//                    colRef(EES_POST)
//                            .get()
//                            .addOnSuccessListener {
//                                it.forEach { snap ->
//                                    val d = snap.toObject(Post::class.java)
//                                    val value = d.addDate + 1
//                                    update(docRef(EES_POST, d.key), CH_ADD_DATE, value)
//
//
////                                    lllogI("OnTotalDB   :${d.name}")
////                                    d.addDate = d.addDate + 1
////                                    set(docRef(CH_MYTAG, d.key), d)
//                                }
//                                myCommit("OnTotalDB  ESget",
//                                        success = {
//                                            toastNormal(R.string.complete_upload)
//                                        },
//                                        fail = { toastAlert(R.string.db_no_data) }
//                                )
//
//                            }.addOnFailureListener {
//                                lllogI("OnTotalDB fail")
//
//                            }
//                }


        }

        fun addAll() {
//            batch().apply {
//                testListPost.forEach { set(docRef(CH_POST, it.key), it) }
//                myCommit("changeAllData",
//                        success = { toastNormal(R.string.complete_upload) },
//                        fail = { toastAlert(R.string.db_no_data) })
//            }

        }

        fun changeAllData2COL() {
//            val query = ESquery.matchAll(200, 0)

            batch().apply {
                colRef(EES_PEER, myuid(), EES_BANNER)
                        .get()
                        .addOnSuccessListener {
                            it.forEach { snap ->
                                val d = snap.toObject(Bid::class.java)
                                lllogI("OnTotalDB  snap  !   :${d}")
                                update(docRef( EES_BANNER, d.key), CH_ADD_DATE, d.addDate + 1)
                            }
                            myCommit("OnTotalDB  ESget",
                                    success = { toastNormal(R.string.complete_upload) },
                                    fail = { toastAlert(R.string.db_no_data) }
                            )

                        }
                        .addOnFailureListener { lllogI("OnTotalDB fail") }
            }

        }


        /**************************************************************************************
         *************************** sttReply  **************************************************
         *************************************************************************************/
        val testListReply = arrayListOf<Reply>()

        fun replyBackup() {
            val query = ESquery.matchAll(500, 0)
            ESget.getReplyList(query,
                    success = { list ->
                        lllogI("OnTotalDB  ESget get!  size:${list?.size}")
                        if (!list.isNullOrEmpty()) testListReply.addAll(list)
                    }, fail = { lllogI("OnTotalDB ESget fail!") })
        }

        fun replyReAdd() {
            batch().apply {
                testListReply.forEach {
                    val topChild = it.dataTypeNo.dataType().child
                    val modelKey = it.modelKey
                    // 날짜 숫자 바꿔야 함.
                    set(docRef(topChild, modelKey, EES_REPLY, it.key), it)
                }
                myCommit("replyReAdd", { lllogI("OnTotalDB replyReAdd success ") }, { lllogI("OnTotalDB replyReAdd fail  ") })
            }
        }


        /**************************************************************************************
         *************************** sttPost  **************************************************
         *************************************************************************************/

        fun updatePost() {
            var batch = batch()
            val buffer = StringBuffer()

            Firee.commonFirstQuery(arrayOf(EES_POST), null, {
                buffer.append("==== size : ${it.size()} \n\n")
                it.forEachIndexed { i, snap ->
                    val post = snap.toObject(Post::class.java)
                    buffer.append("  ${post.title}\n")
                    batch.update(docRef(EES_POST, post.key), CH_ADD_DATE, post.addDate + 1L)
//                Firee.addPost(post, {}, {})
                    if (i == 499) {
                        batchCommit(batch, buffer)
                        batch = batch()
                    }
                }
                batchCommit(batch, buffer)

            }, {})
        }

        private fun batchCommit(batch: WriteBatch, buffer: StringBuffer) {
            batch.commit()
                    .addOnSuccessListener { Log.d(Cons.TAG, "OnTotalDB    addOnSuccessListener!  \n $buffer") }
                    .addOnFailureListener { Log.d(Cons.TAG, "OnTotalDB    addOnFailureListener!") }
        }


        /**************************************************************************************
         *************************** sttPost  **************************************************
         *************************************************************************************/

        fun updateBoard() {
            var batch = batch()
            val buffer = StringBuffer()

            Firee.commonFirstQuery(arrayOf(EES_BOARD), null, {
                buffer.append("==== size : ${it.size()} \n\n")
                it.forEachIndexed { i, snap ->
                    val board = snap.toObject(Board::class.java)
                    buffer.append("  ${board.title}\n")

                    batch.update(docRef(EES_BOARD, board.key), "boardTypeNo", 20)
                    if (i == 499) {
                        batchCommit(batch, buffer)
                        batch = batch()
                    }
                }
                batchCommit(batch, buffer)

            }, {})
        }


        private val listBoard: ArrayList<Board> = arrayListOf()
        private val listPost: ArrayList<Post> = arrayListOf()
        private val listFlea: ArrayList<Flea> = arrayListOf()
        private val listRoom: ArrayList<Room> = arrayListOf()
        private val listStore: ArrayList<Store> = arrayListOf()

        fun getAllModel() {

            val query = ESquery.matchAll(500, 0)

            ESget.getBoardList(query, success = {
                if (!it.isNullOrEmpty()) {
                    it.forEachIndexed { i, model ->
                        listBoard.add(model)
                        lllogD("TTTestActivity  listBoard  model___$i  ")
                    }
                }
            }, fail = {})
            ESget.getStoreList(query, success = {
                if (!it.isNullOrEmpty()) {
                    it.forEachIndexed { i, model ->
                        listStore.add(model)
                        lllogD("TTTestActivity listStore  model___$i  ")
                    }
                }
            }, fail = {})
            ESget.getFleaList(query, success = {
                if (!it.isNullOrEmpty()) {
                    it.forEachIndexed { i, model ->
                        listFlea.add(model)
                        lllogD("TTTestActivity listFlea  model___$i  ")
                    }
                }
            }, fail = {})
            ESget.getRoomList(query, success = {
                if (!it.isNullOrEmpty()) {
                    it.forEachIndexed { i, model ->
                        listRoom.add(model)
                        lllogD("TTTestActivity  listRoom model___$i  ")
                    }
                }
            }, fail = {})

            ESget.getPostList(query, success = {
                if (!it.isNullOrEmpty()) {
                    it.forEachIndexed { i, model ->
                        listPost.add(model)
                        lllogD("TTTestActivity listPost  model___$i  ")
                    }
                }
            }, fail = {})
        }

        fun reUpdate() {

            batch().apply {
                listBoard.forEach { update(docRef(EES_BOARD, it.key), CH_ADD_DATE, it.addDate + 1) }
                listPost.forEach { update(docRef(EES_BOARD, it.key), CH_ADD_DATE, it.addDate + 1) }
                listFlea.forEach { update(docRef(EES_BOARD, it.key), CH_ADD_DATE, it.addDate + 1) }
                listRoom.forEach { update(docRef(EES_BOARD, it.key), CH_ADD_DATE, it.addDate + 1) }
                listStore.forEach { update(docRef(EES_BOARD, it.key), CH_ADD_DATE, it.addDate + 1) }
            }.myCommit("reUpdate",
                    success = {
                        lllogD("TTTestActivity reUpdate  success  ")
                    }, fail = {})
        }

    }

}