package com.tkip.mycode.my_test

import org.json.JSONObject

class Json (){
    val json = JSONObject()

    constructor(init: Json.() -> Unit) : this() {
        this.init()
    }

    infix fun <T> String.to(value: T) {
        json.put(this, value)
    }

    override fun toString(): String {
        return json.toString()
    }
}


/*

val json0  = Json {
    "height" to 173
    "weight" to 80
}

val json1 = Json {
    "name" to "Roy"
    "body" to json0
}
val json = JSONObject(mapOf(
        "name" to "ilkin",
        "age" to 37,
        "male" to true,
        "contact" to mapOf(
                "city" to "istanbul",
                "email" to "xxx@yyy.com"
        ),
        "contact" to arrayOf(
                "city" to "istanbul",
                "email" to "xxx@yyy.com"
        ),
        "cars" to JSONArray().apply {
            put("Tesla")
            put("Porsche")
            put("BMW")
            put("Ferrari")
        }
))

println(" $json1")
println(" $json")
*/
