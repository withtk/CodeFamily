package com.tkip.mycode.my_test.sample.assignment

import android.content.Context
import android.view.LayoutInflater
import android.widget.LinearLayout
import android.widget.TextView
import androidx.databinding.DataBindingUtil
import com.tkip.mycode.R
import com.tkip.mycode.databinding.ViewTvBoxBinding
import com.tkip.mycode.init.base.getClr
import com.tkip.mycode.util.tools.anim.hide

/**
 */
class ViewTvBox : LinearLayout {
    lateinit var b: ViewTvBoxBinding
    val boolList: ArrayList<Boolean> = arrayListOf()
    lateinit var noStr: String

    constructor(context: Context) : super(context)
    constructor(context: Context,   box: BOX) : super(context) {
        b = DataBindingUtil.inflate(LayoutInflater.from(context), R.layout.view_tv_box, this, true)
        b.msg = "(${box.g + box.h} = ${box.g} + ${box.h})"
        this.noStr = box.box

        (box.box.indices).forEachIndexed { index, _ ->
            boolList.add((GOAL[index] == box.box[index]))
        }

        b.tv1.setNo(0)
        b.tv2.setNo(1)
        b.tv3.setNo(2)

        b.tv4.setNo(3)
        b.tv5.setNo(4)
        b.tv6.setNo(5)

        b.tv7.setNo(6)
        b.tv8.setNo(7)
        b.tv9.setNo(8)

        if(box.h == 0) b.cons.setBackgroundColor(R.color.colorAccent.getClr())
    }

    private fun TextView.setNo(i: Int) {
        if (noStr[i].toString() == "0") this.hide()
        else {
            text = noStr[i].toString()
            if (!boolList[i]) setBackgroundColor(R.color.md_pink_300.getClr())
        }
    }


}