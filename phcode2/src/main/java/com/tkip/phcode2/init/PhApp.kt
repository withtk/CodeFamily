package com.tkip.phcode2.init

import android.app.Application
import android.content.Context
import android.content.Intent
import com.tkip.mycode.funs.common.lllogD
import com.tkip.mycode.funs.common.lllogI
import com.tkip.mycode.funs.common.showLog
import com.tkip.mycode.init.COUNTRY
import com.tkip.mycode.init.base.getDrawable
import com.tkip.mycode.init.base.getStr
import com.tkip.mycode.init.inter.OnExitInterface
import com.tkip.mycode.init.part.CodeApp
import com.tkip.phcode2.BuildConfig
import com.tkip.phcode2.PhMainActivity
import com.tkip.phcode2.R


class PhApp : Application() {

    init {
        instance = this
    }

    companion object {
        private var instance: PhApp? = null
        fun getAppContext(): PhApp {
            return instance as PhApp
        }
    }

//    var db: FirebaseFirestore? = null

    override fun onCreate() {
        super.onCreate()
        showLog = true   // 로그 스위치
        lllogI("MyAPP PhApp onCreate")

//        Fabric.with(this, Crashlytics())


        CodeApp(applicationContext)
        COUNTRY = "ph_"  // todo : ph_ 다른 나라 prefix.
        CodeApp.versionCode = BuildConfig.VERSION_CODE
        CodeApp.versionName = BuildConfig.VERSION_NAME
        CodeApp.clientId = R.string.default_web_client_id.getStr()
        CodeApp.icon = R.drawable.ic_logo_box.getDrawable()


        CodeApp.interExit = object : OnExitInterface{
            override fun onContext(context: Context) {
                lllogD("PhApp interTTTTest interTTTTest ")
                context.startActivity(Intent(context,PhMainActivity::class.java))
            }
        }


//        OnAuth.auth = FirebaseAuth.getInstance()

//        setInit()

    }
//
//    private fun setInit() {
//
//        Fabric.with(this, Crashlytics())
//
//        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
//            NotiChannelManager.createGroupChannel(applicationContext)     // NotiManager  setup
//        }
//
//
//        /** peer 초기화 */
//        Sred.init(applicationContext)                              // Sred  setup
//        val realPeer = Sred.getObjectPeer(Sred.PEER_REAL)
//        lllogI("MyApp realPeer.uid: " + realPeer.uid)
//        OnPeer.setUpPeer(realPeer)  // 앱 실행때마다
//        OnPeer.peer = realPeer
//
//
//        val settings = FirebaseFirestoreSettings.Builder()         // Firee Settings  setup
//            .setPersistenceEnabled(true)
//            .build()
//        //        db = FirebaseFirestore.getInstance()                       // Firee  setup
//        FirebaseFirestore.getInstance().firestoreSettings = settings   // Firee  setup
//
//        initRemoteConfig()                                         // RemoteConfig  setup
//
//        /**         * 기본 게시판 셋팅         */
//        Board.setDefaultBoard()
//
//        //        /** retrofit2 header 초기화 */
//        //        Cons.headerMap["Authorization"] = Credentials.basic(getString(R.string.elastic_user), getString(R.string.elastic_pw))
//
//        Places.initialize(applicationContext, getString(R.string.my_google_maps_api_key))
//
//        /**refreshLayout*/
//        SmartRefreshLayout.setDefaultRefreshHeaderCreator { context, layout ->
//            layout.setPrimaryColorsId(
//                R.color.colorAccent,
//                com.tkip.mycode.R.color.white
//            ) // 테마 색을 전체적으로 설정 하거나
//            //                    .SetTimeFormat (new DynamicTimeFormat ( "% s" 업데이트) //클래식 헤드로 지정, 기본값은 베지어 입니다
//            return@setDefaultRefreshHeaderCreator ClassicsHeader(context)
//        }
//        SmartRefreshLayout.setDefaultRefreshFooterCreator { context, _ ->
//            return@setDefaultRefreshFooterCreator ClassicsFooter(context).setDrawableSize(20f)
//        }
//    }
//
//    private fun initRemoteConfig() {
//        lllogI("MyApp initRemoteConfig start")
//
//        val remoteConfig = FirebaseRemoteConfig.getInstance()
//        val configSettings = FirebaseRemoteConfigSettings.Builder()
//            .setDeveloperModeEnabled(BuildConfig.DEBUG)
////                .setMinimumFetchIntervalInSeconds(4200)
//            .build()
//
//        /* long cacheExpiration = 3600; // 1 hour in seconds.
//        // If your app is using developer mode, cacheExpiration is set id 0, so each fetch will
//        // retrieve values from the service.
//        if (remoteConfig.getInfo().getConfigSettings().isDeveloperModeEnabled()) {
//            cacheExpiration = 0;
//        }*/
//
//        remoteConfig.setConfigSettingsAsync(configSettings)
//        remoteConfig.setDefaults(R.xml.remote_config_defaults)
//
//        remoteConfig.fetch(if (BuildConfig.DEBUG) 0 else TimeUnit.MINUTES.toSeconds(10))
//            .addOnCompleteListener { task ->
//
//                if (task.isSuccessful) {
//                    remoteConfig.activate()
//                } else {
//                    toastAlert(R.string.db_error_remote_config_get_data)
//                }
//
//                // --------------------remote data 정리-------------------------
////                    val lastestVersion = remoteConfig.getString("latest_version")
//
//                LIMIT_UPLOAD_COUNT = remoteConfig.getLong("limit_upload_count").toInt()
//                WIDTH_THUMB = remoteConfig.getLong("width_thumb").toInt()
//                WIDTH_MID = remoteConfig.getLong("width_mid").toInt()
//
//
//            }
//            .addOnFailureListener { e -> lllogW(e) }
//
//
//    }


}
